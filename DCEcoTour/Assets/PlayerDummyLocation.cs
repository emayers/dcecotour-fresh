﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRubyShared;
using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;

public class PlayerDummyLocation : MonoBehaviour {

	[SerializeField]
	private AbstractMap _map;


	public double lat;
	public double lng;

	Vector3 _targetPosition;



	// Use this for initialization
	void Start () {

		_targetPosition = Conversions.GeoToWorldPosition (lat, lng, _map.CenterMercator, _map.WorldRelativeScale).ToVector3xz ();
		_targetPosition.y = 2;
		transform.localPosition = _targetPosition;
		
	}
	

}
