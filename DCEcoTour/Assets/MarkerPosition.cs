
	using Mapbox.Unity.Location;
	using Mapbox.Unity.Utilities;
	using Mapbox.Unity.Map;
	using Mapbox.Utils;
	using UnityEngine;

	public class MarkerPosition : MonoBehaviour
	{
		[SerializeField]
		private AbstractMap _map;

		[SerializeField]
		[Geocode]
		string _latitudeLongitude;




		bool _isInitialized;



		Vector3 _targetPosition;

		public Vector2d Location
		{
			get
			{
				var split = _latitudeLongitude.Split(',');
				return new Vector2d(double.Parse(split[0]), double.Parse(split[1]));
			}
		}


		void Start()
		{
			
			_map.OnInitialized += () => _isInitialized = true;


		}

	void Update()
	{
		if (_isInitialized){
			_targetPosition = Conversions.GeoToWorldPosition(Location, _map.CenterMercator, _map.WorldRelativeScale).ToVector3xz();

			Debug.Log (Location);
					Debug.Log ("Center Mercator: " + _map.CenterMercator);
					Debug.Log ("WorldRelativeScale: " + _map.WorldRelativeScale);

			Debug.Log (_targetPosition);


			transform.position = _targetPosition;
		}
	}


}



		
