﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class aboutScreenController : MonoBehaviour {

	private Scene scene;


	void Start () {
		scene = SceneManager.GetActiveScene();
		DataController.ChangeLastScene (scene.name);
	}

	public void OpenGTBIDSite(){
		Application.OpenURL("https://goldentriangledc.com/initiatives-environmental-sustainability/");
	
	}

	public void OpenDesignGreenSite(){
		Application.OpenURL("https://www.designgreenllc.com/duke-ellington-park");

	}

	public void OpenDOEESite(){
		Application.OpenURL("https://doee.dc.gov/service/watershed-protection-planning-and-restoration-branch");

	}

	public void OpenDCGocSite(){
		Application.OpenURL("https://dc.gov/");

	}

	public void LoadAboutGTBID(){
		SceneManager.LoadScene ("AboutGTBIDScene");
	}

	public void LoadAboutPartners(){
		SceneManager.LoadScene ("AboutPartnersScene");
	}
	public void LoadAboutThisApp(){
		SceneManager.LoadScene ("AboutThisAppScene");
	}
}
