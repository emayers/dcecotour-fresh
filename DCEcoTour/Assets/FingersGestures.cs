﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DigitalRubyShared;

public class FingersGestures : MonoBehaviour {
	[Tooltip("Minimum touch count to start panning. Rotating and scaling always requires two fingers. This should be 1 or 2.")]
	public int PanMinimumTouchCount = 1;
//	public GameObject FingersScriptPrefab;  deprecated
	public FingersScript FingersScript;

	private PanGestureRecognizer panGesture;
	private ScaleGestureRecognizer scaleGesture;
	//1/4/18: disabled Rotate Gestures
	//private RotateGestureRecognizer rotateGesture;



	private void PanGestureUpdated(GestureRecognizer r, ICollection<GestureTouch> touches)
	{
		if (r.State == GestureRecognizerState.Executing) {
			// was 10f, testing higher values to slow down panning
			Vector3 dir = new Vector3 (panGesture.DeltaX, 0, panGesture.DeltaY) / 20f;
			Vector3 rot = transform.eulerAngles;
			transform.position -= Quaternion.Euler(0, rot.y, 0) * dir;
		}
		
	}

	private void ScaleGestureUpdated(GestureRecognizer r, ICollection<GestureTouch> touches)
	{
		if (r.State == GestureRecognizerState.Executing) {
			Vector3 pos = transform.position;
			transform.position = new Vector3(pos.x, pos.y / scaleGesture.ScaleMultiplier, pos.z);
		}
	}

//	private void RotateGestureUpdated(GestureRecognizer r, ICollection<GestureTouch> touches)
//	{
//		if (r.State == GestureRecognizerState.Executing)
//			transform.Rotate(Vector3.up, rotateGesture.RotationDegreesDelta, Space.World);
//	}

	private void Start()
	{
//		FingersScript.CreateSingletonFromPrefabIfNeeded(FingersScriptPrefab); deprecated
		panGesture = new PanGestureRecognizer();
		panGesture.MinimumNumberOfTouchesToTrack = PanMinimumTouchCount;
		panGesture.Updated += PanGestureUpdated;
		scaleGesture = new ScaleGestureRecognizer();
		scaleGesture.Updated += ScaleGestureUpdated;
//		rotateGesture = new RotateGestureRecognizer();
//		rotateGesture.Updated += RotateGestureUpdated;

		panGesture.AllowSimultaneousExecution(scaleGesture);
//		panGesture.AllowSimultaneousExecution(rotateGesture);
//		scaleGesture.AllowSimultaneousExecution(rotateGesture);
		FingersScript.Instance.AddGesture(panGesture);
		FingersScript.Instance.AddGesture(scaleGesture);
//		FingersScript.Instance.AddGesture(rotateGesture);
	}
}