﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using GoShared;
//using GoMap;
using DigitalRubyShared;
//using Mapbox.Unity.Map;
//using Mapbox.Unity.Location;
//using Mapbox.Unity.Utilities;
//using Mapbox.Utils;

public class POIController : MonoBehaviour {

	public int siteID;
	public string siteLatitudeLongitude;
	public double lat;
	public double lng;
	private DataController dataController;
	private SiteData siteData;
	private BMPData[] siteBMPs;
	private SiteData[] sitePool;
	private int siteBMPID;
	private LongPressGestureRecognizer longPressGesture;
	private Transform popuptext;
	private Transform BMPIcon0;
	private Transform BMPIcon1;
	private Transform BMPIcon2;

	//variables for popup window
//	public Transform popuptext;
	public static string textstatus="off";

	public GameObject POIPrefab;

//	public GameObject FingersScriptPrefab; deprecated
	public FingersScript FingersScript;

	private TapGestureRecognizer tapGesture;
	private TapGestureRecognizer doubleTapGesture;

	//public bool triggered = false;

//	[SerializeField]
//	private AbstractMap _map;

	/// <summary>
	/// The rate at which the transform's position tries catch up to the provided location.
	/// </summary>
	//[SerializeField]
	//float _positionFollowFactor;

	/// <summary>
	/// Use a mock <see cref="T:Mapbox.Unity.Location.TransformLocationProvider"/>,
	/// rather than a <see cref="T:Mapbox.Unity.Location.EditorLocationProvider"/>. 
	/// </summary>
	//[SerializeField]
	//bool _useTransformLocationProvider;

//	bool _isInitialized;

/*	ILocationProvider _locationProvider;
	public ILocationProvider LocationProvider {
		private get { 
			if (_locationProvider == null) {
				_locationProvider = _useTransformLocationProvider ?
					LocationProviderFactory.Instance.TransformLocationProvider : LocationProviderFactory.Instance.DefaultLocationProvider;
			}
			return _locationProvider;
		}
		set { 
			if (_locationProvider != null) {
				_locationProvider.OnLocationUpdated -= LocationProvider_OnLocationUpdated;

			}
			_locationProvider = value;
			_locationProvider.OnLocationUpdated += LocationProvider_OnLocationUpdated;
		}
	}

	public Vector2d Location
	{
		get
		{
			var split = siteLatitudeLongitude.Split(',');
			return new Vector2d(double.Parse(split[0]), double.Parse(split[1]));

		}
	}
*/


	Vector3 _targetPosition;


	private GestureTouch FirstTouch(ICollection<GestureTouch> touches)
	{
		foreach (GestureTouch t in touches)
		{
			return t;
		}
		return new GestureTouch();
	}

	private void DebugText(string text, params object[] format)
	{
		//bottomLabel.text = string.Format(text, format);
		Debug.Log(string.Format(text, format));
	}




	private void TapGestureCallback(GestureRecognizer gesture, ICollection<GestureTouch> touches)
	{
		if (gesture.State == GestureRecognizerState.Ended)
		{
			GestureTouch t = FirstTouch(touches);
			DebugText("Tapped at {0}, {1}", t.X, t.Y);

		}
	}

	private void CreateTapGesture()
	{
		tapGesture = new TapGestureRecognizer();
		tapGesture.Updated += TapGestureCallback;
		tapGesture.RequireGestureRecognizerToFail = doubleTapGesture;
		FingersScript.Instance.AddGesture(tapGesture);
	}

	public void DoubleTapGestureCallback(GestureRecognizer gesture, ICollection<GestureTouch> touches)
	{
		if (gesture.State == GestureRecognizerState.Ended)
		{
			GestureTouch t = FirstTouch(touches);
			DebugText("Double tapped at {0}, {1}", t.X, t.Y);
			SiteButtonClicked (siteID);


		}
	}

	public void CreateDoubleTapGesture()
	{
		doubleTapGesture = new TapGestureRecognizer();
		doubleTapGesture.NumberOfTapsRequired = 2;
		doubleTapGesture.Updated += DoubleTapGestureCallback;
		FingersScript.Instance.AddGesture(doubleTapGesture);

	}

	private static bool? CaptureGestureHandler(GameObject obj)
	{
		// I've named objects PassThrough* if the gesture should pass through and NoPass* if the gesture should be gobbled up, everything else gets default behavior
		if (obj.name.StartsWith("PassThrough"))
		{
			// allow the pass through for any element named "PassThrough*"
			return false;
		}
		else if (obj.name.StartsWith("NoPass"))
		{
			// prevent the gesture from passing through
			return true;
		}

		// fall-back to default behavior for anything else
		return null;
	}

	void Awake()
	{
		//Coordinates coordinates = new Coordinates (lat, lng, 0 );
		//gameobject.transform.localPosition = coordinates.convertCoordinateToVector( 0 );

		// translates lat/long to gamespace
		//LocationProvider.OnLocationUpdated += LocationProvider_OnLocationUpdated;
		//_map = GameObject.FindObjectOfType<AbstractMap> (); 

		//_map.OnInitialized += () => _isInitialized = true;

	}


	// Use this for initialization
	void Start () {
		Debug.Log ("started");
		dataController = FindObjectOfType<DataController>();	
		sitePool = dataController.allSiteData;
		siteBMPs = siteData.bmps;


		// we have set a prefab instance on this script - if we don't have a singleton of FingersScript yet,
		// create an instance of the prefab and set it as the singleton. This allows the fingers script
		// to live forever and eliminates the need to add the fingers script to every scene in your game.
		// you should only have this logic in the first scene of your game, and it must be in the Start method.
//		FingersScript.CreateSingletonFromPrefabIfNeeded(FingersScriptPrefab); deprecated

		// prevent the one special no-pass button from passing through,
		//  even though the parent scroll view allows pass through (see FingerScript.PassThroughObjects)
		FingersScript.Instance.CaptureGestureHandler = CaptureGestureHandler;



		// set up site text label
		popuptext = transform.Find ("popuptext");
		popuptext.GetComponent<TextMesh> ().text = siteData.siteName;


		//set up BMP icons.  Turn on the BMP icons corresponding to the ones that have BMPData entries in SiteData. 
		//NOTE: kind of a cheat, b/c it just activates based on size of the array. Would be better to search array for specific bmps listed.

		BMPIcon0 = popuptext.transform.Find("BMPIcon0");
		BMPIcon1 = popuptext.transform.Find ("BMPIcon1");
		BMPIcon2 = popuptext.transform.Find ("BMPIcon2");

		if (siteBMPs.Length == 0) {
			BMPIcon0.gameObject.SetActive (false);
			BMPIcon1.gameObject.SetActive (false);
			BMPIcon2.gameObject.SetActive (false);
		} else if (siteBMPs.Length == 1) {
			BMPIcon0.gameObject.SetActive (true);
			BMPIcon1.gameObject.SetActive (false);
			BMPIcon2.gameObject.SetActive (false);
		} else if (siteBMPs.Length == 2) {
			BMPIcon0.gameObject.SetActive (true);
			BMPIcon1.gameObject.SetActive (true);
			BMPIcon2.gameObject.SetActive (false);
		} else if (siteBMPs.Length == 3) {
			BMPIcon0.gameObject.SetActive (true);
			BMPIcon1.gameObject.SetActive (true);
			BMPIcon2.gameObject.SetActive (true);
		}


		

			


		//Instantiate (popuptext, new Vector3 (transform.position.x, transform.position.y, transform.position.z + 10), popuptext.rotation);



		// this is the code to get the text labels. Turn back on if you change your mind!
//		GetComponent<TextMesh>().text = siteData.siteName;
	
			
//		if (FingersScript.Instance == null)
//		{
//			Debug.LogError("Fingers script prefab needs to be added to the first scene");
//			return;
//		}
//
//		//this.Camera = (this.Camera == null ? Camera.main : this.Camera);
//		longPressGesture = new LongPressGestureRecognizer();
//		longPressGesture.Updated += LongPressGestureUpdated;
////		rigidBody = GetComponent<Rigidbody2D>();
////		spriteRenderer = GetComponent<SpriteRenderer>();
////		if (spriteRenderer != null)
////		{
////			startSortOrder = spriteRenderer.sortingOrder;
////		}
//		FingersScript.Instance.AddGesture(longPressGesture);
	}

	private void LateUpdate()
	{


		int touchCount = Input.touchCount;
		if (FingersScript.Instance.TreatMousePointerAsFinger && Input.mousePresent)
		{
			touchCount += (Input.GetMouseButton(0) ? 1 : 0);
			touchCount += (Input.GetMouseButton(1) ? 1 : 0);
			touchCount += (Input.GetMouseButton(2) ? 1 : 0);
		}
		string touchIds = string.Empty;
		foreach (Touch t in Input.touches)
		{
			touchIds += ":" + t.fingerId + ":";
		}


	}

	// code to make text labels pop up on mouse over. Abandoned in favor of making labels persist.
//	void OnMouseEnter(){
//		if (textstatus == "off") {
//			popuptext.GetComponent<TextMesh> ().text = siteData.siteName;
//			textstatus = "on";
//			Instantiate (popuptext, new Vector3 (transform.position.x, transform.position.y, transform.position.z + 10), popuptext.rotation);
//		
//		}
//
//	}


//	void OnMouseExit(){
//		if (textstatus == "on") {
//			textstatus = "off";
//		}
//	
//	}

//	WORKING single click to load site page
	void OnMouseDown(){
		Debug.Log ("POI Clicked");
		SiteButtonClicked (siteID);
	
	}

	void LoadSiteButtonClicked (){
		SiteButtonClicked (siteID);
	}
////
//	private void LongPressGestureUpdated(GestureRecognizer r, ICollection<GestureTouch> touches)
//	{
//
//		SiteButtonClicked (siteID);
//	}


//	void OnTriggerEnter(Collider collider) {
//		
//		Debug.Log ("OnTriggerEnter" + dataController.triggered);
//
//		while (dataController.triggered == false) {
//			Handheld.Vibrate ();
//			Debug.Log ("Vibrate!");
//			//SiteButtonClicked (siteID);
//			dataController.triggered = true;
//			Debug.Log ("OnTriggerEnter" + dataController.triggered);
//		}
//
//	}

	//void OnTriggerExit(Collider collider) {
	//	dataController.triggered = false;
	//}


	public void SiteButtonClicked(int siteID)
	{
		
		Debug.Log ("Clicked siteid: " + siteID);
		switch (siteID) {
		case 0:
			SceneManager.LoadScene ("DukeEllingtonParkScene");
			break;

		case 1:
			SceneManager.LoadScene ("19thandLScene");
			break;

		case 2:
			SceneManager.LoadScene ("M&RI&CTScene");
			break;

		default:
			SceneManager.LoadScene ("Park_1");
			break;
		}
		//dataController.selectedSiteID = siteID;
	}



	public void Setup(SiteData data)
	{
		siteData = data;
		lat = siteData.siteLatitude;
		lng = siteData.siteLongitude;

		siteID = siteData.siteID;
		//siteImage.sprite = siteData.siteSprite;

		// don't reorder the creation of these :)

//		CreateDoubleTapGesture();
//		CreateTapGesture();
	}



	/*
		void OnDestroy()
		{
			if (LocationProvider != null) {
				LocationProvider.OnLocationUpdated -= LocationProvider_OnLocationUpdated;
			}
		}

		void LocationProvider_OnLocationUpdated(object sender, LocationUpdatedEventArgs e)
		{
			if (_isInitialized) {
				_targetPosition = Conversions.GeoToWorldPosition (Location,
					_map.CenterMercator, _map.WorldRelativeScale).ToVector3xz ();
			_targetPosition.y = _targetPosition.y + 15;
			}

			//	transform.position = Vector3.Lerp (transform.position, _targetPosition, Time.deltaTime * _positionFollowFactor);
			//Debug.Log ("refPoint = " + _map.CenterMercator);
			//Debug.Log ("scale = " + _map.WorldRelativeScale);
			//Debug.Log (_targetPosition);
			//Debug.Log (Location);
		}

		void Update() //moves to new position
		{
			transform.position = Vector3.Lerp(transform.position, _targetPosition, Time.deltaTime * _positionFollowFactor);

		}
	*/
}