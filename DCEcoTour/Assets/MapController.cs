﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour {

	private Scene scene;

	// Use this for initialization
	void Start () {
		scene = SceneManager.GetActiveScene();
		DataController.ChangeLastScene (scene.name);
		Debug.Log ("Scene: " + scene.name);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
