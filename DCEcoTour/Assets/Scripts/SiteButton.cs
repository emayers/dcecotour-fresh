﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SiteButton : MonoBehaviour {

	public Text siteNameText;
	public int siteID;
	public Image siteImage;

	private SiteData siteData;
	private BrowseSitesController browseSitesController;

	// Use this for initialization
	void Start () {

		browseSitesController = FindObjectOfType<BrowseSitesController>();
		
	}

	public void Setup(SiteData data)
	{
		siteData = data;
		siteNameText.text = siteData.siteName;
		siteID = siteData.siteID;
		//siteImage.sprite = siteData.siteSprite;
	}

	public void HandleClick()
	{
		browseSitesController.SiteButtonClicked (siteData.siteID);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
