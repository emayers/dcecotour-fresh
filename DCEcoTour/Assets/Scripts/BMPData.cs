﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class BMPData {

	public int bmpID;
	public string bmpName;
}
