﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //allows data controller to load scenes

public class DataController : MonoBehaviour {

	public SiteData[] allSiteData;
	public int selectedSiteID;
	static string lastScene;
	static string currentScene;
	public bool triggered = false;



	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject); //scene will persist when we load new scenes
		lastScene = "MenuScreen";
		SceneManager.LoadScene ("MenuScreen"); //loads starting screen -- change this later
	}

	public static void ChangeLastScene(string sceneName) {
		lastScene = currentScene;
		currentScene = sceneName;
		Debug.Log ("lastScene = " + lastScene);
		Debug.Log ("currentScene = " + currentScene);
	}

	public static void LoadLastScene() {
		string last = lastScene;
		//lastScene = currentScene;
		//currentScene = last;

			SceneManager.LoadScene (last);

		Debug.Log ("Loading: " + last);
	}
		/* public SiteData GetCurrentSiteData()
		{
			return allSiteData[0]; //initializes array?

		}
		*/
	
	
	// Update is called once per frame
	void Update () {
		
	}
}
