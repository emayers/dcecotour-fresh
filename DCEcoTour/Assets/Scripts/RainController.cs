﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainController : MonoBehaviour {

	public void PlayAnimation() {
		GetComponent<Animator>().SetBool("StartRain",true);
		Debug.Log ("on");
	}

	public void ResetButton(){
		GetComponent<Animator> ().SetBool ("StartRain", false);
		Debug.Log ("off");
	}
}
