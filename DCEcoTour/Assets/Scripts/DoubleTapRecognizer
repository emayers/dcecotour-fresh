﻿//
// Fingers Gestures
// (c) 2015 Digital Ruby, LLC
// Source code may be used for personal or commercial projects.
// Source code may NOT be redistributed or sold.
// 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DigitalRubyShared
{
    public class DoubleTap : MonoBehaviour
    {
        public GameObject FingersScriptPrefab;
       
        private TapGestureRecognizer tapGesture;
        private TapGestureRecognizer doubleTapGesture;
        

        
        private GestureTouch FirstTouch(ICollection<GestureTouch> touches)
        {
            foreach (GestureTouch t in touches)
            {
                return t;
            }
            return new GestureTouch();
        }

        private void DebugText(string text, params object[] format)
        {
            //bottomLabel.text = string.Format(text, format);
            Debug.Log(string.Format(text, format));
        }

        
        

        private void TapGestureCallback(GestureRecognizer gesture, ICollection<GestureTouch> touches)
        {
            if (gesture.State == GestureRecognizerState.Ended)
            {
                GestureTouch t = FirstTouch(touches);
                DebugText("Tapped at {0}, {1}", t.X, t.Y);
                
            }
        }

        private void CreateTapGesture()
        {
            tapGesture = new TapGestureRecognizer();
            tapGesture.Updated += TapGestureCallback;
            tapGesture.RequireGestureRecognizerToFail = doubleTapGesture;
            FingersScript.Instance.AddGesture(tapGesture);
        }

        private void DoubleTapGestureCallback(GestureRecognizer gesture, ICollection<GestureTouch> touches)
        {
            if (gesture.State == GestureRecognizerState.Ended)
            {
                GestureTouch t = FirstTouch(touches);
                DebugText("Double tapped at {0}, {1}", t.X, t.Y);
                RemoveAsteroids(t.X, t.Y, 16.0f);
            }
        }

        private void CreateDoubleTapGesture()
        {
            doubleTapGesture = new TapGestureRecognizer();
            doubleTapGesture.NumberOfTapsRequired = 2;
            doubleTapGesture.Updated += DoubleTapGestureCallback;
            FingersScript.Instance.AddGesture(doubleTapGesture);
        }

      

        private void Start()
        {
            // we have set a prefab instance on this script - if we don't have a singleton of FingersScript yet,
            // create an instance of the prefab and set it as the singleton. This allows the fingers script
            // to live forever and eliminates the need to add the fingers script to every scene in your game.
            // you should only have this logic in the first scene of your game, and it must be in the Start method.
            FingersScript.CreateSingletonFromPrefabIfNeeded(FingersScriptPrefab);

            

            // don't reorder the creation of these :)
            
            CreateDoubleTapGesture();
            CreateTapGesture();
            

            

            // prevent the one special no-pass button from passing through,
            //  even though the parent scroll view allows pass through (see FingerScript.PassThroughObjects)
            FingersScript.Instance.CaptureGestureHandler = CaptureGestureHandler;

            // show touches, only do this for debugging as it can interfere with other canvases
            FingersScript.Instance.ShowTouches = true;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ReloadDemoScene();
            }
        }

        private void LateUpdate()
        {
            

            int touchCount = Input.touchCount;
            if (FingersScript.Instance.TreatMousePointerAsFinger && Input.mousePresent)
            {
                touchCount += (Input.GetMouseButton(0) ? 1 : 0);
                touchCount += (Input.GetMouseButton(1) ? 1 : 0);
                touchCount += (Input.GetMouseButton(2) ? 1 : 0);
            }
            string touchIds = string.Empty;
            foreach (Touch t in Input.touches)
            {
                touchIds += ":" + t.fingerId + ":";
            }

            
        }

        

        public void ReloadDemoScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }
}
