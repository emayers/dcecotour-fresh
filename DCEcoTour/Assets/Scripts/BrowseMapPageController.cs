﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using GoMap;
//using GoShared;
using DigitalRubyShared;
using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine;

public class BrowseMapPageController : MonoBehaviour {

	[SerializeField]
	private AbstractMap _map;


	private Scene scene;

	public SimpleObjectPool POIObjectPool;

	//public Transform siteButtonParent;

	private DataController dataController;
	private SiteData[] sitePool;

	private int siteIndex;
	private int siteID;
	private double lat;
	private double lng;

	Vector3 _targetPosition;

	public bool _isInitialized;






	private List<GameObject> POIGameObjects = new List<GameObject> ();



	// Use this for initialization
	void Start () {
		//GameObject fingersScript = GameObject.Find ("FingersScript");
//		GetComponent<FingersScript> ().enabled = false;
		//	fingers.SetActive = false;
		//FingersScript.enabled = false;
//		DontDestroyOnLoad (gameObject); //scene will persist when we load new scenes
//		scene = SceneManager.GetActiveScene();
//		DataController.ChangeLastScene (scene.name);
//		Debug.Log ("Scene: " + scene.name);

		dataController = FindObjectOfType<DataController> ();
		sitePool = dataController.allSiteData;

		_map.OnInitialized += () => _isInitialized = true;

		StartCoroutine ("ShowSites");

	}
	
	// Update is called once per frame
	void Update () {
		
		
	}



public void LoadParkPage()
{
	SceneManager.LoadScene ("BrowseScreen");
}

	public void LoadSlippy()
	{
		SceneManager.LoadScene ("Slippy");
	}
	// loads Browse Map (with location disabled)
	public void LoadBrowseMap(){
		SceneManager.LoadScene ("BrowseMapScene");
	}

	// loads Browse Map (with location disabled)
	public void LoadMap(){
		SceneManager.LoadScene ("MapboxMapScene");
	}

	// changed to coroutine, so that showsites waits for map to finish loading before determining placement of site markers.
	IEnumerator ShowSites()
	{
		for (int i = 0; i < sitePool.Length; i++) {
			
			GameObject POIGameObject = POIObjectPool.GetObject ();
			POIGameObjects.Add (POIGameObject);
			//POIGameObject.transform.SetParent (POIParent);

			POIController pointOfInterest = POIGameObject.GetComponent<POIController> ();
			pointOfInterest.Setup (sitePool[i]);

			lat = sitePool[i].siteLatitude;
			lng = sitePool[i].siteLongitude;
//			Coordinates coordinates = new Coordinates (lat, lng, 0 );
//			POIGameObject.transform.localPosition = coordinates.convertCoordinateToVector( 0 );

//			yield return new WaitUntil(()=> _isInitialized == true);
			// changed to WaitForSeconds since Browse map will never show initialized

//			yield return new WaitForSeconds (1);
			yield return null;

				_targetPosition = Conversions.GeoToWorldPosition (lat, lng, _map.CenterMercator, _map.WorldRelativeScale).ToVector3xz ();
			_targetPosition.y = 2;
				POIGameObject.transform.localPosition = _targetPosition;

			pointOfInterest.CreateDoubleTapGesture ();




		}
	}



}