﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BrowseSitesController : MonoBehaviour {


	public SimpleObjectPool siteButtonObjectPool;
	public Transform siteButtonParent;
	//public GameObject siteButtonDisplay;

	private DataController dataController;
	//private SiteData currentSiteData;
	private SiteData[] sitePool;

	private int siteIndex;
	private int siteID;
	private Scene scene;


	private List<GameObject> siteButtonGameObjects = new List<GameObject> ();

	// Use this for initialization
	void Start () {
		dataController = FindObjectOfType<DataController> ();
		sitePool = dataController.allSiteData;

		ShowSites ();

		scene = SceneManager.GetActiveScene();
		DataController.ChangeLastScene (scene.name);
		Debug.Log ("Scene: " + scene.name);

	}

	private void ShowSites()
	{
		for (int i = 0; i < sitePool.Length; i++) {
			GameObject siteButtonGameObject = siteButtonObjectPool.GetObject ();
			siteButtonGameObjects.Add (siteButtonGameObject);
			siteButtonGameObject.transform.SetParent (siteButtonParent);

			SiteButton siteButton = siteButtonGameObject.GetComponent<SiteButton> ();
			siteButton.Setup (sitePool[i]);
		}
	}

	public void LoadMap(){
		SceneManager.LoadScene ("MapboxMapScene");
	
	}

	public void SiteButtonClicked(int siteID)
	{
		switch (siteID) {
		case 0:
			SceneManager.LoadScene ("DukeEllingtonParkScene");
			break;
		
		case 1:
			SceneManager.LoadScene ("19th&LScene");
			break;

		case 2:
			SceneManager.LoadScene ("M&RI&CTScene");
			break;

		default:
			SceneManager.LoadScene ("Park_1");
			break;
		}
		dataController.selectedSiteID = siteID;
	}



	// Update is called once per frame
	void Update () {
		
	}
}



