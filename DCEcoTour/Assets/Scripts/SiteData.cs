﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable] //allows editing in Unity Editor
public class SiteData 
{
	public int siteID;
	public string siteName;
	public string siteAddressLine1;
	public string siteAddressLine2;
	public double siteLatitude;
	public double siteLongitude;
	public Sprite siteSprite;

	public string siteDescription;
	public BMPData[] bmps;



}