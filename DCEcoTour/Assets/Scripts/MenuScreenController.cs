﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScreenController : MonoBehaviour {

	private Scene scene;


	void Start () {
		scene = SceneManager.GetActiveScene();
		DataController.ChangeLastScene (scene.name);
		//Debug.Log ("Scene: " + scene.name);
	}

	public void LoadBrowse()
	{
		SceneManager.LoadScene ("BrowseMapScene");
	}

	public void LoadMap()
	{
		SceneManager.LoadScene ("BrowseMapScene");
	}

	public void LoadHowGIWorks()
	{
		SceneManager.LoadScene ("GreenInfrastructureScene");
	}

	public void LoadAbout()
	{
		SceneManager.LoadScene ("AboutScene");
	}

	public void LoadTour()
	{
		SceneManager.LoadScene ("TourScene");
	}


}
