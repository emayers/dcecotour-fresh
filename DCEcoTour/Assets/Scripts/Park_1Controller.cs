﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Park_1Controller : MonoBehaviour {

	public int selectedSiteID;
//	public Text siteNameText;

	public Text siteAddressLine1Text;
	public Text siteAddresssLine2Text;
	public Text siteDescriptionText;
	public Image siteImage;
	public double siteLatitude;
	public double siteLongitude;

	private DataController dataController;
	private SiteData siteData;
	private SiteData[] sitePool;
	private Park_1Controller park_1Controller;
	private Scene scene;
	private string googleMapsURL;

	// Use this for initialization
	void Start () {

		//update current and last scenes 
//		scene = SceneManager.GetActiveScene();
//		DataController.ChangeLastScene (scene.name);
//		Debug.Log ("Scene: " + scene.name);

		//park_1Controller = FindObjectOfType<Park_1Controller> ();
		dataController = FindObjectOfType<DataController> ();
		sitePool = dataController.allSiteData;
//		selectedSiteID = dataController.selectedSiteID;
		Setup (sitePool[selectedSiteID]); //NOTE: this works, but it's a cheat.  It requires siteID to be the same as the site index. Would be better to make it look up the site by the ID field once I know how to do that.
	}





	public void Setup(SiteData data)
	{
		siteData = data;
//		siteNameText.text = siteData.siteName;
		siteAddressLine1Text.text = siteData.siteAddressLine1 + ", " + siteData.siteAddressLine2;
		siteAddresssLine2Text.text = siteData.siteAddressLine2;
		siteDescriptionText.text = siteData.siteDescription;
		siteImage.sprite = siteData.siteSprite;
		siteImage.preserveAspect = true;
		siteLatitude = siteData.siteLatitude;
		siteLongitude = siteData.siteLongitude;
		googleMapsURL = "http://www.google.com/maps/place/" + siteLatitude + "," + siteLongitude;


	}

	// Update is called once per frame
	void Update () {
		
	}

	public void OpenMap(){
		//SceneManager.LoadScene ("POI Demo");
		Application.OpenURL(googleMapsURL);
		Debug.Log (googleMapsURL);
	}

	public void OpenBioretention(){
		SceneManager.LoadScene ("BioretentionScene");
			}
	public void OpenPermPave(){
		SceneManager.LoadScene ("PermPavScreen2");
	}
	public void OpenRWH(){
		SceneManager.LoadScene ("RWHScreen");
	}

	public void OpenGreenSolutions(){
		SceneManager.LoadScene ("GreenSolutionScene");
	}

	public void OpenGreenToolsScene(){
		SceneManager.LoadScene ("GreenToolsScene");
	}


}




//	public Text siteNameText;
//	public int siteID;
//
//
//	private SiteData siteData;
//	private BrowseSitesController browseSitesController;
//
//	// Use this for initialization
//	void Start () {
//
//		browseSitesController = FindObjectOfType<BrowseSitesController>();
//
//	}
//
//	public void Setup(SiteData data)
//	{
//		siteData = data;
//		siteNameText.text = siteData.siteName;
//		siteID = siteData.siteID;
//	}
//
//	public void HandleClick()
//	{
//		browseSitesController.SiteButtonClicked (siteData.siteID);
//	}
//
