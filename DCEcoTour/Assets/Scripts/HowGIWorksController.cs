﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HowGIWorksController : MonoBehaviour {

	private Scene scene;

	// Use this for initialization
	void Start () {
		scene = SceneManager.GetActiveScene();
		DataController.ChangeLastScene (scene.name);
		Debug.Log ("Scene: " + scene.name);
	}

	public void LoadUrbanChallenge()
	{
		SceneManager.LoadScene("UrbanChallengeScene");
	}

	public void LoadGreenSolution()
	{
		SceneManager.LoadScene("GreenSolutionScene");
	}

	public void LoadGreenTools()
	{
		SceneManager.LoadScene("GreenToolsScene");
	}

	public void LoadBioretention()
	{
		SceneManager.LoadScene("BioretentionScene");
	}

	public void LoadPermeablePavement()
	{
		SceneManager.LoadScene("PermPavScreen2");
	}

	public void LoadRWH()
	{
		SceneManager.LoadScene("RWHScreen");
	}
}
