﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class NavBarController : MonoBehaviour {

	private Scene scene;

	//private DataController dataController;



	// Use this for initialization
	void Start () {
		scene = SceneManager.GetActiveScene();
		//DataController.ChangeLastScene (scene.name);
	}

	public void LoadMenu()
	{
		SceneManager.LoadScene ("MenuScreen");


		//Debug.Log (scene.name);
	}

	public void LoadBack()
	{
		//dataController = FindObjectOfType<DataController> ();
		DataController.LoadLastScene ();

	}

	// new code to load specific pages when back button is pressed, depending on what page you're on.  

	public void LoadMap (){
		SceneManager.LoadScene ("BrowseMapScene");
	}

	public void LoadGreenInfrastructure (){
		SceneManager.LoadScene ("GreenInfrastructureScene");
	}

	public void LoadAbout (){
		SceneManager.LoadScene ("AboutScene");
	}


	// Update is called once per frame
	void Update () {
		
	}
}
