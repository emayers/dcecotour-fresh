Mini Compass Pack ReadMe

Step 1
Add the compass prefab of your choice to the scene.

Step 2
Under the ‘script’ component of the compass that you want, assign a PlayerController to the ‘player’ field.

That Simple!

Notes 

Thanks for downloading my asset pack, drop me some feedback on the asset store, or if you have any questions tweet me @landanlloyd on twitter.