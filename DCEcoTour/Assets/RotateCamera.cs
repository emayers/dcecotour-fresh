﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRubyShared;

public class RotateCamera : MonoBehaviour {

	private RotateGestureRecognizer rotateGesture;
	public Transform target;
	public float cameraRotationSpeed = 1.0f;

	// Use this for initialization
	void Start () {
		SetupGesture ();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SetupGesture(){
		rotateGesture = new RotateGestureRecognizer ();
		rotateGesture.Updated += RotateGestureCallback;
		FingersScript.Instance.AddGesture (rotateGesture);
	
	}

	void RotateGestureCallback(GestureRecognizer gesture, ICollection<GestureTouch> touches){
		if(gesture.State == GestureRecognizerState.Executing){
			transform.RotateAround (target.position, Vector3.up, rotateGesture.RotationDegreesDelta * cameraRotationSpeed);

		}
	
	}
}
