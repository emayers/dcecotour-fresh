﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1809665003;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t2186285234;
// System.Xml.XmlParserInput
struct XmlParserInput_t2182411204;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t1729680289;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t3368185270;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.IO.TextReader
struct TextReader_t283511965;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;
// System.Xml.XmlNameEntry
struct XmlNameEntry_t1073099671;
// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_t139615908;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlNodeListChildren
struct XmlNodeListChildren_t1082692789;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Xml.IHasXmlChildNode
struct IHasXmlChildNode_t2708887342;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1437094927;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Xml.XmlParserInput/XmlParserInputSource
struct XmlParserInputSource_t3533005609;
// System.Xml.XmlNamespaceManager/NsDecl[]
struct NsDeclU5BU5D_t2116608150;
// System.Xml.XmlNamespaceManager/NsScope[]
struct NsScopeU5BU5D_t382374428;
// Mono.Xml.DTDAutomataFactory
struct DTDAutomataFactory_t2958275022;
// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t222313714;
// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_t2220366188;
// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_t2844734410;
// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t2250844513;
// Mono.Xml.DTDNotationDeclarationCollection
struct DTDNotationDeclarationCollection_t959292105;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1624492310;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Xml.XmlNodeReaderImpl
struct XmlNodeReaderImpl_t2501602067;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Xml.XmlImplementation
struct XmlImplementation_t254178875;
// System.Xml.XmlNameEntryCache
struct XmlNameEntryCache_t2890546907;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t934654762;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t1533444722;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.Stream
struct Stream_t1273022909;
// System.Xml.XmlException
struct XmlException_t1761730631;
// System.Text.Decoder
struct Decoder_t2204182725;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t100236324;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t322714710;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t3264596611;
// Mono.Xml.DTDNode
struct DTDNode_t858560093;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t491982174;
// System.Xml.XmlElement
struct XmlElement_t561603118;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct List_1_t218596005;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t2821286253;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t2316283784;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Xml.XmlInputStream
struct XmlInputStream_t1691369434;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// Mono.Xml.DictionaryBase
struct DictionaryBase_t52754249;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t3316004401;
// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t2798820000;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XMLQUALIFIEDNAME_T2760654312_H
#define XMLQUALIFIEDNAME_T2760654312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlQualifiedName
struct  XmlQualifiedName_t2760654312  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlQualifiedName::name
	String_t* ___name_1;
	// System.String System.Xml.XmlQualifiedName::ns
	String_t* ___ns_2;
	// System.Int32 System.Xml.XmlQualifiedName::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t2760654312, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t2760654312, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t2760654312, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

struct XmlQualifiedName_t2760654312_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Xml.XmlQualifiedName::Empty
	XmlQualifiedName_t2760654312 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t2760654312_StaticFields, ___Empty_0)); }
	inline XmlQualifiedName_t2760654312 * get_Empty_0() const { return ___Empty_0; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(XmlQualifiedName_t2760654312 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLQUALIFIEDNAME_T2760654312_H
#ifndef ENTRY_T3052280359_H
#define ENTRY_T3052280359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable/Entry
struct  Entry_t3052280359  : public RuntimeObject
{
public:
	// System.String System.Xml.NameTable/Entry::str
	String_t* ___str_0;
	// System.Int32 System.Xml.NameTable/Entry::hash
	int32_t ___hash_1;
	// System.Int32 System.Xml.NameTable/Entry::len
	int32_t ___len_2;
	// System.Xml.NameTable/Entry System.Xml.NameTable/Entry::next
	Entry_t3052280359 * ___next_3;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___hash_1)); }
	inline int32_t get_hash_1() const { return ___hash_1; }
	inline int32_t* get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(int32_t value)
	{
		___hash_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_next_3() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___next_3)); }
	inline Entry_t3052280359 * get_next_3() const { return ___next_3; }
	inline Entry_t3052280359 ** get_address_of_next_3() { return &___next_3; }
	inline void set_next_3(Entry_t3052280359 * value)
	{
		___next_3 = value;
		Il2CppCodeGenWriteBarrier((&___next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3052280359_H
#ifndef XMLREADER_T3121518892_H
#define XMLREADER_T3121518892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3121518892  : public RuntimeObject
{
public:
	// System.Text.StringBuilder System.Xml.XmlReader::readStringBuffer
	StringBuilder_t * ___readStringBuffer_0;
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1809665003 * ___binary_1;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t2186285234 * ___settings_2;

public:
	inline static int32_t get_offset_of_readStringBuffer_0() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___readStringBuffer_0)); }
	inline StringBuilder_t * get_readStringBuffer_0() const { return ___readStringBuffer_0; }
	inline StringBuilder_t ** get_address_of_readStringBuffer_0() { return &___readStringBuffer_0; }
	inline void set_readStringBuffer_0(StringBuilder_t * value)
	{
		___readStringBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___readStringBuffer_0), value);
	}

	inline static int32_t get_offset_of_binary_1() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___binary_1)); }
	inline XmlReaderBinarySupport_t1809665003 * get_binary_1() const { return ___binary_1; }
	inline XmlReaderBinarySupport_t1809665003 ** get_address_of_binary_1() { return &___binary_1; }
	inline void set_binary_1(XmlReaderBinarySupport_t1809665003 * value)
	{
		___binary_1 = value;
		Il2CppCodeGenWriteBarrier((&___binary_1), value);
	}

	inline static int32_t get_offset_of_settings_2() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___settings_2)); }
	inline XmlReaderSettings_t2186285234 * get_settings_2() const { return ___settings_2; }
	inline XmlReaderSettings_t2186285234 ** get_address_of_settings_2() { return &___settings_2; }
	inline void set_settings_2(XmlReaderSettings_t2186285234 * value)
	{
		___settings_2 = value;
		Il2CppCodeGenWriteBarrier((&___settings_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3121518892_H
#ifndef DTDREADER_T386081180_H
#define DTDREADER_T386081180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DTDReader
struct  DTDReader_t386081180  : public RuntimeObject
{
public:
	// System.Xml.XmlParserInput System.Xml.DTDReader::currentInput
	XmlParserInput_t2182411204 * ___currentInput_0;
	// System.Collections.Stack System.Xml.DTDReader::parserInputStack
	Stack_t2329662280 * ___parserInputStack_1;
	// System.Char[] System.Xml.DTDReader::nameBuffer
	CharU5BU5D_t3528271667* ___nameBuffer_2;
	// System.Int32 System.Xml.DTDReader::nameLength
	int32_t ___nameLength_3;
	// System.Int32 System.Xml.DTDReader::nameCapacity
	int32_t ___nameCapacity_4;
	// System.Text.StringBuilder System.Xml.DTDReader::valueBuffer
	StringBuilder_t * ___valueBuffer_5;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_6;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_7;
	// System.Int32 System.Xml.DTDReader::dtdIncludeSect
	int32_t ___dtdIncludeSect_8;
	// System.Boolean System.Xml.DTDReader::normalization
	bool ___normalization_9;
	// System.Boolean System.Xml.DTDReader::processingInternalSubset
	bool ___processingInternalSubset_10;
	// System.String System.Xml.DTDReader::cachedPublicId
	String_t* ___cachedPublicId_11;
	// System.String System.Xml.DTDReader::cachedSystemId
	String_t* ___cachedSystemId_12;
	// Mono.Xml.DTDObjectModel System.Xml.DTDReader::DTD
	DTDObjectModel_t1729680289 * ___DTD_13;

public:
	inline static int32_t get_offset_of_currentInput_0() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___currentInput_0)); }
	inline XmlParserInput_t2182411204 * get_currentInput_0() const { return ___currentInput_0; }
	inline XmlParserInput_t2182411204 ** get_address_of_currentInput_0() { return &___currentInput_0; }
	inline void set_currentInput_0(XmlParserInput_t2182411204 * value)
	{
		___currentInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentInput_0), value);
	}

	inline static int32_t get_offset_of_parserInputStack_1() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___parserInputStack_1)); }
	inline Stack_t2329662280 * get_parserInputStack_1() const { return ___parserInputStack_1; }
	inline Stack_t2329662280 ** get_address_of_parserInputStack_1() { return &___parserInputStack_1; }
	inline void set_parserInputStack_1(Stack_t2329662280 * value)
	{
		___parserInputStack_1 = value;
		Il2CppCodeGenWriteBarrier((&___parserInputStack_1), value);
	}

	inline static int32_t get_offset_of_nameBuffer_2() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___nameBuffer_2)); }
	inline CharU5BU5D_t3528271667* get_nameBuffer_2() const { return ___nameBuffer_2; }
	inline CharU5BU5D_t3528271667** get_address_of_nameBuffer_2() { return &___nameBuffer_2; }
	inline void set_nameBuffer_2(CharU5BU5D_t3528271667* value)
	{
		___nameBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameBuffer_2), value);
	}

	inline static int32_t get_offset_of_nameLength_3() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___nameLength_3)); }
	inline int32_t get_nameLength_3() const { return ___nameLength_3; }
	inline int32_t* get_address_of_nameLength_3() { return &___nameLength_3; }
	inline void set_nameLength_3(int32_t value)
	{
		___nameLength_3 = value;
	}

	inline static int32_t get_offset_of_nameCapacity_4() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___nameCapacity_4)); }
	inline int32_t get_nameCapacity_4() const { return ___nameCapacity_4; }
	inline int32_t* get_address_of_nameCapacity_4() { return &___nameCapacity_4; }
	inline void set_nameCapacity_4(int32_t value)
	{
		___nameCapacity_4 = value;
	}

	inline static int32_t get_offset_of_valueBuffer_5() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___valueBuffer_5)); }
	inline StringBuilder_t * get_valueBuffer_5() const { return ___valueBuffer_5; }
	inline StringBuilder_t ** get_address_of_valueBuffer_5() { return &___valueBuffer_5; }
	inline void set_valueBuffer_5(StringBuilder_t * value)
	{
		___valueBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_5), value);
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_6() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___currentLinkedNodeLineNumber_6)); }
	inline int32_t get_currentLinkedNodeLineNumber_6() const { return ___currentLinkedNodeLineNumber_6; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_6() { return &___currentLinkedNodeLineNumber_6; }
	inline void set_currentLinkedNodeLineNumber_6(int32_t value)
	{
		___currentLinkedNodeLineNumber_6 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_7() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___currentLinkedNodeLinePosition_7)); }
	inline int32_t get_currentLinkedNodeLinePosition_7() const { return ___currentLinkedNodeLinePosition_7; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_7() { return &___currentLinkedNodeLinePosition_7; }
	inline void set_currentLinkedNodeLinePosition_7(int32_t value)
	{
		___currentLinkedNodeLinePosition_7 = value;
	}

	inline static int32_t get_offset_of_dtdIncludeSect_8() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___dtdIncludeSect_8)); }
	inline int32_t get_dtdIncludeSect_8() const { return ___dtdIncludeSect_8; }
	inline int32_t* get_address_of_dtdIncludeSect_8() { return &___dtdIncludeSect_8; }
	inline void set_dtdIncludeSect_8(int32_t value)
	{
		___dtdIncludeSect_8 = value;
	}

	inline static int32_t get_offset_of_normalization_9() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___normalization_9)); }
	inline bool get_normalization_9() const { return ___normalization_9; }
	inline bool* get_address_of_normalization_9() { return &___normalization_9; }
	inline void set_normalization_9(bool value)
	{
		___normalization_9 = value;
	}

	inline static int32_t get_offset_of_processingInternalSubset_10() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___processingInternalSubset_10)); }
	inline bool get_processingInternalSubset_10() const { return ___processingInternalSubset_10; }
	inline bool* get_address_of_processingInternalSubset_10() { return &___processingInternalSubset_10; }
	inline void set_processingInternalSubset_10(bool value)
	{
		___processingInternalSubset_10 = value;
	}

	inline static int32_t get_offset_of_cachedPublicId_11() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___cachedPublicId_11)); }
	inline String_t* get_cachedPublicId_11() const { return ___cachedPublicId_11; }
	inline String_t** get_address_of_cachedPublicId_11() { return &___cachedPublicId_11; }
	inline void set_cachedPublicId_11(String_t* value)
	{
		___cachedPublicId_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPublicId_11), value);
	}

	inline static int32_t get_offset_of_cachedSystemId_12() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___cachedSystemId_12)); }
	inline String_t* get_cachedSystemId_12() const { return ___cachedSystemId_12; }
	inline String_t** get_address_of_cachedSystemId_12() { return &___cachedSystemId_12; }
	inline void set_cachedSystemId_12(String_t* value)
	{
		___cachedSystemId_12 = value;
		Il2CppCodeGenWriteBarrier((&___cachedSystemId_12), value);
	}

	inline static int32_t get_offset_of_DTD_13() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___DTD_13)); }
	inline DTDObjectModel_t1729680289 * get_DTD_13() const { return ___DTD_13; }
	inline DTDObjectModel_t1729680289 ** get_address_of_DTD_13() { return &___DTD_13; }
	inline void set_DTD_13(DTDObjectModel_t1729680289 * value)
	{
		___DTD_13 = value;
		Il2CppCodeGenWriteBarrier((&___DTD_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDREADER_T386081180_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef LIST_1_T218596005_H
#define LIST_1_T218596005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  List_1_t218596005  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t3368185270* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t218596005, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t3368185270* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t3368185270** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t3368185270* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t218596005, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t218596005, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t218596005_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t3368185270* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t218596005_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t3368185270* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t3368185270** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t3368185270* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T218596005_H
#ifndef DTDPARAMETERENTITYDECLARATIONCOLLECTION_T2844734410_H
#define DTDPARAMETERENTITYDECLARATIONCOLLECTION_T2844734410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclarationCollection
struct  DTDParameterEntityDeclarationCollection_t2844734410  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.DTDParameterEntityDeclarationCollection::peDecls
	Hashtable_t1853889766 * ___peDecls_0;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDParameterEntityDeclarationCollection::root
	DTDObjectModel_t1729680289 * ___root_1;

public:
	inline static int32_t get_offset_of_peDecls_0() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t2844734410, ___peDecls_0)); }
	inline Hashtable_t1853889766 * get_peDecls_0() const { return ___peDecls_0; }
	inline Hashtable_t1853889766 ** get_address_of_peDecls_0() { return &___peDecls_0; }
	inline void set_peDecls_0(Hashtable_t1853889766 * value)
	{
		___peDecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t2844734410, ___root_1)); }
	inline DTDObjectModel_t1729680289 * get_root_1() const { return ___root_1; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(DTDObjectModel_t1729680289 * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATIONCOLLECTION_T2844734410_H
#ifndef DTDNODE_T858560093_H
#define DTDNODE_T858560093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNode
struct  DTDNode_t858560093  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDNode::root
	DTDObjectModel_t1729680289 * ___root_0;
	// System.Boolean Mono.Xml.DTDNode::isInternalSubset
	bool ___isInternalSubset_1;
	// System.String Mono.Xml.DTDNode::baseURI
	String_t* ___baseURI_2;
	// System.Int32 Mono.Xml.DTDNode::lineNumber
	int32_t ___lineNumber_3;
	// System.Int32 Mono.Xml.DTDNode::linePosition
	int32_t ___linePosition_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___root_0)); }
	inline DTDObjectModel_t1729680289 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1729680289 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_isInternalSubset_1() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___isInternalSubset_1)); }
	inline bool get_isInternalSubset_1() const { return ___isInternalSubset_1; }
	inline bool* get_address_of_isInternalSubset_1() { return &___isInternalSubset_1; }
	inline void set_isInternalSubset_1(bool value)
	{
		___isInternalSubset_1 = value;
	}

	inline static int32_t get_offset_of_baseURI_2() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___baseURI_2)); }
	inline String_t* get_baseURI_2() const { return ___baseURI_2; }
	inline String_t** get_address_of_baseURI_2() { return &___baseURI_2; }
	inline void set_baseURI_2(String_t* value)
	{
		___baseURI_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_2), value);
	}

	inline static int32_t get_offset_of_lineNumber_3() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___lineNumber_3)); }
	inline int32_t get_lineNumber_3() const { return ___lineNumber_3; }
	inline int32_t* get_address_of_lineNumber_3() { return &___lineNumber_3; }
	inline void set_lineNumber_3(int32_t value)
	{
		___lineNumber_3 = value;
	}

	inline static int32_t get_offset_of_linePosition_4() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___linePosition_4)); }
	inline int32_t get_linePosition_4() const { return ___linePosition_4; }
	inline int32_t* get_address_of_linePosition_4() { return &___linePosition_4; }
	inline void set_linePosition_4(int32_t value)
	{
		___linePosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNODE_T858560093_H
#ifndef DTDCONTENTMODELCOLLECTION_T2798820000_H
#define DTDCONTENTMODELCOLLECTION_T2798820000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModelCollection
struct  DTDContentModelCollection_t2798820000  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Xml.DTDContentModelCollection::contentModel
	ArrayList_t2718874744 * ___contentModel_0;

public:
	inline static int32_t get_offset_of_contentModel_0() { return static_cast<int32_t>(offsetof(DTDContentModelCollection_t2798820000, ___contentModel_0)); }
	inline ArrayList_t2718874744 * get_contentModel_0() const { return ___contentModel_0; }
	inline ArrayList_t2718874744 ** get_address_of_contentModel_0() { return &___contentModel_0; }
	inline void set_contentModel_0(ArrayList_t2718874744 * value)
	{
		___contentModel_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODELCOLLECTION_T2798820000_H
#ifndef XMLPARSERINPUTSOURCE_T3533005609_H
#define XMLPARSERINPUTSOURCE_T3533005609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserInput/XmlParserInputSource
struct  XmlParserInputSource_t3533005609  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserInput/XmlParserInputSource::BaseURI
	String_t* ___BaseURI_0;
	// System.IO.TextReader System.Xml.XmlParserInput/XmlParserInputSource::reader
	TextReader_t283511965 * ___reader_1;
	// System.Int32 System.Xml.XmlParserInput/XmlParserInputSource::state
	int32_t ___state_2;
	// System.Boolean System.Xml.XmlParserInput/XmlParserInputSource::isPE
	bool ___isPE_3;
	// System.Int32 System.Xml.XmlParserInput/XmlParserInputSource::line
	int32_t ___line_4;
	// System.Int32 System.Xml.XmlParserInput/XmlParserInputSource::column
	int32_t ___column_5;

public:
	inline static int32_t get_offset_of_BaseURI_0() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t3533005609, ___BaseURI_0)); }
	inline String_t* get_BaseURI_0() const { return ___BaseURI_0; }
	inline String_t** get_address_of_BaseURI_0() { return &___BaseURI_0; }
	inline void set_BaseURI_0(String_t* value)
	{
		___BaseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURI_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t3533005609, ___reader_1)); }
	inline TextReader_t283511965 * get_reader_1() const { return ___reader_1; }
	inline TextReader_t283511965 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(TextReader_t283511965 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t3533005609, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_isPE_3() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t3533005609, ___isPE_3)); }
	inline bool get_isPE_3() const { return ___isPE_3; }
	inline bool* get_address_of_isPE_3() { return &___isPE_3; }
	inline void set_isPE_3(bool value)
	{
		___isPE_3 = value;
	}

	inline static int32_t get_offset_of_line_4() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t3533005609, ___line_4)); }
	inline int32_t get_line_4() const { return ___line_4; }
	inline int32_t* get_address_of_line_4() { return &___line_4; }
	inline void set_line_4(int32_t value)
	{
		___line_4 = value;
	}

	inline static int32_t get_offset_of_column_5() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t3533005609, ___column_5)); }
	inline int32_t get_column_5() const { return ___column_5; }
	inline int32_t* get_address_of_column_5() { return &___column_5; }
	inline void set_column_5(int32_t value)
	{
		___column_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERINPUTSOURCE_T3533005609_H
#ifndef XMLNAMEDNODEMAP_T2821286253_H
#define XMLNAMEDNODEMAP_T2821286253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t2821286253  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t3767805227 * ___parent_1;
	// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::nodeList
	ArrayList_t2718874744 * ___nodeList_2;
	// System.Boolean System.Xml.XmlNamedNodeMap::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___parent_1)); }
	inline XmlNode_t3767805227 * get_parent_1() const { return ___parent_1; }
	inline XmlNode_t3767805227 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlNode_t3767805227 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}

	inline static int32_t get_offset_of_nodeList_2() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___nodeList_2)); }
	inline ArrayList_t2718874744 * get_nodeList_2() const { return ___nodeList_2; }
	inline ArrayList_t2718874744 ** get_address_of_nodeList_2() { return &___nodeList_2; }
	inline void set_nodeList_2(ArrayList_t2718874744 * value)
	{
		___nodeList_2 = value;
		Il2CppCodeGenWriteBarrier((&___nodeList_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct XmlNamedNodeMap_t2821286253_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_T2821286253_H
#ifndef XMLNAMETABLE_T71772148_H
#define XMLNAMETABLE_T71772148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t71772148  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T71772148_H
#ifndef XMLNAMEENTRYCACHE_T2890546907_H
#define XMLNAMEENTRYCACHE_T2890546907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntryCache
struct  XmlNameEntryCache_t2890546907  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.XmlNameEntryCache::table
	Hashtable_t1853889766 * ___table_0;
	// System.Xml.XmlNameTable System.Xml.XmlNameEntryCache::nameTable
	XmlNameTable_t71772148 * ___nameTable_1;
	// System.Xml.XmlNameEntry System.Xml.XmlNameEntryCache::dummy
	XmlNameEntry_t1073099671 * ___dummy_2;
	// System.Char[] System.Xml.XmlNameEntryCache::cacheBuffer
	CharU5BU5D_t3528271667* ___cacheBuffer_3;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___table_0)); }
	inline Hashtable_t1853889766 * get_table_0() const { return ___table_0; }
	inline Hashtable_t1853889766 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_t1853889766 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___nameTable_1)); }
	inline XmlNameTable_t71772148 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t71772148 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_dummy_2() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___dummy_2)); }
	inline XmlNameEntry_t1073099671 * get_dummy_2() const { return ___dummy_2; }
	inline XmlNameEntry_t1073099671 ** get_address_of_dummy_2() { return &___dummy_2; }
	inline void set_dummy_2(XmlNameEntry_t1073099671 * value)
	{
		___dummy_2 = value;
		Il2CppCodeGenWriteBarrier((&___dummy_2), value);
	}

	inline static int32_t get_offset_of_cacheBuffer_3() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___cacheBuffer_3)); }
	inline CharU5BU5D_t3528271667* get_cacheBuffer_3() const { return ___cacheBuffer_3; }
	inline CharU5BU5D_t3528271667** get_address_of_cacheBuffer_3() { return &___cacheBuffer_3; }
	inline void set_cacheBuffer_3(CharU5BU5D_t3528271667* value)
	{
		___cacheBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___cacheBuffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRYCACHE_T2890546907_H
#ifndef XMLNAMEENTRY_T1073099671_H
#define XMLNAMEENTRY_T1073099671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntry
struct  XmlNameEntry_t1073099671  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlNameEntry::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNameEntry::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlNameEntry::NS
	String_t* ___NS_2;
	// System.Int32 System.Xml.XmlNameEntry::Hash
	int32_t ___Hash_3;
	// System.String System.Xml.XmlNameEntry::prefixed_name_cache
	String_t* ___prefixed_name_cache_4;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_Hash_3() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___Hash_3)); }
	inline int32_t get_Hash_3() const { return ___Hash_3; }
	inline int32_t* get_address_of_Hash_3() { return &___Hash_3; }
	inline void set_Hash_3(int32_t value)
	{
		___Hash_3 = value;
	}

	inline static int32_t get_offset_of_prefixed_name_cache_4() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___prefixed_name_cache_4)); }
	inline String_t* get_prefixed_name_cache_4() const { return ___prefixed_name_cache_4; }
	inline String_t** get_address_of_prefixed_name_cache_4() { return &___prefixed_name_cache_4; }
	inline void set_prefixed_name_cache_4(String_t* value)
	{
		___prefixed_name_cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefixed_name_cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRY_T1073099671_H
#ifndef XMLIMPLEMENTATION_T254178875_H
#define XMLIMPLEMENTATION_T254178875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlImplementation
struct  XmlImplementation_t254178875  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::InternalNameTable
	XmlNameTable_t71772148 * ___InternalNameTable_0;

public:
	inline static int32_t get_offset_of_InternalNameTable_0() { return static_cast<int32_t>(offsetof(XmlImplementation_t254178875, ___InternalNameTable_0)); }
	inline XmlNameTable_t71772148 * get_InternalNameTable_0() const { return ___InternalNameTable_0; }
	inline XmlNameTable_t71772148 ** get_address_of_InternalNameTable_0() { return &___InternalNameTable_0; }
	inline void set_InternalNameTable_0(XmlNameTable_t71772148 * value)
	{
		___InternalNameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___InternalNameTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIMPLEMENTATION_T254178875_H
#ifndef XMLNODE_T3767805227_H
#define XMLNODE_T3767805227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t3767805227  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNode::ownerDocument
	XmlDocument_t2837193595 * ___ownerDocument_1;
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t3767805227 * ___parentNode_2;
	// System.Xml.XmlNodeListChildren System.Xml.XmlNode::childNodes
	XmlNodeListChildren_t1082692789 * ___childNodes_3;

public:
	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___ownerDocument_1)); }
	inline XmlDocument_t2837193595 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline XmlDocument_t2837193595 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(XmlDocument_t2837193595 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parentNode_2() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___parentNode_2)); }
	inline XmlNode_t3767805227 * get_parentNode_2() const { return ___parentNode_2; }
	inline XmlNode_t3767805227 ** get_address_of_parentNode_2() { return &___parentNode_2; }
	inline void set_parentNode_2(XmlNode_t3767805227 * value)
	{
		___parentNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_2), value);
	}

	inline static int32_t get_offset_of_childNodes_3() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___childNodes_3)); }
	inline XmlNodeListChildren_t1082692789 * get_childNodes_3() const { return ___childNodes_3; }
	inline XmlNodeListChildren_t1082692789 ** get_address_of_childNodes_3() { return &___childNodes_3; }
	inline void set_childNodes_3(XmlNodeListChildren_t1082692789 * value)
	{
		___childNodes_3 = value;
		Il2CppCodeGenWriteBarrier((&___childNodes_3), value);
	}
};

struct XmlNode_t3767805227_StaticFields
{
public:
	// System.Xml.XmlNode/EmptyNodeList System.Xml.XmlNode::emptyList
	EmptyNodeList_t139615908 * ___emptyList_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map44
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map44_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map46
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map46_5;

public:
	inline static int32_t get_offset_of_emptyList_0() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___emptyList_0)); }
	inline EmptyNodeList_t139615908 * get_emptyList_0() const { return ___emptyList_0; }
	inline EmptyNodeList_t139615908 ** get_address_of_emptyList_0() { return &___emptyList_0; }
	inline void set_emptyList_0(EmptyNodeList_t139615908 * value)
	{
		___emptyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map44_4() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___U3CU3Ef__switchU24map44_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map44_4() const { return ___U3CU3Ef__switchU24map44_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map44_4() { return &___U3CU3Ef__switchU24map44_4; }
	inline void set_U3CU3Ef__switchU24map44_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map44_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map44_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map46_5() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___U3CU3Ef__switchU24map46_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map46_5() const { return ___U3CU3Ef__switchU24map46_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map46_5() { return &___U3CU3Ef__switchU24map46_5; }
	inline void set_U3CU3Ef__switchU24map46_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map46_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map46_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T3767805227_H
#ifndef XMLNODELIST_T2551693786_H
#define XMLNODELIST_T2551693786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeList
struct  XmlNodeList_t2551693786  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELIST_T2551693786_H
#ifndef ENUMERATOR_T97922292_H
#define ENUMERATOR_T97922292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren/Enumerator
struct  Enumerator_t97922292  : public RuntimeObject
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren/Enumerator::parent
	RuntimeObject* ___parent_0;
	// System.Xml.XmlLinkedNode System.Xml.XmlNodeListChildren/Enumerator::currentChild
	XmlLinkedNode_t1437094927 * ___currentChild_1;
	// System.Boolean System.Xml.XmlNodeListChildren/Enumerator::passedLastNode
	bool ___passedLastNode_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Enumerator_t97922292, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_currentChild_1() { return static_cast<int32_t>(offsetof(Enumerator_t97922292, ___currentChild_1)); }
	inline XmlLinkedNode_t1437094927 * get_currentChild_1() const { return ___currentChild_1; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_currentChild_1() { return &___currentChild_1; }
	inline void set_currentChild_1(XmlLinkedNode_t1437094927 * value)
	{
		___currentChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentChild_1), value);
	}

	inline static int32_t get_offset_of_passedLastNode_2() { return static_cast<int32_t>(offsetof(Enumerator_t97922292, ___passedLastNode_2)); }
	inline bool get_passedLastNode_2() const { return ___passedLastNode_2; }
	inline bool* get_address_of_passedLastNode_2() { return &___passedLastNode_2; }
	inline void set_passedLastNode_2(bool value)
	{
		___passedLastNode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T97922292_H
#ifndef XMLCHAR_T3816087079_H
#define XMLCHAR_T3816087079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChar
struct  XmlChar_t3816087079  : public RuntimeObject
{
public:

public:
};

struct XmlChar_t3816087079_StaticFields
{
public:
	// System.Char[] System.Xml.XmlChar::WhitespaceChars
	CharU5BU5D_t3528271667* ___WhitespaceChars_0;
	// System.Byte[] System.Xml.XmlChar::firstNamePages
	ByteU5BU5D_t4116647657* ___firstNamePages_1;
	// System.Byte[] System.Xml.XmlChar::namePages
	ByteU5BU5D_t4116647657* ___namePages_2;
	// System.UInt32[] System.Xml.XmlChar::nameBitmap
	UInt32U5BU5D_t2770800703* ___nameBitmap_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlChar::<>f__switch$map47
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map47_4;

public:
	inline static int32_t get_offset_of_WhitespaceChars_0() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___WhitespaceChars_0)); }
	inline CharU5BU5D_t3528271667* get_WhitespaceChars_0() const { return ___WhitespaceChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_WhitespaceChars_0() { return &___WhitespaceChars_0; }
	inline void set_WhitespaceChars_0(CharU5BU5D_t3528271667* value)
	{
		___WhitespaceChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_0), value);
	}

	inline static int32_t get_offset_of_firstNamePages_1() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___firstNamePages_1)); }
	inline ByteU5BU5D_t4116647657* get_firstNamePages_1() const { return ___firstNamePages_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_firstNamePages_1() { return &___firstNamePages_1; }
	inline void set_firstNamePages_1(ByteU5BU5D_t4116647657* value)
	{
		___firstNamePages_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstNamePages_1), value);
	}

	inline static int32_t get_offset_of_namePages_2() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___namePages_2)); }
	inline ByteU5BU5D_t4116647657* get_namePages_2() const { return ___namePages_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_namePages_2() { return &___namePages_2; }
	inline void set_namePages_2(ByteU5BU5D_t4116647657* value)
	{
		___namePages_2 = value;
		Il2CppCodeGenWriteBarrier((&___namePages_2), value);
	}

	inline static int32_t get_offset_of_nameBitmap_3() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___nameBitmap_3)); }
	inline UInt32U5BU5D_t2770800703* get_nameBitmap_3() const { return ___nameBitmap_3; }
	inline UInt32U5BU5D_t2770800703** get_address_of_nameBitmap_3() { return &___nameBitmap_3; }
	inline void set_nameBitmap_3(UInt32U5BU5D_t2770800703* value)
	{
		___nameBitmap_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameBitmap_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map47_4() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___U3CU3Ef__switchU24map47_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map47_4() const { return ___U3CU3Ef__switchU24map47_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map47_4() { return &___U3CU3Ef__switchU24map47_4; }
	inline void set_U3CU3Ef__switchU24map47_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map47_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map47_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHAR_T3816087079_H
#ifndef XMLPARSERINPUT_T2182411204_H
#define XMLPARSERINPUT_T2182411204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserInput
struct  XmlParserInput_t2182411204  : public RuntimeObject
{
public:
	// System.Collections.Stack System.Xml.XmlParserInput::sourceStack
	Stack_t2329662280 * ___sourceStack_0;
	// System.Xml.XmlParserInput/XmlParserInputSource System.Xml.XmlParserInput::source
	XmlParserInputSource_t3533005609 * ___source_1;
	// System.Boolean System.Xml.XmlParserInput::has_peek
	bool ___has_peek_2;
	// System.Int32 System.Xml.XmlParserInput::peek_char
	int32_t ___peek_char_3;
	// System.Boolean System.Xml.XmlParserInput::allowTextDecl
	bool ___allowTextDecl_4;

public:
	inline static int32_t get_offset_of_sourceStack_0() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___sourceStack_0)); }
	inline Stack_t2329662280 * get_sourceStack_0() const { return ___sourceStack_0; }
	inline Stack_t2329662280 ** get_address_of_sourceStack_0() { return &___sourceStack_0; }
	inline void set_sourceStack_0(Stack_t2329662280 * value)
	{
		___sourceStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceStack_0), value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___source_1)); }
	inline XmlParserInputSource_t3533005609 * get_source_1() const { return ___source_1; }
	inline XmlParserInputSource_t3533005609 ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(XmlParserInputSource_t3533005609 * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_has_peek_2() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___has_peek_2)); }
	inline bool get_has_peek_2() const { return ___has_peek_2; }
	inline bool* get_address_of_has_peek_2() { return &___has_peek_2; }
	inline void set_has_peek_2(bool value)
	{
		___has_peek_2 = value;
	}

	inline static int32_t get_offset_of_peek_char_3() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___peek_char_3)); }
	inline int32_t get_peek_char_3() const { return ___peek_char_3; }
	inline int32_t* get_address_of_peek_char_3() { return &___peek_char_3; }
	inline void set_peek_char_3(int32_t value)
	{
		___peek_char_3 = value;
	}

	inline static int32_t get_offset_of_allowTextDecl_4() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___allowTextDecl_4)); }
	inline bool get_allowTextDecl_4() const { return ___allowTextDecl_4; }
	inline bool* get_address_of_allowTextDecl_4() { return &___allowTextDecl_4; }
	inline void set_allowTextDecl_4(bool value)
	{
		___allowTextDecl_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERINPUT_T2182411204_H
#ifndef XMLNAMESPACEMANAGER_T418790500_H
#define XMLNAMESPACEMANAGER_T418790500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t418790500  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NsDecl[] System.Xml.XmlNamespaceManager::decls
	NsDeclU5BU5D_t2116608150* ___decls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::declPos
	int32_t ___declPos_1;
	// System.Xml.XmlNamespaceManager/NsScope[] System.Xml.XmlNamespaceManager::scopes
	NsScopeU5BU5D_t382374428* ___scopes_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopePos
	int32_t ___scopePos_3;
	// System.String System.Xml.XmlNamespaceManager::defaultNamespace
	String_t* ___defaultNamespace_4;
	// System.Int32 System.Xml.XmlNamespaceManager::count
	int32_t ___count_5;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t71772148 * ___nameTable_6;
	// System.Boolean System.Xml.XmlNamespaceManager::internalAtomizedNames
	bool ___internalAtomizedNames_7;

public:
	inline static int32_t get_offset_of_decls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___decls_0)); }
	inline NsDeclU5BU5D_t2116608150* get_decls_0() const { return ___decls_0; }
	inline NsDeclU5BU5D_t2116608150** get_address_of_decls_0() { return &___decls_0; }
	inline void set_decls_0(NsDeclU5BU5D_t2116608150* value)
	{
		___decls_0 = value;
		Il2CppCodeGenWriteBarrier((&___decls_0), value);
	}

	inline static int32_t get_offset_of_declPos_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___declPos_1)); }
	inline int32_t get_declPos_1() const { return ___declPos_1; }
	inline int32_t* get_address_of_declPos_1() { return &___declPos_1; }
	inline void set_declPos_1(int32_t value)
	{
		___declPos_1 = value;
	}

	inline static int32_t get_offset_of_scopes_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___scopes_2)); }
	inline NsScopeU5BU5D_t382374428* get_scopes_2() const { return ___scopes_2; }
	inline NsScopeU5BU5D_t382374428** get_address_of_scopes_2() { return &___scopes_2; }
	inline void set_scopes_2(NsScopeU5BU5D_t382374428* value)
	{
		___scopes_2 = value;
		Il2CppCodeGenWriteBarrier((&___scopes_2), value);
	}

	inline static int32_t get_offset_of_scopePos_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___scopePos_3)); }
	inline int32_t get_scopePos_3() const { return ___scopePos_3; }
	inline int32_t* get_address_of_scopePos_3() { return &___scopePos_3; }
	inline void set_scopePos_3(int32_t value)
	{
		___scopePos_3 = value;
	}

	inline static int32_t get_offset_of_defaultNamespace_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___defaultNamespace_4)); }
	inline String_t* get_defaultNamespace_4() const { return ___defaultNamespace_4; }
	inline String_t** get_address_of_defaultNamespace_4() { return &___defaultNamespace_4; }
	inline void set_defaultNamespace_4(String_t* value)
	{
		___defaultNamespace_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_4), value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_nameTable_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___nameTable_6)); }
	inline XmlNameTable_t71772148 * get_nameTable_6() const { return ___nameTable_6; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_6() { return &___nameTable_6; }
	inline void set_nameTable_6(XmlNameTable_t71772148 * value)
	{
		___nameTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_6), value);
	}

	inline static int32_t get_offset_of_internalAtomizedNames_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___internalAtomizedNames_7)); }
	inline bool get_internalAtomizedNames_7() const { return ___internalAtomizedNames_7; }
	inline bool* get_address_of_internalAtomizedNames_7() { return &___internalAtomizedNames_7; }
	inline void set_internalAtomizedNames_7(bool value)
	{
		___internalAtomizedNames_7 = value;
	}
};

struct XmlNamespaceManager_t418790500_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::<>f__switch$map28
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map28_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_8() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500_StaticFields, ___U3CU3Ef__switchU24map28_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map28_8() const { return ___U3CU3Ef__switchU24map28_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map28_8() { return &___U3CU3Ef__switchU24map28_8; }
	inline void set_U3CU3Ef__switchU24map28_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map28_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map28_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T418790500_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef DTDOBJECTMODEL_T1729680289_H
#define DTDOBJECTMODEL_T1729680289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDObjectModel
struct  DTDObjectModel_t1729680289  : public RuntimeObject
{
public:
	// Mono.Xml.DTDAutomataFactory Mono.Xml.DTDObjectModel::factory
	DTDAutomataFactory_t2958275022 * ___factory_0;
	// Mono.Xml.DTDElementDeclarationCollection Mono.Xml.DTDObjectModel::elementDecls
	DTDElementDeclarationCollection_t222313714 * ___elementDecls_1;
	// Mono.Xml.DTDAttListDeclarationCollection Mono.Xml.DTDObjectModel::attListDecls
	DTDAttListDeclarationCollection_t2220366188 * ___attListDecls_2;
	// Mono.Xml.DTDParameterEntityDeclarationCollection Mono.Xml.DTDObjectModel::peDecls
	DTDParameterEntityDeclarationCollection_t2844734410 * ___peDecls_3;
	// Mono.Xml.DTDEntityDeclarationCollection Mono.Xml.DTDObjectModel::entityDecls
	DTDEntityDeclarationCollection_t2250844513 * ___entityDecls_4;
	// Mono.Xml.DTDNotationDeclarationCollection Mono.Xml.DTDObjectModel::notationDecls
	DTDNotationDeclarationCollection_t959292105 * ___notationDecls_5;
	// System.Collections.ArrayList Mono.Xml.DTDObjectModel::validationErrors
	ArrayList_t2718874744 * ___validationErrors_6;
	// System.Xml.XmlResolver Mono.Xml.DTDObjectModel::resolver
	XmlResolver_t626023767 * ___resolver_7;
	// System.Xml.XmlNameTable Mono.Xml.DTDObjectModel::nameTable
	XmlNameTable_t71772148 * ___nameTable_8;
	// System.Collections.Hashtable Mono.Xml.DTDObjectModel::externalResources
	Hashtable_t1853889766 * ___externalResources_9;
	// System.String Mono.Xml.DTDObjectModel::baseURI
	String_t* ___baseURI_10;
	// System.String Mono.Xml.DTDObjectModel::name
	String_t* ___name_11;
	// System.String Mono.Xml.DTDObjectModel::publicId
	String_t* ___publicId_12;
	// System.String Mono.Xml.DTDObjectModel::systemId
	String_t* ___systemId_13;
	// System.String Mono.Xml.DTDObjectModel::intSubset
	String_t* ___intSubset_14;
	// System.Boolean Mono.Xml.DTDObjectModel::intSubsetHasPERef
	bool ___intSubsetHasPERef_15;
	// System.Boolean Mono.Xml.DTDObjectModel::isStandalone
	bool ___isStandalone_16;
	// System.Int32 Mono.Xml.DTDObjectModel::lineNumber
	int32_t ___lineNumber_17;
	// System.Int32 Mono.Xml.DTDObjectModel::linePosition
	int32_t ___linePosition_18;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___factory_0)); }
	inline DTDAutomataFactory_t2958275022 * get_factory_0() const { return ___factory_0; }
	inline DTDAutomataFactory_t2958275022 ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(DTDAutomataFactory_t2958275022 * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___factory_0), value);
	}

	inline static int32_t get_offset_of_elementDecls_1() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___elementDecls_1)); }
	inline DTDElementDeclarationCollection_t222313714 * get_elementDecls_1() const { return ___elementDecls_1; }
	inline DTDElementDeclarationCollection_t222313714 ** get_address_of_elementDecls_1() { return &___elementDecls_1; }
	inline void set_elementDecls_1(DTDElementDeclarationCollection_t222313714 * value)
	{
		___elementDecls_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecls_1), value);
	}

	inline static int32_t get_offset_of_attListDecls_2() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___attListDecls_2)); }
	inline DTDAttListDeclarationCollection_t2220366188 * get_attListDecls_2() const { return ___attListDecls_2; }
	inline DTDAttListDeclarationCollection_t2220366188 ** get_address_of_attListDecls_2() { return &___attListDecls_2; }
	inline void set_attListDecls_2(DTDAttListDeclarationCollection_t2220366188 * value)
	{
		___attListDecls_2 = value;
		Il2CppCodeGenWriteBarrier((&___attListDecls_2), value);
	}

	inline static int32_t get_offset_of_peDecls_3() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___peDecls_3)); }
	inline DTDParameterEntityDeclarationCollection_t2844734410 * get_peDecls_3() const { return ___peDecls_3; }
	inline DTDParameterEntityDeclarationCollection_t2844734410 ** get_address_of_peDecls_3() { return &___peDecls_3; }
	inline void set_peDecls_3(DTDParameterEntityDeclarationCollection_t2844734410 * value)
	{
		___peDecls_3 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_3), value);
	}

	inline static int32_t get_offset_of_entityDecls_4() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___entityDecls_4)); }
	inline DTDEntityDeclarationCollection_t2250844513 * get_entityDecls_4() const { return ___entityDecls_4; }
	inline DTDEntityDeclarationCollection_t2250844513 ** get_address_of_entityDecls_4() { return &___entityDecls_4; }
	inline void set_entityDecls_4(DTDEntityDeclarationCollection_t2250844513 * value)
	{
		___entityDecls_4 = value;
		Il2CppCodeGenWriteBarrier((&___entityDecls_4), value);
	}

	inline static int32_t get_offset_of_notationDecls_5() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___notationDecls_5)); }
	inline DTDNotationDeclarationCollection_t959292105 * get_notationDecls_5() const { return ___notationDecls_5; }
	inline DTDNotationDeclarationCollection_t959292105 ** get_address_of_notationDecls_5() { return &___notationDecls_5; }
	inline void set_notationDecls_5(DTDNotationDeclarationCollection_t959292105 * value)
	{
		___notationDecls_5 = value;
		Il2CppCodeGenWriteBarrier((&___notationDecls_5), value);
	}

	inline static int32_t get_offset_of_validationErrors_6() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___validationErrors_6)); }
	inline ArrayList_t2718874744 * get_validationErrors_6() const { return ___validationErrors_6; }
	inline ArrayList_t2718874744 ** get_address_of_validationErrors_6() { return &___validationErrors_6; }
	inline void set_validationErrors_6(ArrayList_t2718874744 * value)
	{
		___validationErrors_6 = value;
		Il2CppCodeGenWriteBarrier((&___validationErrors_6), value);
	}

	inline static int32_t get_offset_of_resolver_7() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___resolver_7)); }
	inline XmlResolver_t626023767 * get_resolver_7() const { return ___resolver_7; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_7() { return &___resolver_7; }
	inline void set_resolver_7(XmlResolver_t626023767 * value)
	{
		___resolver_7 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_7), value);
	}

	inline static int32_t get_offset_of_nameTable_8() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___nameTable_8)); }
	inline XmlNameTable_t71772148 * get_nameTable_8() const { return ___nameTable_8; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_8() { return &___nameTable_8; }
	inline void set_nameTable_8(XmlNameTable_t71772148 * value)
	{
		___nameTable_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_8), value);
	}

	inline static int32_t get_offset_of_externalResources_9() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___externalResources_9)); }
	inline Hashtable_t1853889766 * get_externalResources_9() const { return ___externalResources_9; }
	inline Hashtable_t1853889766 ** get_address_of_externalResources_9() { return &___externalResources_9; }
	inline void set_externalResources_9(Hashtable_t1853889766 * value)
	{
		___externalResources_9 = value;
		Il2CppCodeGenWriteBarrier((&___externalResources_9), value);
	}

	inline static int32_t get_offset_of_baseURI_10() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___baseURI_10)); }
	inline String_t* get_baseURI_10() const { return ___baseURI_10; }
	inline String_t** get_address_of_baseURI_10() { return &___baseURI_10; }
	inline void set_baseURI_10(String_t* value)
	{
		___baseURI_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_10), value);
	}

	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___name_11)); }
	inline String_t* get_name_11() const { return ___name_11; }
	inline String_t** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(String_t* value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier((&___name_11), value);
	}

	inline static int32_t get_offset_of_publicId_12() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___publicId_12)); }
	inline String_t* get_publicId_12() const { return ___publicId_12; }
	inline String_t** get_address_of_publicId_12() { return &___publicId_12; }
	inline void set_publicId_12(String_t* value)
	{
		___publicId_12 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_12), value);
	}

	inline static int32_t get_offset_of_systemId_13() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___systemId_13)); }
	inline String_t* get_systemId_13() const { return ___systemId_13; }
	inline String_t** get_address_of_systemId_13() { return &___systemId_13; }
	inline void set_systemId_13(String_t* value)
	{
		___systemId_13 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_13), value);
	}

	inline static int32_t get_offset_of_intSubset_14() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___intSubset_14)); }
	inline String_t* get_intSubset_14() const { return ___intSubset_14; }
	inline String_t** get_address_of_intSubset_14() { return &___intSubset_14; }
	inline void set_intSubset_14(String_t* value)
	{
		___intSubset_14 = value;
		Il2CppCodeGenWriteBarrier((&___intSubset_14), value);
	}

	inline static int32_t get_offset_of_intSubsetHasPERef_15() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___intSubsetHasPERef_15)); }
	inline bool get_intSubsetHasPERef_15() const { return ___intSubsetHasPERef_15; }
	inline bool* get_address_of_intSubsetHasPERef_15() { return &___intSubsetHasPERef_15; }
	inline void set_intSubsetHasPERef_15(bool value)
	{
		___intSubsetHasPERef_15 = value;
	}

	inline static int32_t get_offset_of_isStandalone_16() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___isStandalone_16)); }
	inline bool get_isStandalone_16() const { return ___isStandalone_16; }
	inline bool* get_address_of_isStandalone_16() { return &___isStandalone_16; }
	inline void set_isStandalone_16(bool value)
	{
		___isStandalone_16 = value;
	}

	inline static int32_t get_offset_of_lineNumber_17() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___lineNumber_17)); }
	inline int32_t get_lineNumber_17() const { return ___lineNumber_17; }
	inline int32_t* get_address_of_lineNumber_17() { return &___lineNumber_17; }
	inline void set_lineNumber_17(int32_t value)
	{
		___lineNumber_17 = value;
	}

	inline static int32_t get_offset_of_linePosition_18() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___linePosition_18)); }
	inline int32_t get_linePosition_18() const { return ___linePosition_18; }
	inline int32_t* get_address_of_linePosition_18() { return &___linePosition_18; }
	inline void set_linePosition_18(int32_t value)
	{
		___linePosition_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOBJECTMODEL_T1729680289_H
#ifndef DTDAUTOMATAFACTORY_T2958275022_H
#define DTDAUTOMATAFACTORY_T2958275022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomataFactory
struct  DTDAutomataFactory_t2958275022  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomataFactory::root
	DTDObjectModel_t1729680289 * ___root_0;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::choiceTable
	Hashtable_t1853889766 * ___choiceTable_1;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::sequenceTable
	Hashtable_t1853889766 * ___sequenceTable_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t2958275022, ___root_0)); }
	inline DTDObjectModel_t1729680289 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1729680289 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_choiceTable_1() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t2958275022, ___choiceTable_1)); }
	inline Hashtable_t1853889766 * get_choiceTable_1() const { return ___choiceTable_1; }
	inline Hashtable_t1853889766 ** get_address_of_choiceTable_1() { return &___choiceTable_1; }
	inline void set_choiceTable_1(Hashtable_t1853889766 * value)
	{
		___choiceTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___choiceTable_1), value);
	}

	inline static int32_t get_offset_of_sequenceTable_2() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t2958275022, ___sequenceTable_2)); }
	inline Hashtable_t1853889766 * get_sequenceTable_2() const { return ___sequenceTable_2; }
	inline Hashtable_t1853889766 ** get_address_of_sequenceTable_2() { return &___sequenceTable_2; }
	inline void set_sequenceTable_2(Hashtable_t1853889766 * value)
	{
		___sequenceTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATAFACTORY_T2958275022_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef TEXTREADER_T283511965_H
#define TEXTREADER_T283511965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t283511965  : public RuntimeObject
{
public:

public:
};

struct TextReader_t283511965_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t283511965 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t283511965_StaticFields, ___Null_0)); }
	inline TextReader_t283511965 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t283511965 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t283511965 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T283511965_H
#ifndef TYPETRANSLATOR_T3446962748_H
#define TYPETRANSLATOR_T3446962748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeTranslator
struct  TypeTranslator_t3446962748  : public RuntimeObject
{
public:

public:
};

struct TypeTranslator_t3446962748_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nameCache
	Hashtable_t1853889766 * ___nameCache_0;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveTypes
	Hashtable_t1853889766 * ___primitiveTypes_1;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveArrayTypes
	Hashtable_t1853889766 * ___primitiveArrayTypes_2;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nullableTypes
	Hashtable_t1853889766 * ___nullableTypes_3;

public:
	inline static int32_t get_offset_of_nameCache_0() { return static_cast<int32_t>(offsetof(TypeTranslator_t3446962748_StaticFields, ___nameCache_0)); }
	inline Hashtable_t1853889766 * get_nameCache_0() const { return ___nameCache_0; }
	inline Hashtable_t1853889766 ** get_address_of_nameCache_0() { return &___nameCache_0; }
	inline void set_nameCache_0(Hashtable_t1853889766 * value)
	{
		___nameCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_0), value);
	}

	inline static int32_t get_offset_of_primitiveTypes_1() { return static_cast<int32_t>(offsetof(TypeTranslator_t3446962748_StaticFields, ___primitiveTypes_1)); }
	inline Hashtable_t1853889766 * get_primitiveTypes_1() const { return ___primitiveTypes_1; }
	inline Hashtable_t1853889766 ** get_address_of_primitiveTypes_1() { return &___primitiveTypes_1; }
	inline void set_primitiveTypes_1(Hashtable_t1853889766 * value)
	{
		___primitiveTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveTypes_1), value);
	}

	inline static int32_t get_offset_of_primitiveArrayTypes_2() { return static_cast<int32_t>(offsetof(TypeTranslator_t3446962748_StaticFields, ___primitiveArrayTypes_2)); }
	inline Hashtable_t1853889766 * get_primitiveArrayTypes_2() const { return ___primitiveArrayTypes_2; }
	inline Hashtable_t1853889766 ** get_address_of_primitiveArrayTypes_2() { return &___primitiveArrayTypes_2; }
	inline void set_primitiveArrayTypes_2(Hashtable_t1853889766 * value)
	{
		___primitiveArrayTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveArrayTypes_2), value);
	}

	inline static int32_t get_offset_of_nullableTypes_3() { return static_cast<int32_t>(offsetof(TypeTranslator_t3446962748_StaticFields, ___nullableTypes_3)); }
	inline Hashtable_t1853889766 * get_nullableTypes_3() const { return ___nullableTypes_3; }
	inline Hashtable_t1853889766 ** get_address_of_nullableTypes_3() { return &___nullableTypes_3; }
	inline void set_nullableTypes_3(Hashtable_t1853889766 * value)
	{
		___nullableTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___nullableTypes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPETRANSLATOR_T3446962748_H
#ifndef XMLSERIALIZERNAMESPACES_T2702737953_H
#define XMLSERIALIZERNAMESPACES_T2702737953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t2702737953  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	ListDictionary_t1624492310 * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_t2702737953, ___namespaces_0)); }
	inline ListDictionary_t1624492310 * get_namespaces_0() const { return ___namespaces_0; }
	inline ListDictionary_t1624492310 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(ListDictionary_t1624492310 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERNAMESPACES_T2702737953_H
#ifndef XMLDOCUMENTFRAGMENT_T1323348855_H
#define XMLDOCUMENTFRAGMENT_T1323348855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_t1323348855  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_6;

public:
	inline static int32_t get_offset_of_lastLinkedChild_6() { return static_cast<int32_t>(offsetof(XmlDocumentFragment_t1323348855, ___lastLinkedChild_6)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_6() const { return ___lastLinkedChild_6; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_6() { return &___lastLinkedChild_6; }
	inline void set_lastLinkedChild_6(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTFRAGMENT_T1323348855_H
#ifndef EMPTYNODELIST_T139615908_H
#define EMPTYNODELIST_T139615908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode/EmptyNodeList
struct  EmptyNodeList_t139615908  : public XmlNodeList_t2551693786
{
public:

public:
};

struct EmptyNodeList_t139615908_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNode/EmptyNodeList::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(EmptyNodeList_t139615908_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYNODELIST_T139615908_H
#ifndef XMLNAMESPACEDECLARATIONSATTRIBUTE_T966425202_H
#define XMLNAMESPACEDECLARATIONSATTRIBUTE_T966425202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlNamespaceDeclarationsAttribute
struct  XmlNamespaceDeclarationsAttribute_t966425202  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEDECLARATIONSATTRIBUTE_T966425202_H
#ifndef XMLNODEREADER_T4064889562_H
#define XMLNODEREADER_T4064889562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReader
struct  XmlNodeReader_t4064889562  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlReader System.Xml.XmlNodeReader::entity
	XmlReader_t3121518892 * ___entity_3;
	// System.Xml.XmlNodeReaderImpl System.Xml.XmlNodeReader::source
	XmlNodeReaderImpl_t2501602067 * ___source_4;
	// System.Boolean System.Xml.XmlNodeReader::entityInsideAttribute
	bool ___entityInsideAttribute_5;
	// System.Boolean System.Xml.XmlNodeReader::insideAttribute
	bool ___insideAttribute_6;

public:
	inline static int32_t get_offset_of_entity_3() { return static_cast<int32_t>(offsetof(XmlNodeReader_t4064889562, ___entity_3)); }
	inline XmlReader_t3121518892 * get_entity_3() const { return ___entity_3; }
	inline XmlReader_t3121518892 ** get_address_of_entity_3() { return &___entity_3; }
	inline void set_entity_3(XmlReader_t3121518892 * value)
	{
		___entity_3 = value;
		Il2CppCodeGenWriteBarrier((&___entity_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(XmlNodeReader_t4064889562, ___source_4)); }
	inline XmlNodeReaderImpl_t2501602067 * get_source_4() const { return ___source_4; }
	inline XmlNodeReaderImpl_t2501602067 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(XmlNodeReaderImpl_t2501602067 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_entityInsideAttribute_5() { return static_cast<int32_t>(offsetof(XmlNodeReader_t4064889562, ___entityInsideAttribute_5)); }
	inline bool get_entityInsideAttribute_5() const { return ___entityInsideAttribute_5; }
	inline bool* get_address_of_entityInsideAttribute_5() { return &___entityInsideAttribute_5; }
	inline void set_entityInsideAttribute_5(bool value)
	{
		___entityInsideAttribute_5 = value;
	}

	inline static int32_t get_offset_of_insideAttribute_6() { return static_cast<int32_t>(offsetof(XmlNodeReader_t4064889562, ___insideAttribute_6)); }
	inline bool get_insideAttribute_6() const { return ___insideAttribute_6; }
	inline bool* get_address_of_insideAttribute_6() { return &___insideAttribute_6; }
	inline void set_insideAttribute_6(bool value)
	{
		___insideAttribute_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEREADER_T4064889562_H
#ifndef XMLIGNOREATTRIBUTE_T1428424057_H
#define XMLIGNOREATTRIBUTE_T1428424057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t1428424057  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIGNOREATTRIBUTE_T1428424057_H
#ifndef XMLNODELISTCHILDREN_T1082692789_H
#define XMLNODELISTCHILDREN_T1082692789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren
struct  XmlNodeListChildren_t1082692789  : public XmlNodeList_t2551693786
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren::parent
	RuntimeObject* ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNodeListChildren_t1082692789, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELISTCHILDREN_T1082692789_H
#ifndef XMLENUMATTRIBUTE_T106705320_H
#define XMLENUMATTRIBUTE_T106705320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_t106705320  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XmlEnumAttribute_t106705320, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENUMATTRIBUTE_T106705320_H
#ifndef XMLDOCUMENT_T2837193595_H
#define XMLDOCUMENT_T2837193595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t2837193595  : public XmlNode_t3767805227
{
public:
	// System.Boolean System.Xml.XmlDocument::optimal_create_element
	bool ___optimal_create_element_7;
	// System.Boolean System.Xml.XmlDocument::optimal_create_attribute
	bool ___optimal_create_attribute_8;
	// System.Xml.XmlNameTable System.Xml.XmlDocument::nameTable
	XmlNameTable_t71772148 * ___nameTable_9;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_10;
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t254178875 * ___implementation_11;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_12;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t626023767 * ___resolver_13;
	// System.Collections.Hashtable System.Xml.XmlDocument::idTable
	Hashtable_t1853889766 * ___idTable_14;
	// System.Xml.XmlNameEntryCache System.Xml.XmlDocument::nameCache
	XmlNameEntryCache_t2890546907 * ___nameCache_15;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_16;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::schemaInfo
	RuntimeObject* ___schemaInfo_17;
	// System.Boolean System.Xml.XmlDocument::loadMode
	bool ___loadMode_18;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanged
	XmlNodeChangedEventHandler_t1533444722 * ___NodeChanged_19;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanging
	XmlNodeChangedEventHandler_t1533444722 * ___NodeChanging_20;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserted
	XmlNodeChangedEventHandler_t1533444722 * ___NodeInserted_21;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserting
	XmlNodeChangedEventHandler_t1533444722 * ___NodeInserting_22;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoved
	XmlNodeChangedEventHandler_t1533444722 * ___NodeRemoved_23;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoving
	XmlNodeChangedEventHandler_t1533444722 * ___NodeRemoving_24;

public:
	inline static int32_t get_offset_of_optimal_create_element_7() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___optimal_create_element_7)); }
	inline bool get_optimal_create_element_7() const { return ___optimal_create_element_7; }
	inline bool* get_address_of_optimal_create_element_7() { return &___optimal_create_element_7; }
	inline void set_optimal_create_element_7(bool value)
	{
		___optimal_create_element_7 = value;
	}

	inline static int32_t get_offset_of_optimal_create_attribute_8() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___optimal_create_attribute_8)); }
	inline bool get_optimal_create_attribute_8() const { return ___optimal_create_attribute_8; }
	inline bool* get_address_of_optimal_create_attribute_8() { return &___optimal_create_attribute_8; }
	inline void set_optimal_create_attribute_8(bool value)
	{
		___optimal_create_attribute_8 = value;
	}

	inline static int32_t get_offset_of_nameTable_9() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___nameTable_9)); }
	inline XmlNameTable_t71772148 * get_nameTable_9() const { return ___nameTable_9; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_9() { return &___nameTable_9; }
	inline void set_nameTable_9(XmlNameTable_t71772148 * value)
	{
		___nameTable_9 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_9), value);
	}

	inline static int32_t get_offset_of_baseURI_10() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___baseURI_10)); }
	inline String_t* get_baseURI_10() const { return ___baseURI_10; }
	inline String_t** get_address_of_baseURI_10() { return &___baseURI_10; }
	inline void set_baseURI_10(String_t* value)
	{
		___baseURI_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_10), value);
	}

	inline static int32_t get_offset_of_implementation_11() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___implementation_11)); }
	inline XmlImplementation_t254178875 * get_implementation_11() const { return ___implementation_11; }
	inline XmlImplementation_t254178875 ** get_address_of_implementation_11() { return &___implementation_11; }
	inline void set_implementation_11(XmlImplementation_t254178875 * value)
	{
		___implementation_11 = value;
		Il2CppCodeGenWriteBarrier((&___implementation_11), value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_12() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___preserveWhitespace_12)); }
	inline bool get_preserveWhitespace_12() const { return ___preserveWhitespace_12; }
	inline bool* get_address_of_preserveWhitespace_12() { return &___preserveWhitespace_12; }
	inline void set_preserveWhitespace_12(bool value)
	{
		___preserveWhitespace_12 = value;
	}

	inline static int32_t get_offset_of_resolver_13() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___resolver_13)); }
	inline XmlResolver_t626023767 * get_resolver_13() const { return ___resolver_13; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_13() { return &___resolver_13; }
	inline void set_resolver_13(XmlResolver_t626023767 * value)
	{
		___resolver_13 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_13), value);
	}

	inline static int32_t get_offset_of_idTable_14() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___idTable_14)); }
	inline Hashtable_t1853889766 * get_idTable_14() const { return ___idTable_14; }
	inline Hashtable_t1853889766 ** get_address_of_idTable_14() { return &___idTable_14; }
	inline void set_idTable_14(Hashtable_t1853889766 * value)
	{
		___idTable_14 = value;
		Il2CppCodeGenWriteBarrier((&___idTable_14), value);
	}

	inline static int32_t get_offset_of_nameCache_15() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___nameCache_15)); }
	inline XmlNameEntryCache_t2890546907 * get_nameCache_15() const { return ___nameCache_15; }
	inline XmlNameEntryCache_t2890546907 ** get_address_of_nameCache_15() { return &___nameCache_15; }
	inline void set_nameCache_15(XmlNameEntryCache_t2890546907 * value)
	{
		___nameCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_15), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_16() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___lastLinkedChild_16)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_16() const { return ___lastLinkedChild_16; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_16() { return &___lastLinkedChild_16; }
	inline void set_lastLinkedChild_16(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_16 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_16), value);
	}

	inline static int32_t get_offset_of_schemaInfo_17() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___schemaInfo_17)); }
	inline RuntimeObject* get_schemaInfo_17() const { return ___schemaInfo_17; }
	inline RuntimeObject** get_address_of_schemaInfo_17() { return &___schemaInfo_17; }
	inline void set_schemaInfo_17(RuntimeObject* value)
	{
		___schemaInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_17), value);
	}

	inline static int32_t get_offset_of_loadMode_18() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___loadMode_18)); }
	inline bool get_loadMode_18() const { return ___loadMode_18; }
	inline bool* get_address_of_loadMode_18() { return &___loadMode_18; }
	inline void set_loadMode_18(bool value)
	{
		___loadMode_18 = value;
	}

	inline static int32_t get_offset_of_NodeChanged_19() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeChanged_19)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeChanged_19() const { return ___NodeChanged_19; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeChanged_19() { return &___NodeChanged_19; }
	inline void set_NodeChanged_19(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanged_19), value);
	}

	inline static int32_t get_offset_of_NodeChanging_20() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeChanging_20)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeChanging_20() const { return ___NodeChanging_20; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeChanging_20() { return &___NodeChanging_20; }
	inline void set_NodeChanging_20(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeChanging_20 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanging_20), value);
	}

	inline static int32_t get_offset_of_NodeInserted_21() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeInserted_21)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeInserted_21() const { return ___NodeInserted_21; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeInserted_21() { return &___NodeInserted_21; }
	inline void set_NodeInserted_21(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeInserted_21 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserted_21), value);
	}

	inline static int32_t get_offset_of_NodeInserting_22() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeInserting_22)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeInserting_22() const { return ___NodeInserting_22; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeInserting_22() { return &___NodeInserting_22; }
	inline void set_NodeInserting_22(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeInserting_22 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserting_22), value);
	}

	inline static int32_t get_offset_of_NodeRemoved_23() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeRemoved_23)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeRemoved_23() const { return ___NodeRemoved_23; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeRemoved_23() { return &___NodeRemoved_23; }
	inline void set_NodeRemoved_23(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeRemoved_23 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoved_23), value);
	}

	inline static int32_t get_offset_of_NodeRemoving_24() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeRemoving_24)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeRemoving_24() const { return ___NodeRemoving_24; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeRemoving_24() { return &___NodeRemoving_24; }
	inline void set_NodeRemoving_24(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeRemoving_24 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoving_24), value);
	}
};

struct XmlDocument_t2837193595_StaticFields
{
public:
	// System.Type[] System.Xml.XmlDocument::optimal_create_types
	TypeU5BU5D_t3940880105* ___optimal_create_types_6;

public:
	inline static int32_t get_offset_of_optimal_create_types_6() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595_StaticFields, ___optimal_create_types_6)); }
	inline TypeU5BU5D_t3940880105* get_optimal_create_types_6() const { return ___optimal_create_types_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_optimal_create_types_6() { return &___optimal_create_types_6; }
	inline void set_optimal_create_types_6(TypeU5BU5D_t3940880105* value)
	{
		___optimal_create_types_6 = value;
		Il2CppCodeGenWriteBarrier((&___optimal_create_types_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_T2837193595_H
#ifndef XMLENTITY_T3308518401_H
#define XMLENTITY_T3308518401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t3308518401  : public XmlNode_t3767805227
{
public:
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_6;
	// System.String System.Xml.XmlEntity::NDATA
	String_t* ___NDATA_7;
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_8;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_9;
	// System.String System.Xml.XmlEntity::baseUri
	String_t* ___baseUri_10;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_11;
	// System.Boolean System.Xml.XmlEntity::contentAlreadySet
	bool ___contentAlreadySet_12;

public:
	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_NDATA_7() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___NDATA_7)); }
	inline String_t* get_NDATA_7() const { return ___NDATA_7; }
	inline String_t** get_address_of_NDATA_7() { return &___NDATA_7; }
	inline void set_NDATA_7(String_t* value)
	{
		___NDATA_7 = value;
		Il2CppCodeGenWriteBarrier((&___NDATA_7), value);
	}

	inline static int32_t get_offset_of_publicId_8() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___publicId_8)); }
	inline String_t* get_publicId_8() const { return ___publicId_8; }
	inline String_t** get_address_of_publicId_8() { return &___publicId_8; }
	inline void set_publicId_8(String_t* value)
	{
		___publicId_8 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_8), value);
	}

	inline static int32_t get_offset_of_systemId_9() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___systemId_9)); }
	inline String_t* get_systemId_9() const { return ___systemId_9; }
	inline String_t** get_address_of_systemId_9() { return &___systemId_9; }
	inline void set_systemId_9(String_t* value)
	{
		___systemId_9 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_9), value);
	}

	inline static int32_t get_offset_of_baseUri_10() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___baseUri_10)); }
	inline String_t* get_baseUri_10() const { return ___baseUri_10; }
	inline String_t** get_address_of_baseUri_10() { return &___baseUri_10; }
	inline void set_baseUri_10(String_t* value)
	{
		___baseUri_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_10), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_11() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___lastLinkedChild_11)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_11() const { return ___lastLinkedChild_11; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_11() { return &___lastLinkedChild_11; }
	inline void set_lastLinkedChild_11(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_11 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_11), value);
	}

	inline static int32_t get_offset_of_contentAlreadySet_12() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___contentAlreadySet_12)); }
	inline bool get_contentAlreadySet_12() const { return ___contentAlreadySet_12; }
	inline bool* get_address_of_contentAlreadySet_12() { return &___contentAlreadySet_12; }
	inline void set_contentAlreadySet_12(bool value)
	{
		___contentAlreadySet_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITY_T3308518401_H
#ifndef XMLANYATTRIBUTEATTRIBUTE_T1449326428_H
#define XMLANYATTRIBUTEATTRIBUTE_T1449326428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyAttributeAttribute
struct  XmlAnyAttributeAttribute_t1449326428  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYATTRIBUTEATTRIBUTE_T1449326428_H
#ifndef XMLANYELEMENTATTRIBUTE_T4038919363_H
#define XMLANYELEMENTATTRIBUTE_T4038919363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyElementAttribute
struct  XmlAnyElementAttribute_t4038919363  : public Attribute_t861562559
{
public:
	// System.Int32 System.Xml.Serialization.XmlAnyElementAttribute::order
	int32_t ___order_0;

public:
	inline static int32_t get_offset_of_order_0() { return static_cast<int32_t>(offsetof(XmlAnyElementAttribute_t4038919363, ___order_0)); }
	inline int32_t get_order_0() const { return ___order_0; }
	inline int32_t* get_address_of_order_0() { return &___order_0; }
	inline void set_order_0(int32_t value)
	{
		___order_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYELEMENTATTRIBUTE_T4038919363_H
#ifndef XMLLINKEDNODE_T1437094927_H
#define XMLLINKEDNODE_T1437094927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1437094927  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::nextSibling
	XmlLinkedNode_t1437094927 * ___nextSibling_6;

public:
	inline static int32_t get_offset_of_nextSibling_6() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1437094927, ___nextSibling_6)); }
	inline XmlLinkedNode_t1437094927 * get_nextSibling_6() const { return ___nextSibling_6; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_nextSibling_6() { return &___nextSibling_6; }
	inline void set_nextSibling_6(XmlLinkedNode_t1437094927 * value)
	{
		___nextSibling_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextSibling_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T1437094927_H
#ifndef XMLINPUTSTREAM_T1691369434_H
#define XMLINPUTSTREAM_T1691369434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlInputStream
struct  XmlInputStream_t1691369434  : public Stream_t1273022909
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::enc
	Encoding_t1523322056 * ___enc_2;
	// System.IO.Stream System.Xml.XmlInputStream::stream
	Stream_t1273022909 * ___stream_3;
	// System.Byte[] System.Xml.XmlInputStream::buffer
	ByteU5BU5D_t4116647657* ___buffer_4;
	// System.Int32 System.Xml.XmlInputStream::bufLength
	int32_t ___bufLength_5;
	// System.Int32 System.Xml.XmlInputStream::bufPos
	int32_t ___bufPos_6;

public:
	inline static int32_t get_offset_of_enc_2() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___enc_2)); }
	inline Encoding_t1523322056 * get_enc_2() const { return ___enc_2; }
	inline Encoding_t1523322056 ** get_address_of_enc_2() { return &___enc_2; }
	inline void set_enc_2(Encoding_t1523322056 * value)
	{
		___enc_2 = value;
		Il2CppCodeGenWriteBarrier((&___enc_2), value);
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___stream_3)); }
	inline Stream_t1273022909 * get_stream_3() const { return ___stream_3; }
	inline Stream_t1273022909 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_t1273022909 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}

	inline static int32_t get_offset_of_buffer_4() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___buffer_4)); }
	inline ByteU5BU5D_t4116647657* get_buffer_4() const { return ___buffer_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_4() { return &___buffer_4; }
	inline void set_buffer_4(ByteU5BU5D_t4116647657* value)
	{
		___buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_4), value);
	}

	inline static int32_t get_offset_of_bufLength_5() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___bufLength_5)); }
	inline int32_t get_bufLength_5() const { return ___bufLength_5; }
	inline int32_t* get_address_of_bufLength_5() { return &___bufLength_5; }
	inline void set_bufLength_5(int32_t value)
	{
		___bufLength_5 = value;
	}

	inline static int32_t get_offset_of_bufPos_6() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___bufPos_6)); }
	inline int32_t get_bufPos_6() const { return ___bufPos_6; }
	inline int32_t* get_address_of_bufPos_6() { return &___bufPos_6; }
	inline void set_bufPos_6(int32_t value)
	{
		___bufPos_6 = value;
	}
};

struct XmlInputStream_t1691369434_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::StrictUTF8
	Encoding_t1523322056 * ___StrictUTF8_1;
	// System.Xml.XmlException System.Xml.XmlInputStream::encodingException
	XmlException_t1761730631 * ___encodingException_7;

public:
	inline static int32_t get_offset_of_StrictUTF8_1() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434_StaticFields, ___StrictUTF8_1)); }
	inline Encoding_t1523322056 * get_StrictUTF8_1() const { return ___StrictUTF8_1; }
	inline Encoding_t1523322056 ** get_address_of_StrictUTF8_1() { return &___StrictUTF8_1; }
	inline void set_StrictUTF8_1(Encoding_t1523322056 * value)
	{
		___StrictUTF8_1 = value;
		Il2CppCodeGenWriteBarrier((&___StrictUTF8_1), value);
	}

	inline static int32_t get_offset_of_encodingException_7() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434_StaticFields, ___encodingException_7)); }
	inline XmlException_t1761730631 * get_encodingException_7() const { return ___encodingException_7; }
	inline XmlException_t1761730631 ** get_address_of_encodingException_7() { return &___encodingException_7; }
	inline void set_encodingException_7(XmlException_t1761730631 * value)
	{
		___encodingException_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodingException_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLINPUTSTREAM_T1691369434_H
#ifndef NONBLOCKINGSTREAMREADER_T2495303928_H
#define NONBLOCKINGSTREAMREADER_T2495303928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NonBlockingStreamReader
struct  NonBlockingStreamReader_t2495303928  : public TextReader_t283511965
{
public:
	// System.Byte[] System.Xml.NonBlockingStreamReader::input_buffer
	ByteU5BU5D_t4116647657* ___input_buffer_1;
	// System.Char[] System.Xml.NonBlockingStreamReader::decoded_buffer
	CharU5BU5D_t3528271667* ___decoded_buffer_2;
	// System.Int32 System.Xml.NonBlockingStreamReader::decoded_count
	int32_t ___decoded_count_3;
	// System.Int32 System.Xml.NonBlockingStreamReader::pos
	int32_t ___pos_4;
	// System.Int32 System.Xml.NonBlockingStreamReader::buffer_size
	int32_t ___buffer_size_5;
	// System.Text.Encoding System.Xml.NonBlockingStreamReader::encoding
	Encoding_t1523322056 * ___encoding_6;
	// System.Text.Decoder System.Xml.NonBlockingStreamReader::decoder
	Decoder_t2204182725 * ___decoder_7;
	// System.IO.Stream System.Xml.NonBlockingStreamReader::base_stream
	Stream_t1273022909 * ___base_stream_8;
	// System.Boolean System.Xml.NonBlockingStreamReader::mayBlock
	bool ___mayBlock_9;
	// System.Text.StringBuilder System.Xml.NonBlockingStreamReader::line_builder
	StringBuilder_t * ___line_builder_10;
	// System.Boolean System.Xml.NonBlockingStreamReader::foundCR
	bool ___foundCR_11;

public:
	inline static int32_t get_offset_of_input_buffer_1() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___input_buffer_1)); }
	inline ByteU5BU5D_t4116647657* get_input_buffer_1() const { return ___input_buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_input_buffer_1() { return &___input_buffer_1; }
	inline void set_input_buffer_1(ByteU5BU5D_t4116647657* value)
	{
		___input_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_buffer_1), value);
	}

	inline static int32_t get_offset_of_decoded_buffer_2() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___decoded_buffer_2)); }
	inline CharU5BU5D_t3528271667* get_decoded_buffer_2() const { return ___decoded_buffer_2; }
	inline CharU5BU5D_t3528271667** get_address_of_decoded_buffer_2() { return &___decoded_buffer_2; }
	inline void set_decoded_buffer_2(CharU5BU5D_t3528271667* value)
	{
		___decoded_buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___decoded_buffer_2), value);
	}

	inline static int32_t get_offset_of_decoded_count_3() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___decoded_count_3)); }
	inline int32_t get_decoded_count_3() const { return ___decoded_count_3; }
	inline int32_t* get_address_of_decoded_count_3() { return &___decoded_count_3; }
	inline void set_decoded_count_3(int32_t value)
	{
		___decoded_count_3 = value;
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}

	inline static int32_t get_offset_of_buffer_size_5() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___buffer_size_5)); }
	inline int32_t get_buffer_size_5() const { return ___buffer_size_5; }
	inline int32_t* get_address_of_buffer_size_5() { return &___buffer_size_5; }
	inline void set_buffer_size_5(int32_t value)
	{
		___buffer_size_5 = value;
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___encoding_6)); }
	inline Encoding_t1523322056 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t1523322056 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t1523322056 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_decoder_7() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___decoder_7)); }
	inline Decoder_t2204182725 * get_decoder_7() const { return ___decoder_7; }
	inline Decoder_t2204182725 ** get_address_of_decoder_7() { return &___decoder_7; }
	inline void set_decoder_7(Decoder_t2204182725 * value)
	{
		___decoder_7 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_7), value);
	}

	inline static int32_t get_offset_of_base_stream_8() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___base_stream_8)); }
	inline Stream_t1273022909 * get_base_stream_8() const { return ___base_stream_8; }
	inline Stream_t1273022909 ** get_address_of_base_stream_8() { return &___base_stream_8; }
	inline void set_base_stream_8(Stream_t1273022909 * value)
	{
		___base_stream_8 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_8), value);
	}

	inline static int32_t get_offset_of_mayBlock_9() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___mayBlock_9)); }
	inline bool get_mayBlock_9() const { return ___mayBlock_9; }
	inline bool* get_address_of_mayBlock_9() { return &___mayBlock_9; }
	inline void set_mayBlock_9(bool value)
	{
		___mayBlock_9 = value;
	}

	inline static int32_t get_offset_of_line_builder_10() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___line_builder_10)); }
	inline StringBuilder_t * get_line_builder_10() const { return ___line_builder_10; }
	inline StringBuilder_t ** get_address_of_line_builder_10() { return &___line_builder_10; }
	inline void set_line_builder_10(StringBuilder_t * value)
	{
		___line_builder_10 = value;
		Il2CppCodeGenWriteBarrier((&___line_builder_10), value);
	}

	inline static int32_t get_offset_of_foundCR_11() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___foundCR_11)); }
	inline bool get_foundCR_11() const { return ___foundCR_11; }
	inline bool* get_address_of_foundCR_11() { return &___foundCR_11; }
	inline void set_foundCR_11(bool value)
	{
		___foundCR_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONBLOCKINGSTREAMREADER_T2495303928_H
#ifndef NSDECL_T3938094415_H
#define NSDECL_T3938094415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NsDecl
struct  NsDecl_t3938094415 
{
public:
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Uri
	String_t* ___Uri_1;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(NsDecl_t3938094415, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_Uri_1() { return static_cast<int32_t>(offsetof(NsDecl_t3938094415, ___Uri_1)); }
	inline String_t* get_Uri_1() const { return ___Uri_1; }
	inline String_t** get_address_of_Uri_1() { return &___Uri_1; }
	inline void set_Uri_1(String_t* value)
	{
		___Uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_t3938094415_marshaled_pinvoke
{
	char* ___Prefix_0;
	char* ___Uri_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_t3938094415_marshaled_com
{
	Il2CppChar* ___Prefix_0;
	Il2CppChar* ___Uri_1;
};
#endif // NSDECL_T3938094415_H
#ifndef XMLATTRIBUTEATTRIBUTE_T2511360870_H
#define XMLATTRIBUTEATTRIBUTE_T2511360870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeAttribute
struct  XmlAttributeAttribute_t2511360870  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::dataType
	String_t* ___dataType_1;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t2511360870, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeName_0), value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t2511360870, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEATTRIBUTE_T2511360870_H
#ifndef NSSCOPE_T3958624705_H
#define NSSCOPE_T3958624705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NsScope
struct  NsScope_t3958624705 
{
public:
	// System.Int32 System.Xml.XmlNamespaceManager/NsScope::DeclCount
	int32_t ___DeclCount_0;
	// System.String System.Xml.XmlNamespaceManager/NsScope::DefaultNamespace
	String_t* ___DefaultNamespace_1;

public:
	inline static int32_t get_offset_of_DeclCount_0() { return static_cast<int32_t>(offsetof(NsScope_t3958624705, ___DeclCount_0)); }
	inline int32_t get_DeclCount_0() const { return ___DeclCount_0; }
	inline int32_t* get_address_of_DeclCount_0() { return &___DeclCount_0; }
	inline void set_DeclCount_0(int32_t value)
	{
		___DeclCount_0 = value;
	}

	inline static int32_t get_offset_of_DefaultNamespace_1() { return static_cast<int32_t>(offsetof(NsScope_t3958624705, ___DefaultNamespace_1)); }
	inline String_t* get_DefaultNamespace_1() const { return ___DefaultNamespace_1; }
	inline String_t** get_address_of_DefaultNamespace_1() { return &___DefaultNamespace_1; }
	inline void set_DefaultNamespace_1(String_t* value)
	{
		___DefaultNamespace_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultNamespace_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_t3958624705_marshaled_pinvoke
{
	int32_t ___DeclCount_0;
	char* ___DefaultNamespace_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_t3958624705_marshaled_com
{
	int32_t ___DeclCount_0;
	Il2CppChar* ___DefaultNamespace_1;
};
#endif // NSSCOPE_T3958624705_H
#ifndef XMLELEMENTATTRIBUTE_T17472343_H
#define XMLELEMENTATTRIBUTE_T17472343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttribute
struct  XmlElementAttribute_t17472343  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_0;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_1;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t17472343, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t17472343, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t17472343, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTE_T17472343_H
#ifndef XMLNOTATION_T1476580686_H
#define XMLNOTATION_T1476580686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNotation
struct  XmlNotation_t1476580686  : public XmlNode_t3767805227
{
public:
	// System.String System.Xml.XmlNotation::localName
	String_t* ___localName_6;
	// System.String System.Xml.XmlNotation::publicId
	String_t* ___publicId_7;
	// System.String System.Xml.XmlNotation::systemId
	String_t* ___systemId_8;
	// System.String System.Xml.XmlNotation::prefix
	String_t* ___prefix_9;

public:
	inline static int32_t get_offset_of_localName_6() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___localName_6)); }
	inline String_t* get_localName_6() const { return ___localName_6; }
	inline String_t** get_address_of_localName_6() { return &___localName_6; }
	inline void set_localName_6(String_t* value)
	{
		___localName_6 = value;
		Il2CppCodeGenWriteBarrier((&___localName_6), value);
	}

	inline static int32_t get_offset_of_publicId_7() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___publicId_7)); }
	inline String_t* get_publicId_7() const { return ___publicId_7; }
	inline String_t** get_address_of_publicId_7() { return &___publicId_7; }
	inline void set_publicId_7(String_t* value)
	{
		___publicId_7 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_7), value);
	}

	inline static int32_t get_offset_of_systemId_8() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___systemId_8)); }
	inline String_t* get_systemId_8() const { return ___systemId_8; }
	inline String_t** get_address_of_systemId_8() { return &___systemId_8; }
	inline void set_systemId_8(String_t* value)
	{
		___systemId_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_8), value);
	}

	inline static int32_t get_offset_of_prefix_9() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___prefix_9)); }
	inline String_t* get_prefix_9() const { return ___prefix_9; }
	inline String_t** get_address_of_prefix_9() { return &___prefix_9; }
	inline void set_prefix_9(String_t* value)
	{
		___prefix_9 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNOTATION_T1476580686_H
#ifndef XMLATTRIBUTE_T1173852259_H
#define XMLATTRIBUTE_T1173852259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_t1173852259  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlNameEntry System.Xml.XmlAttribute::name
	XmlNameEntry_t1073099671 * ___name_6;
	// System.Boolean System.Xml.XmlAttribute::isDefault
	bool ___isDefault_7;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_8;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlAttribute::schemaInfo
	RuntimeObject* ___schemaInfo_9;

public:
	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___name_6)); }
	inline XmlNameEntry_t1073099671 * get_name_6() const { return ___name_6; }
	inline XmlNameEntry_t1073099671 ** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(XmlNameEntry_t1073099671 * value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_isDefault_7() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___isDefault_7)); }
	inline bool get_isDefault_7() const { return ___isDefault_7; }
	inline bool* get_address_of_isDefault_7() { return &___isDefault_7; }
	inline void set_isDefault_7(bool value)
	{
		___isDefault_7 = value;
	}

	inline static int32_t get_offset_of_lastLinkedChild_8() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___lastLinkedChild_8)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_8() const { return ___lastLinkedChild_8; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_8() { return &___lastLinkedChild_8; }
	inline void set_lastLinkedChild_8(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_8), value);
	}

	inline static int32_t get_offset_of_schemaInfo_9() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___schemaInfo_9)); }
	inline RuntimeObject* get_schemaInfo_9() const { return ___schemaInfo_9; }
	inline RuntimeObject** get_address_of_schemaInfo_9() { return &___schemaInfo_9; }
	inline void set_schemaInfo_9(RuntimeObject* value)
	{
		___schemaInfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_T1173852259_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DICTIONARYBASE_T52754249_H
#define DICTIONARYBASE_T52754249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase
struct  DictionaryBase_t52754249  : public List_1_t218596005
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYBASE_T52754249_H
#ifndef DTDNOTATIONDECLARATION_T3702682588_H
#define DTDNOTATIONDECLARATION_T3702682588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclaration
struct  DTDNotationDeclaration_t3702682588  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDNotationDeclaration::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDNotationDeclaration::localName
	String_t* ___localName_6;
	// System.String Mono.Xml.DTDNotationDeclaration::prefix
	String_t* ___prefix_7;
	// System.String Mono.Xml.DTDNotationDeclaration::publicId
	String_t* ___publicId_8;
	// System.String Mono.Xml.DTDNotationDeclaration::systemId
	String_t* ___systemId_9;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_localName_6() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___localName_6)); }
	inline String_t* get_localName_6() const { return ___localName_6; }
	inline String_t** get_address_of_localName_6() { return &___localName_6; }
	inline void set_localName_6(String_t* value)
	{
		___localName_6 = value;
		Il2CppCodeGenWriteBarrier((&___localName_6), value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___prefix_7)); }
	inline String_t* get_prefix_7() const { return ___prefix_7; }
	inline String_t** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(String_t* value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_7), value);
	}

	inline static int32_t get_offset_of_publicId_8() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___publicId_8)); }
	inline String_t* get_publicId_8() const { return ___publicId_8; }
	inline String_t** get_address_of_publicId_8() { return &___publicId_8; }
	inline void set_publicId_8(String_t* value)
	{
		___publicId_8 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_8), value);
	}

	inline static int32_t get_offset_of_systemId_9() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___systemId_9)); }
	inline String_t* get_systemId_9() const { return ___systemId_9; }
	inline String_t** get_address_of_systemId_9() { return &___systemId_9; }
	inline void set_systemId_9(String_t* value)
	{
		___systemId_9 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATION_T3702682588_H
#ifndef DTDENTITYBASE_T1228162861_H
#define DTDENTITYBASE_T1228162861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityBase
struct  DTDEntityBase_t1228162861  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDEntityBase::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDEntityBase::publicId
	String_t* ___publicId_6;
	// System.String Mono.Xml.DTDEntityBase::systemId
	String_t* ___systemId_7;
	// System.String Mono.Xml.DTDEntityBase::literalValue
	String_t* ___literalValue_8;
	// System.String Mono.Xml.DTDEntityBase::replacementText
	String_t* ___replacementText_9;
	// System.String Mono.Xml.DTDEntityBase::uriString
	String_t* ___uriString_10;
	// System.Uri Mono.Xml.DTDEntityBase::absUri
	Uri_t100236324 * ___absUri_11;
	// System.Boolean Mono.Xml.DTDEntityBase::isInvalid
	bool ___isInvalid_12;
	// System.Boolean Mono.Xml.DTDEntityBase::loadFailed
	bool ___loadFailed_13;
	// System.Xml.XmlResolver Mono.Xml.DTDEntityBase::resolver
	XmlResolver_t626023767 * ___resolver_14;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_literalValue_8() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___literalValue_8)); }
	inline String_t* get_literalValue_8() const { return ___literalValue_8; }
	inline String_t** get_address_of_literalValue_8() { return &___literalValue_8; }
	inline void set_literalValue_8(String_t* value)
	{
		___literalValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___literalValue_8), value);
	}

	inline static int32_t get_offset_of_replacementText_9() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___replacementText_9)); }
	inline String_t* get_replacementText_9() const { return ___replacementText_9; }
	inline String_t** get_address_of_replacementText_9() { return &___replacementText_9; }
	inline void set_replacementText_9(String_t* value)
	{
		___replacementText_9 = value;
		Il2CppCodeGenWriteBarrier((&___replacementText_9), value);
	}

	inline static int32_t get_offset_of_uriString_10() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___uriString_10)); }
	inline String_t* get_uriString_10() const { return ___uriString_10; }
	inline String_t** get_address_of_uriString_10() { return &___uriString_10; }
	inline void set_uriString_10(String_t* value)
	{
		___uriString_10 = value;
		Il2CppCodeGenWriteBarrier((&___uriString_10), value);
	}

	inline static int32_t get_offset_of_absUri_11() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___absUri_11)); }
	inline Uri_t100236324 * get_absUri_11() const { return ___absUri_11; }
	inline Uri_t100236324 ** get_address_of_absUri_11() { return &___absUri_11; }
	inline void set_absUri_11(Uri_t100236324 * value)
	{
		___absUri_11 = value;
		Il2CppCodeGenWriteBarrier((&___absUri_11), value);
	}

	inline static int32_t get_offset_of_isInvalid_12() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___isInvalid_12)); }
	inline bool get_isInvalid_12() const { return ___isInvalid_12; }
	inline bool* get_address_of_isInvalid_12() { return &___isInvalid_12; }
	inline void set_isInvalid_12(bool value)
	{
		___isInvalid_12 = value;
	}

	inline static int32_t get_offset_of_loadFailed_13() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___loadFailed_13)); }
	inline bool get_loadFailed_13() const { return ___loadFailed_13; }
	inline bool* get_address_of_loadFailed_13() { return &___loadFailed_13; }
	inline void set_loadFailed_13(bool value)
	{
		___loadFailed_13 = value;
	}

	inline static int32_t get_offset_of_resolver_14() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___resolver_14)); }
	inline XmlResolver_t626023767 * get_resolver_14() const { return ___resolver_14; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_14() { return &___resolver_14; }
	inline void set_resolver_14(XmlResolver_t626023767 * value)
	{
		___resolver_14 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYBASE_T1228162861_H
#ifndef DTDATTLISTDECLARATION_T3593159715_H
#define DTDATTLISTDECLARATION_T3593159715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclaration
struct  DTDAttListDeclaration_t3593159715  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDAttListDeclaration::name
	String_t* ___name_5;
	// System.Collections.Hashtable Mono.Xml.DTDAttListDeclaration::attributeOrders
	Hashtable_t1853889766 * ___attributeOrders_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttListDeclaration::attributes
	ArrayList_t2718874744 * ___attributes_7;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t3593159715, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_attributeOrders_6() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t3593159715, ___attributeOrders_6)); }
	inline Hashtable_t1853889766 * get_attributeOrders_6() const { return ___attributeOrders_6; }
	inline Hashtable_t1853889766 ** get_address_of_attributeOrders_6() { return &___attributeOrders_6; }
	inline void set_attributeOrders_6(Hashtable_t1853889766 * value)
	{
		___attributeOrders_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeOrders_6), value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t3593159715, ___attributes_7)); }
	inline ArrayList_t2718874744 * get_attributes_7() const { return ___attributes_7; }
	inline ArrayList_t2718874744 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(ArrayList_t2718874744 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATION_T3593159715_H
#ifndef DTDATTRIBUTEDEFINITION_T3434905422_H
#define DTDATTRIBUTEDEFINITION_T3434905422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeDefinition
struct  DTDAttributeDefinition_t3434905422  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDAttributeDefinition::name
	String_t* ___name_5;
	// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::datatype
	XmlSchemaDatatype_t322714710 * ___datatype_6;
	// System.String Mono.Xml.DTDAttributeDefinition::unresolvedDefault
	String_t* ___unresolvedDefault_7;
	// System.String Mono.Xml.DTDAttributeDefinition::resolvedDefaultValue
	String_t* ___resolvedDefaultValue_8;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_datatype_6() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___datatype_6)); }
	inline XmlSchemaDatatype_t322714710 * get_datatype_6() const { return ___datatype_6; }
	inline XmlSchemaDatatype_t322714710 ** get_address_of_datatype_6() { return &___datatype_6; }
	inline void set_datatype_6(XmlSchemaDatatype_t322714710 * value)
	{
		___datatype_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_6), value);
	}

	inline static int32_t get_offset_of_unresolvedDefault_7() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___unresolvedDefault_7)); }
	inline String_t* get_unresolvedDefault_7() const { return ___unresolvedDefault_7; }
	inline String_t** get_address_of_unresolvedDefault_7() { return &___unresolvedDefault_7; }
	inline void set_unresolvedDefault_7(String_t* value)
	{
		___unresolvedDefault_7 = value;
		Il2CppCodeGenWriteBarrier((&___unresolvedDefault_7), value);
	}

	inline static int32_t get_offset_of_resolvedDefaultValue_8() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___resolvedDefaultValue_8)); }
	inline String_t* get_resolvedDefaultValue_8() const { return ___resolvedDefaultValue_8; }
	inline String_t** get_address_of_resolvedDefaultValue_8() { return &___resolvedDefaultValue_8; }
	inline void set_resolvedDefaultValue_8(String_t* value)
	{
		___resolvedDefaultValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___resolvedDefaultValue_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEDEFINITION_T3434905422_H
#ifndef DTDELEMENTDECLARATION_T1830540991_H
#define DTDELEMENTDECLARATION_T1830540991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclaration
struct  DTDElementDeclaration_t1830540991  : public DTDNode_t858560093
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDElementDeclaration::root
	DTDObjectModel_t1729680289 * ___root_5;
	// Mono.Xml.DTDContentModel Mono.Xml.DTDElementDeclaration::contentModel
	DTDContentModel_t3264596611 * ___contentModel_6;
	// System.String Mono.Xml.DTDElementDeclaration::name
	String_t* ___name_7;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isEmpty
	bool ___isEmpty_8;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isAny
	bool ___isAny_9;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isMixedContent
	bool ___isMixedContent_10;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___root_5)); }
	inline DTDObjectModel_t1729680289 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1729680289 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_contentModel_6() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___contentModel_6)); }
	inline DTDContentModel_t3264596611 * get_contentModel_6() const { return ___contentModel_6; }
	inline DTDContentModel_t3264596611 ** get_address_of_contentModel_6() { return &___contentModel_6; }
	inline void set_contentModel_6(DTDContentModel_t3264596611 * value)
	{
		___contentModel_6 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_isEmpty_8() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___isEmpty_8)); }
	inline bool get_isEmpty_8() const { return ___isEmpty_8; }
	inline bool* get_address_of_isEmpty_8() { return &___isEmpty_8; }
	inline void set_isEmpty_8(bool value)
	{
		___isEmpty_8 = value;
	}

	inline static int32_t get_offset_of_isAny_9() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___isAny_9)); }
	inline bool get_isAny_9() const { return ___isAny_9; }
	inline bool* get_address_of_isAny_9() { return &___isAny_9; }
	inline void set_isAny_9(bool value)
	{
		___isAny_9 = value;
	}

	inline static int32_t get_offset_of_isMixedContent_10() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___isMixedContent_10)); }
	inline bool get_isMixedContent_10() const { return ___isMixedContent_10; }
	inline bool* get_address_of_isMixedContent_10() { return &___isMixedContent_10; }
	inline void set_isMixedContent_10(bool value)
	{
		___isMixedContent_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATION_T1830540991_H
#ifndef KEYVALUEPAIR_2_T3041488559_H
#define KEYVALUEPAIR_2_T3041488559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>
struct  KeyValuePair_2_t3041488559 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	DTDNode_t858560093 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3041488559, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3041488559, ___value_1)); }
	inline DTDNode_t858560093 * get_value_1() const { return ___value_1; }
	inline DTDNode_t858560093 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(DTDNode_t858560093 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3041488559_H
#ifndef NAMETABLE_T3178203267_H
#define NAMETABLE_T3178203267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_t3178203267  : public XmlNameTable_t71772148
{
public:
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t491982174* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NameTable_t3178203267, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_buckets_1() { return static_cast<int32_t>(offsetof(NameTable_t3178203267, ___buckets_1)); }
	inline EntryU5BU5D_t491982174* get_buckets_1() const { return ___buckets_1; }
	inline EntryU5BU5D_t491982174** get_address_of_buckets_1() { return &___buckets_1; }
	inline void set_buckets_1(EntryU5BU5D_t491982174* value)
	{
		___buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(NameTable_t3178203267, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_T3178203267_H
#ifndef XMLATTRIBUTECOLLECTION_T2316283784_H
#define XMLATTRIBUTECOLLECTION_T2316283784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_t2316283784  : public XmlNamedNodeMap_t2821286253
{
public:
	// System.Xml.XmlElement System.Xml.XmlAttributeCollection::ownerElement
	XmlElement_t561603118 * ___ownerElement_4;
	// System.Xml.XmlDocument System.Xml.XmlAttributeCollection::ownerDocument
	XmlDocument_t2837193595 * ___ownerDocument_5;

public:
	inline static int32_t get_offset_of_ownerElement_4() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t2316283784, ___ownerElement_4)); }
	inline XmlElement_t561603118 * get_ownerElement_4() const { return ___ownerElement_4; }
	inline XmlElement_t561603118 ** get_address_of_ownerElement_4() { return &___ownerElement_4; }
	inline void set_ownerElement_4(XmlElement_t561603118 * value)
	{
		___ownerElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElement_4), value);
	}

	inline static int32_t get_offset_of_ownerDocument_5() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t2316283784, ___ownerDocument_5)); }
	inline XmlDocument_t2837193595 * get_ownerDocument_5() const { return ___ownerDocument_5; }
	inline XmlDocument_t2837193595 ** get_address_of_ownerDocument_5() { return &___ownerDocument_5; }
	inline void set_ownerDocument_5(XmlDocument_t2837193595 * value)
	{
		___ownerDocument_5 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTECOLLECTION_T2316283784_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef XMLROOTATTRIBUTE_T2306097217_H
#define XMLROOTATTRIBUTE_T2306097217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlRootAttribute
struct  XmlRootAttribute_t2306097217  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlRootAttribute::elementName
	String_t* ___elementName_0;
	// System.Boolean System.Xml.Serialization.XmlRootAttribute::isNullable
	bool ___isNullable_1;
	// System.String System.Xml.Serialization.XmlRootAttribute::ns
	String_t* ___ns_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t2306097217, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_isNullable_1() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t2306097217, ___isNullable_1)); }
	inline bool get_isNullable_1() const { return ___isNullable_1; }
	inline bool* get_address_of_isNullable_1() { return &___isNullable_1; }
	inline void set_isNullable_1(bool value)
	{
		___isNullable_1 = value;
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t2306097217, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLROOTATTRIBUTE_T2306097217_H
#ifndef XMLSCHEMAPROVIDERATTRIBUTE_T3872582200_H
#define XMLSCHEMAPROVIDERATTRIBUTE_T3872582200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSchemaProviderAttribute
struct  XmlSchemaProviderAttribute_t3872582200  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlSchemaProviderAttribute::_methodName
	String_t* ____methodName_0;
	// System.Boolean System.Xml.Serialization.XmlSchemaProviderAttribute::_isAny
	bool ____isAny_1;

public:
	inline static int32_t get_offset_of__methodName_0() { return static_cast<int32_t>(offsetof(XmlSchemaProviderAttribute_t3872582200, ____methodName_0)); }
	inline String_t* get__methodName_0() const { return ____methodName_0; }
	inline String_t** get_address_of__methodName_0() { return &____methodName_0; }
	inline void set__methodName_0(String_t* value)
	{
		____methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&____methodName_0), value);
	}

	inline static int32_t get_offset_of__isAny_1() { return static_cast<int32_t>(offsetof(XmlSchemaProviderAttribute_t3872582200, ____isAny_1)); }
	inline bool get__isAny_1() const { return ____isAny_1; }
	inline bool* get_address_of__isAny_1() { return &____isAny_1; }
	inline void set__isAny_1(bool value)
	{
		____isAny_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPROVIDERATTRIBUTE_T3872582200_H
#ifndef XMLTEXTATTRIBUTE_T499390083_H
#define XMLTEXTATTRIBUTE_T499390083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTextAttribute
struct  XmlTextAttribute_t499390083  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTATTRIBUTE_T499390083_H
#ifndef XMLPROCESSINGINSTRUCTION_T425688976_H
#define XMLPROCESSINGINSTRUCTION_T425688976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlProcessingInstruction
struct  XmlProcessingInstruction_t425688976  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlProcessingInstruction::target
	String_t* ___target_7;
	// System.String System.Xml.XmlProcessingInstruction::data
	String_t* ___data_8;

public:
	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t425688976, ___target_7)); }
	inline String_t* get_target_7() const { return ___target_7; }
	inline String_t** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(String_t* value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t425688976, ___data_8)); }
	inline String_t* get_data_8() const { return ___data_8; }
	inline String_t** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(String_t* value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPROCESSINGINSTRUCTION_T425688976_H
#ifndef COMMANDSTATE_T1020432923_H
#define COMMANDSTATE_T1020432923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport/CommandState
struct  CommandState_t1020432923 
{
public:
	// System.Int32 System.Xml.XmlReaderBinarySupport/CommandState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CommandState_t1020432923, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDSTATE_T1020432923_H
#ifndef DATETIMESTYLES_T840957420_H
#define DATETIMESTYLES_T840957420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeStyles
struct  DateTimeStyles_t840957420 
{
public:
	// System.Int32 System.Globalization.DateTimeStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeStyles_t840957420, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMESTYLES_T840957420_H
#ifndef XMLNODECHANGEDACTION_T3227731597_H
#define XMLNODECHANGEDACTION_T3227731597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedAction
struct  XmlNodeChangedAction_t3227731597 
{
public:
	// System.Int32 System.Xml.XmlNodeChangedAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeChangedAction_t3227731597, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDACTION_T3227731597_H
#ifndef XMLNODETYPE_T1672767151_H
#define XMLNODETYPE_T1672767151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t1672767151 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_t1672767151, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T1672767151_H
#ifndef ENUMERATOR_T2107839882_H
#define ENUMERATOR_T2107839882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  Enumerator_t2107839882 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t218596005 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	KeyValuePair_2_t3041488559  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___l_0)); }
	inline List_1_t218596005 * get_l_0() const { return ___l_0; }
	inline List_1_t218596005 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t218596005 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___current_3)); }
	inline KeyValuePair_2_t3041488559  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3041488559 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3041488559  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2107839882_H
#ifndef XMLOUTPUTMETHOD_T2185361861_H
#define XMLOUTPUTMETHOD_T2185361861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_t2185361861 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlOutputMethod_t2185361861, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLOUTPUTMETHOD_T2185361861_H
#ifndef XMLSPACE_T3324193251_H
#define XMLSPACE_T3324193251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_t3324193251 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSpace_t3324193251, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_T3324193251_H
#ifndef WHITESPACEHANDLING_T784045650_H
#define WHITESPACEHANDLING_T784045650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t784045650 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t784045650, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T784045650_H
#ifndef READSTATE_T944984020_H
#define READSTATE_T944984020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_t944984020 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t944984020, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T944984020_H
#ifndef NEWLINEHANDLING_T850339274_H
#define NEWLINEHANDLING_T850339274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t850339274 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_t850339274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T850339274_H
#ifndef NAMESPACEHANDLING_T4087553436_H
#define NAMESPACEHANDLING_T4087553436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t4087553436 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t4087553436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T4087553436_H
#ifndef FORMATTING_T1232942836_H
#define FORMATTING_T1232942836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Formatting
struct  Formatting_t1232942836 
{
public:
	// System.Int32 System.Xml.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t1232942836, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T1232942836_H
#ifndef ENTITYHANDLING_T1047276436_H
#define ENTITYHANDLING_T1047276436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t1047276436 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityHandling_t1047276436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T1047276436_H
#ifndef CONFORMANCELEVEL_T3899847875_H
#define CONFORMANCELEVEL_T3899847875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t3899847875 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_t3899847875, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T3899847875_H
#ifndef DTDCOLLECTIONBASE_T3926218464_H
#define DTDCOLLECTIONBASE_T3926218464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDCollectionBase
struct  DTDCollectionBase_t3926218464  : public DictionaryBase_t52754249
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::root
	DTDObjectModel_t1729680289 * ___root_5;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDCollectionBase_t3926218464, ___root_5)); }
	inline DTDObjectModel_t1729680289 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1729680289 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCOLLECTIONBASE_T3926218464_H
#ifndef DTDENTITYDECLARATION_T811637416_H
#define DTDENTITYDECLARATION_T811637416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclaration
struct  DTDEntityDeclaration_t811637416  : public DTDEntityBase_t1228162861
{
public:
	// System.String Mono.Xml.DTDEntityDeclaration::entityValue
	String_t* ___entityValue_15;
	// System.String Mono.Xml.DTDEntityDeclaration::notationName
	String_t* ___notationName_16;
	// System.Collections.ArrayList Mono.Xml.DTDEntityDeclaration::ReferencingEntities
	ArrayList_t2718874744 * ___ReferencingEntities_17;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::scanned
	bool ___scanned_18;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::recursed
	bool ___recursed_19;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::hasExternalReference
	bool ___hasExternalReference_20;

public:
	inline static int32_t get_offset_of_entityValue_15() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___entityValue_15)); }
	inline String_t* get_entityValue_15() const { return ___entityValue_15; }
	inline String_t** get_address_of_entityValue_15() { return &___entityValue_15; }
	inline void set_entityValue_15(String_t* value)
	{
		___entityValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___entityValue_15), value);
	}

	inline static int32_t get_offset_of_notationName_16() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___notationName_16)); }
	inline String_t* get_notationName_16() const { return ___notationName_16; }
	inline String_t** get_address_of_notationName_16() { return &___notationName_16; }
	inline void set_notationName_16(String_t* value)
	{
		___notationName_16 = value;
		Il2CppCodeGenWriteBarrier((&___notationName_16), value);
	}

	inline static int32_t get_offset_of_ReferencingEntities_17() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___ReferencingEntities_17)); }
	inline ArrayList_t2718874744 * get_ReferencingEntities_17() const { return ___ReferencingEntities_17; }
	inline ArrayList_t2718874744 ** get_address_of_ReferencingEntities_17() { return &___ReferencingEntities_17; }
	inline void set_ReferencingEntities_17(ArrayList_t2718874744 * value)
	{
		___ReferencingEntities_17 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencingEntities_17), value);
	}

	inline static int32_t get_offset_of_scanned_18() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___scanned_18)); }
	inline bool get_scanned_18() const { return ___scanned_18; }
	inline bool* get_address_of_scanned_18() { return &___scanned_18; }
	inline void set_scanned_18(bool value)
	{
		___scanned_18 = value;
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_hasExternalReference_20() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___hasExternalReference_20)); }
	inline bool get_hasExternalReference_20() const { return ___hasExternalReference_20; }
	inline bool* get_address_of_hasExternalReference_20() { return &___hasExternalReference_20; }
	inline void set_hasExternalReference_20(bool value)
	{
		___hasExternalReference_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATION_T811637416_H
#ifndef DTDPARAMETERENTITYDECLARATION_T3796253422_H
#define DTDPARAMETERENTITYDECLARATION_T3796253422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclaration
struct  DTDParameterEntityDeclaration_t3796253422  : public DTDEntityBase_t1228162861
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATION_T3796253422_H
#ifndef DTDCONTENTORDERTYPE_T1195786655_H
#define DTDCONTENTORDERTYPE_T1195786655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentOrderType
struct  DTDContentOrderType_t1195786655 
{
public:
	// System.Int32 Mono.Xml.DTDContentOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDContentOrderType_t1195786655, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTORDERTYPE_T1195786655_H
#ifndef DTDOCCURENCE_T3140866896_H
#define DTDOCCURENCE_T3140866896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOccurence
struct  DTDOccurence_t3140866896 
{
public:
	// System.Int32 Mono.Xml.DTDOccurence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDOccurence_t3140866896, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOCCURENCE_T3140866896_H
#ifndef WRITESTATE_T3983380671_H
#define WRITESTATE_T3983380671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t3983380671 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t3983380671, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T3983380671_H
#ifndef XMLDECLARATION_T679870411_H
#define XMLDECLARATION_T679870411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t679870411  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_7;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_8;
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_9;

public:
	inline static int32_t get_offset_of_encoding_7() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___encoding_7)); }
	inline String_t* get_encoding_7() const { return ___encoding_7; }
	inline String_t** get_address_of_encoding_7() { return &___encoding_7; }
	inline void set_encoding_7(String_t* value)
	{
		___encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_7), value);
	}

	inline static int32_t get_offset_of_standalone_8() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___standalone_8)); }
	inline String_t* get_standalone_8() const { return ___standalone_8; }
	inline String_t** get_address_of_standalone_8() { return &___standalone_8; }
	inline void set_standalone_8(String_t* value)
	{
		___standalone_8 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_8), value);
	}

	inline static int32_t get_offset_of_version_9() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___version_9)); }
	inline String_t* get_version_9() const { return ___version_9; }
	inline String_t** get_address_of_version_9() { return &___version_9; }
	inline void set_version_9(String_t* value)
	{
		___version_9 = value;
		Il2CppCodeGenWriteBarrier((&___version_9), value);
	}
};

struct XmlDeclaration_t679870411_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDeclaration::<>f__switch$map4A
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4A_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4A_10() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411_StaticFields, ___U3CU3Ef__switchU24map4A_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4A_10() const { return ___U3CU3Ef__switchU24map4A_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4A_10() { return &___U3CU3Ef__switchU24map4A_10; }
	inline void set_U3CU3Ef__switchU24map4A_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4A_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4A_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATION_T679870411_H
#ifndef XMLDOCUMENTTYPE_T4112370061_H
#define XMLDOCUMENTTYPE_T4112370061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t4112370061  : public XmlLinkedNode_t1437094927
{
public:
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t2821286253 * ___entities_7;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t2821286253 * ___notations_8;
	// Mono.Xml.DTDObjectModel System.Xml.XmlDocumentType::dtd
	DTDObjectModel_t1729680289 * ___dtd_9;

public:
	inline static int32_t get_offset_of_entities_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___entities_7)); }
	inline XmlNamedNodeMap_t2821286253 * get_entities_7() const { return ___entities_7; }
	inline XmlNamedNodeMap_t2821286253 ** get_address_of_entities_7() { return &___entities_7; }
	inline void set_entities_7(XmlNamedNodeMap_t2821286253 * value)
	{
		___entities_7 = value;
		Il2CppCodeGenWriteBarrier((&___entities_7), value);
	}

	inline static int32_t get_offset_of_notations_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___notations_8)); }
	inline XmlNamedNodeMap_t2821286253 * get_notations_8() const { return ___notations_8; }
	inline XmlNamedNodeMap_t2821286253 ** get_address_of_notations_8() { return &___notations_8; }
	inline void set_notations_8(XmlNamedNodeMap_t2821286253 * value)
	{
		___notations_8 = value;
		Il2CppCodeGenWriteBarrier((&___notations_8), value);
	}

	inline static int32_t get_offset_of_dtd_9() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___dtd_9)); }
	inline DTDObjectModel_t1729680289 * get_dtd_9() const { return ___dtd_9; }
	inline DTDObjectModel_t1729680289 ** get_address_of_dtd_9() { return &___dtd_9; }
	inline void set_dtd_9(DTDObjectModel_t1729680289 * value)
	{
		___dtd_9 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPE_T4112370061_H
#ifndef XMLELEMENT_T561603118_H
#define XMLELEMENT_T561603118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t561603118  : public XmlLinkedNode_t1437094927
{
public:
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t2316283784 * ___attributes_7;
	// System.Xml.XmlNameEntry System.Xml.XmlElement::name
	XmlNameEntry_t1073099671 * ___name_8;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_9;
	// System.Boolean System.Xml.XmlElement::isNotEmpty
	bool ___isNotEmpty_10;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlElement::schemaInfo
	RuntimeObject* ___schemaInfo_11;

public:
	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___attributes_7)); }
	inline XmlAttributeCollection_t2316283784 * get_attributes_7() const { return ___attributes_7; }
	inline XmlAttributeCollection_t2316283784 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(XmlAttributeCollection_t2316283784 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_7), value);
	}

	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___name_8)); }
	inline XmlNameEntry_t1073099671 * get_name_8() const { return ___name_8; }
	inline XmlNameEntry_t1073099671 ** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(XmlNameEntry_t1073099671 * value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier((&___name_8), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_9() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___lastLinkedChild_9)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_9() const { return ___lastLinkedChild_9; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_9() { return &___lastLinkedChild_9; }
	inline void set_lastLinkedChild_9(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_9 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_9), value);
	}

	inline static int32_t get_offset_of_isNotEmpty_10() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___isNotEmpty_10)); }
	inline bool get_isNotEmpty_10() const { return ___isNotEmpty_10; }
	inline bool* get_address_of_isNotEmpty_10() { return &___isNotEmpty_10; }
	inline void set_isNotEmpty_10(bool value)
	{
		___isNotEmpty_10 = value;
	}

	inline static int32_t get_offset_of_schemaInfo_11() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___schemaInfo_11)); }
	inline RuntimeObject* get_schemaInfo_11() const { return ___schemaInfo_11; }
	inline RuntimeObject** get_address_of_schemaInfo_11() { return &___schemaInfo_11; }
	inline void set_schemaInfo_11(RuntimeObject* value)
	{
		___schemaInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENT_T561603118_H
#ifndef XMLENTITYREFERENCE_T1966808559_H
#define XMLENTITYREFERENCE_T1966808559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t1966808559  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlEntityReference::entityName
	String_t* ___entityName_7;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_8;

public:
	inline static int32_t get_offset_of_entityName_7() { return static_cast<int32_t>(offsetof(XmlEntityReference_t1966808559, ___entityName_7)); }
	inline String_t* get_entityName_7() const { return ___entityName_7; }
	inline String_t** get_address_of_entityName_7() { return &___entityName_7; }
	inline void set_entityName_7(String_t* value)
	{
		___entityName_7 = value;
		Il2CppCodeGenWriteBarrier((&___entityName_7), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_8() { return static_cast<int32_t>(offsetof(XmlEntityReference_t1966808559, ___lastLinkedChild_8)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_8() const { return ___lastLinkedChild_8; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_8() { return &___lastLinkedChild_8; }
	inline void set_lastLinkedChild_8(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITYREFERENCE_T1966808559_H
#ifndef XMLEXCEPTION_T1761730631_H
#define XMLEXCEPTION_T1761730631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlException
struct  XmlException_t1761730631  : public SystemException_t176217640
{
public:
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_12;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_13;
	// System.String System.Xml.XmlException::res
	String_t* ___res_14;
	// System.String[] System.Xml.XmlException::messages
	StringU5BU5D_t1281789340* ___messages_15;

public:
	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_linePosition_12() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___linePosition_12)); }
	inline int32_t get_linePosition_12() const { return ___linePosition_12; }
	inline int32_t* get_address_of_linePosition_12() { return &___linePosition_12; }
	inline void set_linePosition_12(int32_t value)
	{
		___linePosition_12 = value;
	}

	inline static int32_t get_offset_of_sourceUri_13() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___sourceUri_13)); }
	inline String_t* get_sourceUri_13() const { return ___sourceUri_13; }
	inline String_t** get_address_of_sourceUri_13() { return &___sourceUri_13; }
	inline void set_sourceUri_13(String_t* value)
	{
		___sourceUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_13), value);
	}

	inline static int32_t get_offset_of_res_14() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___res_14)); }
	inline String_t* get_res_14() const { return ___res_14; }
	inline String_t** get_address_of_res_14() { return &___res_14; }
	inline void set_res_14(String_t* value)
	{
		___res_14 = value;
		Il2CppCodeGenWriteBarrier((&___res_14), value);
	}

	inline static int32_t get_offset_of_messages_15() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___messages_15)); }
	inline StringU5BU5D_t1281789340* get_messages_15() const { return ___messages_15; }
	inline StringU5BU5D_t1281789340** get_address_of_messages_15() { return &___messages_15; }
	inline void set_messages_15(StringU5BU5D_t1281789340* value)
	{
		___messages_15 = value;
		Il2CppCodeGenWriteBarrier((&___messages_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEXCEPTION_T1761730631_H
#ifndef XMLSTREAMREADER_T727818754_H
#define XMLSTREAMREADER_T727818754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStreamReader
struct  XmlStreamReader_t727818754  : public NonBlockingStreamReader_t2495303928
{
public:
	// System.Xml.XmlInputStream System.Xml.XmlStreamReader::input
	XmlInputStream_t1691369434 * ___input_12;

public:
	inline static int32_t get_offset_of_input_12() { return static_cast<int32_t>(offsetof(XmlStreamReader_t727818754, ___input_12)); }
	inline XmlInputStream_t1691369434 * get_input_12() const { return ___input_12; }
	inline XmlInputStream_t1691369434 ** get_address_of_input_12() { return &___input_12; }
	inline void set_input_12(XmlInputStream_t1691369434 * value)
	{
		___input_12 = value;
		Il2CppCodeGenWriteBarrier((&___input_12), value);
	}
};

struct XmlStreamReader_t727818754_StaticFields
{
public:
	// System.Xml.XmlException System.Xml.XmlStreamReader::invalidDataException
	XmlException_t1761730631 * ___invalidDataException_13;

public:
	inline static int32_t get_offset_of_invalidDataException_13() { return static_cast<int32_t>(offsetof(XmlStreamReader_t727818754_StaticFields, ___invalidDataException_13)); }
	inline XmlException_t1761730631 * get_invalidDataException_13() const { return ___invalidDataException_13; }
	inline XmlException_t1761730631 ** get_address_of_invalidDataException_13() { return &___invalidDataException_13; }
	inline void set_invalidDataException_13(XmlException_t1761730631 * value)
	{
		___invalidDataException_13 = value;
		Il2CppCodeGenWriteBarrier((&___invalidDataException_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTREAMREADER_T727818754_H
#ifndef SCHEMATYPES_T1741406581_H
#define SCHEMATYPES_T1741406581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SchemaTypes
struct  SchemaTypes_t1741406581 
{
public:
	// System.Int32 System.Xml.Serialization.SchemaTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SchemaTypes_t1741406581, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMATYPES_T1741406581_H
#ifndef XMLDATETIMESERIALIZATIONMODE_T1214355817_H
#define XMLDATETIMESERIALIZATIONMODE_T1214355817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDateTimeSerializationMode
struct  XmlDateTimeSerializationMode_t1214355817 
{
public:
	// System.Int32 System.Xml.XmlDateTimeSerializationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlDateTimeSerializationMode_t1214355817, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATETIMESERIALIZATIONMODE_T1214355817_H
#ifndef XMLCHARACTERDATA_T1167807131_H
#define XMLCHARACTERDATA_T1167807131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t1167807131  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_7;

public:
	inline static int32_t get_offset_of_data_7() { return static_cast<int32_t>(offsetof(XmlCharacterData_t1167807131, ___data_7)); }
	inline String_t* get_data_7() const { return ___data_7; }
	inline String_t** get_address_of_data_7() { return &___data_7; }
	inline void set_data_7(String_t* value)
	{
		___data_7 = value;
		Il2CppCodeGenWriteBarrier((&___data_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T1167807131_H
#ifndef XMLCOMMENT_T2476947920_H
#define XMLCOMMENT_T2476947920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComment
struct  XmlComment_t2476947920  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMMENT_T2476947920_H
#ifndef DTDATTLISTDECLARATIONCOLLECTION_T2220366188_H
#define DTDATTLISTDECLARATIONCOLLECTION_T2220366188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclarationCollection
struct  DTDAttListDeclarationCollection_t2220366188  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATIONCOLLECTION_T2220366188_H
#ifndef DTDELEMENTDECLARATIONCOLLECTION_T222313714_H
#define DTDELEMENTDECLARATIONCOLLECTION_T222313714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclarationCollection
struct  DTDElementDeclarationCollection_t222313714  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATIONCOLLECTION_T222313714_H
#ifndef XMLPARSERCONTEXT_T2544895291_H
#define XMLPARSERCONTEXT_T2544895291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext
struct  XmlParserContext_t2544895291  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext::baseURI
	String_t* ___baseURI_0;
	// System.String System.Xml.XmlParserContext::docTypeName
	String_t* ___docTypeName_1;
	// System.Text.Encoding System.Xml.XmlParserContext::encoding
	Encoding_t1523322056 * ___encoding_2;
	// System.String System.Xml.XmlParserContext::internalSubset
	String_t* ___internalSubset_3;
	// System.Xml.XmlNamespaceManager System.Xml.XmlParserContext::namespaceManager
	XmlNamespaceManager_t418790500 * ___namespaceManager_4;
	// System.Xml.XmlNameTable System.Xml.XmlParserContext::nameTable
	XmlNameTable_t71772148 * ___nameTable_5;
	// System.String System.Xml.XmlParserContext::publicID
	String_t* ___publicID_6;
	// System.String System.Xml.XmlParserContext::systemID
	String_t* ___systemID_7;
	// System.String System.Xml.XmlParserContext::xmlLang
	String_t* ___xmlLang_8;
	// System.Xml.XmlSpace System.Xml.XmlParserContext::xmlSpace
	int32_t ___xmlSpace_9;
	// System.Collections.ArrayList System.Xml.XmlParserContext::contextItems
	ArrayList_t2718874744 * ___contextItems_10;
	// System.Int32 System.Xml.XmlParserContext::contextItemCount
	int32_t ___contextItemCount_11;
	// Mono.Xml.DTDObjectModel System.Xml.XmlParserContext::dtd
	DTDObjectModel_t1729680289 * ___dtd_12;

public:
	inline static int32_t get_offset_of_baseURI_0() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___baseURI_0)); }
	inline String_t* get_baseURI_0() const { return ___baseURI_0; }
	inline String_t** get_address_of_baseURI_0() { return &___baseURI_0; }
	inline void set_baseURI_0(String_t* value)
	{
		___baseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_0), value);
	}

	inline static int32_t get_offset_of_docTypeName_1() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___docTypeName_1)); }
	inline String_t* get_docTypeName_1() const { return ___docTypeName_1; }
	inline String_t** get_address_of_docTypeName_1() { return &___docTypeName_1; }
	inline void set_docTypeName_1(String_t* value)
	{
		___docTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeName_1), value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___encoding_2)); }
	inline Encoding_t1523322056 * get_encoding_2() const { return ___encoding_2; }
	inline Encoding_t1523322056 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Encoding_t1523322056 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}

	inline static int32_t get_offset_of_internalSubset_3() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___internalSubset_3)); }
	inline String_t* get_internalSubset_3() const { return ___internalSubset_3; }
	inline String_t** get_address_of_internalSubset_3() { return &___internalSubset_3; }
	inline void set_internalSubset_3(String_t* value)
	{
		___internalSubset_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubset_3), value);
	}

	inline static int32_t get_offset_of_namespaceManager_4() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___namespaceManager_4)); }
	inline XmlNamespaceManager_t418790500 * get_namespaceManager_4() const { return ___namespaceManager_4; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_namespaceManager_4() { return &___namespaceManager_4; }
	inline void set_namespaceManager_4(XmlNamespaceManager_t418790500 * value)
	{
		___namespaceManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_4), value);
	}

	inline static int32_t get_offset_of_nameTable_5() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___nameTable_5)); }
	inline XmlNameTable_t71772148 * get_nameTable_5() const { return ___nameTable_5; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_5() { return &___nameTable_5; }
	inline void set_nameTable_5(XmlNameTable_t71772148 * value)
	{
		___nameTable_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_5), value);
	}

	inline static int32_t get_offset_of_publicID_6() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___publicID_6)); }
	inline String_t* get_publicID_6() const { return ___publicID_6; }
	inline String_t** get_address_of_publicID_6() { return &___publicID_6; }
	inline void set_publicID_6(String_t* value)
	{
		___publicID_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicID_6), value);
	}

	inline static int32_t get_offset_of_systemID_7() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___systemID_7)); }
	inline String_t* get_systemID_7() const { return ___systemID_7; }
	inline String_t** get_address_of_systemID_7() { return &___systemID_7; }
	inline void set_systemID_7(String_t* value)
	{
		___systemID_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemID_7), value);
	}

	inline static int32_t get_offset_of_xmlLang_8() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___xmlLang_8)); }
	inline String_t* get_xmlLang_8() const { return ___xmlLang_8; }
	inline String_t** get_address_of_xmlLang_8() { return &___xmlLang_8; }
	inline void set_xmlLang_8(String_t* value)
	{
		___xmlLang_8 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_8), value);
	}

	inline static int32_t get_offset_of_xmlSpace_9() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___xmlSpace_9)); }
	inline int32_t get_xmlSpace_9() const { return ___xmlSpace_9; }
	inline int32_t* get_address_of_xmlSpace_9() { return &___xmlSpace_9; }
	inline void set_xmlSpace_9(int32_t value)
	{
		___xmlSpace_9 = value;
	}

	inline static int32_t get_offset_of_contextItems_10() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___contextItems_10)); }
	inline ArrayList_t2718874744 * get_contextItems_10() const { return ___contextItems_10; }
	inline ArrayList_t2718874744 ** get_address_of_contextItems_10() { return &___contextItems_10; }
	inline void set_contextItems_10(ArrayList_t2718874744 * value)
	{
		___contextItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___contextItems_10), value);
	}

	inline static int32_t get_offset_of_contextItemCount_11() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___contextItemCount_11)); }
	inline int32_t get_contextItemCount_11() const { return ___contextItemCount_11; }
	inline int32_t* get_address_of_contextItemCount_11() { return &___contextItemCount_11; }
	inline void set_contextItemCount_11(int32_t value)
	{
		___contextItemCount_11 = value;
	}

	inline static int32_t get_offset_of_dtd_12() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___dtd_12)); }
	inline DTDObjectModel_t1729680289 * get_dtd_12() const { return ___dtd_12; }
	inline DTDObjectModel_t1729680289 ** get_address_of_dtd_12() { return &___dtd_12; }
	inline void set_dtd_12(DTDObjectModel_t1729680289 * value)
	{
		___dtd_12 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERCONTEXT_T2544895291_H
#ifndef U3CU3EC__ITERATOR3_T2072931442_H
#define U3CU3EC__ITERATOR3_T2072931442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase/<>c__Iterator3
struct  U3CU3Ec__Iterator3_t2072931442  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>> Mono.Xml.DictionaryBase/<>c__Iterator3::<$s_431>__0
	Enumerator_t2107839882  ___U3CU24s_431U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::<p>__1
	KeyValuePair_2_t3041488559  ___U3CpU3E__1_1;
	// System.Int32 Mono.Xml.DictionaryBase/<>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::$current
	DTDNode_t858560093 * ___U24current_3;
	// Mono.Xml.DictionaryBase Mono.Xml.DictionaryBase/<>c__Iterator3::<>f__this
	DictionaryBase_t52754249 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_431U3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U3CU24s_431U3E__0_0)); }
	inline Enumerator_t2107839882  get_U3CU24s_431U3E__0_0() const { return ___U3CU24s_431U3E__0_0; }
	inline Enumerator_t2107839882 * get_address_of_U3CU24s_431U3E__0_0() { return &___U3CU24s_431U3E__0_0; }
	inline void set_U3CU24s_431U3E__0_0(Enumerator_t2107839882  value)
	{
		___U3CU24s_431U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U3CpU3E__1_1)); }
	inline KeyValuePair_2_t3041488559  get_U3CpU3E__1_1() const { return ___U3CpU3E__1_1; }
	inline KeyValuePair_2_t3041488559 * get_address_of_U3CpU3E__1_1() { return &___U3CpU3E__1_1; }
	inline void set_U3CpU3E__1_1(KeyValuePair_2_t3041488559  value)
	{
		___U3CpU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U24current_3)); }
	inline DTDNode_t858560093 * get_U24current_3() const { return ___U24current_3; }
	inline DTDNode_t858560093 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(DTDNode_t858560093 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U3CU3Ef__this_4)); }
	inline DictionaryBase_t52754249 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline DictionaryBase_t52754249 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(DictionaryBase_t52754249 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR3_T2072931442_H
#ifndef XMLCDATASECTION_T3267478366_H
#define XMLCDATASECTION_T3267478366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCDataSection
struct  XmlCDataSection_t3267478366  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCDATASECTION_T3267478366_H
#ifndef TYPEDATA_T476999220_H
#define TYPEDATA_T476999220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeData
struct  TypeData_t476999220  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.TypeData::type
	Type_t * ___type_0;
	// System.String System.Xml.Serialization.TypeData::elementName
	String_t* ___elementName_1;
	// System.Xml.Serialization.SchemaTypes System.Xml.Serialization.TypeData::sType
	int32_t ___sType_2;
	// System.Type System.Xml.Serialization.TypeData::listItemType
	Type_t * ___listItemType_3;
	// System.String System.Xml.Serialization.TypeData::typeName
	String_t* ___typeName_4;
	// System.String System.Xml.Serialization.TypeData::fullTypeName
	String_t* ___fullTypeName_5;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::listItemTypeData
	TypeData_t476999220 * ___listItemTypeData_6;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::mappedType
	TypeData_t476999220 * ___mappedType_7;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Serialization.TypeData::facet
	XmlSchemaPatternFacet_t3316004401 * ___facet_8;
	// System.Boolean System.Xml.Serialization.TypeData::hasPublicConstructor
	bool ___hasPublicConstructor_9;
	// System.Boolean System.Xml.Serialization.TypeData::nullableOverride
	bool ___nullableOverride_10;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_1), value);
	}

	inline static int32_t get_offset_of_sType_2() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___sType_2)); }
	inline int32_t get_sType_2() const { return ___sType_2; }
	inline int32_t* get_address_of_sType_2() { return &___sType_2; }
	inline void set_sType_2(int32_t value)
	{
		___sType_2 = value;
	}

	inline static int32_t get_offset_of_listItemType_3() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___listItemType_3)); }
	inline Type_t * get_listItemType_3() const { return ___listItemType_3; }
	inline Type_t ** get_address_of_listItemType_3() { return &___listItemType_3; }
	inline void set_listItemType_3(Type_t * value)
	{
		___listItemType_3 = value;
		Il2CppCodeGenWriteBarrier((&___listItemType_3), value);
	}

	inline static int32_t get_offset_of_typeName_4() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___typeName_4)); }
	inline String_t* get_typeName_4() const { return ___typeName_4; }
	inline String_t** get_address_of_typeName_4() { return &___typeName_4; }
	inline void set_typeName_4(String_t* value)
	{
		___typeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_4), value);
	}

	inline static int32_t get_offset_of_fullTypeName_5() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___fullTypeName_5)); }
	inline String_t* get_fullTypeName_5() const { return ___fullTypeName_5; }
	inline String_t** get_address_of_fullTypeName_5() { return &___fullTypeName_5; }
	inline void set_fullTypeName_5(String_t* value)
	{
		___fullTypeName_5 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_5), value);
	}

	inline static int32_t get_offset_of_listItemTypeData_6() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___listItemTypeData_6)); }
	inline TypeData_t476999220 * get_listItemTypeData_6() const { return ___listItemTypeData_6; }
	inline TypeData_t476999220 ** get_address_of_listItemTypeData_6() { return &___listItemTypeData_6; }
	inline void set_listItemTypeData_6(TypeData_t476999220 * value)
	{
		___listItemTypeData_6 = value;
		Il2CppCodeGenWriteBarrier((&___listItemTypeData_6), value);
	}

	inline static int32_t get_offset_of_mappedType_7() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___mappedType_7)); }
	inline TypeData_t476999220 * get_mappedType_7() const { return ___mappedType_7; }
	inline TypeData_t476999220 ** get_address_of_mappedType_7() { return &___mappedType_7; }
	inline void set_mappedType_7(TypeData_t476999220 * value)
	{
		___mappedType_7 = value;
		Il2CppCodeGenWriteBarrier((&___mappedType_7), value);
	}

	inline static int32_t get_offset_of_facet_8() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___facet_8)); }
	inline XmlSchemaPatternFacet_t3316004401 * get_facet_8() const { return ___facet_8; }
	inline XmlSchemaPatternFacet_t3316004401 ** get_address_of_facet_8() { return &___facet_8; }
	inline void set_facet_8(XmlSchemaPatternFacet_t3316004401 * value)
	{
		___facet_8 = value;
		Il2CppCodeGenWriteBarrier((&___facet_8), value);
	}

	inline static int32_t get_offset_of_hasPublicConstructor_9() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___hasPublicConstructor_9)); }
	inline bool get_hasPublicConstructor_9() const { return ___hasPublicConstructor_9; }
	inline bool* get_address_of_hasPublicConstructor_9() { return &___hasPublicConstructor_9; }
	inline void set_hasPublicConstructor_9(bool value)
	{
		___hasPublicConstructor_9 = value;
	}

	inline static int32_t get_offset_of_nullableOverride_10() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___nullableOverride_10)); }
	inline bool get_nullableOverride_10() const { return ___nullableOverride_10; }
	inline bool* get_address_of_nullableOverride_10() { return &___nullableOverride_10; }
	inline void set_nullableOverride_10(bool value)
	{
		___nullableOverride_10 = value;
	}
};

struct TypeData_t476999220_StaticFields
{
public:
	// System.String[] System.Xml.Serialization.TypeData::keywords
	StringU5BU5D_t1281789340* ___keywords_11;

public:
	inline static int32_t get_offset_of_keywords_11() { return static_cast<int32_t>(offsetof(TypeData_t476999220_StaticFields, ___keywords_11)); }
	inline StringU5BU5D_t1281789340* get_keywords_11() const { return ___keywords_11; }
	inline StringU5BU5D_t1281789340** get_address_of_keywords_11() { return &___keywords_11; }
	inline void set_keywords_11(StringU5BU5D_t1281789340* value)
	{
		___keywords_11 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDATA_T476999220_H
#ifndef DTDENTITYDECLARATIONCOLLECTION_T2250844513_H
#define DTDENTITYDECLARATIONCOLLECTION_T2250844513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclarationCollection
struct  DTDEntityDeclarationCollection_t2250844513  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATIONCOLLECTION_T2250844513_H
#ifndef XMLNODECHANGEDEVENTARGS_T2486095928_H
#define XMLNODECHANGEDEVENTARGS_T2486095928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventArgs
struct  XmlNodeChangedEventArgs_t2486095928  : public EventArgs_t3591816995
{
public:
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_oldParent
	XmlNode_t3767805227 * ____oldParent_1;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_newParent
	XmlNode_t3767805227 * ____newParent_2;
	// System.Xml.XmlNodeChangedAction System.Xml.XmlNodeChangedEventArgs::_action
	int32_t ____action_3;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_node
	XmlNode_t3767805227 * ____node_4;
	// System.String System.Xml.XmlNodeChangedEventArgs::_oldValue
	String_t* ____oldValue_5;
	// System.String System.Xml.XmlNodeChangedEventArgs::_newValue
	String_t* ____newValue_6;

public:
	inline static int32_t get_offset_of__oldParent_1() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____oldParent_1)); }
	inline XmlNode_t3767805227 * get__oldParent_1() const { return ____oldParent_1; }
	inline XmlNode_t3767805227 ** get_address_of__oldParent_1() { return &____oldParent_1; }
	inline void set__oldParent_1(XmlNode_t3767805227 * value)
	{
		____oldParent_1 = value;
		Il2CppCodeGenWriteBarrier((&____oldParent_1), value);
	}

	inline static int32_t get_offset_of__newParent_2() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____newParent_2)); }
	inline XmlNode_t3767805227 * get__newParent_2() const { return ____newParent_2; }
	inline XmlNode_t3767805227 ** get_address_of__newParent_2() { return &____newParent_2; }
	inline void set__newParent_2(XmlNode_t3767805227 * value)
	{
		____newParent_2 = value;
		Il2CppCodeGenWriteBarrier((&____newParent_2), value);
	}

	inline static int32_t get_offset_of__action_3() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____action_3)); }
	inline int32_t get__action_3() const { return ____action_3; }
	inline int32_t* get_address_of__action_3() { return &____action_3; }
	inline void set__action_3(int32_t value)
	{
		____action_3 = value;
	}

	inline static int32_t get_offset_of__node_4() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____node_4)); }
	inline XmlNode_t3767805227 * get__node_4() const { return ____node_4; }
	inline XmlNode_t3767805227 ** get_address_of__node_4() { return &____node_4; }
	inline void set__node_4(XmlNode_t3767805227 * value)
	{
		____node_4 = value;
		Il2CppCodeGenWriteBarrier((&____node_4), value);
	}

	inline static int32_t get_offset_of__oldValue_5() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____oldValue_5)); }
	inline String_t* get__oldValue_5() const { return ____oldValue_5; }
	inline String_t** get_address_of__oldValue_5() { return &____oldValue_5; }
	inline void set__oldValue_5(String_t* value)
	{
		____oldValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____oldValue_5), value);
	}

	inline static int32_t get_offset_of__newValue_6() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____newValue_6)); }
	inline String_t* get__newValue_6() const { return ____newValue_6; }
	inline String_t** get_address_of__newValue_6() { return &____newValue_6; }
	inline void set__newValue_6(String_t* value)
	{
		____newValue_6 = value;
		Il2CppCodeGenWriteBarrier((&____newValue_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTARGS_T2486095928_H
#ifndef XMLNODEREADERIMPL_T2501602067_H
#define XMLNODEREADERIMPL_T2501602067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReaderImpl
struct  XmlNodeReaderImpl_t2501602067  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNodeReaderImpl::document
	XmlDocument_t2837193595 * ___document_3;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderImpl::startNode
	XmlNode_t3767805227 * ___startNode_4;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderImpl::current
	XmlNode_t3767805227 * ___current_5;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderImpl::ownerLinkedNode
	XmlNode_t3767805227 * ___ownerLinkedNode_6;
	// System.Xml.ReadState System.Xml.XmlNodeReaderImpl::state
	int32_t ___state_7;
	// System.Int32 System.Xml.XmlNodeReaderImpl::depth
	int32_t ___depth_8;
	// System.Boolean System.Xml.XmlNodeReaderImpl::isEndElement
	bool ___isEndElement_9;
	// System.Boolean System.Xml.XmlNodeReaderImpl::ignoreStartNode
	bool ___ignoreStartNode_10;

public:
	inline static int32_t get_offset_of_document_3() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___document_3)); }
	inline XmlDocument_t2837193595 * get_document_3() const { return ___document_3; }
	inline XmlDocument_t2837193595 ** get_address_of_document_3() { return &___document_3; }
	inline void set_document_3(XmlDocument_t2837193595 * value)
	{
		___document_3 = value;
		Il2CppCodeGenWriteBarrier((&___document_3), value);
	}

	inline static int32_t get_offset_of_startNode_4() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___startNode_4)); }
	inline XmlNode_t3767805227 * get_startNode_4() const { return ___startNode_4; }
	inline XmlNode_t3767805227 ** get_address_of_startNode_4() { return &___startNode_4; }
	inline void set_startNode_4(XmlNode_t3767805227 * value)
	{
		___startNode_4 = value;
		Il2CppCodeGenWriteBarrier((&___startNode_4), value);
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___current_5)); }
	inline XmlNode_t3767805227 * get_current_5() const { return ___current_5; }
	inline XmlNode_t3767805227 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(XmlNode_t3767805227 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_ownerLinkedNode_6() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___ownerLinkedNode_6)); }
	inline XmlNode_t3767805227 * get_ownerLinkedNode_6() const { return ___ownerLinkedNode_6; }
	inline XmlNode_t3767805227 ** get_address_of_ownerLinkedNode_6() { return &___ownerLinkedNode_6; }
	inline void set_ownerLinkedNode_6(XmlNode_t3767805227 * value)
	{
		___ownerLinkedNode_6 = value;
		Il2CppCodeGenWriteBarrier((&___ownerLinkedNode_6), value);
	}

	inline static int32_t get_offset_of_state_7() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___state_7)); }
	inline int32_t get_state_7() const { return ___state_7; }
	inline int32_t* get_address_of_state_7() { return &___state_7; }
	inline void set_state_7(int32_t value)
	{
		___state_7 = value;
	}

	inline static int32_t get_offset_of_depth_8() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___depth_8)); }
	inline int32_t get_depth_8() const { return ___depth_8; }
	inline int32_t* get_address_of_depth_8() { return &___depth_8; }
	inline void set_depth_8(int32_t value)
	{
		___depth_8 = value;
	}

	inline static int32_t get_offset_of_isEndElement_9() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___isEndElement_9)); }
	inline bool get_isEndElement_9() const { return ___isEndElement_9; }
	inline bool* get_address_of_isEndElement_9() { return &___isEndElement_9; }
	inline void set_isEndElement_9(bool value)
	{
		___isEndElement_9 = value;
	}

	inline static int32_t get_offset_of_ignoreStartNode_10() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067, ___ignoreStartNode_10)); }
	inline bool get_ignoreStartNode_10() const { return ___ignoreStartNode_10; }
	inline bool* get_address_of_ignoreStartNode_10() { return &___ignoreStartNode_10; }
	inline void set_ignoreStartNode_10(bool value)
	{
		___ignoreStartNode_10 = value;
	}
};

struct XmlNodeReaderImpl_t2501602067_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNodeReaderImpl::<>f__switch$map4D
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4D_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNodeReaderImpl::<>f__switch$map4E
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4E_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNodeReaderImpl::<>f__switch$map4F
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4F_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4D_11() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067_StaticFields, ___U3CU3Ef__switchU24map4D_11)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4D_11() const { return ___U3CU3Ef__switchU24map4D_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4D_11() { return &___U3CU3Ef__switchU24map4D_11; }
	inline void set_U3CU3Ef__switchU24map4D_11(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4D_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4D_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4E_12() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067_StaticFields, ___U3CU3Ef__switchU24map4E_12)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4E_12() const { return ___U3CU3Ef__switchU24map4E_12; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4E_12() { return &___U3CU3Ef__switchU24map4E_12; }
	inline void set_U3CU3Ef__switchU24map4E_12(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4E_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4E_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4F_13() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t2501602067_StaticFields, ___U3CU3Ef__switchU24map4F_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4F_13() const { return ___U3CU3Ef__switchU24map4F_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4F_13() { return &___U3CU3Ef__switchU24map4F_13; }
	inline void set_U3CU3Ef__switchU24map4F_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4F_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4F_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEREADERIMPL_T2501602067_H
#ifndef XMLREADERBINARYSUPPORT_T1809665003_H
#define XMLREADERBINARYSUPPORT_T1809665003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport
struct  XmlReaderBinarySupport_t1809665003  : public RuntimeObject
{
public:
	// System.Xml.XmlReader System.Xml.XmlReaderBinarySupport::reader
	XmlReader_t3121518892 * ___reader_0;
	// System.Int32 System.Xml.XmlReaderBinarySupport::base64CacheStartsAt
	int32_t ___base64CacheStartsAt_1;
	// System.Xml.XmlReaderBinarySupport/CommandState System.Xml.XmlReaderBinarySupport::state
	int32_t ___state_2;
	// System.Boolean System.Xml.XmlReaderBinarySupport::hasCache
	bool ___hasCache_3;
	// System.Boolean System.Xml.XmlReaderBinarySupport::dontReset
	bool ___dontReset_4;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___reader_0)); }
	inline XmlReader_t3121518892 * get_reader_0() const { return ___reader_0; }
	inline XmlReader_t3121518892 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_t3121518892 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_base64CacheStartsAt_1() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___base64CacheStartsAt_1)); }
	inline int32_t get_base64CacheStartsAt_1() const { return ___base64CacheStartsAt_1; }
	inline int32_t* get_address_of_base64CacheStartsAt_1() { return &___base64CacheStartsAt_1; }
	inline void set_base64CacheStartsAt_1(int32_t value)
	{
		___base64CacheStartsAt_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_hasCache_3() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___hasCache_3)); }
	inline bool get_hasCache_3() const { return ___hasCache_3; }
	inline bool* get_address_of_hasCache_3() { return &___hasCache_3; }
	inline void set_hasCache_3(bool value)
	{
		___hasCache_3 = value;
	}

	inline static int32_t get_offset_of_dontReset_4() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___dontReset_4)); }
	inline bool get_dontReset_4() const { return ___dontReset_4; }
	inline bool* get_address_of_dontReset_4() { return &___dontReset_4; }
	inline void set_dontReset_4(bool value)
	{
		___dontReset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERBINARYSUPPORT_T1809665003_H
#ifndef CONTEXTITEM_T3112052795_H
#define CONTEXTITEM_T3112052795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext/ContextItem
struct  ContextItem_t3112052795  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext/ContextItem::BaseURI
	String_t* ___BaseURI_0;
	// System.String System.Xml.XmlParserContext/ContextItem::XmlLang
	String_t* ___XmlLang_1;
	// System.Xml.XmlSpace System.Xml.XmlParserContext/ContextItem::XmlSpace
	int32_t ___XmlSpace_2;

public:
	inline static int32_t get_offset_of_BaseURI_0() { return static_cast<int32_t>(offsetof(ContextItem_t3112052795, ___BaseURI_0)); }
	inline String_t* get_BaseURI_0() const { return ___BaseURI_0; }
	inline String_t** get_address_of_BaseURI_0() { return &___BaseURI_0; }
	inline void set_BaseURI_0(String_t* value)
	{
		___BaseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURI_0), value);
	}

	inline static int32_t get_offset_of_XmlLang_1() { return static_cast<int32_t>(offsetof(ContextItem_t3112052795, ___XmlLang_1)); }
	inline String_t* get_XmlLang_1() const { return ___XmlLang_1; }
	inline String_t** get_address_of_XmlLang_1() { return &___XmlLang_1; }
	inline void set_XmlLang_1(String_t* value)
	{
		___XmlLang_1 = value;
		Il2CppCodeGenWriteBarrier((&___XmlLang_1), value);
	}

	inline static int32_t get_offset_of_XmlSpace_2() { return static_cast<int32_t>(offsetof(ContextItem_t3112052795, ___XmlSpace_2)); }
	inline int32_t get_XmlSpace_2() const { return ___XmlSpace_2; }
	inline int32_t* get_address_of_XmlSpace_2() { return &___XmlSpace_2; }
	inline void set_XmlSpace_2(int32_t value)
	{
		___XmlSpace_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTITEM_T3112052795_H
#ifndef XMLCONVERT_T1981561327_H
#define XMLCONVERT_T1981561327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t1981561327  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t1981561327_StaticFields
{
public:
	// System.String[] System.Xml.XmlConvert::datetimeFormats
	StringU5BU5D_t1281789340* ___datetimeFormats_0;
	// System.String[] System.Xml.XmlConvert::defaultDateTimeFormats
	StringU5BU5D_t1281789340* ___defaultDateTimeFormats_1;
	// System.String[] System.Xml.XmlConvert::roundtripDateTimeFormats
	StringU5BU5D_t1281789340* ___roundtripDateTimeFormats_2;
	// System.String[] System.Xml.XmlConvert::localDateTimeFormats
	StringU5BU5D_t1281789340* ___localDateTimeFormats_3;
	// System.String[] System.Xml.XmlConvert::utcDateTimeFormats
	StringU5BU5D_t1281789340* ___utcDateTimeFormats_4;
	// System.String[] System.Xml.XmlConvert::unspecifiedDateTimeFormats
	StringU5BU5D_t1281789340* ___unspecifiedDateTimeFormats_5;
	// System.Globalization.DateTimeStyles System.Xml.XmlConvert::_defaultStyle
	int32_t ____defaultStyle_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlConvert::<>f__switch$map49
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map49_7;

public:
	inline static int32_t get_offset_of_datetimeFormats_0() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___datetimeFormats_0)); }
	inline StringU5BU5D_t1281789340* get_datetimeFormats_0() const { return ___datetimeFormats_0; }
	inline StringU5BU5D_t1281789340** get_address_of_datetimeFormats_0() { return &___datetimeFormats_0; }
	inline void set_datetimeFormats_0(StringU5BU5D_t1281789340* value)
	{
		___datetimeFormats_0 = value;
		Il2CppCodeGenWriteBarrier((&___datetimeFormats_0), value);
	}

	inline static int32_t get_offset_of_defaultDateTimeFormats_1() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___defaultDateTimeFormats_1)); }
	inline StringU5BU5D_t1281789340* get_defaultDateTimeFormats_1() const { return ___defaultDateTimeFormats_1; }
	inline StringU5BU5D_t1281789340** get_address_of_defaultDateTimeFormats_1() { return &___defaultDateTimeFormats_1; }
	inline void set_defaultDateTimeFormats_1(StringU5BU5D_t1281789340* value)
	{
		___defaultDateTimeFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDateTimeFormats_1), value);
	}

	inline static int32_t get_offset_of_roundtripDateTimeFormats_2() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___roundtripDateTimeFormats_2)); }
	inline StringU5BU5D_t1281789340* get_roundtripDateTimeFormats_2() const { return ___roundtripDateTimeFormats_2; }
	inline StringU5BU5D_t1281789340** get_address_of_roundtripDateTimeFormats_2() { return &___roundtripDateTimeFormats_2; }
	inline void set_roundtripDateTimeFormats_2(StringU5BU5D_t1281789340* value)
	{
		___roundtripDateTimeFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___roundtripDateTimeFormats_2), value);
	}

	inline static int32_t get_offset_of_localDateTimeFormats_3() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___localDateTimeFormats_3)); }
	inline StringU5BU5D_t1281789340* get_localDateTimeFormats_3() const { return ___localDateTimeFormats_3; }
	inline StringU5BU5D_t1281789340** get_address_of_localDateTimeFormats_3() { return &___localDateTimeFormats_3; }
	inline void set_localDateTimeFormats_3(StringU5BU5D_t1281789340* value)
	{
		___localDateTimeFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___localDateTimeFormats_3), value);
	}

	inline static int32_t get_offset_of_utcDateTimeFormats_4() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___utcDateTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_utcDateTimeFormats_4() const { return ___utcDateTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_utcDateTimeFormats_4() { return &___utcDateTimeFormats_4; }
	inline void set_utcDateTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___utcDateTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___utcDateTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_unspecifiedDateTimeFormats_5() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___unspecifiedDateTimeFormats_5)); }
	inline StringU5BU5D_t1281789340* get_unspecifiedDateTimeFormats_5() const { return ___unspecifiedDateTimeFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_unspecifiedDateTimeFormats_5() { return &___unspecifiedDateTimeFormats_5; }
	inline void set_unspecifiedDateTimeFormats_5(StringU5BU5D_t1281789340* value)
	{
		___unspecifiedDateTimeFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___unspecifiedDateTimeFormats_5), value);
	}

	inline static int32_t get_offset_of__defaultStyle_6() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ____defaultStyle_6)); }
	inline int32_t get__defaultStyle_6() const { return ____defaultStyle_6; }
	inline int32_t* get_address_of__defaultStyle_6() { return &____defaultStyle_6; }
	inline void set__defaultStyle_6(int32_t value)
	{
		____defaultStyle_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map49_7() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___U3CU3Ef__switchU24map49_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map49_7() const { return ___U3CU3Ef__switchU24map49_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map49_7() { return &___U3CU3Ef__switchU24map49_7; }
	inline void set_U3CU3Ef__switchU24map49_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map49_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map49_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONVERT_T1981561327_H
#ifndef DTDNOTATIONDECLARATIONCOLLECTION_T959292105_H
#define DTDNOTATIONDECLARATIONCOLLECTION_T959292105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclarationCollection
struct  DTDNotationDeclarationCollection_t959292105  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATIONCOLLECTION_T959292105_H
#ifndef DTDCONTENTMODEL_T3264596611_H
#define DTDCONTENTMODEL_T3264596611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModel
struct  DTDContentModel_t3264596611  : public DTDNode_t858560093
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDContentModel::root
	DTDObjectModel_t1729680289 * ___root_5;
	// System.String Mono.Xml.DTDContentModel::ownerElementName
	String_t* ___ownerElementName_6;
	// System.String Mono.Xml.DTDContentModel::elementName
	String_t* ___elementName_7;
	// Mono.Xml.DTDContentOrderType Mono.Xml.DTDContentModel::orderType
	int32_t ___orderType_8;
	// Mono.Xml.DTDContentModelCollection Mono.Xml.DTDContentModel::childModels
	DTDContentModelCollection_t2798820000 * ___childModels_9;
	// Mono.Xml.DTDOccurence Mono.Xml.DTDContentModel::occurence
	int32_t ___occurence_10;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___root_5)); }
	inline DTDObjectModel_t1729680289 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1729680289 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_ownerElementName_6() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___ownerElementName_6)); }
	inline String_t* get_ownerElementName_6() const { return ___ownerElementName_6; }
	inline String_t** get_address_of_ownerElementName_6() { return &___ownerElementName_6; }
	inline void set_ownerElementName_6(String_t* value)
	{
		___ownerElementName_6 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElementName_6), value);
	}

	inline static int32_t get_offset_of_elementName_7() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___elementName_7)); }
	inline String_t* get_elementName_7() const { return ___elementName_7; }
	inline String_t** get_address_of_elementName_7() { return &___elementName_7; }
	inline void set_elementName_7(String_t* value)
	{
		___elementName_7 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_7), value);
	}

	inline static int32_t get_offset_of_orderType_8() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___orderType_8)); }
	inline int32_t get_orderType_8() const { return ___orderType_8; }
	inline int32_t* get_address_of_orderType_8() { return &___orderType_8; }
	inline void set_orderType_8(int32_t value)
	{
		___orderType_8 = value;
	}

	inline static int32_t get_offset_of_childModels_9() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___childModels_9)); }
	inline DTDContentModelCollection_t2798820000 * get_childModels_9() const { return ___childModels_9; }
	inline DTDContentModelCollection_t2798820000 ** get_address_of_childModels_9() { return &___childModels_9; }
	inline void set_childModels_9(DTDContentModelCollection_t2798820000 * value)
	{
		___childModels_9 = value;
		Il2CppCodeGenWriteBarrier((&___childModels_9), value);
	}

	inline static int32_t get_offset_of_occurence_10() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___occurence_10)); }
	inline int32_t get_occurence_10() const { return ___occurence_10; }
	inline int32_t* get_address_of_occurence_10() { return &___occurence_10; }
	inline void set_occurence_10(int32_t value)
	{
		___occurence_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODEL_T3264596611_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (SchemaTypes_t1741406581)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2101[9] = 
{
	SchemaTypes_t1741406581::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (TypeData_t476999220), -1, sizeof(TypeData_t476999220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2102[12] = 
{
	TypeData_t476999220::get_offset_of_type_0(),
	TypeData_t476999220::get_offset_of_elementName_1(),
	TypeData_t476999220::get_offset_of_sType_2(),
	TypeData_t476999220::get_offset_of_listItemType_3(),
	TypeData_t476999220::get_offset_of_typeName_4(),
	TypeData_t476999220::get_offset_of_fullTypeName_5(),
	TypeData_t476999220::get_offset_of_listItemTypeData_6(),
	TypeData_t476999220::get_offset_of_mappedType_7(),
	TypeData_t476999220::get_offset_of_facet_8(),
	TypeData_t476999220::get_offset_of_hasPublicConstructor_9(),
	TypeData_t476999220::get_offset_of_nullableOverride_10(),
	TypeData_t476999220_StaticFields::get_offset_of_keywords_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (TypeTranslator_t3446962748), -1, sizeof(TypeTranslator_t3446962748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[4] = 
{
	TypeTranslator_t3446962748_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t3446962748_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t3446962748_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t3446962748_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (XmlAnyAttributeAttribute_t1449326428), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (XmlAnyElementAttribute_t4038919363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[1] = 
{
	XmlAnyElementAttribute_t4038919363::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (XmlAttributeAttribute_t2511360870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[2] = 
{
	XmlAttributeAttribute_t2511360870::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t2511360870::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (XmlElementAttribute_t17472343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[3] = 
{
	XmlElementAttribute_t17472343::get_offset_of_elementName_0(),
	XmlElementAttribute_t17472343::get_offset_of_type_1(),
	XmlElementAttribute_t17472343::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (XmlEnumAttribute_t106705320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[1] = 
{
	XmlEnumAttribute_t106705320::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (XmlIgnoreAttribute_t1428424057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (XmlNamespaceDeclarationsAttribute_t966425202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (XmlRootAttribute_t2306097217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[3] = 
{
	XmlRootAttribute_t2306097217::get_offset_of_elementName_0(),
	XmlRootAttribute_t2306097217::get_offset_of_isNullable_1(),
	XmlRootAttribute_t2306097217::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (XmlSchemaProviderAttribute_t3872582200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[2] = 
{
	XmlSchemaProviderAttribute_t3872582200::get_offset_of__methodName_0(),
	XmlSchemaProviderAttribute_t3872582200::get_offset_of__isAny_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (XmlSerializerNamespaces_t2702737953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	XmlSerializerNamespaces_t2702737953::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (XmlTextAttribute_t499390083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (ConformanceLevel_t3899847875)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2116[4] = 
{
	ConformanceLevel_t3899847875::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (DTDAutomataFactory_t2958275022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[3] = 
{
	DTDAutomataFactory_t2958275022::get_offset_of_root_0(),
	DTDAutomataFactory_t2958275022::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t2958275022::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (DTDObjectModel_t1729680289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[19] = 
{
	DTDObjectModel_t1729680289::get_offset_of_factory_0(),
	DTDObjectModel_t1729680289::get_offset_of_elementDecls_1(),
	DTDObjectModel_t1729680289::get_offset_of_attListDecls_2(),
	DTDObjectModel_t1729680289::get_offset_of_peDecls_3(),
	DTDObjectModel_t1729680289::get_offset_of_entityDecls_4(),
	DTDObjectModel_t1729680289::get_offset_of_notationDecls_5(),
	DTDObjectModel_t1729680289::get_offset_of_validationErrors_6(),
	DTDObjectModel_t1729680289::get_offset_of_resolver_7(),
	DTDObjectModel_t1729680289::get_offset_of_nameTable_8(),
	DTDObjectModel_t1729680289::get_offset_of_externalResources_9(),
	DTDObjectModel_t1729680289::get_offset_of_baseURI_10(),
	DTDObjectModel_t1729680289::get_offset_of_name_11(),
	DTDObjectModel_t1729680289::get_offset_of_publicId_12(),
	DTDObjectModel_t1729680289::get_offset_of_systemId_13(),
	DTDObjectModel_t1729680289::get_offset_of_intSubset_14(),
	DTDObjectModel_t1729680289::get_offset_of_intSubsetHasPERef_15(),
	DTDObjectModel_t1729680289::get_offset_of_isStandalone_16(),
	DTDObjectModel_t1729680289::get_offset_of_lineNumber_17(),
	DTDObjectModel_t1729680289::get_offset_of_linePosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (DictionaryBase_t52754249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (U3CU3Ec__Iterator3_t2072931442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[5] = 
{
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (DTDCollectionBase_t3926218464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[1] = 
{
	DTDCollectionBase_t3926218464::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (DTDElementDeclarationCollection_t222313714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (DTDAttListDeclarationCollection_t2220366188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (DTDEntityDeclarationCollection_t2250844513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (DTDNotationDeclarationCollection_t959292105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (DTDContentModel_t3264596611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[6] = 
{
	DTDContentModel_t3264596611::get_offset_of_root_5(),
	DTDContentModel_t3264596611::get_offset_of_ownerElementName_6(),
	DTDContentModel_t3264596611::get_offset_of_elementName_7(),
	DTDContentModel_t3264596611::get_offset_of_orderType_8(),
	DTDContentModel_t3264596611::get_offset_of_childModels_9(),
	DTDContentModel_t3264596611::get_offset_of_occurence_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (DTDContentModelCollection_t2798820000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	DTDContentModelCollection_t2798820000::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (DTDNode_t858560093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[5] = 
{
	DTDNode_t858560093::get_offset_of_root_0(),
	DTDNode_t858560093::get_offset_of_isInternalSubset_1(),
	DTDNode_t858560093::get_offset_of_baseURI_2(),
	DTDNode_t858560093::get_offset_of_lineNumber_3(),
	DTDNode_t858560093::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (DTDElementDeclaration_t1830540991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[6] = 
{
	DTDElementDeclaration_t1830540991::get_offset_of_root_5(),
	DTDElementDeclaration_t1830540991::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t1830540991::get_offset_of_name_7(),
	DTDElementDeclaration_t1830540991::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t1830540991::get_offset_of_isAny_9(),
	DTDElementDeclaration_t1830540991::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (DTDAttributeDefinition_t3434905422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[4] = 
{
	DTDAttributeDefinition_t3434905422::get_offset_of_name_5(),
	DTDAttributeDefinition_t3434905422::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3434905422::get_offset_of_unresolvedDefault_7(),
	DTDAttributeDefinition_t3434905422::get_offset_of_resolvedDefaultValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (DTDAttListDeclaration_t3593159715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[3] = 
{
	DTDAttListDeclaration_t3593159715::get_offset_of_name_5(),
	DTDAttListDeclaration_t3593159715::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t3593159715::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (DTDEntityBase_t1228162861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[10] = 
{
	DTDEntityBase_t1228162861::get_offset_of_name_5(),
	DTDEntityBase_t1228162861::get_offset_of_publicId_6(),
	DTDEntityBase_t1228162861::get_offset_of_systemId_7(),
	DTDEntityBase_t1228162861::get_offset_of_literalValue_8(),
	DTDEntityBase_t1228162861::get_offset_of_replacementText_9(),
	DTDEntityBase_t1228162861::get_offset_of_uriString_10(),
	DTDEntityBase_t1228162861::get_offset_of_absUri_11(),
	DTDEntityBase_t1228162861::get_offset_of_isInvalid_12(),
	DTDEntityBase_t1228162861::get_offset_of_loadFailed_13(),
	DTDEntityBase_t1228162861::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (DTDEntityDeclaration_t811637416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[6] = 
{
	DTDEntityDeclaration_t811637416::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t811637416::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t811637416::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t811637416::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t811637416::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t811637416::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (DTDNotationDeclaration_t3702682588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[5] = 
{
	DTDNotationDeclaration_t3702682588::get_offset_of_name_5(),
	DTDNotationDeclaration_t3702682588::get_offset_of_localName_6(),
	DTDNotationDeclaration_t3702682588::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t3702682588::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t3702682588::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (DTDParameterEntityDeclarationCollection_t2844734410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[2] = 
{
	DTDParameterEntityDeclarationCollection_t2844734410::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t2844734410::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (DTDParameterEntityDeclaration_t3796253422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (DTDContentOrderType_t1195786655)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	DTDContentOrderType_t1195786655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (DTDOccurence_t3140866896)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2138[5] = 
{
	DTDOccurence_t3140866896::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (DTDReader_t386081180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[14] = 
{
	DTDReader_t386081180::get_offset_of_currentInput_0(),
	DTDReader_t386081180::get_offset_of_parserInputStack_1(),
	DTDReader_t386081180::get_offset_of_nameBuffer_2(),
	DTDReader_t386081180::get_offset_of_nameLength_3(),
	DTDReader_t386081180::get_offset_of_nameCapacity_4(),
	DTDReader_t386081180::get_offset_of_valueBuffer_5(),
	DTDReader_t386081180::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t386081180::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t386081180::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t386081180::get_offset_of_normalization_9(),
	DTDReader_t386081180::get_offset_of_processingInternalSubset_10(),
	DTDReader_t386081180::get_offset_of_cachedPublicId_11(),
	DTDReader_t386081180::get_offset_of_cachedSystemId_12(),
	DTDReader_t386081180::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (EntityHandling_t1047276436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2140[3] = 
{
	EntityHandling_t1047276436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (Formatting_t1232942836)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2141[3] = 
{
	Formatting_t1232942836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (NameTable_t3178203267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[3] = 
{
	NameTable_t3178203267::get_offset_of_count_0(),
	NameTable_t3178203267::get_offset_of_buckets_1(),
	NameTable_t3178203267::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (Entry_t3052280359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[4] = 
{
	Entry_t3052280359::get_offset_of_str_0(),
	Entry_t3052280359::get_offset_of_hash_1(),
	Entry_t3052280359::get_offset_of_len_2(),
	Entry_t3052280359::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (NamespaceHandling_t4087553436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2147[3] = 
{
	NamespaceHandling_t4087553436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (NewLineHandling_t850339274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2148[4] = 
{
	NewLineHandling_t850339274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (ReadState_t944984020)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2149[6] = 
{
	ReadState_t944984020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (WhitespaceHandling_t784045650)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2150[4] = 
{
	WhitespaceHandling_t784045650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (WriteState_t3983380671)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2151[8] = 
{
	WriteState_t3983380671::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (XmlAttribute_t1173852259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[4] = 
{
	XmlAttribute_t1173852259::get_offset_of_name_6(),
	XmlAttribute_t1173852259::get_offset_of_isDefault_7(),
	XmlAttribute_t1173852259::get_offset_of_lastLinkedChild_8(),
	XmlAttribute_t1173852259::get_offset_of_schemaInfo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (XmlAttributeCollection_t2316283784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[2] = 
{
	XmlAttributeCollection_t2316283784::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t2316283784::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (XmlCDataSection_t3267478366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (XmlChar_t3816087079), -1, sizeof(XmlChar_t3816087079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[5] = 
{
	XmlChar_t3816087079_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t3816087079_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t3816087079_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t3816087079_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t3816087079_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (XmlCharacterData_t1167807131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[1] = 
{
	XmlCharacterData_t1167807131::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (XmlComment_t2476947920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (XmlConvert_t1981561327), -1, sizeof(XmlConvert_t1981561327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2158[8] = 
{
	XmlConvert_t1981561327_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1981561327_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_U3CU3Ef__switchU24map49_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (XmlDateTimeSerializationMode_t1214355817)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2159[5] = 
{
	XmlDateTimeSerializationMode_t1214355817::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (XmlDeclaration_t679870411), -1, sizeof(XmlDeclaration_t679870411_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2160[4] = 
{
	XmlDeclaration_t679870411::get_offset_of_encoding_7(),
	XmlDeclaration_t679870411::get_offset_of_standalone_8(),
	XmlDeclaration_t679870411::get_offset_of_version_9(),
	XmlDeclaration_t679870411_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (XmlDocument_t2837193595), -1, sizeof(XmlDocument_t2837193595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[19] = 
{
	XmlDocument_t2837193595_StaticFields::get_offset_of_optimal_create_types_6(),
	XmlDocument_t2837193595::get_offset_of_optimal_create_element_7(),
	XmlDocument_t2837193595::get_offset_of_optimal_create_attribute_8(),
	XmlDocument_t2837193595::get_offset_of_nameTable_9(),
	XmlDocument_t2837193595::get_offset_of_baseURI_10(),
	XmlDocument_t2837193595::get_offset_of_implementation_11(),
	XmlDocument_t2837193595::get_offset_of_preserveWhitespace_12(),
	XmlDocument_t2837193595::get_offset_of_resolver_13(),
	XmlDocument_t2837193595::get_offset_of_idTable_14(),
	XmlDocument_t2837193595::get_offset_of_nameCache_15(),
	XmlDocument_t2837193595::get_offset_of_lastLinkedChild_16(),
	XmlDocument_t2837193595::get_offset_of_schemaInfo_17(),
	XmlDocument_t2837193595::get_offset_of_loadMode_18(),
	XmlDocument_t2837193595::get_offset_of_NodeChanged_19(),
	XmlDocument_t2837193595::get_offset_of_NodeChanging_20(),
	XmlDocument_t2837193595::get_offset_of_NodeInserted_21(),
	XmlDocument_t2837193595::get_offset_of_NodeInserting_22(),
	XmlDocument_t2837193595::get_offset_of_NodeRemoved_23(),
	XmlDocument_t2837193595::get_offset_of_NodeRemoving_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (XmlDocumentFragment_t1323348855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[1] = 
{
	XmlDocumentFragment_t1323348855::get_offset_of_lastLinkedChild_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (XmlDocumentType_t4112370061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[3] = 
{
	XmlDocumentType_t4112370061::get_offset_of_entities_7(),
	XmlDocumentType_t4112370061::get_offset_of_notations_8(),
	XmlDocumentType_t4112370061::get_offset_of_dtd_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (XmlElement_t561603118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[5] = 
{
	XmlElement_t561603118::get_offset_of_attributes_7(),
	XmlElement_t561603118::get_offset_of_name_8(),
	XmlElement_t561603118::get_offset_of_lastLinkedChild_9(),
	XmlElement_t561603118::get_offset_of_isNotEmpty_10(),
	XmlElement_t561603118::get_offset_of_schemaInfo_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (XmlEntity_t3308518401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[7] = 
{
	XmlEntity_t3308518401::get_offset_of_name_6(),
	XmlEntity_t3308518401::get_offset_of_NDATA_7(),
	XmlEntity_t3308518401::get_offset_of_publicId_8(),
	XmlEntity_t3308518401::get_offset_of_systemId_9(),
	XmlEntity_t3308518401::get_offset_of_baseUri_10(),
	XmlEntity_t3308518401::get_offset_of_lastLinkedChild_11(),
	XmlEntity_t3308518401::get_offset_of_contentAlreadySet_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (XmlEntityReference_t1966808559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[2] = 
{
	XmlEntityReference_t1966808559::get_offset_of_entityName_7(),
	XmlEntityReference_t1966808559::get_offset_of_lastLinkedChild_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (XmlException_t1761730631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[5] = 
{
	XmlException_t1761730631::get_offset_of_lineNumber_11(),
	XmlException_t1761730631::get_offset_of_linePosition_12(),
	XmlException_t1761730631::get_offset_of_sourceUri_13(),
	XmlException_t1761730631::get_offset_of_res_14(),
	XmlException_t1761730631::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (XmlImplementation_t254178875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[1] = 
{
	XmlImplementation_t254178875::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (XmlStreamReader_t727818754), -1, sizeof(XmlStreamReader_t727818754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2169[2] = 
{
	XmlStreamReader_t727818754::get_offset_of_input_12(),
	XmlStreamReader_t727818754_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (NonBlockingStreamReader_t2495303928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[11] = 
{
	NonBlockingStreamReader_t2495303928::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t2495303928::get_offset_of_pos_4(),
	NonBlockingStreamReader_t2495303928::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t2495303928::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t2495303928::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t2495303928::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t2495303928::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t2495303928::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (XmlInputStream_t1691369434), -1, sizeof(XmlInputStream_t1691369434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2171[7] = 
{
	XmlInputStream_t1691369434_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t1691369434::get_offset_of_enc_2(),
	XmlInputStream_t1691369434::get_offset_of_stream_3(),
	XmlInputStream_t1691369434::get_offset_of_buffer_4(),
	XmlInputStream_t1691369434::get_offset_of_bufLength_5(),
	XmlInputStream_t1691369434::get_offset_of_bufPos_6(),
	XmlInputStream_t1691369434_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (XmlLinkedNode_t1437094927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[1] = 
{
	XmlLinkedNode_t1437094927::get_offset_of_nextSibling_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (XmlNameEntry_t1073099671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[5] = 
{
	XmlNameEntry_t1073099671::get_offset_of_Prefix_0(),
	XmlNameEntry_t1073099671::get_offset_of_LocalName_1(),
	XmlNameEntry_t1073099671::get_offset_of_NS_2(),
	XmlNameEntry_t1073099671::get_offset_of_Hash_3(),
	XmlNameEntry_t1073099671::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (XmlNameEntryCache_t2890546907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[4] = 
{
	XmlNameEntryCache_t2890546907::get_offset_of_table_0(),
	XmlNameEntryCache_t2890546907::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t2890546907::get_offset_of_dummy_2(),
	XmlNameEntryCache_t2890546907::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (XmlNameTable_t71772148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (XmlNamedNodeMap_t2821286253), -1, sizeof(XmlNamedNodeMap_t2821286253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2176[4] = 
{
	XmlNamedNodeMap_t2821286253_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t2821286253::get_offset_of_parent_1(),
	XmlNamedNodeMap_t2821286253::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t2821286253::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (XmlNamespaceManager_t418790500), -1, sizeof(XmlNamespaceManager_t418790500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2177[9] = 
{
	XmlNamespaceManager_t418790500::get_offset_of_decls_0(),
	XmlNamespaceManager_t418790500::get_offset_of_declPos_1(),
	XmlNamespaceManager_t418790500::get_offset_of_scopes_2(),
	XmlNamespaceManager_t418790500::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t418790500::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t418790500::get_offset_of_count_5(),
	XmlNamespaceManager_t418790500::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t418790500::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t418790500_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (NsDecl_t3938094415)+ sizeof (RuntimeObject), sizeof(NsDecl_t3938094415_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2178[2] = 
{
	NsDecl_t3938094415::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsDecl_t3938094415::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (NsScope_t3958624705)+ sizeof (RuntimeObject), sizeof(NsScope_t3958624705_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2179[2] = 
{
	NsScope_t3958624705::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsScope_t3958624705::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (XmlNode_t3767805227), -1, sizeof(XmlNode_t3767805227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2180[6] = 
{
	XmlNode_t3767805227_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t3767805227::get_offset_of_ownerDocument_1(),
	XmlNode_t3767805227::get_offset_of_parentNode_2(),
	XmlNode_t3767805227::get_offset_of_childNodes_3(),
	XmlNode_t3767805227_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
	XmlNode_t3767805227_StaticFields::get_offset_of_U3CU3Ef__switchU24map46_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (EmptyNodeList_t139615908), -1, sizeof(EmptyNodeList_t139615908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2181[1] = 
{
	EmptyNodeList_t139615908_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (XmlNodeChangedAction_t3227731597)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2182[4] = 
{
	XmlNodeChangedAction_t3227731597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (XmlNodeChangedEventArgs_t2486095928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[6] = 
{
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (XmlNodeList_t2551693786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (XmlNodeListChildren_t1082692789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[1] = 
{
	XmlNodeListChildren_t1082692789::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (Enumerator_t97922292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[3] = 
{
	Enumerator_t97922292::get_offset_of_parent_0(),
	Enumerator_t97922292::get_offset_of_currentChild_1(),
	Enumerator_t97922292::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (XmlNodeReader_t4064889562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[4] = 
{
	XmlNodeReader_t4064889562::get_offset_of_entity_3(),
	XmlNodeReader_t4064889562::get_offset_of_source_4(),
	XmlNodeReader_t4064889562::get_offset_of_entityInsideAttribute_5(),
	XmlNodeReader_t4064889562::get_offset_of_insideAttribute_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (XmlNodeReaderImpl_t2501602067), -1, sizeof(XmlNodeReaderImpl_t2501602067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[11] = 
{
	XmlNodeReaderImpl_t2501602067::get_offset_of_document_3(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_startNode_4(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_current_5(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_ownerLinkedNode_6(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_state_7(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_depth_8(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_isEndElement_9(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_ignoreStartNode_10(),
	XmlNodeReaderImpl_t2501602067_StaticFields::get_offset_of_U3CU3Ef__switchU24map4D_11(),
	XmlNodeReaderImpl_t2501602067_StaticFields::get_offset_of_U3CU3Ef__switchU24map4E_12(),
	XmlNodeReaderImpl_t2501602067_StaticFields::get_offset_of_U3CU3Ef__switchU24map4F_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (XmlNodeType_t1672767151)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2189[19] = 
{
	XmlNodeType_t1672767151::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (XmlNotation_t1476580686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[4] = 
{
	XmlNotation_t1476580686::get_offset_of_localName_6(),
	XmlNotation_t1476580686::get_offset_of_publicId_7(),
	XmlNotation_t1476580686::get_offset_of_systemId_8(),
	XmlNotation_t1476580686::get_offset_of_prefix_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (XmlOutputMethod_t2185361861)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2191[5] = 
{
	XmlOutputMethod_t2185361861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (XmlParserContext_t2544895291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[13] = 
{
	XmlParserContext_t2544895291::get_offset_of_baseURI_0(),
	XmlParserContext_t2544895291::get_offset_of_docTypeName_1(),
	XmlParserContext_t2544895291::get_offset_of_encoding_2(),
	XmlParserContext_t2544895291::get_offset_of_internalSubset_3(),
	XmlParserContext_t2544895291::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2544895291::get_offset_of_nameTable_5(),
	XmlParserContext_t2544895291::get_offset_of_publicID_6(),
	XmlParserContext_t2544895291::get_offset_of_systemID_7(),
	XmlParserContext_t2544895291::get_offset_of_xmlLang_8(),
	XmlParserContext_t2544895291::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2544895291::get_offset_of_contextItems_10(),
	XmlParserContext_t2544895291::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2544895291::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (ContextItem_t3112052795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[3] = 
{
	ContextItem_t3112052795::get_offset_of_BaseURI_0(),
	ContextItem_t3112052795::get_offset_of_XmlLang_1(),
	ContextItem_t3112052795::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (XmlParserInput_t2182411204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[5] = 
{
	XmlParserInput_t2182411204::get_offset_of_sourceStack_0(),
	XmlParserInput_t2182411204::get_offset_of_source_1(),
	XmlParserInput_t2182411204::get_offset_of_has_peek_2(),
	XmlParserInput_t2182411204::get_offset_of_peek_char_3(),
	XmlParserInput_t2182411204::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (XmlParserInputSource_t3533005609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[6] = 
{
	XmlParserInputSource_t3533005609::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t3533005609::get_offset_of_reader_1(),
	XmlParserInputSource_t3533005609::get_offset_of_state_2(),
	XmlParserInputSource_t3533005609::get_offset_of_isPE_3(),
	XmlParserInputSource_t3533005609::get_offset_of_line_4(),
	XmlParserInputSource_t3533005609::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (XmlProcessingInstruction_t425688976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[2] = 
{
	XmlProcessingInstruction_t425688976::get_offset_of_target_7(),
	XmlProcessingInstruction_t425688976::get_offset_of_data_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (XmlQualifiedName_t2760654312), -1, sizeof(XmlQualifiedName_t2760654312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2197[4] = 
{
	XmlQualifiedName_t2760654312_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t2760654312::get_offset_of_name_1(),
	XmlQualifiedName_t2760654312::get_offset_of_ns_2(),
	XmlQualifiedName_t2760654312::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (XmlReader_t3121518892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[3] = 
{
	XmlReader_t3121518892::get_offset_of_readStringBuffer_0(),
	XmlReader_t3121518892::get_offset_of_binary_1(),
	XmlReader_t3121518892::get_offset_of_settings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (XmlReaderBinarySupport_t1809665003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[5] = 
{
	XmlReaderBinarySupport_t1809665003::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_state_2(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_dontReset_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
