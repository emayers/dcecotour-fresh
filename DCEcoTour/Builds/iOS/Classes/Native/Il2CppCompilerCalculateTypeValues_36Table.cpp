﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// TMPro.Examples.WarpTextExample
struct WarpTextExample_t3821118074;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#define U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct  U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::modifiedCharScale
	List_1_t2869341516 * ___modifiedCharScale_0;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::<>f__ref$0
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_modifiedCharScale_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514, ___modifiedCharScale_0)); }
	inline List_1_t2869341516 * get_modifiedCharScale_0() const { return ___modifiedCharScale_0; }
	inline List_1_t2869341516 ** get_address_of_modifiedCharScale_0() { return &___modifiedCharScale_0; }
	inline void set_modifiedCharScale_0(List_1_t2869341516 * value)
	{
		___modifiedCharScale_0 = value;
		Il2CppCodeGenWriteBarrier((&___modifiedCharScale_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514, ___U3CU3Ef__refU240_1)); }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef U24ARRAYTYPEU3D116_T1514025261_H
#define U24ARRAYTYPEU3D116_T1514025261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=116
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D116_t1514025261 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D116_t1514025261__padding[116];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D116_T1514025261_H
#ifndef U24ARRAYTYPEU3D20_T1702832645_H
#define U24ARRAYTYPEU3D20_T1702832645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t1702832645 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t1702832645__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T1702832645_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef U24ARRAYTYPEU3D64_T498138225_H
#define U24ARRAYTYPEU3D64_T498138225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t498138225 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t498138225__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D64_T498138225_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U24ARRAYTYPEU3D128_T4235014459_H
#define U24ARRAYTYPEU3D128_T4235014459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=128
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D128_t4235014459 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D128_t4235014459__padding[128];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D128_T4235014459_H
#ifndef U24ARRAYTYPEU3D520_T2265645983_H
#define U24ARRAYTYPEU3D520_T2265645983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=520
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D520_t2265645983 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D520_t2265645983__padding[520];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D520_T2265645983_H
#ifndef U24ARRAYTYPEU3D1024_T3853988145_H
#define U24ARRAYTYPEU3D1024_T3853988145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=1024
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D1024_t3853988145 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D1024_t3853988145__padding[1024];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D1024_T3853988145_H
#ifndef U24ARRAYTYPEU3D100_T3852677427_H
#define U24ARRAYTYPEU3D100_T3852677427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=100
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D100_t3852677427 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D100_t3852677427__padding[100];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D100_T3852677427_H
#ifndef U24ARRAYTYPEU3D2052_T3450310401_H
#define U24ARRAYTYPEU3D2052_T3450310401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=2052
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D2052_t3450310401 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D2052_t3450310401__padding[2052];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D2052_T3450310401_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#define U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t4025661343  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_1;
	// TMPro.TMP_TextInfo TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_2;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_4;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_5;
	// UnityEngine.Vector3[] TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_6;
	// UnityEngine.Matrix4x4 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_7;
	// TMPro.Examples.WarpTextExample TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$this
	WarpTextExample_t3821118074 * ___U24this_8;
	// System.Object TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3Cold_curveU3E__0_1)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_1() const { return ___U3Cold_curveU3E__0_1; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_1() { return &___U3Cold_curveU3E__0_1; }
	inline void set_U3Cold_curveU3E__0_1(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CtextInfoU3E__1_2)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_2() const { return ___U3CtextInfoU3E__1_2; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_2() { return &___U3CtextInfoU3E__1_2; }
	inline void set_U3CtextInfoU3E__1_2(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CboundsMinXU3E__1_4)); }
	inline float get_U3CboundsMinXU3E__1_4() const { return ___U3CboundsMinXU3E__1_4; }
	inline float* get_address_of_U3CboundsMinXU3E__1_4() { return &___U3CboundsMinXU3E__1_4; }
	inline void set_U3CboundsMinXU3E__1_4(float value)
	{
		___U3CboundsMinXU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CboundsMaxXU3E__1_5)); }
	inline float get_U3CboundsMaxXU3E__1_5() const { return ___U3CboundsMaxXU3E__1_5; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_5() { return &___U3CboundsMaxXU3E__1_5; }
	inline void set_U3CboundsMaxXU3E__1_5(float value)
	{
		___U3CboundsMaxXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CverticesU3E__2_6)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_6() const { return ___U3CverticesU3E__2_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_6() { return &___U3CverticesU3E__2_6; }
	inline void set_U3CverticesU3E__2_6(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CmatrixU3E__2_7)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_7() const { return ___U3CmatrixU3E__2_7; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_7() { return &___U3CmatrixU3E__2_7; }
	inline void set_U3CmatrixU3E__2_7(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24this_8)); }
	inline WarpTextExample_t3821118074 * get_U24this_8() const { return ___U24this_8; }
	inline WarpTextExample_t3821118074 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(WarpTextExample_t3821118074 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255371  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5DFEE31002BF167E0453CB8643D3E1D44FE3F325
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-885DDD59952941A3E98DD105FDAADBE334B79776
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-6EB03E1E021F75A722B15DD1E230C3794339E58F
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-82C0D5F32D3B4A90C255DF4203A719570A4A71C6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-DD84CB646A0ED87DE965197F182EA6E70D7710FB
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-EEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-7553F65BB6AEF17B8E9BEB262884FD704C46DC9D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8727E44B94789414DE204CBA117AD05443BDFF85
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-CD9B3692D323300F91E71C04211EA451047039D6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-F3D84C957438E86E3A03BA8266CEFF9B039760AC
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-34EDEEBC5EB29562EF18C5F3AA190B324187790C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-A6E8715255241F47AD0B3D93E1BA359D52D4F773
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-7EA0120ED2A607EE02EE175E161E3AB6678EAEF3
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-C63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5568A7B56666FACA81071906CAD5B68BEF9CBA7D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-80837B69D4B41AE55078725B3ACCF0A27C21EA8C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-F1562B156A30ABF23C48AEEB151115973E5E0E68
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-21F3C52377932DF2C57E466D2DF531566BF21C8E
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-78B13EFFF801B0EEDCD91D16B161747601E46D7F
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-9EECA0B3D49F88F27A544B19940E1E8375C54E90
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-B1C28103C273F13F1776F8909515A1B1E42EFA96
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-408A4836C23B92A6BAD4820B377880F6CC1FA2A8
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-457EC0A425D11B4CCAB4205123D64045679F8AB3
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-12536FB84378CA4223BBD8FB3833394D625A09D3
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-2903EDF33AFE43744D8D4676E80AE98CA93318D6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-05A61FB9C1B133245C69208185ADD4E2668799FD
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-FD39839203F719185D79D5FB10889EBD650235A8
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-E11E7676B42F5205585524A542DC08BC21B6BC74
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-0AA3902D236F027C82FC3F064812EF11C90B8873
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3D84862348B9A730AE6F506EA7FB70C660C88A2A
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8456C8142FC382C9C7D5CC2BA87FD135D90889B1
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-45507F8AFFC24037D807B05461DD6C77DEC72C1F
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-F14497D0866AAE006DAA1A34D21DCA4E99BE3CB4
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-183F34689D23DCB29B9F650AAE12A25144D57EA5
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-1B9653E9DAF24F1B6E8E0646A5426E8207CAC175
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-655283041A648AE2C424D8E20F6905C22E8834E8
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-23468C894E1BC8AFF095FB390877A8EAE07CAD38
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-450ADA4BD48BD2F5C266223142DBC1AFC68C533C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-98B6C8EFF0078C310BF5655A7FF296752695FD7D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-D238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-CFE35FBC0A074388D82D17E334381E3CB97AF384
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-64B4EE118F3DF0EC4723AC0E41203320BCC36752
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59;
	// <PrivateImplementationDetails>/$ArrayType=520 <PrivateImplementationDetails>::$field-B51C0DEB10EE1C5E9888F5306B744ACD1F38D767
	U24ArrayTypeU3D520_t2265645983  ___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60;
	// <PrivateImplementationDetails>/$ArrayType=1024 <PrivateImplementationDetails>::$field-373B494F210C656134C5728D551D4C97B013EB33
	U24ArrayTypeU3D1024_t3853988145  ___U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61;
	// <PrivateImplementationDetails>/$ArrayType=100 <PrivateImplementationDetails>::$field-F5B52AE718355A38E8190D5F947B52DFB427A1D7
	U24ArrayTypeU3D100_t3852677427  ___U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62;
	// <PrivateImplementationDetails>/$ArrayType=100 <PrivateImplementationDetails>::$field-81D5D8B4DDFA3FBB954AC400415044EB9F11E33E
	U24ArrayTypeU3D100_t3852677427  ___U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63;
	// <PrivateImplementationDetails>/$ArrayType=2052 <PrivateImplementationDetails>::$field-C0C10EC6AF4A4101F894B153E1CD493ADC01A67F
	U24ArrayTypeU3D2052_t3450310401  ___U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-6A94F0C3DCA389344CEDB99F171FA1E092E842E6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-8ED4E99B936B26A09EDFAB9E336CF75F4913B454
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-F5F598DAC7D3E479CD72BAAB99EE6617D8190398
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-2492606636F4A4666E0D617B51116A5A68539881
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-472655E8FD2B8B97DB5D188D273A20E8834C19BB
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69;
	// <PrivateImplementationDetails>/$ArrayType=116 <PrivateImplementationDetails>::$field-D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF
	U24ArrayTypeU3D116_t1514025261  ___U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-FD5BE77C4372533D7C16BF67D58A3ABBE604ED81
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71;
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-AE6B2897A8B88E297D61124152931A88D5D977F4
	U24ArrayTypeU3D20_t1702832645  ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-BEEEBCEF33AF5B72B0463DF9185C0226DE5909AD
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74;

public:
	inline static int32_t get_offset_of_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0() const { return ___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0() { return &___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0; }
	inline void set_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1() const { return ___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1() { return &___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1; }
	inline void set_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2() const { return ___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2() { return &___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2; }
	inline void set_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3() const { return ___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3() { return &___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3; }
	inline void set_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4() const { return ___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4() { return &___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4; }
	inline void set_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5() const { return ___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5() { return &___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5; }
	inline void set_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6() const { return ___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6() { return &___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6; }
	inline void set_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7() const { return ___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7() { return &___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7; }
	inline void set_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8() const { return ___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8() { return &___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8; }
	inline void set_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9() const { return ___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9() { return &___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9; }
	inline void set_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10() const { return ___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10() { return &___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10; }
	inline void set_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11() const { return ___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11() { return &___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11; }
	inline void set_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12() const { return ___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12() { return &___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12; }
	inline void set_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13() const { return ___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13() { return &___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13; }
	inline void set_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14() const { return ___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14() { return &___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14; }
	inline void set_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15() const { return ___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15() { return &___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15; }
	inline void set_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16() const { return ___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16() { return &___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16; }
	inline void set_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17() const { return ___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17() { return &___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17; }
	inline void set_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18() const { return ___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18() { return &___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18; }
	inline void set_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19() const { return ___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19() { return &___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19; }
	inline void set_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20() const { return ___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20() { return &___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20; }
	inline void set_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21() const { return ___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21() { return &___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21; }
	inline void set_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22() const { return ___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22() { return &___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22; }
	inline void set_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23() const { return ___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23() { return &___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23; }
	inline void set_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24() const { return ___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24() { return &___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24; }
	inline void set_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25() const { return ___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25() { return &___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25; }
	inline void set_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26() const { return ___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26() { return &___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26; }
	inline void set_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27() const { return ___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27() { return &___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27; }
	inline void set_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28() const { return ___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28() { return &___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28; }
	inline void set_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29() const { return ___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29() { return &___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29; }
	inline void set_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30() const { return ___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30() { return &___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30; }
	inline void set_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31() const { return ___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31() { return &___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31; }
	inline void set_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32() const { return ___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32() { return &___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32; }
	inline void set_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33() const { return ___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33() { return &___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33; }
	inline void set_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34() const { return ___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34() { return &___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34; }
	inline void set_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35() const { return ___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35() { return &___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35; }
	inline void set_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36() const { return ___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36() { return &___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36; }
	inline void set_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37() const { return ___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37() { return &___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37; }
	inline void set_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38() const { return ___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38() { return &___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38; }
	inline void set_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39() const { return ___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39() { return &___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39; }
	inline void set_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40() const { return ___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40() { return &___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40; }
	inline void set_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41() const { return ___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41() { return &___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41; }
	inline void set_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42() const { return ___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42() { return &___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42; }
	inline void set_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43() const { return ___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43() { return &___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43; }
	inline void set_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44() const { return ___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44() { return &___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44; }
	inline void set_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45() const { return ___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45() { return &___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45; }
	inline void set_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46() const { return ___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46() { return &___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46; }
	inline void set_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47() const { return ___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47() { return &___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47; }
	inline void set_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48() const { return ___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48() { return &___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48; }
	inline void set_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49() const { return ___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49() { return &___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49; }
	inline void set_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50() const { return ___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50() { return &___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50; }
	inline void set_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51() const { return ___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51() { return &___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51; }
	inline void set_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52() const { return ___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52() { return &___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52; }
	inline void set_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53() const { return ___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53() { return &___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53; }
	inline void set_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54() const { return ___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54() { return &___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54; }
	inline void set_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55() const { return ___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55() { return &___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55; }
	inline void set_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56() const { return ___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56() { return &___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56; }
	inline void set_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57() const { return ___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57() { return &___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57; }
	inline void set_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58() const { return ___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58() { return &___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58; }
	inline void set_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59() const { return ___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59() { return &___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59; }
	inline void set_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60)); }
	inline U24ArrayTypeU3D520_t2265645983  get_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60() const { return ___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60; }
	inline U24ArrayTypeU3D520_t2265645983 * get_address_of_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60() { return &___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60; }
	inline void set_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60(U24ArrayTypeU3D520_t2265645983  value)
	{
		___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61)); }
	inline U24ArrayTypeU3D1024_t3853988145  get_U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61() const { return ___U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61; }
	inline U24ArrayTypeU3D1024_t3853988145 * get_address_of_U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61() { return &___U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61; }
	inline void set_U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61(U24ArrayTypeU3D1024_t3853988145  value)
	{
		___U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62)); }
	inline U24ArrayTypeU3D100_t3852677427  get_U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62() const { return ___U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62; }
	inline U24ArrayTypeU3D100_t3852677427 * get_address_of_U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62() { return &___U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62; }
	inline void set_U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62(U24ArrayTypeU3D100_t3852677427  value)
	{
		___U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63)); }
	inline U24ArrayTypeU3D100_t3852677427  get_U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63() const { return ___U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63; }
	inline U24ArrayTypeU3D100_t3852677427 * get_address_of_U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63() { return &___U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63; }
	inline void set_U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63(U24ArrayTypeU3D100_t3852677427  value)
	{
		___U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64)); }
	inline U24ArrayTypeU3D2052_t3450310401  get_U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64() const { return ___U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64; }
	inline U24ArrayTypeU3D2052_t3450310401 * get_address_of_U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64() { return &___U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64; }
	inline void set_U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64(U24ArrayTypeU3D2052_t3450310401  value)
	{
		___U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65() const { return ___U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65() { return &___U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65; }
	inline void set_U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66() const { return ___U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66() { return &___U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66; }
	inline void set_U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67() const { return ___U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67() { return &___U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67; }
	inline void set_U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68() const { return ___U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68() { return &___U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68; }
	inline void set_U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69() const { return ___U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69() { return &___U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69; }
	inline void set_U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70)); }
	inline U24ArrayTypeU3D116_t1514025261  get_U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70() const { return ___U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70; }
	inline U24ArrayTypeU3D116_t1514025261 * get_address_of_U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70() { return &___U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70; }
	inline void set_U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70(U24ArrayTypeU3D116_t1514025261  value)
	{
		___U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71() const { return ___U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71() { return &___U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71; }
	inline void set_U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72)); }
	inline U24ArrayTypeU3D20_t1702832645  get_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72() const { return ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72; }
	inline U24ArrayTypeU3D20_t1702832645 * get_address_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72() { return &___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72; }
	inline void set_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72(U24ArrayTypeU3D20_t1702832645  value)
	{
		___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73() const { return ___U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73() { return &___U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73; }
	inline void set_U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74() const { return ___U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74() { return &___U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74; }
	inline void set_U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef WARPTEXTEXAMPLE_T3821118074_H
#define WARPTEXTEXAMPLE_T3821118074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample
struct  WarpTextExample_t3821118074  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.WarpTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_2;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_3;
	// System.Single TMPro.Examples.WarpTextExample::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.WarpTextExample::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.WarpTextExample::CurveScale
	float ___CurveScale_6;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___m_TextComponent_2)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_VertexCurve_3() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___VertexCurve_3)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_3() const { return ___VertexCurve_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_3() { return &___VertexCurve_3; }
	inline void set_VertexCurve_3(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_3), value);
	}

	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARPTEXTEXAMPLE_T3821118074_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[2] = 
{
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_modifiedCharScale_0(),
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (WarpTextExample_t3821118074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3601[5] = 
{
	WarpTextExample_t3821118074::get_offset_of_m_TextComponent_2(),
	WarpTextExample_t3821118074::get_offset_of_VertexCurve_3(),
	WarpTextExample_t3821118074::get_offset_of_AngleMultiplier_4(),
	WarpTextExample_t3821118074::get_offset_of_SpeedMultiplier_5(),
	WarpTextExample_t3821118074::get_offset_of_CurveScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (U3CWarpTextU3Ec__Iterator0_t4025661343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[12] = 
{
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_curveU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CtextInfoU3E__1_2(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMinXU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMaxXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CverticesU3E__2_6(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CmatrixU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24this_8(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24current_9(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24disposing_10(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255371), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3603[75] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_0(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_1(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_2(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_3(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_4(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_5(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_6(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_7(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_8(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_9(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_10(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_11(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_12(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_13(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_14(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_15(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_16(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_17(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_18(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_19(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_20(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_21(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_22(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_23(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_24(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_25(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_26(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_27(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_28(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_29(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_30(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_31(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_32(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_33(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_34(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_35(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_36(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_37(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_38(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_39(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_40(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_41(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_42(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_43(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_44(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_45(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_46(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_47(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_48(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_49(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_50(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_51(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_52(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_53(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_54(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_55(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_56(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_57(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_58(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_59(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_60(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D373B494F210C656134C5728D551D4C97B013EB33_61(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DF5B52AE718355A38E8190D5F947B52DFB427A1D7_62(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D81D5D8B4DDFA3FBB954AC400415044EB9F11E33E_63(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DC0C10EC6AF4A4101F894B153E1CD493ADC01A67F_64(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D6A94F0C3DCA389344CEDB99F171FA1E092E842E6_65(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D8ED4E99B936B26A09EDFAB9E336CF75F4913B454_66(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DF5F598DAC7D3E479CD72BAAB99EE6617D8190398_67(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D2492606636F4A4666E0D617B51116A5A68539881_68(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D472655E8FD2B8B97DB5D188D273A20E8834C19BB_69(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DD8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_70(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DFD5BE77C4372533D7C16BF67D58A3ABBE604ED81_71(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_72(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2D794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6_73(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U24fieldU2DBEEEBCEF33AF5B72B0463DF9185C0226DE5909AD_74(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (U24ArrayTypeU3D128_t4235014459)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D128_t4235014459 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (U24ArrayTypeU3D520_t2265645983)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D520_t2265645983 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (U24ArrayTypeU3D1024_t3853988145)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D1024_t3853988145 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (U24ArrayTypeU3D100_t3852677427)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D100_t3852677427 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (U24ArrayTypeU3D2052_t3450310401)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D2052_t3450310401 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (U24ArrayTypeU3D64_t498138225)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D64_t498138225 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (U24ArrayTypeU3D116_t1514025261)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D116_t1514025261 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (U24ArrayTypeU3D20_t1702832645)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t1702832645 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[2] = 
{
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
