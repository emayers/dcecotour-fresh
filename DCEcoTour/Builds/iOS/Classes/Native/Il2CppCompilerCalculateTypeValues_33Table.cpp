﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// System.Threading.Mutex
struct Mutex_t3066672582;
// Mapbox.Map.RawPngRasterTile
struct RawPngRasterTile_t2301039290;
// Mapbox.Unity.MeshGeneration.Data.UnityTile
struct UnityTile_t2405085845;
// Mapbox.Unity.MeshGeneration.Factories.TerrainFactory
struct TerrainFactory_t974472764;
// Mapbox.Map.VectorTile
struct VectorTile_t4284514353;
// Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory
struct VectorTileFactory_t815718376;
// System.String
struct String_t;
// Mapbox.Unity.MeshGeneration.Modifiers.ModifierStackBase
struct ModifierStackBase_t4180154112;
// System.Collections.Generic.List`1<Mapbox.Examples.Voxels.VoxelData>
struct List_1_t3720956926;
// Mapbox.Examples.Voxels.VoxelTile
struct VoxelTile_t1944340880;
// System.Type
struct Type_t;
// Mapbox.Unity.Location.DeviceLocationProvider
struct DeviceLocationProvider_t4095080695;
// Mapbox.Unity.Telemetry.ITelemetryLibrary
struct ITelemetryLibrary_t1008417135;
// Mapbox.Platform.Cache.CachingWebFileSource
struct CachingWebFileSource_t328893056;
// Mapbox.Unity.MapboxConfiguration
struct MapboxConfiguration_t2330578778;
// Mapbox.Geocoding.Geocoder
struct Geocoder_t3195298050;
// Mapbox.Directions.Directions
struct Directions_t1397515081;
// Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory
struct LowPolyTerrainFactory_t2718546587;
// Mapbox.VectorTile.VectorTileFeature
struct VectorTileFeature_t4093669591;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct List_1_t2371495652;
// Mapbox.Map.RasterTile
struct RasterTile_t2895285341;
// Mapbox.Unity.MeshGeneration.Factories.MapImageFactory
struct MapImageFactory_t200237364;
// Mapbox.Unity.MeshGeneration.Factories.MeshFactory
struct MeshFactory_t3379932301;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// Mapbox.Map.StyleOptimizedVectorTile
struct StyleOptimizedVectorTile_t1819232013;
// Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory
struct StyleOptimizedVectorTileFactory_t1939249911;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Int32>>
struct List_1_t1600127941;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct List_1_t805411711;
// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory>
struct List_1_t3005512771;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// Mapbox.Unity.Map.IMapReadable
struct IMapReadable_t983691703;
// System.Collections.Generic.Dictionary`2<Mapbox.Map.UnwrappedTileId,Mapbox.Unity.MeshGeneration.Data.UnityTile>
struct Dictionary_2_t1603180064;
// System.Collections.Generic.Queue`1<Mapbox.Unity.MeshGeneration.Data.UnityTile>
struct Queue_1_t2251345339;
// System.Action`1<Mapbox.Unity.Map.ModuleState>
struct Action_1_t2062571319;
// Mapbox.Platform.IFileSource
struct IFileSource_t3859839141;
// System.Action`1<Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory>
struct Action_1_t1705905624;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// System.Collections.Generic.Dictionary`2<Mapbox.Map.UnwrappedTileId,UnityEngine.Mesh>
struct Dictionary_2_t2847058503;
// Mapbox.Unity.MeshGeneration.Data.MeshData
struct MeshData_t2053528051;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase>
struct List_1_t77595264;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase>>
struct Dictionary_2_t4157818859;
// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,Mapbox.Map.VectorTile>
struct Dictionary_2_t1598572408;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Filters.FilterBase>
struct List_1_t654335614;
// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.TypeVisualizerTuple>
struct List_1_t2105734821;
// Mapbox.Unity.MeshGeneration.Factories.Style
struct Style_t2684836192;
// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,Mapbox.Map.StyleOptimizedVectorTile>
struct Dictionary_2_t3428257364;
// Mapbox.Unity.Map.AbstractMap
struct AbstractMap_t3082917158;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.EventHandler`1<Mapbox.Unity.Location.HeadingUpdatedEventArgs>
struct EventHandler_1_t2962207339;
// System.EventHandler`1<Mapbox.Unity.Location.LocationUpdatedEventArgs>
struct EventHandler_1_t1729069707;
// UnityEngine.Camera
struct Camera_t4157153871;
// Mapbox.Unity.Map.DynamicZoomTileProvider
struct DynamicZoomTileProvider_t2524886064;
// Mapbox.Unity.Map.DynamicZoomMap
struct DynamicZoomMap_t2517981420;
// Mapbox.Unity.Map.AbstractTileProvider
struct AbstractTileProvider_t1076087356;
// Mapbox.Unity.Map.AbstractMapVisualizer
struct AbstractMapVisualizer_t551237957;
// Mapbox.Unity.MapboxAccess
struct MapboxAccess_t3460807032;
// System.Action
struct Action_t1264377477;
// Mapbox.Examples.Voxels.VoxelColorMapper[]
struct VoxelColorMapperU5BU5D_t1422690960;
// Mapbox.Examples.ForwardGeocodeUserInput
struct ForwardGeocodeUserInput_t2575136032;
// Mapbox.Examples.Voxels.VoxelFetcher
struct VoxelFetcher_t3713644963;
// Mapbox.Map.Map`1<Mapbox.Map.RasterTile>
struct Map_1_t1665010825;
// Mapbox.Map.Map`1<Mapbox.Map.RawPngRasterTile>
struct Map_1_t1070764774;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// Mapbox.Unity.Location.EditorLocationProvider
struct EditorLocationProvider_t3344154418;
// Mapbox.Unity.Location.TransformLocationProvider
struct TransformLocationProvider_t3083760227;
// Mapbox.Unity.Location.ILocationProvider
struct ILocationProvider_t2513726273;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t4027761066;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1812449865;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// Mapbox.Geocoding.ForwardGeocodeResource
struct ForwardGeocodeResource_t367023433;
// Mapbox.Geocoding.ForwardGeocodeResponse
struct ForwardGeocodeResponse_t2959476828;
// System.Action`1<Mapbox.Geocoding.ForwardGeocodeResponse>
struct Action_1_t3131944423;
// Mapbox.Examples.FeatureUiMarker
struct FeatureUiMarker_t3847499068;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour
struct FeatureBehaviour_t1816865007;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String>
struct Func_2_t268983481;
// Mapbox.Geocoding.ReverseGeocodeResource
struct ReverseGeocodeResource_t2777886177;
// Mapbox.Geocoding.ReverseGeocodeResponse
struct ReverseGeocodeResponse_t3723437562;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// Mapbox.Unity.Map.MapVisualizer
struct MapVisualizer_t2953540877;
// Mapbox.Unity.MeshGeneration.Data.VectorFeatureUnity
struct VectorFeatureUnity_t1815898701;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.List`1<Mapbox.Map.Tile>
struct List_1_t2569976014;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.Collider
struct Collider_t1773347010;
// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile>
struct Action_1_t2577553440;
// Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier[]
struct MeshModifierU5BU5D_t115465745;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier,System.Boolean>
struct Func_2_t1030805891;
// Mapbox.Unity.MeshGeneration.Modifiers.AddMonoBehavioursModifierType[]
struct AddMonoBehavioursModifierTypeU5BU5D_t228035385;
// System.Action`1<Mapbox.Map.UnwrappedTileId>
struct Action_1_t2759321132;
// Mapbox.Unity.Map.IMap
struct IMap_t3092174808;
// System.Collections.Generic.Dictionary`2<Mapbox.Map.UnwrappedTileId,System.Byte>
struct Dictionary_2_t332390595;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TEXTURESCALE_T57896704_H
#define TEXTURESCALE_T57896704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Voxels.TextureScale
struct  TextureScale_t57896704  : public RuntimeObject
{
public:

public:
};

struct TextureScale_t57896704_StaticFields
{
public:
	// UnityEngine.Color[] Mapbox.Examples.Voxels.TextureScale::texColors
	ColorU5BU5D_t941916413* ___texColors_0;
	// UnityEngine.Color[] Mapbox.Examples.Voxels.TextureScale::newColors
	ColorU5BU5D_t941916413* ___newColors_1;
	// System.Int32 Mapbox.Examples.Voxels.TextureScale::w
	int32_t ___w_2;
	// System.Single Mapbox.Examples.Voxels.TextureScale::ratioX
	float ___ratioX_3;
	// System.Single Mapbox.Examples.Voxels.TextureScale::ratioY
	float ___ratioY_4;
	// System.Int32 Mapbox.Examples.Voxels.TextureScale::w2
	int32_t ___w2_5;
	// System.Int32 Mapbox.Examples.Voxels.TextureScale::finishCount
	int32_t ___finishCount_6;
	// System.Threading.Mutex Mapbox.Examples.Voxels.TextureScale::mutex
	Mutex_t3066672582 * ___mutex_7;

public:
	inline static int32_t get_offset_of_texColors_0() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___texColors_0)); }
	inline ColorU5BU5D_t941916413* get_texColors_0() const { return ___texColors_0; }
	inline ColorU5BU5D_t941916413** get_address_of_texColors_0() { return &___texColors_0; }
	inline void set_texColors_0(ColorU5BU5D_t941916413* value)
	{
		___texColors_0 = value;
		Il2CppCodeGenWriteBarrier((&___texColors_0), value);
	}

	inline static int32_t get_offset_of_newColors_1() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___newColors_1)); }
	inline ColorU5BU5D_t941916413* get_newColors_1() const { return ___newColors_1; }
	inline ColorU5BU5D_t941916413** get_address_of_newColors_1() { return &___newColors_1; }
	inline void set_newColors_1(ColorU5BU5D_t941916413* value)
	{
		___newColors_1 = value;
		Il2CppCodeGenWriteBarrier((&___newColors_1), value);
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___w_2)); }
	inline int32_t get_w_2() const { return ___w_2; }
	inline int32_t* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(int32_t value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_ratioX_3() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___ratioX_3)); }
	inline float get_ratioX_3() const { return ___ratioX_3; }
	inline float* get_address_of_ratioX_3() { return &___ratioX_3; }
	inline void set_ratioX_3(float value)
	{
		___ratioX_3 = value;
	}

	inline static int32_t get_offset_of_ratioY_4() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___ratioY_4)); }
	inline float get_ratioY_4() const { return ___ratioY_4; }
	inline float* get_address_of_ratioY_4() { return &___ratioY_4; }
	inline void set_ratioY_4(float value)
	{
		___ratioY_4 = value;
	}

	inline static int32_t get_offset_of_w2_5() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___w2_5)); }
	inline int32_t get_w2_5() const { return ___w2_5; }
	inline int32_t* get_address_of_w2_5() { return &___w2_5; }
	inline void set_w2_5(int32_t value)
	{
		___w2_5 = value;
	}

	inline static int32_t get_offset_of_finishCount_6() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___finishCount_6)); }
	inline int32_t get_finishCount_6() const { return ___finishCount_6; }
	inline int32_t* get_address_of_finishCount_6() { return &___finishCount_6; }
	inline void set_finishCount_6(int32_t value)
	{
		___finishCount_6 = value;
	}

	inline static int32_t get_offset_of_mutex_7() { return static_cast<int32_t>(offsetof(TextureScale_t57896704_StaticFields, ___mutex_7)); }
	inline Mutex_t3066672582 * get_mutex_7() const { return ___mutex_7; }
	inline Mutex_t3066672582 ** get_address_of_mutex_7() { return &___mutex_7; }
	inline void set_mutex_7(Mutex_t3066672582 * value)
	{
		___mutex_7 = value;
		Il2CppCodeGenWriteBarrier((&___mutex_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURESCALE_T57896704_H
#ifndef THREADDATA_T1095464109_H
#define THREADDATA_T1095464109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Voxels.TextureScale/ThreadData
struct  ThreadData_t1095464109  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Examples.Voxels.TextureScale/ThreadData::start
	int32_t ___start_0;
	// System.Int32 Mapbox.Examples.Voxels.TextureScale/ThreadData::end
	int32_t ___end_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(ThreadData_t1095464109, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(ThreadData_t1095464109, ___end_1)); }
	inline int32_t get_end_1() const { return ___end_1; }
	inline int32_t* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(int32_t value)
	{
		___end_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADDATA_T1095464109_H
#ifndef U3CCREATETERRAINHEIGHTU3EC__ANONSTOREY0_T3949184541_H
#define U3CCREATETERRAINHEIGHTU3EC__ANONSTOREY0_T3949184541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.TerrainFactory/<CreateTerrainHeight>c__AnonStorey0
struct  U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541  : public RuntimeObject
{
public:
	// Mapbox.Map.RawPngRasterTile Mapbox.Unity.MeshGeneration.Factories.TerrainFactory/<CreateTerrainHeight>c__AnonStorey0::pngRasterTile
	RawPngRasterTile_t2301039290 * ___pngRasterTile_0;
	// Mapbox.Unity.MeshGeneration.Data.UnityTile Mapbox.Unity.MeshGeneration.Factories.TerrainFactory/<CreateTerrainHeight>c__AnonStorey0::tile
	UnityTile_t2405085845 * ___tile_1;
	// Mapbox.Unity.MeshGeneration.Factories.TerrainFactory Mapbox.Unity.MeshGeneration.Factories.TerrainFactory/<CreateTerrainHeight>c__AnonStorey0::$this
	TerrainFactory_t974472764 * ___U24this_2;

public:
	inline static int32_t get_offset_of_pngRasterTile_0() { return static_cast<int32_t>(offsetof(U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541, ___pngRasterTile_0)); }
	inline RawPngRasterTile_t2301039290 * get_pngRasterTile_0() const { return ___pngRasterTile_0; }
	inline RawPngRasterTile_t2301039290 ** get_address_of_pngRasterTile_0() { return &___pngRasterTile_0; }
	inline void set_pngRasterTile_0(RawPngRasterTile_t2301039290 * value)
	{
		___pngRasterTile_0 = value;
		Il2CppCodeGenWriteBarrier((&___pngRasterTile_0), value);
	}

	inline static int32_t get_offset_of_tile_1() { return static_cast<int32_t>(offsetof(U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541, ___tile_1)); }
	inline UnityTile_t2405085845 * get_tile_1() const { return ___tile_1; }
	inline UnityTile_t2405085845 ** get_address_of_tile_1() { return &___tile_1; }
	inline void set_tile_1(UnityTile_t2405085845 * value)
	{
		___tile_1 = value;
		Il2CppCodeGenWriteBarrier((&___tile_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541, ___U24this_2)); }
	inline TerrainFactory_t974472764 * get_U24this_2() const { return ___U24this_2; }
	inline TerrainFactory_t974472764 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TerrainFactory_t974472764 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATETERRAINHEIGHTU3EC__ANONSTOREY0_T3949184541_H
#ifndef U3CONREGISTEREDU3EC__ANONSTOREY0_T2929291401_H
#define U3CONREGISTEREDU3EC__ANONSTOREY0_T2929291401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory/<OnRegistered>c__AnonStorey0
struct  U3COnRegisteredU3Ec__AnonStorey0_t2929291401  : public RuntimeObject
{
public:
	// Mapbox.Map.VectorTile Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory/<OnRegistered>c__AnonStorey0::vectorTile
	VectorTile_t4284514353 * ___vectorTile_0;
	// Mapbox.Unity.MeshGeneration.Data.UnityTile Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory/<OnRegistered>c__AnonStorey0::tile
	UnityTile_t2405085845 * ___tile_1;
	// Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory/<OnRegistered>c__AnonStorey0::$this
	VectorTileFactory_t815718376 * ___U24this_2;

public:
	inline static int32_t get_offset_of_vectorTile_0() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t2929291401, ___vectorTile_0)); }
	inline VectorTile_t4284514353 * get_vectorTile_0() const { return ___vectorTile_0; }
	inline VectorTile_t4284514353 ** get_address_of_vectorTile_0() { return &___vectorTile_0; }
	inline void set_vectorTile_0(VectorTile_t4284514353 * value)
	{
		___vectorTile_0 = value;
		Il2CppCodeGenWriteBarrier((&___vectorTile_0), value);
	}

	inline static int32_t get_offset_of_tile_1() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t2929291401, ___tile_1)); }
	inline UnityTile_t2405085845 * get_tile_1() const { return ___tile_1; }
	inline UnityTile_t2405085845 ** get_address_of_tile_1() { return &___tile_1; }
	inline void set_tile_1(UnityTile_t2405085845 * value)
	{
		___tile_1 = value;
		Il2CppCodeGenWriteBarrier((&___tile_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t2929291401, ___U24this_2)); }
	inline VectorTileFactory_t815718376 * get_U24this_2() const { return ___U24this_2; }
	inline VectorTileFactory_t815718376 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(VectorTileFactory_t815718376 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONREGISTEREDU3EC__ANONSTOREY0_T2929291401_H
#ifndef TYPEVISUALIZERTUPLE_T633660079_H
#define TYPEVISUALIZERTUPLE_T633660079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Interfaces.TypeVisualizerTuple
struct  TypeVisualizerTuple_t633660079  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Interfaces.TypeVisualizerTuple::Type
	String_t* ___Type_0;
	// Mapbox.Unity.MeshGeneration.Modifiers.ModifierStackBase Mapbox.Unity.MeshGeneration.Interfaces.TypeVisualizerTuple::Stack
	ModifierStackBase_t4180154112 * ___Stack_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(TypeVisualizerTuple_t633660079, ___Type_0)); }
	inline String_t* get_Type_0() const { return ___Type_0; }
	inline String_t** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(String_t* value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Stack_1() { return static_cast<int32_t>(offsetof(TypeVisualizerTuple_t633660079, ___Stack_1)); }
	inline ModifierStackBase_t4180154112 * get_Stack_1() const { return ___Stack_1; }
	inline ModifierStackBase_t4180154112 ** get_address_of_Stack_1() { return &___Stack_1; }
	inline void set_Stack_1(ModifierStackBase_t4180154112 * value)
	{
		___Stack_1 = value;
		Il2CppCodeGenWriteBarrier((&___Stack_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEVISUALIZERTUPLE_T633660079_H
#ifndef U3CBUILDU3EC__ANONSTOREY0_T2244634411_H
#define U3CBUILDU3EC__ANONSTOREY0_T2244634411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer/<Build>c__AnonStorey0
struct  U3CBuildU3Ec__AnonStorey0_t2244634411  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer/<Build>c__AnonStorey0::styleSelectorKey
	String_t* ___styleSelectorKey_0;

public:
	inline static int32_t get_offset_of_styleSelectorKey_0() { return static_cast<int32_t>(offsetof(U3CBuildU3Ec__AnonStorey0_t2244634411, ___styleSelectorKey_0)); }
	inline String_t* get_styleSelectorKey_0() const { return ___styleSelectorKey_0; }
	inline String_t** get_address_of_styleSelectorKey_0() { return &___styleSelectorKey_0; }
	inline void set_styleSelectorKey_0(String_t* value)
	{
		___styleSelectorKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___styleSelectorKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUILDU3EC__ANONSTOREY0_T2244634411_H
#ifndef U3CBUILDROUTINEU3EC__ITERATOR0_T1339467344_H
#define U3CBUILDROUTINEU3EC__ITERATOR0_T1339467344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Voxels.VoxelTile/<BuildRoutine>c__Iterator0
struct  U3CBuildRoutineU3Ec__Iterator0_t1339467344  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Mapbox.Examples.Voxels.VoxelData> Mapbox.Examples.Voxels.VoxelTile/<BuildRoutine>c__Iterator0::<distanceOrderedVoxels>__0
	List_1_t3720956926 * ___U3CdistanceOrderedVoxelsU3E__0_0;
	// System.Int32 Mapbox.Examples.Voxels.VoxelTile/<BuildRoutine>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_1;
	// Mapbox.Examples.Voxels.VoxelTile Mapbox.Examples.Voxels.VoxelTile/<BuildRoutine>c__Iterator0::$this
	VoxelTile_t1944340880 * ___U24this_2;
	// System.Object Mapbox.Examples.Voxels.VoxelTile/<BuildRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Mapbox.Examples.Voxels.VoxelTile/<BuildRoutine>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Mapbox.Examples.Voxels.VoxelTile/<BuildRoutine>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CdistanceOrderedVoxelsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBuildRoutineU3Ec__Iterator0_t1339467344, ___U3CdistanceOrderedVoxelsU3E__0_0)); }
	inline List_1_t3720956926 * get_U3CdistanceOrderedVoxelsU3E__0_0() const { return ___U3CdistanceOrderedVoxelsU3E__0_0; }
	inline List_1_t3720956926 ** get_address_of_U3CdistanceOrderedVoxelsU3E__0_0() { return &___U3CdistanceOrderedVoxelsU3E__0_0; }
	inline void set_U3CdistanceOrderedVoxelsU3E__0_0(List_1_t3720956926 * value)
	{
		___U3CdistanceOrderedVoxelsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdistanceOrderedVoxelsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBuildRoutineU3Ec__Iterator0_t1339467344, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBuildRoutineU3Ec__Iterator0_t1339467344, ___U24this_2)); }
	inline VoxelTile_t1944340880 * get_U24this_2() const { return ___U24this_2; }
	inline VoxelTile_t1944340880 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(VoxelTile_t1944340880 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CBuildRoutineU3Ec__Iterator0_t1339467344, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CBuildRoutineU3Ec__Iterator0_t1339467344, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CBuildRoutineU3Ec__Iterator0_t1339467344, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUILDROUTINEU3EC__ITERATOR0_T1339467344_H
#ifndef CONSTANTS_T2712025141_H
#define CONSTANTS_T2712025141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Constants
struct  Constants_t2712025141  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T2712025141_H
#ifndef PATH_T1402229194_H
#define PATH_T1402229194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Constants/Path
struct  Path_t1402229194  : public RuntimeObject
{
public:

public:
};

struct Path_t1402229194_StaticFields
{
public:
	// System.String Mapbox.Unity.Constants/Path::MAPBOX_RESOURCES_RELATIVE
	String_t* ___MAPBOX_RESOURCES_RELATIVE_4;
	// System.String Mapbox.Unity.Constants/Path::MAPBOX_RESOURCES_ABSOLUTE
	String_t* ___MAPBOX_RESOURCES_ABSOLUTE_5;

public:
	inline static int32_t get_offset_of_MAPBOX_RESOURCES_RELATIVE_4() { return static_cast<int32_t>(offsetof(Path_t1402229194_StaticFields, ___MAPBOX_RESOURCES_RELATIVE_4)); }
	inline String_t* get_MAPBOX_RESOURCES_RELATIVE_4() const { return ___MAPBOX_RESOURCES_RELATIVE_4; }
	inline String_t** get_address_of_MAPBOX_RESOURCES_RELATIVE_4() { return &___MAPBOX_RESOURCES_RELATIVE_4; }
	inline void set_MAPBOX_RESOURCES_RELATIVE_4(String_t* value)
	{
		___MAPBOX_RESOURCES_RELATIVE_4 = value;
		Il2CppCodeGenWriteBarrier((&___MAPBOX_RESOURCES_RELATIVE_4), value);
	}

	inline static int32_t get_offset_of_MAPBOX_RESOURCES_ABSOLUTE_5() { return static_cast<int32_t>(offsetof(Path_t1402229194_StaticFields, ___MAPBOX_RESOURCES_ABSOLUTE_5)); }
	inline String_t* get_MAPBOX_RESOURCES_ABSOLUTE_5() const { return ___MAPBOX_RESOURCES_ABSOLUTE_5; }
	inline String_t** get_address_of_MAPBOX_RESOURCES_ABSOLUTE_5() { return &___MAPBOX_RESOURCES_ABSOLUTE_5; }
	inline void set_MAPBOX_RESOURCES_ABSOLUTE_5(String_t* value)
	{
		___MAPBOX_RESOURCES_ABSOLUTE_5 = value;
		Il2CppCodeGenWriteBarrier((&___MAPBOX_RESOURCES_ABSOLUTE_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T1402229194_H
#ifndef ADDMONOBEHAVIOURSMODIFIERTYPE_T1393761064_H
#define ADDMONOBEHAVIOURSMODIFIERTYPE_T1393761064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.AddMonoBehavioursModifierType
struct  AddMonoBehavioursModifierType_t1393761064  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Modifiers.AddMonoBehavioursModifierType::_typeString
	String_t* ____typeString_0;
	// System.Type Mapbox.Unity.MeshGeneration.Modifiers.AddMonoBehavioursModifierType::_type
	Type_t * ____type_1;

public:
	inline static int32_t get_offset_of__typeString_0() { return static_cast<int32_t>(offsetof(AddMonoBehavioursModifierType_t1393761064, ____typeString_0)); }
	inline String_t* get__typeString_0() const { return ____typeString_0; }
	inline String_t** get_address_of__typeString_0() { return &____typeString_0; }
	inline void set__typeString_0(String_t* value)
	{
		____typeString_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeString_0), value);
	}

	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(AddMonoBehavioursModifierType_t1393761064, ____type_1)); }
	inline Type_t * get__type_1() const { return ____type_1; }
	inline Type_t ** get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(Type_t * value)
	{
		____type_1 = value;
		Il2CppCodeGenWriteBarrier((&____type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDMONOBEHAVIOURSMODIFIERTYPE_T1393761064_H
#ifndef U3CPOLLLOCATIONROUTINEU3EC__ITERATOR0_T2570282366_H
#define U3CPOLLLOCATIONROUTINEU3EC__ITERATOR0_T2570282366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Location.DeviceLocationProvider/<PollLocationRoutine>c__Iterator0
struct  U3CPollLocationRoutineU3Ec__Iterator0_t2570282366  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Unity.Location.DeviceLocationProvider/<PollLocationRoutine>c__Iterator0::<maxWait>__0
	int32_t ___U3CmaxWaitU3E__0_0;
	// System.Double Mapbox.Unity.Location.DeviceLocationProvider/<PollLocationRoutine>c__Iterator0::<timestamp>__1
	double ___U3CtimestampU3E__1_1;
	// Mapbox.Unity.Location.DeviceLocationProvider Mapbox.Unity.Location.DeviceLocationProvider/<PollLocationRoutine>c__Iterator0::$this
	DeviceLocationProvider_t4095080695 * ___U24this_2;
	// System.Object Mapbox.Unity.Location.DeviceLocationProvider/<PollLocationRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Mapbox.Unity.Location.DeviceLocationProvider/<PollLocationRoutine>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Mapbox.Unity.Location.DeviceLocationProvider/<PollLocationRoutine>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CmaxWaitU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPollLocationRoutineU3Ec__Iterator0_t2570282366, ___U3CmaxWaitU3E__0_0)); }
	inline int32_t get_U3CmaxWaitU3E__0_0() const { return ___U3CmaxWaitU3E__0_0; }
	inline int32_t* get_address_of_U3CmaxWaitU3E__0_0() { return &___U3CmaxWaitU3E__0_0; }
	inline void set_U3CmaxWaitU3E__0_0(int32_t value)
	{
		___U3CmaxWaitU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtimestampU3E__1_1() { return static_cast<int32_t>(offsetof(U3CPollLocationRoutineU3Ec__Iterator0_t2570282366, ___U3CtimestampU3E__1_1)); }
	inline double get_U3CtimestampU3E__1_1() const { return ___U3CtimestampU3E__1_1; }
	inline double* get_address_of_U3CtimestampU3E__1_1() { return &___U3CtimestampU3E__1_1; }
	inline void set_U3CtimestampU3E__1_1(double value)
	{
		___U3CtimestampU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CPollLocationRoutineU3Ec__Iterator0_t2570282366, ___U24this_2)); }
	inline DeviceLocationProvider_t4095080695 * get_U24this_2() const { return ___U24this_2; }
	inline DeviceLocationProvider_t4095080695 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(DeviceLocationProvider_t4095080695 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CPollLocationRoutineU3Ec__Iterator0_t2570282366, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CPollLocationRoutineU3Ec__Iterator0_t2570282366, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPollLocationRoutineU3Ec__Iterator0_t2570282366, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOLLLOCATIONROUTINEU3EC__ITERATOR0_T2570282366_H
#ifndef MAPBOXACCESS_T3460807032_H
#define MAPBOXACCESS_T3460807032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MapboxAccess
struct  MapboxAccess_t3460807032  : public RuntimeObject
{
public:
	// Mapbox.Unity.Telemetry.ITelemetryLibrary Mapbox.Unity.MapboxAccess::_telemetryLibrary
	RuntimeObject* ____telemetryLibrary_0;
	// Mapbox.Platform.Cache.CachingWebFileSource Mapbox.Unity.MapboxAccess::_fileSource
	CachingWebFileSource_t328893056 * ____fileSource_1;
	// Mapbox.Unity.MapboxConfiguration Mapbox.Unity.MapboxAccess::_configuration
	MapboxConfiguration_t2330578778 * ____configuration_3;
	// Mapbox.Geocoding.Geocoder Mapbox.Unity.MapboxAccess::_geocoder
	Geocoder_t3195298050 * ____geocoder_4;
	// Mapbox.Directions.Directions Mapbox.Unity.MapboxAccess::_directions
	Directions_t1397515081 * ____directions_5;

public:
	inline static int32_t get_offset_of__telemetryLibrary_0() { return static_cast<int32_t>(offsetof(MapboxAccess_t3460807032, ____telemetryLibrary_0)); }
	inline RuntimeObject* get__telemetryLibrary_0() const { return ____telemetryLibrary_0; }
	inline RuntimeObject** get_address_of__telemetryLibrary_0() { return &____telemetryLibrary_0; }
	inline void set__telemetryLibrary_0(RuntimeObject* value)
	{
		____telemetryLibrary_0 = value;
		Il2CppCodeGenWriteBarrier((&____telemetryLibrary_0), value);
	}

	inline static int32_t get_offset_of__fileSource_1() { return static_cast<int32_t>(offsetof(MapboxAccess_t3460807032, ____fileSource_1)); }
	inline CachingWebFileSource_t328893056 * get__fileSource_1() const { return ____fileSource_1; }
	inline CachingWebFileSource_t328893056 ** get_address_of__fileSource_1() { return &____fileSource_1; }
	inline void set__fileSource_1(CachingWebFileSource_t328893056 * value)
	{
		____fileSource_1 = value;
		Il2CppCodeGenWriteBarrier((&____fileSource_1), value);
	}

	inline static int32_t get_offset_of__configuration_3() { return static_cast<int32_t>(offsetof(MapboxAccess_t3460807032, ____configuration_3)); }
	inline MapboxConfiguration_t2330578778 * get__configuration_3() const { return ____configuration_3; }
	inline MapboxConfiguration_t2330578778 ** get_address_of__configuration_3() { return &____configuration_3; }
	inline void set__configuration_3(MapboxConfiguration_t2330578778 * value)
	{
		____configuration_3 = value;
		Il2CppCodeGenWriteBarrier((&____configuration_3), value);
	}

	inline static int32_t get_offset_of__geocoder_4() { return static_cast<int32_t>(offsetof(MapboxAccess_t3460807032, ____geocoder_4)); }
	inline Geocoder_t3195298050 * get__geocoder_4() const { return ____geocoder_4; }
	inline Geocoder_t3195298050 ** get_address_of__geocoder_4() { return &____geocoder_4; }
	inline void set__geocoder_4(Geocoder_t3195298050 * value)
	{
		____geocoder_4 = value;
		Il2CppCodeGenWriteBarrier((&____geocoder_4), value);
	}

	inline static int32_t get_offset_of__directions_5() { return static_cast<int32_t>(offsetof(MapboxAccess_t3460807032, ____directions_5)); }
	inline Directions_t1397515081 * get__directions_5() const { return ____directions_5; }
	inline Directions_t1397515081 ** get_address_of__directions_5() { return &____directions_5; }
	inline void set__directions_5(Directions_t1397515081 * value)
	{
		____directions_5 = value;
		Il2CppCodeGenWriteBarrier((&____directions_5), value);
	}
};

struct MapboxAccess_t3460807032_StaticFields
{
public:
	// Mapbox.Unity.MapboxAccess Mapbox.Unity.MapboxAccess::_instance
	MapboxAccess_t3460807032 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(MapboxAccess_t3460807032_StaticFields, ____instance_2)); }
	inline MapboxAccess_t3460807032 * get__instance_2() const { return ____instance_2; }
	inline MapboxAccess_t3460807032 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(MapboxAccess_t3460807032 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPBOXACCESS_T3460807032_H
#ifndef U3CCREATETERRAINHEIGHTU3EC__ANONSTOREY0_T1976526639_H
#define U3CCREATETERRAINHEIGHTU3EC__ANONSTOREY0_T1976526639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory/<CreateTerrainHeight>c__AnonStorey0
struct  U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639  : public RuntimeObject
{
public:
	// Mapbox.Map.RawPngRasterTile Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory/<CreateTerrainHeight>c__AnonStorey0::pngRasterTile
	RawPngRasterTile_t2301039290 * ___pngRasterTile_0;
	// Mapbox.Unity.MeshGeneration.Data.UnityTile Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory/<CreateTerrainHeight>c__AnonStorey0::tile
	UnityTile_t2405085845 * ___tile_1;
	// Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory/<CreateTerrainHeight>c__AnonStorey0::$this
	LowPolyTerrainFactory_t2718546587 * ___U24this_2;

public:
	inline static int32_t get_offset_of_pngRasterTile_0() { return static_cast<int32_t>(offsetof(U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639, ___pngRasterTile_0)); }
	inline RawPngRasterTile_t2301039290 * get_pngRasterTile_0() const { return ___pngRasterTile_0; }
	inline RawPngRasterTile_t2301039290 ** get_address_of_pngRasterTile_0() { return &___pngRasterTile_0; }
	inline void set_pngRasterTile_0(RawPngRasterTile_t2301039290 * value)
	{
		___pngRasterTile_0 = value;
		Il2CppCodeGenWriteBarrier((&___pngRasterTile_0), value);
	}

	inline static int32_t get_offset_of_tile_1() { return static_cast<int32_t>(offsetof(U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639, ___tile_1)); }
	inline UnityTile_t2405085845 * get_tile_1() const { return ___tile_1; }
	inline UnityTile_t2405085845 ** get_address_of_tile_1() { return &___tile_1; }
	inline void set_tile_1(UnityTile_t2405085845 * value)
	{
		___tile_1 = value;
		Il2CppCodeGenWriteBarrier((&___tile_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639, ___U24this_2)); }
	inline LowPolyTerrainFactory_t2718546587 * get_U24this_2() const { return ___U24this_2; }
	inline LowPolyTerrainFactory_t2718546587 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(LowPolyTerrainFactory_t2718546587 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATETERRAINHEIGHTU3EC__ANONSTOREY0_T1976526639_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VECTORFEATUREUNITY_T1815898701_H
#define VECTORFEATUREUNITY_T1815898701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Data.VectorFeatureUnity
struct  VectorFeatureUnity_t1815898701  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.VectorTileFeature Mapbox.Unity.MeshGeneration.Data.VectorFeatureUnity::<Data>k__BackingField
	VectorTileFeature_t4093669591 * ___U3CDataU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Mapbox.Unity.MeshGeneration.Data.VectorFeatureUnity::<Properties>k__BackingField
	Dictionary_2_t2865362463 * ___U3CPropertiesU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>> Mapbox.Unity.MeshGeneration.Data.VectorFeatureUnity::Points
	List_1_t2371495652 * ___Points_2;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VectorFeatureUnity_t1815898701, ___U3CDataU3Ek__BackingField_0)); }
	inline VectorTileFeature_t4093669591 * get_U3CDataU3Ek__BackingField_0() const { return ___U3CDataU3Ek__BackingField_0; }
	inline VectorTileFeature_t4093669591 ** get_address_of_U3CDataU3Ek__BackingField_0() { return &___U3CDataU3Ek__BackingField_0; }
	inline void set_U3CDataU3Ek__BackingField_0(VectorTileFeature_t4093669591 * value)
	{
		___U3CDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VectorFeatureUnity_t1815898701, ___U3CPropertiesU3Ek__BackingField_1)); }
	inline Dictionary_2_t2865362463 * get_U3CPropertiesU3Ek__BackingField_1() const { return ___U3CPropertiesU3Ek__BackingField_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CPropertiesU3Ek__BackingField_1() { return &___U3CPropertiesU3Ek__BackingField_1; }
	inline void set_U3CPropertiesU3Ek__BackingField_1(Dictionary_2_t2865362463 * value)
	{
		___U3CPropertiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_Points_2() { return static_cast<int32_t>(offsetof(VectorFeatureUnity_t1815898701, ___Points_2)); }
	inline List_1_t2371495652 * get_Points_2() const { return ___Points_2; }
	inline List_1_t2371495652 ** get_address_of_Points_2() { return &___Points_2; }
	inline void set_Points_2(List_1_t2371495652 * value)
	{
		___Points_2 = value;
		Il2CppCodeGenWriteBarrier((&___Points_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORFEATUREUNITY_T1815898701_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CONREGISTEREDU3EC__ANONSTOREY0_T833595815_H
#define U3CONREGISTEREDU3EC__ANONSTOREY0_T833595815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.MapImageFactory/<OnRegistered>c__AnonStorey0
struct  U3COnRegisteredU3Ec__AnonStorey0_t833595815  : public RuntimeObject
{
public:
	// Mapbox.Map.RasterTile Mapbox.Unity.MeshGeneration.Factories.MapImageFactory/<OnRegistered>c__AnonStorey0::rasterTile
	RasterTile_t2895285341 * ___rasterTile_0;
	// Mapbox.Unity.MeshGeneration.Data.UnityTile Mapbox.Unity.MeshGeneration.Factories.MapImageFactory/<OnRegistered>c__AnonStorey0::tile
	UnityTile_t2405085845 * ___tile_1;
	// Mapbox.Unity.MeshGeneration.Factories.MapImageFactory Mapbox.Unity.MeshGeneration.Factories.MapImageFactory/<OnRegistered>c__AnonStorey0::$this
	MapImageFactory_t200237364 * ___U24this_2;

public:
	inline static int32_t get_offset_of_rasterTile_0() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t833595815, ___rasterTile_0)); }
	inline RasterTile_t2895285341 * get_rasterTile_0() const { return ___rasterTile_0; }
	inline RasterTile_t2895285341 ** get_address_of_rasterTile_0() { return &___rasterTile_0; }
	inline void set_rasterTile_0(RasterTile_t2895285341 * value)
	{
		___rasterTile_0 = value;
		Il2CppCodeGenWriteBarrier((&___rasterTile_0), value);
	}

	inline static int32_t get_offset_of_tile_1() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t833595815, ___tile_1)); }
	inline UnityTile_t2405085845 * get_tile_1() const { return ___tile_1; }
	inline UnityTile_t2405085845 ** get_address_of_tile_1() { return &___tile_1; }
	inline void set_tile_1(UnityTile_t2405085845 * value)
	{
		___tile_1 = value;
		Il2CppCodeGenWriteBarrier((&___tile_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t833595815, ___U24this_2)); }
	inline MapImageFactory_t200237364 * get_U24this_2() const { return ___U24this_2; }
	inline MapImageFactory_t200237364 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MapImageFactory_t200237364 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONREGISTEREDU3EC__ANONSTOREY0_T833595815_H
#ifndef U3CONREGISTEREDU3EC__ANONSTOREY0_T2918131947_H
#define U3CONREGISTEREDU3EC__ANONSTOREY0_T2918131947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.MeshFactory/<OnRegistered>c__AnonStorey0
struct  U3COnRegisteredU3Ec__AnonStorey0_t2918131947  : public RuntimeObject
{
public:
	// Mapbox.Map.VectorTile Mapbox.Unity.MeshGeneration.Factories.MeshFactory/<OnRegistered>c__AnonStorey0::vectorTile
	VectorTile_t4284514353 * ___vectorTile_0;
	// Mapbox.Unity.MeshGeneration.Data.UnityTile Mapbox.Unity.MeshGeneration.Factories.MeshFactory/<OnRegistered>c__AnonStorey0::tile
	UnityTile_t2405085845 * ___tile_1;
	// Mapbox.Unity.MeshGeneration.Factories.MeshFactory Mapbox.Unity.MeshGeneration.Factories.MeshFactory/<OnRegistered>c__AnonStorey0::$this
	MeshFactory_t3379932301 * ___U24this_2;

public:
	inline static int32_t get_offset_of_vectorTile_0() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t2918131947, ___vectorTile_0)); }
	inline VectorTile_t4284514353 * get_vectorTile_0() const { return ___vectorTile_0; }
	inline VectorTile_t4284514353 ** get_address_of_vectorTile_0() { return &___vectorTile_0; }
	inline void set_vectorTile_0(VectorTile_t4284514353 * value)
	{
		___vectorTile_0 = value;
		Il2CppCodeGenWriteBarrier((&___vectorTile_0), value);
	}

	inline static int32_t get_offset_of_tile_1() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t2918131947, ___tile_1)); }
	inline UnityTile_t2405085845 * get_tile_1() const { return ___tile_1; }
	inline UnityTile_t2405085845 ** get_address_of_tile_1() { return &___tile_1; }
	inline void set_tile_1(UnityTile_t2405085845 * value)
	{
		___tile_1 = value;
		Il2CppCodeGenWriteBarrier((&___tile_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t2918131947, ___U24this_2)); }
	inline MeshFactory_t3379932301 * get_U24this_2() const { return ___U24this_2; }
	inline MeshFactory_t3379932301 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MeshFactory_t3379932301 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONREGISTEREDU3EC__ANONSTOREY0_T2918131947_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MAPBOXCONFIGURATION_T2330578778_H
#define MAPBOXCONFIGURATION_T2330578778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MapboxConfiguration
struct  MapboxConfiguration_t2330578778  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.MapboxConfiguration::AccessToken
	String_t* ___AccessToken_0;
	// System.UInt32 Mapbox.Unity.MapboxConfiguration::MemoryCacheSize
	uint32_t ___MemoryCacheSize_1;
	// System.UInt32 Mapbox.Unity.MapboxConfiguration::MbTilesCacheSize
	uint32_t ___MbTilesCacheSize_2;
	// System.Int32 Mapbox.Unity.MapboxConfiguration::DefaultTimeout
	int32_t ___DefaultTimeout_3;

public:
	inline static int32_t get_offset_of_AccessToken_0() { return static_cast<int32_t>(offsetof(MapboxConfiguration_t2330578778, ___AccessToken_0)); }
	inline String_t* get_AccessToken_0() const { return ___AccessToken_0; }
	inline String_t** get_address_of_AccessToken_0() { return &___AccessToken_0; }
	inline void set_AccessToken_0(String_t* value)
	{
		___AccessToken_0 = value;
		Il2CppCodeGenWriteBarrier((&___AccessToken_0), value);
	}

	inline static int32_t get_offset_of_MemoryCacheSize_1() { return static_cast<int32_t>(offsetof(MapboxConfiguration_t2330578778, ___MemoryCacheSize_1)); }
	inline uint32_t get_MemoryCacheSize_1() const { return ___MemoryCacheSize_1; }
	inline uint32_t* get_address_of_MemoryCacheSize_1() { return &___MemoryCacheSize_1; }
	inline void set_MemoryCacheSize_1(uint32_t value)
	{
		___MemoryCacheSize_1 = value;
	}

	inline static int32_t get_offset_of_MbTilesCacheSize_2() { return static_cast<int32_t>(offsetof(MapboxConfiguration_t2330578778, ___MbTilesCacheSize_2)); }
	inline uint32_t get_MbTilesCacheSize_2() const { return ___MbTilesCacheSize_2; }
	inline uint32_t* get_address_of_MbTilesCacheSize_2() { return &___MbTilesCacheSize_2; }
	inline void set_MbTilesCacheSize_2(uint32_t value)
	{
		___MbTilesCacheSize_2 = value;
	}

	inline static int32_t get_offset_of_DefaultTimeout_3() { return static_cast<int32_t>(offsetof(MapboxConfiguration_t2330578778, ___DefaultTimeout_3)); }
	inline int32_t get_DefaultTimeout_3() const { return ___DefaultTimeout_3; }
	inline int32_t* get_address_of_DefaultTimeout_3() { return &___DefaultTimeout_3; }
	inline void set_DefaultTimeout_3(int32_t value)
	{
		___DefaultTimeout_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPBOXCONFIGURATION_T2330578778_H
#ifndef STYLE_T2684836192_H
#define STYLE_T2684836192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.Style
struct  Style_t2684836192  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Factories.Style::Name
	String_t* ___Name_0;
	// System.String Mapbox.Unity.MeshGeneration.Factories.Style::Id
	String_t* ___Id_1;
	// System.String Mapbox.Unity.MeshGeneration.Factories.Style::Modified
	String_t* ___Modified_2;
	// System.String Mapbox.Unity.MeshGeneration.Factories.Style::UserName
	String_t* ___UserName_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Style_t2684836192, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(Style_t2684836192, ___Id_1)); }
	inline String_t* get_Id_1() const { return ___Id_1; }
	inline String_t** get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(String_t* value)
	{
		___Id_1 = value;
		Il2CppCodeGenWriteBarrier((&___Id_1), value);
	}

	inline static int32_t get_offset_of_Modified_2() { return static_cast<int32_t>(offsetof(Style_t2684836192, ___Modified_2)); }
	inline String_t* get_Modified_2() const { return ___Modified_2; }
	inline String_t** get_address_of_Modified_2() { return &___Modified_2; }
	inline void set_Modified_2(String_t* value)
	{
		___Modified_2 = value;
		Il2CppCodeGenWriteBarrier((&___Modified_2), value);
	}

	inline static int32_t get_offset_of_UserName_3() { return static_cast<int32_t>(offsetof(Style_t2684836192, ___UserName_3)); }
	inline String_t* get_UserName_3() const { return ___UserName_3; }
	inline String_t** get_address_of_UserName_3() { return &___UserName_3; }
	inline void set_UserName_3(String_t* value)
	{
		___UserName_3 = value;
		Il2CppCodeGenWriteBarrier((&___UserName_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLE_T2684836192_H
#ifndef U3CONREGISTEREDU3EC__ANONSTOREY0_T25107931_H
#define U3CONREGISTEREDU3EC__ANONSTOREY0_T25107931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory/<OnRegistered>c__AnonStorey0
struct  U3COnRegisteredU3Ec__AnonStorey0_t25107931  : public RuntimeObject
{
public:
	// Mapbox.Map.StyleOptimizedVectorTile Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory/<OnRegistered>c__AnonStorey0::vectorTile
	StyleOptimizedVectorTile_t1819232013 * ___vectorTile_0;
	// Mapbox.Unity.MeshGeneration.Data.UnityTile Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory/<OnRegistered>c__AnonStorey0::tile
	UnityTile_t2405085845 * ___tile_1;
	// Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory/<OnRegistered>c__AnonStorey0::$this
	StyleOptimizedVectorTileFactory_t1939249911 * ___U24this_2;

public:
	inline static int32_t get_offset_of_vectorTile_0() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t25107931, ___vectorTile_0)); }
	inline StyleOptimizedVectorTile_t1819232013 * get_vectorTile_0() const { return ___vectorTile_0; }
	inline StyleOptimizedVectorTile_t1819232013 ** get_address_of_vectorTile_0() { return &___vectorTile_0; }
	inline void set_vectorTile_0(StyleOptimizedVectorTile_t1819232013 * value)
	{
		___vectorTile_0 = value;
		Il2CppCodeGenWriteBarrier((&___vectorTile_0), value);
	}

	inline static int32_t get_offset_of_tile_1() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t25107931, ___tile_1)); }
	inline UnityTile_t2405085845 * get_tile_1() const { return ___tile_1; }
	inline UnityTile_t2405085845 ** get_address_of_tile_1() { return &___tile_1; }
	inline void set_tile_1(UnityTile_t2405085845 * value)
	{
		___tile_1 = value;
		Il2CppCodeGenWriteBarrier((&___tile_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COnRegisteredU3Ec__AnonStorey0_t25107931, ___U24this_2)); }
	inline StyleOptimizedVectorTileFactory_t1939249911 * get_U24this_2() const { return ___U24this_2; }
	inline StyleOptimizedVectorTileFactory_t1939249911 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(StyleOptimizedVectorTileFactory_t1939249911 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONREGISTEREDU3EC__ANONSTOREY0_T25107931_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2D_T1865246568_H
#define VECTOR2D_T1865246568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Vector2d
struct  Vector2d_t1865246568 
{
public:
	// System.Double Mapbox.Utils.Vector2d::x
	double ___x_1;
	// System.Double Mapbox.Utils.Vector2d::y
	double ___y_2;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___x_1)); }
	inline double get_x_1() const { return ___x_1; }
	inline double* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(double value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___y_2)); }
	inline double get_y_2() const { return ___y_2; }
	inline double* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(double value)
	{
		___y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2D_T1865246568_H
#ifndef INVALIDTOKENEXCEPTION_T1836971595_H
#define INVALIDTOKENEXCEPTION_T1836971595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MapboxAccess/InvalidTokenException
struct  InvalidTokenException_t1836971595  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDTOKENEXCEPTION_T1836971595_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef UNWRAPPEDTILEID_T2586853537_H
#define UNWRAPPEDTILEID_T2586853537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.UnwrappedTileId
struct  UnwrappedTileId_t2586853537 
{
public:
	// System.Int32 Mapbox.Map.UnwrappedTileId::Z
	int32_t ___Z_0;
	// System.Int32 Mapbox.Map.UnwrappedTileId::X
	int32_t ___X_1;
	// System.Int32 Mapbox.Map.UnwrappedTileId::Y
	int32_t ___Y_2;

public:
	inline static int32_t get_offset_of_Z_0() { return static_cast<int32_t>(offsetof(UnwrappedTileId_t2586853537, ___Z_0)); }
	inline int32_t get_Z_0() const { return ___Z_0; }
	inline int32_t* get_address_of_Z_0() { return &___Z_0; }
	inline void set_Z_0(int32_t value)
	{
		___Z_0 = value;
	}

	inline static int32_t get_offset_of_X_1() { return static_cast<int32_t>(offsetof(UnwrappedTileId_t2586853537, ___X_1)); }
	inline int32_t get_X_1() const { return ___X_1; }
	inline int32_t* get_address_of_X_1() { return &___X_1; }
	inline void set_X_1(int32_t value)
	{
		___X_1 = value;
	}

	inline static int32_t get_offset_of_Y_2() { return static_cast<int32_t>(offsetof(UnwrappedTileId_t2586853537, ___Y_2)); }
	inline int32_t get_Y_2() const { return ___Y_2; }
	inline int32_t* get_address_of_Y_2() { return &___Y_2; }
	inline void set_Y_2(int32_t value)
	{
		___Y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNWRAPPEDTILEID_T2586853537_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef CANONICALTILEID_T4184902996_H
#define CANONICALTILEID_T4184902996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.CanonicalTileId
struct  CanonicalTileId_t4184902996 
{
public:
	// System.Int32 Mapbox.Map.CanonicalTileId::Z
	int32_t ___Z_0;
	// System.Int32 Mapbox.Map.CanonicalTileId::X
	int32_t ___X_1;
	// System.Int32 Mapbox.Map.CanonicalTileId::Y
	int32_t ___Y_2;

public:
	inline static int32_t get_offset_of_Z_0() { return static_cast<int32_t>(offsetof(CanonicalTileId_t4184902996, ___Z_0)); }
	inline int32_t get_Z_0() const { return ___Z_0; }
	inline int32_t* get_address_of_Z_0() { return &___Z_0; }
	inline void set_Z_0(int32_t value)
	{
		___Z_0 = value;
	}

	inline static int32_t get_offset_of_X_1() { return static_cast<int32_t>(offsetof(CanonicalTileId_t4184902996, ___X_1)); }
	inline int32_t get_X_1() const { return ___X_1; }
	inline int32_t* get_address_of_X_1() { return &___X_1; }
	inline void set_X_1(int32_t value)
	{
		___X_1 = value;
	}

	inline static int32_t get_offset_of_Y_2() { return static_cast<int32_t>(offsetof(CanonicalTileId_t4184902996, ___Y_2)); }
	inline int32_t get_Y_2() const { return ___Y_2; }
	inline int32_t* get_address_of_Y_2() { return &___Y_2; }
	inline void set_Y_2(int32_t value)
	{
		___Y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANONICALTILEID_T4184902996_H
#ifndef HEADINGUPDATEDEVENTARGS_T743080610_H
#define HEADINGUPDATEDEVENTARGS_T743080610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Location.HeadingUpdatedEventArgs
struct  HeadingUpdatedEventArgs_t743080610  : public EventArgs_t3591816995
{
public:
	// System.Single Mapbox.Unity.Location.HeadingUpdatedEventArgs::Heading
	float ___Heading_1;

public:
	inline static int32_t get_offset_of_Heading_1() { return static_cast<int32_t>(offsetof(HeadingUpdatedEventArgs_t743080610, ___Heading_1)); }
	inline float get_Heading_1() const { return ___Heading_1; }
	inline float* get_address_of_Heading_1() { return &___Heading_1; }
	inline void set_Heading_1(float value)
	{
		___Heading_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADINGUPDATEDEVENTARGS_T743080610_H
#ifndef MAPIDTYPE_T41340012_H
#define MAPIDTYPE_T41340012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.MapIdType
struct  MapIdType_t41340012 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.MapIdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MapIdType_t41340012, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPIDTYPE_T41340012_H
#ifndef ASSIGNMENTTYPEATTRIBUTE_T3305162592_H
#define ASSIGNMENTTYPEATTRIBUTE_T3305162592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.AssignmentTypeAttribute
struct  AssignmentTypeAttribute_t3305162592  : public PropertyAttribute_t3677895545
{
public:
	// System.Type Mapbox.Unity.Map.AssignmentTypeAttribute::Type
	Type_t * ___Type_0;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(AssignmentTypeAttribute_t3305162592, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIGNMENTTYPEATTRIBUTE_T3305162592_H
#ifndef MAPIMAGETYPE_T1139904885_H
#define MAPIMAGETYPE_T1139904885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.MapImageType
struct  MapImageType_t1139904885 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.MapImageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MapImageType_t1139904885, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPIMAGETYPE_T1139904885_H
#ifndef VECTOR2DBOUNDS_T1974840945_H
#define VECTOR2DBOUNDS_T1974840945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Vector2dBounds
struct  Vector2dBounds_t1974840945 
{
public:
	// Mapbox.Utils.Vector2d Mapbox.Utils.Vector2dBounds::SouthWest
	Vector2d_t1865246568  ___SouthWest_0;
	// Mapbox.Utils.Vector2d Mapbox.Utils.Vector2dBounds::NorthEast
	Vector2d_t1865246568  ___NorthEast_1;

public:
	inline static int32_t get_offset_of_SouthWest_0() { return static_cast<int32_t>(offsetof(Vector2dBounds_t1974840945, ___SouthWest_0)); }
	inline Vector2d_t1865246568  get_SouthWest_0() const { return ___SouthWest_0; }
	inline Vector2d_t1865246568 * get_address_of_SouthWest_0() { return &___SouthWest_0; }
	inline void set_SouthWest_0(Vector2d_t1865246568  value)
	{
		___SouthWest_0 = value;
	}

	inline static int32_t get_offset_of_NorthEast_1() { return static_cast<int32_t>(offsetof(Vector2dBounds_t1974840945, ___NorthEast_1)); }
	inline Vector2d_t1865246568  get_NorthEast_1() const { return ___NorthEast_1; }
	inline Vector2d_t1865246568 * get_address_of_NorthEast_1() { return &___NorthEast_1; }
	inline void set_NorthEast_1(Vector2d_t1865246568  value)
	{
		___NorthEast_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2DBOUNDS_T1974840945_H
#ifndef VOXELDATA_T2248882184_H
#define VOXELDATA_T2248882184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Voxels.VoxelData
struct  VoxelData_t2248882184  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Mapbox.Examples.Voxels.VoxelData::Position
	Vector3_t3722313464  ___Position_0;
	// UnityEngine.GameObject Mapbox.Examples.Voxels.VoxelData::Prefab
	GameObject_t1113636619 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(VoxelData_t2248882184, ___Position_0)); }
	inline Vector3_t3722313464  get_Position_0() const { return ___Position_0; }
	inline Vector3_t3722313464 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t3722313464  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(VoxelData_t2248882184, ___Prefab_1)); }
	inline GameObject_t1113636619 * get_Prefab_1() const { return ___Prefab_1; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(GameObject_t1113636619 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELDATA_T2248882184_H
#ifndef TILEPROPERTYSTATE_T855605749_H
#define TILEPROPERTYSTATE_T855605749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Enums.TilePropertyState
struct  TilePropertyState_t855605749 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Enums.TilePropertyState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TilePropertyState_t855605749, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEPROPERTYSTATE_T855605749_H
#ifndef RECTD_T151583371_H
#define RECTD_T151583371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.RectD
struct  RectD_t151583371 
{
public:
	union
	{
		struct
		{
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Min>k__BackingField
			Vector2d_t1865246568  ___U3CMinU3Ek__BackingField_0;
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Max>k__BackingField
			Vector2d_t1865246568  ___U3CMaxU3Ek__BackingField_1;
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Size>k__BackingField
			Vector2d_t1865246568  ___U3CSizeU3Ek__BackingField_2;
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Center>k__BackingField
			Vector2d_t1865246568  ___U3CCenterU3Ek__BackingField_3;
		};
		uint8_t RectD_t151583371__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CMinU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CMinU3Ek__BackingField_0)); }
	inline Vector2d_t1865246568  get_U3CMinU3Ek__BackingField_0() const { return ___U3CMinU3Ek__BackingField_0; }
	inline Vector2d_t1865246568 * get_address_of_U3CMinU3Ek__BackingField_0() { return &___U3CMinU3Ek__BackingField_0; }
	inline void set_U3CMinU3Ek__BackingField_0(Vector2d_t1865246568  value)
	{
		___U3CMinU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CMaxU3Ek__BackingField_1)); }
	inline Vector2d_t1865246568  get_U3CMaxU3Ek__BackingField_1() const { return ___U3CMaxU3Ek__BackingField_1; }
	inline Vector2d_t1865246568 * get_address_of_U3CMaxU3Ek__BackingField_1() { return &___U3CMaxU3Ek__BackingField_1; }
	inline void set_U3CMaxU3Ek__BackingField_1(Vector2d_t1865246568  value)
	{
		___U3CMaxU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CSizeU3Ek__BackingField_2)); }
	inline Vector2d_t1865246568  get_U3CSizeU3Ek__BackingField_2() const { return ___U3CSizeU3Ek__BackingField_2; }
	inline Vector2d_t1865246568 * get_address_of_U3CSizeU3Ek__BackingField_2() { return &___U3CSizeU3Ek__BackingField_2; }
	inline void set_U3CSizeU3Ek__BackingField_2(Vector2d_t1865246568  value)
	{
		___U3CSizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CCenterU3Ek__BackingField_3)); }
	inline Vector2d_t1865246568  get_U3CCenterU3Ek__BackingField_3() const { return ___U3CCenterU3Ek__BackingField_3; }
	inline Vector2d_t1865246568 * get_address_of_U3CCenterU3Ek__BackingField_3() { return &___U3CCenterU3Ek__BackingField_3; }
	inline void set_U3CCenterU3Ek__BackingField_3(Vector2d_t1865246568  value)
	{
		___U3CCenterU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTD_T151583371_H
#ifndef MODULESTATE_T1890103724_H
#define MODULESTATE_T1890103724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.ModuleState
struct  ModuleState_t1890103724 
{
public:
	// System.Int32 Mapbox.Unity.Map.ModuleState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ModuleState_t1890103724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULESTATE_T1890103724_H
#ifndef LOCATIONUPDATEDEVENTARGS_T3804910274_H
#define LOCATIONUPDATEDEVENTARGS_T3804910274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Location.LocationUpdatedEventArgs
struct  LocationUpdatedEventArgs_t3804910274  : public EventArgs_t3591816995
{
public:
	// Mapbox.Utils.Vector2d Mapbox.Unity.Location.LocationUpdatedEventArgs::Location
	Vector2d_t1865246568  ___Location_1;

public:
	inline static int32_t get_offset_of_Location_1() { return static_cast<int32_t>(offsetof(LocationUpdatedEventArgs_t3804910274, ___Location_1)); }
	inline Vector2d_t1865246568  get_Location_1() const { return ___Location_1; }
	inline Vector2d_t1865246568 * get_address_of_Location_1() { return &___Location_1; }
	inline void set_Location_1(Vector2d_t1865246568  value)
	{
		___Location_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATIONUPDATEDEVENTARGS_T3804910274_H
#ifndef PLANE_T1000493321_H
#define PLANE_T1000493321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1000493321 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3722313464  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Normal_0)); }
	inline Vector3_t3722313464  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3722313464  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1000493321_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef COLLIDERTYPE_T2253951828_H
#define COLLIDERTYPE_T2253951828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ColliderModifier/ColliderType
struct  ColliderType_t2253951828 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.ColliderModifier/ColliderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColliderType_t2253951828, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERTYPE_T2253951828_H
#ifndef MATH_T2875019722_H
#define MATH_T2875019722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Constants/Math
struct  Math_t2875019722  : public RuntimeObject
{
public:

public:
};

struct Math_t2875019722_StaticFields
{
public:
	// UnityEngine.Vector3 Mapbox.Unity.Constants/Math::Vector3Zero
	Vector3_t3722313464  ___Vector3Zero_0;
	// UnityEngine.Vector3 Mapbox.Unity.Constants/Math::Vector3Up
	Vector3_t3722313464  ___Vector3Up_1;
	// UnityEngine.Vector3 Mapbox.Unity.Constants/Math::Vector3Down
	Vector3_t3722313464  ___Vector3Down_2;
	// UnityEngine.Vector3 Mapbox.Unity.Constants/Math::Vector3One
	Vector3_t3722313464  ___Vector3One_3;

public:
	inline static int32_t get_offset_of_Vector3Zero_0() { return static_cast<int32_t>(offsetof(Math_t2875019722_StaticFields, ___Vector3Zero_0)); }
	inline Vector3_t3722313464  get_Vector3Zero_0() const { return ___Vector3Zero_0; }
	inline Vector3_t3722313464 * get_address_of_Vector3Zero_0() { return &___Vector3Zero_0; }
	inline void set_Vector3Zero_0(Vector3_t3722313464  value)
	{
		___Vector3Zero_0 = value;
	}

	inline static int32_t get_offset_of_Vector3Up_1() { return static_cast<int32_t>(offsetof(Math_t2875019722_StaticFields, ___Vector3Up_1)); }
	inline Vector3_t3722313464  get_Vector3Up_1() const { return ___Vector3Up_1; }
	inline Vector3_t3722313464 * get_address_of_Vector3Up_1() { return &___Vector3Up_1; }
	inline void set_Vector3Up_1(Vector3_t3722313464  value)
	{
		___Vector3Up_1 = value;
	}

	inline static int32_t get_offset_of_Vector3Down_2() { return static_cast<int32_t>(offsetof(Math_t2875019722_StaticFields, ___Vector3Down_2)); }
	inline Vector3_t3722313464  get_Vector3Down_2() const { return ___Vector3Down_2; }
	inline Vector3_t3722313464 * get_address_of_Vector3Down_2() { return &___Vector3Down_2; }
	inline void set_Vector3Down_2(Vector3_t3722313464  value)
	{
		___Vector3Down_2 = value;
	}

	inline static int32_t get_offset_of_Vector3One_3() { return static_cast<int32_t>(offsetof(Math_t2875019722_StaticFields, ___Vector3One_3)); }
	inline Vector3_t3722313464  get_Vector3One_3() const { return ___Vector3One_3; }
	inline Vector3_t3722313464 * get_address_of_Vector3One_3() { return &___Vector3One_3; }
	inline void set_Vector3One_3(Vector3_t3722313464  value)
	{
		___Vector3One_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATH_T2875019722_H
#ifndef HEIGHTFILTEROPTIONS_T2664590344_H
#define HEIGHTFILTEROPTIONS_T2664590344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Filters.HeightFilter/HeightFilterOptions
struct  HeightFilterOptions_t2664590344 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Filters.HeightFilter/HeightFilterOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HeightFilterOptions_t2664590344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEIGHTFILTEROPTIONS_T2664590344_H
#ifndef TILESTATECHANGEDEVENTARGS_T145677042_H
#define TILESTATECHANGEDEVENTARGS_T145677042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.TileStateChangedEventArgs
struct  TileStateChangedEventArgs_t145677042  : public EventArgs_t3591816995
{
public:
	// Mapbox.Map.UnwrappedTileId Mapbox.Unity.Map.TileStateChangedEventArgs::TileId
	UnwrappedTileId_t2586853537  ___TileId_1;

public:
	inline static int32_t get_offset_of_TileId_1() { return static_cast<int32_t>(offsetof(TileStateChangedEventArgs_t145677042, ___TileId_1)); }
	inline UnwrappedTileId_t2586853537  get_TileId_1() const { return ___TileId_1; }
	inline UnwrappedTileId_t2586853537 * get_address_of_TileId_1() { return &___TileId_1; }
	inline void set_TileId_1(UnwrappedTileId_t2586853537  value)
	{
		___TileId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILESTATECHANGEDEVENTARGS_T145677042_H
#ifndef TYPEFILTERTYPE_T700042925_H
#define TYPEFILTERTYPE_T700042925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Filters.TypeFilter/TypeFilterType
struct  TypeFilterType_t700042925 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Filters.TypeFilter/TypeFilterType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeFilterType_t700042925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERTYPE_T700042925_H
#ifndef VOXELCOLORMAPPER_T2180346717_H
#define VOXELCOLORMAPPER_T2180346717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Voxels.VoxelColorMapper
struct  VoxelColorMapper_t2180346717  : public RuntimeObject
{
public:
	// UnityEngine.Color Mapbox.Examples.Voxels.VoxelColorMapper::Color
	Color_t2555686324  ___Color_0;
	// UnityEngine.GameObject Mapbox.Examples.Voxels.VoxelColorMapper::Voxel
	GameObject_t1113636619 * ___Voxel_1;

public:
	inline static int32_t get_offset_of_Color_0() { return static_cast<int32_t>(offsetof(VoxelColorMapper_t2180346717, ___Color_0)); }
	inline Color_t2555686324  get_Color_0() const { return ___Color_0; }
	inline Color_t2555686324 * get_address_of_Color_0() { return &___Color_0; }
	inline void set_Color_0(Color_t2555686324  value)
	{
		___Color_0 = value;
	}

	inline static int32_t get_offset_of_Voxel_1() { return static_cast<int32_t>(offsetof(VoxelColorMapper_t2180346717, ___Voxel_1)); }
	inline GameObject_t1113636619 * get_Voxel_1() const { return ___Voxel_1; }
	inline GameObject_t1113636619 ** get_address_of_Voxel_1() { return &___Voxel_1; }
	inline void set_Voxel_1(GameObject_t1113636619 * value)
	{
		___Voxel_1 = value;
		Il2CppCodeGenWriteBarrier((&___Voxel_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELCOLORMAPPER_T2180346717_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MESHDATA_T2053528051_H
#define MESHDATA_T2053528051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Data.MeshData
struct  MeshData_t2053528051  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> Mapbox.Unity.MeshGeneration.Data.MeshData::<Edges>k__BackingField
	List_1_t128053199 * ___U3CEdgesU3Ek__BackingField_0;
	// UnityEngine.Vector2 Mapbox.Unity.MeshGeneration.Data.MeshData::<MercatorCenter>k__BackingField
	Vector2_t2156229523  ___U3CMercatorCenterU3Ek__BackingField_1;
	// Mapbox.Utils.RectD Mapbox.Unity.MeshGeneration.Data.MeshData::<TileRect>k__BackingField
	RectD_t151583371  ___U3CTileRectU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Mapbox.Unity.MeshGeneration.Data.MeshData::<Vertices>k__BackingField
	List_1_t899420910 * ___U3CVerticesU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Mapbox.Unity.MeshGeneration.Data.MeshData::<Normals>k__BackingField
	List_1_t899420910 * ___U3CNormalsU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Int32>> Mapbox.Unity.MeshGeneration.Data.MeshData::<Triangles>k__BackingField
	List_1_t1600127941 * ___U3CTrianglesU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>> Mapbox.Unity.MeshGeneration.Data.MeshData::<UV>k__BackingField
	List_1_t805411711 * ___U3CUVU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeshData_t2053528051, ___U3CEdgesU3Ek__BackingField_0)); }
	inline List_1_t128053199 * get_U3CEdgesU3Ek__BackingField_0() const { return ___U3CEdgesU3Ek__BackingField_0; }
	inline List_1_t128053199 ** get_address_of_U3CEdgesU3Ek__BackingField_0() { return &___U3CEdgesU3Ek__BackingField_0; }
	inline void set_U3CEdgesU3Ek__BackingField_0(List_1_t128053199 * value)
	{
		___U3CEdgesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMercatorCenterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeshData_t2053528051, ___U3CMercatorCenterU3Ek__BackingField_1)); }
	inline Vector2_t2156229523  get_U3CMercatorCenterU3Ek__BackingField_1() const { return ___U3CMercatorCenterU3Ek__BackingField_1; }
	inline Vector2_t2156229523 * get_address_of_U3CMercatorCenterU3Ek__BackingField_1() { return &___U3CMercatorCenterU3Ek__BackingField_1; }
	inline void set_U3CMercatorCenterU3Ek__BackingField_1(Vector2_t2156229523  value)
	{
		___U3CMercatorCenterU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTileRectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MeshData_t2053528051, ___U3CTileRectU3Ek__BackingField_2)); }
	inline RectD_t151583371  get_U3CTileRectU3Ek__BackingField_2() const { return ___U3CTileRectU3Ek__BackingField_2; }
	inline RectD_t151583371 * get_address_of_U3CTileRectU3Ek__BackingField_2() { return &___U3CTileRectU3Ek__BackingField_2; }
	inline void set_U3CTileRectU3Ek__BackingField_2(RectD_t151583371  value)
	{
		___U3CTileRectU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CVerticesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MeshData_t2053528051, ___U3CVerticesU3Ek__BackingField_3)); }
	inline List_1_t899420910 * get_U3CVerticesU3Ek__BackingField_3() const { return ___U3CVerticesU3Ek__BackingField_3; }
	inline List_1_t899420910 ** get_address_of_U3CVerticesU3Ek__BackingField_3() { return &___U3CVerticesU3Ek__BackingField_3; }
	inline void set_U3CVerticesU3Ek__BackingField_3(List_1_t899420910 * value)
	{
		___U3CVerticesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVerticesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CNormalsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MeshData_t2053528051, ___U3CNormalsU3Ek__BackingField_4)); }
	inline List_1_t899420910 * get_U3CNormalsU3Ek__BackingField_4() const { return ___U3CNormalsU3Ek__BackingField_4; }
	inline List_1_t899420910 ** get_address_of_U3CNormalsU3Ek__BackingField_4() { return &___U3CNormalsU3Ek__BackingField_4; }
	inline void set_U3CNormalsU3Ek__BackingField_4(List_1_t899420910 * value)
	{
		___U3CNormalsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNormalsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTrianglesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MeshData_t2053528051, ___U3CTrianglesU3Ek__BackingField_5)); }
	inline List_1_t1600127941 * get_U3CTrianglesU3Ek__BackingField_5() const { return ___U3CTrianglesU3Ek__BackingField_5; }
	inline List_1_t1600127941 ** get_address_of_U3CTrianglesU3Ek__BackingField_5() { return &___U3CTrianglesU3Ek__BackingField_5; }
	inline void set_U3CTrianglesU3Ek__BackingField_5(List_1_t1600127941 * value)
	{
		___U3CTrianglesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrianglesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CUVU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MeshData_t2053528051, ___U3CUVU3Ek__BackingField_6)); }
	inline List_1_t805411711 * get_U3CUVU3Ek__BackingField_6() const { return ___U3CUVU3Ek__BackingField_6; }
	inline List_1_t805411711 ** get_address_of_U3CUVU3Ek__BackingField_6() { return &___U3CUVU3Ek__BackingField_6; }
	inline void set_U3CUVU3Ek__BackingField_6(List_1_t805411711 * value)
	{
		___U3CUVU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUVU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_T2053528051_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MODIFIERBASE_T1320963181_H
#define MODIFIERBASE_T1320963181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ModifierBase
struct  ModifierBase_t1320963181  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.ModifierBase::Active
	bool ___Active_2;

public:
	inline static int32_t get_offset_of_Active_2() { return static_cast<int32_t>(offsetof(ModifierBase_t1320963181, ___Active_2)); }
	inline bool get_Active_2() const { return ___Active_2; }
	inline bool* get_address_of_Active_2() { return &___Active_2; }
	inline void set_Active_2(bool value)
	{
		___Active_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERBASE_T1320963181_H
#ifndef FILTERBASE_T3477228168_H
#define FILTERBASE_T3477228168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Filters.FilterBase
struct  FilterBase_t3477228168  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERBASE_T3477228168_H
#ifndef LAYERVISUALIZERBASE_T2900487818_H
#define LAYERVISUALIZERBASE_T2900487818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase
struct  LayerVisualizerBase_t2900487818  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase::Active
	bool ___Active_2;

public:
	inline static int32_t get_offset_of_Active_2() { return static_cast<int32_t>(offsetof(LayerVisualizerBase_t2900487818, ___Active_2)); }
	inline bool get_Active_2() const { return ___Active_2; }
	inline bool* get_address_of_Active_2() { return &___Active_2; }
	inline void set_Active_2(bool value)
	{
		___Active_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERVISUALIZERBASE_T2900487818_H
#ifndef ABSTRACTMAPVISUALIZER_T551237957_H
#define ABSTRACTMAPVISUALIZER_T551237957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.AbstractMapVisualizer
struct  AbstractMapVisualizer_t551237957  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory> Mapbox.Unity.Map.AbstractMapVisualizer::Factories
	List_1_t3005512771 * ___Factories_2;
	// UnityEngine.Texture2D Mapbox.Unity.Map.AbstractMapVisualizer::_loadingTexture
	Texture2D_t3840446185 * ____loadingTexture_3;
	// Mapbox.Unity.Map.IMapReadable Mapbox.Unity.Map.AbstractMapVisualizer::_map
	RuntimeObject* ____map_4;
	// System.Collections.Generic.Dictionary`2<Mapbox.Map.UnwrappedTileId,Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.Map.AbstractMapVisualizer::_activeTiles
	Dictionary_2_t1603180064 * ____activeTiles_5;
	// System.Collections.Generic.Queue`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.Map.AbstractMapVisualizer::_inactiveTiles
	Queue_1_t2251345339 * ____inactiveTiles_6;
	// Mapbox.Unity.Map.ModuleState Mapbox.Unity.Map.AbstractMapVisualizer::_state
	int32_t ____state_7;
	// System.Action`1<Mapbox.Unity.Map.ModuleState> Mapbox.Unity.Map.AbstractMapVisualizer::OnMapVisualizerStateChanged
	Action_1_t2062571319 * ___OnMapVisualizerStateChanged_8;

public:
	inline static int32_t get_offset_of_Factories_2() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957, ___Factories_2)); }
	inline List_1_t3005512771 * get_Factories_2() const { return ___Factories_2; }
	inline List_1_t3005512771 ** get_address_of_Factories_2() { return &___Factories_2; }
	inline void set_Factories_2(List_1_t3005512771 * value)
	{
		___Factories_2 = value;
		Il2CppCodeGenWriteBarrier((&___Factories_2), value);
	}

	inline static int32_t get_offset_of__loadingTexture_3() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957, ____loadingTexture_3)); }
	inline Texture2D_t3840446185 * get__loadingTexture_3() const { return ____loadingTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of__loadingTexture_3() { return &____loadingTexture_3; }
	inline void set__loadingTexture_3(Texture2D_t3840446185 * value)
	{
		____loadingTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&____loadingTexture_3), value);
	}

	inline static int32_t get_offset_of__map_4() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957, ____map_4)); }
	inline RuntimeObject* get__map_4() const { return ____map_4; }
	inline RuntimeObject** get_address_of__map_4() { return &____map_4; }
	inline void set__map_4(RuntimeObject* value)
	{
		____map_4 = value;
		Il2CppCodeGenWriteBarrier((&____map_4), value);
	}

	inline static int32_t get_offset_of__activeTiles_5() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957, ____activeTiles_5)); }
	inline Dictionary_2_t1603180064 * get__activeTiles_5() const { return ____activeTiles_5; }
	inline Dictionary_2_t1603180064 ** get_address_of__activeTiles_5() { return &____activeTiles_5; }
	inline void set__activeTiles_5(Dictionary_2_t1603180064 * value)
	{
		____activeTiles_5 = value;
		Il2CppCodeGenWriteBarrier((&____activeTiles_5), value);
	}

	inline static int32_t get_offset_of__inactiveTiles_6() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957, ____inactiveTiles_6)); }
	inline Queue_1_t2251345339 * get__inactiveTiles_6() const { return ____inactiveTiles_6; }
	inline Queue_1_t2251345339 ** get_address_of__inactiveTiles_6() { return &____inactiveTiles_6; }
	inline void set__inactiveTiles_6(Queue_1_t2251345339 * value)
	{
		____inactiveTiles_6 = value;
		Il2CppCodeGenWriteBarrier((&____inactiveTiles_6), value);
	}

	inline static int32_t get_offset_of__state_7() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957, ____state_7)); }
	inline int32_t get__state_7() const { return ____state_7; }
	inline int32_t* get_address_of__state_7() { return &____state_7; }
	inline void set__state_7(int32_t value)
	{
		____state_7 = value;
	}

	inline static int32_t get_offset_of_OnMapVisualizerStateChanged_8() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957, ___OnMapVisualizerStateChanged_8)); }
	inline Action_1_t2062571319 * get_OnMapVisualizerStateChanged_8() const { return ___OnMapVisualizerStateChanged_8; }
	inline Action_1_t2062571319 ** get_address_of_OnMapVisualizerStateChanged_8() { return &___OnMapVisualizerStateChanged_8; }
	inline void set_OnMapVisualizerStateChanged_8(Action_1_t2062571319 * value)
	{
		___OnMapVisualizerStateChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnMapVisualizerStateChanged_8), value);
	}
};

struct AbstractMapVisualizer_t551237957_StaticFields
{
public:
	// System.Action`1<Mapbox.Unity.Map.ModuleState> Mapbox.Unity.Map.AbstractMapVisualizer::<>f__am$cache0
	Action_1_t2062571319 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(AbstractMapVisualizer_t551237957_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_1_t2062571319 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_1_t2062571319 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_1_t2062571319 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMAPVISUALIZER_T551237957_H
#ifndef ABSTRACTTILEFACTORY_T1533438029_H
#define ABSTRACTTILEFACTORY_T1533438029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory
struct  AbstractTileFactory_t1533438029  : public ScriptableObject_t2528358522
{
public:
	// Mapbox.Platform.IFileSource Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory::_fileSource
	RuntimeObject* ____fileSource_2;
	// Mapbox.Unity.Map.ModuleState Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_3;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory::_progress
	int32_t ____progress_4;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory> Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory::OnFactoryStateChanged
	Action_1_t1705905624 * ___OnFactoryStateChanged_5;

public:
	inline static int32_t get_offset_of__fileSource_2() { return static_cast<int32_t>(offsetof(AbstractTileFactory_t1533438029, ____fileSource_2)); }
	inline RuntimeObject* get__fileSource_2() const { return ____fileSource_2; }
	inline RuntimeObject** get_address_of__fileSource_2() { return &____fileSource_2; }
	inline void set__fileSource_2(RuntimeObject* value)
	{
		____fileSource_2 = value;
		Il2CppCodeGenWriteBarrier((&____fileSource_2), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AbstractTileFactory_t1533438029, ___U3CStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CStateU3Ek__BackingField_3() const { return ___U3CStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_3() { return &___U3CStateU3Ek__BackingField_3; }
	inline void set_U3CStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of__progress_4() { return static_cast<int32_t>(offsetof(AbstractTileFactory_t1533438029, ____progress_4)); }
	inline int32_t get__progress_4() const { return ____progress_4; }
	inline int32_t* get_address_of__progress_4() { return &____progress_4; }
	inline void set__progress_4(int32_t value)
	{
		____progress_4 = value;
	}

	inline static int32_t get_offset_of_OnFactoryStateChanged_5() { return static_cast<int32_t>(offsetof(AbstractTileFactory_t1533438029, ___OnFactoryStateChanged_5)); }
	inline Action_1_t1705905624 * get_OnFactoryStateChanged_5() const { return ___OnFactoryStateChanged_5; }
	inline Action_1_t1705905624 ** get_address_of_OnFactoryStateChanged_5() { return &___OnFactoryStateChanged_5; }
	inline void set_OnFactoryStateChanged_5(Action_1_t1705905624 * value)
	{
		___OnFactoryStateChanged_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnFactoryStateChanged_5), value);
	}
};

struct AbstractTileFactory_t1533438029_StaticFields
{
public:
	// System.Action`1<Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory> Mapbox.Unity.MeshGeneration.Factories.AbstractTileFactory::<>f__am$cache0
	Action_1_t1705905624 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(AbstractTileFactory_t1533438029_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t1705905624 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t1705905624 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t1705905624 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTILEFACTORY_T1533438029_H
#ifndef TERRAINFACTORY_T974472764_H
#define TERRAINFACTORY_T974472764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.TerrainFactory
struct  TerrainFactory_t974472764  : public AbstractTileFactory_t1533438029
{
public:
	// UnityEngine.Material Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_baseMaterial
	Material_t340375123 * ____baseMaterial_7;
	// Mapbox.Unity.MeshGeneration.Factories.MapIdType Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_mapIdType
	int32_t ____mapIdType_8;
	// System.String Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_customMapId
	String_t* ____customMapId_9;
	// System.String Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_mapId
	String_t* ____mapId_10;
	// System.Single Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_heightModifier
	float ____heightModifier_11;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_sampleCount
	int32_t ____sampleCount_12;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_addCollider
	bool ____addCollider_13;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_addToLayer
	bool ____addToLayer_14;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_layerId
	int32_t ____layerId_15;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_useRelativeHeight
	bool ____useRelativeHeight_16;
	// UnityEngine.Mesh Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_stitchTarget
	Mesh_t3648964284 * ____stitchTarget_17;
	// System.Collections.Generic.Dictionary`2<Mapbox.Map.UnwrappedTileId,UnityEngine.Mesh> Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_meshData
	Dictionary_2_t2847058503 * ____meshData_18;
	// Mapbox.Unity.MeshGeneration.Data.MeshData Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_currentTileMeshData
	MeshData_t2053528051 * ____currentTileMeshData_19;
	// Mapbox.Unity.MeshGeneration.Data.MeshData Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_stitchTargetMeshData
	MeshData_t2053528051 * ____stitchTargetMeshData_20;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_newVertexList
	List_1_t899420910 * ____newVertexList_21;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_newNormalList
	List_1_t899420910 * ____newNormalList_22;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_newUvList
	List_1_t3628304265 * ____newUvList_23;
	// System.Collections.Generic.List`1<System.Int32> Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_newTriangleList
	List_1_t128053199 * ____newTriangleList_24;
	// UnityEngine.Vector3 Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_newDir
	Vector3_t3722313464  ____newDir_25;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_vertA
	int32_t ____vertA_26;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_vertB
	int32_t ____vertB_27;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.TerrainFactory::_vertC
	int32_t ____vertC_28;

public:
	inline static int32_t get_offset_of__baseMaterial_7() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____baseMaterial_7)); }
	inline Material_t340375123 * get__baseMaterial_7() const { return ____baseMaterial_7; }
	inline Material_t340375123 ** get_address_of__baseMaterial_7() { return &____baseMaterial_7; }
	inline void set__baseMaterial_7(Material_t340375123 * value)
	{
		____baseMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&____baseMaterial_7), value);
	}

	inline static int32_t get_offset_of__mapIdType_8() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____mapIdType_8)); }
	inline int32_t get__mapIdType_8() const { return ____mapIdType_8; }
	inline int32_t* get_address_of__mapIdType_8() { return &____mapIdType_8; }
	inline void set__mapIdType_8(int32_t value)
	{
		____mapIdType_8 = value;
	}

	inline static int32_t get_offset_of__customMapId_9() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____customMapId_9)); }
	inline String_t* get__customMapId_9() const { return ____customMapId_9; }
	inline String_t** get_address_of__customMapId_9() { return &____customMapId_9; }
	inline void set__customMapId_9(String_t* value)
	{
		____customMapId_9 = value;
		Il2CppCodeGenWriteBarrier((&____customMapId_9), value);
	}

	inline static int32_t get_offset_of__mapId_10() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____mapId_10)); }
	inline String_t* get__mapId_10() const { return ____mapId_10; }
	inline String_t** get_address_of__mapId_10() { return &____mapId_10; }
	inline void set__mapId_10(String_t* value)
	{
		____mapId_10 = value;
		Il2CppCodeGenWriteBarrier((&____mapId_10), value);
	}

	inline static int32_t get_offset_of__heightModifier_11() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____heightModifier_11)); }
	inline float get__heightModifier_11() const { return ____heightModifier_11; }
	inline float* get_address_of__heightModifier_11() { return &____heightModifier_11; }
	inline void set__heightModifier_11(float value)
	{
		____heightModifier_11 = value;
	}

	inline static int32_t get_offset_of__sampleCount_12() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____sampleCount_12)); }
	inline int32_t get__sampleCount_12() const { return ____sampleCount_12; }
	inline int32_t* get_address_of__sampleCount_12() { return &____sampleCount_12; }
	inline void set__sampleCount_12(int32_t value)
	{
		____sampleCount_12 = value;
	}

	inline static int32_t get_offset_of__addCollider_13() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____addCollider_13)); }
	inline bool get__addCollider_13() const { return ____addCollider_13; }
	inline bool* get_address_of__addCollider_13() { return &____addCollider_13; }
	inline void set__addCollider_13(bool value)
	{
		____addCollider_13 = value;
	}

	inline static int32_t get_offset_of__addToLayer_14() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____addToLayer_14)); }
	inline bool get__addToLayer_14() const { return ____addToLayer_14; }
	inline bool* get_address_of__addToLayer_14() { return &____addToLayer_14; }
	inline void set__addToLayer_14(bool value)
	{
		____addToLayer_14 = value;
	}

	inline static int32_t get_offset_of__layerId_15() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____layerId_15)); }
	inline int32_t get__layerId_15() const { return ____layerId_15; }
	inline int32_t* get_address_of__layerId_15() { return &____layerId_15; }
	inline void set__layerId_15(int32_t value)
	{
		____layerId_15 = value;
	}

	inline static int32_t get_offset_of__useRelativeHeight_16() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____useRelativeHeight_16)); }
	inline bool get__useRelativeHeight_16() const { return ____useRelativeHeight_16; }
	inline bool* get_address_of__useRelativeHeight_16() { return &____useRelativeHeight_16; }
	inline void set__useRelativeHeight_16(bool value)
	{
		____useRelativeHeight_16 = value;
	}

	inline static int32_t get_offset_of__stitchTarget_17() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____stitchTarget_17)); }
	inline Mesh_t3648964284 * get__stitchTarget_17() const { return ____stitchTarget_17; }
	inline Mesh_t3648964284 ** get_address_of__stitchTarget_17() { return &____stitchTarget_17; }
	inline void set__stitchTarget_17(Mesh_t3648964284 * value)
	{
		____stitchTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&____stitchTarget_17), value);
	}

	inline static int32_t get_offset_of__meshData_18() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____meshData_18)); }
	inline Dictionary_2_t2847058503 * get__meshData_18() const { return ____meshData_18; }
	inline Dictionary_2_t2847058503 ** get_address_of__meshData_18() { return &____meshData_18; }
	inline void set__meshData_18(Dictionary_2_t2847058503 * value)
	{
		____meshData_18 = value;
		Il2CppCodeGenWriteBarrier((&____meshData_18), value);
	}

	inline static int32_t get_offset_of__currentTileMeshData_19() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____currentTileMeshData_19)); }
	inline MeshData_t2053528051 * get__currentTileMeshData_19() const { return ____currentTileMeshData_19; }
	inline MeshData_t2053528051 ** get_address_of__currentTileMeshData_19() { return &____currentTileMeshData_19; }
	inline void set__currentTileMeshData_19(MeshData_t2053528051 * value)
	{
		____currentTileMeshData_19 = value;
		Il2CppCodeGenWriteBarrier((&____currentTileMeshData_19), value);
	}

	inline static int32_t get_offset_of__stitchTargetMeshData_20() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____stitchTargetMeshData_20)); }
	inline MeshData_t2053528051 * get__stitchTargetMeshData_20() const { return ____stitchTargetMeshData_20; }
	inline MeshData_t2053528051 ** get_address_of__stitchTargetMeshData_20() { return &____stitchTargetMeshData_20; }
	inline void set__stitchTargetMeshData_20(MeshData_t2053528051 * value)
	{
		____stitchTargetMeshData_20 = value;
		Il2CppCodeGenWriteBarrier((&____stitchTargetMeshData_20), value);
	}

	inline static int32_t get_offset_of__newVertexList_21() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____newVertexList_21)); }
	inline List_1_t899420910 * get__newVertexList_21() const { return ____newVertexList_21; }
	inline List_1_t899420910 ** get_address_of__newVertexList_21() { return &____newVertexList_21; }
	inline void set__newVertexList_21(List_1_t899420910 * value)
	{
		____newVertexList_21 = value;
		Il2CppCodeGenWriteBarrier((&____newVertexList_21), value);
	}

	inline static int32_t get_offset_of__newNormalList_22() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____newNormalList_22)); }
	inline List_1_t899420910 * get__newNormalList_22() const { return ____newNormalList_22; }
	inline List_1_t899420910 ** get_address_of__newNormalList_22() { return &____newNormalList_22; }
	inline void set__newNormalList_22(List_1_t899420910 * value)
	{
		____newNormalList_22 = value;
		Il2CppCodeGenWriteBarrier((&____newNormalList_22), value);
	}

	inline static int32_t get_offset_of__newUvList_23() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____newUvList_23)); }
	inline List_1_t3628304265 * get__newUvList_23() const { return ____newUvList_23; }
	inline List_1_t3628304265 ** get_address_of__newUvList_23() { return &____newUvList_23; }
	inline void set__newUvList_23(List_1_t3628304265 * value)
	{
		____newUvList_23 = value;
		Il2CppCodeGenWriteBarrier((&____newUvList_23), value);
	}

	inline static int32_t get_offset_of__newTriangleList_24() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____newTriangleList_24)); }
	inline List_1_t128053199 * get__newTriangleList_24() const { return ____newTriangleList_24; }
	inline List_1_t128053199 ** get_address_of__newTriangleList_24() { return &____newTriangleList_24; }
	inline void set__newTriangleList_24(List_1_t128053199 * value)
	{
		____newTriangleList_24 = value;
		Il2CppCodeGenWriteBarrier((&____newTriangleList_24), value);
	}

	inline static int32_t get_offset_of__newDir_25() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____newDir_25)); }
	inline Vector3_t3722313464  get__newDir_25() const { return ____newDir_25; }
	inline Vector3_t3722313464 * get_address_of__newDir_25() { return &____newDir_25; }
	inline void set__newDir_25(Vector3_t3722313464  value)
	{
		____newDir_25 = value;
	}

	inline static int32_t get_offset_of__vertA_26() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____vertA_26)); }
	inline int32_t get__vertA_26() const { return ____vertA_26; }
	inline int32_t* get_address_of__vertA_26() { return &____vertA_26; }
	inline void set__vertA_26(int32_t value)
	{
		____vertA_26 = value;
	}

	inline static int32_t get_offset_of__vertB_27() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____vertB_27)); }
	inline int32_t get__vertB_27() const { return ____vertB_27; }
	inline int32_t* get_address_of__vertB_27() { return &____vertB_27; }
	inline void set__vertB_27(int32_t value)
	{
		____vertB_27 = value;
	}

	inline static int32_t get_offset_of__vertC_28() { return static_cast<int32_t>(offsetof(TerrainFactory_t974472764, ____vertC_28)); }
	inline int32_t get__vertC_28() const { return ____vertC_28; }
	inline int32_t* get_address_of__vertC_28() { return &____vertC_28; }
	inline void set__vertC_28(int32_t value)
	{
		____vertC_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINFACTORY_T974472764_H
#ifndef VECTORTILEFACTORY_T815718376_H
#define VECTORTILEFACTORY_T815718376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory
struct  VectorTileFactory_t815718376  : public AbstractTileFactory_t1533438029
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory::_mapId
	String_t* ____mapId_7;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase> Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory::Visualizers
	List_1_t77595264 * ___Visualizers_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase>> Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory::_layerBuilder
	Dictionary_2_t4157818859 * ____layerBuilder_9;
	// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,Mapbox.Map.VectorTile> Mapbox.Unity.MeshGeneration.Factories.VectorTileFactory::_cachedData
	Dictionary_2_t1598572408 * ____cachedData_10;

public:
	inline static int32_t get_offset_of__mapId_7() { return static_cast<int32_t>(offsetof(VectorTileFactory_t815718376, ____mapId_7)); }
	inline String_t* get__mapId_7() const { return ____mapId_7; }
	inline String_t** get_address_of__mapId_7() { return &____mapId_7; }
	inline void set__mapId_7(String_t* value)
	{
		____mapId_7 = value;
		Il2CppCodeGenWriteBarrier((&____mapId_7), value);
	}

	inline static int32_t get_offset_of_Visualizers_8() { return static_cast<int32_t>(offsetof(VectorTileFactory_t815718376, ___Visualizers_8)); }
	inline List_1_t77595264 * get_Visualizers_8() const { return ___Visualizers_8; }
	inline List_1_t77595264 ** get_address_of_Visualizers_8() { return &___Visualizers_8; }
	inline void set_Visualizers_8(List_1_t77595264 * value)
	{
		___Visualizers_8 = value;
		Il2CppCodeGenWriteBarrier((&___Visualizers_8), value);
	}

	inline static int32_t get_offset_of__layerBuilder_9() { return static_cast<int32_t>(offsetof(VectorTileFactory_t815718376, ____layerBuilder_9)); }
	inline Dictionary_2_t4157818859 * get__layerBuilder_9() const { return ____layerBuilder_9; }
	inline Dictionary_2_t4157818859 ** get_address_of__layerBuilder_9() { return &____layerBuilder_9; }
	inline void set__layerBuilder_9(Dictionary_2_t4157818859 * value)
	{
		____layerBuilder_9 = value;
		Il2CppCodeGenWriteBarrier((&____layerBuilder_9), value);
	}

	inline static int32_t get_offset_of__cachedData_10() { return static_cast<int32_t>(offsetof(VectorTileFactory_t815718376, ____cachedData_10)); }
	inline Dictionary_2_t1598572408 * get__cachedData_10() const { return ____cachedData_10; }
	inline Dictionary_2_t1598572408 ** get_address_of__cachedData_10() { return &____cachedData_10; }
	inline void set__cachedData_10(Dictionary_2_t1598572408 * value)
	{
		____cachedData_10 = value;
		Il2CppCodeGenWriteBarrier((&____cachedData_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILEFACTORY_T815718376_H
#ifndef MAPVISUALIZER_T2953540877_H
#define MAPVISUALIZER_T2953540877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.MapVisualizer
struct  MapVisualizer_t2953540877  : public AbstractMapVisualizer_t551237957
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPVISUALIZER_T2953540877_H
#ifndef FLATSPHERETERRAINFACTORY_T1099794750_H
#define FLATSPHERETERRAINFACTORY_T1099794750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory
struct  FlatSphereTerrainFactory_t1099794750  : public AbstractTileFactory_t1533438029
{
public:
	// UnityEngine.Material Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory::_baseMaterial
	Material_t340375123 * ____baseMaterial_7;
	// System.Single Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory::_radius
	float ____radius_8;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory::_sampleCount
	int32_t ____sampleCount_9;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory::_addCollider
	bool ____addCollider_10;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory::_addToLayer
	bool ____addToLayer_11;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory::_layerId
	int32_t ____layerId_12;

public:
	inline static int32_t get_offset_of__baseMaterial_7() { return static_cast<int32_t>(offsetof(FlatSphereTerrainFactory_t1099794750, ____baseMaterial_7)); }
	inline Material_t340375123 * get__baseMaterial_7() const { return ____baseMaterial_7; }
	inline Material_t340375123 ** get_address_of__baseMaterial_7() { return &____baseMaterial_7; }
	inline void set__baseMaterial_7(Material_t340375123 * value)
	{
		____baseMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&____baseMaterial_7), value);
	}

	inline static int32_t get_offset_of__radius_8() { return static_cast<int32_t>(offsetof(FlatSphereTerrainFactory_t1099794750, ____radius_8)); }
	inline float get__radius_8() const { return ____radius_8; }
	inline float* get_address_of__radius_8() { return &____radius_8; }
	inline void set__radius_8(float value)
	{
		____radius_8 = value;
	}

	inline static int32_t get_offset_of__sampleCount_9() { return static_cast<int32_t>(offsetof(FlatSphereTerrainFactory_t1099794750, ____sampleCount_9)); }
	inline int32_t get__sampleCount_9() const { return ____sampleCount_9; }
	inline int32_t* get_address_of__sampleCount_9() { return &____sampleCount_9; }
	inline void set__sampleCount_9(int32_t value)
	{
		____sampleCount_9 = value;
	}

	inline static int32_t get_offset_of__addCollider_10() { return static_cast<int32_t>(offsetof(FlatSphereTerrainFactory_t1099794750, ____addCollider_10)); }
	inline bool get__addCollider_10() const { return ____addCollider_10; }
	inline bool* get_address_of__addCollider_10() { return &____addCollider_10; }
	inline void set__addCollider_10(bool value)
	{
		____addCollider_10 = value;
	}

	inline static int32_t get_offset_of__addToLayer_11() { return static_cast<int32_t>(offsetof(FlatSphereTerrainFactory_t1099794750, ____addToLayer_11)); }
	inline bool get__addToLayer_11() const { return ____addToLayer_11; }
	inline bool* get_address_of__addToLayer_11() { return &____addToLayer_11; }
	inline void set__addToLayer_11(bool value)
	{
		____addToLayer_11 = value;
	}

	inline static int32_t get_offset_of__layerId_12() { return static_cast<int32_t>(offsetof(FlatSphereTerrainFactory_t1099794750, ____layerId_12)); }
	inline int32_t get__layerId_12() const { return ____layerId_12; }
	inline int32_t* get_address_of__layerId_12() { return &____layerId_12; }
	inline void set__layerId_12(int32_t value)
	{
		____layerId_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLATSPHERETERRAINFACTORY_T1099794750_H
#ifndef TYPEFILTER_T542231371_H
#define TYPEFILTER_T542231371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Filters.TypeFilter
struct  TypeFilter_t542231371  : public FilterBase_t3477228168
{
public:
	// System.String[] Mapbox.Unity.MeshGeneration.Filters.TypeFilter::_types
	StringU5BU5D_t1281789340* ____types_2;
	// Mapbox.Unity.MeshGeneration.Filters.TypeFilter/TypeFilterType Mapbox.Unity.MeshGeneration.Filters.TypeFilter::_behaviour
	int32_t ____behaviour_3;

public:
	inline static int32_t get_offset_of__types_2() { return static_cast<int32_t>(offsetof(TypeFilter_t542231371, ____types_2)); }
	inline StringU5BU5D_t1281789340* get__types_2() const { return ____types_2; }
	inline StringU5BU5D_t1281789340** get_address_of__types_2() { return &____types_2; }
	inline void set__types_2(StringU5BU5D_t1281789340* value)
	{
		____types_2 = value;
		Il2CppCodeGenWriteBarrier((&____types_2), value);
	}

	inline static int32_t get_offset_of__behaviour_3() { return static_cast<int32_t>(offsetof(TypeFilter_t542231371, ____behaviour_3)); }
	inline int32_t get__behaviour_3() const { return ____behaviour_3; }
	inline int32_t* get_address_of__behaviour_3() { return &____behaviour_3; }
	inline void set__behaviour_3(int32_t value)
	{
		____behaviour_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTER_T542231371_H
#ifndef POIVISUALIZER_T1810903380_H
#define POIVISUALIZER_T1810903380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Interfaces.PoiVisualizer
struct  PoiVisualizer_t1810903380  : public LayerVisualizerBase_t2900487818
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Interfaces.PoiVisualizer::_key
	String_t* ____key_3;
	// UnityEngine.GameObject Mapbox.Unity.MeshGeneration.Interfaces.PoiVisualizer::PoiPrefab
	GameObject_t1113636619 * ___PoiPrefab_4;
	// UnityEngine.GameObject Mapbox.Unity.MeshGeneration.Interfaces.PoiVisualizer::_container
	GameObject_t1113636619 * ____container_5;
	// System.Boolean Mapbox.Unity.MeshGeneration.Interfaces.PoiVisualizer::_scaleDownWithWorld
	bool ____scaleDownWithWorld_6;

public:
	inline static int32_t get_offset_of__key_3() { return static_cast<int32_t>(offsetof(PoiVisualizer_t1810903380, ____key_3)); }
	inline String_t* get__key_3() const { return ____key_3; }
	inline String_t** get_address_of__key_3() { return &____key_3; }
	inline void set__key_3(String_t* value)
	{
		____key_3 = value;
		Il2CppCodeGenWriteBarrier((&____key_3), value);
	}

	inline static int32_t get_offset_of_PoiPrefab_4() { return static_cast<int32_t>(offsetof(PoiVisualizer_t1810903380, ___PoiPrefab_4)); }
	inline GameObject_t1113636619 * get_PoiPrefab_4() const { return ___PoiPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_PoiPrefab_4() { return &___PoiPrefab_4; }
	inline void set_PoiPrefab_4(GameObject_t1113636619 * value)
	{
		___PoiPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___PoiPrefab_4), value);
	}

	inline static int32_t get_offset_of__container_5() { return static_cast<int32_t>(offsetof(PoiVisualizer_t1810903380, ____container_5)); }
	inline GameObject_t1113636619 * get__container_5() const { return ____container_5; }
	inline GameObject_t1113636619 ** get_address_of__container_5() { return &____container_5; }
	inline void set__container_5(GameObject_t1113636619 * value)
	{
		____container_5 = value;
		Il2CppCodeGenWriteBarrier((&____container_5), value);
	}

	inline static int32_t get_offset_of__scaleDownWithWorld_6() { return static_cast<int32_t>(offsetof(PoiVisualizer_t1810903380, ____scaleDownWithWorld_6)); }
	inline bool get__scaleDownWithWorld_6() const { return ____scaleDownWithWorld_6; }
	inline bool* get_address_of__scaleDownWithWorld_6() { return &____scaleDownWithWorld_6; }
	inline void set__scaleDownWithWorld_6(bool value)
	{
		____scaleDownWithWorld_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POIVISUALIZER_T1810903380_H
#ifndef VECTORLAYERVISUALIZER_T1004319578_H
#define VECTORLAYERVISUALIZER_T1004319578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer
struct  VectorLayerVisualizer_t1004319578  : public LayerVisualizerBase_t2900487818
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer::_classificationKey
	String_t* ____classificationKey_3;
	// System.String Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer::_key
	String_t* ____key_4;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Filters.FilterBase> Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer::Filters
	List_1_t654335614 * ___Filters_5;
	// Mapbox.Unity.MeshGeneration.Modifiers.ModifierStackBase Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer::_defaultStack
	ModifierStackBase_t4180154112 * ____defaultStack_6;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.TypeVisualizerTuple> Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer::Stacks
	List_1_t2105734821 * ___Stacks_7;
	// UnityEngine.GameObject Mapbox.Unity.MeshGeneration.Interfaces.VectorLayerVisualizer::_container
	GameObject_t1113636619 * ____container_8;

public:
	inline static int32_t get_offset_of__classificationKey_3() { return static_cast<int32_t>(offsetof(VectorLayerVisualizer_t1004319578, ____classificationKey_3)); }
	inline String_t* get__classificationKey_3() const { return ____classificationKey_3; }
	inline String_t** get_address_of__classificationKey_3() { return &____classificationKey_3; }
	inline void set__classificationKey_3(String_t* value)
	{
		____classificationKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____classificationKey_3), value);
	}

	inline static int32_t get_offset_of__key_4() { return static_cast<int32_t>(offsetof(VectorLayerVisualizer_t1004319578, ____key_4)); }
	inline String_t* get__key_4() const { return ____key_4; }
	inline String_t** get_address_of__key_4() { return &____key_4; }
	inline void set__key_4(String_t* value)
	{
		____key_4 = value;
		Il2CppCodeGenWriteBarrier((&____key_4), value);
	}

	inline static int32_t get_offset_of_Filters_5() { return static_cast<int32_t>(offsetof(VectorLayerVisualizer_t1004319578, ___Filters_5)); }
	inline List_1_t654335614 * get_Filters_5() const { return ___Filters_5; }
	inline List_1_t654335614 ** get_address_of_Filters_5() { return &___Filters_5; }
	inline void set_Filters_5(List_1_t654335614 * value)
	{
		___Filters_5 = value;
		Il2CppCodeGenWriteBarrier((&___Filters_5), value);
	}

	inline static int32_t get_offset_of__defaultStack_6() { return static_cast<int32_t>(offsetof(VectorLayerVisualizer_t1004319578, ____defaultStack_6)); }
	inline ModifierStackBase_t4180154112 * get__defaultStack_6() const { return ____defaultStack_6; }
	inline ModifierStackBase_t4180154112 ** get_address_of__defaultStack_6() { return &____defaultStack_6; }
	inline void set__defaultStack_6(ModifierStackBase_t4180154112 * value)
	{
		____defaultStack_6 = value;
		Il2CppCodeGenWriteBarrier((&____defaultStack_6), value);
	}

	inline static int32_t get_offset_of_Stacks_7() { return static_cast<int32_t>(offsetof(VectorLayerVisualizer_t1004319578, ___Stacks_7)); }
	inline List_1_t2105734821 * get_Stacks_7() const { return ___Stacks_7; }
	inline List_1_t2105734821 ** get_address_of_Stacks_7() { return &___Stacks_7; }
	inline void set_Stacks_7(List_1_t2105734821 * value)
	{
		___Stacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___Stacks_7), value);
	}

	inline static int32_t get_offset_of__container_8() { return static_cast<int32_t>(offsetof(VectorLayerVisualizer_t1004319578, ____container_8)); }
	inline GameObject_t1113636619 * get__container_8() const { return ____container_8; }
	inline GameObject_t1113636619 ** get_address_of__container_8() { return &____container_8; }
	inline void set__container_8(GameObject_t1113636619 * value)
	{
		____container_8 = value;
		Il2CppCodeGenWriteBarrier((&____container_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORLAYERVISUALIZER_T1004319578_H
#ifndef DYNAMICZOOMMAPVISUALIZER_T2283552828_H
#define DYNAMICZOOMMAPVISUALIZER_T2283552828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.DynamicZoomMapVisualizer
struct  DynamicZoomMapVisualizer_t2283552828  : public AbstractMapVisualizer_t551237957
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICZOOMMAPVISUALIZER_T2283552828_H
#ifndef GAMEOBJECTMODIFIER_T609190006_H
#define GAMEOBJECTMODIFIER_T609190006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier
struct  GameObjectModifier_t609190006  : public ModifierBase_t1320963181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTMODIFIER_T609190006_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef HEIGHTFILTER_T1216745462_H
#define HEIGHTFILTER_T1216745462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Filters.HeightFilter
struct  HeightFilter_t1216745462  : public FilterBase_t3477228168
{
public:
	// System.Single Mapbox.Unity.MeshGeneration.Filters.HeightFilter::_height
	float ____height_2;
	// Mapbox.Unity.MeshGeneration.Filters.HeightFilter/HeightFilterOptions Mapbox.Unity.MeshGeneration.Filters.HeightFilter::_type
	int32_t ____type_3;

public:
	inline static int32_t get_offset_of__height_2() { return static_cast<int32_t>(offsetof(HeightFilter_t1216745462, ____height_2)); }
	inline float get__height_2() const { return ____height_2; }
	inline float* get_address_of__height_2() { return &____height_2; }
	inline void set__height_2(float value)
	{
		____height_2 = value;
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(HeightFilter_t1216745462, ____type_3)); }
	inline int32_t get__type_3() const { return ____type_3; }
	inline int32_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int32_t value)
	{
		____type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEIGHTFILTER_T1216745462_H
#ifndef LOWPOLYTERRAINFACTORY_T2718546587_H
#define LOWPOLYTERRAINFACTORY_T2718546587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory
struct  LowPolyTerrainFactory_t2718546587  : public AbstractTileFactory_t1533438029
{
public:
	// UnityEngine.Material Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_baseMaterial
	Material_t340375123 * ____baseMaterial_7;
	// Mapbox.Unity.MeshGeneration.Factories.MapIdType Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_mapIdType
	int32_t ____mapIdType_8;
	// System.String Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_customMapId
	String_t* ____customMapId_9;
	// System.String Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_mapId
	String_t* ____mapId_10;
	// System.Single Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_heightModifier
	float ____heightModifier_11;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_sampleCount
	int32_t ____sampleCount_12;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_addCollider
	bool ____addCollider_13;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_addToLayer
	bool ____addToLayer_14;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_layerId
	int32_t ____layerId_15;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_useRelativeHeight
	bool ____useRelativeHeight_16;
	// UnityEngine.Mesh Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_stitchTarget
	Mesh_t3648964284 * ____stitchTarget_17;
	// System.Collections.Generic.Dictionary`2<Mapbox.Map.UnwrappedTileId,UnityEngine.Mesh> Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_meshData
	Dictionary_2_t2847058503 * ____meshData_18;
	// Mapbox.Unity.MeshGeneration.Data.MeshData Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_currentTileMeshData
	MeshData_t2053528051 * ____currentTileMeshData_19;
	// Mapbox.Unity.MeshGeneration.Data.MeshData Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_stitchTargetMeshData
	MeshData_t2053528051 * ____stitchTargetMeshData_20;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_newVertexList
	List_1_t899420910 * ____newVertexList_21;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_newNormalList
	List_1_t899420910 * ____newNormalList_22;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_newUvList
	List_1_t3628304265 * ____newUvList_23;
	// System.Collections.Generic.List`1<System.Int32> Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_newTriangleList
	List_1_t128053199 * ____newTriangleList_24;
	// UnityEngine.Vector3 Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_newDir
	Vector3_t3722313464  ____newDir_25;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_vertA
	int32_t ____vertA_26;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_vertB
	int32_t ____vertB_27;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.LowPolyTerrainFactory::_vertC
	int32_t ____vertC_28;

public:
	inline static int32_t get_offset_of__baseMaterial_7() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____baseMaterial_7)); }
	inline Material_t340375123 * get__baseMaterial_7() const { return ____baseMaterial_7; }
	inline Material_t340375123 ** get_address_of__baseMaterial_7() { return &____baseMaterial_7; }
	inline void set__baseMaterial_7(Material_t340375123 * value)
	{
		____baseMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&____baseMaterial_7), value);
	}

	inline static int32_t get_offset_of__mapIdType_8() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____mapIdType_8)); }
	inline int32_t get__mapIdType_8() const { return ____mapIdType_8; }
	inline int32_t* get_address_of__mapIdType_8() { return &____mapIdType_8; }
	inline void set__mapIdType_8(int32_t value)
	{
		____mapIdType_8 = value;
	}

	inline static int32_t get_offset_of__customMapId_9() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____customMapId_9)); }
	inline String_t* get__customMapId_9() const { return ____customMapId_9; }
	inline String_t** get_address_of__customMapId_9() { return &____customMapId_9; }
	inline void set__customMapId_9(String_t* value)
	{
		____customMapId_9 = value;
		Il2CppCodeGenWriteBarrier((&____customMapId_9), value);
	}

	inline static int32_t get_offset_of__mapId_10() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____mapId_10)); }
	inline String_t* get__mapId_10() const { return ____mapId_10; }
	inline String_t** get_address_of__mapId_10() { return &____mapId_10; }
	inline void set__mapId_10(String_t* value)
	{
		____mapId_10 = value;
		Il2CppCodeGenWriteBarrier((&____mapId_10), value);
	}

	inline static int32_t get_offset_of__heightModifier_11() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____heightModifier_11)); }
	inline float get__heightModifier_11() const { return ____heightModifier_11; }
	inline float* get_address_of__heightModifier_11() { return &____heightModifier_11; }
	inline void set__heightModifier_11(float value)
	{
		____heightModifier_11 = value;
	}

	inline static int32_t get_offset_of__sampleCount_12() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____sampleCount_12)); }
	inline int32_t get__sampleCount_12() const { return ____sampleCount_12; }
	inline int32_t* get_address_of__sampleCount_12() { return &____sampleCount_12; }
	inline void set__sampleCount_12(int32_t value)
	{
		____sampleCount_12 = value;
	}

	inline static int32_t get_offset_of__addCollider_13() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____addCollider_13)); }
	inline bool get__addCollider_13() const { return ____addCollider_13; }
	inline bool* get_address_of__addCollider_13() { return &____addCollider_13; }
	inline void set__addCollider_13(bool value)
	{
		____addCollider_13 = value;
	}

	inline static int32_t get_offset_of__addToLayer_14() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____addToLayer_14)); }
	inline bool get__addToLayer_14() const { return ____addToLayer_14; }
	inline bool* get_address_of__addToLayer_14() { return &____addToLayer_14; }
	inline void set__addToLayer_14(bool value)
	{
		____addToLayer_14 = value;
	}

	inline static int32_t get_offset_of__layerId_15() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____layerId_15)); }
	inline int32_t get__layerId_15() const { return ____layerId_15; }
	inline int32_t* get_address_of__layerId_15() { return &____layerId_15; }
	inline void set__layerId_15(int32_t value)
	{
		____layerId_15 = value;
	}

	inline static int32_t get_offset_of__useRelativeHeight_16() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____useRelativeHeight_16)); }
	inline bool get__useRelativeHeight_16() const { return ____useRelativeHeight_16; }
	inline bool* get_address_of__useRelativeHeight_16() { return &____useRelativeHeight_16; }
	inline void set__useRelativeHeight_16(bool value)
	{
		____useRelativeHeight_16 = value;
	}

	inline static int32_t get_offset_of__stitchTarget_17() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____stitchTarget_17)); }
	inline Mesh_t3648964284 * get__stitchTarget_17() const { return ____stitchTarget_17; }
	inline Mesh_t3648964284 ** get_address_of__stitchTarget_17() { return &____stitchTarget_17; }
	inline void set__stitchTarget_17(Mesh_t3648964284 * value)
	{
		____stitchTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&____stitchTarget_17), value);
	}

	inline static int32_t get_offset_of__meshData_18() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____meshData_18)); }
	inline Dictionary_2_t2847058503 * get__meshData_18() const { return ____meshData_18; }
	inline Dictionary_2_t2847058503 ** get_address_of__meshData_18() { return &____meshData_18; }
	inline void set__meshData_18(Dictionary_2_t2847058503 * value)
	{
		____meshData_18 = value;
		Il2CppCodeGenWriteBarrier((&____meshData_18), value);
	}

	inline static int32_t get_offset_of__currentTileMeshData_19() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____currentTileMeshData_19)); }
	inline MeshData_t2053528051 * get__currentTileMeshData_19() const { return ____currentTileMeshData_19; }
	inline MeshData_t2053528051 ** get_address_of__currentTileMeshData_19() { return &____currentTileMeshData_19; }
	inline void set__currentTileMeshData_19(MeshData_t2053528051 * value)
	{
		____currentTileMeshData_19 = value;
		Il2CppCodeGenWriteBarrier((&____currentTileMeshData_19), value);
	}

	inline static int32_t get_offset_of__stitchTargetMeshData_20() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____stitchTargetMeshData_20)); }
	inline MeshData_t2053528051 * get__stitchTargetMeshData_20() const { return ____stitchTargetMeshData_20; }
	inline MeshData_t2053528051 ** get_address_of__stitchTargetMeshData_20() { return &____stitchTargetMeshData_20; }
	inline void set__stitchTargetMeshData_20(MeshData_t2053528051 * value)
	{
		____stitchTargetMeshData_20 = value;
		Il2CppCodeGenWriteBarrier((&____stitchTargetMeshData_20), value);
	}

	inline static int32_t get_offset_of__newVertexList_21() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____newVertexList_21)); }
	inline List_1_t899420910 * get__newVertexList_21() const { return ____newVertexList_21; }
	inline List_1_t899420910 ** get_address_of__newVertexList_21() { return &____newVertexList_21; }
	inline void set__newVertexList_21(List_1_t899420910 * value)
	{
		____newVertexList_21 = value;
		Il2CppCodeGenWriteBarrier((&____newVertexList_21), value);
	}

	inline static int32_t get_offset_of__newNormalList_22() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____newNormalList_22)); }
	inline List_1_t899420910 * get__newNormalList_22() const { return ____newNormalList_22; }
	inline List_1_t899420910 ** get_address_of__newNormalList_22() { return &____newNormalList_22; }
	inline void set__newNormalList_22(List_1_t899420910 * value)
	{
		____newNormalList_22 = value;
		Il2CppCodeGenWriteBarrier((&____newNormalList_22), value);
	}

	inline static int32_t get_offset_of__newUvList_23() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____newUvList_23)); }
	inline List_1_t3628304265 * get__newUvList_23() const { return ____newUvList_23; }
	inline List_1_t3628304265 ** get_address_of__newUvList_23() { return &____newUvList_23; }
	inline void set__newUvList_23(List_1_t3628304265 * value)
	{
		____newUvList_23 = value;
		Il2CppCodeGenWriteBarrier((&____newUvList_23), value);
	}

	inline static int32_t get_offset_of__newTriangleList_24() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____newTriangleList_24)); }
	inline List_1_t128053199 * get__newTriangleList_24() const { return ____newTriangleList_24; }
	inline List_1_t128053199 ** get_address_of__newTriangleList_24() { return &____newTriangleList_24; }
	inline void set__newTriangleList_24(List_1_t128053199 * value)
	{
		____newTriangleList_24 = value;
		Il2CppCodeGenWriteBarrier((&____newTriangleList_24), value);
	}

	inline static int32_t get_offset_of__newDir_25() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____newDir_25)); }
	inline Vector3_t3722313464  get__newDir_25() const { return ____newDir_25; }
	inline Vector3_t3722313464 * get_address_of__newDir_25() { return &____newDir_25; }
	inline void set__newDir_25(Vector3_t3722313464  value)
	{
		____newDir_25 = value;
	}

	inline static int32_t get_offset_of__vertA_26() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____vertA_26)); }
	inline int32_t get__vertA_26() const { return ____vertA_26; }
	inline int32_t* get_address_of__vertA_26() { return &____vertA_26; }
	inline void set__vertA_26(int32_t value)
	{
		____vertA_26 = value;
	}

	inline static int32_t get_offset_of__vertB_27() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____vertB_27)); }
	inline int32_t get__vertB_27() const { return ____vertB_27; }
	inline int32_t* get_address_of__vertB_27() { return &____vertB_27; }
	inline void set__vertB_27(int32_t value)
	{
		____vertB_27 = value;
	}

	inline static int32_t get_offset_of__vertC_28() { return static_cast<int32_t>(offsetof(LowPolyTerrainFactory_t2718546587, ____vertC_28)); }
	inline int32_t get__vertC_28() const { return ____vertC_28; }
	inline int32_t* get_address_of__vertC_28() { return &____vertC_28; }
	inline void set__vertC_28(int32_t value)
	{
		____vertC_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOWPOLYTERRAINFACTORY_T2718546587_H
#ifndef MESHFACTORY_T3379932301_H
#define MESHFACTORY_T3379932301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.MeshFactory
struct  MeshFactory_t3379932301  : public AbstractTileFactory_t1533438029
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Factories.MeshFactory::_mapId
	String_t* ____mapId_7;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase> Mapbox.Unity.MeshGeneration.Factories.MeshFactory::Visualizers
	List_1_t77595264 * ___Visualizers_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase>> Mapbox.Unity.MeshGeneration.Factories.MeshFactory::_layerBuilder
	Dictionary_2_t4157818859 * ____layerBuilder_9;
	// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,Mapbox.Map.VectorTile> Mapbox.Unity.MeshGeneration.Factories.MeshFactory::_cachedData
	Dictionary_2_t1598572408 * ____cachedData_10;

public:
	inline static int32_t get_offset_of__mapId_7() { return static_cast<int32_t>(offsetof(MeshFactory_t3379932301, ____mapId_7)); }
	inline String_t* get__mapId_7() const { return ____mapId_7; }
	inline String_t** get_address_of__mapId_7() { return &____mapId_7; }
	inline void set__mapId_7(String_t* value)
	{
		____mapId_7 = value;
		Il2CppCodeGenWriteBarrier((&____mapId_7), value);
	}

	inline static int32_t get_offset_of_Visualizers_8() { return static_cast<int32_t>(offsetof(MeshFactory_t3379932301, ___Visualizers_8)); }
	inline List_1_t77595264 * get_Visualizers_8() const { return ___Visualizers_8; }
	inline List_1_t77595264 ** get_address_of_Visualizers_8() { return &___Visualizers_8; }
	inline void set_Visualizers_8(List_1_t77595264 * value)
	{
		___Visualizers_8 = value;
		Il2CppCodeGenWriteBarrier((&___Visualizers_8), value);
	}

	inline static int32_t get_offset_of__layerBuilder_9() { return static_cast<int32_t>(offsetof(MeshFactory_t3379932301, ____layerBuilder_9)); }
	inline Dictionary_2_t4157818859 * get__layerBuilder_9() const { return ____layerBuilder_9; }
	inline Dictionary_2_t4157818859 ** get_address_of__layerBuilder_9() { return &____layerBuilder_9; }
	inline void set__layerBuilder_9(Dictionary_2_t4157818859 * value)
	{
		____layerBuilder_9 = value;
		Il2CppCodeGenWriteBarrier((&____layerBuilder_9), value);
	}

	inline static int32_t get_offset_of__cachedData_10() { return static_cast<int32_t>(offsetof(MeshFactory_t3379932301, ____cachedData_10)); }
	inline Dictionary_2_t1598572408 * get__cachedData_10() const { return ____cachedData_10; }
	inline Dictionary_2_t1598572408 ** get_address_of__cachedData_10() { return &____cachedData_10; }
	inline void set__cachedData_10(Dictionary_2_t1598572408 * value)
	{
		____cachedData_10 = value;
		Il2CppCodeGenWriteBarrier((&____cachedData_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFACTORY_T3379932301_H
#ifndef MAPIMAGEFACTORY_T200237364_H
#define MAPIMAGEFACTORY_T200237364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.MapImageFactory
struct  MapImageFactory_t200237364  : public AbstractTileFactory_t1533438029
{
public:
	// Mapbox.Unity.MeshGeneration.Factories.MapImageType Mapbox.Unity.MeshGeneration.Factories.MapImageFactory::_mapIdType
	int32_t ____mapIdType_7;
	// Mapbox.Unity.MeshGeneration.Factories.Style Mapbox.Unity.MeshGeneration.Factories.MapImageFactory::_customStyle
	Style_t2684836192 * ____customStyle_8;
	// System.String Mapbox.Unity.MeshGeneration.Factories.MapImageFactory::_mapId
	String_t* ____mapId_9;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.MapImageFactory::_useCompression
	bool ____useCompression_10;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.MapImageFactory::_useMipMap
	bool ____useMipMap_11;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.MapImageFactory::_useRetina
	bool ____useRetina_12;

public:
	inline static int32_t get_offset_of__mapIdType_7() { return static_cast<int32_t>(offsetof(MapImageFactory_t200237364, ____mapIdType_7)); }
	inline int32_t get__mapIdType_7() const { return ____mapIdType_7; }
	inline int32_t* get_address_of__mapIdType_7() { return &____mapIdType_7; }
	inline void set__mapIdType_7(int32_t value)
	{
		____mapIdType_7 = value;
	}

	inline static int32_t get_offset_of__customStyle_8() { return static_cast<int32_t>(offsetof(MapImageFactory_t200237364, ____customStyle_8)); }
	inline Style_t2684836192 * get__customStyle_8() const { return ____customStyle_8; }
	inline Style_t2684836192 ** get_address_of__customStyle_8() { return &____customStyle_8; }
	inline void set__customStyle_8(Style_t2684836192 * value)
	{
		____customStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&____customStyle_8), value);
	}

	inline static int32_t get_offset_of__mapId_9() { return static_cast<int32_t>(offsetof(MapImageFactory_t200237364, ____mapId_9)); }
	inline String_t* get__mapId_9() const { return ____mapId_9; }
	inline String_t** get_address_of__mapId_9() { return &____mapId_9; }
	inline void set__mapId_9(String_t* value)
	{
		____mapId_9 = value;
		Il2CppCodeGenWriteBarrier((&____mapId_9), value);
	}

	inline static int32_t get_offset_of__useCompression_10() { return static_cast<int32_t>(offsetof(MapImageFactory_t200237364, ____useCompression_10)); }
	inline bool get__useCompression_10() const { return ____useCompression_10; }
	inline bool* get_address_of__useCompression_10() { return &____useCompression_10; }
	inline void set__useCompression_10(bool value)
	{
		____useCompression_10 = value;
	}

	inline static int32_t get_offset_of__useMipMap_11() { return static_cast<int32_t>(offsetof(MapImageFactory_t200237364, ____useMipMap_11)); }
	inline bool get__useMipMap_11() const { return ____useMipMap_11; }
	inline bool* get_address_of__useMipMap_11() { return &____useMipMap_11; }
	inline void set__useMipMap_11(bool value)
	{
		____useMipMap_11 = value;
	}

	inline static int32_t get_offset_of__useRetina_12() { return static_cast<int32_t>(offsetof(MapImageFactory_t200237364, ____useRetina_12)); }
	inline bool get__useRetina_12() const { return ____useRetina_12; }
	inline bool* get_address_of__useRetina_12() { return &____useRetina_12; }
	inline void set__useRetina_12(bool value)
	{
		____useRetina_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPIMAGEFACTORY_T200237364_H
#ifndef STYLEOPTIMIZEDVECTORTILEFACTORY_T1939249911_H
#define STYLEOPTIMIZEDVECTORTILEFACTORY_T1939249911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory
struct  StyleOptimizedVectorTileFactory_t1939249911  : public AbstractTileFactory_t1533438029
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory::_mapId
	String_t* ____mapId_7;
	// Mapbox.Unity.MeshGeneration.Factories.Style Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory::_optimizedStyle
	Style_t2684836192 * ____optimizedStyle_8;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase> Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory::Visualizers
	List_1_t77595264 * ___Visualizers_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Interfaces.LayerVisualizerBase>> Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory::_layerBuilder
	Dictionary_2_t4157818859 * ____layerBuilder_10;
	// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,Mapbox.Map.StyleOptimizedVectorTile> Mapbox.Unity.MeshGeneration.Factories.StyleOptimizedVectorTileFactory::_cachedData
	Dictionary_2_t3428257364 * ____cachedData_11;

public:
	inline static int32_t get_offset_of__mapId_7() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTileFactory_t1939249911, ____mapId_7)); }
	inline String_t* get__mapId_7() const { return ____mapId_7; }
	inline String_t** get_address_of__mapId_7() { return &____mapId_7; }
	inline void set__mapId_7(String_t* value)
	{
		____mapId_7 = value;
		Il2CppCodeGenWriteBarrier((&____mapId_7), value);
	}

	inline static int32_t get_offset_of__optimizedStyle_8() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTileFactory_t1939249911, ____optimizedStyle_8)); }
	inline Style_t2684836192 * get__optimizedStyle_8() const { return ____optimizedStyle_8; }
	inline Style_t2684836192 ** get_address_of__optimizedStyle_8() { return &____optimizedStyle_8; }
	inline void set__optimizedStyle_8(Style_t2684836192 * value)
	{
		____optimizedStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&____optimizedStyle_8), value);
	}

	inline static int32_t get_offset_of_Visualizers_9() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTileFactory_t1939249911, ___Visualizers_9)); }
	inline List_1_t77595264 * get_Visualizers_9() const { return ___Visualizers_9; }
	inline List_1_t77595264 ** get_address_of_Visualizers_9() { return &___Visualizers_9; }
	inline void set_Visualizers_9(List_1_t77595264 * value)
	{
		___Visualizers_9 = value;
		Il2CppCodeGenWriteBarrier((&___Visualizers_9), value);
	}

	inline static int32_t get_offset_of__layerBuilder_10() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTileFactory_t1939249911, ____layerBuilder_10)); }
	inline Dictionary_2_t4157818859 * get__layerBuilder_10() const { return ____layerBuilder_10; }
	inline Dictionary_2_t4157818859 ** get_address_of__layerBuilder_10() { return &____layerBuilder_10; }
	inline void set__layerBuilder_10(Dictionary_2_t4157818859 * value)
	{
		____layerBuilder_10 = value;
		Il2CppCodeGenWriteBarrier((&____layerBuilder_10), value);
	}

	inline static int32_t get_offset_of__cachedData_11() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTileFactory_t1939249911, ____cachedData_11)); }
	inline Dictionary_2_t3428257364 * get__cachedData_11() const { return ____cachedData_11; }
	inline Dictionary_2_t3428257364 ** get_address_of__cachedData_11() { return &____cachedData_11; }
	inline void set__cachedData_11(Dictionary_2_t3428257364 * value)
	{
		____cachedData_11 = value;
		Il2CppCodeGenWriteBarrier((&____cachedData_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLEOPTIMIZEDVECTORTILEFACTORY_T1939249911_H
#ifndef FLATTERRAINFACTORY_T2329281599_H
#define FLATTERRAINFACTORY_T2329281599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.FlatTerrainFactory
struct  FlatTerrainFactory_t2329281599  : public AbstractTileFactory_t1533438029
{
public:
	// UnityEngine.Material Mapbox.Unity.MeshGeneration.Factories.FlatTerrainFactory::_baseMaterial
	Material_t340375123 * ____baseMaterial_7;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.FlatTerrainFactory::_addCollider
	bool ____addCollider_8;
	// System.Boolean Mapbox.Unity.MeshGeneration.Factories.FlatTerrainFactory::_addToLayer
	bool ____addToLayer_9;
	// System.Int32 Mapbox.Unity.MeshGeneration.Factories.FlatTerrainFactory::_layerId
	int32_t ____layerId_10;
	// UnityEngine.Mesh Mapbox.Unity.MeshGeneration.Factories.FlatTerrainFactory::_cachedQuad
	Mesh_t3648964284 * ____cachedQuad_11;

public:
	inline static int32_t get_offset_of__baseMaterial_7() { return static_cast<int32_t>(offsetof(FlatTerrainFactory_t2329281599, ____baseMaterial_7)); }
	inline Material_t340375123 * get__baseMaterial_7() const { return ____baseMaterial_7; }
	inline Material_t340375123 ** get_address_of__baseMaterial_7() { return &____baseMaterial_7; }
	inline void set__baseMaterial_7(Material_t340375123 * value)
	{
		____baseMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&____baseMaterial_7), value);
	}

	inline static int32_t get_offset_of__addCollider_8() { return static_cast<int32_t>(offsetof(FlatTerrainFactory_t2329281599, ____addCollider_8)); }
	inline bool get__addCollider_8() const { return ____addCollider_8; }
	inline bool* get_address_of__addCollider_8() { return &____addCollider_8; }
	inline void set__addCollider_8(bool value)
	{
		____addCollider_8 = value;
	}

	inline static int32_t get_offset_of__addToLayer_9() { return static_cast<int32_t>(offsetof(FlatTerrainFactory_t2329281599, ____addToLayer_9)); }
	inline bool get__addToLayer_9() const { return ____addToLayer_9; }
	inline bool* get_address_of__addToLayer_9() { return &____addToLayer_9; }
	inline void set__addToLayer_9(bool value)
	{
		____addToLayer_9 = value;
	}

	inline static int32_t get_offset_of__layerId_10() { return static_cast<int32_t>(offsetof(FlatTerrainFactory_t2329281599, ____layerId_10)); }
	inline int32_t get__layerId_10() const { return ____layerId_10; }
	inline int32_t* get_address_of__layerId_10() { return &____layerId_10; }
	inline void set__layerId_10(int32_t value)
	{
		____layerId_10 = value;
	}

	inline static int32_t get_offset_of__cachedQuad_11() { return static_cast<int32_t>(offsetof(FlatTerrainFactory_t2329281599, ____cachedQuad_11)); }
	inline Mesh_t3648964284 * get__cachedQuad_11() const { return ____cachedQuad_11; }
	inline Mesh_t3648964284 ** get_address_of__cachedQuad_11() { return &____cachedQuad_11; }
	inline void set__cachedQuad_11(Mesh_t3648964284 * value)
	{
		____cachedQuad_11 = value;
		Il2CppCodeGenWriteBarrier((&____cachedQuad_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLATTERRAINFACTORY_T2329281599_H
#ifndef TRANSFORMLOCATIONPROVIDER_T3083760227_H
#define TRANSFORMLOCATIONPROVIDER_T3083760227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Location.TransformLocationProvider
struct  TransformLocationProvider_t3083760227  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Map.AbstractMap Mapbox.Unity.Location.TransformLocationProvider::_map
	AbstractMap_t3082917158 * ____map_2;
	// UnityEngine.Transform Mapbox.Unity.Location.TransformLocationProvider::_targetTransform
	Transform_t3600365921 * ____targetTransform_3;
	// System.EventHandler`1<Mapbox.Unity.Location.HeadingUpdatedEventArgs> Mapbox.Unity.Location.TransformLocationProvider::OnHeadingUpdated
	EventHandler_1_t2962207339 * ___OnHeadingUpdated_4;
	// System.EventHandler`1<Mapbox.Unity.Location.LocationUpdatedEventArgs> Mapbox.Unity.Location.TransformLocationProvider::OnLocationUpdated
	EventHandler_1_t1729069707 * ___OnLocationUpdated_5;

public:
	inline static int32_t get_offset_of__map_2() { return static_cast<int32_t>(offsetof(TransformLocationProvider_t3083760227, ____map_2)); }
	inline AbstractMap_t3082917158 * get__map_2() const { return ____map_2; }
	inline AbstractMap_t3082917158 ** get_address_of__map_2() { return &____map_2; }
	inline void set__map_2(AbstractMap_t3082917158 * value)
	{
		____map_2 = value;
		Il2CppCodeGenWriteBarrier((&____map_2), value);
	}

	inline static int32_t get_offset_of__targetTransform_3() { return static_cast<int32_t>(offsetof(TransformLocationProvider_t3083760227, ____targetTransform_3)); }
	inline Transform_t3600365921 * get__targetTransform_3() const { return ____targetTransform_3; }
	inline Transform_t3600365921 ** get_address_of__targetTransform_3() { return &____targetTransform_3; }
	inline void set__targetTransform_3(Transform_t3600365921 * value)
	{
		____targetTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&____targetTransform_3), value);
	}

	inline static int32_t get_offset_of_OnHeadingUpdated_4() { return static_cast<int32_t>(offsetof(TransformLocationProvider_t3083760227, ___OnHeadingUpdated_4)); }
	inline EventHandler_1_t2962207339 * get_OnHeadingUpdated_4() const { return ___OnHeadingUpdated_4; }
	inline EventHandler_1_t2962207339 ** get_address_of_OnHeadingUpdated_4() { return &___OnHeadingUpdated_4; }
	inline void set_OnHeadingUpdated_4(EventHandler_1_t2962207339 * value)
	{
		___OnHeadingUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeadingUpdated_4), value);
	}

	inline static int32_t get_offset_of_OnLocationUpdated_5() { return static_cast<int32_t>(offsetof(TransformLocationProvider_t3083760227, ___OnLocationUpdated_5)); }
	inline EventHandler_1_t1729069707 * get_OnLocationUpdated_5() const { return ___OnLocationUpdated_5; }
	inline EventHandler_1_t1729069707 ** get_address_of_OnLocationUpdated_5() { return &___OnLocationUpdated_5; }
	inline void set_OnLocationUpdated_5(EventHandler_1_t1729069707 * value)
	{
		___OnLocationUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnLocationUpdated_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMLOCATIONPROVIDER_T3083760227_H
#ifndef DYNAMICZOOMCAMERAMOVEMENT_T1805284077_H
#define DYNAMICZOOMCAMERAMOVEMENT_T1805284077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.DynamicZoomCameraMovement
struct  DynamicZoomCameraMovement_t1805284077  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mapbox.Examples.DynamicZoomCameraMovement::_zoomSpeed
	float ____zoomSpeed_2;
	// UnityEngine.Camera Mapbox.Examples.DynamicZoomCameraMovement::_referenceCamera
	Camera_t4157153871 * ____referenceCamera_3;
	// Mapbox.Unity.Map.DynamicZoomTileProvider Mapbox.Examples.DynamicZoomCameraMovement::_dynamicZoomTileProvider
	DynamicZoomTileProvider_t2524886064 * ____dynamicZoomTileProvider_4;
	// Mapbox.Unity.Map.DynamicZoomMap Mapbox.Examples.DynamicZoomCameraMovement::_dynamicZoomMap
	DynamicZoomMap_t2517981420 * ____dynamicZoomMap_5;
	// UnityEngine.Vector3 Mapbox.Examples.DynamicZoomCameraMovement::_origin
	Vector3_t3722313464  ____origin_6;

public:
	inline static int32_t get_offset_of__zoomSpeed_2() { return static_cast<int32_t>(offsetof(DynamicZoomCameraMovement_t1805284077, ____zoomSpeed_2)); }
	inline float get__zoomSpeed_2() const { return ____zoomSpeed_2; }
	inline float* get_address_of__zoomSpeed_2() { return &____zoomSpeed_2; }
	inline void set__zoomSpeed_2(float value)
	{
		____zoomSpeed_2 = value;
	}

	inline static int32_t get_offset_of__referenceCamera_3() { return static_cast<int32_t>(offsetof(DynamicZoomCameraMovement_t1805284077, ____referenceCamera_3)); }
	inline Camera_t4157153871 * get__referenceCamera_3() const { return ____referenceCamera_3; }
	inline Camera_t4157153871 ** get_address_of__referenceCamera_3() { return &____referenceCamera_3; }
	inline void set__referenceCamera_3(Camera_t4157153871 * value)
	{
		____referenceCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&____referenceCamera_3), value);
	}

	inline static int32_t get_offset_of__dynamicZoomTileProvider_4() { return static_cast<int32_t>(offsetof(DynamicZoomCameraMovement_t1805284077, ____dynamicZoomTileProvider_4)); }
	inline DynamicZoomTileProvider_t2524886064 * get__dynamicZoomTileProvider_4() const { return ____dynamicZoomTileProvider_4; }
	inline DynamicZoomTileProvider_t2524886064 ** get_address_of__dynamicZoomTileProvider_4() { return &____dynamicZoomTileProvider_4; }
	inline void set__dynamicZoomTileProvider_4(DynamicZoomTileProvider_t2524886064 * value)
	{
		____dynamicZoomTileProvider_4 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicZoomTileProvider_4), value);
	}

	inline static int32_t get_offset_of__dynamicZoomMap_5() { return static_cast<int32_t>(offsetof(DynamicZoomCameraMovement_t1805284077, ____dynamicZoomMap_5)); }
	inline DynamicZoomMap_t2517981420 * get__dynamicZoomMap_5() const { return ____dynamicZoomMap_5; }
	inline DynamicZoomMap_t2517981420 ** get_address_of__dynamicZoomMap_5() { return &____dynamicZoomMap_5; }
	inline void set__dynamicZoomMap_5(DynamicZoomMap_t2517981420 * value)
	{
		____dynamicZoomMap_5 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicZoomMap_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(DynamicZoomCameraMovement_t1805284077, ____origin_6)); }
	inline Vector3_t3722313464  get__origin_6() const { return ____origin_6; }
	inline Vector3_t3722313464 * get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(Vector3_t3722313464  value)
	{
		____origin_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICZOOMCAMERAMOVEMENT_T1805284077_H
#ifndef ABSTRACTMAP_T3082917158_H
#define ABSTRACTMAP_T3082917158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.AbstractMap
struct  AbstractMap_t3082917158  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Mapbox.Unity.Map.AbstractMap::_initializeOnStart
	bool ____initializeOnStart_2;
	// System.String Mapbox.Unity.Map.AbstractMap::_latitudeLongitudeString
	String_t* ____latitudeLongitudeString_3;
	// System.Int32 Mapbox.Unity.Map.AbstractMap::_zoom
	int32_t ____zoom_4;
	// UnityEngine.Transform Mapbox.Unity.Map.AbstractMap::_root
	Transform_t3600365921 * ____root_5;
	// Mapbox.Unity.Map.AbstractTileProvider Mapbox.Unity.Map.AbstractMap::_tileProvider
	AbstractTileProvider_t1076087356 * ____tileProvider_6;
	// Mapbox.Unity.Map.AbstractMapVisualizer Mapbox.Unity.Map.AbstractMap::_mapVisualizer
	AbstractMapVisualizer_t551237957 * ____mapVisualizer_7;
	// System.Single Mapbox.Unity.Map.AbstractMap::_unityTileSize
	float ____unityTileSize_8;
	// System.Boolean Mapbox.Unity.Map.AbstractMap::_snapMapHeightToZero
	bool ____snapMapHeightToZero_9;
	// System.Boolean Mapbox.Unity.Map.AbstractMap::_worldHeightFixed
	bool ____worldHeightFixed_10;
	// Mapbox.Unity.MapboxAccess Mapbox.Unity.Map.AbstractMap::_fileSouce
	MapboxAccess_t3460807032 * ____fileSouce_11;
	// Mapbox.Utils.Vector2d Mapbox.Unity.Map.AbstractMap::_centerLatitudeLongitude
	Vector2d_t1865246568  ____centerLatitudeLongitude_12;
	// Mapbox.Utils.Vector2d Mapbox.Unity.Map.AbstractMap::_centerMercator
	Vector2d_t1865246568  ____centerMercator_13;
	// System.Single Mapbox.Unity.Map.AbstractMap::_worldRelativeScale
	float ____worldRelativeScale_14;
	// System.Action Mapbox.Unity.Map.AbstractMap::OnInitialized
	Action_t1264377477 * ___OnInitialized_15;

public:
	inline static int32_t get_offset_of__initializeOnStart_2() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____initializeOnStart_2)); }
	inline bool get__initializeOnStart_2() const { return ____initializeOnStart_2; }
	inline bool* get_address_of__initializeOnStart_2() { return &____initializeOnStart_2; }
	inline void set__initializeOnStart_2(bool value)
	{
		____initializeOnStart_2 = value;
	}

	inline static int32_t get_offset_of__latitudeLongitudeString_3() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____latitudeLongitudeString_3)); }
	inline String_t* get__latitudeLongitudeString_3() const { return ____latitudeLongitudeString_3; }
	inline String_t** get_address_of__latitudeLongitudeString_3() { return &____latitudeLongitudeString_3; }
	inline void set__latitudeLongitudeString_3(String_t* value)
	{
		____latitudeLongitudeString_3 = value;
		Il2CppCodeGenWriteBarrier((&____latitudeLongitudeString_3), value);
	}

	inline static int32_t get_offset_of__zoom_4() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____zoom_4)); }
	inline int32_t get__zoom_4() const { return ____zoom_4; }
	inline int32_t* get_address_of__zoom_4() { return &____zoom_4; }
	inline void set__zoom_4(int32_t value)
	{
		____zoom_4 = value;
	}

	inline static int32_t get_offset_of__root_5() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____root_5)); }
	inline Transform_t3600365921 * get__root_5() const { return ____root_5; }
	inline Transform_t3600365921 ** get_address_of__root_5() { return &____root_5; }
	inline void set__root_5(Transform_t3600365921 * value)
	{
		____root_5 = value;
		Il2CppCodeGenWriteBarrier((&____root_5), value);
	}

	inline static int32_t get_offset_of__tileProvider_6() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____tileProvider_6)); }
	inline AbstractTileProvider_t1076087356 * get__tileProvider_6() const { return ____tileProvider_6; }
	inline AbstractTileProvider_t1076087356 ** get_address_of__tileProvider_6() { return &____tileProvider_6; }
	inline void set__tileProvider_6(AbstractTileProvider_t1076087356 * value)
	{
		____tileProvider_6 = value;
		Il2CppCodeGenWriteBarrier((&____tileProvider_6), value);
	}

	inline static int32_t get_offset_of__mapVisualizer_7() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____mapVisualizer_7)); }
	inline AbstractMapVisualizer_t551237957 * get__mapVisualizer_7() const { return ____mapVisualizer_7; }
	inline AbstractMapVisualizer_t551237957 ** get_address_of__mapVisualizer_7() { return &____mapVisualizer_7; }
	inline void set__mapVisualizer_7(AbstractMapVisualizer_t551237957 * value)
	{
		____mapVisualizer_7 = value;
		Il2CppCodeGenWriteBarrier((&____mapVisualizer_7), value);
	}

	inline static int32_t get_offset_of__unityTileSize_8() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____unityTileSize_8)); }
	inline float get__unityTileSize_8() const { return ____unityTileSize_8; }
	inline float* get_address_of__unityTileSize_8() { return &____unityTileSize_8; }
	inline void set__unityTileSize_8(float value)
	{
		____unityTileSize_8 = value;
	}

	inline static int32_t get_offset_of__snapMapHeightToZero_9() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____snapMapHeightToZero_9)); }
	inline bool get__snapMapHeightToZero_9() const { return ____snapMapHeightToZero_9; }
	inline bool* get_address_of__snapMapHeightToZero_9() { return &____snapMapHeightToZero_9; }
	inline void set__snapMapHeightToZero_9(bool value)
	{
		____snapMapHeightToZero_9 = value;
	}

	inline static int32_t get_offset_of__worldHeightFixed_10() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____worldHeightFixed_10)); }
	inline bool get__worldHeightFixed_10() const { return ____worldHeightFixed_10; }
	inline bool* get_address_of__worldHeightFixed_10() { return &____worldHeightFixed_10; }
	inline void set__worldHeightFixed_10(bool value)
	{
		____worldHeightFixed_10 = value;
	}

	inline static int32_t get_offset_of__fileSouce_11() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____fileSouce_11)); }
	inline MapboxAccess_t3460807032 * get__fileSouce_11() const { return ____fileSouce_11; }
	inline MapboxAccess_t3460807032 ** get_address_of__fileSouce_11() { return &____fileSouce_11; }
	inline void set__fileSouce_11(MapboxAccess_t3460807032 * value)
	{
		____fileSouce_11 = value;
		Il2CppCodeGenWriteBarrier((&____fileSouce_11), value);
	}

	inline static int32_t get_offset_of__centerLatitudeLongitude_12() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____centerLatitudeLongitude_12)); }
	inline Vector2d_t1865246568  get__centerLatitudeLongitude_12() const { return ____centerLatitudeLongitude_12; }
	inline Vector2d_t1865246568 * get_address_of__centerLatitudeLongitude_12() { return &____centerLatitudeLongitude_12; }
	inline void set__centerLatitudeLongitude_12(Vector2d_t1865246568  value)
	{
		____centerLatitudeLongitude_12 = value;
	}

	inline static int32_t get_offset_of__centerMercator_13() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____centerMercator_13)); }
	inline Vector2d_t1865246568  get__centerMercator_13() const { return ____centerMercator_13; }
	inline Vector2d_t1865246568 * get_address_of__centerMercator_13() { return &____centerMercator_13; }
	inline void set__centerMercator_13(Vector2d_t1865246568  value)
	{
		____centerMercator_13 = value;
	}

	inline static int32_t get_offset_of__worldRelativeScale_14() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ____worldRelativeScale_14)); }
	inline float get__worldRelativeScale_14() const { return ____worldRelativeScale_14; }
	inline float* get_address_of__worldRelativeScale_14() { return &____worldRelativeScale_14; }
	inline void set__worldRelativeScale_14(float value)
	{
		____worldRelativeScale_14 = value;
	}

	inline static int32_t get_offset_of_OnInitialized_15() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158, ___OnInitialized_15)); }
	inline Action_t1264377477 * get_OnInitialized_15() const { return ___OnInitialized_15; }
	inline Action_t1264377477 ** get_address_of_OnInitialized_15() { return &___OnInitialized_15; }
	inline void set_OnInitialized_15(Action_t1264377477 * value)
	{
		___OnInitialized_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnInitialized_15), value);
	}
};

struct AbstractMap_t3082917158_StaticFields
{
public:
	// System.Action Mapbox.Unity.Map.AbstractMap::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_16() { return static_cast<int32_t>(offsetof(AbstractMap_t3082917158_StaticFields, ___U3CU3Ef__amU24cache0_16)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_16() const { return ___U3CU3Ef__amU24cache0_16; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_16() { return &___U3CU3Ef__amU24cache0_16; }
	inline void set_U3CU3Ef__amU24cache0_16(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMAP_T3082917158_H
#ifndef VOXELFETCHER_T3713644963_H
#define VOXELFETCHER_T3713644963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Voxels.VoxelFetcher
struct  VoxelFetcher_t3713644963  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Examples.Voxels.VoxelColorMapper[] Mapbox.Examples.Voxels.VoxelFetcher::_voxels
	VoxelColorMapperU5BU5D_t1422690960* ____voxels_2;

public:
	inline static int32_t get_offset_of__voxels_2() { return static_cast<int32_t>(offsetof(VoxelFetcher_t3713644963, ____voxels_2)); }
	inline VoxelColorMapperU5BU5D_t1422690960* get__voxels_2() const { return ____voxels_2; }
	inline VoxelColorMapperU5BU5D_t1422690960** get_address_of__voxels_2() { return &____voxels_2; }
	inline void set__voxels_2(VoxelColorMapperU5BU5D_t1422690960* value)
	{
		____voxels_2 = value;
		Il2CppCodeGenWriteBarrier((&____voxels_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELFETCHER_T3713644963_H
#ifndef VOXELTILE_T1944340880_H
#define VOXELTILE_T1944340880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Voxels.VoxelTile
struct  VoxelTile_t1944340880  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Examples.ForwardGeocodeUserInput Mapbox.Examples.Voxels.VoxelTile::_geocodeInput
	ForwardGeocodeUserInput_t2575136032 * ____geocodeInput_2;
	// System.Int32 Mapbox.Examples.Voxels.VoxelTile::_zoom
	int32_t ____zoom_3;
	// System.Single Mapbox.Examples.Voxels.VoxelTile::_elevationMultiplier
	float ____elevationMultiplier_4;
	// System.Int32 Mapbox.Examples.Voxels.VoxelTile::_voxelDepthPadding
	int32_t ____voxelDepthPadding_5;
	// System.Int32 Mapbox.Examples.Voxels.VoxelTile::_tileWidthInVoxels
	int32_t ____tileWidthInVoxels_6;
	// Mapbox.Examples.Voxels.VoxelFetcher Mapbox.Examples.Voxels.VoxelTile::_voxelFetcher
	VoxelFetcher_t3713644963 * ____voxelFetcher_7;
	// UnityEngine.GameObject Mapbox.Examples.Voxels.VoxelTile::_camera
	GameObject_t1113636619 * ____camera_8;
	// System.Int32 Mapbox.Examples.Voxels.VoxelTile::_voxelBatchCount
	int32_t ____voxelBatchCount_9;
	// System.String Mapbox.Examples.Voxels.VoxelTile::_styleUrl
	String_t* ____styleUrl_10;
	// Mapbox.Map.Map`1<Mapbox.Map.RasterTile> Mapbox.Examples.Voxels.VoxelTile::_raster
	Map_1_t1665010825 * ____raster_11;
	// Mapbox.Map.Map`1<Mapbox.Map.RawPngRasterTile> Mapbox.Examples.Voxels.VoxelTile::_elevation
	Map_1_t1070764774 * ____elevation_12;
	// UnityEngine.Texture2D Mapbox.Examples.Voxels.VoxelTile::_rasterTexture
	Texture2D_t3840446185 * ____rasterTexture_13;
	// UnityEngine.Texture2D Mapbox.Examples.Voxels.VoxelTile::_elevationTexture
	Texture2D_t3840446185 * ____elevationTexture_14;
	// Mapbox.Platform.IFileSource Mapbox.Examples.Voxels.VoxelTile::_fileSource
	RuntimeObject* ____fileSource_15;
	// System.Collections.Generic.List`1<Mapbox.Examples.Voxels.VoxelData> Mapbox.Examples.Voxels.VoxelTile::_voxels
	List_1_t3720956926 * ____voxels_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Mapbox.Examples.Voxels.VoxelTile::_instantiatedVoxels
	List_1_t2585711361 * ____instantiatedVoxels_17;
	// System.Single Mapbox.Examples.Voxels.VoxelTile::_tileScale
	float ____tileScale_18;

public:
	inline static int32_t get_offset_of__geocodeInput_2() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____geocodeInput_2)); }
	inline ForwardGeocodeUserInput_t2575136032 * get__geocodeInput_2() const { return ____geocodeInput_2; }
	inline ForwardGeocodeUserInput_t2575136032 ** get_address_of__geocodeInput_2() { return &____geocodeInput_2; }
	inline void set__geocodeInput_2(ForwardGeocodeUserInput_t2575136032 * value)
	{
		____geocodeInput_2 = value;
		Il2CppCodeGenWriteBarrier((&____geocodeInput_2), value);
	}

	inline static int32_t get_offset_of__zoom_3() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____zoom_3)); }
	inline int32_t get__zoom_3() const { return ____zoom_3; }
	inline int32_t* get_address_of__zoom_3() { return &____zoom_3; }
	inline void set__zoom_3(int32_t value)
	{
		____zoom_3 = value;
	}

	inline static int32_t get_offset_of__elevationMultiplier_4() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____elevationMultiplier_4)); }
	inline float get__elevationMultiplier_4() const { return ____elevationMultiplier_4; }
	inline float* get_address_of__elevationMultiplier_4() { return &____elevationMultiplier_4; }
	inline void set__elevationMultiplier_4(float value)
	{
		____elevationMultiplier_4 = value;
	}

	inline static int32_t get_offset_of__voxelDepthPadding_5() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____voxelDepthPadding_5)); }
	inline int32_t get__voxelDepthPadding_5() const { return ____voxelDepthPadding_5; }
	inline int32_t* get_address_of__voxelDepthPadding_5() { return &____voxelDepthPadding_5; }
	inline void set__voxelDepthPadding_5(int32_t value)
	{
		____voxelDepthPadding_5 = value;
	}

	inline static int32_t get_offset_of__tileWidthInVoxels_6() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____tileWidthInVoxels_6)); }
	inline int32_t get__tileWidthInVoxels_6() const { return ____tileWidthInVoxels_6; }
	inline int32_t* get_address_of__tileWidthInVoxels_6() { return &____tileWidthInVoxels_6; }
	inline void set__tileWidthInVoxels_6(int32_t value)
	{
		____tileWidthInVoxels_6 = value;
	}

	inline static int32_t get_offset_of__voxelFetcher_7() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____voxelFetcher_7)); }
	inline VoxelFetcher_t3713644963 * get__voxelFetcher_7() const { return ____voxelFetcher_7; }
	inline VoxelFetcher_t3713644963 ** get_address_of__voxelFetcher_7() { return &____voxelFetcher_7; }
	inline void set__voxelFetcher_7(VoxelFetcher_t3713644963 * value)
	{
		____voxelFetcher_7 = value;
		Il2CppCodeGenWriteBarrier((&____voxelFetcher_7), value);
	}

	inline static int32_t get_offset_of__camera_8() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____camera_8)); }
	inline GameObject_t1113636619 * get__camera_8() const { return ____camera_8; }
	inline GameObject_t1113636619 ** get_address_of__camera_8() { return &____camera_8; }
	inline void set__camera_8(GameObject_t1113636619 * value)
	{
		____camera_8 = value;
		Il2CppCodeGenWriteBarrier((&____camera_8), value);
	}

	inline static int32_t get_offset_of__voxelBatchCount_9() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____voxelBatchCount_9)); }
	inline int32_t get__voxelBatchCount_9() const { return ____voxelBatchCount_9; }
	inline int32_t* get_address_of__voxelBatchCount_9() { return &____voxelBatchCount_9; }
	inline void set__voxelBatchCount_9(int32_t value)
	{
		____voxelBatchCount_9 = value;
	}

	inline static int32_t get_offset_of__styleUrl_10() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____styleUrl_10)); }
	inline String_t* get__styleUrl_10() const { return ____styleUrl_10; }
	inline String_t** get_address_of__styleUrl_10() { return &____styleUrl_10; }
	inline void set__styleUrl_10(String_t* value)
	{
		____styleUrl_10 = value;
		Il2CppCodeGenWriteBarrier((&____styleUrl_10), value);
	}

	inline static int32_t get_offset_of__raster_11() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____raster_11)); }
	inline Map_1_t1665010825 * get__raster_11() const { return ____raster_11; }
	inline Map_1_t1665010825 ** get_address_of__raster_11() { return &____raster_11; }
	inline void set__raster_11(Map_1_t1665010825 * value)
	{
		____raster_11 = value;
		Il2CppCodeGenWriteBarrier((&____raster_11), value);
	}

	inline static int32_t get_offset_of__elevation_12() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____elevation_12)); }
	inline Map_1_t1070764774 * get__elevation_12() const { return ____elevation_12; }
	inline Map_1_t1070764774 ** get_address_of__elevation_12() { return &____elevation_12; }
	inline void set__elevation_12(Map_1_t1070764774 * value)
	{
		____elevation_12 = value;
		Il2CppCodeGenWriteBarrier((&____elevation_12), value);
	}

	inline static int32_t get_offset_of__rasterTexture_13() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____rasterTexture_13)); }
	inline Texture2D_t3840446185 * get__rasterTexture_13() const { return ____rasterTexture_13; }
	inline Texture2D_t3840446185 ** get_address_of__rasterTexture_13() { return &____rasterTexture_13; }
	inline void set__rasterTexture_13(Texture2D_t3840446185 * value)
	{
		____rasterTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&____rasterTexture_13), value);
	}

	inline static int32_t get_offset_of__elevationTexture_14() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____elevationTexture_14)); }
	inline Texture2D_t3840446185 * get__elevationTexture_14() const { return ____elevationTexture_14; }
	inline Texture2D_t3840446185 ** get_address_of__elevationTexture_14() { return &____elevationTexture_14; }
	inline void set__elevationTexture_14(Texture2D_t3840446185 * value)
	{
		____elevationTexture_14 = value;
		Il2CppCodeGenWriteBarrier((&____elevationTexture_14), value);
	}

	inline static int32_t get_offset_of__fileSource_15() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____fileSource_15)); }
	inline RuntimeObject* get__fileSource_15() const { return ____fileSource_15; }
	inline RuntimeObject** get_address_of__fileSource_15() { return &____fileSource_15; }
	inline void set__fileSource_15(RuntimeObject* value)
	{
		____fileSource_15 = value;
		Il2CppCodeGenWriteBarrier((&____fileSource_15), value);
	}

	inline static int32_t get_offset_of__voxels_16() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____voxels_16)); }
	inline List_1_t3720956926 * get__voxels_16() const { return ____voxels_16; }
	inline List_1_t3720956926 ** get_address_of__voxels_16() { return &____voxels_16; }
	inline void set__voxels_16(List_1_t3720956926 * value)
	{
		____voxels_16 = value;
		Il2CppCodeGenWriteBarrier((&____voxels_16), value);
	}

	inline static int32_t get_offset_of__instantiatedVoxels_17() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____instantiatedVoxels_17)); }
	inline List_1_t2585711361 * get__instantiatedVoxels_17() const { return ____instantiatedVoxels_17; }
	inline List_1_t2585711361 ** get_address_of__instantiatedVoxels_17() { return &____instantiatedVoxels_17; }
	inline void set__instantiatedVoxels_17(List_1_t2585711361 * value)
	{
		____instantiatedVoxels_17 = value;
		Il2CppCodeGenWriteBarrier((&____instantiatedVoxels_17), value);
	}

	inline static int32_t get_offset_of__tileScale_18() { return static_cast<int32_t>(offsetof(VoxelTile_t1944340880, ____tileScale_18)); }
	inline float get__tileScale_18() const { return ____tileScale_18; }
	inline float* get_address_of__tileScale_18() { return &____tileScale_18; }
	inline void set__tileScale_18(float value)
	{
		____tileScale_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELTILE_T1944340880_H
#ifndef LOCATIONPROVIDERFACTORY_T2702491050_H
#define LOCATIONPROVIDERFACTORY_T2702491050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Location.LocationProviderFactory
struct  LocationProviderFactory_t2702491050  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Location.DeviceLocationProvider Mapbox.Unity.Location.LocationProviderFactory::_deviceLocationProvider
	DeviceLocationProvider_t4095080695 * ____deviceLocationProvider_2;
	// Mapbox.Unity.Location.EditorLocationProvider Mapbox.Unity.Location.LocationProviderFactory::_editorLocationProvider
	EditorLocationProvider_t3344154418 * ____editorLocationProvider_3;
	// Mapbox.Unity.Location.TransformLocationProvider Mapbox.Unity.Location.LocationProviderFactory::_transformLocationProvider
	TransformLocationProvider_t3083760227 * ____transformLocationProvider_4;
	// Mapbox.Unity.Location.ILocationProvider Mapbox.Unity.Location.LocationProviderFactory::_defaultLocationProvider
	RuntimeObject* ____defaultLocationProvider_6;

public:
	inline static int32_t get_offset_of__deviceLocationProvider_2() { return static_cast<int32_t>(offsetof(LocationProviderFactory_t2702491050, ____deviceLocationProvider_2)); }
	inline DeviceLocationProvider_t4095080695 * get__deviceLocationProvider_2() const { return ____deviceLocationProvider_2; }
	inline DeviceLocationProvider_t4095080695 ** get_address_of__deviceLocationProvider_2() { return &____deviceLocationProvider_2; }
	inline void set__deviceLocationProvider_2(DeviceLocationProvider_t4095080695 * value)
	{
		____deviceLocationProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&____deviceLocationProvider_2), value);
	}

	inline static int32_t get_offset_of__editorLocationProvider_3() { return static_cast<int32_t>(offsetof(LocationProviderFactory_t2702491050, ____editorLocationProvider_3)); }
	inline EditorLocationProvider_t3344154418 * get__editorLocationProvider_3() const { return ____editorLocationProvider_3; }
	inline EditorLocationProvider_t3344154418 ** get_address_of__editorLocationProvider_3() { return &____editorLocationProvider_3; }
	inline void set__editorLocationProvider_3(EditorLocationProvider_t3344154418 * value)
	{
		____editorLocationProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&____editorLocationProvider_3), value);
	}

	inline static int32_t get_offset_of__transformLocationProvider_4() { return static_cast<int32_t>(offsetof(LocationProviderFactory_t2702491050, ____transformLocationProvider_4)); }
	inline TransformLocationProvider_t3083760227 * get__transformLocationProvider_4() const { return ____transformLocationProvider_4; }
	inline TransformLocationProvider_t3083760227 ** get_address_of__transformLocationProvider_4() { return &____transformLocationProvider_4; }
	inline void set__transformLocationProvider_4(TransformLocationProvider_t3083760227 * value)
	{
		____transformLocationProvider_4 = value;
		Il2CppCodeGenWriteBarrier((&____transformLocationProvider_4), value);
	}

	inline static int32_t get_offset_of__defaultLocationProvider_6() { return static_cast<int32_t>(offsetof(LocationProviderFactory_t2702491050, ____defaultLocationProvider_6)); }
	inline RuntimeObject* get__defaultLocationProvider_6() const { return ____defaultLocationProvider_6; }
	inline RuntimeObject** get_address_of__defaultLocationProvider_6() { return &____defaultLocationProvider_6; }
	inline void set__defaultLocationProvider_6(RuntimeObject* value)
	{
		____defaultLocationProvider_6 = value;
		Il2CppCodeGenWriteBarrier((&____defaultLocationProvider_6), value);
	}
};

struct LocationProviderFactory_t2702491050_StaticFields
{
public:
	// Mapbox.Unity.Location.LocationProviderFactory Mapbox.Unity.Location.LocationProviderFactory::_instance
	LocationProviderFactory_t2702491050 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(LocationProviderFactory_t2702491050_StaticFields, ____instance_5)); }
	inline LocationProviderFactory_t2702491050 * get__instance_5() const { return ____instance_5; }
	inline LocationProviderFactory_t2702491050 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(LocationProviderFactory_t2702491050 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((&____instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATIONPROVIDERFACTORY_T2702491050_H
#ifndef DEVICELOCATIONPROVIDER_T4095080695_H
#define DEVICELOCATIONPROVIDER_T4095080695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Location.DeviceLocationProvider
struct  DeviceLocationProvider_t4095080695  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mapbox.Unity.Location.DeviceLocationProvider::_desiredAccuracyInMeters
	float ____desiredAccuracyInMeters_2;
	// System.Single Mapbox.Unity.Location.DeviceLocationProvider::_updateDistanceInMeters
	float ____updateDistanceInMeters_3;
	// UnityEngine.Coroutine Mapbox.Unity.Location.DeviceLocationProvider::_pollRoutine
	Coroutine_t3829159415 * ____pollRoutine_4;
	// System.Double Mapbox.Unity.Location.DeviceLocationProvider::_lastLocationTimestamp
	double ____lastLocationTimestamp_5;
	// System.Double Mapbox.Unity.Location.DeviceLocationProvider::_lastHeadingTimestamp
	double ____lastHeadingTimestamp_6;
	// UnityEngine.WaitForSeconds Mapbox.Unity.Location.DeviceLocationProvider::_wait
	WaitForSeconds_t1699091251 * ____wait_7;
	// Mapbox.Utils.Vector2d Mapbox.Unity.Location.DeviceLocationProvider::_location
	Vector2d_t1865246568  ____location_8;
	// System.EventHandler`1<Mapbox.Unity.Location.LocationUpdatedEventArgs> Mapbox.Unity.Location.DeviceLocationProvider::OnLocationUpdated
	EventHandler_1_t1729069707 * ___OnLocationUpdated_9;
	// System.EventHandler`1<Mapbox.Unity.Location.HeadingUpdatedEventArgs> Mapbox.Unity.Location.DeviceLocationProvider::OnHeadingUpdated
	EventHandler_1_t2962207339 * ___OnHeadingUpdated_10;

public:
	inline static int32_t get_offset_of__desiredAccuracyInMeters_2() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ____desiredAccuracyInMeters_2)); }
	inline float get__desiredAccuracyInMeters_2() const { return ____desiredAccuracyInMeters_2; }
	inline float* get_address_of__desiredAccuracyInMeters_2() { return &____desiredAccuracyInMeters_2; }
	inline void set__desiredAccuracyInMeters_2(float value)
	{
		____desiredAccuracyInMeters_2 = value;
	}

	inline static int32_t get_offset_of__updateDistanceInMeters_3() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ____updateDistanceInMeters_3)); }
	inline float get__updateDistanceInMeters_3() const { return ____updateDistanceInMeters_3; }
	inline float* get_address_of__updateDistanceInMeters_3() { return &____updateDistanceInMeters_3; }
	inline void set__updateDistanceInMeters_3(float value)
	{
		____updateDistanceInMeters_3 = value;
	}

	inline static int32_t get_offset_of__pollRoutine_4() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ____pollRoutine_4)); }
	inline Coroutine_t3829159415 * get__pollRoutine_4() const { return ____pollRoutine_4; }
	inline Coroutine_t3829159415 ** get_address_of__pollRoutine_4() { return &____pollRoutine_4; }
	inline void set__pollRoutine_4(Coroutine_t3829159415 * value)
	{
		____pollRoutine_4 = value;
		Il2CppCodeGenWriteBarrier((&____pollRoutine_4), value);
	}

	inline static int32_t get_offset_of__lastLocationTimestamp_5() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ____lastLocationTimestamp_5)); }
	inline double get__lastLocationTimestamp_5() const { return ____lastLocationTimestamp_5; }
	inline double* get_address_of__lastLocationTimestamp_5() { return &____lastLocationTimestamp_5; }
	inline void set__lastLocationTimestamp_5(double value)
	{
		____lastLocationTimestamp_5 = value;
	}

	inline static int32_t get_offset_of__lastHeadingTimestamp_6() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ____lastHeadingTimestamp_6)); }
	inline double get__lastHeadingTimestamp_6() const { return ____lastHeadingTimestamp_6; }
	inline double* get_address_of__lastHeadingTimestamp_6() { return &____lastHeadingTimestamp_6; }
	inline void set__lastHeadingTimestamp_6(double value)
	{
		____lastHeadingTimestamp_6 = value;
	}

	inline static int32_t get_offset_of__wait_7() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ____wait_7)); }
	inline WaitForSeconds_t1699091251 * get__wait_7() const { return ____wait_7; }
	inline WaitForSeconds_t1699091251 ** get_address_of__wait_7() { return &____wait_7; }
	inline void set__wait_7(WaitForSeconds_t1699091251 * value)
	{
		____wait_7 = value;
		Il2CppCodeGenWriteBarrier((&____wait_7), value);
	}

	inline static int32_t get_offset_of__location_8() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ____location_8)); }
	inline Vector2d_t1865246568  get__location_8() const { return ____location_8; }
	inline Vector2d_t1865246568 * get_address_of__location_8() { return &____location_8; }
	inline void set__location_8(Vector2d_t1865246568  value)
	{
		____location_8 = value;
	}

	inline static int32_t get_offset_of_OnLocationUpdated_9() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ___OnLocationUpdated_9)); }
	inline EventHandler_1_t1729069707 * get_OnLocationUpdated_9() const { return ___OnLocationUpdated_9; }
	inline EventHandler_1_t1729069707 ** get_address_of_OnLocationUpdated_9() { return &___OnLocationUpdated_9; }
	inline void set_OnLocationUpdated_9(EventHandler_1_t1729069707 * value)
	{
		___OnLocationUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnLocationUpdated_9), value);
	}

	inline static int32_t get_offset_of_OnHeadingUpdated_10() { return static_cast<int32_t>(offsetof(DeviceLocationProvider_t4095080695, ___OnHeadingUpdated_10)); }
	inline EventHandler_1_t2962207339 * get_OnHeadingUpdated_10() const { return ___OnHeadingUpdated_10; }
	inline EventHandler_1_t2962207339 ** get_address_of_OnHeadingUpdated_10() { return &___OnHeadingUpdated_10; }
	inline void set_OnHeadingUpdated_10(EventHandler_1_t2962207339 * value)
	{
		___OnHeadingUpdated_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeadingUpdated_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICELOCATIONPROVIDER_T4095080695_H
#ifndef EDITORLOCATIONPROVIDER_T3344154418_H
#define EDITORLOCATIONPROVIDER_T3344154418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Location.EditorLocationProvider
struct  EditorLocationProvider_t3344154418  : public MonoBehaviour_t3962482529
{
public:
	// System.String Mapbox.Unity.Location.EditorLocationProvider::_latitudeLongitude
	String_t* ____latitudeLongitude_2;
	// System.Single Mapbox.Unity.Location.EditorLocationProvider::_heading
	float ____heading_3;
	// System.EventHandler`1<Mapbox.Unity.Location.HeadingUpdatedEventArgs> Mapbox.Unity.Location.EditorLocationProvider::OnHeadingUpdated
	EventHandler_1_t2962207339 * ___OnHeadingUpdated_4;
	// System.EventHandler`1<Mapbox.Unity.Location.LocationUpdatedEventArgs> Mapbox.Unity.Location.EditorLocationProvider::OnLocationUpdated
	EventHandler_1_t1729069707 * ___OnLocationUpdated_5;

public:
	inline static int32_t get_offset_of__latitudeLongitude_2() { return static_cast<int32_t>(offsetof(EditorLocationProvider_t3344154418, ____latitudeLongitude_2)); }
	inline String_t* get__latitudeLongitude_2() const { return ____latitudeLongitude_2; }
	inline String_t** get_address_of__latitudeLongitude_2() { return &____latitudeLongitude_2; }
	inline void set__latitudeLongitude_2(String_t* value)
	{
		____latitudeLongitude_2 = value;
		Il2CppCodeGenWriteBarrier((&____latitudeLongitude_2), value);
	}

	inline static int32_t get_offset_of__heading_3() { return static_cast<int32_t>(offsetof(EditorLocationProvider_t3344154418, ____heading_3)); }
	inline float get__heading_3() const { return ____heading_3; }
	inline float* get_address_of__heading_3() { return &____heading_3; }
	inline void set__heading_3(float value)
	{
		____heading_3 = value;
	}

	inline static int32_t get_offset_of_OnHeadingUpdated_4() { return static_cast<int32_t>(offsetof(EditorLocationProvider_t3344154418, ___OnHeadingUpdated_4)); }
	inline EventHandler_1_t2962207339 * get_OnHeadingUpdated_4() const { return ___OnHeadingUpdated_4; }
	inline EventHandler_1_t2962207339 ** get_address_of_OnHeadingUpdated_4() { return &___OnHeadingUpdated_4; }
	inline void set_OnHeadingUpdated_4(EventHandler_1_t2962207339 * value)
	{
		___OnHeadingUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeadingUpdated_4), value);
	}

	inline static int32_t get_offset_of_OnLocationUpdated_5() { return static_cast<int32_t>(offsetof(EditorLocationProvider_t3344154418, ___OnLocationUpdated_5)); }
	inline EventHandler_1_t1729069707 * get_OnLocationUpdated_5() const { return ___OnLocationUpdated_5; }
	inline EventHandler_1_t1729069707 ** get_address_of_OnLocationUpdated_5() { return &___OnLocationUpdated_5; }
	inline void set_OnLocationUpdated_5(EventHandler_1_t1729069707 * value)
	{
		___OnLocationUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnLocationUpdated_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORLOCATIONPROVIDER_T3344154418_H
#ifndef HIGHLIGHTFEATURE_T1851209614_H
#define HIGHLIGHTFEATURE_T1851209614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.HighlightFeature
struct  HighlightFeature_t1851209614  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Color> Mapbox.Examples.HighlightFeature::_original
	List_1_t4027761066 * ____original_2;
	// UnityEngine.Color Mapbox.Examples.HighlightFeature::_highlight
	Color_t2555686324  ____highlight_3;
	// System.Collections.Generic.List`1<UnityEngine.Material> Mapbox.Examples.HighlightFeature::_materials
	List_1_t1812449865 * ____materials_4;

public:
	inline static int32_t get_offset_of__original_2() { return static_cast<int32_t>(offsetof(HighlightFeature_t1851209614, ____original_2)); }
	inline List_1_t4027761066 * get__original_2() const { return ____original_2; }
	inline List_1_t4027761066 ** get_address_of__original_2() { return &____original_2; }
	inline void set__original_2(List_1_t4027761066 * value)
	{
		____original_2 = value;
		Il2CppCodeGenWriteBarrier((&____original_2), value);
	}

	inline static int32_t get_offset_of__highlight_3() { return static_cast<int32_t>(offsetof(HighlightFeature_t1851209614, ____highlight_3)); }
	inline Color_t2555686324  get__highlight_3() const { return ____highlight_3; }
	inline Color_t2555686324 * get_address_of__highlight_3() { return &____highlight_3; }
	inline void set__highlight_3(Color_t2555686324  value)
	{
		____highlight_3 = value;
	}

	inline static int32_t get_offset_of__materials_4() { return static_cast<int32_t>(offsetof(HighlightFeature_t1851209614, ____materials_4)); }
	inline List_1_t1812449865 * get__materials_4() const { return ____materials_4; }
	inline List_1_t1812449865 ** get_address_of__materials_4() { return &____materials_4; }
	inline void set__materials_4(List_1_t1812449865 * value)
	{
		____materials_4 = value;
		Il2CppCodeGenWriteBarrier((&____materials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTFEATURE_T1851209614_H
#ifndef MAKIHELPER_T3260814930_H
#define MAKIHELPER_T3260814930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.MakiHelper
struct  MakiHelper_t3260814930  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Mapbox.Examples.MakiHelper::_uiObject
	GameObject_t1113636619 * ____uiObject_4;

public:
	inline static int32_t get_offset_of__uiObject_4() { return static_cast<int32_t>(offsetof(MakiHelper_t3260814930, ____uiObject_4)); }
	inline GameObject_t1113636619 * get__uiObject_4() const { return ____uiObject_4; }
	inline GameObject_t1113636619 ** get_address_of__uiObject_4() { return &____uiObject_4; }
	inline void set__uiObject_4(GameObject_t1113636619 * value)
	{
		____uiObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____uiObject_4), value);
	}
};

struct MakiHelper_t3260814930_StaticFields
{
public:
	// UnityEngine.RectTransform Mapbox.Examples.MakiHelper::Parent
	RectTransform_t3704657025 * ___Parent_2;
	// UnityEngine.GameObject Mapbox.Examples.MakiHelper::UiPrefab
	GameObject_t1113636619 * ___UiPrefab_3;

public:
	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(MakiHelper_t3260814930_StaticFields, ___Parent_2)); }
	inline RectTransform_t3704657025 * get_Parent_2() const { return ___Parent_2; }
	inline RectTransform_t3704657025 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(RectTransform_t3704657025 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_UiPrefab_3() { return static_cast<int32_t>(offsetof(MakiHelper_t3260814930_StaticFields, ___UiPrefab_3)); }
	inline GameObject_t1113636619 * get_UiPrefab_3() const { return ___UiPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_UiPrefab_3() { return &___UiPrefab_3; }
	inline void set_UiPrefab_3(GameObject_t1113636619 * value)
	{
		___UiPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___UiPrefab_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAKIHELPER_T3260814930_H
#ifndef FORWARDGEOCODEUSERINPUT_T2575136032_H
#define FORWARDGEOCODEUSERINPUT_T2575136032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.ForwardGeocodeUserInput
struct  ForwardGeocodeUserInput_t2575136032  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField Mapbox.Examples.ForwardGeocodeUserInput::_inputField
	InputField_t3762917431 * ____inputField_2;
	// Mapbox.Geocoding.ForwardGeocodeResource Mapbox.Examples.ForwardGeocodeUserInput::_resource
	ForwardGeocodeResource_t367023433 * ____resource_3;
	// Mapbox.Utils.Vector2d Mapbox.Examples.ForwardGeocodeUserInput::_coordinate
	Vector2d_t1865246568  ____coordinate_4;
	// System.Boolean Mapbox.Examples.ForwardGeocodeUserInput::_hasResponse
	bool ____hasResponse_5;
	// Mapbox.Geocoding.ForwardGeocodeResponse Mapbox.Examples.ForwardGeocodeUserInput::<Response>k__BackingField
	ForwardGeocodeResponse_t2959476828 * ___U3CResponseU3Ek__BackingField_6;
	// System.Action`1<Mapbox.Geocoding.ForwardGeocodeResponse> Mapbox.Examples.ForwardGeocodeUserInput::OnGeocoderResponse
	Action_1_t3131944423 * ___OnGeocoderResponse_7;

public:
	inline static int32_t get_offset_of__inputField_2() { return static_cast<int32_t>(offsetof(ForwardGeocodeUserInput_t2575136032, ____inputField_2)); }
	inline InputField_t3762917431 * get__inputField_2() const { return ____inputField_2; }
	inline InputField_t3762917431 ** get_address_of__inputField_2() { return &____inputField_2; }
	inline void set__inputField_2(InputField_t3762917431 * value)
	{
		____inputField_2 = value;
		Il2CppCodeGenWriteBarrier((&____inputField_2), value);
	}

	inline static int32_t get_offset_of__resource_3() { return static_cast<int32_t>(offsetof(ForwardGeocodeUserInput_t2575136032, ____resource_3)); }
	inline ForwardGeocodeResource_t367023433 * get__resource_3() const { return ____resource_3; }
	inline ForwardGeocodeResource_t367023433 ** get_address_of__resource_3() { return &____resource_3; }
	inline void set__resource_3(ForwardGeocodeResource_t367023433 * value)
	{
		____resource_3 = value;
		Il2CppCodeGenWriteBarrier((&____resource_3), value);
	}

	inline static int32_t get_offset_of__coordinate_4() { return static_cast<int32_t>(offsetof(ForwardGeocodeUserInput_t2575136032, ____coordinate_4)); }
	inline Vector2d_t1865246568  get__coordinate_4() const { return ____coordinate_4; }
	inline Vector2d_t1865246568 * get_address_of__coordinate_4() { return &____coordinate_4; }
	inline void set__coordinate_4(Vector2d_t1865246568  value)
	{
		____coordinate_4 = value;
	}

	inline static int32_t get_offset_of__hasResponse_5() { return static_cast<int32_t>(offsetof(ForwardGeocodeUserInput_t2575136032, ____hasResponse_5)); }
	inline bool get__hasResponse_5() const { return ____hasResponse_5; }
	inline bool* get_address_of__hasResponse_5() { return &____hasResponse_5; }
	inline void set__hasResponse_5(bool value)
	{
		____hasResponse_5 = value;
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ForwardGeocodeUserInput_t2575136032, ___U3CResponseU3Ek__BackingField_6)); }
	inline ForwardGeocodeResponse_t2959476828 * get_U3CResponseU3Ek__BackingField_6() const { return ___U3CResponseU3Ek__BackingField_6; }
	inline ForwardGeocodeResponse_t2959476828 ** get_address_of_U3CResponseU3Ek__BackingField_6() { return &___U3CResponseU3Ek__BackingField_6; }
	inline void set_U3CResponseU3Ek__BackingField_6(ForwardGeocodeResponse_t2959476828 * value)
	{
		___U3CResponseU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_OnGeocoderResponse_7() { return static_cast<int32_t>(offsetof(ForwardGeocodeUserInput_t2575136032, ___OnGeocoderResponse_7)); }
	inline Action_1_t3131944423 * get_OnGeocoderResponse_7() const { return ___OnGeocoderResponse_7; }
	inline Action_1_t3131944423 ** get_address_of_OnGeocoderResponse_7() { return &___OnGeocoderResponse_7; }
	inline void set_OnGeocoderResponse_7(Action_1_t3131944423 * value)
	{
		___OnGeocoderResponse_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnGeocoderResponse_7), value);
	}
};

struct ForwardGeocodeUserInput_t2575136032_StaticFields
{
public:
	// System.Action`1<Mapbox.Geocoding.ForwardGeocodeResponse> Mapbox.Examples.ForwardGeocodeUserInput::<>f__am$cache0
	Action_1_t3131944423 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(ForwardGeocodeUserInput_t2575136032_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_1_t3131944423 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_1_t3131944423 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_1_t3131944423 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDGEOCODEUSERINPUT_T2575136032_H
#ifndef OBJECTINSPECTORMODIFIER_T3950592485_H
#define OBJECTINSPECTORMODIFIER_T3950592485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.ObjectInspectorModifier
struct  ObjectInspectorModifier_t3950592485  : public GameObjectModifier_t609190006
{
public:
	// Mapbox.Examples.FeatureUiMarker Mapbox.Examples.ObjectInspectorModifier::_marker
	FeatureUiMarker_t3847499068 * ____marker_3;

public:
	inline static int32_t get_offset_of__marker_3() { return static_cast<int32_t>(offsetof(ObjectInspectorModifier_t3950592485, ____marker_3)); }
	inline FeatureUiMarker_t3847499068 * get__marker_3() const { return ____marker_3; }
	inline FeatureUiMarker_t3847499068 ** get_address_of__marker_3() { return &____marker_3; }
	inline void set__marker_3(FeatureUiMarker_t3847499068 * value)
	{
		____marker_3 = value;
		Il2CppCodeGenWriteBarrier((&____marker_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTINSPECTORMODIFIER_T3950592485_H
#ifndef FEATUREUIMARKER_T3847499068_H
#define FEATUREUIMARKER_T3847499068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.FeatureUiMarker
struct  FeatureUiMarker_t3847499068  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Mapbox.Examples.FeatureUiMarker::_wrapperMarker
	Transform_t3600365921 * ____wrapperMarker_2;
	// UnityEngine.Transform Mapbox.Examples.FeatureUiMarker::_infoPanel
	Transform_t3600365921 * ____infoPanel_3;
	// UnityEngine.UI.Text Mapbox.Examples.FeatureUiMarker::_info
	Text_t1901882714 * ____info_4;
	// UnityEngine.Vector3[] Mapbox.Examples.FeatureUiMarker::_targetVerts
	Vector3U5BU5D_t1718750761* ____targetVerts_5;
	// Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour Mapbox.Examples.FeatureUiMarker::_selectedFeature
	FeatureBehaviour_t1816865007 * ____selectedFeature_6;

public:
	inline static int32_t get_offset_of__wrapperMarker_2() { return static_cast<int32_t>(offsetof(FeatureUiMarker_t3847499068, ____wrapperMarker_2)); }
	inline Transform_t3600365921 * get__wrapperMarker_2() const { return ____wrapperMarker_2; }
	inline Transform_t3600365921 ** get_address_of__wrapperMarker_2() { return &____wrapperMarker_2; }
	inline void set__wrapperMarker_2(Transform_t3600365921 * value)
	{
		____wrapperMarker_2 = value;
		Il2CppCodeGenWriteBarrier((&____wrapperMarker_2), value);
	}

	inline static int32_t get_offset_of__infoPanel_3() { return static_cast<int32_t>(offsetof(FeatureUiMarker_t3847499068, ____infoPanel_3)); }
	inline Transform_t3600365921 * get__infoPanel_3() const { return ____infoPanel_3; }
	inline Transform_t3600365921 ** get_address_of__infoPanel_3() { return &____infoPanel_3; }
	inline void set__infoPanel_3(Transform_t3600365921 * value)
	{
		____infoPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&____infoPanel_3), value);
	}

	inline static int32_t get_offset_of__info_4() { return static_cast<int32_t>(offsetof(FeatureUiMarker_t3847499068, ____info_4)); }
	inline Text_t1901882714 * get__info_4() const { return ____info_4; }
	inline Text_t1901882714 ** get_address_of__info_4() { return &____info_4; }
	inline void set__info_4(Text_t1901882714 * value)
	{
		____info_4 = value;
		Il2CppCodeGenWriteBarrier((&____info_4), value);
	}

	inline static int32_t get_offset_of__targetVerts_5() { return static_cast<int32_t>(offsetof(FeatureUiMarker_t3847499068, ____targetVerts_5)); }
	inline Vector3U5BU5D_t1718750761* get__targetVerts_5() const { return ____targetVerts_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of__targetVerts_5() { return &____targetVerts_5; }
	inline void set__targetVerts_5(Vector3U5BU5D_t1718750761* value)
	{
		____targetVerts_5 = value;
		Il2CppCodeGenWriteBarrier((&____targetVerts_5), value);
	}

	inline static int32_t get_offset_of__selectedFeature_6() { return static_cast<int32_t>(offsetof(FeatureUiMarker_t3847499068, ____selectedFeature_6)); }
	inline FeatureBehaviour_t1816865007 * get__selectedFeature_6() const { return ____selectedFeature_6; }
	inline FeatureBehaviour_t1816865007 ** get_address_of__selectedFeature_6() { return &____selectedFeature_6; }
	inline void set__selectedFeature_6(FeatureBehaviour_t1816865007 * value)
	{
		____selectedFeature_6 = value;
		Il2CppCodeGenWriteBarrier((&____selectedFeature_6), value);
	}
};

struct FeatureUiMarker_t3847499068_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String> Mapbox.Examples.FeatureUiMarker::<>f__am$cache0
	Func_2_t268983481 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(FeatureUiMarker_t3847499068_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t268983481 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t268983481 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t268983481 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATUREUIMARKER_T3847499068_H
#ifndef POIMARKERHELPER_T575496201_H
#define POIMARKERHELPER_T575496201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.PoiMarkerHelper
struct  PoiMarkerHelper_t575496201  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Mapbox.Examples.PoiMarkerHelper::_props
	Dictionary_2_t2865362463 * ____props_2;

public:
	inline static int32_t get_offset_of__props_2() { return static_cast<int32_t>(offsetof(PoiMarkerHelper_t575496201, ____props_2)); }
	inline Dictionary_2_t2865362463 * get__props_2() const { return ____props_2; }
	inline Dictionary_2_t2865362463 ** get_address_of__props_2() { return &____props_2; }
	inline void set__props_2(Dictionary_2_t2865362463 * value)
	{
		____props_2 = value;
		Il2CppCodeGenWriteBarrier((&____props_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POIMARKERHELPER_T575496201_H
#ifndef POSITIONWITHLOCATIONPROVIDER_T2078499108_H
#define POSITIONWITHLOCATIONPROVIDER_T2078499108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.PositionWithLocationProvider
struct  PositionWithLocationProvider_t2078499108  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Map.AbstractMap Mapbox.Examples.PositionWithLocationProvider::_map
	AbstractMap_t3082917158 * ____map_2;
	// System.Single Mapbox.Examples.PositionWithLocationProvider::_positionFollowFactor
	float ____positionFollowFactor_3;
	// System.Boolean Mapbox.Examples.PositionWithLocationProvider::_useTransformLocationProvider
	bool ____useTransformLocationProvider_4;
	// System.Boolean Mapbox.Examples.PositionWithLocationProvider::_isInitialized
	bool ____isInitialized_5;
	// Mapbox.Unity.Location.ILocationProvider Mapbox.Examples.PositionWithLocationProvider::_locationProvider
	RuntimeObject* ____locationProvider_6;
	// UnityEngine.Vector3 Mapbox.Examples.PositionWithLocationProvider::_targetPosition
	Vector3_t3722313464  ____targetPosition_7;

public:
	inline static int32_t get_offset_of__map_2() { return static_cast<int32_t>(offsetof(PositionWithLocationProvider_t2078499108, ____map_2)); }
	inline AbstractMap_t3082917158 * get__map_2() const { return ____map_2; }
	inline AbstractMap_t3082917158 ** get_address_of__map_2() { return &____map_2; }
	inline void set__map_2(AbstractMap_t3082917158 * value)
	{
		____map_2 = value;
		Il2CppCodeGenWriteBarrier((&____map_2), value);
	}

	inline static int32_t get_offset_of__positionFollowFactor_3() { return static_cast<int32_t>(offsetof(PositionWithLocationProvider_t2078499108, ____positionFollowFactor_3)); }
	inline float get__positionFollowFactor_3() const { return ____positionFollowFactor_3; }
	inline float* get_address_of__positionFollowFactor_3() { return &____positionFollowFactor_3; }
	inline void set__positionFollowFactor_3(float value)
	{
		____positionFollowFactor_3 = value;
	}

	inline static int32_t get_offset_of__useTransformLocationProvider_4() { return static_cast<int32_t>(offsetof(PositionWithLocationProvider_t2078499108, ____useTransformLocationProvider_4)); }
	inline bool get__useTransformLocationProvider_4() const { return ____useTransformLocationProvider_4; }
	inline bool* get_address_of__useTransformLocationProvider_4() { return &____useTransformLocationProvider_4; }
	inline void set__useTransformLocationProvider_4(bool value)
	{
		____useTransformLocationProvider_4 = value;
	}

	inline static int32_t get_offset_of__isInitialized_5() { return static_cast<int32_t>(offsetof(PositionWithLocationProvider_t2078499108, ____isInitialized_5)); }
	inline bool get__isInitialized_5() const { return ____isInitialized_5; }
	inline bool* get_address_of__isInitialized_5() { return &____isInitialized_5; }
	inline void set__isInitialized_5(bool value)
	{
		____isInitialized_5 = value;
	}

	inline static int32_t get_offset_of__locationProvider_6() { return static_cast<int32_t>(offsetof(PositionWithLocationProvider_t2078499108, ____locationProvider_6)); }
	inline RuntimeObject* get__locationProvider_6() const { return ____locationProvider_6; }
	inline RuntimeObject** get_address_of__locationProvider_6() { return &____locationProvider_6; }
	inline void set__locationProvider_6(RuntimeObject* value)
	{
		____locationProvider_6 = value;
		Il2CppCodeGenWriteBarrier((&____locationProvider_6), value);
	}

	inline static int32_t get_offset_of__targetPosition_7() { return static_cast<int32_t>(offsetof(PositionWithLocationProvider_t2078499108, ____targetPosition_7)); }
	inline Vector3_t3722313464  get__targetPosition_7() const { return ____targetPosition_7; }
	inline Vector3_t3722313464 * get_address_of__targetPosition_7() { return &____targetPosition_7; }
	inline void set__targetPosition_7(Vector3_t3722313464  value)
	{
		____targetPosition_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONWITHLOCATIONPROVIDER_T2078499108_H
#ifndef REVERSEGEOCODEUSERINPUT_T2632079094_H
#define REVERSEGEOCODEUSERINPUT_T2632079094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.ReverseGeocodeUserInput
struct  ReverseGeocodeUserInput_t2632079094  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField Mapbox.Examples.ReverseGeocodeUserInput::_inputField
	InputField_t3762917431 * ____inputField_2;
	// Mapbox.Geocoding.ReverseGeocodeResource Mapbox.Examples.ReverseGeocodeUserInput::_resource
	ReverseGeocodeResource_t2777886177 * ____resource_3;
	// Mapbox.Geocoding.Geocoder Mapbox.Examples.ReverseGeocodeUserInput::_geocoder
	Geocoder_t3195298050 * ____geocoder_4;
	// Mapbox.Utils.Vector2d Mapbox.Examples.ReverseGeocodeUserInput::_coordinate
	Vector2d_t1865246568  ____coordinate_5;
	// System.Boolean Mapbox.Examples.ReverseGeocodeUserInput::_hasResponse
	bool ____hasResponse_6;
	// Mapbox.Geocoding.ReverseGeocodeResponse Mapbox.Examples.ReverseGeocodeUserInput::<Response>k__BackingField
	ReverseGeocodeResponse_t3723437562 * ___U3CResponseU3Ek__BackingField_7;
	// System.EventHandler`1<System.EventArgs> Mapbox.Examples.ReverseGeocodeUserInput::OnGeocoderResponse
	EventHandler_1_t1515976428 * ___OnGeocoderResponse_8;

public:
	inline static int32_t get_offset_of__inputField_2() { return static_cast<int32_t>(offsetof(ReverseGeocodeUserInput_t2632079094, ____inputField_2)); }
	inline InputField_t3762917431 * get__inputField_2() const { return ____inputField_2; }
	inline InputField_t3762917431 ** get_address_of__inputField_2() { return &____inputField_2; }
	inline void set__inputField_2(InputField_t3762917431 * value)
	{
		____inputField_2 = value;
		Il2CppCodeGenWriteBarrier((&____inputField_2), value);
	}

	inline static int32_t get_offset_of__resource_3() { return static_cast<int32_t>(offsetof(ReverseGeocodeUserInput_t2632079094, ____resource_3)); }
	inline ReverseGeocodeResource_t2777886177 * get__resource_3() const { return ____resource_3; }
	inline ReverseGeocodeResource_t2777886177 ** get_address_of__resource_3() { return &____resource_3; }
	inline void set__resource_3(ReverseGeocodeResource_t2777886177 * value)
	{
		____resource_3 = value;
		Il2CppCodeGenWriteBarrier((&____resource_3), value);
	}

	inline static int32_t get_offset_of__geocoder_4() { return static_cast<int32_t>(offsetof(ReverseGeocodeUserInput_t2632079094, ____geocoder_4)); }
	inline Geocoder_t3195298050 * get__geocoder_4() const { return ____geocoder_4; }
	inline Geocoder_t3195298050 ** get_address_of__geocoder_4() { return &____geocoder_4; }
	inline void set__geocoder_4(Geocoder_t3195298050 * value)
	{
		____geocoder_4 = value;
		Il2CppCodeGenWriteBarrier((&____geocoder_4), value);
	}

	inline static int32_t get_offset_of__coordinate_5() { return static_cast<int32_t>(offsetof(ReverseGeocodeUserInput_t2632079094, ____coordinate_5)); }
	inline Vector2d_t1865246568  get__coordinate_5() const { return ____coordinate_5; }
	inline Vector2d_t1865246568 * get_address_of__coordinate_5() { return &____coordinate_5; }
	inline void set__coordinate_5(Vector2d_t1865246568  value)
	{
		____coordinate_5 = value;
	}

	inline static int32_t get_offset_of__hasResponse_6() { return static_cast<int32_t>(offsetof(ReverseGeocodeUserInput_t2632079094, ____hasResponse_6)); }
	inline bool get__hasResponse_6() const { return ____hasResponse_6; }
	inline bool* get_address_of__hasResponse_6() { return &____hasResponse_6; }
	inline void set__hasResponse_6(bool value)
	{
		____hasResponse_6 = value;
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ReverseGeocodeUserInput_t2632079094, ___U3CResponseU3Ek__BackingField_7)); }
	inline ReverseGeocodeResponse_t3723437562 * get_U3CResponseU3Ek__BackingField_7() const { return ___U3CResponseU3Ek__BackingField_7; }
	inline ReverseGeocodeResponse_t3723437562 ** get_address_of_U3CResponseU3Ek__BackingField_7() { return &___U3CResponseU3Ek__BackingField_7; }
	inline void set_U3CResponseU3Ek__BackingField_7(ReverseGeocodeResponse_t3723437562 * value)
	{
		___U3CResponseU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_OnGeocoderResponse_8() { return static_cast<int32_t>(offsetof(ReverseGeocodeUserInput_t2632079094, ___OnGeocoderResponse_8)); }
	inline EventHandler_1_t1515976428 * get_OnGeocoderResponse_8() const { return ___OnGeocoderResponse_8; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnGeocoderResponse_8() { return &___OnGeocoderResponse_8; }
	inline void set_OnGeocoderResponse_8(EventHandler_1_t1515976428 * value)
	{
		___OnGeocoderResponse_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnGeocoderResponse_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVERSEGEOCODEUSERINPUT_T2632079094_H
#ifndef FEATURESELECTIONDETECTOR_T508482069_H
#define FEATURESELECTIONDETECTOR_T508482069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.FeatureSelectionDetector
struct  FeatureSelectionDetector_t508482069  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Examples.FeatureUiMarker Mapbox.Examples.FeatureSelectionDetector::_marker
	FeatureUiMarker_t3847499068 * ____marker_2;
	// Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour Mapbox.Examples.FeatureSelectionDetector::_feature
	FeatureBehaviour_t1816865007 * ____feature_3;

public:
	inline static int32_t get_offset_of__marker_2() { return static_cast<int32_t>(offsetof(FeatureSelectionDetector_t508482069, ____marker_2)); }
	inline FeatureUiMarker_t3847499068 * get__marker_2() const { return ____marker_2; }
	inline FeatureUiMarker_t3847499068 ** get_address_of__marker_2() { return &____marker_2; }
	inline void set__marker_2(FeatureUiMarker_t3847499068 * value)
	{
		____marker_2 = value;
		Il2CppCodeGenWriteBarrier((&____marker_2), value);
	}

	inline static int32_t get_offset_of__feature_3() { return static_cast<int32_t>(offsetof(FeatureSelectionDetector_t508482069, ____feature_3)); }
	inline FeatureBehaviour_t1816865007 * get__feature_3() const { return ____feature_3; }
	inline FeatureBehaviour_t1816865007 ** get_address_of__feature_3() { return &____feature_3; }
	inline void set__feature_3(FeatureBehaviour_t1816865007 * value)
	{
		____feature_3 = value;
		Il2CppCodeGenWriteBarrier((&____feature_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURESELECTIONDETECTOR_T508482069_H
#ifndef LOADINGPANELCONTROLLER_T2494933297_H
#define LOADINGPANELCONTROLLER_T2494933297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.LoadingPanelController
struct  LoadingPanelController_t2494933297  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Map.MapVisualizer Mapbox.Examples.LoadingPanelController::MapVisualizer
	MapVisualizer_t2953540877 * ___MapVisualizer_2;
	// UnityEngine.GameObject Mapbox.Examples.LoadingPanelController::Content
	GameObject_t1113636619 * ___Content_3;

public:
	inline static int32_t get_offset_of_MapVisualizer_2() { return static_cast<int32_t>(offsetof(LoadingPanelController_t2494933297, ___MapVisualizer_2)); }
	inline MapVisualizer_t2953540877 * get_MapVisualizer_2() const { return ___MapVisualizer_2; }
	inline MapVisualizer_t2953540877 ** get_address_of_MapVisualizer_2() { return &___MapVisualizer_2; }
	inline void set_MapVisualizer_2(MapVisualizer_t2953540877 * value)
	{
		___MapVisualizer_2 = value;
		Il2CppCodeGenWriteBarrier((&___MapVisualizer_2), value);
	}

	inline static int32_t get_offset_of_Content_3() { return static_cast<int32_t>(offsetof(LoadingPanelController_t2494933297, ___Content_3)); }
	inline GameObject_t1113636619 * get_Content_3() const { return ___Content_3; }
	inline GameObject_t1113636619 ** get_address_of_Content_3() { return &___Content_3; }
	inline void set_Content_3(GameObject_t1113636619 * value)
	{
		___Content_3 = value;
		Il2CppCodeGenWriteBarrier((&___Content_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGPANELCONTROLLER_T2494933297_H
#ifndef ROTATEWITHLOCATIONPROVIDER_T2777253481_H
#define ROTATEWITHLOCATIONPROVIDER_T2777253481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.RotateWithLocationProvider
struct  RotateWithLocationProvider_t2777253481  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mapbox.Examples.RotateWithLocationProvider::_rotationFollowFactor
	float ____rotationFollowFactor_2;
	// System.Boolean Mapbox.Examples.RotateWithLocationProvider::_rotateZ
	bool ____rotateZ_3;
	// System.Boolean Mapbox.Examples.RotateWithLocationProvider::_useTransformLocationProvider
	bool ____useTransformLocationProvider_4;
	// Mapbox.Unity.Location.ILocationProvider Mapbox.Examples.RotateWithLocationProvider::_locationProvider
	RuntimeObject* ____locationProvider_5;
	// UnityEngine.Vector3 Mapbox.Examples.RotateWithLocationProvider::_targetPosition
	Vector3_t3722313464  ____targetPosition_6;

public:
	inline static int32_t get_offset_of__rotationFollowFactor_2() { return static_cast<int32_t>(offsetof(RotateWithLocationProvider_t2777253481, ____rotationFollowFactor_2)); }
	inline float get__rotationFollowFactor_2() const { return ____rotationFollowFactor_2; }
	inline float* get_address_of__rotationFollowFactor_2() { return &____rotationFollowFactor_2; }
	inline void set__rotationFollowFactor_2(float value)
	{
		____rotationFollowFactor_2 = value;
	}

	inline static int32_t get_offset_of__rotateZ_3() { return static_cast<int32_t>(offsetof(RotateWithLocationProvider_t2777253481, ____rotateZ_3)); }
	inline bool get__rotateZ_3() const { return ____rotateZ_3; }
	inline bool* get_address_of__rotateZ_3() { return &____rotateZ_3; }
	inline void set__rotateZ_3(bool value)
	{
		____rotateZ_3 = value;
	}

	inline static int32_t get_offset_of__useTransformLocationProvider_4() { return static_cast<int32_t>(offsetof(RotateWithLocationProvider_t2777253481, ____useTransformLocationProvider_4)); }
	inline bool get__useTransformLocationProvider_4() const { return ____useTransformLocationProvider_4; }
	inline bool* get_address_of__useTransformLocationProvider_4() { return &____useTransformLocationProvider_4; }
	inline void set__useTransformLocationProvider_4(bool value)
	{
		____useTransformLocationProvider_4 = value;
	}

	inline static int32_t get_offset_of__locationProvider_5() { return static_cast<int32_t>(offsetof(RotateWithLocationProvider_t2777253481, ____locationProvider_5)); }
	inline RuntimeObject* get__locationProvider_5() const { return ____locationProvider_5; }
	inline RuntimeObject** get_address_of__locationProvider_5() { return &____locationProvider_5; }
	inline void set__locationProvider_5(RuntimeObject* value)
	{
		____locationProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&____locationProvider_5), value);
	}

	inline static int32_t get_offset_of__targetPosition_6() { return static_cast<int32_t>(offsetof(RotateWithLocationProvider_t2777253481, ____targetPosition_6)); }
	inline Vector3_t3722313464  get__targetPosition_6() const { return ____targetPosition_6; }
	inline Vector3_t3722313464 * get_address_of__targetPosition_6() { return &____targetPosition_6; }
	inline void set__targetPosition_6(Vector3_t3722313464  value)
	{
		____targetPosition_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEWITHLOCATIONPROVIDER_T2777253481_H
#ifndef CHANGESHADOWDISTANCE_T4294610605_H
#define CHANGESHADOWDISTANCE_T4294610605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.ChangeShadowDistance
struct  ChangeShadowDistance_t4294610605  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Mapbox.Examples.ChangeShadowDistance::ShadowDistance
	int32_t ___ShadowDistance_2;

public:
	inline static int32_t get_offset_of_ShadowDistance_2() { return static_cast<int32_t>(offsetof(ChangeShadowDistance_t4294610605, ___ShadowDistance_2)); }
	inline int32_t get_ShadowDistance_2() const { return ___ShadowDistance_2; }
	inline int32_t* get_address_of_ShadowDistance_2() { return &___ShadowDistance_2; }
	inline void set_ShadowDistance_2(int32_t value)
	{
		___ShadowDistance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGESHADOWDISTANCE_T4294610605_H
#ifndef INITIALIZEMAPWITHLOCATIONPROVIDER_T2863001309_H
#define INITIALIZEMAPWITHLOCATIONPROVIDER_T2863001309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.InitializeMapWithLocationProvider
struct  InitializeMapWithLocationProvider_t2863001309  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Map.AbstractMap Mapbox.Unity.Map.InitializeMapWithLocationProvider::_map
	AbstractMap_t3082917158 * ____map_2;
	// Mapbox.Unity.Location.ILocationProvider Mapbox.Unity.Map.InitializeMapWithLocationProvider::_locationProvider
	RuntimeObject* ____locationProvider_3;

public:
	inline static int32_t get_offset_of__map_2() { return static_cast<int32_t>(offsetof(InitializeMapWithLocationProvider_t2863001309, ____map_2)); }
	inline AbstractMap_t3082917158 * get__map_2() const { return ____map_2; }
	inline AbstractMap_t3082917158 ** get_address_of__map_2() { return &____map_2; }
	inline void set__map_2(AbstractMap_t3082917158 * value)
	{
		____map_2 = value;
		Il2CppCodeGenWriteBarrier((&____map_2), value);
	}

	inline static int32_t get_offset_of__locationProvider_3() { return static_cast<int32_t>(offsetof(InitializeMapWithLocationProvider_t2863001309, ____locationProvider_3)); }
	inline RuntimeObject* get__locationProvider_3() const { return ____locationProvider_3; }
	inline RuntimeObject** get_address_of__locationProvider_3() { return &____locationProvider_3; }
	inline void set__locationProvider_3(RuntimeObject* value)
	{
		____locationProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&____locationProvider_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZEMAPWITHLOCATIONPROVIDER_T2863001309_H
#ifndef FEATUREBEHAVIOUR_T1816865007_H
#define FEATUREBEHAVIOUR_T1816865007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour
struct  FeatureBehaviour_t1816865007  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour::<Transform>k__BackingField
	Transform_t3600365921 * ___U3CTransformU3Ek__BackingField_2;
	// Mapbox.Unity.MeshGeneration.Data.VectorFeatureUnity Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour::Data
	VectorFeatureUnity_t1815898701 * ___Data_3;
	// System.String Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour::DataString
	String_t* ___DataString_4;

public:
	inline static int32_t get_offset_of_U3CTransformU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FeatureBehaviour_t1816865007, ___U3CTransformU3Ek__BackingField_2)); }
	inline Transform_t3600365921 * get_U3CTransformU3Ek__BackingField_2() const { return ___U3CTransformU3Ek__BackingField_2; }
	inline Transform_t3600365921 ** get_address_of_U3CTransformU3Ek__BackingField_2() { return &___U3CTransformU3Ek__BackingField_2; }
	inline void set_U3CTransformU3Ek__BackingField_2(Transform_t3600365921 * value)
	{
		___U3CTransformU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTransformU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_Data_3() { return static_cast<int32_t>(offsetof(FeatureBehaviour_t1816865007, ___Data_3)); }
	inline VectorFeatureUnity_t1815898701 * get_Data_3() const { return ___Data_3; }
	inline VectorFeatureUnity_t1815898701 ** get_address_of_Data_3() { return &___Data_3; }
	inline void set_Data_3(VectorFeatureUnity_t1815898701 * value)
	{
		___Data_3 = value;
		Il2CppCodeGenWriteBarrier((&___Data_3), value);
	}

	inline static int32_t get_offset_of_DataString_4() { return static_cast<int32_t>(offsetof(FeatureBehaviour_t1816865007, ___DataString_4)); }
	inline String_t* get_DataString_4() const { return ___DataString_4; }
	inline String_t** get_address_of_DataString_4() { return &___DataString_4; }
	inline void set_DataString_4(String_t* value)
	{
		___DataString_4 = value;
		Il2CppCodeGenWriteBarrier((&___DataString_4), value);
	}
};

struct FeatureBehaviour_t1816865007_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String> Mapbox.Unity.MeshGeneration.Components.FeatureBehaviour::<>f__am$cache0
	Func_2_t268983481 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(FeatureBehaviour_t1816865007_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t268983481 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t268983481 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t268983481 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATUREBEHAVIOUR_T1816865007_H
#ifndef TEXTURESELECTOR_T4065682441_H
#define TEXTURESELECTOR_T4065682441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Components.TextureSelector
struct  TextureSelector_t4065682441  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Components.TextureSelector::_useSatelliteRoof
	bool ____useSatelliteRoof_2;
	// System.Boolean Mapbox.Unity.MeshGeneration.Components.TextureSelector::_textureTop
	bool ____textureTop_3;
	// System.Boolean Mapbox.Unity.MeshGeneration.Components.TextureSelector::_textureSides
	bool ____textureSides_4;
	// Mapbox.Unity.MeshGeneration.Data.UnityTile Mapbox.Unity.MeshGeneration.Components.TextureSelector::_tile
	UnityTile_t2405085845 * ____tile_5;
	// UnityEngine.MeshRenderer Mapbox.Unity.MeshGeneration.Components.TextureSelector::_meshRenderer
	MeshRenderer_t587009260 * ____meshRenderer_6;
	// UnityEngine.Material[] Mapbox.Unity.MeshGeneration.Components.TextureSelector::_topTextures
	MaterialU5BU5D_t561872642* ____topTextures_7;
	// UnityEngine.Material[] Mapbox.Unity.MeshGeneration.Components.TextureSelector::_sideTextures
	MaterialU5BU5D_t561872642* ____sideTextures_8;

public:
	inline static int32_t get_offset_of__useSatelliteRoof_2() { return static_cast<int32_t>(offsetof(TextureSelector_t4065682441, ____useSatelliteRoof_2)); }
	inline bool get__useSatelliteRoof_2() const { return ____useSatelliteRoof_2; }
	inline bool* get_address_of__useSatelliteRoof_2() { return &____useSatelliteRoof_2; }
	inline void set__useSatelliteRoof_2(bool value)
	{
		____useSatelliteRoof_2 = value;
	}

	inline static int32_t get_offset_of__textureTop_3() { return static_cast<int32_t>(offsetof(TextureSelector_t4065682441, ____textureTop_3)); }
	inline bool get__textureTop_3() const { return ____textureTop_3; }
	inline bool* get_address_of__textureTop_3() { return &____textureTop_3; }
	inline void set__textureTop_3(bool value)
	{
		____textureTop_3 = value;
	}

	inline static int32_t get_offset_of__textureSides_4() { return static_cast<int32_t>(offsetof(TextureSelector_t4065682441, ____textureSides_4)); }
	inline bool get__textureSides_4() const { return ____textureSides_4; }
	inline bool* get_address_of__textureSides_4() { return &____textureSides_4; }
	inline void set__textureSides_4(bool value)
	{
		____textureSides_4 = value;
	}

	inline static int32_t get_offset_of__tile_5() { return static_cast<int32_t>(offsetof(TextureSelector_t4065682441, ____tile_5)); }
	inline UnityTile_t2405085845 * get__tile_5() const { return ____tile_5; }
	inline UnityTile_t2405085845 ** get_address_of__tile_5() { return &____tile_5; }
	inline void set__tile_5(UnityTile_t2405085845 * value)
	{
		____tile_5 = value;
		Il2CppCodeGenWriteBarrier((&____tile_5), value);
	}

	inline static int32_t get_offset_of__meshRenderer_6() { return static_cast<int32_t>(offsetof(TextureSelector_t4065682441, ____meshRenderer_6)); }
	inline MeshRenderer_t587009260 * get__meshRenderer_6() const { return ____meshRenderer_6; }
	inline MeshRenderer_t587009260 ** get_address_of__meshRenderer_6() { return &____meshRenderer_6; }
	inline void set__meshRenderer_6(MeshRenderer_t587009260 * value)
	{
		____meshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&____meshRenderer_6), value);
	}

	inline static int32_t get_offset_of__topTextures_7() { return static_cast<int32_t>(offsetof(TextureSelector_t4065682441, ____topTextures_7)); }
	inline MaterialU5BU5D_t561872642* get__topTextures_7() const { return ____topTextures_7; }
	inline MaterialU5BU5D_t561872642** get_address_of__topTextures_7() { return &____topTextures_7; }
	inline void set__topTextures_7(MaterialU5BU5D_t561872642* value)
	{
		____topTextures_7 = value;
		Il2CppCodeGenWriteBarrier((&____topTextures_7), value);
	}

	inline static int32_t get_offset_of__sideTextures_8() { return static_cast<int32_t>(offsetof(TextureSelector_t4065682441, ____sideTextures_8)); }
	inline MaterialU5BU5D_t561872642* get__sideTextures_8() const { return ____sideTextures_8; }
	inline MaterialU5BU5D_t561872642** get_address_of__sideTextures_8() { return &____sideTextures_8; }
	inline void set__sideTextures_8(MaterialU5BU5D_t561872642* value)
	{
		____sideTextures_8 = value;
		Il2CppCodeGenWriteBarrier((&____sideTextures_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURESELECTOR_T4065682441_H
#ifndef VERTEXDEBUGGER_T3902268203_H
#define VERTEXDEBUGGER_T3902268203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Components.VertexDebugger
struct  VertexDebugger_t3902268203  : public MonoBehaviour_t3962482529
{
public:
	// System.String Mapbox.Unity.MeshGeneration.Components.VertexDebugger::Triangles
	String_t* ___Triangles_2;

public:
	inline static int32_t get_offset_of_Triangles_2() { return static_cast<int32_t>(offsetof(VertexDebugger_t3902268203, ___Triangles_2)); }
	inline String_t* get_Triangles_2() const { return ___Triangles_2; }
	inline String_t** get_address_of_Triangles_2() { return &___Triangles_2; }
	inline void set_Triangles_2(String_t* value)
	{
		___Triangles_2 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXDEBUGGER_T3902268203_H
#ifndef VERTEXDEBUGGERGIZMO_T66328475_H
#define VERTEXDEBUGGERGIZMO_T66328475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Components.VertexDebuggerGizmo
struct  VertexDebuggerGizmo_t66328475  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXDEBUGGERGIZMO_T66328475_H
#ifndef UNITYTILE_T2405085845_H
#define UNITYTILE_T2405085845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Data.UnityTile
struct  UnityTile_t2405085845  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D Mapbox.Unity.MeshGeneration.Data.UnityTile::_rasterData
	Texture2D_t3840446185 * ____rasterData_2;
	// System.Single[] Mapbox.Unity.MeshGeneration.Data.UnityTile::_heightData
	SingleU5BU5D_t1444911251* ____heightData_3;
	// System.Single Mapbox.Unity.MeshGeneration.Data.UnityTile::_relativeScale
	float ____relativeScale_4;
	// UnityEngine.Texture2D Mapbox.Unity.MeshGeneration.Data.UnityTile::_heightTexture
	Texture2D_t3840446185 * ____heightTexture_5;
	// UnityEngine.Texture2D Mapbox.Unity.MeshGeneration.Data.UnityTile::_loadingTexture
	Texture2D_t3840446185 * ____loadingTexture_6;
	// System.Collections.Generic.List`1<Mapbox.Map.Tile> Mapbox.Unity.MeshGeneration.Data.UnityTile::_tiles
	List_1_t2569976014 * ____tiles_7;
	// UnityEngine.MeshRenderer Mapbox.Unity.MeshGeneration.Data.UnityTile::_meshRenderer
	MeshRenderer_t587009260 * ____meshRenderer_8;
	// UnityEngine.MeshFilter Mapbox.Unity.MeshGeneration.Data.UnityTile::_meshFilter
	MeshFilter_t3523625662 * ____meshFilter_9;
	// UnityEngine.Collider Mapbox.Unity.MeshGeneration.Data.UnityTile::_collider
	Collider_t1773347010 * ____collider_10;
	// System.String Mapbox.Unity.MeshGeneration.Data.UnityTile::_vectorData
	String_t* ____vectorData_11;
	// Mapbox.Utils.RectD Mapbox.Unity.MeshGeneration.Data.UnityTile::_rect
	RectD_t151583371  ____rect_12;
	// Mapbox.Map.UnwrappedTileId Mapbox.Unity.MeshGeneration.Data.UnityTile::_unwrappedTileId
	UnwrappedTileId_t2586853537  ____unwrappedTileId_13;
	// Mapbox.Map.CanonicalTileId Mapbox.Unity.MeshGeneration.Data.UnityTile::_canonicalTileId
	CanonicalTileId_t4184902996  ____canonicalTileId_14;
	// System.Single Mapbox.Unity.MeshGeneration.Data.UnityTile::<TileScale>k__BackingField
	float ___U3CTileScaleU3Ek__BackingField_15;
	// Mapbox.Unity.MeshGeneration.Enums.TilePropertyState Mapbox.Unity.MeshGeneration.Data.UnityTile::RasterDataState
	int32_t ___RasterDataState_16;
	// Mapbox.Unity.MeshGeneration.Enums.TilePropertyState Mapbox.Unity.MeshGeneration.Data.UnityTile::HeightDataState
	int32_t ___HeightDataState_17;
	// Mapbox.Unity.MeshGeneration.Enums.TilePropertyState Mapbox.Unity.MeshGeneration.Data.UnityTile::VectorDataState
	int32_t ___VectorDataState_18;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::OnHeightDataChanged
	Action_1_t2577553440 * ___OnHeightDataChanged_19;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::OnRasterDataChanged
	Action_1_t2577553440 * ___OnRasterDataChanged_20;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::OnVectorDataChanged
	Action_1_t2577553440 * ___OnVectorDataChanged_21;

public:
	inline static int32_t get_offset_of__rasterData_2() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____rasterData_2)); }
	inline Texture2D_t3840446185 * get__rasterData_2() const { return ____rasterData_2; }
	inline Texture2D_t3840446185 ** get_address_of__rasterData_2() { return &____rasterData_2; }
	inline void set__rasterData_2(Texture2D_t3840446185 * value)
	{
		____rasterData_2 = value;
		Il2CppCodeGenWriteBarrier((&____rasterData_2), value);
	}

	inline static int32_t get_offset_of__heightData_3() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____heightData_3)); }
	inline SingleU5BU5D_t1444911251* get__heightData_3() const { return ____heightData_3; }
	inline SingleU5BU5D_t1444911251** get_address_of__heightData_3() { return &____heightData_3; }
	inline void set__heightData_3(SingleU5BU5D_t1444911251* value)
	{
		____heightData_3 = value;
		Il2CppCodeGenWriteBarrier((&____heightData_3), value);
	}

	inline static int32_t get_offset_of__relativeScale_4() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____relativeScale_4)); }
	inline float get__relativeScale_4() const { return ____relativeScale_4; }
	inline float* get_address_of__relativeScale_4() { return &____relativeScale_4; }
	inline void set__relativeScale_4(float value)
	{
		____relativeScale_4 = value;
	}

	inline static int32_t get_offset_of__heightTexture_5() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____heightTexture_5)); }
	inline Texture2D_t3840446185 * get__heightTexture_5() const { return ____heightTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of__heightTexture_5() { return &____heightTexture_5; }
	inline void set__heightTexture_5(Texture2D_t3840446185 * value)
	{
		____heightTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&____heightTexture_5), value);
	}

	inline static int32_t get_offset_of__loadingTexture_6() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____loadingTexture_6)); }
	inline Texture2D_t3840446185 * get__loadingTexture_6() const { return ____loadingTexture_6; }
	inline Texture2D_t3840446185 ** get_address_of__loadingTexture_6() { return &____loadingTexture_6; }
	inline void set__loadingTexture_6(Texture2D_t3840446185 * value)
	{
		____loadingTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&____loadingTexture_6), value);
	}

	inline static int32_t get_offset_of__tiles_7() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____tiles_7)); }
	inline List_1_t2569976014 * get__tiles_7() const { return ____tiles_7; }
	inline List_1_t2569976014 ** get_address_of__tiles_7() { return &____tiles_7; }
	inline void set__tiles_7(List_1_t2569976014 * value)
	{
		____tiles_7 = value;
		Il2CppCodeGenWriteBarrier((&____tiles_7), value);
	}

	inline static int32_t get_offset_of__meshRenderer_8() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____meshRenderer_8)); }
	inline MeshRenderer_t587009260 * get__meshRenderer_8() const { return ____meshRenderer_8; }
	inline MeshRenderer_t587009260 ** get_address_of__meshRenderer_8() { return &____meshRenderer_8; }
	inline void set__meshRenderer_8(MeshRenderer_t587009260 * value)
	{
		____meshRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&____meshRenderer_8), value);
	}

	inline static int32_t get_offset_of__meshFilter_9() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____meshFilter_9)); }
	inline MeshFilter_t3523625662 * get__meshFilter_9() const { return ____meshFilter_9; }
	inline MeshFilter_t3523625662 ** get_address_of__meshFilter_9() { return &____meshFilter_9; }
	inline void set__meshFilter_9(MeshFilter_t3523625662 * value)
	{
		____meshFilter_9 = value;
		Il2CppCodeGenWriteBarrier((&____meshFilter_9), value);
	}

	inline static int32_t get_offset_of__collider_10() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____collider_10)); }
	inline Collider_t1773347010 * get__collider_10() const { return ____collider_10; }
	inline Collider_t1773347010 ** get_address_of__collider_10() { return &____collider_10; }
	inline void set__collider_10(Collider_t1773347010 * value)
	{
		____collider_10 = value;
		Il2CppCodeGenWriteBarrier((&____collider_10), value);
	}

	inline static int32_t get_offset_of__vectorData_11() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____vectorData_11)); }
	inline String_t* get__vectorData_11() const { return ____vectorData_11; }
	inline String_t** get_address_of__vectorData_11() { return &____vectorData_11; }
	inline void set__vectorData_11(String_t* value)
	{
		____vectorData_11 = value;
		Il2CppCodeGenWriteBarrier((&____vectorData_11), value);
	}

	inline static int32_t get_offset_of__rect_12() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____rect_12)); }
	inline RectD_t151583371  get__rect_12() const { return ____rect_12; }
	inline RectD_t151583371 * get_address_of__rect_12() { return &____rect_12; }
	inline void set__rect_12(RectD_t151583371  value)
	{
		____rect_12 = value;
	}

	inline static int32_t get_offset_of__unwrappedTileId_13() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____unwrappedTileId_13)); }
	inline UnwrappedTileId_t2586853537  get__unwrappedTileId_13() const { return ____unwrappedTileId_13; }
	inline UnwrappedTileId_t2586853537 * get_address_of__unwrappedTileId_13() { return &____unwrappedTileId_13; }
	inline void set__unwrappedTileId_13(UnwrappedTileId_t2586853537  value)
	{
		____unwrappedTileId_13 = value;
	}

	inline static int32_t get_offset_of__canonicalTileId_14() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ____canonicalTileId_14)); }
	inline CanonicalTileId_t4184902996  get__canonicalTileId_14() const { return ____canonicalTileId_14; }
	inline CanonicalTileId_t4184902996 * get_address_of__canonicalTileId_14() { return &____canonicalTileId_14; }
	inline void set__canonicalTileId_14(CanonicalTileId_t4184902996  value)
	{
		____canonicalTileId_14 = value;
	}

	inline static int32_t get_offset_of_U3CTileScaleU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ___U3CTileScaleU3Ek__BackingField_15)); }
	inline float get_U3CTileScaleU3Ek__BackingField_15() const { return ___U3CTileScaleU3Ek__BackingField_15; }
	inline float* get_address_of_U3CTileScaleU3Ek__BackingField_15() { return &___U3CTileScaleU3Ek__BackingField_15; }
	inline void set_U3CTileScaleU3Ek__BackingField_15(float value)
	{
		___U3CTileScaleU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_RasterDataState_16() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ___RasterDataState_16)); }
	inline int32_t get_RasterDataState_16() const { return ___RasterDataState_16; }
	inline int32_t* get_address_of_RasterDataState_16() { return &___RasterDataState_16; }
	inline void set_RasterDataState_16(int32_t value)
	{
		___RasterDataState_16 = value;
	}

	inline static int32_t get_offset_of_HeightDataState_17() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ___HeightDataState_17)); }
	inline int32_t get_HeightDataState_17() const { return ___HeightDataState_17; }
	inline int32_t* get_address_of_HeightDataState_17() { return &___HeightDataState_17; }
	inline void set_HeightDataState_17(int32_t value)
	{
		___HeightDataState_17 = value;
	}

	inline static int32_t get_offset_of_VectorDataState_18() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ___VectorDataState_18)); }
	inline int32_t get_VectorDataState_18() const { return ___VectorDataState_18; }
	inline int32_t* get_address_of_VectorDataState_18() { return &___VectorDataState_18; }
	inline void set_VectorDataState_18(int32_t value)
	{
		___VectorDataState_18 = value;
	}

	inline static int32_t get_offset_of_OnHeightDataChanged_19() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ___OnHeightDataChanged_19)); }
	inline Action_1_t2577553440 * get_OnHeightDataChanged_19() const { return ___OnHeightDataChanged_19; }
	inline Action_1_t2577553440 ** get_address_of_OnHeightDataChanged_19() { return &___OnHeightDataChanged_19; }
	inline void set_OnHeightDataChanged_19(Action_1_t2577553440 * value)
	{
		___OnHeightDataChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeightDataChanged_19), value);
	}

	inline static int32_t get_offset_of_OnRasterDataChanged_20() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ___OnRasterDataChanged_20)); }
	inline Action_1_t2577553440 * get_OnRasterDataChanged_20() const { return ___OnRasterDataChanged_20; }
	inline Action_1_t2577553440 ** get_address_of_OnRasterDataChanged_20() { return &___OnRasterDataChanged_20; }
	inline void set_OnRasterDataChanged_20(Action_1_t2577553440 * value)
	{
		___OnRasterDataChanged_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnRasterDataChanged_20), value);
	}

	inline static int32_t get_offset_of_OnVectorDataChanged_21() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845, ___OnVectorDataChanged_21)); }
	inline Action_1_t2577553440 * get_OnVectorDataChanged_21() const { return ___OnVectorDataChanged_21; }
	inline Action_1_t2577553440 ** get_address_of_OnVectorDataChanged_21() { return &___OnVectorDataChanged_21; }
	inline void set_OnVectorDataChanged_21(Action_1_t2577553440 * value)
	{
		___OnVectorDataChanged_21 = value;
		Il2CppCodeGenWriteBarrier((&___OnVectorDataChanged_21), value);
	}
};

struct UnityTile_t2405085845_StaticFields
{
public:
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::<>f__am$cache0
	Action_1_t2577553440 * ___U3CU3Ef__amU24cache0_22;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::<>f__am$cache1
	Action_1_t2577553440 * ___U3CU3Ef__amU24cache1_23;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::<>f__am$cache2
	Action_1_t2577553440 * ___U3CU3Ef__amU24cache2_24;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::<>f__am$cache3
	Action_1_t2577553440 * ___U3CU3Ef__amU24cache3_25;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::<>f__am$cache4
	Action_1_t2577553440 * ___U3CU3Ef__amU24cache4_26;
	// System.Action`1<Mapbox.Unity.MeshGeneration.Data.UnityTile> Mapbox.Unity.MeshGeneration.Data.UnityTile::<>f__am$cache5
	Action_1_t2577553440 * ___U3CU3Ef__amU24cache5_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Action_1_t2577553440 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Action_1_t2577553440 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Action_1_t2577553440 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_23() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845_StaticFields, ___U3CU3Ef__amU24cache1_23)); }
	inline Action_1_t2577553440 * get_U3CU3Ef__amU24cache1_23() const { return ___U3CU3Ef__amU24cache1_23; }
	inline Action_1_t2577553440 ** get_address_of_U3CU3Ef__amU24cache1_23() { return &___U3CU3Ef__amU24cache1_23; }
	inline void set_U3CU3Ef__amU24cache1_23(Action_1_t2577553440 * value)
	{
		___U3CU3Ef__amU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_24() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845_StaticFields, ___U3CU3Ef__amU24cache2_24)); }
	inline Action_1_t2577553440 * get_U3CU3Ef__amU24cache2_24() const { return ___U3CU3Ef__amU24cache2_24; }
	inline Action_1_t2577553440 ** get_address_of_U3CU3Ef__amU24cache2_24() { return &___U3CU3Ef__amU24cache2_24; }
	inline void set_U3CU3Ef__amU24cache2_24(Action_1_t2577553440 * value)
	{
		___U3CU3Ef__amU24cache2_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_25() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845_StaticFields, ___U3CU3Ef__amU24cache3_25)); }
	inline Action_1_t2577553440 * get_U3CU3Ef__amU24cache3_25() const { return ___U3CU3Ef__amU24cache3_25; }
	inline Action_1_t2577553440 ** get_address_of_U3CU3Ef__amU24cache3_25() { return &___U3CU3Ef__amU24cache3_25; }
	inline void set_U3CU3Ef__amU24cache3_25(Action_1_t2577553440 * value)
	{
		___U3CU3Ef__amU24cache3_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_26() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845_StaticFields, ___U3CU3Ef__amU24cache4_26)); }
	inline Action_1_t2577553440 * get_U3CU3Ef__amU24cache4_26() const { return ___U3CU3Ef__amU24cache4_26; }
	inline Action_1_t2577553440 ** get_address_of_U3CU3Ef__amU24cache4_26() { return &___U3CU3Ef__amU24cache4_26; }
	inline void set_U3CU3Ef__amU24cache4_26(Action_1_t2577553440 * value)
	{
		___U3CU3Ef__amU24cache4_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_27() { return static_cast<int32_t>(offsetof(UnityTile_t2405085845_StaticFields, ___U3CU3Ef__amU24cache5_27)); }
	inline Action_1_t2577553440 * get_U3CU3Ef__amU24cache5_27() const { return ___U3CU3Ef__amU24cache5_27; }
	inline Action_1_t2577553440 ** get_address_of_U3CU3Ef__amU24cache5_27() { return &___U3CU3Ef__amU24cache5_27; }
	inline void set_U3CU3Ef__amU24cache5_27(Action_1_t2577553440 * value)
	{
		___U3CU3Ef__amU24cache5_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTILE_T2405085845_H
#ifndef DIRECTIONSFACTORY_T1089493808_H
#define DIRECTIONSFACTORY_T1089493808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Factories.DirectionsFactory
struct  DirectionsFactory_t1089493808  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Map.AbstractMap Mapbox.Unity.MeshGeneration.Factories.DirectionsFactory::_map
	AbstractMap_t3082917158 * ____map_2;
	// Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier[] Mapbox.Unity.MeshGeneration.Factories.DirectionsFactory::MeshModifiers
	MeshModifierU5BU5D_t115465745* ___MeshModifiers_3;
	// UnityEngine.Transform[] Mapbox.Unity.MeshGeneration.Factories.DirectionsFactory::_waypoints
	TransformU5BU5D_t807237628* ____waypoints_4;
	// UnityEngine.Material Mapbox.Unity.MeshGeneration.Factories.DirectionsFactory::_material
	Material_t340375123 * ____material_5;
	// Mapbox.Directions.Directions Mapbox.Unity.MeshGeneration.Factories.DirectionsFactory::_directions
	Directions_t1397515081 * ____directions_6;

public:
	inline static int32_t get_offset_of__map_2() { return static_cast<int32_t>(offsetof(DirectionsFactory_t1089493808, ____map_2)); }
	inline AbstractMap_t3082917158 * get__map_2() const { return ____map_2; }
	inline AbstractMap_t3082917158 ** get_address_of__map_2() { return &____map_2; }
	inline void set__map_2(AbstractMap_t3082917158 * value)
	{
		____map_2 = value;
		Il2CppCodeGenWriteBarrier((&____map_2), value);
	}

	inline static int32_t get_offset_of_MeshModifiers_3() { return static_cast<int32_t>(offsetof(DirectionsFactory_t1089493808, ___MeshModifiers_3)); }
	inline MeshModifierU5BU5D_t115465745* get_MeshModifiers_3() const { return ___MeshModifiers_3; }
	inline MeshModifierU5BU5D_t115465745** get_address_of_MeshModifiers_3() { return &___MeshModifiers_3; }
	inline void set_MeshModifiers_3(MeshModifierU5BU5D_t115465745* value)
	{
		___MeshModifiers_3 = value;
		Il2CppCodeGenWriteBarrier((&___MeshModifiers_3), value);
	}

	inline static int32_t get_offset_of__waypoints_4() { return static_cast<int32_t>(offsetof(DirectionsFactory_t1089493808, ____waypoints_4)); }
	inline TransformU5BU5D_t807237628* get__waypoints_4() const { return ____waypoints_4; }
	inline TransformU5BU5D_t807237628** get_address_of__waypoints_4() { return &____waypoints_4; }
	inline void set__waypoints_4(TransformU5BU5D_t807237628* value)
	{
		____waypoints_4 = value;
		Il2CppCodeGenWriteBarrier((&____waypoints_4), value);
	}

	inline static int32_t get_offset_of__material_5() { return static_cast<int32_t>(offsetof(DirectionsFactory_t1089493808, ____material_5)); }
	inline Material_t340375123 * get__material_5() const { return ____material_5; }
	inline Material_t340375123 ** get_address_of__material_5() { return &____material_5; }
	inline void set__material_5(Material_t340375123 * value)
	{
		____material_5 = value;
		Il2CppCodeGenWriteBarrier((&____material_5), value);
	}

	inline static int32_t get_offset_of__directions_6() { return static_cast<int32_t>(offsetof(DirectionsFactory_t1089493808, ____directions_6)); }
	inline Directions_t1397515081 * get__directions_6() const { return ____directions_6; }
	inline Directions_t1397515081 ** get_address_of__directions_6() { return &____directions_6; }
	inline void set__directions_6(Directions_t1397515081 * value)
	{
		____directions_6 = value;
		Il2CppCodeGenWriteBarrier((&____directions_6), value);
	}
};

struct DirectionsFactory_t1089493808_StaticFields
{
public:
	// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier,System.Boolean> Mapbox.Unity.MeshGeneration.Factories.DirectionsFactory::<>f__am$cache0
	Func_2_t1030805891 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(DirectionsFactory_t1089493808_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t1030805891 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t1030805891 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t1030805891 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONSFACTORY_T1089493808_H
#ifndef COLLIDERMODIFIER_T3767354032_H
#define COLLIDERMODIFIER_T3767354032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ColliderModifier
struct  ColliderModifier_t3767354032  : public GameObjectModifier_t609190006
{
public:
	// Mapbox.Unity.MeshGeneration.Modifiers.ColliderModifier/ColliderType Mapbox.Unity.MeshGeneration.Modifiers.ColliderModifier::_colliderType
	int32_t ____colliderType_3;

public:
	inline static int32_t get_offset_of__colliderType_3() { return static_cast<int32_t>(offsetof(ColliderModifier_t3767354032, ____colliderType_3)); }
	inline int32_t get__colliderType_3() const { return ____colliderType_3; }
	inline int32_t* get_address_of__colliderType_3() { return &____colliderType_3; }
	inline void set__colliderType_3(int32_t value)
	{
		____colliderType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERMODIFIER_T3767354032_H
#ifndef ADDMONOBEHAVIOURSMODIFIER_T4170257881_H
#define ADDMONOBEHAVIOURSMODIFIER_T4170257881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.AddMonoBehavioursModifier
struct  AddMonoBehavioursModifier_t4170257881  : public GameObjectModifier_t609190006
{
public:
	// Mapbox.Unity.MeshGeneration.Modifiers.AddMonoBehavioursModifierType[] Mapbox.Unity.MeshGeneration.Modifiers.AddMonoBehavioursModifier::_types
	AddMonoBehavioursModifierTypeU5BU5D_t228035385* ____types_3;

public:
	inline static int32_t get_offset_of__types_3() { return static_cast<int32_t>(offsetof(AddMonoBehavioursModifier_t4170257881, ____types_3)); }
	inline AddMonoBehavioursModifierTypeU5BU5D_t228035385* get__types_3() const { return ____types_3; }
	inline AddMonoBehavioursModifierTypeU5BU5D_t228035385** get_address_of__types_3() { return &____types_3; }
	inline void set__types_3(AddMonoBehavioursModifierTypeU5BU5D_t228035385* value)
	{
		____types_3 = value;
		Il2CppCodeGenWriteBarrier((&____types_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDMONOBEHAVIOURSMODIFIER_T4170257881_H
#ifndef ABSTRACTTILEPROVIDER_T1076087356_H
#define ABSTRACTTILEPROVIDER_T1076087356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.AbstractTileProvider
struct  AbstractTileProvider_t1076087356  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<Mapbox.Map.UnwrappedTileId> Mapbox.Unity.Map.AbstractTileProvider::OnTileAdded
	Action_1_t2759321132 * ___OnTileAdded_2;
	// System.Action`1<Mapbox.Map.UnwrappedTileId> Mapbox.Unity.Map.AbstractTileProvider::OnTileRemoved
	Action_1_t2759321132 * ___OnTileRemoved_3;
	// Mapbox.Unity.Map.IMap Mapbox.Unity.Map.AbstractTileProvider::_map
	RuntimeObject* ____map_4;
	// System.Collections.Generic.Dictionary`2<Mapbox.Map.UnwrappedTileId,System.Byte> Mapbox.Unity.Map.AbstractTileProvider::_activeTiles
	Dictionary_2_t332390595 * ____activeTiles_5;

public:
	inline static int32_t get_offset_of_OnTileAdded_2() { return static_cast<int32_t>(offsetof(AbstractTileProvider_t1076087356, ___OnTileAdded_2)); }
	inline Action_1_t2759321132 * get_OnTileAdded_2() const { return ___OnTileAdded_2; }
	inline Action_1_t2759321132 ** get_address_of_OnTileAdded_2() { return &___OnTileAdded_2; }
	inline void set_OnTileAdded_2(Action_1_t2759321132 * value)
	{
		___OnTileAdded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnTileAdded_2), value);
	}

	inline static int32_t get_offset_of_OnTileRemoved_3() { return static_cast<int32_t>(offsetof(AbstractTileProvider_t1076087356, ___OnTileRemoved_3)); }
	inline Action_1_t2759321132 * get_OnTileRemoved_3() const { return ___OnTileRemoved_3; }
	inline Action_1_t2759321132 ** get_address_of_OnTileRemoved_3() { return &___OnTileRemoved_3; }
	inline void set_OnTileRemoved_3(Action_1_t2759321132 * value)
	{
		___OnTileRemoved_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnTileRemoved_3), value);
	}

	inline static int32_t get_offset_of__map_4() { return static_cast<int32_t>(offsetof(AbstractTileProvider_t1076087356, ____map_4)); }
	inline RuntimeObject* get__map_4() const { return ____map_4; }
	inline RuntimeObject** get_address_of__map_4() { return &____map_4; }
	inline void set__map_4(RuntimeObject* value)
	{
		____map_4 = value;
		Il2CppCodeGenWriteBarrier((&____map_4), value);
	}

	inline static int32_t get_offset_of__activeTiles_5() { return static_cast<int32_t>(offsetof(AbstractTileProvider_t1076087356, ____activeTiles_5)); }
	inline Dictionary_2_t332390595 * get__activeTiles_5() const { return ____activeTiles_5; }
	inline Dictionary_2_t332390595 ** get_address_of__activeTiles_5() { return &____activeTiles_5; }
	inline void set__activeTiles_5(Dictionary_2_t332390595 * value)
	{
		____activeTiles_5 = value;
		Il2CppCodeGenWriteBarrier((&____activeTiles_5), value);
	}
};

struct AbstractTileProvider_t1076087356_StaticFields
{
public:
	// System.Action`1<Mapbox.Map.UnwrappedTileId> Mapbox.Unity.Map.AbstractTileProvider::<>f__am$cache0
	Action_1_t2759321132 * ___U3CU3Ef__amU24cache0_6;
	// System.Action`1<Mapbox.Map.UnwrappedTileId> Mapbox.Unity.Map.AbstractTileProvider::<>f__am$cache1
	Action_1_t2759321132 * ___U3CU3Ef__amU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(AbstractTileProvider_t1076087356_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t2759321132 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t2759321132 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t2759321132 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(AbstractTileProvider_t1076087356_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Action_1_t2759321132 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Action_1_t2759321132 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Action_1_t2759321132 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTILEPROVIDER_T1076087356_H
#ifndef PREFABMODIFIER_T985809945_H
#define PREFABMODIFIER_T985809945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.PrefabModifier
struct  PrefabModifier_t985809945  : public GameObjectModifier_t609190006
{
public:
	// UnityEngine.GameObject Mapbox.Unity.MeshGeneration.Modifiers.PrefabModifier::_prefab
	GameObject_t1113636619 * ____prefab_3;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.PrefabModifier::_scaleDownWithWorld
	bool ____scaleDownWithWorld_4;

public:
	inline static int32_t get_offset_of__prefab_3() { return static_cast<int32_t>(offsetof(PrefabModifier_t985809945, ____prefab_3)); }
	inline GameObject_t1113636619 * get__prefab_3() const { return ____prefab_3; }
	inline GameObject_t1113636619 ** get_address_of__prefab_3() { return &____prefab_3; }
	inline void set__prefab_3(GameObject_t1113636619 * value)
	{
		____prefab_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_3), value);
	}

	inline static int32_t get_offset_of__scaleDownWithWorld_4() { return static_cast<int32_t>(offsetof(PrefabModifier_t985809945, ____scaleDownWithWorld_4)); }
	inline bool get__scaleDownWithWorld_4() const { return ____scaleDownWithWorld_4; }
	inline bool* get_address_of__scaleDownWithWorld_4() { return &____scaleDownWithWorld_4; }
	inline void set__scaleDownWithWorld_4(bool value)
	{
		____scaleDownWithWorld_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABMODIFIER_T985809945_H
#ifndef LAYERMODIFIER_T1135889872_H
#define LAYERMODIFIER_T1135889872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.LayerModifier
struct  LayerModifier_t1135889872  : public GameObjectModifier_t609190006
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.LayerModifier::_layerId
	int32_t ____layerId_3;

public:
	inline static int32_t get_offset_of__layerId_3() { return static_cast<int32_t>(offsetof(LayerModifier_t1135889872, ____layerId_3)); }
	inline int32_t get__layerId_3() const { return ____layerId_3; }
	inline int32_t* get_address_of__layerId_3() { return &____layerId_3; }
	inline void set__layerId_3(int32_t value)
	{
		____layerId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMODIFIER_T1135889872_H
#ifndef BASICMAP_T2129013278_H
#define BASICMAP_T2129013278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.BasicMap
struct  BasicMap_t2129013278  : public AbstractMap_t3082917158
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICMAP_T2129013278_H
#ifndef CAMERABOUNDSTILEPROVIDER_T706805987_H
#define CAMERABOUNDSTILEPROVIDER_T706805987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.CameraBoundsTileProvider
struct  CameraBoundsTileProvider_t706805987  : public AbstractTileProvider_t1076087356
{
public:
	// UnityEngine.Camera Mapbox.Unity.Map.CameraBoundsTileProvider::_camera
	Camera_t4157153871 * ____camera_8;
	// System.Int32 Mapbox.Unity.Map.CameraBoundsTileProvider::_visibleBuffer
	int32_t ____visibleBuffer_9;
	// System.Int32 Mapbox.Unity.Map.CameraBoundsTileProvider::_disposeBuffer
	int32_t ____disposeBuffer_10;
	// System.Single Mapbox.Unity.Map.CameraBoundsTileProvider::_updateInterval
	float ____updateInterval_11;
	// UnityEngine.Plane Mapbox.Unity.Map.CameraBoundsTileProvider::_groundPlane
	Plane_t1000493321  ____groundPlane_12;
	// UnityEngine.Ray Mapbox.Unity.Map.CameraBoundsTileProvider::_ray
	Ray_t3785851493  ____ray_13;
	// System.Single Mapbox.Unity.Map.CameraBoundsTileProvider::_hitDistance
	float ____hitDistance_14;
	// UnityEngine.Vector3 Mapbox.Unity.Map.CameraBoundsTileProvider::_viewportTarget
	Vector3_t3722313464  ____viewportTarget_15;
	// System.Single Mapbox.Unity.Map.CameraBoundsTileProvider::_elapsedTime
	float ____elapsedTime_16;
	// System.Boolean Mapbox.Unity.Map.CameraBoundsTileProvider::_shouldUpdate
	bool ____shouldUpdate_17;
	// Mapbox.Utils.Vector2d Mapbox.Unity.Map.CameraBoundsTileProvider::_currentLatitudeLongitude
	Vector2d_t1865246568  ____currentLatitudeLongitude_18;
	// Mapbox.Map.UnwrappedTileId Mapbox.Unity.Map.CameraBoundsTileProvider::_cachedTile
	UnwrappedTileId_t2586853537  ____cachedTile_19;
	// Mapbox.Map.UnwrappedTileId Mapbox.Unity.Map.CameraBoundsTileProvider::_currentTile
	UnwrappedTileId_t2586853537  ____currentTile_20;

public:
	inline static int32_t get_offset_of__camera_8() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____camera_8)); }
	inline Camera_t4157153871 * get__camera_8() const { return ____camera_8; }
	inline Camera_t4157153871 ** get_address_of__camera_8() { return &____camera_8; }
	inline void set__camera_8(Camera_t4157153871 * value)
	{
		____camera_8 = value;
		Il2CppCodeGenWriteBarrier((&____camera_8), value);
	}

	inline static int32_t get_offset_of__visibleBuffer_9() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____visibleBuffer_9)); }
	inline int32_t get__visibleBuffer_9() const { return ____visibleBuffer_9; }
	inline int32_t* get_address_of__visibleBuffer_9() { return &____visibleBuffer_9; }
	inline void set__visibleBuffer_9(int32_t value)
	{
		____visibleBuffer_9 = value;
	}

	inline static int32_t get_offset_of__disposeBuffer_10() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____disposeBuffer_10)); }
	inline int32_t get__disposeBuffer_10() const { return ____disposeBuffer_10; }
	inline int32_t* get_address_of__disposeBuffer_10() { return &____disposeBuffer_10; }
	inline void set__disposeBuffer_10(int32_t value)
	{
		____disposeBuffer_10 = value;
	}

	inline static int32_t get_offset_of__updateInterval_11() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____updateInterval_11)); }
	inline float get__updateInterval_11() const { return ____updateInterval_11; }
	inline float* get_address_of__updateInterval_11() { return &____updateInterval_11; }
	inline void set__updateInterval_11(float value)
	{
		____updateInterval_11 = value;
	}

	inline static int32_t get_offset_of__groundPlane_12() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____groundPlane_12)); }
	inline Plane_t1000493321  get__groundPlane_12() const { return ____groundPlane_12; }
	inline Plane_t1000493321 * get_address_of__groundPlane_12() { return &____groundPlane_12; }
	inline void set__groundPlane_12(Plane_t1000493321  value)
	{
		____groundPlane_12 = value;
	}

	inline static int32_t get_offset_of__ray_13() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____ray_13)); }
	inline Ray_t3785851493  get__ray_13() const { return ____ray_13; }
	inline Ray_t3785851493 * get_address_of__ray_13() { return &____ray_13; }
	inline void set__ray_13(Ray_t3785851493  value)
	{
		____ray_13 = value;
	}

	inline static int32_t get_offset_of__hitDistance_14() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____hitDistance_14)); }
	inline float get__hitDistance_14() const { return ____hitDistance_14; }
	inline float* get_address_of__hitDistance_14() { return &____hitDistance_14; }
	inline void set__hitDistance_14(float value)
	{
		____hitDistance_14 = value;
	}

	inline static int32_t get_offset_of__viewportTarget_15() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____viewportTarget_15)); }
	inline Vector3_t3722313464  get__viewportTarget_15() const { return ____viewportTarget_15; }
	inline Vector3_t3722313464 * get_address_of__viewportTarget_15() { return &____viewportTarget_15; }
	inline void set__viewportTarget_15(Vector3_t3722313464  value)
	{
		____viewportTarget_15 = value;
	}

	inline static int32_t get_offset_of__elapsedTime_16() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____elapsedTime_16)); }
	inline float get__elapsedTime_16() const { return ____elapsedTime_16; }
	inline float* get_address_of__elapsedTime_16() { return &____elapsedTime_16; }
	inline void set__elapsedTime_16(float value)
	{
		____elapsedTime_16 = value;
	}

	inline static int32_t get_offset_of__shouldUpdate_17() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____shouldUpdate_17)); }
	inline bool get__shouldUpdate_17() const { return ____shouldUpdate_17; }
	inline bool* get_address_of__shouldUpdate_17() { return &____shouldUpdate_17; }
	inline void set__shouldUpdate_17(bool value)
	{
		____shouldUpdate_17 = value;
	}

	inline static int32_t get_offset_of__currentLatitudeLongitude_18() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____currentLatitudeLongitude_18)); }
	inline Vector2d_t1865246568  get__currentLatitudeLongitude_18() const { return ____currentLatitudeLongitude_18; }
	inline Vector2d_t1865246568 * get_address_of__currentLatitudeLongitude_18() { return &____currentLatitudeLongitude_18; }
	inline void set__currentLatitudeLongitude_18(Vector2d_t1865246568  value)
	{
		____currentLatitudeLongitude_18 = value;
	}

	inline static int32_t get_offset_of__cachedTile_19() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____cachedTile_19)); }
	inline UnwrappedTileId_t2586853537  get__cachedTile_19() const { return ____cachedTile_19; }
	inline UnwrappedTileId_t2586853537 * get_address_of__cachedTile_19() { return &____cachedTile_19; }
	inline void set__cachedTile_19(UnwrappedTileId_t2586853537  value)
	{
		____cachedTile_19 = value;
	}

	inline static int32_t get_offset_of__currentTile_20() { return static_cast<int32_t>(offsetof(CameraBoundsTileProvider_t706805987, ____currentTile_20)); }
	inline UnwrappedTileId_t2586853537  get__currentTile_20() const { return ____currentTile_20; }
	inline UnwrappedTileId_t2586853537 * get_address_of__currentTile_20() { return &____currentTile_20; }
	inline void set__currentTile_20(UnwrappedTileId_t2586853537  value)
	{
		____currentTile_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERABOUNDSTILEPROVIDER_T706805987_H
#ifndef DYNAMICZOOMTILEPROVIDER_T2524886064_H
#define DYNAMICZOOMTILEPROVIDER_T2524886064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.DynamicZoomTileProvider
struct  DynamicZoomTileProvider_t2524886064  : public AbstractTileProvider_t1076087356
{
public:
	// UnityEngine.Camera Mapbox.Unity.Map.DynamicZoomTileProvider::_referenceCamera
	Camera_t4157153871 * ____referenceCamera_8;
	// System.Single Mapbox.Unity.Map.DynamicZoomTileProvider::_previousY
	float ____previousY_9;
	// Mapbox.Utils.Vector2d Mapbox.Unity.Map.DynamicZoomTileProvider::_previousWebMercCenter
	Vector2d_t1865246568  ____previousWebMercCenter_10;
	// Mapbox.Utils.Vector2dBounds Mapbox.Unity.Map.DynamicZoomTileProvider::_viewPortWebMercBounds
	Vector2dBounds_t1974840945  ____viewPortWebMercBounds_11;
	// System.Int32 Mapbox.Unity.Map.DynamicZoomTileProvider::_cameraZoomingRangeMinY
	int32_t ____cameraZoomingRangeMinY_12;
	// System.Int32 Mapbox.Unity.Map.DynamicZoomTileProvider::_cameraZoomingRangeMaxY
	int32_t ____cameraZoomingRangeMaxY_13;
	// UnityEngine.Plane Mapbox.Unity.Map.DynamicZoomTileProvider::_groundPlane
	Plane_t1000493321  ____groundPlane_14;
	// Mapbox.Unity.Map.DynamicZoomMap Mapbox.Unity.Map.DynamicZoomTileProvider::_dynamicZoomMap
	DynamicZoomMap_t2517981420 * ____dynamicZoomMap_15;
	// System.String Mapbox.Unity.Map.DynamicZoomTileProvider::_className
	String_t* ____className_16;

public:
	inline static int32_t get_offset_of__referenceCamera_8() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____referenceCamera_8)); }
	inline Camera_t4157153871 * get__referenceCamera_8() const { return ____referenceCamera_8; }
	inline Camera_t4157153871 ** get_address_of__referenceCamera_8() { return &____referenceCamera_8; }
	inline void set__referenceCamera_8(Camera_t4157153871 * value)
	{
		____referenceCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&____referenceCamera_8), value);
	}

	inline static int32_t get_offset_of__previousY_9() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____previousY_9)); }
	inline float get__previousY_9() const { return ____previousY_9; }
	inline float* get_address_of__previousY_9() { return &____previousY_9; }
	inline void set__previousY_9(float value)
	{
		____previousY_9 = value;
	}

	inline static int32_t get_offset_of__previousWebMercCenter_10() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____previousWebMercCenter_10)); }
	inline Vector2d_t1865246568  get__previousWebMercCenter_10() const { return ____previousWebMercCenter_10; }
	inline Vector2d_t1865246568 * get_address_of__previousWebMercCenter_10() { return &____previousWebMercCenter_10; }
	inline void set__previousWebMercCenter_10(Vector2d_t1865246568  value)
	{
		____previousWebMercCenter_10 = value;
	}

	inline static int32_t get_offset_of__viewPortWebMercBounds_11() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____viewPortWebMercBounds_11)); }
	inline Vector2dBounds_t1974840945  get__viewPortWebMercBounds_11() const { return ____viewPortWebMercBounds_11; }
	inline Vector2dBounds_t1974840945 * get_address_of__viewPortWebMercBounds_11() { return &____viewPortWebMercBounds_11; }
	inline void set__viewPortWebMercBounds_11(Vector2dBounds_t1974840945  value)
	{
		____viewPortWebMercBounds_11 = value;
	}

	inline static int32_t get_offset_of__cameraZoomingRangeMinY_12() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____cameraZoomingRangeMinY_12)); }
	inline int32_t get__cameraZoomingRangeMinY_12() const { return ____cameraZoomingRangeMinY_12; }
	inline int32_t* get_address_of__cameraZoomingRangeMinY_12() { return &____cameraZoomingRangeMinY_12; }
	inline void set__cameraZoomingRangeMinY_12(int32_t value)
	{
		____cameraZoomingRangeMinY_12 = value;
	}

	inline static int32_t get_offset_of__cameraZoomingRangeMaxY_13() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____cameraZoomingRangeMaxY_13)); }
	inline int32_t get__cameraZoomingRangeMaxY_13() const { return ____cameraZoomingRangeMaxY_13; }
	inline int32_t* get_address_of__cameraZoomingRangeMaxY_13() { return &____cameraZoomingRangeMaxY_13; }
	inline void set__cameraZoomingRangeMaxY_13(int32_t value)
	{
		____cameraZoomingRangeMaxY_13 = value;
	}

	inline static int32_t get_offset_of__groundPlane_14() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____groundPlane_14)); }
	inline Plane_t1000493321  get__groundPlane_14() const { return ____groundPlane_14; }
	inline Plane_t1000493321 * get_address_of__groundPlane_14() { return &____groundPlane_14; }
	inline void set__groundPlane_14(Plane_t1000493321  value)
	{
		____groundPlane_14 = value;
	}

	inline static int32_t get_offset_of__dynamicZoomMap_15() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____dynamicZoomMap_15)); }
	inline DynamicZoomMap_t2517981420 * get__dynamicZoomMap_15() const { return ____dynamicZoomMap_15; }
	inline DynamicZoomMap_t2517981420 ** get_address_of__dynamicZoomMap_15() { return &____dynamicZoomMap_15; }
	inline void set__dynamicZoomMap_15(DynamicZoomMap_t2517981420 * value)
	{
		____dynamicZoomMap_15 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicZoomMap_15), value);
	}

	inline static int32_t get_offset_of__className_16() { return static_cast<int32_t>(offsetof(DynamicZoomTileProvider_t2524886064, ____className_16)); }
	inline String_t* get__className_16() const { return ____className_16; }
	inline String_t** get_address_of__className_16() { return &____className_16; }
	inline void set__className_16(String_t* value)
	{
		____className_16 = value;
		Il2CppCodeGenWriteBarrier((&____className_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICZOOMTILEPROVIDER_T2524886064_H
#ifndef RANGETILEPROVIDER_T2484217965_H
#define RANGETILEPROVIDER_T2484217965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.RangeTileProvider
struct  RangeTileProvider_t2484217965  : public AbstractTileProvider_t1076087356
{
public:
	// System.Int32 Mapbox.Unity.Map.RangeTileProvider::_west
	int32_t ____west_8;
	// System.Int32 Mapbox.Unity.Map.RangeTileProvider::_north
	int32_t ____north_9;
	// System.Int32 Mapbox.Unity.Map.RangeTileProvider::_east
	int32_t ____east_10;
	// System.Int32 Mapbox.Unity.Map.RangeTileProvider::_south
	int32_t ____south_11;

public:
	inline static int32_t get_offset_of__west_8() { return static_cast<int32_t>(offsetof(RangeTileProvider_t2484217965, ____west_8)); }
	inline int32_t get__west_8() const { return ____west_8; }
	inline int32_t* get_address_of__west_8() { return &____west_8; }
	inline void set__west_8(int32_t value)
	{
		____west_8 = value;
	}

	inline static int32_t get_offset_of__north_9() { return static_cast<int32_t>(offsetof(RangeTileProvider_t2484217965, ____north_9)); }
	inline int32_t get__north_9() const { return ____north_9; }
	inline int32_t* get_address_of__north_9() { return &____north_9; }
	inline void set__north_9(int32_t value)
	{
		____north_9 = value;
	}

	inline static int32_t get_offset_of__east_10() { return static_cast<int32_t>(offsetof(RangeTileProvider_t2484217965, ____east_10)); }
	inline int32_t get__east_10() const { return ____east_10; }
	inline int32_t* get_address_of__east_10() { return &____east_10; }
	inline void set__east_10(int32_t value)
	{
		____east_10 = value;
	}

	inline static int32_t get_offset_of__south_11() { return static_cast<int32_t>(offsetof(RangeTileProvider_t2484217965, ____south_11)); }
	inline int32_t get__south_11() const { return ____south_11; }
	inline int32_t* get_address_of__south_11() { return &____south_11; }
	inline void set__south_11(int32_t value)
	{
		____south_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGETILEPROVIDER_T2484217965_H
#ifndef RANGEAROUNDTRANSFORMTILEPROVIDER_T2204850524_H
#define RANGEAROUNDTRANSFORMTILEPROVIDER_T2204850524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.RangeAroundTransformTileProvider
struct  RangeAroundTransformTileProvider_t2204850524  : public AbstractTileProvider_t1076087356
{
public:
	// UnityEngine.Transform Mapbox.Unity.Map.RangeAroundTransformTileProvider::_targetTransform
	Transform_t3600365921 * ____targetTransform_8;
	// System.Int32 Mapbox.Unity.Map.RangeAroundTransformTileProvider::_visibleBuffer
	int32_t ____visibleBuffer_9;
	// System.Int32 Mapbox.Unity.Map.RangeAroundTransformTileProvider::_disposeBuffer
	int32_t ____disposeBuffer_10;
	// System.Boolean Mapbox.Unity.Map.RangeAroundTransformTileProvider::_initialized
	bool ____initialized_11;
	// Mapbox.Map.UnwrappedTileId Mapbox.Unity.Map.RangeAroundTransformTileProvider::_currentTile
	UnwrappedTileId_t2586853537  ____currentTile_12;
	// Mapbox.Map.UnwrappedTileId Mapbox.Unity.Map.RangeAroundTransformTileProvider::_cachedTile
	UnwrappedTileId_t2586853537  ____cachedTile_13;

public:
	inline static int32_t get_offset_of__targetTransform_8() { return static_cast<int32_t>(offsetof(RangeAroundTransformTileProvider_t2204850524, ____targetTransform_8)); }
	inline Transform_t3600365921 * get__targetTransform_8() const { return ____targetTransform_8; }
	inline Transform_t3600365921 ** get_address_of__targetTransform_8() { return &____targetTransform_8; }
	inline void set__targetTransform_8(Transform_t3600365921 * value)
	{
		____targetTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&____targetTransform_8), value);
	}

	inline static int32_t get_offset_of__visibleBuffer_9() { return static_cast<int32_t>(offsetof(RangeAroundTransformTileProvider_t2204850524, ____visibleBuffer_9)); }
	inline int32_t get__visibleBuffer_9() const { return ____visibleBuffer_9; }
	inline int32_t* get_address_of__visibleBuffer_9() { return &____visibleBuffer_9; }
	inline void set__visibleBuffer_9(int32_t value)
	{
		____visibleBuffer_9 = value;
	}

	inline static int32_t get_offset_of__disposeBuffer_10() { return static_cast<int32_t>(offsetof(RangeAroundTransformTileProvider_t2204850524, ____disposeBuffer_10)); }
	inline int32_t get__disposeBuffer_10() const { return ____disposeBuffer_10; }
	inline int32_t* get_address_of__disposeBuffer_10() { return &____disposeBuffer_10; }
	inline void set__disposeBuffer_10(int32_t value)
	{
		____disposeBuffer_10 = value;
	}

	inline static int32_t get_offset_of__initialized_11() { return static_cast<int32_t>(offsetof(RangeAroundTransformTileProvider_t2204850524, ____initialized_11)); }
	inline bool get__initialized_11() const { return ____initialized_11; }
	inline bool* get_address_of__initialized_11() { return &____initialized_11; }
	inline void set__initialized_11(bool value)
	{
		____initialized_11 = value;
	}

	inline static int32_t get_offset_of__currentTile_12() { return static_cast<int32_t>(offsetof(RangeAroundTransformTileProvider_t2204850524, ____currentTile_12)); }
	inline UnwrappedTileId_t2586853537  get__currentTile_12() const { return ____currentTile_12; }
	inline UnwrappedTileId_t2586853537 * get_address_of__currentTile_12() { return &____currentTile_12; }
	inline void set__currentTile_12(UnwrappedTileId_t2586853537  value)
	{
		____currentTile_12 = value;
	}

	inline static int32_t get_offset_of__cachedTile_13() { return static_cast<int32_t>(offsetof(RangeAroundTransformTileProvider_t2204850524, ____cachedTile_13)); }
	inline UnwrappedTileId_t2586853537  get__cachedTile_13() const { return ____cachedTile_13; }
	inline UnwrappedTileId_t2586853537 * get_address_of__cachedTile_13() { return &____cachedTile_13; }
	inline void set__cachedTile_13(UnwrappedTileId_t2586853537  value)
	{
		____cachedTile_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEAROUNDTRANSFORMTILEPROVIDER_T2204850524_H
#ifndef MAPATSPECIFICLOCATION_T683025726_H
#define MAPATSPECIFICLOCATION_T683025726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.MapAtSpecificLocation
struct  MapAtSpecificLocation_t683025726  : public AbstractMap_t3082917158
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPATSPECIFICLOCATION_T683025726_H
#ifndef DYNAMICZOOMMAP_T2517981420_H
#define DYNAMICZOOMMAP_T2517981420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.DynamicZoomMap
struct  DynamicZoomMap_t2517981420  : public AbstractMap_t3082917158
{
public:
	// System.Int32 Mapbox.Unity.Map.DynamicZoomMap::MinZoom
	int32_t ___MinZoom_17;
	// System.Int32 Mapbox.Unity.Map.DynamicZoomMap::MaxZoom
	int32_t ___MaxZoom_18;

public:
	inline static int32_t get_offset_of_MinZoom_17() { return static_cast<int32_t>(offsetof(DynamicZoomMap_t2517981420, ___MinZoom_17)); }
	inline int32_t get_MinZoom_17() const { return ___MinZoom_17; }
	inline int32_t* get_address_of_MinZoom_17() { return &___MinZoom_17; }
	inline void set_MinZoom_17(int32_t value)
	{
		___MinZoom_17 = value;
	}

	inline static int32_t get_offset_of_MaxZoom_18() { return static_cast<int32_t>(offsetof(DynamicZoomMap_t2517981420, ___MaxZoom_18)); }
	inline int32_t get_MaxZoom_18() const { return ___MaxZoom_18; }
	inline int32_t* get_address_of_MaxZoom_18() { return &___MaxZoom_18; }
	inline void set_MaxZoom_18(int32_t value)
	{
		___MaxZoom_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICZOOMMAP_T2517981420_H
#ifndef GLOBETILEPROVIDER_T1213554907_H
#define GLOBETILEPROVIDER_T1213554907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Map.GlobeTileProvider
struct  GlobeTileProvider_t1213554907  : public AbstractTileProvider_t1076087356
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBETILEPROVIDER_T1213554907_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (ChangeShadowDistance_t4294610605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[1] = 
{
	ChangeShadowDistance_t4294610605::get_offset_of_ShadowDistance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (DynamicZoomCameraMovement_t1805284077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3301[5] = 
{
	DynamicZoomCameraMovement_t1805284077::get_offset_of__zoomSpeed_2(),
	DynamicZoomCameraMovement_t1805284077::get_offset_of__referenceCamera_3(),
	DynamicZoomCameraMovement_t1805284077::get_offset_of__dynamicZoomTileProvider_4(),
	DynamicZoomCameraMovement_t1805284077::get_offset_of__dynamicZoomMap_5(),
	DynamicZoomCameraMovement_t1805284077::get_offset_of__origin_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (FeatureSelectionDetector_t508482069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[2] = 
{
	FeatureSelectionDetector_t508482069::get_offset_of__marker_2(),
	FeatureSelectionDetector_t508482069::get_offset_of__feature_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (FeatureUiMarker_t3847499068), -1, sizeof(FeatureUiMarker_t3847499068_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3303[6] = 
{
	FeatureUiMarker_t3847499068::get_offset_of__wrapperMarker_2(),
	FeatureUiMarker_t3847499068::get_offset_of__infoPanel_3(),
	FeatureUiMarker_t3847499068::get_offset_of__info_4(),
	FeatureUiMarker_t3847499068::get_offset_of__targetVerts_5(),
	FeatureUiMarker_t3847499068::get_offset_of__selectedFeature_6(),
	FeatureUiMarker_t3847499068_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (ForwardGeocodeUserInput_t2575136032), -1, sizeof(ForwardGeocodeUserInput_t2575136032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3304[7] = 
{
	ForwardGeocodeUserInput_t2575136032::get_offset_of__inputField_2(),
	ForwardGeocodeUserInput_t2575136032::get_offset_of__resource_3(),
	ForwardGeocodeUserInput_t2575136032::get_offset_of__coordinate_4(),
	ForwardGeocodeUserInput_t2575136032::get_offset_of__hasResponse_5(),
	ForwardGeocodeUserInput_t2575136032::get_offset_of_U3CResponseU3Ek__BackingField_6(),
	ForwardGeocodeUserInput_t2575136032::get_offset_of_OnGeocoderResponse_7(),
	ForwardGeocodeUserInput_t2575136032_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (HighlightFeature_t1851209614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3305[3] = 
{
	HighlightFeature_t1851209614::get_offset_of__original_2(),
	HighlightFeature_t1851209614::get_offset_of__highlight_3(),
	HighlightFeature_t1851209614::get_offset_of__materials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (LoadingPanelController_t2494933297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[2] = 
{
	LoadingPanelController_t2494933297::get_offset_of_MapVisualizer_2(),
	LoadingPanelController_t2494933297::get_offset_of_Content_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (MakiHelper_t3260814930), -1, sizeof(MakiHelper_t3260814930_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3307[3] = 
{
	MakiHelper_t3260814930_StaticFields::get_offset_of_Parent_2(),
	MakiHelper_t3260814930_StaticFields::get_offset_of_UiPrefab_3(),
	MakiHelper_t3260814930::get_offset_of__uiObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (ObjectInspectorModifier_t3950592485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[1] = 
{
	ObjectInspectorModifier_t3950592485::get_offset_of__marker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (PoiMarkerHelper_t575496201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[1] = 
{
	PoiMarkerHelper_t575496201::get_offset_of__props_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (PositionWithLocationProvider_t2078499108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3310[6] = 
{
	PositionWithLocationProvider_t2078499108::get_offset_of__map_2(),
	PositionWithLocationProvider_t2078499108::get_offset_of__positionFollowFactor_3(),
	PositionWithLocationProvider_t2078499108::get_offset_of__useTransformLocationProvider_4(),
	PositionWithLocationProvider_t2078499108::get_offset_of__isInitialized_5(),
	PositionWithLocationProvider_t2078499108::get_offset_of__locationProvider_6(),
	PositionWithLocationProvider_t2078499108::get_offset_of__targetPosition_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (ReverseGeocodeUserInput_t2632079094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3311[7] = 
{
	ReverseGeocodeUserInput_t2632079094::get_offset_of__inputField_2(),
	ReverseGeocodeUserInput_t2632079094::get_offset_of__resource_3(),
	ReverseGeocodeUserInput_t2632079094::get_offset_of__geocoder_4(),
	ReverseGeocodeUserInput_t2632079094::get_offset_of__coordinate_5(),
	ReverseGeocodeUserInput_t2632079094::get_offset_of__hasResponse_6(),
	ReverseGeocodeUserInput_t2632079094::get_offset_of_U3CResponseU3Ek__BackingField_7(),
	ReverseGeocodeUserInput_t2632079094::get_offset_of_OnGeocoderResponse_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (RotateWithLocationProvider_t2777253481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3312[5] = 
{
	RotateWithLocationProvider_t2777253481::get_offset_of__rotationFollowFactor_2(),
	RotateWithLocationProvider_t2777253481::get_offset_of__rotateZ_3(),
	RotateWithLocationProvider_t2777253481::get_offset_of__useTransformLocationProvider_4(),
	RotateWithLocationProvider_t2777253481::get_offset_of__locationProvider_5(),
	RotateWithLocationProvider_t2777253481::get_offset_of__targetPosition_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (TextureScale_t57896704), -1, sizeof(TextureScale_t57896704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3313[8] = 
{
	TextureScale_t57896704_StaticFields::get_offset_of_texColors_0(),
	TextureScale_t57896704_StaticFields::get_offset_of_newColors_1(),
	TextureScale_t57896704_StaticFields::get_offset_of_w_2(),
	TextureScale_t57896704_StaticFields::get_offset_of_ratioX_3(),
	TextureScale_t57896704_StaticFields::get_offset_of_ratioY_4(),
	TextureScale_t57896704_StaticFields::get_offset_of_w2_5(),
	TextureScale_t57896704_StaticFields::get_offset_of_finishCount_6(),
	TextureScale_t57896704_StaticFields::get_offset_of_mutex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (ThreadData_t1095464109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[2] = 
{
	ThreadData_t1095464109::get_offset_of_start_0(),
	ThreadData_t1095464109::get_offset_of_end_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (VoxelData_t2248882184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[2] = 
{
	VoxelData_t2248882184::get_offset_of_Position_0(),
	VoxelData_t2248882184::get_offset_of_Prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (VoxelFetcher_t3713644963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[1] = 
{
	VoxelFetcher_t3713644963::get_offset_of__voxels_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (VoxelColorMapper_t2180346717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[2] = 
{
	VoxelColorMapper_t2180346717::get_offset_of_Color_0(),
	VoxelColorMapper_t2180346717::get_offset_of_Voxel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (VoxelTile_t1944340880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[17] = 
{
	VoxelTile_t1944340880::get_offset_of__geocodeInput_2(),
	VoxelTile_t1944340880::get_offset_of__zoom_3(),
	VoxelTile_t1944340880::get_offset_of__elevationMultiplier_4(),
	VoxelTile_t1944340880::get_offset_of__voxelDepthPadding_5(),
	VoxelTile_t1944340880::get_offset_of__tileWidthInVoxels_6(),
	VoxelTile_t1944340880::get_offset_of__voxelFetcher_7(),
	VoxelTile_t1944340880::get_offset_of__camera_8(),
	VoxelTile_t1944340880::get_offset_of__voxelBatchCount_9(),
	VoxelTile_t1944340880::get_offset_of__styleUrl_10(),
	VoxelTile_t1944340880::get_offset_of__raster_11(),
	VoxelTile_t1944340880::get_offset_of__elevation_12(),
	VoxelTile_t1944340880::get_offset_of__rasterTexture_13(),
	VoxelTile_t1944340880::get_offset_of__elevationTexture_14(),
	VoxelTile_t1944340880::get_offset_of__fileSource_15(),
	VoxelTile_t1944340880::get_offset_of__voxels_16(),
	VoxelTile_t1944340880::get_offset_of__instantiatedVoxels_17(),
	VoxelTile_t1944340880::get_offset_of__tileScale_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (U3CBuildRoutineU3Ec__Iterator0_t1339467344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[6] = 
{
	U3CBuildRoutineU3Ec__Iterator0_t1339467344::get_offset_of_U3CdistanceOrderedVoxelsU3E__0_0(),
	U3CBuildRoutineU3Ec__Iterator0_t1339467344::get_offset_of_U3CiU3E__1_1(),
	U3CBuildRoutineU3Ec__Iterator0_t1339467344::get_offset_of_U24this_2(),
	U3CBuildRoutineU3Ec__Iterator0_t1339467344::get_offset_of_U24current_3(),
	U3CBuildRoutineU3Ec__Iterator0_t1339467344::get_offset_of_U24disposing_4(),
	U3CBuildRoutineU3Ec__Iterator0_t1339467344::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (Constants_t2712025141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (Path_t1402229194), -1, sizeof(Path_t1402229194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3321[6] = 
{
	0,
	0,
	0,
	0,
	Path_t1402229194_StaticFields::get_offset_of_MAPBOX_RESOURCES_RELATIVE_4(),
	Path_t1402229194_StaticFields::get_offset_of_MAPBOX_RESOURCES_ABSOLUTE_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (Math_t2875019722), -1, sizeof(Math_t2875019722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3322[4] = 
{
	Math_t2875019722_StaticFields::get_offset_of_Vector3Zero_0(),
	Math_t2875019722_StaticFields::get_offset_of_Vector3Up_1(),
	Math_t2875019722_StaticFields::get_offset_of_Vector3Down_2(),
	Math_t2875019722_StaticFields::get_offset_of_Vector3One_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (DeviceLocationProvider_t4095080695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[9] = 
{
	DeviceLocationProvider_t4095080695::get_offset_of__desiredAccuracyInMeters_2(),
	DeviceLocationProvider_t4095080695::get_offset_of__updateDistanceInMeters_3(),
	DeviceLocationProvider_t4095080695::get_offset_of__pollRoutine_4(),
	DeviceLocationProvider_t4095080695::get_offset_of__lastLocationTimestamp_5(),
	DeviceLocationProvider_t4095080695::get_offset_of__lastHeadingTimestamp_6(),
	DeviceLocationProvider_t4095080695::get_offset_of__wait_7(),
	DeviceLocationProvider_t4095080695::get_offset_of__location_8(),
	DeviceLocationProvider_t4095080695::get_offset_of_OnLocationUpdated_9(),
	DeviceLocationProvider_t4095080695::get_offset_of_OnHeadingUpdated_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (U3CPollLocationRoutineU3Ec__Iterator0_t2570282366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[6] = 
{
	U3CPollLocationRoutineU3Ec__Iterator0_t2570282366::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CPollLocationRoutineU3Ec__Iterator0_t2570282366::get_offset_of_U3CtimestampU3E__1_1(),
	U3CPollLocationRoutineU3Ec__Iterator0_t2570282366::get_offset_of_U24this_2(),
	U3CPollLocationRoutineU3Ec__Iterator0_t2570282366::get_offset_of_U24current_3(),
	U3CPollLocationRoutineU3Ec__Iterator0_t2570282366::get_offset_of_U24disposing_4(),
	U3CPollLocationRoutineU3Ec__Iterator0_t2570282366::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (EditorLocationProvider_t3344154418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[4] = 
{
	EditorLocationProvider_t3344154418::get_offset_of__latitudeLongitude_2(),
	EditorLocationProvider_t3344154418::get_offset_of__heading_3(),
	EditorLocationProvider_t3344154418::get_offset_of_OnHeadingUpdated_4(),
	EditorLocationProvider_t3344154418::get_offset_of_OnLocationUpdated_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (LocationUpdatedEventArgs_t3804910274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3327[1] = 
{
	LocationUpdatedEventArgs_t3804910274::get_offset_of_Location_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (HeadingUpdatedEventArgs_t743080610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3328[1] = 
{
	HeadingUpdatedEventArgs_t743080610::get_offset_of_Heading_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (LocationProviderFactory_t2702491050), -1, sizeof(LocationProviderFactory_t2702491050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3329[5] = 
{
	LocationProviderFactory_t2702491050::get_offset_of__deviceLocationProvider_2(),
	LocationProviderFactory_t2702491050::get_offset_of__editorLocationProvider_3(),
	LocationProviderFactory_t2702491050::get_offset_of__transformLocationProvider_4(),
	LocationProviderFactory_t2702491050_StaticFields::get_offset_of__instance_5(),
	LocationProviderFactory_t2702491050::get_offset_of__defaultLocationProvider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (TransformLocationProvider_t3083760227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[4] = 
{
	TransformLocationProvider_t3083760227::get_offset_of__map_2(),
	TransformLocationProvider_t3083760227::get_offset_of__targetTransform_3(),
	TransformLocationProvider_t3083760227::get_offset_of_OnHeadingUpdated_4(),
	TransformLocationProvider_t3083760227::get_offset_of_OnLocationUpdated_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (AbstractMap_t3082917158), -1, sizeof(AbstractMap_t3082917158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3331[15] = 
{
	AbstractMap_t3082917158::get_offset_of__initializeOnStart_2(),
	AbstractMap_t3082917158::get_offset_of__latitudeLongitudeString_3(),
	AbstractMap_t3082917158::get_offset_of__zoom_4(),
	AbstractMap_t3082917158::get_offset_of__root_5(),
	AbstractMap_t3082917158::get_offset_of__tileProvider_6(),
	AbstractMap_t3082917158::get_offset_of__mapVisualizer_7(),
	AbstractMap_t3082917158::get_offset_of__unityTileSize_8(),
	AbstractMap_t3082917158::get_offset_of__snapMapHeightToZero_9(),
	AbstractMap_t3082917158::get_offset_of__worldHeightFixed_10(),
	AbstractMap_t3082917158::get_offset_of__fileSouce_11(),
	AbstractMap_t3082917158::get_offset_of__centerLatitudeLongitude_12(),
	AbstractMap_t3082917158::get_offset_of__centerMercator_13(),
	AbstractMap_t3082917158::get_offset_of__worldRelativeScale_14(),
	AbstractMap_t3082917158::get_offset_of_OnInitialized_15(),
	AbstractMap_t3082917158_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (AbstractMapVisualizer_t551237957), -1, sizeof(AbstractMapVisualizer_t551237957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3332[8] = 
{
	AbstractMapVisualizer_t551237957::get_offset_of_Factories_2(),
	AbstractMapVisualizer_t551237957::get_offset_of__loadingTexture_3(),
	AbstractMapVisualizer_t551237957::get_offset_of__map_4(),
	AbstractMapVisualizer_t551237957::get_offset_of__activeTiles_5(),
	AbstractMapVisualizer_t551237957::get_offset_of__inactiveTiles_6(),
	AbstractMapVisualizer_t551237957::get_offset_of__state_7(),
	AbstractMapVisualizer_t551237957::get_offset_of_OnMapVisualizerStateChanged_8(),
	AbstractMapVisualizer_t551237957_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (AbstractTileProvider_t1076087356), -1, sizeof(AbstractTileProvider_t1076087356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3333[6] = 
{
	AbstractTileProvider_t1076087356::get_offset_of_OnTileAdded_2(),
	AbstractTileProvider_t1076087356::get_offset_of_OnTileRemoved_3(),
	AbstractTileProvider_t1076087356::get_offset_of__map_4(),
	AbstractTileProvider_t1076087356::get_offset_of__activeTiles_5(),
	AbstractTileProvider_t1076087356_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	AbstractTileProvider_t1076087356_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (BasicMap_t2129013278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (CameraBoundsTileProvider_t706805987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[13] = 
{
	CameraBoundsTileProvider_t706805987::get_offset_of__camera_8(),
	CameraBoundsTileProvider_t706805987::get_offset_of__visibleBuffer_9(),
	CameraBoundsTileProvider_t706805987::get_offset_of__disposeBuffer_10(),
	CameraBoundsTileProvider_t706805987::get_offset_of__updateInterval_11(),
	CameraBoundsTileProvider_t706805987::get_offset_of__groundPlane_12(),
	CameraBoundsTileProvider_t706805987::get_offset_of__ray_13(),
	CameraBoundsTileProvider_t706805987::get_offset_of__hitDistance_14(),
	CameraBoundsTileProvider_t706805987::get_offset_of__viewportTarget_15(),
	CameraBoundsTileProvider_t706805987::get_offset_of__elapsedTime_16(),
	CameraBoundsTileProvider_t706805987::get_offset_of__shouldUpdate_17(),
	CameraBoundsTileProvider_t706805987::get_offset_of__currentLatitudeLongitude_18(),
	CameraBoundsTileProvider_t706805987::get_offset_of__cachedTile_19(),
	CameraBoundsTileProvider_t706805987::get_offset_of__currentTile_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (DynamicZoomMap_t2517981420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[2] = 
{
	DynamicZoomMap_t2517981420::get_offset_of_MinZoom_17(),
	DynamicZoomMap_t2517981420::get_offset_of_MaxZoom_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (DynamicZoomMapVisualizer_t2283552828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (DynamicZoomTileProvider_t2524886064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[9] = 
{
	DynamicZoomTileProvider_t2524886064::get_offset_of__referenceCamera_8(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__previousY_9(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__previousWebMercCenter_10(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__viewPortWebMercBounds_11(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__cameraZoomingRangeMinY_12(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__cameraZoomingRangeMaxY_13(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__groundPlane_14(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__dynamicZoomMap_15(),
	DynamicZoomTileProvider_t2524886064::get_offset_of__className_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (GlobeTileProvider_t1213554907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (InitializeMapWithLocationProvider_t2863001309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[2] = 
{
	InitializeMapWithLocationProvider_t2863001309::get_offset_of__map_2(),
	InitializeMapWithLocationProvider_t2863001309::get_offset_of__locationProvider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (TileStateChangedEventArgs_t145677042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[1] = 
{
	TileStateChangedEventArgs_t145677042::get_offset_of_TileId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (MapAtSpecificLocation_t683025726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (ModuleState_t1890103724)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3347[4] = 
{
	ModuleState_t1890103724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (AssignmentTypeAttribute_t3305162592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[1] = 
{
	AssignmentTypeAttribute_t3305162592::get_offset_of_Type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (MapVisualizer_t2953540877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (RangeAroundTransformTileProvider_t2204850524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[6] = 
{
	RangeAroundTransformTileProvider_t2204850524::get_offset_of__targetTransform_8(),
	RangeAroundTransformTileProvider_t2204850524::get_offset_of__visibleBuffer_9(),
	RangeAroundTransformTileProvider_t2204850524::get_offset_of__disposeBuffer_10(),
	RangeAroundTransformTileProvider_t2204850524::get_offset_of__initialized_11(),
	RangeAroundTransformTileProvider_t2204850524::get_offset_of__currentTile_12(),
	RangeAroundTransformTileProvider_t2204850524::get_offset_of__cachedTile_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (RangeTileProvider_t2484217965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[4] = 
{
	RangeTileProvider_t2484217965::get_offset_of__west_8(),
	RangeTileProvider_t2484217965::get_offset_of__north_9(),
	RangeTileProvider_t2484217965::get_offset_of__east_10(),
	RangeTileProvider_t2484217965::get_offset_of__south_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (MapboxAccess_t3460807032), -1, sizeof(MapboxAccess_t3460807032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3352[6] = 
{
	MapboxAccess_t3460807032::get_offset_of__telemetryLibrary_0(),
	MapboxAccess_t3460807032::get_offset_of__fileSource_1(),
	MapboxAccess_t3460807032_StaticFields::get_offset_of__instance_2(),
	MapboxAccess_t3460807032::get_offset_of__configuration_3(),
	MapboxAccess_t3460807032::get_offset_of__geocoder_4(),
	MapboxAccess_t3460807032::get_offset_of__directions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (InvalidTokenException_t1836971595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (MapboxConfiguration_t2330578778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[4] = 
{
	MapboxConfiguration_t2330578778::get_offset_of_AccessToken_0(),
	MapboxConfiguration_t2330578778::get_offset_of_MemoryCacheSize_1(),
	MapboxConfiguration_t2330578778::get_offset_of_MbTilesCacheSize_2(),
	MapboxConfiguration_t2330578778::get_offset_of_DefaultTimeout_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (FeatureBehaviour_t1816865007), -1, sizeof(FeatureBehaviour_t1816865007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3355[4] = 
{
	FeatureBehaviour_t1816865007::get_offset_of_U3CTransformU3Ek__BackingField_2(),
	FeatureBehaviour_t1816865007::get_offset_of_Data_3(),
	FeatureBehaviour_t1816865007::get_offset_of_DataString_4(),
	FeatureBehaviour_t1816865007_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (TextureSelector_t4065682441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[7] = 
{
	TextureSelector_t4065682441::get_offset_of__useSatelliteRoof_2(),
	TextureSelector_t4065682441::get_offset_of__textureTop_3(),
	TextureSelector_t4065682441::get_offset_of__textureSides_4(),
	TextureSelector_t4065682441::get_offset_of__tile_5(),
	TextureSelector_t4065682441::get_offset_of__meshRenderer_6(),
	TextureSelector_t4065682441::get_offset_of__topTextures_7(),
	TextureSelector_t4065682441::get_offset_of__sideTextures_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (VertexDebugger_t3902268203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3357[1] = 
{
	VertexDebugger_t3902268203::get_offset_of_Triangles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (VertexDebuggerGizmo_t66328475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (MeshData_t2053528051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[7] = 
{
	MeshData_t2053528051::get_offset_of_U3CEdgesU3Ek__BackingField_0(),
	MeshData_t2053528051::get_offset_of_U3CMercatorCenterU3Ek__BackingField_1(),
	MeshData_t2053528051::get_offset_of_U3CTileRectU3Ek__BackingField_2(),
	MeshData_t2053528051::get_offset_of_U3CVerticesU3Ek__BackingField_3(),
	MeshData_t2053528051::get_offset_of_U3CNormalsU3Ek__BackingField_4(),
	MeshData_t2053528051::get_offset_of_U3CTrianglesU3Ek__BackingField_5(),
	MeshData_t2053528051::get_offset_of_U3CUVU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (UnityTile_t2405085845), -1, sizeof(UnityTile_t2405085845_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3360[26] = 
{
	UnityTile_t2405085845::get_offset_of__rasterData_2(),
	UnityTile_t2405085845::get_offset_of__heightData_3(),
	UnityTile_t2405085845::get_offset_of__relativeScale_4(),
	UnityTile_t2405085845::get_offset_of__heightTexture_5(),
	UnityTile_t2405085845::get_offset_of__loadingTexture_6(),
	UnityTile_t2405085845::get_offset_of__tiles_7(),
	UnityTile_t2405085845::get_offset_of__meshRenderer_8(),
	UnityTile_t2405085845::get_offset_of__meshFilter_9(),
	UnityTile_t2405085845::get_offset_of__collider_10(),
	UnityTile_t2405085845::get_offset_of__vectorData_11(),
	UnityTile_t2405085845::get_offset_of__rect_12(),
	UnityTile_t2405085845::get_offset_of__unwrappedTileId_13(),
	UnityTile_t2405085845::get_offset_of__canonicalTileId_14(),
	UnityTile_t2405085845::get_offset_of_U3CTileScaleU3Ek__BackingField_15(),
	UnityTile_t2405085845::get_offset_of_RasterDataState_16(),
	UnityTile_t2405085845::get_offset_of_HeightDataState_17(),
	UnityTile_t2405085845::get_offset_of_VectorDataState_18(),
	UnityTile_t2405085845::get_offset_of_OnHeightDataChanged_19(),
	UnityTile_t2405085845::get_offset_of_OnRasterDataChanged_20(),
	UnityTile_t2405085845::get_offset_of_OnVectorDataChanged_21(),
	UnityTile_t2405085845_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
	UnityTile_t2405085845_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_23(),
	UnityTile_t2405085845_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_24(),
	UnityTile_t2405085845_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_25(),
	UnityTile_t2405085845_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_26(),
	UnityTile_t2405085845_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (VectorFeatureUnity_t1815898701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3361[3] = 
{
	VectorFeatureUnity_t1815898701::get_offset_of_U3CDataU3Ek__BackingField_0(),
	VectorFeatureUnity_t1815898701::get_offset_of_U3CPropertiesU3Ek__BackingField_1(),
	VectorFeatureUnity_t1815898701::get_offset_of_Points_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (TilePropertyState_t855605749)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3362[5] = 
{
	TilePropertyState_t855605749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (AbstractTileFactory_t1533438029), -1, sizeof(AbstractTileFactory_t1533438029_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3363[5] = 
{
	AbstractTileFactory_t1533438029::get_offset_of__fileSource_2(),
	AbstractTileFactory_t1533438029::get_offset_of_U3CStateU3Ek__BackingField_3(),
	AbstractTileFactory_t1533438029::get_offset_of__progress_4(),
	AbstractTileFactory_t1533438029::get_offset_of_OnFactoryStateChanged_5(),
	AbstractTileFactory_t1533438029_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (DirectionsFactory_t1089493808), -1, sizeof(DirectionsFactory_t1089493808_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3364[6] = 
{
	DirectionsFactory_t1089493808::get_offset_of__map_2(),
	DirectionsFactory_t1089493808::get_offset_of_MeshModifiers_3(),
	DirectionsFactory_t1089493808::get_offset_of__waypoints_4(),
	DirectionsFactory_t1089493808::get_offset_of__material_5(),
	DirectionsFactory_t1089493808::get_offset_of__directions_6(),
	DirectionsFactory_t1089493808_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (FlatSphereTerrainFactory_t1099794750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[6] = 
{
	FlatSphereTerrainFactory_t1099794750::get_offset_of__baseMaterial_7(),
	FlatSphereTerrainFactory_t1099794750::get_offset_of__radius_8(),
	FlatSphereTerrainFactory_t1099794750::get_offset_of__sampleCount_9(),
	FlatSphereTerrainFactory_t1099794750::get_offset_of__addCollider_10(),
	FlatSphereTerrainFactory_t1099794750::get_offset_of__addToLayer_11(),
	FlatSphereTerrainFactory_t1099794750::get_offset_of__layerId_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (FlatTerrainFactory_t2329281599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3366[5] = 
{
	FlatTerrainFactory_t2329281599::get_offset_of__baseMaterial_7(),
	FlatTerrainFactory_t2329281599::get_offset_of__addCollider_8(),
	FlatTerrainFactory_t2329281599::get_offset_of__addToLayer_9(),
	FlatTerrainFactory_t2329281599::get_offset_of__layerId_10(),
	FlatTerrainFactory_t2329281599::get_offset_of__cachedQuad_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (LowPolyTerrainFactory_t2718546587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3367[22] = 
{
	LowPolyTerrainFactory_t2718546587::get_offset_of__baseMaterial_7(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__mapIdType_8(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__customMapId_9(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__mapId_10(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__heightModifier_11(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__sampleCount_12(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__addCollider_13(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__addToLayer_14(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__layerId_15(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__useRelativeHeight_16(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__stitchTarget_17(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__meshData_18(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__currentTileMeshData_19(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__stitchTargetMeshData_20(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__newVertexList_21(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__newNormalList_22(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__newUvList_23(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__newTriangleList_24(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__newDir_25(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__vertA_26(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__vertB_27(),
	LowPolyTerrainFactory_t2718546587::get_offset_of__vertC_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[3] = 
{
	U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639::get_offset_of_pngRasterTile_0(),
	U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639::get_offset_of_tile_1(),
	U3CCreateTerrainHeightU3Ec__AnonStorey0_t1976526639::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (MapImageType_t1139904885)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3369[4] = 
{
	MapImageType_t1139904885::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (MapImageFactory_t200237364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[6] = 
{
	MapImageFactory_t200237364::get_offset_of__mapIdType_7(),
	MapImageFactory_t200237364::get_offset_of__customStyle_8(),
	MapImageFactory_t200237364::get_offset_of__mapId_9(),
	MapImageFactory_t200237364::get_offset_of__useCompression_10(),
	MapImageFactory_t200237364::get_offset_of__useMipMap_11(),
	MapImageFactory_t200237364::get_offset_of__useRetina_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (U3COnRegisteredU3Ec__AnonStorey0_t833595815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[3] = 
{
	U3COnRegisteredU3Ec__AnonStorey0_t833595815::get_offset_of_rasterTile_0(),
	U3COnRegisteredU3Ec__AnonStorey0_t833595815::get_offset_of_tile_1(),
	U3COnRegisteredU3Ec__AnonStorey0_t833595815::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (MeshFactory_t3379932301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[4] = 
{
	MeshFactory_t3379932301::get_offset_of__mapId_7(),
	MeshFactory_t3379932301::get_offset_of_Visualizers_8(),
	MeshFactory_t3379932301::get_offset_of__layerBuilder_9(),
	MeshFactory_t3379932301::get_offset_of__cachedData_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (U3COnRegisteredU3Ec__AnonStorey0_t2918131947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[3] = 
{
	U3COnRegisteredU3Ec__AnonStorey0_t2918131947::get_offset_of_vectorTile_0(),
	U3COnRegisteredU3Ec__AnonStorey0_t2918131947::get_offset_of_tile_1(),
	U3COnRegisteredU3Ec__AnonStorey0_t2918131947::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (StyleOptimizedVectorTileFactory_t1939249911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[5] = 
{
	StyleOptimizedVectorTileFactory_t1939249911::get_offset_of__mapId_7(),
	StyleOptimizedVectorTileFactory_t1939249911::get_offset_of__optimizedStyle_8(),
	StyleOptimizedVectorTileFactory_t1939249911::get_offset_of_Visualizers_9(),
	StyleOptimizedVectorTileFactory_t1939249911::get_offset_of__layerBuilder_10(),
	StyleOptimizedVectorTileFactory_t1939249911::get_offset_of__cachedData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (U3COnRegisteredU3Ec__AnonStorey0_t25107931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[3] = 
{
	U3COnRegisteredU3Ec__AnonStorey0_t25107931::get_offset_of_vectorTile_0(),
	U3COnRegisteredU3Ec__AnonStorey0_t25107931::get_offset_of_tile_1(),
	U3COnRegisteredU3Ec__AnonStorey0_t25107931::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (Style_t2684836192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3376[4] = 
{
	Style_t2684836192::get_offset_of_Name_0(),
	Style_t2684836192::get_offset_of_Id_1(),
	Style_t2684836192::get_offset_of_Modified_2(),
	Style_t2684836192::get_offset_of_UserName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (MapIdType_t41340012)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3377[3] = 
{
	MapIdType_t41340012::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (TerrainFactory_t974472764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[22] = 
{
	TerrainFactory_t974472764::get_offset_of__baseMaterial_7(),
	TerrainFactory_t974472764::get_offset_of__mapIdType_8(),
	TerrainFactory_t974472764::get_offset_of__customMapId_9(),
	TerrainFactory_t974472764::get_offset_of__mapId_10(),
	TerrainFactory_t974472764::get_offset_of__heightModifier_11(),
	TerrainFactory_t974472764::get_offset_of__sampleCount_12(),
	TerrainFactory_t974472764::get_offset_of__addCollider_13(),
	TerrainFactory_t974472764::get_offset_of__addToLayer_14(),
	TerrainFactory_t974472764::get_offset_of__layerId_15(),
	TerrainFactory_t974472764::get_offset_of__useRelativeHeight_16(),
	TerrainFactory_t974472764::get_offset_of__stitchTarget_17(),
	TerrainFactory_t974472764::get_offset_of__meshData_18(),
	TerrainFactory_t974472764::get_offset_of__currentTileMeshData_19(),
	TerrainFactory_t974472764::get_offset_of__stitchTargetMeshData_20(),
	TerrainFactory_t974472764::get_offset_of__newVertexList_21(),
	TerrainFactory_t974472764::get_offset_of__newNormalList_22(),
	TerrainFactory_t974472764::get_offset_of__newUvList_23(),
	TerrainFactory_t974472764::get_offset_of__newTriangleList_24(),
	TerrainFactory_t974472764::get_offset_of__newDir_25(),
	TerrainFactory_t974472764::get_offset_of__vertA_26(),
	TerrainFactory_t974472764::get_offset_of__vertB_27(),
	TerrainFactory_t974472764::get_offset_of__vertC_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[3] = 
{
	U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541::get_offset_of_pngRasterTile_0(),
	U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541::get_offset_of_tile_1(),
	U3CCreateTerrainHeightU3Ec__AnonStorey0_t3949184541::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (VectorTileFactory_t815718376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3380[4] = 
{
	VectorTileFactory_t815718376::get_offset_of__mapId_7(),
	VectorTileFactory_t815718376::get_offset_of_Visualizers_8(),
	VectorTileFactory_t815718376::get_offset_of__layerBuilder_9(),
	VectorTileFactory_t815718376::get_offset_of__cachedData_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (U3COnRegisteredU3Ec__AnonStorey0_t2929291401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[3] = 
{
	U3COnRegisteredU3Ec__AnonStorey0_t2929291401::get_offset_of_vectorTile_0(),
	U3COnRegisteredU3Ec__AnonStorey0_t2929291401::get_offset_of_tile_1(),
	U3COnRegisteredU3Ec__AnonStorey0_t2929291401::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (FilterBase_t3477228168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (HeightFilter_t1216745462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[2] = 
{
	HeightFilter_t1216745462::get_offset_of__height_2(),
	HeightFilter_t1216745462::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (HeightFilterOptions_t2664590344)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3384[3] = 
{
	HeightFilterOptions_t2664590344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (TypeFilter_t542231371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[2] = 
{
	TypeFilter_t542231371::get_offset_of__types_2(),
	TypeFilter_t542231371::get_offset_of__behaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (TypeFilterType_t700042925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3386[3] = 
{
	TypeFilterType_t700042925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (LayerVisualizerBase_t2900487818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[1] = 
{
	LayerVisualizerBase_t2900487818::get_offset_of_Active_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (PoiVisualizer_t1810903380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[4] = 
{
	PoiVisualizer_t1810903380::get_offset_of__key_3(),
	PoiVisualizer_t1810903380::get_offset_of_PoiPrefab_4(),
	PoiVisualizer_t1810903380::get_offset_of__container_5(),
	PoiVisualizer_t1810903380::get_offset_of__scaleDownWithWorld_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (TypeVisualizerTuple_t633660079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[2] = 
{
	TypeVisualizerTuple_t633660079::get_offset_of_Type_0(),
	TypeVisualizerTuple_t633660079::get_offset_of_Stack_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (VectorLayerVisualizer_t1004319578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[6] = 
{
	VectorLayerVisualizer_t1004319578::get_offset_of__classificationKey_3(),
	VectorLayerVisualizer_t1004319578::get_offset_of__key_4(),
	VectorLayerVisualizer_t1004319578::get_offset_of_Filters_5(),
	VectorLayerVisualizer_t1004319578::get_offset_of__defaultStack_6(),
	VectorLayerVisualizer_t1004319578::get_offset_of_Stacks_7(),
	VectorLayerVisualizer_t1004319578::get_offset_of__container_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (U3CBuildU3Ec__AnonStorey0_t2244634411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[1] = 
{
	U3CBuildU3Ec__AnonStorey0_t2244634411::get_offset_of_styleSelectorKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (GameObjectModifier_t609190006), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (AddMonoBehavioursModifier_t4170257881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3394[1] = 
{
	AddMonoBehavioursModifier_t4170257881::get_offset_of__types_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (AddMonoBehavioursModifierType_t1393761064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3395[2] = 
{
	AddMonoBehavioursModifierType_t1393761064::get_offset_of__typeString_0(),
	AddMonoBehavioursModifierType_t1393761064::get_offset_of__type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (ColliderModifier_t3767354032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3396[1] = 
{
	ColliderModifier_t3767354032::get_offset_of__colliderType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (ColliderType_t2253951828)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3397[5] = 
{
	ColliderType_t2253951828::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (LayerModifier_t1135889872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3398[1] = 
{
	LayerModifier_t1135889872::get_offset_of__layerId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (PrefabModifier_t985809945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3399[2] = 
{
	PrefabModifier_t985809945::get_offset_of__prefab_3(),
	PrefabModifier_t985809945::get_offset_of__scaleDownWithWorld_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
