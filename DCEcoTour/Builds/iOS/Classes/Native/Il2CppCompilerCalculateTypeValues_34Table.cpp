﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.String
struct String_t;
// Mapbox.Unity.Telemetry.ITelemetryLibrary
struct ITelemetryLibrary_t1008417135;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// POIController
struct POIController_t3154702473;
// MapPageController
struct MapPageController_t3109321034;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Action`1<Mapbox.Platform.Response>
struct Action_1_t1228140472;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t2596446823;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t558680695;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t639177103;
// System.Comparison`1<Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node>
struct Comparison_1_t2427248770;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t2597455733;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t3872604895;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t2541822629;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t1600652016;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// Mapbox.Platform.Response
struct Response_t1055672877;
// Mapbox.Unity.Utilities.HTTPRequest
struct HTTPRequest_t539111708;
// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier>
struct List_1_t149922598;
// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier>
struct List_1_t2081264748;
// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier,System.Boolean>
struct Func_2_t1030805891;
// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier,System.Boolean>
struct Func_2_t4044917861;
// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,System.Int32>
struct Dictionary_2_t265003808;
// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Data.MeshData>>
struct Dictionary_2_t839660848;
// System.Func`2<Mapbox.Unity.MeshGeneration.Data.MeshData,System.Boolean>
struct Func_2_t475509844;
// UnityEngine.GUIText
struct GUIText_t402233326;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// System.Collections.Generic.Dictionary`2<System.Int32,Mapbox.Unity.Utilities.Runnable/Routine>
struct Dictionary_2_t4036641988;
// Mapbox.Unity.Map.AbstractMap
struct AbstractMap_t3082917158;
// SimpleObjectPool
struct SimpleObjectPool_t1028341060;
// DataController
struct DataController_t353634109;
// SiteData[]
struct SiteDataU5BU5D_t2647968276;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct MouseLook_t2859678661;
// UnityStandardAssets.Utility.CurveControlledBob
struct CurveControlledBob_t2679313829;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct RigidbodyFirstPersonController_t1207297146;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t4087348596;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Object
struct Object_t631007953;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_t1887104210;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t219844479;
// UnityEngine.SpringJoint
struct SpringJoint_t1912369980;
// UnityEngine.Light
struct Light_t3756812086;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t3982445645;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct MovementSettings_t1096092444;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct AdvancedSettings_t778418834;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t197597763;
// UnityStandardAssets.Vehicles.Ball.Ball
struct Ball_t2378314638;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FOVKICK_T120370150_H
#define FOVKICK_T120370150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t120370150  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t4157153871 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_t3046754366 * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___IncreaseCurve_5)); }
	inline AnimationCurve_t3046754366 * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_t3046754366 * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T120370150_H
#ifndef TELEMETRYFALLBACK_T2579077632_H
#define TELEMETRYFALLBACK_T2579077632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Telemetry.TelemetryFallback
struct  TelemetryFallback_t2579077632  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.Telemetry.TelemetryFallback::_url
	String_t* ____url_0;

public:
	inline static int32_t get_offset_of__url_0() { return static_cast<int32_t>(offsetof(TelemetryFallback_t2579077632, ____url_0)); }
	inline String_t* get__url_0() const { return ____url_0; }
	inline String_t** get_address_of__url_0() { return &____url_0; }
	inline void set__url_0(String_t* value)
	{
		____url_0 = value;
		Il2CppCodeGenWriteBarrier((&____url_0), value);
	}
};

struct TelemetryFallback_t2579077632_StaticFields
{
public:
	// Mapbox.Unity.Telemetry.ITelemetryLibrary Mapbox.Unity.Telemetry.TelemetryFallback::_instance
	RuntimeObject* ____instance_1;

public:
	inline static int32_t get_offset_of__instance_1() { return static_cast<int32_t>(offsetof(TelemetryFallback_t2579077632_StaticFields, ____instance_1)); }
	inline RuntimeObject* get__instance_1() const { return ____instance_1; }
	inline RuntimeObject** get_address_of__instance_1() { return &____instance_1; }
	inline void set__instance_1(RuntimeObject* value)
	{
		____instance_1 = value;
		Il2CppCodeGenWriteBarrier((&____instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELEMETRYFALLBACK_T2579077632_H
#ifndef TELEMETRYDUMMY_T2953168920_H
#define TELEMETRYDUMMY_T2953168920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Telemetry.TelemetryDummy
struct  TelemetryDummy_t2953168920  : public RuntimeObject
{
public:

public:
};

struct TelemetryDummy_t2953168920_StaticFields
{
public:
	// Mapbox.Unity.Telemetry.ITelemetryLibrary Mapbox.Unity.Telemetry.TelemetryDummy::_instance
	RuntimeObject* ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(TelemetryDummy_t2953168920_StaticFields, ____instance_0)); }
	inline RuntimeObject* get__instance_0() const { return ____instance_0; }
	inline RuntimeObject** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject* value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELEMETRYDUMMY_T2953168920_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef U3CSHOWSITESU3EC__ITERATOR0_T948523329_H
#define U3CSHOWSITESU3EC__ITERATOR0_T948523329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapPageController/<ShowSites>c__Iterator0
struct  U3CShowSitesU3Ec__Iterator0_t948523329  : public RuntimeObject
{
public:
	// System.Int32 MapPageController/<ShowSites>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// UnityEngine.GameObject MapPageController/<ShowSites>c__Iterator0::<POIGameObject>__2
	GameObject_t1113636619 * ___U3CPOIGameObjectU3E__2_1;
	// POIController MapPageController/<ShowSites>c__Iterator0::<pointOfInterest>__2
	POIController_t3154702473 * ___U3CpointOfInterestU3E__2_2;
	// MapPageController MapPageController/<ShowSites>c__Iterator0::$this
	MapPageController_t3109321034 * ___U24this_3;
	// System.Object MapPageController/<ShowSites>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MapPageController/<ShowSites>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 MapPageController/<ShowSites>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CShowSitesU3Ec__Iterator0_t948523329, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CPOIGameObjectU3E__2_1() { return static_cast<int32_t>(offsetof(U3CShowSitesU3Ec__Iterator0_t948523329, ___U3CPOIGameObjectU3E__2_1)); }
	inline GameObject_t1113636619 * get_U3CPOIGameObjectU3E__2_1() const { return ___U3CPOIGameObjectU3E__2_1; }
	inline GameObject_t1113636619 ** get_address_of_U3CPOIGameObjectU3E__2_1() { return &___U3CPOIGameObjectU3E__2_1; }
	inline void set_U3CPOIGameObjectU3E__2_1(GameObject_t1113636619 * value)
	{
		___U3CPOIGameObjectU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPOIGameObjectU3E__2_1), value);
	}

	inline static int32_t get_offset_of_U3CpointOfInterestU3E__2_2() { return static_cast<int32_t>(offsetof(U3CShowSitesU3Ec__Iterator0_t948523329, ___U3CpointOfInterestU3E__2_2)); }
	inline POIController_t3154702473 * get_U3CpointOfInterestU3E__2_2() const { return ___U3CpointOfInterestU3E__2_2; }
	inline POIController_t3154702473 ** get_address_of_U3CpointOfInterestU3E__2_2() { return &___U3CpointOfInterestU3E__2_2; }
	inline void set_U3CpointOfInterestU3E__2_2(POIController_t3154702473 * value)
	{
		___U3CpointOfInterestU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointOfInterestU3E__2_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CShowSitesU3Ec__Iterator0_t948523329, ___U24this_3)); }
	inline MapPageController_t3109321034 * get_U24this_3() const { return ___U24this_3; }
	inline MapPageController_t3109321034 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MapPageController_t3109321034 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CShowSitesU3Ec__Iterator0_t948523329, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CShowSitesU3Ec__Iterator0_t948523329, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CShowSitesU3Ec__Iterator0_t948523329, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWSITESU3EC__ITERATOR0_T948523329_H
#ifndef REPLACEMENTDEFINITION_T2693741842_H
#define REPLACEMENTDEFINITION_T2693741842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t2693741842  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t4151988712 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t4151988712 * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___original_0)); }
	inline Shader_t4151988712 * get_original_0() const { return ___original_0; }
	inline Shader_t4151988712 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_t4151988712 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___replacement_1)); }
	inline Shader_t4151988712 * get_replacement_1() const { return ___replacement_1; }
	inline Shader_t4151988712 ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_t4151988712 * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_T2693741842_H
#ifndef U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#define U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0
struct  U3CDoBobCycleU3Ec__Iterator0_t1149538828  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$this
	LerpControlledBob_t1895875871 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24this_1)); }
	inline LerpControlledBob_t1895875871 * get_U24this_1() const { return ___U24this_1; }
	inline LerpControlledBob_t1895875871 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LerpControlledBob_t1895875871 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifndef U3CPOSTU3EC__ITERATOR0_T2165426564_H
#define U3CPOSTU3EC__ITERATOR0_T2165426564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0
struct  U3CPostU3Ec__Iterator0_t2165426564  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.Networking.UnityWebRequest Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// System.String Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0::bodyJsonString
	String_t* ___bodyJsonString_2;
	// System.Byte[] Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0::<bodyRaw>__0
	ByteU5BU5D_t4116647657* ___U3CbodyRawU3E__0_3;
	// System.Object Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Mapbox.Unity.Telemetry.TelemetryFallback/<Post>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__Iterator0_t2165426564, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__Iterator0_t2165426564, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_bodyJsonString_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__Iterator0_t2165426564, ___bodyJsonString_2)); }
	inline String_t* get_bodyJsonString_2() const { return ___bodyJsonString_2; }
	inline String_t** get_address_of_bodyJsonString_2() { return &___bodyJsonString_2; }
	inline void set_bodyJsonString_2(String_t* value)
	{
		___bodyJsonString_2 = value;
		Il2CppCodeGenWriteBarrier((&___bodyJsonString_2), value);
	}

	inline static int32_t get_offset_of_U3CbodyRawU3E__0_3() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__Iterator0_t2165426564, ___U3CbodyRawU3E__0_3)); }
	inline ByteU5BU5D_t4116647657* get_U3CbodyRawU3E__0_3() const { return ___U3CbodyRawU3E__0_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbodyRawU3E__0_3() { return &___U3CbodyRawU3E__0_3; }
	inline void set_U3CbodyRawU3E__0_3(ByteU5BU5D_t4116647657* value)
	{
		___U3CbodyRawU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbodyRawU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__Iterator0_t2165426564, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__Iterator0_t2165426564, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__Iterator0_t2165426564, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ITERATOR0_T2165426564_H
#ifndef VECTOREXTENSIONS_T2364469250_H
#define VECTOREXTENSIONS_T2364469250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.VectorExtensions
struct  VectorExtensions_t2364469250  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOREXTENSIONS_T2364469250_H
#ifndef LERPCONTROLLEDBOB_T1895875871_H
#define LERPCONTROLLEDBOB_T1895875871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t1895875871  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T1895875871_H
#ifndef HTTPREQUEST_T539111708_H
#define HTTPREQUEST_T539111708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.HTTPRequest
struct  HTTPRequest_t539111708  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest Mapbox.Unity.Utilities.HTTPRequest::_request
	UnityWebRequest_t463507806 * ____request_0;
	// System.Int32 Mapbox.Unity.Utilities.HTTPRequest::_timeout
	int32_t ____timeout_1;
	// System.Action`1<Mapbox.Platform.Response> Mapbox.Unity.Utilities.HTTPRequest::_callback
	Action_1_t1228140472 * ____callback_2;
	// System.Boolean Mapbox.Unity.Utilities.HTTPRequest::_wasCancelled
	bool ____wasCancelled_3;
	// System.Boolean Mapbox.Unity.Utilities.HTTPRequest::<IsCompleted>k__BackingField
	bool ___U3CIsCompletedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__request_0() { return static_cast<int32_t>(offsetof(HTTPRequest_t539111708, ____request_0)); }
	inline UnityWebRequest_t463507806 * get__request_0() const { return ____request_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__request_0() { return &____request_0; }
	inline void set__request_0(UnityWebRequest_t463507806 * value)
	{
		____request_0 = value;
		Il2CppCodeGenWriteBarrier((&____request_0), value);
	}

	inline static int32_t get_offset_of__timeout_1() { return static_cast<int32_t>(offsetof(HTTPRequest_t539111708, ____timeout_1)); }
	inline int32_t get__timeout_1() const { return ____timeout_1; }
	inline int32_t* get_address_of__timeout_1() { return &____timeout_1; }
	inline void set__timeout_1(int32_t value)
	{
		____timeout_1 = value;
	}

	inline static int32_t get_offset_of__callback_2() { return static_cast<int32_t>(offsetof(HTTPRequest_t539111708, ____callback_2)); }
	inline Action_1_t1228140472 * get__callback_2() const { return ____callback_2; }
	inline Action_1_t1228140472 ** get_address_of__callback_2() { return &____callback_2; }
	inline void set__callback_2(Action_1_t1228140472 * value)
	{
		____callback_2 = value;
		Il2CppCodeGenWriteBarrier((&____callback_2), value);
	}

	inline static int32_t get_offset_of__wasCancelled_3() { return static_cast<int32_t>(offsetof(HTTPRequest_t539111708, ____wasCancelled_3)); }
	inline bool get__wasCancelled_3() const { return ____wasCancelled_3; }
	inline bool* get_address_of__wasCancelled_3() { return &____wasCancelled_3; }
	inline void set__wasCancelled_3(bool value)
	{
		____wasCancelled_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsCompletedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPRequest_t539111708, ___U3CIsCompletedU3Ek__BackingField_4)); }
	inline bool get_U3CIsCompletedU3Ek__BackingField_4() const { return ___U3CIsCompletedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsCompletedU3Ek__BackingField_4() { return &___U3CIsCompletedU3Ek__BackingField_4; }
	inline void set_U3CIsCompletedU3Ek__BackingField_4(bool value)
	{
		___U3CIsCompletedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUEST_T539111708_H
#ifndef REPLACEMENTLIST_T1887104210_H
#define REPLACEMENTLIST_T1887104210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_t1887104210  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t2596446823* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_t1887104210, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t2596446823* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t2596446823** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t2596446823* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_T1887104210_H
#ifndef CONVERSIONS_T2701420957_H
#define CONVERSIONS_T2701420957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.Conversions
struct  Conversions_t2701420957  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSIONS_T2701420957_H
#ifndef TELEMETRYIOS_T3970959974_H
#define TELEMETRYIOS_T3970959974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Telemetry.TelemetryIos
struct  TelemetryIos_t3970959974  : public RuntimeObject
{
public:

public:
};

struct TelemetryIos_t3970959974_StaticFields
{
public:
	// Mapbox.Unity.Telemetry.ITelemetryLibrary Mapbox.Unity.Telemetry.TelemetryIos::_instance
	RuntimeObject* ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(TelemetryIos_t3970959974_StaticFields, ____instance_0)); }
	inline RuntimeObject* get__instance_0() const { return ____instance_0; }
	inline RuntimeObject** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject* value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELEMETRYIOS_T3970959974_H
#ifndef U3CPOSTWWWU3EC__ITERATOR1_T2684415133_H
#define U3CPOSTWWWU3EC__ITERATOR1_T2684415133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1
struct  U3CPostWWWU3Ec__Iterator1_t2684415133  : public RuntimeObject
{
public:
	// System.String Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::bodyJsonString
	String_t* ___bodyJsonString_0;
	// System.Byte[] Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::<bodyRaw>__0
	ByteU5BU5D_t4116647657* ___U3CbodyRawU3E__0_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::<headers>__0
	Dictionary_2_t1632706988 * ___U3CheadersU3E__0_2;
	// System.String Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::url
	String_t* ___url_3;
	// UnityEngine.WWW Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_4;
	// System.Object Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Mapbox.Unity.Telemetry.TelemetryFallback/<PostWWW>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_bodyJsonString_0() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___bodyJsonString_0)); }
	inline String_t* get_bodyJsonString_0() const { return ___bodyJsonString_0; }
	inline String_t** get_address_of_bodyJsonString_0() { return &___bodyJsonString_0; }
	inline void set_bodyJsonString_0(String_t* value)
	{
		___bodyJsonString_0 = value;
		Il2CppCodeGenWriteBarrier((&___bodyJsonString_0), value);
	}

	inline static int32_t get_offset_of_U3CbodyRawU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___U3CbodyRawU3E__0_1)); }
	inline ByteU5BU5D_t4116647657* get_U3CbodyRawU3E__0_1() const { return ___U3CbodyRawU3E__0_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbodyRawU3E__0_1() { return &___U3CbodyRawU3E__0_1; }
	inline void set_U3CbodyRawU3E__0_1(ByteU5BU5D_t4116647657* value)
	{
		___U3CbodyRawU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbodyRawU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___U3CheadersU3E__0_2)); }
	inline Dictionary_2_t1632706988 * get_U3CheadersU3E__0_2() const { return ___U3CheadersU3E__0_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CheadersU3E__0_2() { return &___U3CheadersU3E__0_2; }
	inline void set_U3CheadersU3E__0_2(Dictionary_2_t1632706988 * value)
	{
		___U3CheadersU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CheadersU3E__0_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_4() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___U3CwwwU3E__0_4)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_4() const { return ___U3CwwwU3E__0_4; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_4() { return &___U3CwwwU3E__0_4; }
	inline void set_U3CwwwU3E__0_4(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__Iterator1_t2684415133, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTWWWU3EC__ITERATOR1_T2684415133_H
#ifndef ADVANCEDSETTINGS_T778418834_H
#define ADVANCEDSETTINGS_T778418834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct  AdvancedSettings_t778418834  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::groundCheckDistance
	float ___groundCheckDistance_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::stickToGroundHelperDistance
	float ___stickToGroundHelperDistance_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::slowDownRate
	float ___slowDownRate_2;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::airControl
	bool ___airControl_3;

public:
	inline static int32_t get_offset_of_groundCheckDistance_0() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___groundCheckDistance_0)); }
	inline float get_groundCheckDistance_0() const { return ___groundCheckDistance_0; }
	inline float* get_address_of_groundCheckDistance_0() { return &___groundCheckDistance_0; }
	inline void set_groundCheckDistance_0(float value)
	{
		___groundCheckDistance_0 = value;
	}

	inline static int32_t get_offset_of_stickToGroundHelperDistance_1() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___stickToGroundHelperDistance_1)); }
	inline float get_stickToGroundHelperDistance_1() const { return ___stickToGroundHelperDistance_1; }
	inline float* get_address_of_stickToGroundHelperDistance_1() { return &___stickToGroundHelperDistance_1; }
	inline void set_stickToGroundHelperDistance_1(float value)
	{
		___stickToGroundHelperDistance_1 = value;
	}

	inline static int32_t get_offset_of_slowDownRate_2() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___slowDownRate_2)); }
	inline float get_slowDownRate_2() const { return ___slowDownRate_2; }
	inline float* get_address_of_slowDownRate_2() { return &___slowDownRate_2; }
	inline void set_slowDownRate_2(float value)
	{
		___slowDownRate_2 = value;
	}

	inline static int32_t get_offset_of_airControl_3() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___airControl_3)); }
	inline bool get_airControl_3() const { return ___airControl_3; }
	inline bool* get_address_of_airControl_3() { return &___airControl_3; }
	inline void set_airControl_3(bool value)
	{
		___airControl_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDSETTINGS_T778418834_H
#ifndef VIRTUALAXIS_T4087348596_H
#define VIRTUALAXIS_T4087348596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_t4087348596  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_T4087348596_H
#ifndef VIRTUALBUTTON_T2756566330_H
#define VIRTUALBUTTON_T2756566330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_t2756566330  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T2756566330_H
#ifndef U3CSTARTU3EC__ITERATOR0_T980021917_H
#define U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t980021917  : public RuntimeObject
{
public:
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<systems>__0
	ParticleSystemU5BU5D_t3089334924* ___U3CsystemsU3E__0_0;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar0
	ParticleSystemU5BU5D_t3089334924* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<stopTime>__0
	float ___U3CstopTimeU3E__0_3;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar2
	ParticleSystemU5BU5D_t3089334924* ___U24locvar2_4;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar3
	int32_t ___U24locvar3_5;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$this
	ParticleSystemDestroyer_t558680695 * ___U24this_6;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CsystemsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CsystemsU3E__0_0)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U3CsystemsU3E__0_0() const { return ___U3CsystemsU3E__0_0; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U3CsystemsU3E__0_0() { return &___U3CsystemsU3E__0_0; }
	inline void set_U3CsystemsU3E__0_0(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U3CsystemsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar0_1)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CstopTimeU3E__0_3)); }
	inline float get_U3CstopTimeU3E__0_3() const { return ___U3CstopTimeU3E__0_3; }
	inline float* get_address_of_U3CstopTimeU3E__0_3() { return &___U3CstopTimeU3E__0_3; }
	inline void set_U3CstopTimeU3E__0_3(float value)
	{
		___U3CstopTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar2_4)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar2_4() const { return ___U24locvar2_4; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar2_4() { return &___U24locvar2_4; }
	inline void set_U24locvar2_4(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_4), value);
	}

	inline static int32_t get_offset_of_U24locvar3_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar3_5)); }
	inline int32_t get_U24locvar3_5() const { return ___U24locvar3_5; }
	inline int32_t* get_address_of_U24locvar3_5() { return &___U24locvar3_5; }
	inline void set_U24locvar3_5(int32_t value)
	{
		___U24locvar3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24this_6)); }
	inline ParticleSystemDestroyer_t558680695 * get_U24this_6() const { return ___U24this_6; }
	inline ParticleSystemDestroyer_t558680695 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ParticleSystemDestroyer_t558680695 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifndef U3CENDU3EC__ANONSTOREY0_T266015876_H
#define U3CENDU3EC__ANONSTOREY0_T266015876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack/<End>c__AnonStorey0
struct  U3CEndU3Ec__AnonStorey0_t266015876  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack/<End>c__AnonStorey0::st
	int32_t ___st_0;

public:
	inline static int32_t get_offset_of_st_0() { return static_cast<int32_t>(offsetof(U3CEndU3Ec__AnonStorey0_t266015876, ___st_0)); }
	inline int32_t get_st_0() const { return ___st_0; }
	inline int32_t* get_address_of_st_0() { return &___st_0; }
	inline void set_st_0(int32_t value)
	{
		___st_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENDU3EC__ANONSTOREY0_T266015876_H
#ifndef U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#define U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1
struct  U3CFOVKickDownU3Ec__Iterator1_t1440840980  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifndef U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#define U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0
struct  U3CFOVKickUpU3Ec__Iterator0_t3738408313  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifndef U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#define U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0
struct  U3CResetCoroutineU3Ec__Iterator0_t3232105836  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::delay
	float ___delay_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar0
	TransformU5BU5D_t807237628* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$this
	ObjectResetter_t639177103 * ___U24this_3;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar0_1)); }
	inline TransformU5BU5D_t807237628* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline TransformU5BU5D_t807237628** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(TransformU5BU5D_t807237628* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24this_3)); }
	inline ObjectResetter_t639177103 * get_U24this_3() const { return ___U24this_3; }
	inline ObjectResetter_t639177103 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ObjectResetter_t639177103 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifndef EARCUTLIBRARY_T3691687667_H
#define EARCUTLIBRARY_T3691687667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.EarcutLibrary
struct  EarcutLibrary_t3691687667  : public RuntimeObject
{
public:

public:
};

struct EarcutLibrary_t3691687667_StaticFields
{
public:
	// System.Comparison`1<Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node> Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.EarcutLibrary::<>f__am$cache0
	Comparison_1_t2427248770 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(EarcutLibrary_t3691687667_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Comparison_1_t2427248770 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Comparison_1_t2427248770 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Comparison_1_t2427248770 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EARCUTLIBRARY_T3691687667_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CROSSPLATFORMINPUTMANAGER_T191731427_H
#define CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t191731427  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t191731427_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t2597455733 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t2597455733 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t2597455733 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t2597455733 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t2597455733 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t2597455733 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t2597455733 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t2597455733 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t2597455733 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t2597455733 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t2597455733 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t2597455733 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifndef ROUTINE_T852961361_H
#define ROUTINE_T852961361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.Runnable/Routine
struct  Routine_t852961361  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Unity.Utilities.Runnable/Routine::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_0;
	// System.Boolean Mapbox.Unity.Utilities.Runnable/Routine::<Stop>k__BackingField
	bool ___U3CStopU3Ek__BackingField_1;
	// System.Boolean Mapbox.Unity.Utilities.Runnable/Routine::m_bMoveNext
	bool ___m_bMoveNext_2;
	// System.Collections.IEnumerator Mapbox.Unity.Utilities.Runnable/Routine::m_Enumerator
	RuntimeObject* ___m_Enumerator_3;

public:
	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Routine_t852961361, ___U3CIDU3Ek__BackingField_0)); }
	inline int32_t get_U3CIDU3Ek__BackingField_0() const { return ___U3CIDU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_0() { return &___U3CIDU3Ek__BackingField_0; }
	inline void set_U3CIDU3Ek__BackingField_0(int32_t value)
	{
		___U3CIDU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStopU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Routine_t852961361, ___U3CStopU3Ek__BackingField_1)); }
	inline bool get_U3CStopU3Ek__BackingField_1() const { return ___U3CStopU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CStopU3Ek__BackingField_1() { return &___U3CStopU3Ek__BackingField_1; }
	inline void set_U3CStopU3Ek__BackingField_1(bool value)
	{
		___U3CStopU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_bMoveNext_2() { return static_cast<int32_t>(offsetof(Routine_t852961361, ___m_bMoveNext_2)); }
	inline bool get_m_bMoveNext_2() const { return ___m_bMoveNext_2; }
	inline bool* get_address_of_m_bMoveNext_2() { return &___m_bMoveNext_2; }
	inline void set_m_bMoveNext_2(bool value)
	{
		___m_bMoveNext_2 = value;
	}

	inline static int32_t get_offset_of_m_Enumerator_3() { return static_cast<int32_t>(offsetof(Routine_t852961361, ___m_Enumerator_3)); }
	inline RuntimeObject* get_m_Enumerator_3() const { return ___m_Enumerator_3; }
	inline RuntimeObject** get_address_of_m_Enumerator_3() { return &___m_Enumerator_3; }
	inline void set_m_Enumerator_3(RuntimeObject* value)
	{
		___m_Enumerator_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Enumerator_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTINE_T852961361_H
#ifndef DATA_T1129010489_H
#define DATA_T1129010489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Data
struct  Data_t1129010489  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Data::Vertices
	List_1_t2869341516 * ___Vertices_0;
	// System.Collections.Generic.List`1<System.Int32> Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Data::Holes
	List_1_t128053199 * ___Holes_1;
	// System.Int32 Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Data::Dim
	int32_t ___Dim_2;

public:
	inline static int32_t get_offset_of_Vertices_0() { return static_cast<int32_t>(offsetof(Data_t1129010489, ___Vertices_0)); }
	inline List_1_t2869341516 * get_Vertices_0() const { return ___Vertices_0; }
	inline List_1_t2869341516 ** get_address_of_Vertices_0() { return &___Vertices_0; }
	inline void set_Vertices_0(List_1_t2869341516 * value)
	{
		___Vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_0), value);
	}

	inline static int32_t get_offset_of_Holes_1() { return static_cast<int32_t>(offsetof(Data_t1129010489, ___Holes_1)); }
	inline List_1_t128053199 * get_Holes_1() const { return ___Holes_1; }
	inline List_1_t128053199 ** get_address_of_Holes_1() { return &___Holes_1; }
	inline void set_Holes_1(List_1_t128053199 * value)
	{
		___Holes_1 = value;
		Il2CppCodeGenWriteBarrier((&___Holes_1), value);
	}

	inline static int32_t get_offset_of_Dim_2() { return static_cast<int32_t>(offsetof(Data_t1129010489, ___Dim_2)); }
	inline int32_t get_Dim_2() const { return ___Dim_2; }
	inline int32_t* get_address_of_Dim_2() { return &___Dim_2; }
	inline void set_Dim_2(int32_t value)
	{
		___Dim_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATA_T1129010489_H
#ifndef NODE_T2652317591_H
#define NODE_T2652317591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node
struct  Node_t2652317591  : public RuntimeObject
{
public:
	// System.Int32 Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::i
	int32_t ___i_0;
	// System.Single Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::x
	float ___x_1;
	// System.Single Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::y
	float ___y_2;
	// System.Int32 Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::mZOrder
	int32_t ___mZOrder_3;
	// Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::prev
	Node_t2652317591 * ___prev_4;
	// Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::next
	Node_t2652317591 * ___next_5;
	// Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::prevZ
	Node_t2652317591 * ___prevZ_6;
	// Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::nextZ
	Node_t2652317591 * ___nextZ_7;
	// System.Boolean Assets.Mapbox.Unity.MeshGeneration.Modifiers.MeshModifiers.Node::steiner
	bool ___steiner_8;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_mZOrder_3() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___mZOrder_3)); }
	inline int32_t get_mZOrder_3() const { return ___mZOrder_3; }
	inline int32_t* get_address_of_mZOrder_3() { return &___mZOrder_3; }
	inline void set_mZOrder_3(int32_t value)
	{
		___mZOrder_3 = value;
	}

	inline static int32_t get_offset_of_prev_4() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___prev_4)); }
	inline Node_t2652317591 * get_prev_4() const { return ___prev_4; }
	inline Node_t2652317591 ** get_address_of_prev_4() { return &___prev_4; }
	inline void set_prev_4(Node_t2652317591 * value)
	{
		___prev_4 = value;
		Il2CppCodeGenWriteBarrier((&___prev_4), value);
	}

	inline static int32_t get_offset_of_next_5() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___next_5)); }
	inline Node_t2652317591 * get_next_5() const { return ___next_5; }
	inline Node_t2652317591 ** get_address_of_next_5() { return &___next_5; }
	inline void set_next_5(Node_t2652317591 * value)
	{
		___next_5 = value;
		Il2CppCodeGenWriteBarrier((&___next_5), value);
	}

	inline static int32_t get_offset_of_prevZ_6() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___prevZ_6)); }
	inline Node_t2652317591 * get_prevZ_6() const { return ___prevZ_6; }
	inline Node_t2652317591 ** get_address_of_prevZ_6() { return &___prevZ_6; }
	inline void set_prevZ_6(Node_t2652317591 * value)
	{
		___prevZ_6 = value;
		Il2CppCodeGenWriteBarrier((&___prevZ_6), value);
	}

	inline static int32_t get_offset_of_nextZ_7() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___nextZ_7)); }
	inline Node_t2652317591 * get_nextZ_7() const { return ___nextZ_7; }
	inline Node_t2652317591 ** get_address_of_nextZ_7() { return &___nextZ_7; }
	inline void set_nextZ_7(Node_t2652317591 * value)
	{
		___nextZ_7 = value;
		Il2CppCodeGenWriteBarrier((&___nextZ_7), value);
	}

	inline static int32_t get_offset_of_steiner_8() { return static_cast<int32_t>(offsetof(Node_t2652317591, ___steiner_8)); }
	inline bool get_steiner_8() const { return ___steiner_8; }
	inline bool* get_address_of_steiner_8() { return &___steiner_8; }
	inline void set_steiner_8(bool value)
	{
		___steiner_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T2652317591_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NODEEDITORELEMENTATTRIBUTE_T2832500404_H
#define NODEEDITORELEMENTATTRIBUTE_T2832500404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NodeEditorElementAttribute
struct  NodeEditorElementAttribute_t2832500404  : public Attribute_t861562559
{
public:
	// System.String NodeEditorElementAttribute::Name
	String_t* ___Name_0;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(NodeEditorElementAttribute_t2832500404, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEEDITORELEMENTATTRIBUTE_T2832500404_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef AXISOPTION_T1372819835_H
#define AXISOPTION_T1372819835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t1372819835 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t1372819835, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1372819835_H
#ifndef VIRTUALINPUT_T2597455733_H
#define VIRTUALINPUT_T2597455733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t2597455733  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_t3722313464  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t3872604895 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t2541822629 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_t3319525431 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t3872604895 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t3872604895 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t3872604895 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t2541822629 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t2541822629 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t2541822629 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_AlwaysUseVirtual_3)); }
	inline List_1_t3319525431 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_t3319525431 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_t3319525431 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T2597455733_H
#ifndef EXTRUSIONTYPE_T1596510477_H
#define EXTRUSIONTYPE_T1596510477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ExtrusionType
struct  ExtrusionType_t1596510477 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.ExtrusionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ExtrusionType_t1596510477, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRUSIONTYPE_T1596510477_H
#ifndef ACTIVEINPUTMETHOD_T139315314_H
#define ACTIVEINPUTMETHOD_T139315314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t139315314 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t139315314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T139315314_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef BUILDTARGETGROUP_T72322187_H
#define BUILDTARGETGROUP_T72322187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t72322187 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t72322187, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T72322187_H
#ifndef MAPPINGTYPE_T2039944511_H
#define MAPPINGTYPE_T2039944511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_t2039944511 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MappingType_t2039944511, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T2039944511_H
#ifndef CONTROLSTYLE_T1372986211_H
#define CONTROLSTYLE_T1372986211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t1372986211 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlStyle_t1372986211, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T1372986211_H
#ifndef AXISOPTIONS_T3101732129_H
#define AXISOPTIONS_T3101732129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t3101732129 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOptions_t3101732129, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T3101732129_H
#ifndef MODIFIERTYPE_T2211322250_H
#define MODIFIERTYPE_T2211322250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ModifierType
struct  ModifierType_t2211322250 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.ModifierType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ModifierType_t2211322250, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERTYPE_T2211322250_H
#ifndef AXISOPTION_T3128671669_H
#define AXISOPTION_T3128671669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t3128671669 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t3128671669, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T3128671669_H
#ifndef CURVECONTROLLEDBOB_T2679313829_H
#define CURVECONTROLLEDBOB_T2679313829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t2679313829  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_t3046754366 * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___Bobcurve_2)); }
	inline AnimationCurve_t3046754366 * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_t3046754366 * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T2679313829_H
#ifndef POSITIONTARGETTYPE_T3604874692_H
#define POSITIONTARGETTYPE_T3604874692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.PositionTargetType
struct  PositionTargetType_t3604874692 
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.PositionTargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PositionTargetType_t3604874692, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONTARGETTYPE_T3604874692_H
#ifndef CAMERAREFOCUS_T4263235746_H
#define CAMERAREFOCUS_T4263235746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t4263235746  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t4157153871 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_t3722313464  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_t3600365921 * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_t3722313464  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Lookatpoint_1)); }
	inline Vector3_t3722313464  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_t3722313464 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_t3722313464  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Parent_2)); }
	inline Transform_t3600365921 * get_Parent_2() const { return ___Parent_2; }
	inline Transform_t3600365921 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_t3600365921 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_OrigCameraPos_3)); }
	inline Vector3_t3722313464  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_t3722313464 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_t3722313464  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T4263235746_H
#ifndef GEOCODEATTRIBUTE_T4233991700_H
#define GEOCODEATTRIBUTE_T4233991700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.GeocodeAttribute
struct  GeocodeAttribute_t4233991700  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOCODEATTRIBUTE_T4233991700_H
#ifndef COLLISIONFLAGS_T1776808576_H
#define COLLISIONFLAGS_T1776808576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t1776808576 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t1776808576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T1776808576_H
#ifndef STYLESEARCHATTRIBUTE_T2646828085_H
#define STYLESEARCHATTRIBUTE_T2646828085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.StyleSearchAttribute
struct  StyleSearchAttribute_t2646828085  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLESEARCHATTRIBUTE_T2646828085_H
#ifndef MOUSELOOK_T2859678661_H
#define MOUSELOOK_T2859678661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct  MouseLook_t2859678661  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::XSensitivity
	float ___XSensitivity_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::YSensitivity
	float ___YSensitivity_1;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::clampVerticalRotation
	bool ___clampVerticalRotation_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MinimumX
	float ___MinimumX_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MaximumX
	float ___MaximumX_4;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::smooth
	bool ___smooth_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::smoothTime
	float ___smoothTime_6;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CharacterTargetRot
	Quaternion_t2301928331  ___m_CharacterTargetRot_7;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CameraTargetRot
	Quaternion_t2301928331  ___m_CameraTargetRot_8;

public:
	inline static int32_t get_offset_of_XSensitivity_0() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___XSensitivity_0)); }
	inline float get_XSensitivity_0() const { return ___XSensitivity_0; }
	inline float* get_address_of_XSensitivity_0() { return &___XSensitivity_0; }
	inline void set_XSensitivity_0(float value)
	{
		___XSensitivity_0 = value;
	}

	inline static int32_t get_offset_of_YSensitivity_1() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___YSensitivity_1)); }
	inline float get_YSensitivity_1() const { return ___YSensitivity_1; }
	inline float* get_address_of_YSensitivity_1() { return &___YSensitivity_1; }
	inline void set_YSensitivity_1(float value)
	{
		___YSensitivity_1 = value;
	}

	inline static int32_t get_offset_of_clampVerticalRotation_2() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___clampVerticalRotation_2)); }
	inline bool get_clampVerticalRotation_2() const { return ___clampVerticalRotation_2; }
	inline bool* get_address_of_clampVerticalRotation_2() { return &___clampVerticalRotation_2; }
	inline void set_clampVerticalRotation_2(bool value)
	{
		___clampVerticalRotation_2 = value;
	}

	inline static int32_t get_offset_of_MinimumX_3() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___MinimumX_3)); }
	inline float get_MinimumX_3() const { return ___MinimumX_3; }
	inline float* get_address_of_MinimumX_3() { return &___MinimumX_3; }
	inline void set_MinimumX_3(float value)
	{
		___MinimumX_3 = value;
	}

	inline static int32_t get_offset_of_MaximumX_4() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___MaximumX_4)); }
	inline float get_MaximumX_4() const { return ___MaximumX_4; }
	inline float* get_address_of_MaximumX_4() { return &___MaximumX_4; }
	inline void set_MaximumX_4(float value)
	{
		___MaximumX_4 = value;
	}

	inline static int32_t get_offset_of_smooth_5() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___smooth_5)); }
	inline bool get_smooth_5() const { return ___smooth_5; }
	inline bool* get_address_of_smooth_5() { return &___smooth_5; }
	inline void set_smooth_5(bool value)
	{
		___smooth_5 = value;
	}

	inline static int32_t get_offset_of_smoothTime_6() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___smoothTime_6)); }
	inline float get_smoothTime_6() const { return ___smoothTime_6; }
	inline float* get_address_of_smoothTime_6() { return &___smoothTime_6; }
	inline void set_smoothTime_6(float value)
	{
		___smoothTime_6 = value;
	}

	inline static int32_t get_offset_of_m_CharacterTargetRot_7() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_CharacterTargetRot_7)); }
	inline Quaternion_t2301928331  get_m_CharacterTargetRot_7() const { return ___m_CharacterTargetRot_7; }
	inline Quaternion_t2301928331 * get_address_of_m_CharacterTargetRot_7() { return &___m_CharacterTargetRot_7; }
	inline void set_m_CharacterTargetRot_7(Quaternion_t2301928331  value)
	{
		___m_CharacterTargetRot_7 = value;
	}

	inline static int32_t get_offset_of_m_CameraTargetRot_8() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_CameraTargetRot_8)); }
	inline Quaternion_t2301928331  get_m_CameraTargetRot_8() const { return ___m_CameraTargetRot_8; }
	inline Quaternion_t2301928331 * get_address_of_m_CameraTargetRot_8() { return &___m_CameraTargetRot_8; }
	inline void set_m_CameraTargetRot_8(Quaternion_t2301928331  value)
	{
		___m_CameraTargetRot_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSELOOK_T2859678661_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef MODE_T3024470803_H
#define MODE_T3024470803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t3024470803 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3024470803, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3024470803_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef VECTOR3ANDSPACE_T219844479_H
#define VECTOR3ANDSPACE_T219844479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t219844479  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_t3722313464  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T219844479_H
#ifndef U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#define U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0
struct  U3CDragObjectU3Ec__Iterator0_t4151609119  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldDrag>__0
	float ___U3ColdDragU3E__0_0;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldAngularDrag>__0
	float ___U3ColdAngularDragU3E__0_1;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<mainCamera>__0
	Camera_t4157153871 * ___U3CmainCameraU3E__0_2;
	// UnityEngine.Ray UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<ray>__1
	Ray_t3785851493  ___U3CrayU3E__1_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::distance
	float ___distance_4;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$this
	DragRigidbody_t1600652016 * ___U24this_5;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3ColdDragU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdDragU3E__0_0)); }
	inline float get_U3ColdDragU3E__0_0() const { return ___U3ColdDragU3E__0_0; }
	inline float* get_address_of_U3ColdDragU3E__0_0() { return &___U3ColdDragU3E__0_0; }
	inline void set_U3ColdDragU3E__0_0(float value)
	{
		___U3ColdDragU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdAngularDragU3E__0_1)); }
	inline float get_U3ColdAngularDragU3E__0_1() const { return ___U3ColdAngularDragU3E__0_1; }
	inline float* get_address_of_U3ColdAngularDragU3E__0_1() { return &___U3ColdAngularDragU3E__0_1; }
	inline void set_U3ColdAngularDragU3E__0_1(float value)
	{
		___U3ColdAngularDragU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CmainCameraU3E__0_2)); }
	inline Camera_t4157153871 * get_U3CmainCameraU3E__0_2() const { return ___U3CmainCameraU3E__0_2; }
	inline Camera_t4157153871 ** get_address_of_U3CmainCameraU3E__0_2() { return &___U3CmainCameraU3E__0_2; }
	inline void set_U3CmainCameraU3E__0_2(Camera_t4157153871 * value)
	{
		___U3CmainCameraU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrayU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CrayU3E__1_3)); }
	inline Ray_t3785851493  get_U3CrayU3E__1_3() const { return ___U3CrayU3E__1_3; }
	inline Ray_t3785851493 * get_address_of_U3CrayU3E__1_3() { return &___U3CrayU3E__1_3; }
	inline void set_U3CrayU3E__1_3(Ray_t3785851493  value)
	{
		___U3CrayU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24this_5)); }
	inline DragRigidbody_t1600652016 * get_U24this_5() const { return ___U24this_5; }
	inline DragRigidbody_t1600652016 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(DragRigidbody_t1600652016 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifndef AXISMAPPING_T3982445645_H
#define AXISMAPPING_T3982445645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t3982445645  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T3982445645_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MOVEMENTSETTINGS_T1096092444_H
#define MOVEMENTSETTINGS_T1096092444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct  MovementSettings_t1096092444  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::ForwardSpeed
	float ___ForwardSpeed_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::BackwardSpeed
	float ___BackwardSpeed_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::StrafeSpeed
	float ___StrafeSpeed_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunMultiplier
	float ___RunMultiplier_3;
	// UnityEngine.KeyCode UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunKey
	int32_t ___RunKey_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::JumpForce
	float ___JumpForce_5;
	// UnityEngine.AnimationCurve UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::SlopeCurveModifier
	AnimationCurve_t3046754366 * ___SlopeCurveModifier_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::CurrentTargetSpeed
	float ___CurrentTargetSpeed_7;

public:
	inline static int32_t get_offset_of_ForwardSpeed_0() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___ForwardSpeed_0)); }
	inline float get_ForwardSpeed_0() const { return ___ForwardSpeed_0; }
	inline float* get_address_of_ForwardSpeed_0() { return &___ForwardSpeed_0; }
	inline void set_ForwardSpeed_0(float value)
	{
		___ForwardSpeed_0 = value;
	}

	inline static int32_t get_offset_of_BackwardSpeed_1() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___BackwardSpeed_1)); }
	inline float get_BackwardSpeed_1() const { return ___BackwardSpeed_1; }
	inline float* get_address_of_BackwardSpeed_1() { return &___BackwardSpeed_1; }
	inline void set_BackwardSpeed_1(float value)
	{
		___BackwardSpeed_1 = value;
	}

	inline static int32_t get_offset_of_StrafeSpeed_2() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___StrafeSpeed_2)); }
	inline float get_StrafeSpeed_2() const { return ___StrafeSpeed_2; }
	inline float* get_address_of_StrafeSpeed_2() { return &___StrafeSpeed_2; }
	inline void set_StrafeSpeed_2(float value)
	{
		___StrafeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RunMultiplier_3() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___RunMultiplier_3)); }
	inline float get_RunMultiplier_3() const { return ___RunMultiplier_3; }
	inline float* get_address_of_RunMultiplier_3() { return &___RunMultiplier_3; }
	inline void set_RunMultiplier_3(float value)
	{
		___RunMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_RunKey_4() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___RunKey_4)); }
	inline int32_t get_RunKey_4() const { return ___RunKey_4; }
	inline int32_t* get_address_of_RunKey_4() { return &___RunKey_4; }
	inline void set_RunKey_4(int32_t value)
	{
		___RunKey_4 = value;
	}

	inline static int32_t get_offset_of_JumpForce_5() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___JumpForce_5)); }
	inline float get_JumpForce_5() const { return ___JumpForce_5; }
	inline float* get_address_of_JumpForce_5() { return &___JumpForce_5; }
	inline void set_JumpForce_5(float value)
	{
		___JumpForce_5 = value;
	}

	inline static int32_t get_offset_of_SlopeCurveModifier_6() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___SlopeCurveModifier_6)); }
	inline AnimationCurve_t3046754366 * get_SlopeCurveModifier_6() const { return ___SlopeCurveModifier_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_SlopeCurveModifier_6() { return &___SlopeCurveModifier_6; }
	inline void set_SlopeCurveModifier_6(AnimationCurve_t3046754366 * value)
	{
		___SlopeCurveModifier_6 = value;
		Il2CppCodeGenWriteBarrier((&___SlopeCurveModifier_6), value);
	}

	inline static int32_t get_offset_of_CurrentTargetSpeed_7() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___CurrentTargetSpeed_7)); }
	inline float get_CurrentTargetSpeed_7() const { return ___CurrentTargetSpeed_7; }
	inline float* get_address_of_CurrentTargetSpeed_7() { return &___CurrentTargetSpeed_7; }
	inline void set_CurrentTargetSpeed_7(float value)
	{
		___CurrentTargetSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTSETTINGS_T1096092444_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MOBILEINPUT_T2025745297_H
#define MOBILEINPUT_T2025745297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t2025745297  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T2025745297_H
#ifndef STANDALONEINPUT_T1343950252_H
#define STANDALONEINPUT_T1343950252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t1343950252  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T1343950252_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MODIFIERBASE_T1320963181_H
#define MODIFIERBASE_T1320963181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ModifierBase
struct  ModifierBase_t1320963181  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.ModifierBase::Active
	bool ___Active_2;

public:
	inline static int32_t get_offset_of_Active_2() { return static_cast<int32_t>(offsetof(ModifierBase_t1320963181, ___Active_2)); }
	inline bool get_Active_2() const { return ___Active_2; }
	inline bool* get_address_of_Active_2() { return &___Active_2; }
	inline void set_Active_2(bool value)
	{
		___Active_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERBASE_T1320963181_H
#ifndef MODIFIERSTACKBASE_T4180154112_H
#define MODIFIERSTACKBASE_T4180154112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ModifierStackBase
struct  ModifierStackBase_t4180154112  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERSTACKBASE_T4180154112_H
#ifndef U3CDOREQUESTU3EC__ITERATOR0_T1782862350_H
#define U3CDOREQUESTU3EC__ITERATOR0_T1782862350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0
struct  U3CDoRequestU3Ec__Iterator0_t1782862350  : public RuntimeObject
{
public:
	// System.DateTime Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0::<timeout>__0
	DateTime_t3738529785  ___U3CtimeoutU3E__0_0;
	// System.Boolean Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0::<didTimeout>__0
	bool ___U3CdidTimeoutU3E__0_1;
	// Mapbox.Platform.Response Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0::<response>__1
	Response_t1055672877 * ___U3CresponseU3E__1_2;
	// Mapbox.Unity.Utilities.HTTPRequest Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0::$this
	HTTPRequest_t539111708 * ___U24this_3;
	// System.Object Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Mapbox.Unity.Utilities.HTTPRequest/<DoRequest>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtimeoutU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoRequestU3Ec__Iterator0_t1782862350, ___U3CtimeoutU3E__0_0)); }
	inline DateTime_t3738529785  get_U3CtimeoutU3E__0_0() const { return ___U3CtimeoutU3E__0_0; }
	inline DateTime_t3738529785 * get_address_of_U3CtimeoutU3E__0_0() { return &___U3CtimeoutU3E__0_0; }
	inline void set_U3CtimeoutU3E__0_0(DateTime_t3738529785  value)
	{
		___U3CtimeoutU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdidTimeoutU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoRequestU3Ec__Iterator0_t1782862350, ___U3CdidTimeoutU3E__0_1)); }
	inline bool get_U3CdidTimeoutU3E__0_1() const { return ___U3CdidTimeoutU3E__0_1; }
	inline bool* get_address_of_U3CdidTimeoutU3E__0_1() { return &___U3CdidTimeoutU3E__0_1; }
	inline void set_U3CdidTimeoutU3E__0_1(bool value)
	{
		___U3CdidTimeoutU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CresponseU3E__1_2() { return static_cast<int32_t>(offsetof(U3CDoRequestU3Ec__Iterator0_t1782862350, ___U3CresponseU3E__1_2)); }
	inline Response_t1055672877 * get_U3CresponseU3E__1_2() const { return ___U3CresponseU3E__1_2; }
	inline Response_t1055672877 ** get_address_of_U3CresponseU3E__1_2() { return &___U3CresponseU3E__1_2; }
	inline void set_U3CresponseU3E__1_2(Response_t1055672877 * value)
	{
		___U3CresponseU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDoRequestU3Ec__Iterator0_t1782862350, ___U24this_3)); }
	inline HTTPRequest_t539111708 * get_U24this_3() const { return ___U24this_3; }
	inline HTTPRequest_t539111708 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(HTTPRequest_t539111708 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDoRequestU3Ec__Iterator0_t1782862350, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDoRequestU3Ec__Iterator0_t1782862350, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDoRequestU3Ec__Iterator0_t1782862350, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOREQUESTU3EC__ITERATOR0_T1782862350_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GAMEOBJECTMODIFIER_T609190006_H
#define GAMEOBJECTMODIFIER_T609190006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier
struct  GameObjectModifier_t609190006  : public ModifierBase_t1320963181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTMODIFIER_T609190006_H
#ifndef MODIFIERSTACK_T3760196006_H
#define MODIFIERSTACK_T3760196006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack
struct  ModifierStack_t3760196006  : public ModifierStackBase_t4180154112
{
public:
	// Mapbox.Unity.MeshGeneration.Modifiers.PositionTargetType Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack::_moveFeaturePositionTo
	int32_t ____moveFeaturePositionTo_2;
	// UnityEngine.Vector3 Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack::_center
	Vector3_t3722313464  ____center_3;
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack::vertexIndex
	int32_t ___vertexIndex_4;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier> Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack::MeshModifiers
	List_1_t149922598 * ___MeshModifiers_5;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier> Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack::GoModifiers
	List_1_t2081264748 * ___GoModifiers_6;

public:
	inline static int32_t get_offset_of__moveFeaturePositionTo_2() { return static_cast<int32_t>(offsetof(ModifierStack_t3760196006, ____moveFeaturePositionTo_2)); }
	inline int32_t get__moveFeaturePositionTo_2() const { return ____moveFeaturePositionTo_2; }
	inline int32_t* get_address_of__moveFeaturePositionTo_2() { return &____moveFeaturePositionTo_2; }
	inline void set__moveFeaturePositionTo_2(int32_t value)
	{
		____moveFeaturePositionTo_2 = value;
	}

	inline static int32_t get_offset_of__center_3() { return static_cast<int32_t>(offsetof(ModifierStack_t3760196006, ____center_3)); }
	inline Vector3_t3722313464  get__center_3() const { return ____center_3; }
	inline Vector3_t3722313464 * get_address_of__center_3() { return &____center_3; }
	inline void set__center_3(Vector3_t3722313464  value)
	{
		____center_3 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_4() { return static_cast<int32_t>(offsetof(ModifierStack_t3760196006, ___vertexIndex_4)); }
	inline int32_t get_vertexIndex_4() const { return ___vertexIndex_4; }
	inline int32_t* get_address_of_vertexIndex_4() { return &___vertexIndex_4; }
	inline void set_vertexIndex_4(int32_t value)
	{
		___vertexIndex_4 = value;
	}

	inline static int32_t get_offset_of_MeshModifiers_5() { return static_cast<int32_t>(offsetof(ModifierStack_t3760196006, ___MeshModifiers_5)); }
	inline List_1_t149922598 * get_MeshModifiers_5() const { return ___MeshModifiers_5; }
	inline List_1_t149922598 ** get_address_of_MeshModifiers_5() { return &___MeshModifiers_5; }
	inline void set_MeshModifiers_5(List_1_t149922598 * value)
	{
		___MeshModifiers_5 = value;
		Il2CppCodeGenWriteBarrier((&___MeshModifiers_5), value);
	}

	inline static int32_t get_offset_of_GoModifiers_6() { return static_cast<int32_t>(offsetof(ModifierStack_t3760196006, ___GoModifiers_6)); }
	inline List_1_t2081264748 * get_GoModifiers_6() const { return ___GoModifiers_6; }
	inline List_1_t2081264748 ** get_address_of_GoModifiers_6() { return &___GoModifiers_6; }
	inline void set_GoModifiers_6(List_1_t2081264748 * value)
	{
		___GoModifiers_6 = value;
		Il2CppCodeGenWriteBarrier((&___GoModifiers_6), value);
	}
};

struct ModifierStack_t3760196006_StaticFields
{
public:
	// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier,System.Boolean> Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack::<>f__am$cache0
	Func_2_t1030805891 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier,System.Boolean> Mapbox.Unity.MeshGeneration.Modifiers.ModifierStack::<>f__am$cache1
	Func_2_t4044917861 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(ModifierStack_t3760196006_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t1030805891 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t1030805891 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t1030805891 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(ModifierStack_t3760196006_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t4044917861 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t4044917861 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t4044917861 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIERSTACK_T3760196006_H
#ifndef MERGEDMODIFIERSTACK_T694552105_H
#define MERGEDMODIFIERSTACK_T694552105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack
struct  MergedModifierStack_t694552105  : public ModifierStackBase_t4180154112
{
public:
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::MeshModifiers
	List_1_t149922598 * ___MeshModifiers_2;
	// System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::GoModifiers
	List_1_t2081264748 * ___GoModifiers_3;
	// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,System.Int32> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::_cacheVertexCount
	Dictionary_2_t265003808 * ____cacheVertexCount_4;
	// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,System.Collections.Generic.List`1<Mapbox.Unity.MeshGeneration.Data.MeshData>> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::_cached
	Dictionary_2_t839660848 * ____cached_5;
	// System.Collections.Generic.Dictionary`2<Mapbox.Unity.MeshGeneration.Data.UnityTile,System.Int32> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::_buildingCount
	Dictionary_2_t265003808 * ____buildingCount_6;

public:
	inline static int32_t get_offset_of_MeshModifiers_2() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105, ___MeshModifiers_2)); }
	inline List_1_t149922598 * get_MeshModifiers_2() const { return ___MeshModifiers_2; }
	inline List_1_t149922598 ** get_address_of_MeshModifiers_2() { return &___MeshModifiers_2; }
	inline void set_MeshModifiers_2(List_1_t149922598 * value)
	{
		___MeshModifiers_2 = value;
		Il2CppCodeGenWriteBarrier((&___MeshModifiers_2), value);
	}

	inline static int32_t get_offset_of_GoModifiers_3() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105, ___GoModifiers_3)); }
	inline List_1_t2081264748 * get_GoModifiers_3() const { return ___GoModifiers_3; }
	inline List_1_t2081264748 ** get_address_of_GoModifiers_3() { return &___GoModifiers_3; }
	inline void set_GoModifiers_3(List_1_t2081264748 * value)
	{
		___GoModifiers_3 = value;
		Il2CppCodeGenWriteBarrier((&___GoModifiers_3), value);
	}

	inline static int32_t get_offset_of__cacheVertexCount_4() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105, ____cacheVertexCount_4)); }
	inline Dictionary_2_t265003808 * get__cacheVertexCount_4() const { return ____cacheVertexCount_4; }
	inline Dictionary_2_t265003808 ** get_address_of__cacheVertexCount_4() { return &____cacheVertexCount_4; }
	inline void set__cacheVertexCount_4(Dictionary_2_t265003808 * value)
	{
		____cacheVertexCount_4 = value;
		Il2CppCodeGenWriteBarrier((&____cacheVertexCount_4), value);
	}

	inline static int32_t get_offset_of__cached_5() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105, ____cached_5)); }
	inline Dictionary_2_t839660848 * get__cached_5() const { return ____cached_5; }
	inline Dictionary_2_t839660848 ** get_address_of__cached_5() { return &____cached_5; }
	inline void set__cached_5(Dictionary_2_t839660848 * value)
	{
		____cached_5 = value;
		Il2CppCodeGenWriteBarrier((&____cached_5), value);
	}

	inline static int32_t get_offset_of__buildingCount_6() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105, ____buildingCount_6)); }
	inline Dictionary_2_t265003808 * get__buildingCount_6() const { return ____buildingCount_6; }
	inline Dictionary_2_t265003808 ** get_address_of__buildingCount_6() { return &____buildingCount_6; }
	inline void set__buildingCount_6(Dictionary_2_t265003808 * value)
	{
		____buildingCount_6 = value;
		Il2CppCodeGenWriteBarrier((&____buildingCount_6), value);
	}
};

struct MergedModifierStack_t694552105_StaticFields
{
public:
	// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier,System.Boolean> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::<>f__am$cache0
	Func_2_t1030805891 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<Mapbox.Unity.MeshGeneration.Modifiers.GameObjectModifier,System.Boolean> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::<>f__am$cache1
	Func_2_t4044917861 * ___U3CU3Ef__amU24cache1_8;
	// System.Func`2<Mapbox.Unity.MeshGeneration.Data.MeshData,System.Boolean> Mapbox.Unity.MeshGeneration.Modifiers.MergedModifierStack::<>f__am$cache2
	Func_2_t475509844 * ___U3CU3Ef__amU24cache2_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t1030805891 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t1030805891 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t1030805891 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t4044917861 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t4044917861 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t4044917861 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_9() { return static_cast<int32_t>(offsetof(MergedModifierStack_t694552105_StaticFields, ___U3CU3Ef__amU24cache2_9)); }
	inline Func_2_t475509844 * get_U3CU3Ef__amU24cache2_9() const { return ___U3CU3Ef__amU24cache2_9; }
	inline Func_2_t475509844 ** get_address_of_U3CU3Ef__amU24cache2_9() { return &___U3CU3Ef__amU24cache2_9; }
	inline void set_U3CU3Ef__amU24cache2_9(Func_2_t475509844 * value)
	{
		___U3CU3Ef__amU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEDMODIFIERSTACK_T694552105_H
#ifndef MESHMODIFIER_T2972815152_H
#define MESHMODIFIER_T2972815152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.MeshModifier
struct  MeshModifier_t2972815152  : public ModifierBase_t1320963181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHMODIFIER_T2972815152_H
#ifndef FPSCOUNTER_T2351221284_H
#define FPSCOUNTER_T2351221284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t2351221284  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_3;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_4;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_5;
	// UnityEngine.GUIText UnityStandardAssets.Utility.FPSCounter::m_GuiText
	GUIText_t402233326 * ___m_GuiText_7;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_3() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsAccumulator_3)); }
	inline int32_t get_m_FpsAccumulator_3() const { return ___m_FpsAccumulator_3; }
	inline int32_t* get_address_of_m_FpsAccumulator_3() { return &___m_FpsAccumulator_3; }
	inline void set_m_FpsAccumulator_3(int32_t value)
	{
		___m_FpsAccumulator_3 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_4() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsNextPeriod_4)); }
	inline float get_m_FpsNextPeriod_4() const { return ___m_FpsNextPeriod_4; }
	inline float* get_address_of_m_FpsNextPeriod_4() { return &___m_FpsNextPeriod_4; }
	inline void set_m_FpsNextPeriod_4(float value)
	{
		___m_FpsNextPeriod_4 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_5() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_CurrentFps_5)); }
	inline int32_t get_m_CurrentFps_5() const { return ___m_CurrentFps_5; }
	inline int32_t* get_address_of_m_CurrentFps_5() { return &___m_CurrentFps_5; }
	inline void set_m_CurrentFps_5(int32_t value)
	{
		___m_CurrentFps_5 = value;
	}

	inline static int32_t get_offset_of_m_GuiText_7() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_GuiText_7)); }
	inline GUIText_t402233326 * get_m_GuiText_7() const { return ___m_GuiText_7; }
	inline GUIText_t402233326 ** get_address_of_m_GuiText_7() { return &___m_GuiText_7; }
	inline void set_m_GuiText_7(GUIText_t402233326 * value)
	{
		___m_GuiText_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_GuiText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2351221284_H
#ifndef OBJECTRESETTER_T639177103_H
#define OBJECTRESETTER_T639177103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_t639177103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_t3722313464  ___originalPosition_2;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t2301928331  ___originalRotation_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t777473367 * ___originalStructure_4;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_t3916780224 * ___Rigidbody_5;

public:
	inline static int32_t get_offset_of_originalPosition_2() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalPosition_2)); }
	inline Vector3_t3722313464  get_originalPosition_2() const { return ___originalPosition_2; }
	inline Vector3_t3722313464 * get_address_of_originalPosition_2() { return &___originalPosition_2; }
	inline void set_originalPosition_2(Vector3_t3722313464  value)
	{
		___originalPosition_2 = value;
	}

	inline static int32_t get_offset_of_originalRotation_3() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalRotation_3)); }
	inline Quaternion_t2301928331  get_originalRotation_3() const { return ___originalRotation_3; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_3() { return &___originalRotation_3; }
	inline void set_originalRotation_3(Quaternion_t2301928331  value)
	{
		___originalRotation_3 = value;
	}

	inline static int32_t get_offset_of_originalStructure_4() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalStructure_4)); }
	inline List_1_t777473367 * get_originalStructure_4() const { return ___originalStructure_4; }
	inline List_1_t777473367 ** get_address_of_originalStructure_4() { return &___originalStructure_4; }
	inline void set_originalStructure_4(List_1_t777473367 * value)
	{
		___originalStructure_4 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_4), value);
	}

	inline static int32_t get_offset_of_Rigidbody_5() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___Rigidbody_5)); }
	inline Rigidbody_t3916780224 * get_Rigidbody_5() const { return ___Rigidbody_5; }
	inline Rigidbody_t3916780224 ** get_address_of_Rigidbody_5() { return &___Rigidbody_5; }
	inline void set_Rigidbody_5(Rigidbody_t3916780224 * value)
	{
		___Rigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_T639177103_H
#ifndef PARTICLESYSTEMDESTROYER_T558680695_H
#define PARTICLESYSTEMDESTROYER_T558680695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t558680695  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_4;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_5;

public:
	inline static int32_t get_offset_of_minDuration_2() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___minDuration_2)); }
	inline float get_minDuration_2() const { return ___minDuration_2; }
	inline float* get_address_of_minDuration_2() { return &___minDuration_2; }
	inline void set_minDuration_2(float value)
	{
		___minDuration_2 = value;
	}

	inline static int32_t get_offset_of_maxDuration_3() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___maxDuration_3)); }
	inline float get_maxDuration_3() const { return ___maxDuration_3; }
	inline float* get_address_of_maxDuration_3() { return &___maxDuration_3; }
	inline void set_maxDuration_3(float value)
	{
		___maxDuration_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_MaxLifetime_4)); }
	inline float get_m_MaxLifetime_4() const { return ___m_MaxLifetime_4; }
	inline float* get_address_of_m_MaxLifetime_4() { return &___m_MaxLifetime_4; }
	inline void set_m_MaxLifetime_4(float value)
	{
		___m_MaxLifetime_4 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_EarlyStop_5)); }
	inline bool get_m_EarlyStop_5() const { return ___m_EarlyStop_5; }
	inline bool* get_address_of_m_EarlyStop_5() { return &___m_EarlyStop_5; }
	inline void set_m_EarlyStop_5(bool value)
	{
		___m_EarlyStop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T558680695_H
#ifndef PLATFORMSPECIFICCONTENT_T1404549723_H
#define PLATFORMSPECIFICCONTENT_T1404549723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t1404549723  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_t3328599146* ___m_Content_3;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___m_MonoBehaviours_4;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_5;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_2() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_BuildTargetGroup_2)); }
	inline int32_t get_m_BuildTargetGroup_2() const { return ___m_BuildTargetGroup_2; }
	inline int32_t* get_address_of_m_BuildTargetGroup_2() { return &___m_BuildTargetGroup_2; }
	inline void set_m_BuildTargetGroup_2(int32_t value)
	{
		___m_BuildTargetGroup_2 = value;
	}

	inline static int32_t get_offset_of_m_Content_3() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_Content_3)); }
	inline GameObjectU5BU5D_t3328599146* get_m_Content_3() const { return ___m_Content_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_Content_3() { return &___m_Content_3; }
	inline void set_m_Content_3(GameObjectU5BU5D_t3328599146* value)
	{
		___m_Content_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_3), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_MonoBehaviours_4)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_m_MonoBehaviours_4() const { return ___m_MonoBehaviours_4; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_m_MonoBehaviours_4() { return &___m_MonoBehaviours_4; }
	inline void set_m_MonoBehaviours_4(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___m_MonoBehaviours_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_4), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_ChildrenOfThisObject_5)); }
	inline bool get_m_ChildrenOfThisObject_5() const { return ___m_ChildrenOfThisObject_5; }
	inline bool* get_address_of_m_ChildrenOfThisObject_5() { return &___m_ChildrenOfThisObject_5; }
	inline void set_m_ChildrenOfThisObject_5(bool value)
	{
		___m_ChildrenOfThisObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T1404549723_H
#ifndef UVMODIFIER_T1527142922_H
#define UVMODIFIER_T1527142922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.UvModifier
struct  UvModifier_t1527142922  : public MeshModifier_t2972815152
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.UvModifier::UseSatelliteRoof
	bool ___UseSatelliteRoof_3;

public:
	inline static int32_t get_offset_of_UseSatelliteRoof_3() { return static_cast<int32_t>(offsetof(UvModifier_t1527142922, ___UseSatelliteRoof_3)); }
	inline bool get_UseSatelliteRoof_3() const { return ___UseSatelliteRoof_3; }
	inline bool* get_address_of_UseSatelliteRoof_3() { return &___UseSatelliteRoof_3; }
	inline void set_UseSatelliteRoof_3(bool value)
	{
		___UseSatelliteRoof_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UVMODIFIER_T1527142922_H
#ifndef OPENURLONBUTTONCLICK_T1465608780_H
#define OPENURLONBUTTONCLICK_T1465608780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.OpenUrlOnButtonClick
struct  OpenUrlOnButtonClick_t1465608780  : public MonoBehaviour_t3962482529
{
public:
	// System.String Mapbox.Unity.Utilities.OpenUrlOnButtonClick::_url
	String_t* ____url_2;

public:
	inline static int32_t get_offset_of__url_2() { return static_cast<int32_t>(offsetof(OpenUrlOnButtonClick_t1465608780, ____url_2)); }
	inline String_t* get__url_2() const { return ____url_2; }
	inline String_t** get_address_of__url_2() { return &____url_2; }
	inline void set__url_2(String_t* value)
	{
		____url_2 = value;
		Il2CppCodeGenWriteBarrier((&____url_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENURLONBUTTONCLICK_T1465608780_H
#ifndef RUNNABLE_T3171960597_H
#define RUNNABLE_T3171960597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.Runnable
struct  Runnable_t3171960597  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Mapbox.Unity.Utilities.Runnable/Routine> Mapbox.Unity.Utilities.Runnable::m_Routines
	Dictionary_2_t4036641988 * ___m_Routines_2;
	// System.Int32 Mapbox.Unity.Utilities.Runnable::m_NextRoutineId
	int32_t ___m_NextRoutineId_3;

public:
	inline static int32_t get_offset_of_m_Routines_2() { return static_cast<int32_t>(offsetof(Runnable_t3171960597, ___m_Routines_2)); }
	inline Dictionary_2_t4036641988 * get_m_Routines_2() const { return ___m_Routines_2; }
	inline Dictionary_2_t4036641988 ** get_address_of_m_Routines_2() { return &___m_Routines_2; }
	inline void set_m_Routines_2(Dictionary_2_t4036641988 * value)
	{
		___m_Routines_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Routines_2), value);
	}

	inline static int32_t get_offset_of_m_NextRoutineId_3() { return static_cast<int32_t>(offsetof(Runnable_t3171960597, ___m_NextRoutineId_3)); }
	inline int32_t get_m_NextRoutineId_3() const { return ___m_NextRoutineId_3; }
	inline int32_t* get_address_of_m_NextRoutineId_3() { return &___m_NextRoutineId_3; }
	inline void set_m_NextRoutineId_3(int32_t value)
	{
		___m_NextRoutineId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNABLE_T3171960597_H
#ifndef TELEMETRYCONFIGURATIONBUTTON_T4275913055_H
#define TELEMETRYCONFIGURATIONBUTTON_T4275913055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.Utilities.TelemetryConfigurationButton
struct  TelemetryConfigurationButton_t4275913055  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Mapbox.Unity.Utilities.TelemetryConfigurationButton::_booleanValue
	bool ____booleanValue_2;

public:
	inline static int32_t get_offset_of__booleanValue_2() { return static_cast<int32_t>(offsetof(TelemetryConfigurationButton_t4275913055, ____booleanValue_2)); }
	inline bool get__booleanValue_2() const { return ____booleanValue_2; }
	inline bool* get_address_of__booleanValue_2() { return &____booleanValue_2; }
	inline void set__booleanValue_2(bool value)
	{
		____booleanValue_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELEMETRYCONFIGURATIONBUTTON_T4275913055_H
#ifndef MAPCONTROLLER_T3762646607_H
#define MAPCONTROLLER_T3762646607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapController
struct  MapController_t3762646607  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SceneManagement.Scene MapController::scene
	Scene_t2348375561  ___scene_2;

public:
	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(MapController_t3762646607, ___scene_2)); }
	inline Scene_t2348375561  get_scene_2() const { return ___scene_2; }
	inline Scene_t2348375561 * get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(Scene_t2348375561  value)
	{
		___scene_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPCONTROLLER_T3762646607_H
#ifndef MAPPAGECONTROLLER_T3109321034_H
#define MAPPAGECONTROLLER_T3109321034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapPageController
struct  MapPageController_t3109321034  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Map.AbstractMap MapPageController::_map
	AbstractMap_t3082917158 * ____map_2;
	// UnityEngine.SceneManagement.Scene MapPageController::scene
	Scene_t2348375561  ___scene_3;
	// SimpleObjectPool MapPageController::POIObjectPool
	SimpleObjectPool_t1028341060 * ___POIObjectPool_4;
	// DataController MapPageController::dataController
	DataController_t353634109 * ___dataController_5;
	// SiteData[] MapPageController::sitePool
	SiteDataU5BU5D_t2647968276* ___sitePool_6;
	// System.Int32 MapPageController::siteIndex
	int32_t ___siteIndex_7;
	// System.Int32 MapPageController::siteID
	int32_t ___siteID_8;
	// System.Double MapPageController::lat
	double ___lat_9;
	// System.Double MapPageController::lng
	double ___lng_10;
	// UnityEngine.Vector3 MapPageController::_targetPosition
	Vector3_t3722313464  ____targetPosition_11;
	// System.Boolean MapPageController::_isInitialized
	bool ____isInitialized_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MapPageController::POIGameObjects
	List_1_t2585711361 * ___POIGameObjects_13;

public:
	inline static int32_t get_offset_of__map_2() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ____map_2)); }
	inline AbstractMap_t3082917158 * get__map_2() const { return ____map_2; }
	inline AbstractMap_t3082917158 ** get_address_of__map_2() { return &____map_2; }
	inline void set__map_2(AbstractMap_t3082917158 * value)
	{
		____map_2 = value;
		Il2CppCodeGenWriteBarrier((&____map_2), value);
	}

	inline static int32_t get_offset_of_scene_3() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___scene_3)); }
	inline Scene_t2348375561  get_scene_3() const { return ___scene_3; }
	inline Scene_t2348375561 * get_address_of_scene_3() { return &___scene_3; }
	inline void set_scene_3(Scene_t2348375561  value)
	{
		___scene_3 = value;
	}

	inline static int32_t get_offset_of_POIObjectPool_4() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___POIObjectPool_4)); }
	inline SimpleObjectPool_t1028341060 * get_POIObjectPool_4() const { return ___POIObjectPool_4; }
	inline SimpleObjectPool_t1028341060 ** get_address_of_POIObjectPool_4() { return &___POIObjectPool_4; }
	inline void set_POIObjectPool_4(SimpleObjectPool_t1028341060 * value)
	{
		___POIObjectPool_4 = value;
		Il2CppCodeGenWriteBarrier((&___POIObjectPool_4), value);
	}

	inline static int32_t get_offset_of_dataController_5() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___dataController_5)); }
	inline DataController_t353634109 * get_dataController_5() const { return ___dataController_5; }
	inline DataController_t353634109 ** get_address_of_dataController_5() { return &___dataController_5; }
	inline void set_dataController_5(DataController_t353634109 * value)
	{
		___dataController_5 = value;
		Il2CppCodeGenWriteBarrier((&___dataController_5), value);
	}

	inline static int32_t get_offset_of_sitePool_6() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___sitePool_6)); }
	inline SiteDataU5BU5D_t2647968276* get_sitePool_6() const { return ___sitePool_6; }
	inline SiteDataU5BU5D_t2647968276** get_address_of_sitePool_6() { return &___sitePool_6; }
	inline void set_sitePool_6(SiteDataU5BU5D_t2647968276* value)
	{
		___sitePool_6 = value;
		Il2CppCodeGenWriteBarrier((&___sitePool_6), value);
	}

	inline static int32_t get_offset_of_siteIndex_7() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___siteIndex_7)); }
	inline int32_t get_siteIndex_7() const { return ___siteIndex_7; }
	inline int32_t* get_address_of_siteIndex_7() { return &___siteIndex_7; }
	inline void set_siteIndex_7(int32_t value)
	{
		___siteIndex_7 = value;
	}

	inline static int32_t get_offset_of_siteID_8() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___siteID_8)); }
	inline int32_t get_siteID_8() const { return ___siteID_8; }
	inline int32_t* get_address_of_siteID_8() { return &___siteID_8; }
	inline void set_siteID_8(int32_t value)
	{
		___siteID_8 = value;
	}

	inline static int32_t get_offset_of_lat_9() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___lat_9)); }
	inline double get_lat_9() const { return ___lat_9; }
	inline double* get_address_of_lat_9() { return &___lat_9; }
	inline void set_lat_9(double value)
	{
		___lat_9 = value;
	}

	inline static int32_t get_offset_of_lng_10() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___lng_10)); }
	inline double get_lng_10() const { return ___lng_10; }
	inline double* get_address_of_lng_10() { return &___lng_10; }
	inline void set_lng_10(double value)
	{
		___lng_10 = value;
	}

	inline static int32_t get_offset_of__targetPosition_11() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ____targetPosition_11)); }
	inline Vector3_t3722313464  get__targetPosition_11() const { return ____targetPosition_11; }
	inline Vector3_t3722313464 * get_address_of__targetPosition_11() { return &____targetPosition_11; }
	inline void set__targetPosition_11(Vector3_t3722313464  value)
	{
		____targetPosition_11 = value;
	}

	inline static int32_t get_offset_of__isInitialized_12() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ____isInitialized_12)); }
	inline bool get__isInitialized_12() const { return ____isInitialized_12; }
	inline bool* get_address_of__isInitialized_12() { return &____isInitialized_12; }
	inline void set__isInitialized_12(bool value)
	{
		____isInitialized_12 = value;
	}

	inline static int32_t get_offset_of_POIGameObjects_13() { return static_cast<int32_t>(offsetof(MapPageController_t3109321034, ___POIGameObjects_13)); }
	inline List_1_t2585711361 * get_POIGameObjects_13() const { return ___POIGameObjects_13; }
	inline List_1_t2585711361 ** get_address_of_POIGameObjects_13() { return &___POIGameObjects_13; }
	inline void set_POIGameObjects_13(List_1_t2585711361 * value)
	{
		___POIGameObjects_13 = value;
		Il2CppCodeGenWriteBarrier((&___POIGameObjects_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPAGECONTROLLER_T3109321034_H
#ifndef MARKERPOSITION_T1625649371_H
#define MARKERPOSITION_T1625649371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerPosition
struct  MarkerPosition_t1625649371  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.Map.AbstractMap MarkerPosition::_map
	AbstractMap_t3082917158 * ____map_2;
	// System.String MarkerPosition::_latitudeLongitude
	String_t* ____latitudeLongitude_3;
	// System.Boolean MarkerPosition::_isInitialized
	bool ____isInitialized_4;
	// UnityEngine.Vector3 MarkerPosition::_targetPosition
	Vector3_t3722313464  ____targetPosition_5;

public:
	inline static int32_t get_offset_of__map_2() { return static_cast<int32_t>(offsetof(MarkerPosition_t1625649371, ____map_2)); }
	inline AbstractMap_t3082917158 * get__map_2() const { return ____map_2; }
	inline AbstractMap_t3082917158 ** get_address_of__map_2() { return &____map_2; }
	inline void set__map_2(AbstractMap_t3082917158 * value)
	{
		____map_2 = value;
		Il2CppCodeGenWriteBarrier((&____map_2), value);
	}

	inline static int32_t get_offset_of__latitudeLongitude_3() { return static_cast<int32_t>(offsetof(MarkerPosition_t1625649371, ____latitudeLongitude_3)); }
	inline String_t* get__latitudeLongitude_3() const { return ____latitudeLongitude_3; }
	inline String_t** get_address_of__latitudeLongitude_3() { return &____latitudeLongitude_3; }
	inline void set__latitudeLongitude_3(String_t* value)
	{
		____latitudeLongitude_3 = value;
		Il2CppCodeGenWriteBarrier((&____latitudeLongitude_3), value);
	}

	inline static int32_t get_offset_of__isInitialized_4() { return static_cast<int32_t>(offsetof(MarkerPosition_t1625649371, ____isInitialized_4)); }
	inline bool get__isInitialized_4() const { return ____isInitialized_4; }
	inline bool* get_address_of__isInitialized_4() { return &____isInitialized_4; }
	inline void set__isInitialized_4(bool value)
	{
		____isInitialized_4 = value;
	}

	inline static int32_t get_offset_of__targetPosition_5() { return static_cast<int32_t>(offsetof(MarkerPosition_t1625649371, ____targetPosition_5)); }
	inline Vector3_t3722313464  get__targetPosition_5() const { return ____targetPosition_5; }
	inline Vector3_t3722313464 * get_address_of__targetPosition_5() { return &____targetPosition_5; }
	inline void set__targetPosition_5(Vector3_t3722313464  value)
	{
		____targetPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERPOSITION_T1625649371_H
#ifndef FIRSTPERSONCONTROLLER_T2020989554_H
#define FIRSTPERSONCONTROLLER_T2020989554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.FirstPersonController
struct  FirstPersonController_t2020989554  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_IsWalking
	bool ___m_IsWalking_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_WalkSpeed
	float ___m_WalkSpeed_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunSpeed
	float ___m_RunSpeed_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunstepLenghten
	float ___m_RunstepLenghten_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSpeed
	float ___m_JumpSpeed_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StickToGroundForce
	float ___m_StickToGroundForce_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_GravityMultiplier
	float ___m_GravityMultiplier_8;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MouseLook
	MouseLook_t2859678661 * ___m_MouseLook_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseFovKick
	bool ___m_UseFovKick_10;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FovKick
	FOVKick_t120370150 * ___m_FovKick_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseHeadBob
	bool ___m_UseHeadBob_12;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_HeadBob
	CurveControlledBob_t2679313829 * ___m_HeadBob_13;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpBob
	LerpControlledBob_t1895875871 * ___m_JumpBob_14;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepInterval
	float ___m_StepInterval_15;
	// UnityEngine.AudioClip[] UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FootstepSounds
	AudioClipU5BU5D_t143221404* ___m_FootstepSounds_16;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSound
	AudioClip_t3680889665 * ___m_JumpSound_17;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_LandSound
	AudioClip_t3680889665 * ___m_LandSound_18;
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Camera
	Camera_t4157153871 * ___m_Camera_19;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jump
	bool ___m_Jump_20;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_YRotation
	float ___m_YRotation_21;
	// UnityEngine.Vector2 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Input
	Vector2_t2156229523  ___m_Input_22;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MoveDir
	Vector3_t3722313464  ___m_MoveDir_23;
	// UnityEngine.CharacterController UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CharacterController
	CharacterController_t1138636865 * ___m_CharacterController_24;
	// UnityEngine.CollisionFlags UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CollisionFlags
	int32_t ___m_CollisionFlags_25;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_26;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_27;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepCycle
	float ___m_StepCycle_28;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_NextStep
	float ___m_NextStep_29;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jumping
	bool ___m_Jumping_30;
	// UnityEngine.AudioSource UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_AudioSource
	AudioSource_t3935305588 * ___m_AudioSource_31;

public:
	inline static int32_t get_offset_of_m_IsWalking_2() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_IsWalking_2)); }
	inline bool get_m_IsWalking_2() const { return ___m_IsWalking_2; }
	inline bool* get_address_of_m_IsWalking_2() { return &___m_IsWalking_2; }
	inline void set_m_IsWalking_2(bool value)
	{
		___m_IsWalking_2 = value;
	}

	inline static int32_t get_offset_of_m_WalkSpeed_3() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_WalkSpeed_3)); }
	inline float get_m_WalkSpeed_3() const { return ___m_WalkSpeed_3; }
	inline float* get_address_of_m_WalkSpeed_3() { return &___m_WalkSpeed_3; }
	inline void set_m_WalkSpeed_3(float value)
	{
		___m_WalkSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_RunSpeed_4() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_RunSpeed_4)); }
	inline float get_m_RunSpeed_4() const { return ___m_RunSpeed_4; }
	inline float* get_address_of_m_RunSpeed_4() { return &___m_RunSpeed_4; }
	inline void set_m_RunSpeed_4(float value)
	{
		___m_RunSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_RunstepLenghten_5() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_RunstepLenghten_5)); }
	inline float get_m_RunstepLenghten_5() const { return ___m_RunstepLenghten_5; }
	inline float* get_address_of_m_RunstepLenghten_5() { return &___m_RunstepLenghten_5; }
	inline void set_m_RunstepLenghten_5(float value)
	{
		___m_RunstepLenghten_5 = value;
	}

	inline static int32_t get_offset_of_m_JumpSpeed_6() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpSpeed_6)); }
	inline float get_m_JumpSpeed_6() const { return ___m_JumpSpeed_6; }
	inline float* get_address_of_m_JumpSpeed_6() { return &___m_JumpSpeed_6; }
	inline void set_m_JumpSpeed_6(float value)
	{
		___m_JumpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_StickToGroundForce_7() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StickToGroundForce_7)); }
	inline float get_m_StickToGroundForce_7() const { return ___m_StickToGroundForce_7; }
	inline float* get_address_of_m_StickToGroundForce_7() { return &___m_StickToGroundForce_7; }
	inline void set_m_StickToGroundForce_7(float value)
	{
		___m_StickToGroundForce_7 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_8() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_GravityMultiplier_8)); }
	inline float get_m_GravityMultiplier_8() const { return ___m_GravityMultiplier_8; }
	inline float* get_address_of_m_GravityMultiplier_8() { return &___m_GravityMultiplier_8; }
	inline void set_m_GravityMultiplier_8(float value)
	{
		___m_GravityMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_m_MouseLook_9() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_MouseLook_9)); }
	inline MouseLook_t2859678661 * get_m_MouseLook_9() const { return ___m_MouseLook_9; }
	inline MouseLook_t2859678661 ** get_address_of_m_MouseLook_9() { return &___m_MouseLook_9; }
	inline void set_m_MouseLook_9(MouseLook_t2859678661 * value)
	{
		___m_MouseLook_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseLook_9), value);
	}

	inline static int32_t get_offset_of_m_UseFovKick_10() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_UseFovKick_10)); }
	inline bool get_m_UseFovKick_10() const { return ___m_UseFovKick_10; }
	inline bool* get_address_of_m_UseFovKick_10() { return &___m_UseFovKick_10; }
	inline void set_m_UseFovKick_10(bool value)
	{
		___m_UseFovKick_10 = value;
	}

	inline static int32_t get_offset_of_m_FovKick_11() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_FovKick_11)); }
	inline FOVKick_t120370150 * get_m_FovKick_11() const { return ___m_FovKick_11; }
	inline FOVKick_t120370150 ** get_address_of_m_FovKick_11() { return &___m_FovKick_11; }
	inline void set_m_FovKick_11(FOVKick_t120370150 * value)
	{
		___m_FovKick_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_FovKick_11), value);
	}

	inline static int32_t get_offset_of_m_UseHeadBob_12() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_UseHeadBob_12)); }
	inline bool get_m_UseHeadBob_12() const { return ___m_UseHeadBob_12; }
	inline bool* get_address_of_m_UseHeadBob_12() { return &___m_UseHeadBob_12; }
	inline void set_m_UseHeadBob_12(bool value)
	{
		___m_UseHeadBob_12 = value;
	}

	inline static int32_t get_offset_of_m_HeadBob_13() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_HeadBob_13)); }
	inline CurveControlledBob_t2679313829 * get_m_HeadBob_13() const { return ___m_HeadBob_13; }
	inline CurveControlledBob_t2679313829 ** get_address_of_m_HeadBob_13() { return &___m_HeadBob_13; }
	inline void set_m_HeadBob_13(CurveControlledBob_t2679313829 * value)
	{
		___m_HeadBob_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HeadBob_13), value);
	}

	inline static int32_t get_offset_of_m_JumpBob_14() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpBob_14)); }
	inline LerpControlledBob_t1895875871 * get_m_JumpBob_14() const { return ___m_JumpBob_14; }
	inline LerpControlledBob_t1895875871 ** get_address_of_m_JumpBob_14() { return &___m_JumpBob_14; }
	inline void set_m_JumpBob_14(LerpControlledBob_t1895875871 * value)
	{
		___m_JumpBob_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpBob_14), value);
	}

	inline static int32_t get_offset_of_m_StepInterval_15() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StepInterval_15)); }
	inline float get_m_StepInterval_15() const { return ___m_StepInterval_15; }
	inline float* get_address_of_m_StepInterval_15() { return &___m_StepInterval_15; }
	inline void set_m_StepInterval_15(float value)
	{
		___m_StepInterval_15 = value;
	}

	inline static int32_t get_offset_of_m_FootstepSounds_16() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_FootstepSounds_16)); }
	inline AudioClipU5BU5D_t143221404* get_m_FootstepSounds_16() const { return ___m_FootstepSounds_16; }
	inline AudioClipU5BU5D_t143221404** get_address_of_m_FootstepSounds_16() { return &___m_FootstepSounds_16; }
	inline void set_m_FootstepSounds_16(AudioClipU5BU5D_t143221404* value)
	{
		___m_FootstepSounds_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_FootstepSounds_16), value);
	}

	inline static int32_t get_offset_of_m_JumpSound_17() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpSound_17)); }
	inline AudioClip_t3680889665 * get_m_JumpSound_17() const { return ___m_JumpSound_17; }
	inline AudioClip_t3680889665 ** get_address_of_m_JumpSound_17() { return &___m_JumpSound_17; }
	inline void set_m_JumpSound_17(AudioClip_t3680889665 * value)
	{
		___m_JumpSound_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpSound_17), value);
	}

	inline static int32_t get_offset_of_m_LandSound_18() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_LandSound_18)); }
	inline AudioClip_t3680889665 * get_m_LandSound_18() const { return ___m_LandSound_18; }
	inline AudioClip_t3680889665 ** get_address_of_m_LandSound_18() { return &___m_LandSound_18; }
	inline void set_m_LandSound_18(AudioClip_t3680889665 * value)
	{
		___m_LandSound_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_LandSound_18), value);
	}

	inline static int32_t get_offset_of_m_Camera_19() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Camera_19)); }
	inline Camera_t4157153871 * get_m_Camera_19() const { return ___m_Camera_19; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_19() { return &___m_Camera_19; }
	inline void set_m_Camera_19(Camera_t4157153871 * value)
	{
		___m_Camera_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_19), value);
	}

	inline static int32_t get_offset_of_m_Jump_20() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Jump_20)); }
	inline bool get_m_Jump_20() const { return ___m_Jump_20; }
	inline bool* get_address_of_m_Jump_20() { return &___m_Jump_20; }
	inline void set_m_Jump_20(bool value)
	{
		___m_Jump_20 = value;
	}

	inline static int32_t get_offset_of_m_YRotation_21() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_YRotation_21)); }
	inline float get_m_YRotation_21() const { return ___m_YRotation_21; }
	inline float* get_address_of_m_YRotation_21() { return &___m_YRotation_21; }
	inline void set_m_YRotation_21(float value)
	{
		___m_YRotation_21 = value;
	}

	inline static int32_t get_offset_of_m_Input_22() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Input_22)); }
	inline Vector2_t2156229523  get_m_Input_22() const { return ___m_Input_22; }
	inline Vector2_t2156229523 * get_address_of_m_Input_22() { return &___m_Input_22; }
	inline void set_m_Input_22(Vector2_t2156229523  value)
	{
		___m_Input_22 = value;
	}

	inline static int32_t get_offset_of_m_MoveDir_23() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_MoveDir_23)); }
	inline Vector3_t3722313464  get_m_MoveDir_23() const { return ___m_MoveDir_23; }
	inline Vector3_t3722313464 * get_address_of_m_MoveDir_23() { return &___m_MoveDir_23; }
	inline void set_m_MoveDir_23(Vector3_t3722313464  value)
	{
		___m_MoveDir_23 = value;
	}

	inline static int32_t get_offset_of_m_CharacterController_24() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_CharacterController_24)); }
	inline CharacterController_t1138636865 * get_m_CharacterController_24() const { return ___m_CharacterController_24; }
	inline CharacterController_t1138636865 ** get_address_of_m_CharacterController_24() { return &___m_CharacterController_24; }
	inline void set_m_CharacterController_24(CharacterController_t1138636865 * value)
	{
		___m_CharacterController_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterController_24), value);
	}

	inline static int32_t get_offset_of_m_CollisionFlags_25() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_CollisionFlags_25)); }
	inline int32_t get_m_CollisionFlags_25() const { return ___m_CollisionFlags_25; }
	inline int32_t* get_address_of_m_CollisionFlags_25() { return &___m_CollisionFlags_25; }
	inline void set_m_CollisionFlags_25(int32_t value)
	{
		___m_CollisionFlags_25 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_26() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_PreviouslyGrounded_26)); }
	inline bool get_m_PreviouslyGrounded_26() const { return ___m_PreviouslyGrounded_26; }
	inline bool* get_address_of_m_PreviouslyGrounded_26() { return &___m_PreviouslyGrounded_26; }
	inline void set_m_PreviouslyGrounded_26(bool value)
	{
		___m_PreviouslyGrounded_26 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_27() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_OriginalCameraPosition_27)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_27() const { return ___m_OriginalCameraPosition_27; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_27() { return &___m_OriginalCameraPosition_27; }
	inline void set_m_OriginalCameraPosition_27(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_27 = value;
	}

	inline static int32_t get_offset_of_m_StepCycle_28() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StepCycle_28)); }
	inline float get_m_StepCycle_28() const { return ___m_StepCycle_28; }
	inline float* get_address_of_m_StepCycle_28() { return &___m_StepCycle_28; }
	inline void set_m_StepCycle_28(float value)
	{
		___m_StepCycle_28 = value;
	}

	inline static int32_t get_offset_of_m_NextStep_29() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_NextStep_29)); }
	inline float get_m_NextStep_29() const { return ___m_NextStep_29; }
	inline float* get_address_of_m_NextStep_29() { return &___m_NextStep_29; }
	inline void set_m_NextStep_29(float value)
	{
		___m_NextStep_29 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_30() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Jumping_30)); }
	inline bool get_m_Jumping_30() const { return ___m_Jumping_30; }
	inline bool* get_address_of_m_Jumping_30() { return &___m_Jumping_30; }
	inline void set_m_Jumping_30(bool value)
	{
		___m_Jumping_30 = value;
	}

	inline static int32_t get_offset_of_m_AudioSource_31() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_AudioSource_31)); }
	inline AudioSource_t3935305588 * get_m_AudioSource_31() const { return ___m_AudioSource_31; }
	inline AudioSource_t3935305588 ** get_address_of_m_AudioSource_31() { return &___m_AudioSource_31; }
	inline void set_m_AudioSource_31(AudioSource_t3935305588 * value)
	{
		___m_AudioSource_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioSource_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTPERSONCONTROLLER_T2020989554_H
#ifndef SNAPTERRAINRAYCASTMODIFIER_T3769335075_H
#define SNAPTERRAINRAYCASTMODIFIER_T3769335075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.SnapTerrainRaycastModifier
struct  SnapTerrainRaycastModifier_t3769335075  : public MeshModifier_t2972815152
{
public:
	// UnityEngine.LayerMask Mapbox.Unity.MeshGeneration.Modifiers.SnapTerrainRaycastModifier::_terrainMask
	LayerMask_t3493934918  ____terrainMask_4;

public:
	inline static int32_t get_offset_of__terrainMask_4() { return static_cast<int32_t>(offsetof(SnapTerrainRaycastModifier_t3769335075, ____terrainMask_4)); }
	inline LayerMask_t3493934918  get__terrainMask_4() const { return ____terrainMask_4; }
	inline LayerMask_t3493934918 * get_address_of__terrainMask_4() { return &____terrainMask_4; }
	inline void set__terrainMask_4(LayerMask_t3493934918  value)
	{
		____terrainMask_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPTERRAINRAYCASTMODIFIER_T3769335075_H
#ifndef TEXTUREMODIFIER_T2005389549_H
#define TEXTUREMODIFIER_T2005389549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.TextureModifier
struct  TextureModifier_t2005389549  : public GameObjectModifier_t609190006
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.TextureModifier::_textureTop
	bool ____textureTop_3;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.TextureModifier::_useSatelliteTexture
	bool ____useSatelliteTexture_4;
	// UnityEngine.Material[] Mapbox.Unity.MeshGeneration.Modifiers.TextureModifier::_topMaterials
	MaterialU5BU5D_t561872642* ____topMaterials_5;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.TextureModifier::_textureSides
	bool ____textureSides_6;
	// UnityEngine.Material[] Mapbox.Unity.MeshGeneration.Modifiers.TextureModifier::_sideMaterials
	MaterialU5BU5D_t561872642* ____sideMaterials_7;

public:
	inline static int32_t get_offset_of__textureTop_3() { return static_cast<int32_t>(offsetof(TextureModifier_t2005389549, ____textureTop_3)); }
	inline bool get__textureTop_3() const { return ____textureTop_3; }
	inline bool* get_address_of__textureTop_3() { return &____textureTop_3; }
	inline void set__textureTop_3(bool value)
	{
		____textureTop_3 = value;
	}

	inline static int32_t get_offset_of__useSatelliteTexture_4() { return static_cast<int32_t>(offsetof(TextureModifier_t2005389549, ____useSatelliteTexture_4)); }
	inline bool get__useSatelliteTexture_4() const { return ____useSatelliteTexture_4; }
	inline bool* get_address_of__useSatelliteTexture_4() { return &____useSatelliteTexture_4; }
	inline void set__useSatelliteTexture_4(bool value)
	{
		____useSatelliteTexture_4 = value;
	}

	inline static int32_t get_offset_of__topMaterials_5() { return static_cast<int32_t>(offsetof(TextureModifier_t2005389549, ____topMaterials_5)); }
	inline MaterialU5BU5D_t561872642* get__topMaterials_5() const { return ____topMaterials_5; }
	inline MaterialU5BU5D_t561872642** get_address_of__topMaterials_5() { return &____topMaterials_5; }
	inline void set__topMaterials_5(MaterialU5BU5D_t561872642* value)
	{
		____topMaterials_5 = value;
		Il2CppCodeGenWriteBarrier((&____topMaterials_5), value);
	}

	inline static int32_t get_offset_of__textureSides_6() { return static_cast<int32_t>(offsetof(TextureModifier_t2005389549, ____textureSides_6)); }
	inline bool get__textureSides_6() const { return ____textureSides_6; }
	inline bool* get_address_of__textureSides_6() { return &____textureSides_6; }
	inline void set__textureSides_6(bool value)
	{
		____textureSides_6 = value;
	}

	inline static int32_t get_offset_of__sideMaterials_7() { return static_cast<int32_t>(offsetof(TextureModifier_t2005389549, ____sideMaterials_7)); }
	inline MaterialU5BU5D_t561872642* get__sideMaterials_7() const { return ____sideMaterials_7; }
	inline MaterialU5BU5D_t561872642** get_address_of__sideMaterials_7() { return &____sideMaterials_7; }
	inline void set__sideMaterials_7(MaterialU5BU5D_t561872642* value)
	{
		____sideMaterials_7 = value;
		Il2CppCodeGenWriteBarrier((&____sideMaterials_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMODIFIER_T2005389549_H
#ifndef TEXTUREMONOBEHAVIOURMODIFIER_T899798732_H
#define TEXTUREMONOBEHAVIOURMODIFIER_T899798732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.TextureMonoBehaviourModifier
struct  TextureMonoBehaviourModifier_t899798732  : public GameObjectModifier_t609190006
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.TextureMonoBehaviourModifier::_textureTop
	bool ____textureTop_3;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.TextureMonoBehaviourModifier::_useSatelliteTexture
	bool ____useSatelliteTexture_4;
	// UnityEngine.Material[] Mapbox.Unity.MeshGeneration.Modifiers.TextureMonoBehaviourModifier::_topMaterials
	MaterialU5BU5D_t561872642* ____topMaterials_5;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.TextureMonoBehaviourModifier::_textureSides
	bool ____textureSides_6;
	// UnityEngine.Material[] Mapbox.Unity.MeshGeneration.Modifiers.TextureMonoBehaviourModifier::_sideMaterials
	MaterialU5BU5D_t561872642* ____sideMaterials_7;

public:
	inline static int32_t get_offset_of__textureTop_3() { return static_cast<int32_t>(offsetof(TextureMonoBehaviourModifier_t899798732, ____textureTop_3)); }
	inline bool get__textureTop_3() const { return ____textureTop_3; }
	inline bool* get_address_of__textureTop_3() { return &____textureTop_3; }
	inline void set__textureTop_3(bool value)
	{
		____textureTop_3 = value;
	}

	inline static int32_t get_offset_of__useSatelliteTexture_4() { return static_cast<int32_t>(offsetof(TextureMonoBehaviourModifier_t899798732, ____useSatelliteTexture_4)); }
	inline bool get__useSatelliteTexture_4() const { return ____useSatelliteTexture_4; }
	inline bool* get_address_of__useSatelliteTexture_4() { return &____useSatelliteTexture_4; }
	inline void set__useSatelliteTexture_4(bool value)
	{
		____useSatelliteTexture_4 = value;
	}

	inline static int32_t get_offset_of__topMaterials_5() { return static_cast<int32_t>(offsetof(TextureMonoBehaviourModifier_t899798732, ____topMaterials_5)); }
	inline MaterialU5BU5D_t561872642* get__topMaterials_5() const { return ____topMaterials_5; }
	inline MaterialU5BU5D_t561872642** get_address_of__topMaterials_5() { return &____topMaterials_5; }
	inline void set__topMaterials_5(MaterialU5BU5D_t561872642* value)
	{
		____topMaterials_5 = value;
		Il2CppCodeGenWriteBarrier((&____topMaterials_5), value);
	}

	inline static int32_t get_offset_of__textureSides_6() { return static_cast<int32_t>(offsetof(TextureMonoBehaviourModifier_t899798732, ____textureSides_6)); }
	inline bool get__textureSides_6() const { return ____textureSides_6; }
	inline bool* get_address_of__textureSides_6() { return &____textureSides_6; }
	inline void set__textureSides_6(bool value)
	{
		____textureSides_6 = value;
	}

	inline static int32_t get_offset_of__sideMaterials_7() { return static_cast<int32_t>(offsetof(TextureMonoBehaviourModifier_t899798732, ____sideMaterials_7)); }
	inline MaterialU5BU5D_t561872642* get__sideMaterials_7() const { return ____sideMaterials_7; }
	inline MaterialU5BU5D_t561872642** get_address_of__sideMaterials_7() { return &____sideMaterials_7; }
	inline void set__sideMaterials_7(MaterialU5BU5D_t561872642* value)
	{
		____sideMaterials_7 = value;
		Il2CppCodeGenWriteBarrier((&____sideMaterials_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMONOBEHAVIOURMODIFIER_T899798732_H
#ifndef CHAMFERHEIGHTMODIFIER_T3896880496_H
#define CHAMFERHEIGHTMODIFIER_T3896880496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.ChamferHeightModifier
struct  ChamferHeightModifier_t3896880496  : public MeshModifier_t2972815152
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.ChamferHeightModifier::_flatTops
	bool ____flatTops_3;
	// System.Single Mapbox.Unity.MeshGeneration.Modifiers.ChamferHeightModifier::_height
	float ____height_4;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.ChamferHeightModifier::_forceHeight
	bool ____forceHeight_5;
	// System.Single Mapbox.Unity.MeshGeneration.Modifiers.ChamferHeightModifier::_offset
	float ____offset_6;

public:
	inline static int32_t get_offset_of__flatTops_3() { return static_cast<int32_t>(offsetof(ChamferHeightModifier_t3896880496, ____flatTops_3)); }
	inline bool get__flatTops_3() const { return ____flatTops_3; }
	inline bool* get_address_of__flatTops_3() { return &____flatTops_3; }
	inline void set__flatTops_3(bool value)
	{
		____flatTops_3 = value;
	}

	inline static int32_t get_offset_of__height_4() { return static_cast<int32_t>(offsetof(ChamferHeightModifier_t3896880496, ____height_4)); }
	inline float get__height_4() const { return ____height_4; }
	inline float* get_address_of__height_4() { return &____height_4; }
	inline void set__height_4(float value)
	{
		____height_4 = value;
	}

	inline static int32_t get_offset_of__forceHeight_5() { return static_cast<int32_t>(offsetof(ChamferHeightModifier_t3896880496, ____forceHeight_5)); }
	inline bool get__forceHeight_5() const { return ____forceHeight_5; }
	inline bool* get_address_of__forceHeight_5() { return &____forceHeight_5; }
	inline void set__forceHeight_5(bool value)
	{
		____forceHeight_5 = value;
	}

	inline static int32_t get_offset_of__offset_6() { return static_cast<int32_t>(offsetof(ChamferHeightModifier_t3896880496, ____offset_6)); }
	inline float get__offset_6() const { return ____offset_6; }
	inline float* get_address_of__offset_6() { return &____offset_6; }
	inline void set__offset_6(float value)
	{
		____offset_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAMFERHEIGHTMODIFIER_T3896880496_H
#ifndef HEIGHTMODIFIER_T2123936681_H
#define HEIGHTMODIFIER_T2123936681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.HeightModifier
struct  HeightModifier_t2123936681  : public MeshModifier_t2972815152
{
public:
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.HeightModifier::_flatTops
	bool ____flatTops_3;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.HeightModifier::_forceHeight
	bool ____forceHeight_4;
	// System.Single Mapbox.Unity.MeshGeneration.Modifiers.HeightModifier::_height
	float ____height_5;
	// System.Single Mapbox.Unity.MeshGeneration.Modifiers.HeightModifier::_scale
	float ____scale_6;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.HeightModifier::_createSideWalls
	bool ____createSideWalls_7;

public:
	inline static int32_t get_offset_of__flatTops_3() { return static_cast<int32_t>(offsetof(HeightModifier_t2123936681, ____flatTops_3)); }
	inline bool get__flatTops_3() const { return ____flatTops_3; }
	inline bool* get_address_of__flatTops_3() { return &____flatTops_3; }
	inline void set__flatTops_3(bool value)
	{
		____flatTops_3 = value;
	}

	inline static int32_t get_offset_of__forceHeight_4() { return static_cast<int32_t>(offsetof(HeightModifier_t2123936681, ____forceHeight_4)); }
	inline bool get__forceHeight_4() const { return ____forceHeight_4; }
	inline bool* get_address_of__forceHeight_4() { return &____forceHeight_4; }
	inline void set__forceHeight_4(bool value)
	{
		____forceHeight_4 = value;
	}

	inline static int32_t get_offset_of__height_5() { return static_cast<int32_t>(offsetof(HeightModifier_t2123936681, ____height_5)); }
	inline float get__height_5() const { return ____height_5; }
	inline float* get_address_of__height_5() { return &____height_5; }
	inline void set__height_5(float value)
	{
		____height_5 = value;
	}

	inline static int32_t get_offset_of__scale_6() { return static_cast<int32_t>(offsetof(HeightModifier_t2123936681, ____scale_6)); }
	inline float get__scale_6() const { return ____scale_6; }
	inline float* get_address_of__scale_6() { return &____scale_6; }
	inline void set__scale_6(float value)
	{
		____scale_6 = value;
	}

	inline static int32_t get_offset_of__createSideWalls_7() { return static_cast<int32_t>(offsetof(HeightModifier_t2123936681, ____createSideWalls_7)); }
	inline bool get__createSideWalls_7() const { return ____createSideWalls_7; }
	inline bool* get_address_of__createSideWalls_7() { return &____createSideWalls_7; }
	inline void set__createSideWalls_7(bool value)
	{
		____createSideWalls_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEIGHTMODIFIER_T2123936681_H
#ifndef LINEMESHMODIFIER_T935281077_H
#define LINEMESHMODIFIER_T935281077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.LineMeshModifier
struct  LineMeshModifier_t935281077  : public MeshModifier_t2972815152
{
public:
	// System.Single Mapbox.Unity.MeshGeneration.Modifiers.LineMeshModifier::Width
	float ___Width_3;
	// System.Single Mapbox.Unity.MeshGeneration.Modifiers.LineMeshModifier::_scaledWidth
	float ____scaledWidth_4;

public:
	inline static int32_t get_offset_of_Width_3() { return static_cast<int32_t>(offsetof(LineMeshModifier_t935281077, ___Width_3)); }
	inline float get_Width_3() const { return ___Width_3; }
	inline float* get_address_of_Width_3() { return &___Width_3; }
	inline void set_Width_3(float value)
	{
		___Width_3 = value;
	}

	inline static int32_t get_offset_of__scaledWidth_4() { return static_cast<int32_t>(offsetof(LineMeshModifier_t935281077, ____scaledWidth_4)); }
	inline float get__scaledWidth_4() const { return ____scaledWidth_4; }
	inline float* get_address_of__scaledWidth_4() { return &____scaledWidth_4; }
	inline void set__scaledWidth_4(float value)
	{
		____scaledWidth_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEMESHMODIFIER_T935281077_H
#ifndef POLYGONMESHMODIFIER_T376286888_H
#define POLYGONMESHMODIFIER_T376286888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.PolygonMeshModifier
struct  PolygonMeshModifier_t376286888  : public MeshModifier_t2972815152
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONMESHMODIFIER_T376286888_H
#ifndef SMOOTHLINEMODIFIER_T1592913179_H
#define SMOOTHLINEMODIFIER_T1592913179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.SmoothLineModifier
struct  SmoothLineModifier_t1592913179  : public MeshModifier_t2972815152
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.SmoothLineModifier::_maxEdgeSectionCount
	int32_t ____maxEdgeSectionCount_3;
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.SmoothLineModifier::_preferredEdgeSectionLength
	int32_t ____preferredEdgeSectionLength_4;

public:
	inline static int32_t get_offset_of__maxEdgeSectionCount_3() { return static_cast<int32_t>(offsetof(SmoothLineModifier_t1592913179, ____maxEdgeSectionCount_3)); }
	inline int32_t get__maxEdgeSectionCount_3() const { return ____maxEdgeSectionCount_3; }
	inline int32_t* get_address_of__maxEdgeSectionCount_3() { return &____maxEdgeSectionCount_3; }
	inline void set__maxEdgeSectionCount_3(int32_t value)
	{
		____maxEdgeSectionCount_3 = value;
	}

	inline static int32_t get_offset_of__preferredEdgeSectionLength_4() { return static_cast<int32_t>(offsetof(SmoothLineModifier_t1592913179, ____preferredEdgeSectionLength_4)); }
	inline int32_t get__preferredEdgeSectionLength_4() const { return ____preferredEdgeSectionLength_4; }
	inline int32_t* get_address_of__preferredEdgeSectionLength_4() { return &____preferredEdgeSectionLength_4; }
	inline void set__preferredEdgeSectionLength_4(int32_t value)
	{
		____preferredEdgeSectionLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHLINEMODIFIER_T1592913179_H
#ifndef SNAPTERRAINMODIFIER_T3315843015_H
#define SNAPTERRAINMODIFIER_T3315843015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.SnapTerrainModifier
struct  SnapTerrainModifier_t3315843015  : public MeshModifier_t2972815152
{
public:
	// System.Double Mapbox.Unity.MeshGeneration.Modifiers.SnapTerrainModifier::scaledX
	double ___scaledX_3;
	// System.Double Mapbox.Unity.MeshGeneration.Modifiers.SnapTerrainModifier::scaledY
	double ___scaledY_4;

public:
	inline static int32_t get_offset_of_scaledX_3() { return static_cast<int32_t>(offsetof(SnapTerrainModifier_t3315843015, ___scaledX_3)); }
	inline double get_scaledX_3() const { return ___scaledX_3; }
	inline double* get_address_of_scaledX_3() { return &___scaledX_3; }
	inline void set_scaledX_3(double value)
	{
		___scaledX_3 = value;
	}

	inline static int32_t get_offset_of_scaledY_4() { return static_cast<int32_t>(offsetof(SnapTerrainModifier_t3315843015, ___scaledY_4)); }
	inline double get_scaledY_4() const { return ___scaledY_4; }
	inline double* get_address_of_scaledY_4() { return &___scaledY_4; }
	inline void set_scaledY_4(double value)
	{
		___scaledY_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPTERRAINMODIFIER_T3315843015_H
#ifndef HEADBOB_T3275031667_H
#define HEADBOB_T3275031667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.HeadBob
struct  HeadBob_t3275031667  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.HeadBob::Camera
	Camera_t4157153871 * ___Camera_2;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::motionBob
	CurveControlledBob_t2679313829 * ___motionBob_3;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::jumpAndLandingBob
	LerpControlledBob_t1895875871 * ___jumpAndLandingBob_4;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController UnityStandardAssets.Characters.FirstPerson.HeadBob::rigidbodyFirstPersonController
	RigidbodyFirstPersonController_t1207297146 * ___rigidbodyFirstPersonController_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::StrideInterval
	float ___StrideInterval_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::RunningStrideLengthen
	float ___RunningStrideLengthen_7;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.HeadBob::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_8;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.HeadBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_9;

public:
	inline static int32_t get_offset_of_Camera_2() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___Camera_2)); }
	inline Camera_t4157153871 * get_Camera_2() const { return ___Camera_2; }
	inline Camera_t4157153871 ** get_address_of_Camera_2() { return &___Camera_2; }
	inline void set_Camera_2(Camera_t4157153871 * value)
	{
		___Camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_2), value);
	}

	inline static int32_t get_offset_of_motionBob_3() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___motionBob_3)); }
	inline CurveControlledBob_t2679313829 * get_motionBob_3() const { return ___motionBob_3; }
	inline CurveControlledBob_t2679313829 ** get_address_of_motionBob_3() { return &___motionBob_3; }
	inline void set_motionBob_3(CurveControlledBob_t2679313829 * value)
	{
		___motionBob_3 = value;
		Il2CppCodeGenWriteBarrier((&___motionBob_3), value);
	}

	inline static int32_t get_offset_of_jumpAndLandingBob_4() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___jumpAndLandingBob_4)); }
	inline LerpControlledBob_t1895875871 * get_jumpAndLandingBob_4() const { return ___jumpAndLandingBob_4; }
	inline LerpControlledBob_t1895875871 ** get_address_of_jumpAndLandingBob_4() { return &___jumpAndLandingBob_4; }
	inline void set_jumpAndLandingBob_4(LerpControlledBob_t1895875871 * value)
	{
		___jumpAndLandingBob_4 = value;
		Il2CppCodeGenWriteBarrier((&___jumpAndLandingBob_4), value);
	}

	inline static int32_t get_offset_of_rigidbodyFirstPersonController_5() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___rigidbodyFirstPersonController_5)); }
	inline RigidbodyFirstPersonController_t1207297146 * get_rigidbodyFirstPersonController_5() const { return ___rigidbodyFirstPersonController_5; }
	inline RigidbodyFirstPersonController_t1207297146 ** get_address_of_rigidbodyFirstPersonController_5() { return &___rigidbodyFirstPersonController_5; }
	inline void set_rigidbodyFirstPersonController_5(RigidbodyFirstPersonController_t1207297146 * value)
	{
		___rigidbodyFirstPersonController_5 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbodyFirstPersonController_5), value);
	}

	inline static int32_t get_offset_of_StrideInterval_6() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___StrideInterval_6)); }
	inline float get_StrideInterval_6() const { return ___StrideInterval_6; }
	inline float* get_address_of_StrideInterval_6() { return &___StrideInterval_6; }
	inline void set_StrideInterval_6(float value)
	{
		___StrideInterval_6 = value;
	}

	inline static int32_t get_offset_of_RunningStrideLengthen_7() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___RunningStrideLengthen_7)); }
	inline float get_RunningStrideLengthen_7() const { return ___RunningStrideLengthen_7; }
	inline float* get_address_of_RunningStrideLengthen_7() { return &___RunningStrideLengthen_7; }
	inline void set_RunningStrideLengthen_7(float value)
	{
		___RunningStrideLengthen_7 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_8() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___m_PreviouslyGrounded_8)); }
	inline bool get_m_PreviouslyGrounded_8() const { return ___m_PreviouslyGrounded_8; }
	inline bool* get_address_of_m_PreviouslyGrounded_8() { return &___m_PreviouslyGrounded_8; }
	inline void set_m_PreviouslyGrounded_8(bool value)
	{
		___m_PreviouslyGrounded_8 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_9() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___m_OriginalCameraPosition_9)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_9() const { return ___m_OriginalCameraPosition_9; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_9() { return &___m_OriginalCameraPosition_9; }
	inline void set_m_OriginalCameraPosition_9(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADBOB_T3275031667_H
#ifndef TOUCHPAD_T539039257_H
#define TOUCHPAD_T539039257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_t539039257  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_2;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_3;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_t3722313464  ___m_StartPos_8;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_t2156229523  ___m_PreviousDelta_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_t3722313464  ___m_JoytickOutput_10;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_11;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_12;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_13;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_14;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_15;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_16;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_t2156229523  ___m_PreviousTouchPos_17;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_t3722313464  ___m_Center_18;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t2670269651 * ___m_Image_19;

public:
	inline static int32_t get_offset_of_axesToUse_2() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___axesToUse_2)); }
	inline int32_t get_axesToUse_2() const { return ___axesToUse_2; }
	inline int32_t* get_address_of_axesToUse_2() { return &___axesToUse_2; }
	inline void set_axesToUse_2(int32_t value)
	{
		___axesToUse_2 = value;
	}

	inline static int32_t get_offset_of_controlStyle_3() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___controlStyle_3)); }
	inline int32_t get_controlStyle_3() const { return ___controlStyle_3; }
	inline int32_t* get_address_of_controlStyle_3() { return &___controlStyle_3; }
	inline void set_controlStyle_3(int32_t value)
	{
		___controlStyle_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_6() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Xsensitivity_6)); }
	inline float get_Xsensitivity_6() const { return ___Xsensitivity_6; }
	inline float* get_address_of_Xsensitivity_6() { return &___Xsensitivity_6; }
	inline void set_Xsensitivity_6(float value)
	{
		___Xsensitivity_6 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_7() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Ysensitivity_7)); }
	inline float get_Ysensitivity_7() const { return ___Ysensitivity_7; }
	inline float* get_address_of_Ysensitivity_7() { return &___Ysensitivity_7; }
	inline void set_Ysensitivity_7(float value)
	{
		___Ysensitivity_7 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_StartPos_8)); }
	inline Vector3_t3722313464  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_t3722313464  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_9() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousDelta_9)); }
	inline Vector2_t2156229523  get_m_PreviousDelta_9() const { return ___m_PreviousDelta_9; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousDelta_9() { return &___m_PreviousDelta_9; }
	inline void set_m_PreviousDelta_9(Vector2_t2156229523  value)
	{
		___m_PreviousDelta_9 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_10() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_JoytickOutput_10)); }
	inline Vector3_t3722313464  get_m_JoytickOutput_10() const { return ___m_JoytickOutput_10; }
	inline Vector3_t3722313464 * get_address_of_m_JoytickOutput_10() { return &___m_JoytickOutput_10; }
	inline void set_m_JoytickOutput_10(Vector3_t3722313464  value)
	{
		___m_JoytickOutput_10 = value;
	}

	inline static int32_t get_offset_of_m_UseX_11() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseX_11)); }
	inline bool get_m_UseX_11() const { return ___m_UseX_11; }
	inline bool* get_address_of_m_UseX_11() { return &___m_UseX_11; }
	inline void set_m_UseX_11(bool value)
	{
		___m_UseX_11 = value;
	}

	inline static int32_t get_offset_of_m_UseY_12() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseY_12)); }
	inline bool get_m_UseY_12() const { return ___m_UseY_12; }
	inline bool* get_address_of_m_UseY_12() { return &___m_UseY_12; }
	inline void set_m_UseY_12(bool value)
	{
		___m_UseY_12 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_13() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_HorizontalVirtualAxis_13)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_13() const { return ___m_HorizontalVirtualAxis_13; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_13() { return &___m_HorizontalVirtualAxis_13; }
	inline void set_m_HorizontalVirtualAxis_13(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_14() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_VerticalVirtualAxis_14)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_14() const { return ___m_VerticalVirtualAxis_14; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_14() { return &___m_VerticalVirtualAxis_14; }
	inline void set_m_VerticalVirtualAxis_14(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_14), value);
	}

	inline static int32_t get_offset_of_m_Dragging_15() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Dragging_15)); }
	inline bool get_m_Dragging_15() const { return ___m_Dragging_15; }
	inline bool* get_address_of_m_Dragging_15() { return &___m_Dragging_15; }
	inline void set_m_Dragging_15(bool value)
	{
		___m_Dragging_15 = value;
	}

	inline static int32_t get_offset_of_m_Id_16() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Id_16)); }
	inline int32_t get_m_Id_16() const { return ___m_Id_16; }
	inline int32_t* get_address_of_m_Id_16() { return &___m_Id_16; }
	inline void set_m_Id_16(int32_t value)
	{
		___m_Id_16 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_17() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousTouchPos_17)); }
	inline Vector2_t2156229523  get_m_PreviousTouchPos_17() const { return ___m_PreviousTouchPos_17; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousTouchPos_17() { return &___m_PreviousTouchPos_17; }
	inline void set_m_PreviousTouchPos_17(Vector2_t2156229523  value)
	{
		___m_PreviousTouchPos_17 = value;
	}

	inline static int32_t get_offset_of_m_Center_18() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Center_18)); }
	inline Vector3_t3722313464  get_m_Center_18() const { return ___m_Center_18; }
	inline Vector3_t3722313464 * get_address_of_m_Center_18() { return &___m_Center_18; }
	inline void set_m_Center_18(Vector3_t3722313464  value)
	{
		___m_Center_18 = value;
	}

	inline static int32_t get_offset_of_m_Image_19() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Image_19)); }
	inline Image_t2670269651 * get_m_Image_19() const { return ___m_Image_19; }
	inline Image_t2670269651 ** get_address_of_m_Image_19() { return &___m_Image_19; }
	inline void set_m_Image_19(Image_t2670269651 * value)
	{
		___m_Image_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T539039257_H
#ifndef ACTIVATETRIGGER_T3349759092_H
#define ACTIVATETRIGGER_T3349759092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_t3349759092  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_2;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_t631007953 * ___target_3;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_t1113636619 * ___source_4;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_5;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_6;

public:
	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___action_2)); }
	inline int32_t get_action_2() const { return ___action_2; }
	inline int32_t* get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(int32_t value)
	{
		___action_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___target_3)); }
	inline Object_t631007953 * get_target_3() const { return ___target_3; }
	inline Object_t631007953 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Object_t631007953 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___source_4)); }
	inline GameObject_t1113636619 * get_source_4() const { return ___source_4; }
	inline GameObject_t1113636619 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(GameObject_t1113636619 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_triggerCount_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___triggerCount_5)); }
	inline int32_t get_triggerCount_5() const { return ___triggerCount_5; }
	inline int32_t* get_address_of_triggerCount_5() { return &___triggerCount_5; }
	inline void set_triggerCount_5(int32_t value)
	{
		___triggerCount_5 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___repeatTrigger_6)); }
	inline bool get_repeatTrigger_6() const { return ___repeatTrigger_6; }
	inline bool* get_address_of_repeatTrigger_6() { return &___repeatTrigger_6; }
	inline void set_repeatTrigger_6(bool value)
	{
		___repeatTrigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_T3349759092_H
#ifndef AUTOMOBILESHADERSWITCH_T568447889_H
#define AUTOMOBILESHADERSWITCH_T568447889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_t568447889  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_t1887104210 * ___m_ReplacementList_2;

public:
	inline static int32_t get_offset_of_m_ReplacementList_2() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_t568447889, ___m_ReplacementList_2)); }
	inline ReplacementList_t1887104210 * get_m_ReplacementList_2() const { return ___m_ReplacementList_2; }
	inline ReplacementList_t1887104210 ** get_address_of_m_ReplacementList_2() { return &___m_ReplacementList_2; }
	inline void set_m_ReplacementList_2(ReplacementList_t1887104210 * value)
	{
		___m_ReplacementList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_T568447889_H
#ifndef AUTOMOVEANDROTATE_T2437913015_H
#define AUTOMOVEANDROTATE_T2437913015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t2437913015  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t219844479 * ___moveUnitsPerSecond_2;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t219844479 * ___rotateDegreesPerSecond_3;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_4;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_5;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_2() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___moveUnitsPerSecond_2)); }
	inline Vector3andSpace_t219844479 * get_moveUnitsPerSecond_2() const { return ___moveUnitsPerSecond_2; }
	inline Vector3andSpace_t219844479 ** get_address_of_moveUnitsPerSecond_2() { return &___moveUnitsPerSecond_2; }
	inline void set_moveUnitsPerSecond_2(Vector3andSpace_t219844479 * value)
	{
		___moveUnitsPerSecond_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_2), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_3() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___rotateDegreesPerSecond_3)); }
	inline Vector3andSpace_t219844479 * get_rotateDegreesPerSecond_3() const { return ___rotateDegreesPerSecond_3; }
	inline Vector3andSpace_t219844479 ** get_address_of_rotateDegreesPerSecond_3() { return &___rotateDegreesPerSecond_3; }
	inline void set_rotateDegreesPerSecond_3(Vector3andSpace_t219844479 * value)
	{
		___rotateDegreesPerSecond_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_3), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___ignoreTimescale_4)); }
	inline bool get_ignoreTimescale_4() const { return ___ignoreTimescale_4; }
	inline bool* get_address_of_ignoreTimescale_4() { return &___ignoreTimescale_4; }
	inline void set_ignoreTimescale_4(bool value)
	{
		___ignoreTimescale_4 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___m_LastRealTime_5)); }
	inline float get_m_LastRealTime_5() const { return ___m_LastRealTime_5; }
	inline float* get_address_of_m_LastRealTime_5() { return &___m_LastRealTime_5; }
	inline void set_m_LastRealTime_5(float value)
	{
		___m_LastRealTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T2437913015_H
#ifndef DRAGRIGIDBODY_T1600652016_H
#define DRAGRIGIDBODY_T1600652016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t1600652016  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t1912369980 * ___m_SpringJoint_8;

public:
	inline static int32_t get_offset_of_m_SpringJoint_8() { return static_cast<int32_t>(offsetof(DragRigidbody_t1600652016, ___m_SpringJoint_8)); }
	inline SpringJoint_t1912369980 * get_m_SpringJoint_8() const { return ___m_SpringJoint_8; }
	inline SpringJoint_t1912369980 ** get_address_of_m_SpringJoint_8() { return &___m_SpringJoint_8; }
	inline void set_m_SpringJoint_8(SpringJoint_t1912369980 * value)
	{
		___m_SpringJoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_T1600652016_H
#ifndef DYNAMICSHADOWSETTINGS_T59119858_H
#define DYNAMICSHADOWSETTINGS_T59119858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t59119858  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_t3756812086 * ___sunLight_2;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_3;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_12;

public:
	inline static int32_t get_offset_of_sunLight_2() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___sunLight_2)); }
	inline Light_t3756812086 * get_sunLight_2() const { return ___sunLight_2; }
	inline Light_t3756812086 ** get_address_of_sunLight_2() { return &___sunLight_2; }
	inline void set_sunLight_2(Light_t3756812086 * value)
	{
		___sunLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_2), value);
	}

	inline static int32_t get_offset_of_minHeight_3() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minHeight_3)); }
	inline float get_minHeight_3() const { return ___minHeight_3; }
	inline float* get_address_of_minHeight_3() { return &___minHeight_3; }
	inline void set_minHeight_3(float value)
	{
		___minHeight_3 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowDistance_4)); }
	inline float get_minShadowDistance_4() const { return ___minShadowDistance_4; }
	inline float* get_address_of_minShadowDistance_4() { return &___minShadowDistance_4; }
	inline void set_minShadowDistance_4(float value)
	{
		___minShadowDistance_4 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowBias_5)); }
	inline float get_minShadowBias_5() const { return ___minShadowBias_5; }
	inline float* get_address_of_minShadowBias_5() { return &___minShadowBias_5; }
	inline void set_minShadowBias_5(float value)
	{
		___minShadowBias_5 = value;
	}

	inline static int32_t get_offset_of_maxHeight_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxHeight_6)); }
	inline float get_maxHeight_6() const { return ___maxHeight_6; }
	inline float* get_address_of_maxHeight_6() { return &___maxHeight_6; }
	inline void set_maxHeight_6(float value)
	{
		___maxHeight_6 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowDistance_7)); }
	inline float get_maxShadowDistance_7() const { return ___maxShadowDistance_7; }
	inline float* get_address_of_maxShadowDistance_7() { return &___maxShadowDistance_7; }
	inline void set_maxShadowDistance_7(float value)
	{
		___maxShadowDistance_7 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowBias_8)); }
	inline float get_maxShadowBias_8() const { return ___maxShadowBias_8; }
	inline float* get_address_of_maxShadowBias_8() { return &___maxShadowBias_8; }
	inline void set_maxShadowBias_8(float value)
	{
		___maxShadowBias_8 = value;
	}

	inline static int32_t get_offset_of_adaptTime_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___adaptTime_9)); }
	inline float get_adaptTime_9() const { return ___adaptTime_9; }
	inline float* get_address_of_adaptTime_9() { return &___adaptTime_9; }
	inline void set_adaptTime_9(float value)
	{
		___adaptTime_9 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_SmoothHeight_10)); }
	inline float get_m_SmoothHeight_10() const { return ___m_SmoothHeight_10; }
	inline float* get_address_of_m_SmoothHeight_10() { return &___m_SmoothHeight_10; }
	inline void set_m_SmoothHeight_10(float value)
	{
		___m_SmoothHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_ChangeSpeed_11)); }
	inline float get_m_ChangeSpeed_11() const { return ___m_ChangeSpeed_11; }
	inline float* get_address_of_m_ChangeSpeed_11() { return &___m_ChangeSpeed_11; }
	inline void set_m_ChangeSpeed_11(float value)
	{
		___m_ChangeSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_OriginalStrength_12)); }
	inline float get_m_OriginalStrength_12() const { return ___m_OriginalStrength_12; }
	inline float* get_address_of_m_OriginalStrength_12() { return &___m_OriginalStrength_12; }
	inline void set_m_OriginalStrength_12(float value)
	{
		___m_OriginalStrength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T59119858_H
#ifndef FOLLOWTARGET_T166153614_H
#define FOLLOWTARGET_T166153614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t166153614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t3600365921 * ___target_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t3722313464  ___offset_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___offset_3)); }
	inline Vector3_t3722313464  get_offset_3() const { return ___offset_3; }
	inline Vector3_t3722313464 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_t3722313464  value)
	{
		___offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T166153614_H
#ifndef FORCEDRESET_T301124368_H
#define FORCEDRESET_T301124368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t301124368  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T301124368_H
#ifndef TILTINPUT_T1639936653_H
#define TILTINPUT_T1639936653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t1639936653  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t3982445645 * ___mapping_2;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_5;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_t4087348596 * ___m_SteerAxis_6;

public:
	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___mapping_2)); }
	inline AxisMapping_t3982445645 * get_mapping_2() const { return ___mapping_2; }
	inline AxisMapping_t3982445645 ** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(AxisMapping_t3982445645 * value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_3() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___tiltAroundAxis_3)); }
	inline int32_t get_tiltAroundAxis_3() const { return ___tiltAroundAxis_3; }
	inline int32_t* get_address_of_tiltAroundAxis_3() { return &___tiltAroundAxis_3; }
	inline void set_tiltAroundAxis_3(int32_t value)
	{
		___tiltAroundAxis_3 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_4() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___fullTiltAngle_4)); }
	inline float get_fullTiltAngle_4() const { return ___fullTiltAngle_4; }
	inline float* get_address_of_fullTiltAngle_4() { return &___fullTiltAngle_4; }
	inline void set_fullTiltAngle_4(float value)
	{
		___fullTiltAngle_4 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_5() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___centreAngleOffset_5)); }
	inline float get_centreAngleOffset_5() const { return ___centreAngleOffset_5; }
	inline float* get_address_of_centreAngleOffset_5() { return &___centreAngleOffset_5; }
	inline void set_centreAngleOffset_5(float value)
	{
		___centreAngleOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_6() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___m_SteerAxis_6)); }
	inline VirtualAxis_t4087348596 * get_m_SteerAxis_6() const { return ___m_SteerAxis_6; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_SteerAxis_6() { return &___m_SteerAxis_6; }
	inline void set_m_SteerAxis_6(VirtualAxis_t4087348596 * value)
	{
		___m_SteerAxis_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T1639936653_H
#ifndef RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#define RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct  RigidbodyFirstPersonController_t1207297146  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::cam
	Camera_t4157153871 * ___cam_2;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::movementSettings
	MovementSettings_t1096092444 * ___movementSettings_3;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::mouseLook
	MouseLook_t2859678661 * ___mouseLook_4;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::advancedSettings
	AdvancedSettings_t778418834 * ___advancedSettings_5;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_RigidBody
	Rigidbody_t3916780224 * ___m_RigidBody_6;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Capsule
	CapsuleCollider_t197597763 * ___m_Capsule_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_YRotation
	float ___m_YRotation_8;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_GroundContactNormal
	Vector3_t3722313464  ___m_GroundContactNormal_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jump
	bool ___m_Jump_10;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jumping
	bool ___m_Jumping_12;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_IsGrounded
	bool ___m_IsGrounded_13;

public:
	inline static int32_t get_offset_of_cam_2() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___cam_2)); }
	inline Camera_t4157153871 * get_cam_2() const { return ___cam_2; }
	inline Camera_t4157153871 ** get_address_of_cam_2() { return &___cam_2; }
	inline void set_cam_2(Camera_t4157153871 * value)
	{
		___cam_2 = value;
		Il2CppCodeGenWriteBarrier((&___cam_2), value);
	}

	inline static int32_t get_offset_of_movementSettings_3() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___movementSettings_3)); }
	inline MovementSettings_t1096092444 * get_movementSettings_3() const { return ___movementSettings_3; }
	inline MovementSettings_t1096092444 ** get_address_of_movementSettings_3() { return &___movementSettings_3; }
	inline void set_movementSettings_3(MovementSettings_t1096092444 * value)
	{
		___movementSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___movementSettings_3), value);
	}

	inline static int32_t get_offset_of_mouseLook_4() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___mouseLook_4)); }
	inline MouseLook_t2859678661 * get_mouseLook_4() const { return ___mouseLook_4; }
	inline MouseLook_t2859678661 ** get_address_of_mouseLook_4() { return &___mouseLook_4; }
	inline void set_mouseLook_4(MouseLook_t2859678661 * value)
	{
		___mouseLook_4 = value;
		Il2CppCodeGenWriteBarrier((&___mouseLook_4), value);
	}

	inline static int32_t get_offset_of_advancedSettings_5() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___advancedSettings_5)); }
	inline AdvancedSettings_t778418834 * get_advancedSettings_5() const { return ___advancedSettings_5; }
	inline AdvancedSettings_t778418834 ** get_address_of_advancedSettings_5() { return &___advancedSettings_5; }
	inline void set_advancedSettings_5(AdvancedSettings_t778418834 * value)
	{
		___advancedSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___advancedSettings_5), value);
	}

	inline static int32_t get_offset_of_m_RigidBody_6() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_RigidBody_6)); }
	inline Rigidbody_t3916780224 * get_m_RigidBody_6() const { return ___m_RigidBody_6; }
	inline Rigidbody_t3916780224 ** get_address_of_m_RigidBody_6() { return &___m_RigidBody_6; }
	inline void set_m_RigidBody_6(Rigidbody_t3916780224 * value)
	{
		___m_RigidBody_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RigidBody_6), value);
	}

	inline static int32_t get_offset_of_m_Capsule_7() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Capsule_7)); }
	inline CapsuleCollider_t197597763 * get_m_Capsule_7() const { return ___m_Capsule_7; }
	inline CapsuleCollider_t197597763 ** get_address_of_m_Capsule_7() { return &___m_Capsule_7; }
	inline void set_m_Capsule_7(CapsuleCollider_t197597763 * value)
	{
		___m_Capsule_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_7), value);
	}

	inline static int32_t get_offset_of_m_YRotation_8() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_YRotation_8)); }
	inline float get_m_YRotation_8() const { return ___m_YRotation_8; }
	inline float* get_address_of_m_YRotation_8() { return &___m_YRotation_8; }
	inline void set_m_YRotation_8(float value)
	{
		___m_YRotation_8 = value;
	}

	inline static int32_t get_offset_of_m_GroundContactNormal_9() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_GroundContactNormal_9)); }
	inline Vector3_t3722313464  get_m_GroundContactNormal_9() const { return ___m_GroundContactNormal_9; }
	inline Vector3_t3722313464 * get_address_of_m_GroundContactNormal_9() { return &___m_GroundContactNormal_9; }
	inline void set_m_GroundContactNormal_9(Vector3_t3722313464  value)
	{
		___m_GroundContactNormal_9 = value;
	}

	inline static int32_t get_offset_of_m_Jump_10() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Jump_10)); }
	inline bool get_m_Jump_10() const { return ___m_Jump_10; }
	inline bool* get_address_of_m_Jump_10() { return &___m_Jump_10; }
	inline void set_m_Jump_10(bool value)
	{
		___m_Jump_10 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_11() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_PreviouslyGrounded_11)); }
	inline bool get_m_PreviouslyGrounded_11() const { return ___m_PreviouslyGrounded_11; }
	inline bool* get_address_of_m_PreviouslyGrounded_11() { return &___m_PreviouslyGrounded_11; }
	inline void set_m_PreviouslyGrounded_11(bool value)
	{
		___m_PreviouslyGrounded_11 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_12() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Jumping_12)); }
	inline bool get_m_Jumping_12() const { return ___m_Jumping_12; }
	inline bool* get_address_of_m_Jumping_12() { return &___m_Jumping_12; }
	inline void set_m_Jumping_12(bool value)
	{
		___m_Jumping_12 = value;
	}

	inline static int32_t get_offset_of_m_IsGrounded_13() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_IsGrounded_13)); }
	inline bool get_m_IsGrounded_13() const { return ___m_IsGrounded_13; }
	inline bool* get_address_of_m_IsGrounded_13() { return &___m_IsGrounded_13; }
	inline void set_m_IsGrounded_13(bool value)
	{
		___m_IsGrounded_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#ifndef BALL_T2378314638_H
#define BALL_T2378314638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.Ball
struct  Ball_t2378314638  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MovePower
	float ___m_MovePower_2;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.Ball::m_UseTorque
	bool ___m_UseTorque_3;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MaxAngularVelocity
	float ___m_MaxAngularVelocity_4;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_JumpPower
	float ___m_JumpPower_5;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Ball.Ball::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_7;

public:
	inline static int32_t get_offset_of_m_MovePower_2() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_MovePower_2)); }
	inline float get_m_MovePower_2() const { return ___m_MovePower_2; }
	inline float* get_address_of_m_MovePower_2() { return &___m_MovePower_2; }
	inline void set_m_MovePower_2(float value)
	{
		___m_MovePower_2 = value;
	}

	inline static int32_t get_offset_of_m_UseTorque_3() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_UseTorque_3)); }
	inline bool get_m_UseTorque_3() const { return ___m_UseTorque_3; }
	inline bool* get_address_of_m_UseTorque_3() { return &___m_UseTorque_3; }
	inline void set_m_UseTorque_3(bool value)
	{
		___m_UseTorque_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxAngularVelocity_4() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_MaxAngularVelocity_4)); }
	inline float get_m_MaxAngularVelocity_4() const { return ___m_MaxAngularVelocity_4; }
	inline float* get_address_of_m_MaxAngularVelocity_4() { return &___m_MaxAngularVelocity_4; }
	inline void set_m_MaxAngularVelocity_4(float value)
	{
		___m_MaxAngularVelocity_4 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_5() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_JumpPower_5)); }
	inline float get_m_JumpPower_5() const { return ___m_JumpPower_5; }
	inline float* get_address_of_m_JumpPower_5() { return &___m_JumpPower_5; }
	inline void set_m_JumpPower_5(float value)
	{
		___m_JumpPower_5 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_7() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_Rigidbody_7)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_7() const { return ___m_Rigidbody_7; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_7() { return &___m_Rigidbody_7; }
	inline void set_m_Rigidbody_7(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T2378314638_H
#ifndef BALLUSERCONTROL_T2574698008_H
#define BALLUSERCONTROL_T2574698008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.BallUserControl
struct  BallUserControl_t2574698008  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Ball.Ball UnityStandardAssets.Vehicles.Ball.BallUserControl::ball
	Ball_t2378314638 * ___ball_2;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::move
	Vector3_t3722313464  ___move_3;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Ball.BallUserControl::cam
	Transform_t3600365921 * ___cam_4;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::camForward
	Vector3_t3722313464  ___camForward_5;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.BallUserControl::jump
	bool ___jump_6;

public:
	inline static int32_t get_offset_of_ball_2() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___ball_2)); }
	inline Ball_t2378314638 * get_ball_2() const { return ___ball_2; }
	inline Ball_t2378314638 ** get_address_of_ball_2() { return &___ball_2; }
	inline void set_ball_2(Ball_t2378314638 * value)
	{
		___ball_2 = value;
		Il2CppCodeGenWriteBarrier((&___ball_2), value);
	}

	inline static int32_t get_offset_of_move_3() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___move_3)); }
	inline Vector3_t3722313464  get_move_3() const { return ___move_3; }
	inline Vector3_t3722313464 * get_address_of_move_3() { return &___move_3; }
	inline void set_move_3(Vector3_t3722313464  value)
	{
		___move_3 = value;
	}

	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___cam_4)); }
	inline Transform_t3600365921 * get_cam_4() const { return ___cam_4; }
	inline Transform_t3600365921 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Transform_t3600365921 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier((&___cam_4), value);
	}

	inline static int32_t get_offset_of_camForward_5() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___camForward_5)); }
	inline Vector3_t3722313464  get_camForward_5() const { return ___camForward_5; }
	inline Vector3_t3722313464 * get_address_of_camForward_5() { return &___camForward_5; }
	inline void set_camForward_5(Vector3_t3722313464  value)
	{
		___camForward_5 = value;
	}

	inline static int32_t get_offset_of_jump_6() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___jump_6)); }
	inline bool get_jump_6() const { return ___jump_6; }
	inline bool* get_address_of_jump_6() { return &___jump_6; }
	inline void set_jump_6(bool value)
	{
		___jump_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLUSERCONTROL_T2574698008_H
#ifndef AXISTOUCHBUTTON_T3522881333_H
#define AXISTOUCHBUTTON_T3522881333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t3522881333  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_2;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_5;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t3522881333 * ___m_PairedWith_6;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_t4087348596 * ___m_Axis_7;

public:
	inline static int32_t get_offset_of_axisName_2() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisName_2)); }
	inline String_t* get_axisName_2() const { return ___axisName_2; }
	inline String_t** get_address_of_axisName_2() { return &___axisName_2; }
	inline void set_axisName_2(String_t* value)
	{
		___axisName_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_2), value);
	}

	inline static int32_t get_offset_of_axisValue_3() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisValue_3)); }
	inline float get_axisValue_3() const { return ___axisValue_3; }
	inline float* get_address_of_axisValue_3() { return &___axisValue_3; }
	inline void set_axisValue_3(float value)
	{
		___axisValue_3 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___responseSpeed_4)); }
	inline float get_responseSpeed_4() const { return ___responseSpeed_4; }
	inline float* get_address_of_responseSpeed_4() { return &___responseSpeed_4; }
	inline void set_responseSpeed_4(float value)
	{
		___responseSpeed_4 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___returnToCentreSpeed_5)); }
	inline float get_returnToCentreSpeed_5() const { return ___returnToCentreSpeed_5; }
	inline float* get_address_of_returnToCentreSpeed_5() { return &___returnToCentreSpeed_5; }
	inline void set_returnToCentreSpeed_5(float value)
	{
		___returnToCentreSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_PairedWith_6)); }
	inline AxisTouchButton_t3522881333 * get_m_PairedWith_6() const { return ___m_PairedWith_6; }
	inline AxisTouchButton_t3522881333 ** get_address_of_m_PairedWith_6() { return &___m_PairedWith_6; }
	inline void set_m_PairedWith_6(AxisTouchButton_t3522881333 * value)
	{
		___m_PairedWith_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_6), value);
	}

	inline static int32_t get_offset_of_m_Axis_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_Axis_7)); }
	inline VirtualAxis_t4087348596 * get_m_Axis_7() const { return ___m_Axis_7; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_Axis_7() { return &___m_Axis_7; }
	inline void set_m_Axis_7(VirtualAxis_t4087348596 * value)
	{
		___m_Axis_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T3522881333_H
#ifndef BUTTONHANDLER_T823762219_H
#define BUTTONHANDLER_T823762219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_t823762219  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(ButtonHandler_t823762219, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_T823762219_H
#ifndef INPUTAXISSCROLLBAR_T457958266_H
#define INPUTAXISSCROLLBAR_T457958266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t457958266  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_2;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t457958266, ___axis_2)); }
	inline String_t* get_axis_2() const { return ___axis_2; }
	inline String_t** get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(String_t* value)
	{
		___axis_2 = value;
		Il2CppCodeGenWriteBarrier((&___axis_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T457958266_H
#ifndef JOYSTICK_T2204371675_H
#define JOYSTICK_T2204371675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t2204371675  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_2;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_3;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_5;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_t3722313464  ___m_StartPos_6;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_7;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_9;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_10;

public:
	inline static int32_t get_offset_of_MovementRange_2() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___MovementRange_2)); }
	inline int32_t get_MovementRange_2() const { return ___MovementRange_2; }
	inline int32_t* get_address_of_MovementRange_2() { return &___MovementRange_2; }
	inline void set_MovementRange_2(int32_t value)
	{
		___MovementRange_2 = value;
	}

	inline static int32_t get_offset_of_axesToUse_3() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___axesToUse_3)); }
	inline int32_t get_axesToUse_3() const { return ___axesToUse_3; }
	inline int32_t* get_address_of_axesToUse_3() { return &___axesToUse_3; }
	inline void set_axesToUse_3(int32_t value)
	{
		___axesToUse_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_m_StartPos_6() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_StartPos_6)); }
	inline Vector3_t3722313464  get_m_StartPos_6() const { return ___m_StartPos_6; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_6() { return &___m_StartPos_6; }
	inline void set_m_StartPos_6(Vector3_t3722313464  value)
	{
		___m_StartPos_6 = value;
	}

	inline static int32_t get_offset_of_m_UseX_7() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseX_7)); }
	inline bool get_m_UseX_7() const { return ___m_UseX_7; }
	inline bool* get_address_of_m_UseX_7() { return &___m_UseX_7; }
	inline void set_m_UseX_7(bool value)
	{
		___m_UseX_7 = value;
	}

	inline static int32_t get_offset_of_m_UseY_8() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseY_8)); }
	inline bool get_m_UseY_8() const { return ___m_UseY_8; }
	inline bool* get_address_of_m_UseY_8() { return &___m_UseY_8; }
	inline void set_m_UseY_8(bool value)
	{
		___m_UseY_8 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_9() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_HorizontalVirtualAxis_9)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_9() const { return ___m_HorizontalVirtualAxis_9; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_9() { return &___m_HorizontalVirtualAxis_9; }
	inline void set_m_HorizontalVirtualAxis_9(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_9), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_10() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_VerticalVirtualAxis_10)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_10() const { return ___m_VerticalVirtualAxis_10; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_10() { return &___m_VerticalVirtualAxis_10; }
	inline void set_m_VerticalVirtualAxis_10(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T2204371675_H
#ifndef SPAWNINSIDEMODIFIER_T329223109_H
#define SPAWNINSIDEMODIFIER_T329223109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier
struct  SpawnInsideModifier_t329223109  : public GameObjectModifier_t609190006
{
public:
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_spawnRateInSquareMeters
	int32_t ____spawnRateInSquareMeters_3;
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_maxSpawn
	int32_t ____maxSpawn_4;
	// UnityEngine.GameObject[] Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_prefabs
	GameObjectU5BU5D_t3328599146* ____prefabs_5;
	// UnityEngine.LayerMask Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_layerMask
	LayerMask_t3493934918  ____layerMask_6;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_scaleDownWithWorld
	bool ____scaleDownWithWorld_7;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_randomizeScale
	bool ____randomizeScale_8;
	// System.Boolean Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_randomizeRotation
	bool ____randomizeRotation_9;
	// System.Int32 Mapbox.Unity.MeshGeneration.Modifiers.SpawnInsideModifier::_spawnedCount
	int32_t ____spawnedCount_10;

public:
	inline static int32_t get_offset_of__spawnRateInSquareMeters_3() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____spawnRateInSquareMeters_3)); }
	inline int32_t get__spawnRateInSquareMeters_3() const { return ____spawnRateInSquareMeters_3; }
	inline int32_t* get_address_of__spawnRateInSquareMeters_3() { return &____spawnRateInSquareMeters_3; }
	inline void set__spawnRateInSquareMeters_3(int32_t value)
	{
		____spawnRateInSquareMeters_3 = value;
	}

	inline static int32_t get_offset_of__maxSpawn_4() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____maxSpawn_4)); }
	inline int32_t get__maxSpawn_4() const { return ____maxSpawn_4; }
	inline int32_t* get_address_of__maxSpawn_4() { return &____maxSpawn_4; }
	inline void set__maxSpawn_4(int32_t value)
	{
		____maxSpawn_4 = value;
	}

	inline static int32_t get_offset_of__prefabs_5() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____prefabs_5)); }
	inline GameObjectU5BU5D_t3328599146* get__prefabs_5() const { return ____prefabs_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__prefabs_5() { return &____prefabs_5; }
	inline void set__prefabs_5(GameObjectU5BU5D_t3328599146* value)
	{
		____prefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&____prefabs_5), value);
	}

	inline static int32_t get_offset_of__layerMask_6() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____layerMask_6)); }
	inline LayerMask_t3493934918  get__layerMask_6() const { return ____layerMask_6; }
	inline LayerMask_t3493934918 * get_address_of__layerMask_6() { return &____layerMask_6; }
	inline void set__layerMask_6(LayerMask_t3493934918  value)
	{
		____layerMask_6 = value;
	}

	inline static int32_t get_offset_of__scaleDownWithWorld_7() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____scaleDownWithWorld_7)); }
	inline bool get__scaleDownWithWorld_7() const { return ____scaleDownWithWorld_7; }
	inline bool* get_address_of__scaleDownWithWorld_7() { return &____scaleDownWithWorld_7; }
	inline void set__scaleDownWithWorld_7(bool value)
	{
		____scaleDownWithWorld_7 = value;
	}

	inline static int32_t get_offset_of__randomizeScale_8() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____randomizeScale_8)); }
	inline bool get__randomizeScale_8() const { return ____randomizeScale_8; }
	inline bool* get_address_of__randomizeScale_8() { return &____randomizeScale_8; }
	inline void set__randomizeScale_8(bool value)
	{
		____randomizeScale_8 = value;
	}

	inline static int32_t get_offset_of__randomizeRotation_9() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____randomizeRotation_9)); }
	inline bool get__randomizeRotation_9() const { return ____randomizeRotation_9; }
	inline bool* get_address_of__randomizeRotation_9() { return &____randomizeRotation_9; }
	inline void set__randomizeRotation_9(bool value)
	{
		____randomizeRotation_9 = value;
	}

	inline static int32_t get_offset_of__spawnedCount_10() { return static_cast<int32_t>(offsetof(SpawnInsideModifier_t329223109, ____spawnedCount_10)); }
	inline int32_t get__spawnedCount_10() const { return ____spawnedCount_10; }
	inline int32_t* get_address_of__spawnedCount_10() { return &____spawnedCount_10; }
	inline void set__spawnedCount_10(int32_t value)
	{
		____spawnedCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNINSIDEMODIFIER_T329223109_H
#ifndef MOBILECONTROLRIG_T1964600252_H
#define MOBILECONTROLRIG_T1964600252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_t1964600252  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_T1964600252_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (SpawnInsideModifier_t329223109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3400[8] = 
{
	SpawnInsideModifier_t329223109::get_offset_of__spawnRateInSquareMeters_3(),
	SpawnInsideModifier_t329223109::get_offset_of__maxSpawn_4(),
	SpawnInsideModifier_t329223109::get_offset_of__prefabs_5(),
	SpawnInsideModifier_t329223109::get_offset_of__layerMask_6(),
	SpawnInsideModifier_t329223109::get_offset_of__scaleDownWithWorld_7(),
	SpawnInsideModifier_t329223109::get_offset_of__randomizeScale_8(),
	SpawnInsideModifier_t329223109::get_offset_of__randomizeRotation_9(),
	SpawnInsideModifier_t329223109::get_offset_of__spawnedCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (TextureModifier_t2005389549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3401[5] = 
{
	TextureModifier_t2005389549::get_offset_of__textureTop_3(),
	TextureModifier_t2005389549::get_offset_of__useSatelliteTexture_4(),
	TextureModifier_t2005389549::get_offset_of__topMaterials_5(),
	TextureModifier_t2005389549::get_offset_of__textureSides_6(),
	TextureModifier_t2005389549::get_offset_of__sideMaterials_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (TextureMonoBehaviourModifier_t899798732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3402[5] = 
{
	TextureMonoBehaviourModifier_t899798732::get_offset_of__textureTop_3(),
	TextureMonoBehaviourModifier_t899798732::get_offset_of__useSatelliteTexture_4(),
	TextureMonoBehaviourModifier_t899798732::get_offset_of__topMaterials_5(),
	TextureMonoBehaviourModifier_t899798732::get_offset_of__textureSides_6(),
	TextureMonoBehaviourModifier_t899798732::get_offset_of__sideMaterials_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (MergedModifierStack_t694552105), -1, sizeof(MergedModifierStack_t694552105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3403[8] = 
{
	MergedModifierStack_t694552105::get_offset_of_MeshModifiers_2(),
	MergedModifierStack_t694552105::get_offset_of_GoModifiers_3(),
	MergedModifierStack_t694552105::get_offset_of__cacheVertexCount_4(),
	MergedModifierStack_t694552105::get_offset_of__cached_5(),
	MergedModifierStack_t694552105::get_offset_of__buildingCount_6(),
	MergedModifierStack_t694552105_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	MergedModifierStack_t694552105_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
	MergedModifierStack_t694552105_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (U3CEndU3Ec__AnonStorey0_t266015876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3404[1] = 
{
	U3CEndU3Ec__AnonStorey0_t266015876::get_offset_of_st_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (ModifierType_t2211322250)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3405[3] = 
{
	ModifierType_t2211322250::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (MeshModifier_t2972815152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (ChamferHeightModifier_t3896880496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[4] = 
{
	ChamferHeightModifier_t3896880496::get_offset_of__flatTops_3(),
	ChamferHeightModifier_t3896880496::get_offset_of__height_4(),
	ChamferHeightModifier_t3896880496::get_offset_of__forceHeight_5(),
	ChamferHeightModifier_t3896880496::get_offset_of__offset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (EarcutLibrary_t3691687667), -1, sizeof(EarcutLibrary_t3691687667_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3408[1] = 
{
	EarcutLibrary_t3691687667_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (Data_t1129010489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[3] = 
{
	Data_t1129010489::get_offset_of_Vertices_0(),
	Data_t1129010489::get_offset_of_Holes_1(),
	Data_t1129010489::get_offset_of_Dim_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (Node_t2652317591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3410[9] = 
{
	Node_t2652317591::get_offset_of_i_0(),
	Node_t2652317591::get_offset_of_x_1(),
	Node_t2652317591::get_offset_of_y_2(),
	Node_t2652317591::get_offset_of_mZOrder_3(),
	Node_t2652317591::get_offset_of_prev_4(),
	Node_t2652317591::get_offset_of_next_5(),
	Node_t2652317591::get_offset_of_prevZ_6(),
	Node_t2652317591::get_offset_of_nextZ_7(),
	Node_t2652317591::get_offset_of_steiner_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (ExtrusionType_t1596510477)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3411[4] = 
{
	ExtrusionType_t1596510477::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (HeightModifier_t2123936681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[5] = 
{
	HeightModifier_t2123936681::get_offset_of__flatTops_3(),
	HeightModifier_t2123936681::get_offset_of__forceHeight_4(),
	HeightModifier_t2123936681::get_offset_of__height_5(),
	HeightModifier_t2123936681::get_offset_of__scale_6(),
	HeightModifier_t2123936681::get_offset_of__createSideWalls_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (LineMeshModifier_t935281077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3413[2] = 
{
	LineMeshModifier_t935281077::get_offset_of_Width_3(),
	LineMeshModifier_t935281077::get_offset_of__scaledWidth_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (PolygonMeshModifier_t376286888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (SmoothLineModifier_t1592913179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[2] = 
{
	SmoothLineModifier_t1592913179::get_offset_of__maxEdgeSectionCount_3(),
	SmoothLineModifier_t1592913179::get_offset_of__preferredEdgeSectionLength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (SnapTerrainModifier_t3315843015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3416[2] = 
{
	SnapTerrainModifier_t3315843015::get_offset_of_scaledX_3(),
	SnapTerrainModifier_t3315843015::get_offset_of_scaledY_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (SnapTerrainRaycastModifier_t3769335075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3417[2] = 
{
	0,
	SnapTerrainRaycastModifier_t3769335075::get_offset_of__terrainMask_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (UvModifier_t1527142922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3418[1] = 
{
	UvModifier_t1527142922::get_offset_of_UseSatelliteRoof_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (ModifierBase_t1320963181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3419[1] = 
{
	ModifierBase_t1320963181::get_offset_of_Active_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (PositionTargetType_t3604874692)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3420[4] = 
{
	PositionTargetType_t3604874692::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (ModifierStack_t3760196006), -1, sizeof(ModifierStack_t3760196006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3421[7] = 
{
	ModifierStack_t3760196006::get_offset_of__moveFeaturePositionTo_2(),
	ModifierStack_t3760196006::get_offset_of__center_3(),
	ModifierStack_t3760196006::get_offset_of_vertexIndex_4(),
	ModifierStack_t3760196006::get_offset_of_MeshModifiers_5(),
	ModifierStack_t3760196006::get_offset_of_GoModifiers_6(),
	ModifierStack_t3760196006_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	ModifierStack_t3760196006_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (ModifierStackBase_t4180154112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (TelemetryDummy_t2953168920), -1, sizeof(TelemetryDummy_t2953168920_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3424[1] = 
{
	TelemetryDummy_t2953168920_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (TelemetryFallback_t2579077632), -1, sizeof(TelemetryFallback_t2579077632_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3425[2] = 
{
	TelemetryFallback_t2579077632::get_offset_of__url_0(),
	TelemetryFallback_t2579077632_StaticFields::get_offset_of__instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (U3CPostU3Ec__Iterator0_t2165426564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3426[7] = 
{
	U3CPostU3Ec__Iterator0_t2165426564::get_offset_of_url_0(),
	U3CPostU3Ec__Iterator0_t2165426564::get_offset_of_U3CrequestU3E__0_1(),
	U3CPostU3Ec__Iterator0_t2165426564::get_offset_of_bodyJsonString_2(),
	U3CPostU3Ec__Iterator0_t2165426564::get_offset_of_U3CbodyRawU3E__0_3(),
	U3CPostU3Ec__Iterator0_t2165426564::get_offset_of_U24current_4(),
	U3CPostU3Ec__Iterator0_t2165426564::get_offset_of_U24disposing_5(),
	U3CPostU3Ec__Iterator0_t2165426564::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (U3CPostWWWU3Ec__Iterator1_t2684415133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[8] = 
{
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_bodyJsonString_0(),
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_U3CbodyRawU3E__0_1(),
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_U3CheadersU3E__0_2(),
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_url_3(),
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_U3CwwwU3E__0_4(),
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_U24current_5(),
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_U24disposing_6(),
	U3CPostWWWU3Ec__Iterator1_t2684415133::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (TelemetryIos_t3970959974), -1, sizeof(TelemetryIos_t3970959974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3428[1] = 
{
	TelemetryIos_t3970959974_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (Conversions_t2701420957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3429[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (GeocodeAttribute_t4233991700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (HTTPRequest_t539111708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[5] = 
{
	HTTPRequest_t539111708::get_offset_of__request_0(),
	HTTPRequest_t539111708::get_offset_of__timeout_1(),
	HTTPRequest_t539111708::get_offset_of__callback_2(),
	HTTPRequest_t539111708::get_offset_of__wasCancelled_3(),
	HTTPRequest_t539111708::get_offset_of_U3CIsCompletedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (U3CDoRequestU3Ec__Iterator0_t1782862350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3432[7] = 
{
	U3CDoRequestU3Ec__Iterator0_t1782862350::get_offset_of_U3CtimeoutU3E__0_0(),
	U3CDoRequestU3Ec__Iterator0_t1782862350::get_offset_of_U3CdidTimeoutU3E__0_1(),
	U3CDoRequestU3Ec__Iterator0_t1782862350::get_offset_of_U3CresponseU3E__1_2(),
	U3CDoRequestU3Ec__Iterator0_t1782862350::get_offset_of_U24this_3(),
	U3CDoRequestU3Ec__Iterator0_t1782862350::get_offset_of_U24current_4(),
	U3CDoRequestU3Ec__Iterator0_t1782862350::get_offset_of_U24disposing_5(),
	U3CDoRequestU3Ec__Iterator0_t1782862350::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (NodeEditorElementAttribute_t2832500404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3433[1] = 
{
	NodeEditorElementAttribute_t2832500404::get_offset_of_Name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (OpenUrlOnButtonClick_t1465608780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[1] = 
{
	OpenUrlOnButtonClick_t1465608780::get_offset_of__url_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (Runnable_t3171960597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[2] = 
{
	Runnable_t3171960597::get_offset_of_m_Routines_2(),
	Runnable_t3171960597::get_offset_of_m_NextRoutineId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (Routine_t852961361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3436[4] = 
{
	Routine_t852961361::get_offset_of_U3CIDU3Ek__BackingField_0(),
	Routine_t852961361::get_offset_of_U3CStopU3Ek__BackingField_1(),
	Routine_t852961361::get_offset_of_m_bMoveNext_2(),
	Routine_t852961361::get_offset_of_m_Enumerator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3437[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3438[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (StyleSearchAttribute_t2646828085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (TelemetryConfigurationButton_t4275913055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3440[1] = 
{
	TelemetryConfigurationButton_t4275913055::get_offset_of__booleanValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (VectorExtensions_t2364469250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (MapController_t3762646607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[1] = 
{
	MapController_t3762646607::get_offset_of_scene_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (MapPageController_t3109321034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3443[12] = 
{
	MapPageController_t3109321034::get_offset_of__map_2(),
	MapPageController_t3109321034::get_offset_of_scene_3(),
	MapPageController_t3109321034::get_offset_of_POIObjectPool_4(),
	MapPageController_t3109321034::get_offset_of_dataController_5(),
	MapPageController_t3109321034::get_offset_of_sitePool_6(),
	MapPageController_t3109321034::get_offset_of_siteIndex_7(),
	MapPageController_t3109321034::get_offset_of_siteID_8(),
	MapPageController_t3109321034::get_offset_of_lat_9(),
	MapPageController_t3109321034::get_offset_of_lng_10(),
	MapPageController_t3109321034::get_offset_of__targetPosition_11(),
	MapPageController_t3109321034::get_offset_of__isInitialized_12(),
	MapPageController_t3109321034::get_offset_of_POIGameObjects_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (U3CShowSitesU3Ec__Iterator0_t948523329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[7] = 
{
	U3CShowSitesU3Ec__Iterator0_t948523329::get_offset_of_U3CiU3E__1_0(),
	U3CShowSitesU3Ec__Iterator0_t948523329::get_offset_of_U3CPOIGameObjectU3E__2_1(),
	U3CShowSitesU3Ec__Iterator0_t948523329::get_offset_of_U3CpointOfInterestU3E__2_2(),
	U3CShowSitesU3Ec__Iterator0_t948523329::get_offset_of_U24this_3(),
	U3CShowSitesU3Ec__Iterator0_t948523329::get_offset_of_U24current_4(),
	U3CShowSitesU3Ec__Iterator0_t948523329::get_offset_of_U24disposing_5(),
	U3CShowSitesU3Ec__Iterator0_t948523329::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (MarkerPosition_t1625649371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[4] = 
{
	MarkerPosition_t1625649371::get_offset_of__map_2(),
	MarkerPosition_t1625649371::get_offset_of__latitudeLongitude_3(),
	MarkerPosition_t1625649371::get_offset_of__isInitialized_4(),
	MarkerPosition_t1625649371::get_offset_of__targetPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (FirstPersonController_t2020989554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3446[30] = 
{
	FirstPersonController_t2020989554::get_offset_of_m_IsWalking_2(),
	FirstPersonController_t2020989554::get_offset_of_m_WalkSpeed_3(),
	FirstPersonController_t2020989554::get_offset_of_m_RunSpeed_4(),
	FirstPersonController_t2020989554::get_offset_of_m_RunstepLenghten_5(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpSpeed_6(),
	FirstPersonController_t2020989554::get_offset_of_m_StickToGroundForce_7(),
	FirstPersonController_t2020989554::get_offset_of_m_GravityMultiplier_8(),
	FirstPersonController_t2020989554::get_offset_of_m_MouseLook_9(),
	FirstPersonController_t2020989554::get_offset_of_m_UseFovKick_10(),
	FirstPersonController_t2020989554::get_offset_of_m_FovKick_11(),
	FirstPersonController_t2020989554::get_offset_of_m_UseHeadBob_12(),
	FirstPersonController_t2020989554::get_offset_of_m_HeadBob_13(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpBob_14(),
	FirstPersonController_t2020989554::get_offset_of_m_StepInterval_15(),
	FirstPersonController_t2020989554::get_offset_of_m_FootstepSounds_16(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpSound_17(),
	FirstPersonController_t2020989554::get_offset_of_m_LandSound_18(),
	FirstPersonController_t2020989554::get_offset_of_m_Camera_19(),
	FirstPersonController_t2020989554::get_offset_of_m_Jump_20(),
	FirstPersonController_t2020989554::get_offset_of_m_YRotation_21(),
	FirstPersonController_t2020989554::get_offset_of_m_Input_22(),
	FirstPersonController_t2020989554::get_offset_of_m_MoveDir_23(),
	FirstPersonController_t2020989554::get_offset_of_m_CharacterController_24(),
	FirstPersonController_t2020989554::get_offset_of_m_CollisionFlags_25(),
	FirstPersonController_t2020989554::get_offset_of_m_PreviouslyGrounded_26(),
	FirstPersonController_t2020989554::get_offset_of_m_OriginalCameraPosition_27(),
	FirstPersonController_t2020989554::get_offset_of_m_StepCycle_28(),
	FirstPersonController_t2020989554::get_offset_of_m_NextStep_29(),
	FirstPersonController_t2020989554::get_offset_of_m_Jumping_30(),
	FirstPersonController_t2020989554::get_offset_of_m_AudioSource_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (HeadBob_t3275031667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3447[8] = 
{
	HeadBob_t3275031667::get_offset_of_Camera_2(),
	HeadBob_t3275031667::get_offset_of_motionBob_3(),
	HeadBob_t3275031667::get_offset_of_jumpAndLandingBob_4(),
	HeadBob_t3275031667::get_offset_of_rigidbodyFirstPersonController_5(),
	HeadBob_t3275031667::get_offset_of_StrideInterval_6(),
	HeadBob_t3275031667::get_offset_of_RunningStrideLengthen_7(),
	HeadBob_t3275031667::get_offset_of_m_PreviouslyGrounded_8(),
	HeadBob_t3275031667::get_offset_of_m_OriginalCameraPosition_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (MouseLook_t2859678661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3448[9] = 
{
	MouseLook_t2859678661::get_offset_of_XSensitivity_0(),
	MouseLook_t2859678661::get_offset_of_YSensitivity_1(),
	MouseLook_t2859678661::get_offset_of_clampVerticalRotation_2(),
	MouseLook_t2859678661::get_offset_of_MinimumX_3(),
	MouseLook_t2859678661::get_offset_of_MaximumX_4(),
	MouseLook_t2859678661::get_offset_of_smooth_5(),
	MouseLook_t2859678661::get_offset_of_smoothTime_6(),
	MouseLook_t2859678661::get_offset_of_m_CharacterTargetRot_7(),
	MouseLook_t2859678661::get_offset_of_m_CameraTargetRot_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (RigidbodyFirstPersonController_t1207297146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[12] = 
{
	RigidbodyFirstPersonController_t1207297146::get_offset_of_cam_2(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_movementSettings_3(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_mouseLook_4(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_advancedSettings_5(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_RigidBody_6(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Capsule_7(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_YRotation_8(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_GroundContactNormal_9(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Jump_10(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_PreviouslyGrounded_11(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Jumping_12(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_IsGrounded_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (MovementSettings_t1096092444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3450[8] = 
{
	MovementSettings_t1096092444::get_offset_of_ForwardSpeed_0(),
	MovementSettings_t1096092444::get_offset_of_BackwardSpeed_1(),
	MovementSettings_t1096092444::get_offset_of_StrafeSpeed_2(),
	MovementSettings_t1096092444::get_offset_of_RunMultiplier_3(),
	MovementSettings_t1096092444::get_offset_of_RunKey_4(),
	MovementSettings_t1096092444::get_offset_of_JumpForce_5(),
	MovementSettings_t1096092444::get_offset_of_SlopeCurveModifier_6(),
	MovementSettings_t1096092444::get_offset_of_CurrentTargetSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (AdvancedSettings_t778418834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[4] = 
{
	AdvancedSettings_t778418834::get_offset_of_groundCheckDistance_0(),
	AdvancedSettings_t778418834::get_offset_of_stickToGroundHelperDistance_1(),
	AdvancedSettings_t778418834::get_offset_of_slowDownRate_2(),
	AdvancedSettings_t778418834::get_offset_of_airControl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (Ball_t2378314638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3452[6] = 
{
	Ball_t2378314638::get_offset_of_m_MovePower_2(),
	Ball_t2378314638::get_offset_of_m_UseTorque_3(),
	Ball_t2378314638::get_offset_of_m_MaxAngularVelocity_4(),
	Ball_t2378314638::get_offset_of_m_JumpPower_5(),
	0,
	Ball_t2378314638::get_offset_of_m_Rigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (BallUserControl_t2574698008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3453[5] = 
{
	BallUserControl_t2574698008::get_offset_of_ball_2(),
	BallUserControl_t2574698008::get_offset_of_move_3(),
	BallUserControl_t2574698008::get_offset_of_cam_4(),
	BallUserControl_t2574698008::get_offset_of_camForward_5(),
	BallUserControl_t2574698008::get_offset_of_jump_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (AxisTouchButton_t3522881333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3454[6] = 
{
	AxisTouchButton_t3522881333::get_offset_of_axisName_2(),
	AxisTouchButton_t3522881333::get_offset_of_axisValue_3(),
	AxisTouchButton_t3522881333::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3522881333::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3522881333::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3522881333::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (ButtonHandler_t823762219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3455[1] = 
{
	ButtonHandler_t823762219::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (CrossPlatformInputManager_t191731427), -1, sizeof(CrossPlatformInputManager_t191731427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3456[3] = 
{
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (ActiveInputMethod_t139315314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3457[3] = 
{
	ActiveInputMethod_t139315314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (VirtualAxis_t4087348596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3458[3] = 
{
	VirtualAxis_t4087348596::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t4087348596::get_offset_of_m_Value_1(),
	VirtualAxis_t4087348596::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (VirtualButton_t2756566330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3459[5] = 
{
	VirtualButton_t2756566330::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2756566330::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2756566330::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2756566330::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2756566330::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (InputAxisScrollbar_t457958266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3460[1] = 
{
	InputAxisScrollbar_t457958266::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (Joystick_t2204371675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3461[9] = 
{
	Joystick_t2204371675::get_offset_of_MovementRange_2(),
	Joystick_t2204371675::get_offset_of_axesToUse_3(),
	Joystick_t2204371675::get_offset_of_horizontalAxisName_4(),
	Joystick_t2204371675::get_offset_of_verticalAxisName_5(),
	Joystick_t2204371675::get_offset_of_m_StartPos_6(),
	Joystick_t2204371675::get_offset_of_m_UseX_7(),
	Joystick_t2204371675::get_offset_of_m_UseY_8(),
	Joystick_t2204371675::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t2204371675::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (AxisOption_t3128671669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3462[4] = 
{
	AxisOption_t3128671669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (MobileControlRig_t1964600252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (MobileInput_t2025745297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (StandaloneInput_t1343950252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (TiltInput_t1639936653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3466[5] = 
{
	TiltInput_t1639936653::get_offset_of_mapping_2(),
	TiltInput_t1639936653::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t1639936653::get_offset_of_fullTiltAngle_4(),
	TiltInput_t1639936653::get_offset_of_centreAngleOffset_5(),
	TiltInput_t1639936653::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (AxisOptions_t3101732129)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3467[3] = 
{
	AxisOptions_t3101732129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (AxisMapping_t3982445645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[2] = 
{
	AxisMapping_t3982445645::get_offset_of_type_0(),
	AxisMapping_t3982445645::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (MappingType_t2039944511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3469[5] = 
{
	MappingType_t2039944511::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (TouchPad_t539039257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3470[18] = 
{
	TouchPad_t539039257::get_offset_of_axesToUse_2(),
	TouchPad_t539039257::get_offset_of_controlStyle_3(),
	TouchPad_t539039257::get_offset_of_horizontalAxisName_4(),
	TouchPad_t539039257::get_offset_of_verticalAxisName_5(),
	TouchPad_t539039257::get_offset_of_Xsensitivity_6(),
	TouchPad_t539039257::get_offset_of_Ysensitivity_7(),
	TouchPad_t539039257::get_offset_of_m_StartPos_8(),
	TouchPad_t539039257::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t539039257::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t539039257::get_offset_of_m_UseX_11(),
	TouchPad_t539039257::get_offset_of_m_UseY_12(),
	TouchPad_t539039257::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t539039257::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t539039257::get_offset_of_m_Dragging_15(),
	TouchPad_t539039257::get_offset_of_m_Id_16(),
	TouchPad_t539039257::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t539039257::get_offset_of_m_Center_18(),
	TouchPad_t539039257::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (AxisOption_t1372819835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3471[4] = 
{
	AxisOption_t1372819835::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (ControlStyle_t1372986211)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3472[4] = 
{
	ControlStyle_t1372986211::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (VirtualInput_t2597455733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3473[4] = 
{
	VirtualInput_t2597455733::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t2597455733::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (ActivateTrigger_t3349759092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[5] = 
{
	ActivateTrigger_t3349759092::get_offset_of_action_2(),
	ActivateTrigger_t3349759092::get_offset_of_target_3(),
	ActivateTrigger_t3349759092::get_offset_of_source_4(),
	ActivateTrigger_t3349759092::get_offset_of_triggerCount_5(),
	ActivateTrigger_t3349759092::get_offset_of_repeatTrigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (Mode_t3024470803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3475[7] = 
{
	Mode_t3024470803::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (AutoMobileShaderSwitch_t568447889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3476[1] = 
{
	AutoMobileShaderSwitch_t568447889::get_offset_of_m_ReplacementList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (ReplacementDefinition_t2693741842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3477[2] = 
{
	ReplacementDefinition_t2693741842::get_offset_of_original_0(),
	ReplacementDefinition_t2693741842::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (ReplacementList_t1887104210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[1] = 
{
	ReplacementList_t1887104210::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (AutoMoveAndRotate_t2437913015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3479[4] = 
{
	AutoMoveAndRotate_t2437913015::get_offset_of_moveUnitsPerSecond_2(),
	AutoMoveAndRotate_t2437913015::get_offset_of_rotateDegreesPerSecond_3(),
	AutoMoveAndRotate_t2437913015::get_offset_of_ignoreTimescale_4(),
	AutoMoveAndRotate_t2437913015::get_offset_of_m_LastRealTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (Vector3andSpace_t219844479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3480[2] = 
{
	Vector3andSpace_t219844479::get_offset_of_value_0(),
	Vector3andSpace_t219844479::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (CameraRefocus_t4263235746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[5] = 
{
	CameraRefocus_t4263235746::get_offset_of_Camera_0(),
	CameraRefocus_t4263235746::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t4263235746::get_offset_of_Parent_2(),
	CameraRefocus_t4263235746::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t4263235746::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (CurveControlledBob_t2679313829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3482[9] = 
{
	CurveControlledBob_t2679313829::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2679313829::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2679313829::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2679313829::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2679313829::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2679313829::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2679313829::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (DragRigidbody_t1600652016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t1600652016::get_offset_of_m_SpringJoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (U3CDragObjectU3Ec__Iterator0_t4151609119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[9] = 
{
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdDragU3E__0_0(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdAngularDragU3E__0_1(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CmainCameraU3E__0_2(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CrayU3E__1_3(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_distance_4(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24this_5(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24current_6(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24disposing_7(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (DynamicShadowSettings_t59119858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3485[11] = 
{
	DynamicShadowSettings_t59119858::get_offset_of_sunLight_2(),
	DynamicShadowSettings_t59119858::get_offset_of_minHeight_3(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowDistance_4(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowBias_5(),
	DynamicShadowSettings_t59119858::get_offset_of_maxHeight_6(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowDistance_7(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowBias_8(),
	DynamicShadowSettings_t59119858::get_offset_of_adaptTime_9(),
	DynamicShadowSettings_t59119858::get_offset_of_m_SmoothHeight_10(),
	DynamicShadowSettings_t59119858::get_offset_of_m_ChangeSpeed_11(),
	DynamicShadowSettings_t59119858::get_offset_of_m_OriginalStrength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (FollowTarget_t166153614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3486[2] = 
{
	FollowTarget_t166153614::get_offset_of_target_2(),
	FollowTarget_t166153614::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (ForcedReset_t301124368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (FOVKick_t120370150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3488[6] = 
{
	FOVKick_t120370150::get_offset_of_Camera_0(),
	FOVKick_t120370150::get_offset_of_originalFov_1(),
	FOVKick_t120370150::get_offset_of_FOVIncrease_2(),
	FOVKick_t120370150::get_offset_of_TimeToIncrease_3(),
	FOVKick_t120370150::get_offset_of_TimeToDecrease_4(),
	FOVKick_t120370150::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (U3CFOVKickUpU3Ec__Iterator0_t3738408313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3489[5] = 
{
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24this_1(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24current_2(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24disposing_3(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (U3CFOVKickDownU3Ec__Iterator1_t1440840980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3490[5] = 
{
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24this_1(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24current_2(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24disposing_3(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (FPSCounter_t2351221284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3491[6] = 
{
	0,
	FPSCounter_t2351221284::get_offset_of_m_FpsAccumulator_3(),
	FPSCounter_t2351221284::get_offset_of_m_FpsNextPeriod_4(),
	FPSCounter_t2351221284::get_offset_of_m_CurrentFps_5(),
	0,
	FPSCounter_t2351221284::get_offset_of_m_GuiText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (LerpControlledBob_t1895875871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3492[3] = 
{
	LerpControlledBob_t1895875871::get_offset_of_BobDuration_0(),
	LerpControlledBob_t1895875871::get_offset_of_BobAmount_1(),
	LerpControlledBob_t1895875871::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (U3CDoBobCycleU3Ec__Iterator0_t1149538828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3493[5] = 
{
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U3CtU3E__0_0(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24this_1(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24current_2(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24disposing_3(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (ObjectResetter_t639177103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3494[4] = 
{
	ObjectResetter_t639177103::get_offset_of_originalPosition_2(),
	ObjectResetter_t639177103::get_offset_of_originalRotation_3(),
	ObjectResetter_t639177103::get_offset_of_originalStructure_4(),
	ObjectResetter_t639177103::get_offset_of_Rigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (U3CResetCoroutineU3Ec__Iterator0_t3232105836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3495[7] = 
{
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_delay_0(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar0_1(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar1_2(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24this_3(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24current_4(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24disposing_5(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (ParticleSystemDestroyer_t558680695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3496[4] = 
{
	ParticleSystemDestroyer_t558680695::get_offset_of_minDuration_2(),
	ParticleSystemDestroyer_t558680695::get_offset_of_maxDuration_3(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_MaxLifetime_4(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_EarlyStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (U3CStartU3Ec__Iterator0_t980021917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3497[10] = 
{
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CsystemsU3E__0_0(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar1_2(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CstopTimeU3E__0_3(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar2_4(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar3_5(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (PlatformSpecificContent_t1404549723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3498[4] = 
{
	PlatformSpecificContent_t1404549723::get_offset_of_m_BuildTargetGroup_2(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_Content_3(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_MonoBehaviours_4(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_ChildrenOfThisObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (BuildTargetGroup_t72322187)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3499[3] = 
{
	BuildTargetGroup_t72322187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
