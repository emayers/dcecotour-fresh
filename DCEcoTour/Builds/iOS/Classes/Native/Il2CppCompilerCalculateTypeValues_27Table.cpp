﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mapbox.Json.Utilities.PropertyNameTable/Entry[]
struct EntryU5BU5D_t3603550186;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_t1796590042;
// System.Func`2<System.Reflection.MemberInfo,System.String>
struct Func_2_t3967597302;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t3692615456;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t1761491126;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Type
struct Type_t;
// Mapbox.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_0
struct U3CU3Ec__DisplayClass43_0_t1903311374;
// Mapbox.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t3284513685;
// Mapbox.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t2905058752;
// Mapbox.Json.Serialization.JsonProperty
struct JsonProperty_t1629976260;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2447130374;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2470008838;
// System.Func`1<System.Object>
struct Func_1_t2509852811;
// Mapbox.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t3081361529;
// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t3033093969;
// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2523184513;
// Mapbox.Json.Serialization.NamingStrategy
struct NamingStrategy_t3171016818;
// Mapbox.Json.Serialization.ErrorContext
struct ErrorContext_t3853262553;
// Mapbox.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_t2891093856;
// Mapbox.Json.JsonSerializer
struct JsonSerializer_t2280177768;
// Mapbox.Json.Serialization.ITraceWriter
struct ITraceWriter_t420207159;
// Mapbox.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t2669598769;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Exception
struct Exception_t;
// System.String
struct String_t;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_t2217434578;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>
struct Func_2_t2823819620;
// System.Func`2<Mapbox.Json.Serialization.JsonProperty,System.Int32>
struct Func_2_t1643706843;
// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t1036574653;
// Mapbox.Json.Utilities.TypeInformation[]
struct TypeInformationU5BU5D_t2210396381;
// Mapbox.Json.Utilities.ThreadSafeStore`2<Mapbox.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>
struct ThreadSafeStore_2_t1914384274;
// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.JsonProperty>
struct IList_1_t3445296043;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t5769829;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Collections.Generic.IDictionary`2<System.String,Mapbox.Json.Utilities.ReflectionMember>
struct IDictionary_2_t2713060643;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String>
struct Func_2_t2419460300;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.UInt64[]
struct UInt64U5BU5D_t1659327989;
// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,Mapbox.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>
struct ThreadSafeStore_2_t3956639673;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.Dictionary`2<System.String,Mapbox.Json.Serialization.JsonProperty>
struct Dictionary_2_t1415232559;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// Mapbox.Json.Utilities.ThreadSafeStore`2<Mapbox.Json.Utilities.TypeNameKey,System.Type>
struct ThreadSafeStore_2_t869880030;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<Mapbox.Json.Serialization.JsonProperty>
struct List_1_t3102051002;
// Mapbox.Json.Serialization.IContractResolver
struct IContractResolver_t2761620673;
// Mapbox.Json.JsonConverter[]
struct JsonConverterU5BU5D_t1616679288;
// Mapbox.Json.Utilities.PropertyNameTable
struct PropertyNameTable_t42095745;
// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.Serialization.JsonContract>
struct Dictionary_2_t3412898200;
// System.Collections.Generic.List`1<Mapbox.Json.Serialization.SerializationCallback>
struct List_1_t3209604541;
// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback>
struct IList_1_t3552849582;
// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationErrorCallback>
struct IList_1_t2627238525;
// Mapbox.Json.JsonConverter
struct JsonConverter_t472504469;
// Mapbox.Json.Serialization.JsonContract
struct JsonContract_t968551136;
// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.ReadType>
struct Dictionary_2_t2631974886;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1510070208;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Mapbox.Json.Serialization.IValueProvider
struct IValueProvider_t2840820099;
// Mapbox.Json.Serialization.IAttributeProvider
struct IAttributeProvider_t3323163628;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Collections.Generic.List`1<Mapbox.Json.JsonPosition>
struct List_1_t2313153536;
// Mapbox.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t691804306;
// System.Func`2<System.String,System.String>
struct Func_2_t3947292210;
// Mapbox.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t1665309380;
// Mapbox.Json.Serialization.ExtensionDataSetter
struct ExtensionDataSetter_t1236496959;
// Mapbox.Json.Serialization.ExtensionDataGetter
struct ExtensionDataGetter_t1148678719;
// Mapbox.Json.JsonWriter
struct JsonWriter_t260835314;
// Mapbox.Json.JsonTextWriter
struct JsonTextWriter_t990966167;
// System.IO.StringWriter
struct StringWriter_t802263757;
// Mapbox.Json.JsonReader
struct JsonReader_t1879223345;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef PROPERTYNAMETABLE_T42095745_H
#define PROPERTYNAMETABLE_T42095745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.PropertyNameTable
struct  PropertyNameTable_t42095745  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Json.Utilities.PropertyNameTable::_count
	int32_t ____count_1;
	// Mapbox.Json.Utilities.PropertyNameTable/Entry[] Mapbox.Json.Utilities.PropertyNameTable::_entries
	EntryU5BU5D_t3603550186* ____entries_2;
	// System.Int32 Mapbox.Json.Utilities.PropertyNameTable::_mask
	int32_t ____mask_3;

public:
	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(PropertyNameTable_t42095745, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__entries_2() { return static_cast<int32_t>(offsetof(PropertyNameTable_t42095745, ____entries_2)); }
	inline EntryU5BU5D_t3603550186* get__entries_2() const { return ____entries_2; }
	inline EntryU5BU5D_t3603550186** get_address_of__entries_2() { return &____entries_2; }
	inline void set__entries_2(EntryU5BU5D_t3603550186* value)
	{
		____entries_2 = value;
		Il2CppCodeGenWriteBarrier((&____entries_2), value);
	}

	inline static int32_t get_offset_of__mask_3() { return static_cast<int32_t>(offsetof(PropertyNameTable_t42095745, ____mask_3)); }
	inline int32_t get__mask_3() const { return ____mask_3; }
	inline int32_t* get_address_of__mask_3() { return &____mask_3; }
	inline void set__mask_3(int32_t value)
	{
		____mask_3 = value;
	}
};

struct PropertyNameTable_t42095745_StaticFields
{
public:
	// System.Int32 Mapbox.Json.Utilities.PropertyNameTable::HashCodeRandomizer
	int32_t ___HashCodeRandomizer_0;

public:
	inline static int32_t get_offset_of_HashCodeRandomizer_0() { return static_cast<int32_t>(offsetof(PropertyNameTable_t42095745_StaticFields, ___HashCodeRandomizer_0)); }
	inline int32_t get_HashCodeRandomizer_0() const { return ___HashCodeRandomizer_0; }
	inline int32_t* get_address_of_HashCodeRandomizer_0() { return &___HashCodeRandomizer_0; }
	inline void set_HashCodeRandomizer_0(int32_t value)
	{
		___HashCodeRandomizer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMETABLE_T42095745_H
#ifndef REFERENCEEQUALSEQUALITYCOMPARER_T742836987_H
#define REFERENCEEQUALSEQUALITYCOMPARER_T742836987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalBase/ReferenceEqualsEqualityComparer
struct  ReferenceEqualsEqualityComparer_t742836987  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEEQUALSEQUALITYCOMPARER_T742836987_H
#ifndef REFLECTIONATTRIBUTEPROVIDER_T2348600233_H
#define REFLECTIONATTRIBUTEPROVIDER_T2348600233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.ReflectionAttributeProvider
struct  ReflectionAttributeProvider_t2348600233  : public RuntimeObject
{
public:
	// System.Object Mapbox.Json.Serialization.ReflectionAttributeProvider::_attributeProvider
	RuntimeObject * ____attributeProvider_0;

public:
	inline static int32_t get_offset_of__attributeProvider_0() { return static_cast<int32_t>(offsetof(ReflectionAttributeProvider_t2348600233, ____attributeProvider_0)); }
	inline RuntimeObject * get__attributeProvider_0() const { return ____attributeProvider_0; }
	inline RuntimeObject ** get_address_of__attributeProvider_0() { return &____attributeProvider_0; }
	inline void set__attributeProvider_0(RuntimeObject * value)
	{
		____attributeProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&____attributeProvider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONATTRIBUTEPROVIDER_T2348600233_H
#ifndef NAMINGSTRATEGY_T3171016818_H
#define NAMINGSTRATEGY_T3171016818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.NamingStrategy
struct  NamingStrategy_t3171016818  : public RuntimeObject
{
public:
	// System.Boolean Mapbox.Json.Serialization.NamingStrategy::<ProcessDictionaryKeys>k__BackingField
	bool ___U3CProcessDictionaryKeysU3Ek__BackingField_0;
	// System.Boolean Mapbox.Json.Serialization.NamingStrategy::<ProcessExtensionDataNames>k__BackingField
	bool ___U3CProcessExtensionDataNamesU3Ek__BackingField_1;
	// System.Boolean Mapbox.Json.Serialization.NamingStrategy::<OverrideSpecifiedNames>k__BackingField
	bool ___U3COverrideSpecifiedNamesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CProcessDictionaryKeysU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NamingStrategy_t3171016818, ___U3CProcessDictionaryKeysU3Ek__BackingField_0)); }
	inline bool get_U3CProcessDictionaryKeysU3Ek__BackingField_0() const { return ___U3CProcessDictionaryKeysU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CProcessDictionaryKeysU3Ek__BackingField_0() { return &___U3CProcessDictionaryKeysU3Ek__BackingField_0; }
	inline void set_U3CProcessDictionaryKeysU3Ek__BackingField_0(bool value)
	{
		___U3CProcessDictionaryKeysU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CProcessExtensionDataNamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamingStrategy_t3171016818, ___U3CProcessExtensionDataNamesU3Ek__BackingField_1)); }
	inline bool get_U3CProcessExtensionDataNamesU3Ek__BackingField_1() const { return ___U3CProcessExtensionDataNamesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CProcessExtensionDataNamesU3Ek__BackingField_1() { return &___U3CProcessExtensionDataNamesU3Ek__BackingField_1; }
	inline void set_U3CProcessExtensionDataNamesU3Ek__BackingField_1(bool value)
	{
		___U3CProcessExtensionDataNamesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COverrideSpecifiedNamesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NamingStrategy_t3171016818, ___U3COverrideSpecifiedNamesU3Ek__BackingField_2)); }
	inline bool get_U3COverrideSpecifiedNamesU3Ek__BackingField_2() const { return ___U3COverrideSpecifiedNamesU3Ek__BackingField_2; }
	inline bool* get_address_of_U3COverrideSpecifiedNamesU3Ek__BackingField_2() { return &___U3COverrideSpecifiedNamesU3Ek__BackingField_2; }
	inline void set_U3COverrideSpecifiedNamesU3Ek__BackingField_2(bool value)
	{
		___U3COverrideSpecifiedNamesU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMINGSTRATEGY_T3171016818_H
#ifndef VALIDATIONUTILS_T1614134767_H
#define VALIDATIONUTILS_T1614134767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ValidationUtils
struct  ValidationUtils_t1614134767  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONUTILS_T1614134767_H
#ifndef TYPEEXTENSIONS_T4252539098_H
#define TYPEEXTENSIONS_T4252539098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.TypeExtensions
struct  TypeExtensions_t4252539098  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T4252539098_H
#ifndef REFLECTIONUTILS_T1035580559_H
#define REFLECTIONUTILS_T1035580559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionUtils
struct  ReflectionUtils_t1035580559  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t1035580559_StaticFields
{
public:
	// System.Type[] Mapbox.Json.Utilities.ReflectionUtils::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t1035580559_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T1035580559_H
#ifndef U3CU3EC_T3985673557_H
#define U3CU3EC_T3985673557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionUtils/<>c
struct  U3CU3Ec_t3985673557  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3985673557_StaticFields
{
public:
	// Mapbox.Json.Utilities.ReflectionUtils/<>c Mapbox.Json.Utilities.ReflectionUtils/<>c::<>9
	U3CU3Ec_t3985673557 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Mapbox.Json.Utilities.ReflectionUtils/<>c::<>9__11_0
	Func_2_t1796590042 * ___U3CU3E9__11_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.String> Mapbox.Json.Utilities.ReflectionUtils/<>c::<>9__30_0
	Func_2_t3967597302 * ___U3CU3E9__30_0_2;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Mapbox.Json.Utilities.ReflectionUtils/<>c::<>9__38_0
	Func_2_t3692615456 * ___U3CU3E9__38_0_3;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Mapbox.Json.Utilities.ReflectionUtils/<>c::<>9__40_0
	Func_2_t1761491126 * ___U3CU3E9__40_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3985673557_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3985673557 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3985673557 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3985673557 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3985673557_StaticFields, ___U3CU3E9__11_0_1)); }
	inline Func_2_t1796590042 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline Func_2_t1796590042 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(Func_2_t1796590042 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3985673557_StaticFields, ___U3CU3E9__30_0_2)); }
	inline Func_2_t3967597302 * get_U3CU3E9__30_0_2() const { return ___U3CU3E9__30_0_2; }
	inline Func_2_t3967597302 ** get_address_of_U3CU3E9__30_0_2() { return &___U3CU3E9__30_0_2; }
	inline void set_U3CU3E9__30_0_2(Func_2_t3967597302 * value)
	{
		___U3CU3E9__30_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3985673557_StaticFields, ___U3CU3E9__38_0_3)); }
	inline Func_2_t3692615456 * get_U3CU3E9__38_0_3() const { return ___U3CU3E9__38_0_3; }
	inline Func_2_t3692615456 ** get_address_of_U3CU3E9__38_0_3() { return &___U3CU3E9__38_0_3; }
	inline void set_U3CU3E9__38_0_3(Func_2_t3692615456 * value)
	{
		___U3CU3E9__38_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__38_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3985673557_StaticFields, ___U3CU3E9__40_0_4)); }
	inline Func_2_t1761491126 * get_U3CU3E9__40_0_4() const { return ___U3CU3E9__40_0_4; }
	inline Func_2_t1761491126 ** get_address_of_U3CU3E9__40_0_4() { return &___U3CU3E9__40_0_4; }
	inline void set_U3CU3E9__40_0_4(Func_2_t1761491126 * value)
	{
		___U3CU3E9__40_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3985673557_H
#ifndef U3CU3EC__DISPLAYCLASS43_0_T1903311374_H
#define U3CU3EC__DISPLAYCLASS43_0_T1903311374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t1903311374  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Mapbox.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_0::subTypeProperty
	PropertyInfo_t * ___subTypeProperty_0;

public:
	inline static int32_t get_offset_of_subTypeProperty_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t1903311374, ___subTypeProperty_0)); }
	inline PropertyInfo_t * get_subTypeProperty_0() const { return ___subTypeProperty_0; }
	inline PropertyInfo_t ** get_address_of_subTypeProperty_0() { return &___subTypeProperty_0; }
	inline void set_subTypeProperty_0(PropertyInfo_t * value)
	{
		___subTypeProperty_0 = value;
		Il2CppCodeGenWriteBarrier((&___subTypeProperty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_0_T1903311374_H
#ifndef U3CU3EC__DISPLAYCLASS43_1_T3469395315_H
#define U3CU3EC__DISPLAYCLASS43_1_T3469395315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_1
struct  U3CU3Ec__DisplayClass43_1_t3469395315  : public RuntimeObject
{
public:
	// System.Type Mapbox.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_1::subTypePropertyDeclaringType
	Type_t * ___subTypePropertyDeclaringType_0;
	// Mapbox.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_0 Mapbox.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass43_0_t1903311374 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_subTypePropertyDeclaringType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_1_t3469395315, ___subTypePropertyDeclaringType_0)); }
	inline Type_t * get_subTypePropertyDeclaringType_0() const { return ___subTypePropertyDeclaringType_0; }
	inline Type_t ** get_address_of_subTypePropertyDeclaringType_0() { return &___subTypePropertyDeclaringType_0; }
	inline void set_subTypePropertyDeclaringType_0(Type_t * value)
	{
		___subTypePropertyDeclaringType_0 = value;
		Il2CppCodeGenWriteBarrier((&___subTypePropertyDeclaringType_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_1_t3469395315, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass43_0_t1903311374 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass43_0_t1903311374 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass43_0_t1903311374 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_1_T3469395315_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRINGUTILS_T1714050313_H
#define STRINGUTILS_T1714050313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.StringUtils
struct  StringUtils_t1714050313  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T1714050313_H
#ifndef JSONFORMATTERCONVERTER_T3163187665_H
#define JSONFORMATTERCONVERTER_T3163187665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonFormatterConverter
struct  JsonFormatterConverter_t3163187665  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.JsonSerializerInternalReader Mapbox.Json.Serialization.JsonFormatterConverter::_reader
	JsonSerializerInternalReader_t3284513685 * ____reader_0;
	// Mapbox.Json.Serialization.JsonISerializableContract Mapbox.Json.Serialization.JsonFormatterConverter::_contract
	JsonISerializableContract_t2905058752 * ____contract_1;
	// Mapbox.Json.Serialization.JsonProperty Mapbox.Json.Serialization.JsonFormatterConverter::_member
	JsonProperty_t1629976260 * ____member_2;

public:
	inline static int32_t get_offset_of__reader_0() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t3163187665, ____reader_0)); }
	inline JsonSerializerInternalReader_t3284513685 * get__reader_0() const { return ____reader_0; }
	inline JsonSerializerInternalReader_t3284513685 ** get_address_of__reader_0() { return &____reader_0; }
	inline void set__reader_0(JsonSerializerInternalReader_t3284513685 * value)
	{
		____reader_0 = value;
		Il2CppCodeGenWriteBarrier((&____reader_0), value);
	}

	inline static int32_t get_offset_of__contract_1() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t3163187665, ____contract_1)); }
	inline JsonISerializableContract_t2905058752 * get__contract_1() const { return ____contract_1; }
	inline JsonISerializableContract_t2905058752 ** get_address_of__contract_1() { return &____contract_1; }
	inline void set__contract_1(JsonISerializableContract_t2905058752 * value)
	{
		____contract_1 = value;
		Il2CppCodeGenWriteBarrier((&____contract_1), value);
	}

	inline static int32_t get_offset_of__member_2() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t3163187665, ____member_2)); }
	inline JsonProperty_t1629976260 * get__member_2() const { return ____member_2; }
	inline JsonProperty_t1629976260 ** get_address_of__member_2() { return &____member_2; }
	inline void set__member_2(JsonProperty_t1629976260 * value)
	{
		____member_2 = value;
		Il2CppCodeGenWriteBarrier((&____member_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONFORMATTERCONVERTER_T3163187665_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T3033093969_H
#define U3CU3EC__DISPLAYCLASS33_0_T3033093969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t3033093969  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_0::getExtensionDataDictionary
	Func_2_t2447130374 * ___getExtensionDataDictionary_0;
	// System.Reflection.MemberInfo Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_0::member
	MemberInfo_t * ___member_1;

public:
	inline static int32_t get_offset_of_getExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t3033093969, ___getExtensionDataDictionary_0)); }
	inline Func_2_t2447130374 * get_getExtensionDataDictionary_0() const { return ___getExtensionDataDictionary_0; }
	inline Func_2_t2447130374 ** get_address_of_getExtensionDataDictionary_0() { return &___getExtensionDataDictionary_0; }
	inline void set_getExtensionDataDictionary_0(Func_2_t2447130374 * value)
	{
		___getExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___getExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_member_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t3033093969, ___member_1)); }
	inline MemberInfo_t * get_member_1() const { return ___member_1; }
	inline MemberInfo_t ** get_address_of_member_1() { return &___member_1; }
	inline void set_member_1(MemberInfo_t * value)
	{
		___member_1 = value;
		Il2CppCodeGenWriteBarrier((&___member_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T3033093969_H
#ifndef U3CU3EC__DISPLAYCLASS33_1_T1467010028_H
#define U3CU3EC__DISPLAYCLASS33_1_T1467010028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_1
struct  U3CU3Ec__DisplayClass33_1_t1467010028  : public RuntimeObject
{
public:
	// System.Action`2<System.Object,System.Object> Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_1::setExtensionDataDictionary
	Action_2_t2470008838 * ___setExtensionDataDictionary_0;
	// System.Func`1<System.Object> Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_1::createExtensionDataDictionary
	Func_1_t2509852811 * ___createExtensionDataDictionary_1;
	// Mapbox.Json.Utilities.MethodCall`2<System.Object,System.Object> Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_1::setExtensionDataDictionaryValue
	MethodCall_2_t3081361529 * ___setExtensionDataDictionaryValue_2;
	// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_0 Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass33_0_t3033093969 * ___CSU24U3CU3E8__locals1_3;

public:
	inline static int32_t get_offset_of_setExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_1_t1467010028, ___setExtensionDataDictionary_0)); }
	inline Action_2_t2470008838 * get_setExtensionDataDictionary_0() const { return ___setExtensionDataDictionary_0; }
	inline Action_2_t2470008838 ** get_address_of_setExtensionDataDictionary_0() { return &___setExtensionDataDictionary_0; }
	inline void set_setExtensionDataDictionary_0(Action_2_t2470008838 * value)
	{
		___setExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_createExtensionDataDictionary_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_1_t1467010028, ___createExtensionDataDictionary_1)); }
	inline Func_1_t2509852811 * get_createExtensionDataDictionary_1() const { return ___createExtensionDataDictionary_1; }
	inline Func_1_t2509852811 ** get_address_of_createExtensionDataDictionary_1() { return &___createExtensionDataDictionary_1; }
	inline void set_createExtensionDataDictionary_1(Func_1_t2509852811 * value)
	{
		___createExtensionDataDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___createExtensionDataDictionary_1), value);
	}

	inline static int32_t get_offset_of_setExtensionDataDictionaryValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_1_t1467010028, ___setExtensionDataDictionaryValue_2)); }
	inline MethodCall_2_t3081361529 * get_setExtensionDataDictionaryValue_2() const { return ___setExtensionDataDictionaryValue_2; }
	inline MethodCall_2_t3081361529 ** get_address_of_setExtensionDataDictionaryValue_2() { return &___setExtensionDataDictionaryValue_2; }
	inline void set_setExtensionDataDictionaryValue_2(MethodCall_2_t3081361529 * value)
	{
		___setExtensionDataDictionaryValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionaryValue_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_1_t1467010028, ___CSU24U3CU3E8__locals1_3)); }
	inline U3CU3Ec__DisplayClass33_0_t3033093969 * get_CSU24U3CU3E8__locals1_3() const { return ___CSU24U3CU3E8__locals1_3; }
	inline U3CU3Ec__DisplayClass33_0_t3033093969 ** get_address_of_CSU24U3CU3E8__locals1_3() { return &___CSU24U3CU3E8__locals1_3; }
	inline void set_CSU24U3CU3E8__locals1_3(U3CU3Ec__DisplayClass33_0_t3033093969 * value)
	{
		___CSU24U3CU3E8__locals1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_1_T1467010028_H
#ifndef U3CU3EC__DISPLAYCLASS33_2_T4195893383_H
#define U3CU3EC__DISPLAYCLASS33_2_T4195893383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_2
struct  U3CU3Ec__DisplayClass33_2_t4195893383  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_2::createEnumerableWrapper
	ObjectConstructor_1_t2523184513 * ___createEnumerableWrapper_0;
	// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_0 Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass33_2::CS$<>8__locals2
	U3CU3Ec__DisplayClass33_0_t3033093969 * ___CSU24U3CU3E8__locals2_1;

public:
	inline static int32_t get_offset_of_createEnumerableWrapper_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_2_t4195893383, ___createEnumerableWrapper_0)); }
	inline ObjectConstructor_1_t2523184513 * get_createEnumerableWrapper_0() const { return ___createEnumerableWrapper_0; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of_createEnumerableWrapper_0() { return &___createEnumerableWrapper_0; }
	inline void set_createEnumerableWrapper_0(ObjectConstructor_1_t2523184513 * value)
	{
		___createEnumerableWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___createEnumerableWrapper_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_2_t4195893383, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass33_0_t3033093969 * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass33_0_t3033093969 ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass33_0_t3033093969 * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_2_T4195893383_H
#ifndef U3CU3EC__DISPLAYCLASS47_0_T3434764109_H
#define U3CU3EC__DISPLAYCLASS47_0_T3434764109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass47_0
struct  U3CU3Ec__DisplayClass47_0_t3434764109  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.NamingStrategy Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass47_0::namingStrategy
	NamingStrategy_t3171016818 * ___namingStrategy_0;

public:
	inline static int32_t get_offset_of_namingStrategy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass47_0_t3434764109, ___namingStrategy_0)); }
	inline NamingStrategy_t3171016818 * get_namingStrategy_0() const { return ___namingStrategy_0; }
	inline NamingStrategy_t3171016818 ** get_address_of_namingStrategy_0() { return &___namingStrategy_0; }
	inline void set_namingStrategy_0(NamingStrategy_t3171016818 * value)
	{
		___namingStrategy_0 = value;
		Il2CppCodeGenWriteBarrier((&___namingStrategy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS47_0_T3434764109_H
#ifndef U3CU3EC__DISPLAYCLASS64_0_T3702544208_H
#define U3CU3EC__DISPLAYCLASS64_0_T3702544208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass64_0
struct  U3CU3Ec__DisplayClass64_0_t3702544208  : public RuntimeObject
{
public:
	// Mapbox.Json.Utilities.MethodCall`2<System.Object,System.Object> Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass64_0::shouldSerializeCall
	MethodCall_2_t3081361529 * ___shouldSerializeCall_0;

public:
	inline static int32_t get_offset_of_shouldSerializeCall_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t3702544208, ___shouldSerializeCall_0)); }
	inline MethodCall_2_t3081361529 * get_shouldSerializeCall_0() const { return ___shouldSerializeCall_0; }
	inline MethodCall_2_t3081361529 ** get_address_of_shouldSerializeCall_0() { return &___shouldSerializeCall_0; }
	inline void set_shouldSerializeCall_0(MethodCall_2_t3081361529 * value)
	{
		___shouldSerializeCall_0 = value;
		Il2CppCodeGenWriteBarrier((&___shouldSerializeCall_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS64_0_T3702544208_H
#ifndef U3CU3EC__DISPLAYCLASS65_0_T3702544207_H
#define U3CU3EC__DISPLAYCLASS65_0_T3702544207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_t3702544207  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass65_0::specifiedPropertyGet
	Func_2_t2447130374 * ___specifiedPropertyGet_0;

public:
	inline static int32_t get_offset_of_specifiedPropertyGet_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t3702544207, ___specifiedPropertyGet_0)); }
	inline Func_2_t2447130374 * get_specifiedPropertyGet_0() const { return ___specifiedPropertyGet_0; }
	inline Func_2_t2447130374 ** get_address_of_specifiedPropertyGet_0() { return &___specifiedPropertyGet_0; }
	inline void set_specifiedPropertyGet_0(Func_2_t2447130374 * value)
	{
		___specifiedPropertyGet_0 = value;
		Il2CppCodeGenWriteBarrier((&___specifiedPropertyGet_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS65_0_T3702544207_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T3033093971_H
#define U3CU3EC__DISPLAYCLASS31_0_T3033093971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t3033093971  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.NamingStrategy Mapbox.Json.Serialization.DefaultContractResolver/<>c__DisplayClass31_0::namingStrategy
	NamingStrategy_t3171016818 * ___namingStrategy_0;

public:
	inline static int32_t get_offset_of_namingStrategy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t3033093971, ___namingStrategy_0)); }
	inline NamingStrategy_t3171016818 * get_namingStrategy_0() const { return ___namingStrategy_0; }
	inline NamingStrategy_t3171016818 ** get_address_of_namingStrategy_0() { return &___namingStrategy_0; }
	inline void set_namingStrategy_0(NamingStrategy_t3171016818 * value)
	{
		___namingStrategy_0 = value;
		Il2CppCodeGenWriteBarrier((&___namingStrategy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T3033093971_H
#ifndef JSONSERIALIZERINTERNALBASE_T2072393995_H
#define JSONSERIALIZERINTERNALBASE_T2072393995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_t2072393995  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.ErrorContext Mapbox.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_t3853262553 * ____currentErrorContext_0;
	// Mapbox.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Mapbox.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_t2891093856 * ____mappings_1;
	// Mapbox.Json.JsonSerializer Mapbox.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t2280177768 * ___Serializer_2;
	// Mapbox.Json.Serialization.ITraceWriter Mapbox.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	RuntimeObject* ___TraceWriter_3;
	// Mapbox.Json.Serialization.JsonSerializerProxy Mapbox.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t2669598769 * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ____currentErrorContext_0)); }
	inline ErrorContext_t3853262553 * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_t3853262553 ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_t3853262553 * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentErrorContext_0), value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ____mappings_1)); }
	inline BidirectionalDictionary_2_t2891093856 * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_t2891093856 ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_t2891093856 * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_1), value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ___Serializer_2)); }
	inline JsonSerializer_t2280177768 * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t2280177768 ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t2280177768 * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_2), value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ___TraceWriter_3)); }
	inline RuntimeObject* get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline RuntimeObject** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(RuntimeObject* value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___TraceWriter_3), value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t2669598769 * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t2669598769 ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t2669598769 * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InternalSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALBASE_T2072393995_H
#ifndef U3CU3EC__DISPLAYCLASS59_0_T3999951579_H
#define U3CU3EC__DISPLAYCLASS59_0_T3999951579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonContract/<>c__DisplayClass59_0
struct  U3CU3Ec__DisplayClass59_0_t3999951579  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Mapbox.Json.Serialization.JsonContract/<>c__DisplayClass59_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t3999951579, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS59_0_T3999951579_H
#ifndef U3CU3EC__DISPLAYCLASS58_0_T3999886043_H
#define U3CU3EC__DISPLAYCLASS58_0_T3999886043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonContract/<>c__DisplayClass58_0
struct  U3CU3Ec__DisplayClass58_0_t3999886043  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Mapbox.Json.Serialization.JsonContract/<>c__DisplayClass58_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass58_0_t3999886043, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS58_0_T3999886043_H
#ifndef DEFAULTREFERENCERESOLVER_T674819107_H
#define DEFAULTREFERENCERESOLVER_T674819107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultReferenceResolver
struct  DefaultReferenceResolver_t674819107  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Json.Serialization.DefaultReferenceResolver::_referenceCount
	int32_t ____referenceCount_0;

public:
	inline static int32_t get_offset_of__referenceCount_0() { return static_cast<int32_t>(offsetof(DefaultReferenceResolver_t674819107, ____referenceCount_0)); }
	inline int32_t get__referenceCount_0() const { return ____referenceCount_0; }
	inline int32_t* get_address_of__referenceCount_0() { return &____referenceCount_0; }
	inline void set__referenceCount_0(int32_t value)
	{
		____referenceCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTREFERENCERESOLVER_T674819107_H
#ifndef ERRORCONTEXT_T3853262553_H
#define ERRORCONTEXT_T3853262553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.ErrorContext
struct  ErrorContext_t3853262553  : public RuntimeObject
{
public:
	// System.Boolean Mapbox.Json.Serialization.ErrorContext::<Traced>k__BackingField
	bool ___U3CTracedU3Ek__BackingField_0;
	// System.Exception Mapbox.Json.Serialization.ErrorContext::<Error>k__BackingField
	Exception_t * ___U3CErrorU3Ek__BackingField_1;
	// System.Object Mapbox.Json.Serialization.ErrorContext::<OriginalObject>k__BackingField
	RuntimeObject * ___U3COriginalObjectU3Ek__BackingField_2;
	// System.Object Mapbox.Json.Serialization.ErrorContext::<Member>k__BackingField
	RuntimeObject * ___U3CMemberU3Ek__BackingField_3;
	// System.String Mapbox.Json.Serialization.ErrorContext::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_4;
	// System.Boolean Mapbox.Json.Serialization.ErrorContext::<Handled>k__BackingField
	bool ___U3CHandledU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTracedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorContext_t3853262553, ___U3CTracedU3Ek__BackingField_0)); }
	inline bool get_U3CTracedU3Ek__BackingField_0() const { return ___U3CTracedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTracedU3Ek__BackingField_0() { return &___U3CTracedU3Ek__BackingField_0; }
	inline void set_U3CTracedU3Ek__BackingField_0(bool value)
	{
		___U3CTracedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorContext_t3853262553, ___U3CErrorU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CErrorU3Ek__BackingField_1() const { return ___U3CErrorU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CErrorU3Ek__BackingField_1() { return &___U3CErrorU3Ek__BackingField_1; }
	inline void set_U3CErrorU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CErrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COriginalObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorContext_t3853262553, ___U3COriginalObjectU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3COriginalObjectU3Ek__BackingField_2() const { return ___U3COriginalObjectU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3COriginalObjectU3Ek__BackingField_2() { return &___U3COriginalObjectU3Ek__BackingField_2; }
	inline void set_U3COriginalObjectU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3COriginalObjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalObjectU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMemberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorContext_t3853262553, ___U3CMemberU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CMemberU3Ek__BackingField_3() const { return ___U3CMemberU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CMemberU3Ek__BackingField_3() { return &___U3CMemberU3Ek__BackingField_3; }
	inline void set_U3CMemberU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CMemberU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ErrorContext_t3853262553, ___U3CPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CPathU3Ek__BackingField_4() const { return ___U3CPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_4() { return &___U3CPathU3Ek__BackingField_4; }
	inline void set_U3CPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHandledU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ErrorContext_t3853262553, ___U3CHandledU3Ek__BackingField_5)); }
	inline bool get_U3CHandledU3Ek__BackingField_5() const { return ___U3CHandledU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CHandledU3Ek__BackingField_5() { return &___U3CHandledU3Ek__BackingField_5; }
	inline void set_U3CHandledU3Ek__BackingField_5(bool value)
	{
		___U3CHandledU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCONTEXT_T3853262553_H
#ifndef U3CU3EC_T4006129445_H
#define U3CU3EC_T4006129445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver/<>c
struct  U3CU3Ec_t4006129445  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4006129445_StaticFields
{
public:
	// Mapbox.Json.Serialization.DefaultContractResolver/<>c Mapbox.Json.Serialization.DefaultContractResolver/<>c::<>9
	U3CU3Ec_t4006129445 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Mapbox.Json.Serialization.DefaultContractResolver/<>c::<>9__29_0
	Func_2_t2217434578 * ___U3CU3E9__29_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Mapbox.Json.Serialization.DefaultContractResolver/<>c::<>9__29_1
	Func_2_t2217434578 * ___U3CU3E9__29_1_2;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>> Mapbox.Json.Serialization.DefaultContractResolver/<>c::<>9__32_0
	Func_2_t2823819620 * ___U3CU3E9__32_0_3;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Mapbox.Json.Serialization.DefaultContractResolver/<>c::<>9__32_1
	Func_2_t2217434578 * ___U3CU3E9__32_1_4;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Mapbox.Json.Serialization.DefaultContractResolver/<>c::<>9__35_0
	Func_2_t1796590042 * ___U3CU3E9__35_0_5;
	// System.Func`2<Mapbox.Json.Serialization.JsonProperty,System.Int32> Mapbox.Json.Serialization.DefaultContractResolver/<>c::<>9__59_0
	Func_2_t1643706843 * ___U3CU3E9__59_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4006129445_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4006129445 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4006129445 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4006129445 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4006129445_StaticFields, ___U3CU3E9__29_0_1)); }
	inline Func_2_t2217434578 * get_U3CU3E9__29_0_1() const { return ___U3CU3E9__29_0_1; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__29_0_1() { return &___U3CU3E9__29_0_1; }
	inline void set_U3CU3E9__29_0_1(Func_2_t2217434578 * value)
	{
		___U3CU3E9__29_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4006129445_StaticFields, ___U3CU3E9__29_1_2)); }
	inline Func_2_t2217434578 * get_U3CU3E9__29_1_2() const { return ___U3CU3E9__29_1_2; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__29_1_2() { return &___U3CU3E9__29_1_2; }
	inline void set_U3CU3E9__29_1_2(Func_2_t2217434578 * value)
	{
		___U3CU3E9__29_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4006129445_StaticFields, ___U3CU3E9__32_0_3)); }
	inline Func_2_t2823819620 * get_U3CU3E9__32_0_3() const { return ___U3CU3E9__32_0_3; }
	inline Func_2_t2823819620 ** get_address_of_U3CU3E9__32_0_3() { return &___U3CU3E9__32_0_3; }
	inline void set_U3CU3E9__32_0_3(Func_2_t2823819620 * value)
	{
		___U3CU3E9__32_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4006129445_StaticFields, ___U3CU3E9__32_1_4)); }
	inline Func_2_t2217434578 * get_U3CU3E9__32_1_4() const { return ___U3CU3E9__32_1_4; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__32_1_4() { return &___U3CU3E9__32_1_4; }
	inline void set_U3CU3E9__32_1_4(Func_2_t2217434578 * value)
	{
		___U3CU3E9__32_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4006129445_StaticFields, ___U3CU3E9__35_0_5)); }
	inline Func_2_t1796590042 * get_U3CU3E9__35_0_5() const { return ___U3CU3E9__35_0_5; }
	inline Func_2_t1796590042 ** get_address_of_U3CU3E9__35_0_5() { return &___U3CU3E9__35_0_5; }
	inline void set_U3CU3E9__35_0_5(Func_2_t1796590042 * value)
	{
		___U3CU3E9__35_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__35_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__59_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4006129445_StaticFields, ___U3CU3E9__59_0_6)); }
	inline Func_2_t1643706843 * get_U3CU3E9__59_0_6() const { return ___U3CU3E9__59_0_6; }
	inline Func_2_t1643706843 ** get_address_of_U3CU3E9__59_0_6() { return &___U3CU3E9__59_0_6; }
	inline void set_U3CU3E9__59_0_6(Func_2_t1643706843 * value)
	{
		___U3CU3E9__59_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__59_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4006129445_H
#ifndef MISCELLANEOUSUTILS_T4176861027_H
#define MISCELLANEOUSUTILS_T4176861027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.MiscellaneousUtils
struct  MiscellaneousUtils_t4176861027  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCELLANEOUSUTILS_T4176861027_H
#ifndef CONVERTUTILS_T1194917319_H
#define CONVERTUTILS_T1194917319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ConvertUtils
struct  ConvertUtils_t1194917319  : public RuntimeObject
{
public:

public:
};

struct ConvertUtils_t1194917319_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.Utilities.PrimitiveTypeCode> Mapbox.Json.Utilities.ConvertUtils::TypeCodeMap
	Dictionary_2_t1036574653 * ___TypeCodeMap_0;
	// Mapbox.Json.Utilities.TypeInformation[] Mapbox.Json.Utilities.ConvertUtils::PrimitiveTypeCodes
	TypeInformationU5BU5D_t2210396381* ___PrimitiveTypeCodes_1;
	// Mapbox.Json.Utilities.ThreadSafeStore`2<Mapbox.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>> Mapbox.Json.Utilities.ConvertUtils::CastConverters
	ThreadSafeStore_2_t1914384274 * ___CastConverters_2;

public:
	inline static int32_t get_offset_of_TypeCodeMap_0() { return static_cast<int32_t>(offsetof(ConvertUtils_t1194917319_StaticFields, ___TypeCodeMap_0)); }
	inline Dictionary_2_t1036574653 * get_TypeCodeMap_0() const { return ___TypeCodeMap_0; }
	inline Dictionary_2_t1036574653 ** get_address_of_TypeCodeMap_0() { return &___TypeCodeMap_0; }
	inline void set_TypeCodeMap_0(Dictionary_2_t1036574653 * value)
	{
		___TypeCodeMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___TypeCodeMap_0), value);
	}

	inline static int32_t get_offset_of_PrimitiveTypeCodes_1() { return static_cast<int32_t>(offsetof(ConvertUtils_t1194917319_StaticFields, ___PrimitiveTypeCodes_1)); }
	inline TypeInformationU5BU5D_t2210396381* get_PrimitiveTypeCodes_1() const { return ___PrimitiveTypeCodes_1; }
	inline TypeInformationU5BU5D_t2210396381** get_address_of_PrimitiveTypeCodes_1() { return &___PrimitiveTypeCodes_1; }
	inline void set_PrimitiveTypeCodes_1(TypeInformationU5BU5D_t2210396381* value)
	{
		___PrimitiveTypeCodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveTypeCodes_1), value);
	}

	inline static int32_t get_offset_of_CastConverters_2() { return static_cast<int32_t>(offsetof(ConvertUtils_t1194917319_StaticFields, ___CastConverters_2)); }
	inline ThreadSafeStore_2_t1914384274 * get_CastConverters_2() const { return ___CastConverters_2; }
	inline ThreadSafeStore_2_t1914384274 ** get_address_of_CastConverters_2() { return &___CastConverters_2; }
	inline void set_CastConverters_2(ThreadSafeStore_2_t1914384274 * value)
	{
		___CastConverters_2 = value;
		Il2CppCodeGenWriteBarrier((&___CastConverters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTUTILS_T1194917319_H
#ifndef SERIALIZATIONBINDER_T274213469_H
#define SERIALIZATIONBINDER_T274213469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationBinder
struct  SerializationBinder_t274213469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONBINDER_T274213469_H
#ifndef STRINGREFERENCEEXTENSIONS_T1696722107_H
#define STRINGREFERENCEEXTENSIONS_T1696722107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.StringReferenceExtensions
struct  StringReferenceExtensions_t1696722107  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREFERENCEEXTENSIONS_T1696722107_H
#ifndef U3CU3EC__DISPLAYCLASS13_2_T2956558378_H
#define U3CU3EC__DISPLAYCLASS13_2_T2956558378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionObject/<>c__DisplayClass13_2
struct  U3CU3Ec__DisplayClass13_2_t2956558378  : public RuntimeObject
{
public:
	// Mapbox.Json.Utilities.MethodCall`2<System.Object,System.Object> Mapbox.Json.Utilities.ReflectionObject/<>c__DisplayClass13_2::call
	MethodCall_2_t3081361529 * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_2_t2956558378, ___call_0)); }
	inline MethodCall_2_t3081361529 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t3081361529 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t3081361529 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_2_T2956558378_H
#ifndef U3CU3EC__DISPLAYCLASS13_1_T1382580266_H
#define U3CU3EC__DISPLAYCLASS13_1_T1382580266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1
struct  U3CU3Ec__DisplayClass13_1_t1382580266  : public RuntimeObject
{
public:
	// Mapbox.Json.Utilities.MethodCall`2<System.Object,System.Object> Mapbox.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1::call
	MethodCall_2_t3081361529 * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_1_t1382580266, ___call_0)); }
	inline MethodCall_2_t3081361529 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t3081361529 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t3081361529 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_1_T1382580266_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T3338895402_H
#define U3CU3EC__DISPLAYCLASS13_0_T3338895402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionObject/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t3338895402  : public RuntimeObject
{
public:
	// System.Func`1<System.Object> Mapbox.Json.Utilities.ReflectionObject/<>c__DisplayClass13_0::ctor
	Func_1_t2509852811 * ___ctor_0;

public:
	inline static int32_t get_offset_of_ctor_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t3338895402, ___ctor_0)); }
	inline Func_1_t2509852811 * get_ctor_0() const { return ___ctor_0; }
	inline Func_1_t2509852811 ** get_address_of_ctor_0() { return &___ctor_0; }
	inline void set_ctor_0(Func_1_t2509852811 * value)
	{
		___ctor_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T3338895402_H
#ifndef ENTRY_T1310988683_H
#define ENTRY_T1310988683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.PropertyNameTable/Entry
struct  Entry_t1310988683  : public RuntimeObject
{
public:
	// System.String Mapbox.Json.Utilities.PropertyNameTable/Entry::Value
	String_t* ___Value_0;
	// System.Int32 Mapbox.Json.Utilities.PropertyNameTable/Entry::HashCode
	int32_t ___HashCode_1;
	// Mapbox.Json.Utilities.PropertyNameTable/Entry Mapbox.Json.Utilities.PropertyNameTable/Entry::Next
	Entry_t1310988683 * ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_t1310988683, ___Value_0)); }
	inline String_t* get_Value_0() const { return ___Value_0; }
	inline String_t** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(String_t* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t1310988683, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t1310988683, ___Next_2)); }
	inline Entry_t1310988683 * get_Next_2() const { return ___Next_2; }
	inline Entry_t1310988683 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Entry_t1310988683 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T1310988683_H
#ifndef REFLECTIONDELEGATEFACTORY_T2629290932_H
#define REFLECTIONDELEGATEFACTORY_T2629290932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionDelegateFactory
struct  ReflectionDelegateFactory_t2629290932  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONDELEGATEFACTORY_T2629290932_H
#ifndef COLLECTION_1_T574332178_H
#define COLLECTION_1_T574332178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Mapbox.Json.Serialization.JsonProperty>
struct  Collection_1_t574332178  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t574332178, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t574332178, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T574332178_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T3305159926_H
#define U3CU3EC__DISPLAYCLASS3_0_T3305159926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t3305159926  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo Mapbox.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::c
	ConstructorInfo_t5769829 * ___c_0;
	// System.Reflection.MethodBase Mapbox.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::method
	MethodBase_t * ___method_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t3305159926, ___c_0)); }
	inline ConstructorInfo_t5769829 * get_c_0() const { return ___c_0; }
	inline ConstructorInfo_t5769829 ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(ConstructorInfo_t5769829 * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t3305159926, ___method_1)); }
	inline MethodBase_t * get_method_1() const { return ___method_1; }
	inline MethodBase_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodBase_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T3305159926_H
#ifndef REFLECTIONMEMBER_T168985657_H
#define REFLECTIONMEMBER_T168985657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionMember
struct  ReflectionMember_t168985657  : public RuntimeObject
{
public:
	// System.Type Mapbox.Json.Utilities.ReflectionMember::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_0;
	// System.Func`2<System.Object,System.Object> Mapbox.Json.Utilities.ReflectionMember::<Getter>k__BackingField
	Func_2_t2447130374 * ___U3CGetterU3Ek__BackingField_1;
	// System.Action`2<System.Object,System.Object> Mapbox.Json.Utilities.ReflectionMember::<Setter>k__BackingField
	Action_2_t2470008838 * ___U3CSetterU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionMember_t168985657, ___U3CMemberTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_0() const { return ___U3CMemberTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_0() { return &___U3CMemberTypeU3Ek__BackingField_0; }
	inline void set_U3CMemberTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGetterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionMember_t168985657, ___U3CGetterU3Ek__BackingField_1)); }
	inline Func_2_t2447130374 * get_U3CGetterU3Ek__BackingField_1() const { return ___U3CGetterU3Ek__BackingField_1; }
	inline Func_2_t2447130374 ** get_address_of_U3CGetterU3Ek__BackingField_1() { return &___U3CGetterU3Ek__BackingField_1; }
	inline void set_U3CGetterU3Ek__BackingField_1(Func_2_t2447130374 * value)
	{
		___U3CGetterU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetterU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSetterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReflectionMember_t168985657, ___U3CSetterU3Ek__BackingField_2)); }
	inline Action_2_t2470008838 * get_U3CSetterU3Ek__BackingField_2() const { return ___U3CSetterU3Ek__BackingField_2; }
	inline Action_2_t2470008838 ** get_address_of_U3CSetterU3Ek__BackingField_2() { return &___U3CSetterU3Ek__BackingField_2; }
	inline void set_U3CSetterU3Ek__BackingField_2(Action_2_t2470008838 * value)
	{
		___U3CSetterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetterU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMEMBER_T168985657_H
#ifndef REFLECTIONOBJECT_T2904696228_H
#define REFLECTIONOBJECT_T2904696228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ReflectionObject
struct  ReflectionObject_t2904696228  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Utilities.ReflectionObject::<Creator>k__BackingField
	ObjectConstructor_1_t2523184513 * ___U3CCreatorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Mapbox.Json.Utilities.ReflectionMember> Mapbox.Json.Utilities.ReflectionObject::<Members>k__BackingField
	RuntimeObject* ___U3CMembersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionObject_t2904696228, ___U3CCreatorU3Ek__BackingField_0)); }
	inline ObjectConstructor_1_t2523184513 * get_U3CCreatorU3Ek__BackingField_0() const { return ___U3CCreatorU3Ek__BackingField_0; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of_U3CCreatorU3Ek__BackingField_0() { return &___U3CCreatorU3Ek__BackingField_0; }
	inline void set_U3CCreatorU3Ek__BackingField_0(ObjectConstructor_1_t2523184513 * value)
	{
		___U3CCreatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMembersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionObject_t2904696228, ___U3CMembersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CMembersU3Ek__BackingField_1() const { return ___U3CMembersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CMembersU3Ek__BackingField_1() { return &___U3CMembersU3Ek__BackingField_1; }
	inline void set_U3CMembersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CMembersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMembersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONOBJECT_T2904696228_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T912528346_H
#define U3CU3EC__DISPLAYCLASS9_0_T912528346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t912528346  : public RuntimeObject
{
public:
	// Mapbox.Json.Utilities.MethodCall`2<System.Object,System.Object> Mapbox.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0::call
	MethodCall_2_t3081361529 * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t912528346, ___call_0)); }
	inline MethodCall_2_t3081361529 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t3081361529 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t3081361529 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T912528346_H
#ifndef DATETIMEUTILS_T327169568_H
#define DATETIMEUTILS_T327169568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.DateTimeUtils
struct  DateTimeUtils_t327169568  : public RuntimeObject
{
public:

public:
};

struct DateTimeUtils_t327169568_StaticFields
{
public:
	// System.Int64 Mapbox.Json.Utilities.DateTimeUtils::InitialJavaScriptDateTicks
	int64_t ___InitialJavaScriptDateTicks_0;
	// System.Int32[] Mapbox.Json.Utilities.DateTimeUtils::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_1;
	// System.Int32[] Mapbox.Json.Utilities.DateTimeUtils::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_2;

public:
	inline static int32_t get_offset_of_InitialJavaScriptDateTicks_0() { return static_cast<int32_t>(offsetof(DateTimeUtils_t327169568_StaticFields, ___InitialJavaScriptDateTicks_0)); }
	inline int64_t get_InitialJavaScriptDateTicks_0() const { return ___InitialJavaScriptDateTicks_0; }
	inline int64_t* get_address_of_InitialJavaScriptDateTicks_0() { return &___InitialJavaScriptDateTicks_0; }
	inline void set_InitialJavaScriptDateTicks_0(int64_t value)
	{
		___InitialJavaScriptDateTicks_0 = value;
	}

	inline static int32_t get_offset_of_DaysToMonth365_1() { return static_cast<int32_t>(offsetof(DateTimeUtils_t327169568_StaticFields, ___DaysToMonth365_1)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_1() const { return ___DaysToMonth365_1; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_1() { return &___DaysToMonth365_1; }
	inline void set_DaysToMonth365_1(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_1 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_1), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_2() { return static_cast<int32_t>(offsetof(DateTimeUtils_t327169568_StaticFields, ___DaysToMonth366_2)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_2() const { return ___DaysToMonth366_2; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_2() { return &___DaysToMonth366_2; }
	inline void set_DaysToMonth366_2(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_2 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUTILS_T327169568_H
#ifndef U3CU3EC_T1388036768_H
#define U3CU3EC_T1388036768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.EnumUtils/<>c
struct  U3CU3Ec_t1388036768  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1388036768_StaticFields
{
public:
	// Mapbox.Json.Utilities.EnumUtils/<>c Mapbox.Json.Utilities.EnumUtils/<>c::<>9
	U3CU3Ec_t1388036768 * ___U3CU3E9_0;
	// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String> Mapbox.Json.Utilities.EnumUtils/<>c::<>9__1_0
	Func_2_t2419460300 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1388036768_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1388036768 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1388036768 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1388036768 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1388036768_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t2419460300 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t2419460300 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t2419460300 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1388036768_H
#ifndef BUFFERUTILS_T2040455361_H
#define BUFFERUTILS_T2040455361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.BufferUtils
struct  BufferUtils_t2040455361  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERUTILS_T2040455361_H
#ifndef JAVASCRIPTUTILS_T3910841289_H
#define JAVASCRIPTUTILS_T3910841289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.JavaScriptUtils
struct  JavaScriptUtils_t3910841289  : public RuntimeObject
{
public:

public:
};

struct JavaScriptUtils_t3910841289_StaticFields
{
public:
	// System.Boolean[] Mapbox.Json.Utilities.JavaScriptUtils::SingleQuoteCharEscapeFlags
	BooleanU5BU5D_t2897418192* ___SingleQuoteCharEscapeFlags_0;
	// System.Boolean[] Mapbox.Json.Utilities.JavaScriptUtils::DoubleQuoteCharEscapeFlags
	BooleanU5BU5D_t2897418192* ___DoubleQuoteCharEscapeFlags_1;
	// System.Boolean[] Mapbox.Json.Utilities.JavaScriptUtils::HtmlCharEscapeFlags
	BooleanU5BU5D_t2897418192* ___HtmlCharEscapeFlags_2;

public:
	inline static int32_t get_offset_of_SingleQuoteCharEscapeFlags_0() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t3910841289_StaticFields, ___SingleQuoteCharEscapeFlags_0)); }
	inline BooleanU5BU5D_t2897418192* get_SingleQuoteCharEscapeFlags_0() const { return ___SingleQuoteCharEscapeFlags_0; }
	inline BooleanU5BU5D_t2897418192** get_address_of_SingleQuoteCharEscapeFlags_0() { return &___SingleQuoteCharEscapeFlags_0; }
	inline void set_SingleQuoteCharEscapeFlags_0(BooleanU5BU5D_t2897418192* value)
	{
		___SingleQuoteCharEscapeFlags_0 = value;
		Il2CppCodeGenWriteBarrier((&___SingleQuoteCharEscapeFlags_0), value);
	}

	inline static int32_t get_offset_of_DoubleQuoteCharEscapeFlags_1() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t3910841289_StaticFields, ___DoubleQuoteCharEscapeFlags_1)); }
	inline BooleanU5BU5D_t2897418192* get_DoubleQuoteCharEscapeFlags_1() const { return ___DoubleQuoteCharEscapeFlags_1; }
	inline BooleanU5BU5D_t2897418192** get_address_of_DoubleQuoteCharEscapeFlags_1() { return &___DoubleQuoteCharEscapeFlags_1; }
	inline void set_DoubleQuoteCharEscapeFlags_1(BooleanU5BU5D_t2897418192* value)
	{
		___DoubleQuoteCharEscapeFlags_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleQuoteCharEscapeFlags_1), value);
	}

	inline static int32_t get_offset_of_HtmlCharEscapeFlags_2() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t3910841289_StaticFields, ___HtmlCharEscapeFlags_2)); }
	inline BooleanU5BU5D_t2897418192* get_HtmlCharEscapeFlags_2() const { return ___HtmlCharEscapeFlags_2; }
	inline BooleanU5BU5D_t2897418192** get_address_of_HtmlCharEscapeFlags_2() { return &___HtmlCharEscapeFlags_2; }
	inline void set_HtmlCharEscapeFlags_2(BooleanU5BU5D_t2897418192* value)
	{
		___HtmlCharEscapeFlags_2 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlCharEscapeFlags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTUTILS_T3910841289_H
#ifndef IEEE754_T1074200465_H
#define IEEE754_T1074200465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ConvertUtils/IEEE754
struct  IEEE754_t1074200465  : public RuntimeObject
{
public:

public:
};

struct IEEE754_t1074200465_StaticFields
{
public:
	// System.Int32[] Mapbox.Json.Utilities.ConvertUtils/IEEE754::MultExp64Power10
	Int32U5BU5D_t385246372* ___MultExp64Power10_0;
	// System.UInt64[] Mapbox.Json.Utilities.ConvertUtils/IEEE754::MultVal64Power10
	UInt64U5BU5D_t1659327989* ___MultVal64Power10_1;
	// System.UInt64[] Mapbox.Json.Utilities.ConvertUtils/IEEE754::MultVal64Power10Inv
	UInt64U5BU5D_t1659327989* ___MultVal64Power10Inv_2;
	// System.Int32[] Mapbox.Json.Utilities.ConvertUtils/IEEE754::MultExp64Power10By16
	Int32U5BU5D_t385246372* ___MultExp64Power10By16_3;
	// System.UInt64[] Mapbox.Json.Utilities.ConvertUtils/IEEE754::MultVal64Power10By16
	UInt64U5BU5D_t1659327989* ___MultVal64Power10By16_4;
	// System.UInt64[] Mapbox.Json.Utilities.ConvertUtils/IEEE754::MultVal64Power10By16Inv
	UInt64U5BU5D_t1659327989* ___MultVal64Power10By16Inv_5;

public:
	inline static int32_t get_offset_of_MultExp64Power10_0() { return static_cast<int32_t>(offsetof(IEEE754_t1074200465_StaticFields, ___MultExp64Power10_0)); }
	inline Int32U5BU5D_t385246372* get_MultExp64Power10_0() const { return ___MultExp64Power10_0; }
	inline Int32U5BU5D_t385246372** get_address_of_MultExp64Power10_0() { return &___MultExp64Power10_0; }
	inline void set_MultExp64Power10_0(Int32U5BU5D_t385246372* value)
	{
		___MultExp64Power10_0 = value;
		Il2CppCodeGenWriteBarrier((&___MultExp64Power10_0), value);
	}

	inline static int32_t get_offset_of_MultVal64Power10_1() { return static_cast<int32_t>(offsetof(IEEE754_t1074200465_StaticFields, ___MultVal64Power10_1)); }
	inline UInt64U5BU5D_t1659327989* get_MultVal64Power10_1() const { return ___MultVal64Power10_1; }
	inline UInt64U5BU5D_t1659327989** get_address_of_MultVal64Power10_1() { return &___MultVal64Power10_1; }
	inline void set_MultVal64Power10_1(UInt64U5BU5D_t1659327989* value)
	{
		___MultVal64Power10_1 = value;
		Il2CppCodeGenWriteBarrier((&___MultVal64Power10_1), value);
	}

	inline static int32_t get_offset_of_MultVal64Power10Inv_2() { return static_cast<int32_t>(offsetof(IEEE754_t1074200465_StaticFields, ___MultVal64Power10Inv_2)); }
	inline UInt64U5BU5D_t1659327989* get_MultVal64Power10Inv_2() const { return ___MultVal64Power10Inv_2; }
	inline UInt64U5BU5D_t1659327989** get_address_of_MultVal64Power10Inv_2() { return &___MultVal64Power10Inv_2; }
	inline void set_MultVal64Power10Inv_2(UInt64U5BU5D_t1659327989* value)
	{
		___MultVal64Power10Inv_2 = value;
		Il2CppCodeGenWriteBarrier((&___MultVal64Power10Inv_2), value);
	}

	inline static int32_t get_offset_of_MultExp64Power10By16_3() { return static_cast<int32_t>(offsetof(IEEE754_t1074200465_StaticFields, ___MultExp64Power10By16_3)); }
	inline Int32U5BU5D_t385246372* get_MultExp64Power10By16_3() const { return ___MultExp64Power10By16_3; }
	inline Int32U5BU5D_t385246372** get_address_of_MultExp64Power10By16_3() { return &___MultExp64Power10By16_3; }
	inline void set_MultExp64Power10By16_3(Int32U5BU5D_t385246372* value)
	{
		___MultExp64Power10By16_3 = value;
		Il2CppCodeGenWriteBarrier((&___MultExp64Power10By16_3), value);
	}

	inline static int32_t get_offset_of_MultVal64Power10By16_4() { return static_cast<int32_t>(offsetof(IEEE754_t1074200465_StaticFields, ___MultVal64Power10By16_4)); }
	inline UInt64U5BU5D_t1659327989* get_MultVal64Power10By16_4() const { return ___MultVal64Power10By16_4; }
	inline UInt64U5BU5D_t1659327989** get_address_of_MultVal64Power10By16_4() { return &___MultVal64Power10By16_4; }
	inline void set_MultVal64Power10By16_4(UInt64U5BU5D_t1659327989* value)
	{
		___MultVal64Power10By16_4 = value;
		Il2CppCodeGenWriteBarrier((&___MultVal64Power10By16_4), value);
	}

	inline static int32_t get_offset_of_MultVal64Power10By16Inv_5() { return static_cast<int32_t>(offsetof(IEEE754_t1074200465_StaticFields, ___MultVal64Power10By16Inv_5)); }
	inline UInt64U5BU5D_t1659327989* get_MultVal64Power10By16Inv_5() const { return ___MultVal64Power10By16Inv_5; }
	inline UInt64U5BU5D_t1659327989** get_address_of_MultVal64Power10By16Inv_5() { return &___MultVal64Power10By16Inv_5; }
	inline void set_MultVal64Power10By16Inv_5(UInt64U5BU5D_t1659327989* value)
	{
		___MultVal64Power10By16Inv_5 = value;
		Il2CppCodeGenWriteBarrier((&___MultVal64Power10By16Inv_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IEEE754_T1074200465_H
#ifndef COLLECTIONUTILS_T3138840482_H
#define COLLECTIONUTILS_T3138840482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.CollectionUtils
struct  CollectionUtils_t3138840482  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONUTILS_T3138840482_H
#ifndef MATHUTILS_T1802260133_H
#define MATHUTILS_T1802260133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.MathUtils
struct  MathUtils_t1802260133  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T1802260133_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef ENUMUTILS_T741247584_H
#define ENUMUTILS_T741247584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.EnumUtils
struct  EnumUtils_t741247584  : public RuntimeObject
{
public:

public:
};

struct EnumUtils_t741247584_StaticFields
{
public:
	// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,Mapbox.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>> Mapbox.Json.Utilities.EnumUtils::EnumMemberNamesPerType
	ThreadSafeStore_2_t3956639673 * ___EnumMemberNamesPerType_0;

public:
	inline static int32_t get_offset_of_EnumMemberNamesPerType_0() { return static_cast<int32_t>(offsetof(EnumUtils_t741247584_StaticFields, ___EnumMemberNamesPerType_0)); }
	inline ThreadSafeStore_2_t3956639673 * get_EnumMemberNamesPerType_0() const { return ___EnumMemberNamesPerType_0; }
	inline ThreadSafeStore_2_t3956639673 ** get_address_of_EnumMemberNamesPerType_0() { return &___EnumMemberNamesPerType_0; }
	inline void set_EnumMemberNamesPerType_0(ThreadSafeStore_2_t3956639673 * value)
	{
		___EnumMemberNamesPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___EnumMemberNamesPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMUTILS_T741247584_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef KEYEDCOLLECTION_2_T554659863_H
#define KEYEDCOLLECTION_2_T554659863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.KeyedCollection`2<System.String,Mapbox.Json.Serialization.JsonProperty>
struct  KeyedCollection_2_t554659863  : public Collection_1_t574332178
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2::dictionary
	Dictionary_2_t1415232559 * ___dictionary_2;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.ObjectModel.KeyedCollection`2::comparer
	RuntimeObject* ___comparer_3;
	// System.Int32 System.Collections.ObjectModel.KeyedCollection`2::dictionaryCreationThreshold
	int32_t ___dictionaryCreationThreshold_4;

public:
	inline static int32_t get_offset_of_dictionary_2() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t554659863, ___dictionary_2)); }
	inline Dictionary_2_t1415232559 * get_dictionary_2() const { return ___dictionary_2; }
	inline Dictionary_2_t1415232559 ** get_address_of_dictionary_2() { return &___dictionary_2; }
	inline void set_dictionary_2(Dictionary_2_t1415232559 * value)
	{
		___dictionary_2 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_2), value);
	}

	inline static int32_t get_offset_of_comparer_3() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t554659863, ___comparer_3)); }
	inline RuntimeObject* get_comparer_3() const { return ___comparer_3; }
	inline RuntimeObject** get_address_of_comparer_3() { return &___comparer_3; }
	inline void set_comparer_3(RuntimeObject* value)
	{
		___comparer_3 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_3), value);
	}

	inline static int32_t get_offset_of_dictionaryCreationThreshold_4() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t554659863, ___dictionaryCreationThreshold_4)); }
	inline int32_t get_dictionaryCreationThreshold_4() const { return ___dictionaryCreationThreshold_4; }
	inline int32_t* get_address_of_dictionaryCreationThreshold_4() { return &___dictionaryCreationThreshold_4; }
	inline void set_dictionaryCreationThreshold_4(int32_t value)
	{
		___dictionaryCreationThreshold_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDCOLLECTION_2_T554659863_H
#ifndef JSONSERIALIZERINTERNALREADER_T3284513685_H
#define JSONSERIALIZERINTERNALREADER_T3284513685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalReader
struct  JsonSerializerInternalReader_t3284513685  : public JsonSerializerInternalBase_t2072393995
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALREADER_T3284513685_H
#ifndef DEFAULTSERIALIZATIONBINDER_T1191207404_H
#define DEFAULTSERIALIZATIONBINDER_T1191207404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultSerializationBinder
struct  DefaultSerializationBinder_t1191207404  : public SerializationBinder_t274213469
{
public:
	// Mapbox.Json.Utilities.ThreadSafeStore`2<Mapbox.Json.Utilities.TypeNameKey,System.Type> Mapbox.Json.Serialization.DefaultSerializationBinder::_typeCache
	ThreadSafeStore_2_t869880030 * ____typeCache_1;

public:
	inline static int32_t get_offset_of__typeCache_1() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t1191207404, ____typeCache_1)); }
	inline ThreadSafeStore_2_t869880030 * get__typeCache_1() const { return ____typeCache_1; }
	inline ThreadSafeStore_2_t869880030 ** get_address_of__typeCache_1() { return &____typeCache_1; }
	inline void set__typeCache_1(ThreadSafeStore_2_t869880030 * value)
	{
		____typeCache_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeCache_1), value);
	}
};

struct DefaultSerializationBinder_t1191207404_StaticFields
{
public:
	// Mapbox.Json.Serialization.DefaultSerializationBinder Mapbox.Json.Serialization.DefaultSerializationBinder::Instance
	DefaultSerializationBinder_t1191207404 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t1191207404_StaticFields, ___Instance_0)); }
	inline DefaultSerializationBinder_t1191207404 * get_Instance_0() const { return ___Instance_0; }
	inline DefaultSerializationBinder_t1191207404 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DefaultSerializationBinder_t1191207404 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSERIALIZATIONBINDER_T1191207404_H
#ifndef STRINGBUFFER_T3305294883_H
#define STRINGBUFFER_T3305294883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.StringBuffer
struct  StringBuffer_t3305294883 
{
public:
	// System.Char[] Mapbox.Json.Utilities.StringBuffer::_buffer
	CharU5BU5D_t3528271667* ____buffer_0;
	// System.Int32 Mapbox.Json.Utilities.StringBuffer::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(StringBuffer_t3305294883, ____buffer_0)); }
	inline CharU5BU5D_t3528271667* get__buffer_0() const { return ____buffer_0; }
	inline CharU5BU5D_t3528271667** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(CharU5BU5D_t3528271667* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(StringBuffer_t3305294883, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Json.Utilities.StringBuffer
struct StringBuffer_t3305294883_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
// Native definition for COM marshalling of Mapbox.Json.Utilities.StringBuffer
struct StringBuffer_t3305294883_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
#endif // STRINGBUFFER_T3305294883_H
#ifndef LATEBOUNDREFLECTIONDELEGATEFACTORY_T3245749508_H
#define LATEBOUNDREFLECTIONDELEGATEFACTORY_T3245749508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.LateBoundReflectionDelegateFactory
struct  LateBoundReflectionDelegateFactory_t3245749508  : public ReflectionDelegateFactory_t2629290932
{
public:

public:
};

struct LateBoundReflectionDelegateFactory_t3245749508_StaticFields
{
public:
	// Mapbox.Json.Utilities.LateBoundReflectionDelegateFactory Mapbox.Json.Utilities.LateBoundReflectionDelegateFactory::_instance
	LateBoundReflectionDelegateFactory_t3245749508 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LateBoundReflectionDelegateFactory_t3245749508_StaticFields, ____instance_0)); }
	inline LateBoundReflectionDelegateFactory_t3245749508 * get__instance_0() const { return ____instance_0; }
	inline LateBoundReflectionDelegateFactory_t3245749508 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LateBoundReflectionDelegateFactory_t3245749508 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEBOUNDREFLECTIONDELEGATEFACTORY_T3245749508_H
#ifndef STRINGREFERENCE_T2330690408_H
#define STRINGREFERENCE_T2330690408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.StringReference
struct  StringReference_t2330690408 
{
public:
	// System.Char[] Mapbox.Json.Utilities.StringReference::_chars
	CharU5BU5D_t3528271667* ____chars_0;
	// System.Int32 Mapbox.Json.Utilities.StringReference::_startIndex
	int32_t ____startIndex_1;
	// System.Int32 Mapbox.Json.Utilities.StringReference::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(StringReference_t2330690408, ____chars_0)); }
	inline CharU5BU5D_t3528271667* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t3528271667** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t3528271667* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of__startIndex_1() { return static_cast<int32_t>(offsetof(StringReference_t2330690408, ____startIndex_1)); }
	inline int32_t get__startIndex_1() const { return ____startIndex_1; }
	inline int32_t* get_address_of__startIndex_1() { return &____startIndex_1; }
	inline void set__startIndex_1(int32_t value)
	{
		____startIndex_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(StringReference_t2330690408, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Json.Utilities.StringReference
struct StringReference_t2330690408_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of Mapbox.Json.Utilities.StringReference
struct StringReference_t2330690408_marshaled_com
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
#endif // STRINGREFERENCE_T2330690408_H
#ifndef TYPECONVERTKEY_T1086254200_H
#define TYPECONVERTKEY_T1086254200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ConvertUtils/TypeConvertKey
struct  TypeConvertKey_t1086254200 
{
public:
	// System.Type Mapbox.Json.Utilities.ConvertUtils/TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Mapbox.Json.Utilities.ConvertUtils/TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t1086254200, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t1086254200, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t1086254200_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Mapbox.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t1086254200_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T1086254200_H
#ifndef TYPENAMEKEY_T750778342_H
#define TYPENAMEKEY_T750778342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.TypeNameKey
struct  TypeNameKey_t750778342 
{
public:
	// System.String Mapbox.Json.Utilities.TypeNameKey::AssemblyName
	String_t* ___AssemblyName_0;
	// System.String Mapbox.Json.Utilities.TypeNameKey::TypeName
	String_t* ___TypeName_1;

public:
	inline static int32_t get_offset_of_AssemblyName_0() { return static_cast<int32_t>(offsetof(TypeNameKey_t750778342, ___AssemblyName_0)); }
	inline String_t* get_AssemblyName_0() const { return ___AssemblyName_0; }
	inline String_t** get_address_of_AssemblyName_0() { return &___AssemblyName_0; }
	inline void set_AssemblyName_0(String_t* value)
	{
		___AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyName_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(TypeNameKey_t750778342, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Json.Utilities.TypeNameKey
struct TypeNameKey_t750778342_marshaled_pinvoke
{
	char* ___AssemblyName_0;
	char* ___TypeName_1;
};
// Native definition for COM marshalling of Mapbox.Json.Utilities.TypeNameKey
struct TypeNameKey_t750778342_marshaled_com
{
	Il2CppChar* ___AssemblyName_0;
	Il2CppChar* ___TypeName_1;
};
#endif // TYPENAMEKEY_T750778342_H
#ifndef ERROREVENTARGS_T2120017399_H
#define ERROREVENTARGS_T2120017399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.ErrorEventArgs
struct  ErrorEventArgs_t2120017399  : public EventArgs_t3591816995
{
public:
	// System.Object Mapbox.Json.Serialization.ErrorEventArgs::<CurrentObject>k__BackingField
	RuntimeObject * ___U3CCurrentObjectU3Ek__BackingField_1;
	// Mapbox.Json.Serialization.ErrorContext Mapbox.Json.Serialization.ErrorEventArgs::<ErrorContext>k__BackingField
	ErrorContext_t3853262553 * ___U3CErrorContextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t2120017399, ___U3CCurrentObjectU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CCurrentObjectU3Ek__BackingField_1() const { return ___U3CCurrentObjectU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CCurrentObjectU3Ek__BackingField_1() { return &___U3CCurrentObjectU3Ek__BackingField_1; }
	inline void set_U3CCurrentObjectU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CCurrentObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentObjectU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CErrorContextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t2120017399, ___U3CErrorContextU3Ek__BackingField_2)); }
	inline ErrorContext_t3853262553 * get_U3CErrorContextU3Ek__BackingField_2() const { return ___U3CErrorContextU3Ek__BackingField_2; }
	inline ErrorContext_t3853262553 ** get_address_of_U3CErrorContextU3Ek__BackingField_2() { return &___U3CErrorContextU3Ek__BackingField_2; }
	inline void set_U3CErrorContextU3Ek__BackingField_2(ErrorContext_t3853262553 * value)
	{
		___U3CErrorContextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorContextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T2120017399_H
#ifndef DATETIMEZONEHANDLING_T1164602176_H
#define DATETIMEZONEHANDLING_T1164602176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t1164602176 
{
public:
	// System.Int32 Mapbox.Json.DateTimeZoneHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t1164602176, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T1164602176_H
#ifndef STATE_T433406742_H
#define STATE_T433406742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonReader/State
struct  State_t433406742 
{
public:
	// System.Int32 Mapbox.Json.JsonReader/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t433406742, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T433406742_H
#ifndef JSONTOKEN_T339275982_H
#define JSONTOKEN_T339275982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonToken
struct  JsonToken_t339275982 
{
public:
	// System.Int32 Mapbox.Json.JsonToken::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonToken_t339275982, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T339275982_H
#ifndef CONVERTRESULT_T3062627003_H
#define CONVERTRESULT_T3062627003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ConvertUtils/ConvertResult
struct  ConvertResult_t3062627003 
{
public:
	// System.Int32 Mapbox.Json.Utilities.ConvertUtils/ConvertResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConvertResult_t3062627003, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTRESULT_T3062627003_H
#ifndef PRIMITIVETYPECODE_T2887194885_H
#define PRIMITIVETYPECODE_T2887194885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t2887194885 
{
public:
	// System.Int32 Mapbox.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t2887194885, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T2887194885_H
#ifndef READTYPE_T187627822_H
#define READTYPE_T187627822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.ReadType
struct  ReadType_t187627822 
{
public:
	// System.Int32 Mapbox.Json.ReadType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadType_t187627822, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T187627822_H
#ifndef MEMBERSERIALIZATION_T1508699431_H
#define MEMBERSERIALIZATION_T1508699431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.MemberSerialization
struct  MemberSerialization_t1508699431 
{
public:
	// System.Int32 Mapbox.Json.MemberSerialization::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MemberSerialization_t1508699431, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_T1508699431_H
#ifndef REFERENCELOOPHANDLING_T1937914466_H
#define REFERENCELOOPHANDLING_T1937914466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_t1937914466 
{
public:
	// System.Int32 Mapbox.Json.ReferenceLoopHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_t1937914466, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_T1937914466_H
#ifndef TYPENAMEHANDLING_T3932606415_H
#define TYPENAMEHANDLING_T3932606415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.TypeNameHandling
struct  TypeNameHandling_t3932606415 
{
public:
	// System.Int32 Mapbox.Json.TypeNameHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeNameHandling_t3932606415, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T3932606415_H
#ifndef PARSERESULT_T2731952844_H
#define PARSERESULT_T2731952844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.ParseResult
struct  ParseResult_t2731952844 
{
public:
	// System.Int32 Mapbox.Json.Utilities.ParseResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParseResult_t2731952844, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERESULT_T2731952844_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef DATEPARSEHANDLING_T2175047150_H
#define DATEPARSEHANDLING_T2175047150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DateParseHandling
struct  DateParseHandling_t2175047150 
{
public:
	// System.Int32 Mapbox.Json.DateParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateParseHandling_t2175047150, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T2175047150_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef STREAMINGCONTEXTSTATES_T3580100459_H
#define STREAMINGCONTEXTSTATES_T3580100459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t3580100459 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t3580100459, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T3580100459_H
#ifndef REQUIRED_T501185530_H
#define REQUIRED_T501185530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Required
struct  Required_t501185530 
{
public:
	// System.Int32 Mapbox.Json.Required::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Required_t501185530, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_T501185530_H
#ifndef NULLVALUEHANDLING_T225534363_H
#define NULLVALUEHANDLING_T225534363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.NullValueHandling
struct  NullValueHandling_t225534363 
{
public:
	// System.Int32 Mapbox.Json.NullValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NullValueHandling_t225534363, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T225534363_H
#ifndef DEFAULTVALUEHANDLING_T1787134227_H
#define DEFAULTVALUEHANDLING_T1787134227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DefaultValueHandling
struct  DefaultValueHandling_t1787134227 
{
public:
	// System.Int32 Mapbox.Json.DefaultValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t1787134227, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_T1787134227_H
#ifndef OBJECTCREATIONHANDLING_T848553946_H
#define OBJECTCREATIONHANDLING_T848553946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t848553946 
{
public:
	// System.Int32 Mapbox.Json.ObjectCreationHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t848553946, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T848553946_H
#ifndef JSONCONTRACTTYPE_T1860678823_H
#define JSONCONTRACTTYPE_T1860678823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonContractType
struct  JsonContractType_t1860678823 
{
public:
	// System.Int32 Mapbox.Json.Serialization.JsonContractType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContractType_t1860678823, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_T1860678823_H
#ifndef JSONCONTAINERTYPE_T4094137386_H
#define JSONCONTAINERTYPE_T4094137386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonContainerType
struct  JsonContainerType_t4094137386 
{
public:
	// System.Int32 Mapbox.Json.JsonContainerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContainerType_t4094137386, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T4094137386_H
#ifndef FLOATFORMATHANDLING_T496253539_H
#define FLOATFORMATHANDLING_T496253539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.FloatFormatHandling
struct  FloatFormatHandling_t496253539 
{
public:
	// System.Int32 Mapbox.Json.FloatFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatFormatHandling_t496253539, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_T496253539_H
#ifndef PROPERTYPRESENCE_T53006651_H
#define PROPERTYPRESENCE_T53006651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence
struct  PropertyPresence_t53006651 
{
public:
	// System.Int32 Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyPresence_t53006651, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYPRESENCE_T53006651_H
#ifndef FLOATPARSEHANDLING_T2358379491_H
#define FLOATPARSEHANDLING_T2358379491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.FloatParseHandling
struct  FloatParseHandling_t2358379491 
{
public:
	// System.Int32 Mapbox.Json.FloatParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatParseHandling_t2358379491, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T2358379491_H
#ifndef STATE_T664584870_H
#define STATE_T664584870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonWriter/State
struct  State_t664584870 
{
public:
	// System.Int32 Mapbox.Json.JsonWriter/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t664584870, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T664584870_H
#ifndef FORMATTING_T2995144369_H
#define FORMATTING_T2995144369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Formatting
struct  Formatting_t2995144369 
{
public:
	// System.Int32 Mapbox.Json.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t2995144369, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T2995144369_H
#ifndef DATEFORMATHANDLING_T4226114183_H
#define DATEFORMATHANDLING_T4226114183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DateFormatHandling
struct  DateFormatHandling_t4226114183 
{
public:
	// System.Int32 Mapbox.Json.DateFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateFormatHandling_t4226114183, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T4226114183_H
#ifndef JSONPROPERTYCOLLECTION_T1665309380_H
#define JSONPROPERTYCOLLECTION_T1665309380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonPropertyCollection
struct  JsonPropertyCollection_t1665309380  : public KeyedCollection_2_t554659863
{
public:
	// System.Type Mapbox.Json.Serialization.JsonPropertyCollection::_type
	Type_t * ____type_5;
	// System.Collections.Generic.List`1<Mapbox.Json.Serialization.JsonProperty> Mapbox.Json.Serialization.JsonPropertyCollection::_list
	List_1_t3102051002 * ____list_6;

public:
	inline static int32_t get_offset_of__type_5() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_t1665309380, ____type_5)); }
	inline Type_t * get__type_5() const { return ____type_5; }
	inline Type_t ** get_address_of__type_5() { return &____type_5; }
	inline void set__type_5(Type_t * value)
	{
		____type_5 = value;
		Il2CppCodeGenWriteBarrier((&____type_5), value);
	}

	inline static int32_t get_offset_of__list_6() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_t1665309380, ____list_6)); }
	inline List_1_t3102051002 * get__list_6() const { return ____list_6; }
	inline List_1_t3102051002 ** get_address_of__list_6() { return &____list_6; }
	inline void set__list_6(List_1_t3102051002 * value)
	{
		____list_6 = value;
		Il2CppCodeGenWriteBarrier((&____list_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTYCOLLECTION_T1665309380_H
#ifndef STRINGESCAPEHANDLING_T524910018_H
#define STRINGESCAPEHANDLING_T524910018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.StringEscapeHandling
struct  StringEscapeHandling_t524910018 
{
public:
	// System.Int32 Mapbox.Json.StringEscapeHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t524910018, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T524910018_H
#ifndef JSONPOSITION_T841078794_H
#define JSONPOSITION_T841078794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonPosition
struct  JsonPosition_t841078794 
{
public:
	// Mapbox.Json.JsonContainerType Mapbox.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Mapbox.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Mapbox.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Mapbox.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t841078794_StaticFields
{
public:
	// System.Char[] Mapbox.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t3528271667* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t3528271667* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t3528271667** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t3528271667* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Json.JsonPosition
struct JsonPosition_t841078794_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Mapbox.Json.JsonPosition
struct JsonPosition_t841078794_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T841078794_H
#ifndef NULLABLE_1_T2571116028_H
#define NULLABLE_1_T2571116028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.ObjectCreationHandling>
struct  Nullable_1_t2571116028 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2571116028, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2571116028, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2571116028_H
#ifndef NULLABLE_1_T3509696309_H
#define NULLABLE_1_T3509696309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.DefaultValueHandling>
struct  Nullable_1_t3509696309 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3509696309, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3509696309, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3509696309_H
#ifndef DEFAULTCONTRACTRESOLVER_T2734327692_H
#define DEFAULTCONTRACTRESOLVER_T2734327692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.DefaultContractResolver
struct  DefaultContractResolver_t2734327692  : public RuntimeObject
{
public:
	// System.Object Mapbox.Json.Serialization.DefaultContractResolver::_typeContractCacheLock
	RuntimeObject * ____typeContractCacheLock_2;
	// Mapbox.Json.Utilities.PropertyNameTable Mapbox.Json.Serialization.DefaultContractResolver::_nameTable
	PropertyNameTable_t42095745 * ____nameTable_3;
	// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.Serialization.JsonContract> Mapbox.Json.Serialization.DefaultContractResolver::_contractCache
	Dictionary_2_t3412898200 * ____contractCache_4;
	// System.Reflection.BindingFlags Mapbox.Json.Serialization.DefaultContractResolver::<DefaultMembersSearchFlags>k__BackingField
	int32_t ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5;
	// System.Boolean Mapbox.Json.Serialization.DefaultContractResolver::<SerializeCompilerGeneratedMembers>k__BackingField
	bool ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6;
	// System.Boolean Mapbox.Json.Serialization.DefaultContractResolver::<IgnoreSerializableInterface>k__BackingField
	bool ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7;
	// System.Boolean Mapbox.Json.Serialization.DefaultContractResolver::<IgnoreSerializableAttribute>k__BackingField
	bool ___U3CIgnoreSerializableAttributeU3Ek__BackingField_8;
	// Mapbox.Json.Serialization.NamingStrategy Mapbox.Json.Serialization.DefaultContractResolver::<NamingStrategy>k__BackingField
	NamingStrategy_t3171016818 * ___U3CNamingStrategyU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__typeContractCacheLock_2() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ____typeContractCacheLock_2)); }
	inline RuntimeObject * get__typeContractCacheLock_2() const { return ____typeContractCacheLock_2; }
	inline RuntimeObject ** get_address_of__typeContractCacheLock_2() { return &____typeContractCacheLock_2; }
	inline void set__typeContractCacheLock_2(RuntimeObject * value)
	{
		____typeContractCacheLock_2 = value;
		Il2CppCodeGenWriteBarrier((&____typeContractCacheLock_2), value);
	}

	inline static int32_t get_offset_of__nameTable_3() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ____nameTable_3)); }
	inline PropertyNameTable_t42095745 * get__nameTable_3() const { return ____nameTable_3; }
	inline PropertyNameTable_t42095745 ** get_address_of__nameTable_3() { return &____nameTable_3; }
	inline void set__nameTable_3(PropertyNameTable_t42095745 * value)
	{
		____nameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____nameTable_3), value);
	}

	inline static int32_t get_offset_of__contractCache_4() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ____contractCache_4)); }
	inline Dictionary_2_t3412898200 * get__contractCache_4() const { return ____contractCache_4; }
	inline Dictionary_2_t3412898200 ** get_address_of__contractCache_4() { return &____contractCache_4; }
	inline void set__contractCache_4(Dictionary_2_t3412898200 * value)
	{
		____contractCache_4 = value;
		Il2CppCodeGenWriteBarrier((&____contractCache_4), value);
	}

	inline static int32_t get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5)); }
	inline int32_t get_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5() const { return ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5() { return &___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5; }
	inline void set_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5(int32_t value)
	{
		___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6)); }
	inline bool get_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6() const { return ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6() { return &___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6; }
	inline void set_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6(bool value)
	{
		___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7)); }
	inline bool get_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7() const { return ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7() { return &___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7; }
	inline void set_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7(bool value)
	{
		___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ___U3CIgnoreSerializableAttributeU3Ek__BackingField_8)); }
	inline bool get_U3CIgnoreSerializableAttributeU3Ek__BackingField_8() const { return ___U3CIgnoreSerializableAttributeU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_8() { return &___U3CIgnoreSerializableAttributeU3Ek__BackingField_8; }
	inline void set_U3CIgnoreSerializableAttributeU3Ek__BackingField_8(bool value)
	{
		___U3CIgnoreSerializableAttributeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CNamingStrategyU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692, ___U3CNamingStrategyU3Ek__BackingField_9)); }
	inline NamingStrategy_t3171016818 * get_U3CNamingStrategyU3Ek__BackingField_9() const { return ___U3CNamingStrategyU3Ek__BackingField_9; }
	inline NamingStrategy_t3171016818 ** get_address_of_U3CNamingStrategyU3Ek__BackingField_9() { return &___U3CNamingStrategyU3Ek__BackingField_9; }
	inline void set_U3CNamingStrategyU3Ek__BackingField_9(NamingStrategy_t3171016818 * value)
	{
		___U3CNamingStrategyU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamingStrategyU3Ek__BackingField_9), value);
	}
};

struct DefaultContractResolver_t2734327692_StaticFields
{
public:
	// Mapbox.Json.Serialization.IContractResolver Mapbox.Json.Serialization.DefaultContractResolver::Instance
	RuntimeObject* ___Instance_0;
	// Mapbox.Json.JsonConverter[] Mapbox.Json.Serialization.DefaultContractResolver::BuiltInConverters
	JsonConverterU5BU5D_t1616679288* ___BuiltInConverters_1;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}

	inline static int32_t get_offset_of_BuiltInConverters_1() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t2734327692_StaticFields, ___BuiltInConverters_1)); }
	inline JsonConverterU5BU5D_t1616679288* get_BuiltInConverters_1() const { return ___BuiltInConverters_1; }
	inline JsonConverterU5BU5D_t1616679288** get_address_of_BuiltInConverters_1() { return &___BuiltInConverters_1; }
	inline void set_BuiltInConverters_1(JsonConverterU5BU5D_t1616679288* value)
	{
		___BuiltInConverters_1 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltInConverters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTRACTRESOLVER_T2734327692_H
#ifndef NULLABLE_1_T3660476548_H
#define NULLABLE_1_T3660476548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.ReferenceLoopHandling>
struct  Nullable_1_t3660476548 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3660476548, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3660476548, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3660476548_H
#ifndef NULLABLE_1_T1360201201_H
#define NULLABLE_1_T1360201201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.TypeNameHandling>
struct  Nullable_1_t1360201201 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1360201201, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1360201201, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1360201201_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T1948096445_H
#define NULLABLE_1_T1948096445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.NullValueHandling>
struct  Nullable_1_t1948096445 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1948096445, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1948096445, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1948096445_H
#ifndef NULLABLE_1_T2223747612_H
#define NULLABLE_1_T2223747612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.Required>
struct  Nullable_1_t2223747612 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2223747612, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2223747612, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2223747612_H
#ifndef STREAMINGCONTEXT_T3711869237_H
#define STREAMINGCONTEXT_T3711869237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t3711869237 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T3711869237_H
#ifndef JSONCONTRACT_T968551136_H
#define JSONCONTRACT_T968551136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonContract
struct  JsonContract_t968551136  : public RuntimeObject
{
public:
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Mapbox.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Mapbox.Json.ReadType Mapbox.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Mapbox.Json.Serialization.JsonContractType Mapbox.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_t3209604541 * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationErrorCallback> Mapbox.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Mapbox.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Mapbox.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Mapbox.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t1819850047  ___U3CIsReferenceU3Ek__BackingField_16;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t472504469 * ___U3CConverterU3Ek__BackingField_17;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t472504469 * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Mapbox.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t2509852811 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onDeserializedCallbacks_9)); }
	inline List_1_t3209604541 * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_t3209604541 ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_t3209604541 * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t1819850047  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t1819850047 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t1819850047  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t472504469 * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t472504469 ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t472504469 * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t472504469 * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t472504469 ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t472504469 * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t2509852811 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t2509852811 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t2509852811 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_T968551136_H
#ifndef TYPEINFORMATION_T178482004_H
#define TYPEINFORMATION_T178482004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.TypeInformation
struct  TypeInformation_t178482004  : public RuntimeObject
{
public:
	// System.Type Mapbox.Json.Utilities.TypeInformation::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_0;
	// Mapbox.Json.Utilities.PrimitiveTypeCode Mapbox.Json.Utilities.TypeInformation::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TypeInformation_t178482004, ___U3CTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TypeInformation_t178482004, ___U3CTypeCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_1() const { return ___U3CTypeCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_1() { return &___U3CTypeCodeU3Ek__BackingField_1; }
	inline void set_U3CTypeCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFORMATION_T178482004_H
#ifndef JSONCONTAINERCONTRACT_T790269270_H
#define JSONCONTAINERCONTRACT_T790269270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonContainerContract
struct  JsonContainerContract_t790269270  : public JsonContract_t968551136
{
public:
	// Mapbox.Json.Serialization.JsonContract Mapbox.Json.Serialization.JsonContainerContract::_itemContract
	JsonContract_t968551136 * ____itemContract_21;
	// Mapbox.Json.Serialization.JsonContract Mapbox.Json.Serialization.JsonContainerContract::_finalItemContract
	JsonContract_t968551136 * ____finalItemContract_22;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonContainerContract::<ItemConverter>k__BackingField
	JsonConverter_t472504469 * ___U3CItemConverterU3Ek__BackingField_23;
	// System.Nullable`1<System.Boolean> Mapbox.Json.Serialization.JsonContainerContract::<ItemIsReference>k__BackingField
	Nullable_1_t1819850047  ___U3CItemIsReferenceU3Ek__BackingField_24;
	// System.Nullable`1<Mapbox.Json.ReferenceLoopHandling> Mapbox.Json.Serialization.JsonContainerContract::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t3660476548  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25;
	// System.Nullable`1<Mapbox.Json.TypeNameHandling> Mapbox.Json.Serialization.JsonContainerContract::<ItemTypeNameHandling>k__BackingField
	Nullable_1_t1360201201  ___U3CItemTypeNameHandlingU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of__itemContract_21() { return static_cast<int32_t>(offsetof(JsonContainerContract_t790269270, ____itemContract_21)); }
	inline JsonContract_t968551136 * get__itemContract_21() const { return ____itemContract_21; }
	inline JsonContract_t968551136 ** get_address_of__itemContract_21() { return &____itemContract_21; }
	inline void set__itemContract_21(JsonContract_t968551136 * value)
	{
		____itemContract_21 = value;
		Il2CppCodeGenWriteBarrier((&____itemContract_21), value);
	}

	inline static int32_t get_offset_of__finalItemContract_22() { return static_cast<int32_t>(offsetof(JsonContainerContract_t790269270, ____finalItemContract_22)); }
	inline JsonContract_t968551136 * get__finalItemContract_22() const { return ____finalItemContract_22; }
	inline JsonContract_t968551136 ** get_address_of__finalItemContract_22() { return &____finalItemContract_22; }
	inline void set__finalItemContract_22(JsonContract_t968551136 * value)
	{
		____finalItemContract_22 = value;
		Il2CppCodeGenWriteBarrier((&____finalItemContract_22), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonContainerContract_t790269270, ___U3CItemConverterU3Ek__BackingField_23)); }
	inline JsonConverter_t472504469 * get_U3CItemConverterU3Ek__BackingField_23() const { return ___U3CItemConverterU3Ek__BackingField_23; }
	inline JsonConverter_t472504469 ** get_address_of_U3CItemConverterU3Ek__BackingField_23() { return &___U3CItemConverterU3Ek__BackingField_23; }
	inline void set_U3CItemConverterU3Ek__BackingField_23(JsonConverter_t472504469 * value)
	{
		___U3CItemConverterU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonContainerContract_t790269270, ___U3CItemIsReferenceU3Ek__BackingField_24)); }
	inline Nullable_1_t1819850047  get_U3CItemIsReferenceU3Ek__BackingField_24() const { return ___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline Nullable_1_t1819850047 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_24() { return &___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_24(Nullable_1_t1819850047  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonContainerContract_t790269270, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25)); }
	inline Nullable_1_t3660476548  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline Nullable_1_t3660476548 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(Nullable_1_t3660476548  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonContainerContract_t790269270, ___U3CItemTypeNameHandlingU3Ek__BackingField_26)); }
	inline Nullable_1_t1360201201  get_U3CItemTypeNameHandlingU3Ek__BackingField_26() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline Nullable_1_t1360201201 * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_26(Nullable_1_t1360201201  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERCONTRACT_T790269270_H
#ifndef JSONPRIMITIVECONTRACT_T959031370_H
#define JSONPRIMITIVECONTRACT_T959031370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonPrimitiveContract
struct  JsonPrimitiveContract_t959031370  : public JsonContract_t968551136
{
public:
	// Mapbox.Json.Utilities.PrimitiveTypeCode Mapbox.Json.Serialization.JsonPrimitiveContract::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t959031370, ___U3CTypeCodeU3Ek__BackingField_21)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_21() const { return ___U3CTypeCodeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_21() { return &___U3CTypeCodeU3Ek__BackingField_21; }
	inline void set_U3CTypeCodeU3Ek__BackingField_21(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_21 = value;
	}
};

struct JsonPrimitiveContract_t959031370_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.ReadType> Mapbox.Json.Serialization.JsonPrimitiveContract::ReadTypeMap
	Dictionary_2_t2631974886 * ___ReadTypeMap_22;

public:
	inline static int32_t get_offset_of_ReadTypeMap_22() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t959031370_StaticFields, ___ReadTypeMap_22)); }
	inline Dictionary_2_t2631974886 * get_ReadTypeMap_22() const { return ___ReadTypeMap_22; }
	inline Dictionary_2_t2631974886 ** get_address_of_ReadTypeMap_22() { return &___ReadTypeMap_22; }
	inline void set_ReadTypeMap_22(Dictionary_2_t2631974886 * value)
	{
		___ReadTypeMap_22 = value;
		Il2CppCodeGenWriteBarrier((&___ReadTypeMap_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPRIMITIVECONTRACT_T959031370_H
#ifndef JSONLINQCONTRACT_T1947983261_H
#define JSONLINQCONTRACT_T1947983261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonLinqContract
struct  JsonLinqContract_t1947983261  : public JsonContract_t968551136
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLINQCONTRACT_T1947983261_H
#ifndef EXTENSIONDATAGETTER_T1148678719_H
#define EXTENSIONDATAGETTER_T1148678719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.ExtensionDataGetter
struct  ExtensionDataGetter_t1148678719  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATAGETTER_T1148678719_H
#ifndef JSONPROPERTY_T1629976260_H
#define JSONPROPERTY_T1629976260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonProperty
struct  JsonProperty_t1629976260  : public RuntimeObject
{
public:
	// System.Nullable`1<Mapbox.Json.Required> Mapbox.Json.Serialization.JsonProperty::_required
	Nullable_1_t2223747612  ____required_0;
	// System.Boolean Mapbox.Json.Serialization.JsonProperty::_hasExplicitDefaultValue
	bool ____hasExplicitDefaultValue_1;
	// System.Object Mapbox.Json.Serialization.JsonProperty::_defaultValue
	RuntimeObject * ____defaultValue_2;
	// System.Boolean Mapbox.Json.Serialization.JsonProperty::_hasGeneratedDefaultValue
	bool ____hasGeneratedDefaultValue_3;
	// System.String Mapbox.Json.Serialization.JsonProperty::_propertyName
	String_t* ____propertyName_4;
	// System.Boolean Mapbox.Json.Serialization.JsonProperty::_skipPropertyNameEscape
	bool ____skipPropertyNameEscape_5;
	// System.Type Mapbox.Json.Serialization.JsonProperty::_propertyType
	Type_t * ____propertyType_6;
	// Mapbox.Json.Serialization.JsonContract Mapbox.Json.Serialization.JsonProperty::<PropertyContract>k__BackingField
	JsonContract_t968551136 * ___U3CPropertyContractU3Ek__BackingField_7;
	// System.Type Mapbox.Json.Serialization.JsonProperty::<DeclaringType>k__BackingField
	Type_t * ___U3CDeclaringTypeU3Ek__BackingField_8;
	// System.Nullable`1<System.Int32> Mapbox.Json.Serialization.JsonProperty::<Order>k__BackingField
	Nullable_1_t378540539  ___U3COrderU3Ek__BackingField_9;
	// System.String Mapbox.Json.Serialization.JsonProperty::<UnderlyingName>k__BackingField
	String_t* ___U3CUnderlyingNameU3Ek__BackingField_10;
	// Mapbox.Json.Serialization.IValueProvider Mapbox.Json.Serialization.JsonProperty::<ValueProvider>k__BackingField
	RuntimeObject* ___U3CValueProviderU3Ek__BackingField_11;
	// Mapbox.Json.Serialization.IAttributeProvider Mapbox.Json.Serialization.JsonProperty::<AttributeProvider>k__BackingField
	RuntimeObject* ___U3CAttributeProviderU3Ek__BackingField_12;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonProperty::<Converter>k__BackingField
	JsonConverter_t472504469 * ___U3CConverterU3Ek__BackingField_13;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonProperty::<MemberConverter>k__BackingField
	JsonConverter_t472504469 * ___U3CMemberConverterU3Ek__BackingField_14;
	// System.Boolean Mapbox.Json.Serialization.JsonProperty::<Ignored>k__BackingField
	bool ___U3CIgnoredU3Ek__BackingField_15;
	// System.Boolean Mapbox.Json.Serialization.JsonProperty::<Readable>k__BackingField
	bool ___U3CReadableU3Ek__BackingField_16;
	// System.Boolean Mapbox.Json.Serialization.JsonProperty::<Writable>k__BackingField
	bool ___U3CWritableU3Ek__BackingField_17;
	// System.Boolean Mapbox.Json.Serialization.JsonProperty::<HasMemberAttribute>k__BackingField
	bool ___U3CHasMemberAttributeU3Ek__BackingField_18;
	// System.Nullable`1<System.Boolean> Mapbox.Json.Serialization.JsonProperty::<IsReference>k__BackingField
	Nullable_1_t1819850047  ___U3CIsReferenceU3Ek__BackingField_19;
	// System.Nullable`1<Mapbox.Json.NullValueHandling> Mapbox.Json.Serialization.JsonProperty::<NullValueHandling>k__BackingField
	Nullable_1_t1948096445  ___U3CNullValueHandlingU3Ek__BackingField_20;
	// System.Nullable`1<Mapbox.Json.DefaultValueHandling> Mapbox.Json.Serialization.JsonProperty::<DefaultValueHandling>k__BackingField
	Nullable_1_t3509696309  ___U3CDefaultValueHandlingU3Ek__BackingField_21;
	// System.Nullable`1<Mapbox.Json.ReferenceLoopHandling> Mapbox.Json.Serialization.JsonProperty::<ReferenceLoopHandling>k__BackingField
	Nullable_1_t3660476548  ___U3CReferenceLoopHandlingU3Ek__BackingField_22;
	// System.Nullable`1<Mapbox.Json.ObjectCreationHandling> Mapbox.Json.Serialization.JsonProperty::<ObjectCreationHandling>k__BackingField
	Nullable_1_t2571116028  ___U3CObjectCreationHandlingU3Ek__BackingField_23;
	// System.Nullable`1<Mapbox.Json.TypeNameHandling> Mapbox.Json.Serialization.JsonProperty::<TypeNameHandling>k__BackingField
	Nullable_1_t1360201201  ___U3CTypeNameHandlingU3Ek__BackingField_24;
	// System.Predicate`1<System.Object> Mapbox.Json.Serialization.JsonProperty::<ShouldSerialize>k__BackingField
	Predicate_1_t3905400288 * ___U3CShouldSerializeU3Ek__BackingField_25;
	// System.Predicate`1<System.Object> Mapbox.Json.Serialization.JsonProperty::<ShouldDeserialize>k__BackingField
	Predicate_1_t3905400288 * ___U3CShouldDeserializeU3Ek__BackingField_26;
	// System.Predicate`1<System.Object> Mapbox.Json.Serialization.JsonProperty::<GetIsSpecified>k__BackingField
	Predicate_1_t3905400288 * ___U3CGetIsSpecifiedU3Ek__BackingField_27;
	// System.Action`2<System.Object,System.Object> Mapbox.Json.Serialization.JsonProperty::<SetIsSpecified>k__BackingField
	Action_2_t2470008838 * ___U3CSetIsSpecifiedU3Ek__BackingField_28;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonProperty::<ItemConverter>k__BackingField
	JsonConverter_t472504469 * ___U3CItemConverterU3Ek__BackingField_29;
	// System.Nullable`1<System.Boolean> Mapbox.Json.Serialization.JsonProperty::<ItemIsReference>k__BackingField
	Nullable_1_t1819850047  ___U3CItemIsReferenceU3Ek__BackingField_30;
	// System.Nullable`1<Mapbox.Json.TypeNameHandling> Mapbox.Json.Serialization.JsonProperty::<ItemTypeNameHandling>k__BackingField
	Nullable_1_t1360201201  ___U3CItemTypeNameHandlingU3Ek__BackingField_31;
	// System.Nullable`1<Mapbox.Json.ReferenceLoopHandling> Mapbox.Json.Serialization.JsonProperty::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t3660476548  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of__required_0() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ____required_0)); }
	inline Nullable_1_t2223747612  get__required_0() const { return ____required_0; }
	inline Nullable_1_t2223747612 * get_address_of__required_0() { return &____required_0; }
	inline void set__required_0(Nullable_1_t2223747612  value)
	{
		____required_0 = value;
	}

	inline static int32_t get_offset_of__hasExplicitDefaultValue_1() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ____hasExplicitDefaultValue_1)); }
	inline bool get__hasExplicitDefaultValue_1() const { return ____hasExplicitDefaultValue_1; }
	inline bool* get_address_of__hasExplicitDefaultValue_1() { return &____hasExplicitDefaultValue_1; }
	inline void set__hasExplicitDefaultValue_1(bool value)
	{
		____hasExplicitDefaultValue_1 = value;
	}

	inline static int32_t get_offset_of__defaultValue_2() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ____defaultValue_2)); }
	inline RuntimeObject * get__defaultValue_2() const { return ____defaultValue_2; }
	inline RuntimeObject ** get_address_of__defaultValue_2() { return &____defaultValue_2; }
	inline void set__defaultValue_2(RuntimeObject * value)
	{
		____defaultValue_2 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_2), value);
	}

	inline static int32_t get_offset_of__hasGeneratedDefaultValue_3() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ____hasGeneratedDefaultValue_3)); }
	inline bool get__hasGeneratedDefaultValue_3() const { return ____hasGeneratedDefaultValue_3; }
	inline bool* get_address_of__hasGeneratedDefaultValue_3() { return &____hasGeneratedDefaultValue_3; }
	inline void set__hasGeneratedDefaultValue_3(bool value)
	{
		____hasGeneratedDefaultValue_3 = value;
	}

	inline static int32_t get_offset_of__propertyName_4() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ____propertyName_4)); }
	inline String_t* get__propertyName_4() const { return ____propertyName_4; }
	inline String_t** get_address_of__propertyName_4() { return &____propertyName_4; }
	inline void set__propertyName_4(String_t* value)
	{
		____propertyName_4 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_4), value);
	}

	inline static int32_t get_offset_of__skipPropertyNameEscape_5() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ____skipPropertyNameEscape_5)); }
	inline bool get__skipPropertyNameEscape_5() const { return ____skipPropertyNameEscape_5; }
	inline bool* get_address_of__skipPropertyNameEscape_5() { return &____skipPropertyNameEscape_5; }
	inline void set__skipPropertyNameEscape_5(bool value)
	{
		____skipPropertyNameEscape_5 = value;
	}

	inline static int32_t get_offset_of__propertyType_6() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ____propertyType_6)); }
	inline Type_t * get__propertyType_6() const { return ____propertyType_6; }
	inline Type_t ** get_address_of__propertyType_6() { return &____propertyType_6; }
	inline void set__propertyType_6(Type_t * value)
	{
		____propertyType_6 = value;
		Il2CppCodeGenWriteBarrier((&____propertyType_6), value);
	}

	inline static int32_t get_offset_of_U3CPropertyContractU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CPropertyContractU3Ek__BackingField_7)); }
	inline JsonContract_t968551136 * get_U3CPropertyContractU3Ek__BackingField_7() const { return ___U3CPropertyContractU3Ek__BackingField_7; }
	inline JsonContract_t968551136 ** get_address_of_U3CPropertyContractU3Ek__BackingField_7() { return &___U3CPropertyContractU3Ek__BackingField_7; }
	inline void set_U3CPropertyContractU3Ek__BackingField_7(JsonContract_t968551136 * value)
	{
		___U3CPropertyContractU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyContractU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CDeclaringTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CDeclaringTypeU3Ek__BackingField_8() const { return ___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CDeclaringTypeU3Ek__BackingField_8() { return &___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline void set_U3CDeclaringTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CDeclaringTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclaringTypeU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COrderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3COrderU3Ek__BackingField_9)); }
	inline Nullable_1_t378540539  get_U3COrderU3Ek__BackingField_9() const { return ___U3COrderU3Ek__BackingField_9; }
	inline Nullable_1_t378540539 * get_address_of_U3COrderU3Ek__BackingField_9() { return &___U3COrderU3Ek__BackingField_9; }
	inline void set_U3COrderU3Ek__BackingField_9(Nullable_1_t378540539  value)
	{
		___U3COrderU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CUnderlyingNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CUnderlyingNameU3Ek__BackingField_10() const { return ___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CUnderlyingNameU3Ek__BackingField_10() { return &___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline void set_U3CUnderlyingNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CUnderlyingNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingNameU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CValueProviderU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CValueProviderU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CValueProviderU3Ek__BackingField_11() const { return ___U3CValueProviderU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CValueProviderU3Ek__BackingField_11() { return &___U3CValueProviderU3Ek__BackingField_11; }
	inline void set_U3CValueProviderU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CValueProviderU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueProviderU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CAttributeProviderU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CAttributeProviderU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CAttributeProviderU3Ek__BackingField_12() const { return ___U3CAttributeProviderU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CAttributeProviderU3Ek__BackingField_12() { return &___U3CAttributeProviderU3Ek__BackingField_12; }
	inline void set_U3CAttributeProviderU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CAttributeProviderU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAttributeProviderU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CConverterU3Ek__BackingField_13)); }
	inline JsonConverter_t472504469 * get_U3CConverterU3Ek__BackingField_13() const { return ___U3CConverterU3Ek__BackingField_13; }
	inline JsonConverter_t472504469 ** get_address_of_U3CConverterU3Ek__BackingField_13() { return &___U3CConverterU3Ek__BackingField_13; }
	inline void set_U3CConverterU3Ek__BackingField_13(JsonConverter_t472504469 * value)
	{
		___U3CConverterU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CMemberConverterU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CMemberConverterU3Ek__BackingField_14)); }
	inline JsonConverter_t472504469 * get_U3CMemberConverterU3Ek__BackingField_14() const { return ___U3CMemberConverterU3Ek__BackingField_14; }
	inline JsonConverter_t472504469 ** get_address_of_U3CMemberConverterU3Ek__BackingField_14() { return &___U3CMemberConverterU3Ek__BackingField_14; }
	inline void set_U3CMemberConverterU3Ek__BackingField_14(JsonConverter_t472504469 * value)
	{
		___U3CMemberConverterU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberConverterU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CIgnoredU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CIgnoredU3Ek__BackingField_15)); }
	inline bool get_U3CIgnoredU3Ek__BackingField_15() const { return ___U3CIgnoredU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CIgnoredU3Ek__BackingField_15() { return &___U3CIgnoredU3Ek__BackingField_15; }
	inline void set_U3CIgnoredU3Ek__BackingField_15(bool value)
	{
		___U3CIgnoredU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CReadableU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CReadableU3Ek__BackingField_16)); }
	inline bool get_U3CReadableU3Ek__BackingField_16() const { return ___U3CReadableU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CReadableU3Ek__BackingField_16() { return &___U3CReadableU3Ek__BackingField_16; }
	inline void set_U3CReadableU3Ek__BackingField_16(bool value)
	{
		___U3CReadableU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CWritableU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CWritableU3Ek__BackingField_17)); }
	inline bool get_U3CWritableU3Ek__BackingField_17() const { return ___U3CWritableU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CWritableU3Ek__BackingField_17() { return &___U3CWritableU3Ek__BackingField_17; }
	inline void set_U3CWritableU3Ek__BackingField_17(bool value)
	{
		___U3CWritableU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CHasMemberAttributeU3Ek__BackingField_18)); }
	inline bool get_U3CHasMemberAttributeU3Ek__BackingField_18() const { return ___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return &___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline void set_U3CHasMemberAttributeU3Ek__BackingField_18(bool value)
	{
		___U3CHasMemberAttributeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CIsReferenceU3Ek__BackingField_19)); }
	inline Nullable_1_t1819850047  get_U3CIsReferenceU3Ek__BackingField_19() const { return ___U3CIsReferenceU3Ek__BackingField_19; }
	inline Nullable_1_t1819850047 * get_address_of_U3CIsReferenceU3Ek__BackingField_19() { return &___U3CIsReferenceU3Ek__BackingField_19; }
	inline void set_U3CIsReferenceU3Ek__BackingField_19(Nullable_1_t1819850047  value)
	{
		___U3CIsReferenceU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CNullValueHandlingU3Ek__BackingField_20)); }
	inline Nullable_1_t1948096445  get_U3CNullValueHandlingU3Ek__BackingField_20() const { return ___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline Nullable_1_t1948096445 * get_address_of_U3CNullValueHandlingU3Ek__BackingField_20() { return &___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline void set_U3CNullValueHandlingU3Ek__BackingField_20(Nullable_1_t1948096445  value)
	{
		___U3CNullValueHandlingU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CDefaultValueHandlingU3Ek__BackingField_21)); }
	inline Nullable_1_t3509696309  get_U3CDefaultValueHandlingU3Ek__BackingField_21() const { return ___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline Nullable_1_t3509696309 * get_address_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return &___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline void set_U3CDefaultValueHandlingU3Ek__BackingField_21(Nullable_1_t3509696309  value)
	{
		___U3CDefaultValueHandlingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CReferenceLoopHandlingU3Ek__BackingField_22)); }
	inline Nullable_1_t3660476548  get_U3CReferenceLoopHandlingU3Ek__BackingField_22() const { return ___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline Nullable_1_t3660476548 * get_address_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return &___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline void set_U3CReferenceLoopHandlingU3Ek__BackingField_22(Nullable_1_t3660476548  value)
	{
		___U3CReferenceLoopHandlingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CObjectCreationHandlingU3Ek__BackingField_23)); }
	inline Nullable_1_t2571116028  get_U3CObjectCreationHandlingU3Ek__BackingField_23() const { return ___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline Nullable_1_t2571116028 * get_address_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return &___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline void set_U3CObjectCreationHandlingU3Ek__BackingField_23(Nullable_1_t2571116028  value)
	{
		___U3CObjectCreationHandlingU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CTypeNameHandlingU3Ek__BackingField_24)); }
	inline Nullable_1_t1360201201  get_U3CTypeNameHandlingU3Ek__BackingField_24() const { return ___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline Nullable_1_t1360201201 * get_address_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return &___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline void set_U3CTypeNameHandlingU3Ek__BackingField_24(Nullable_1_t1360201201  value)
	{
		___U3CTypeNameHandlingU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CShouldSerializeU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CShouldSerializeU3Ek__BackingField_25)); }
	inline Predicate_1_t3905400288 * get_U3CShouldSerializeU3Ek__BackingField_25() const { return ___U3CShouldSerializeU3Ek__BackingField_25; }
	inline Predicate_1_t3905400288 ** get_address_of_U3CShouldSerializeU3Ek__BackingField_25() { return &___U3CShouldSerializeU3Ek__BackingField_25; }
	inline void set_U3CShouldSerializeU3Ek__BackingField_25(Predicate_1_t3905400288 * value)
	{
		___U3CShouldSerializeU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldSerializeU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CShouldDeserializeU3Ek__BackingField_26)); }
	inline Predicate_1_t3905400288 * get_U3CShouldDeserializeU3Ek__BackingField_26() const { return ___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline Predicate_1_t3905400288 ** get_address_of_U3CShouldDeserializeU3Ek__BackingField_26() { return &___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline void set_U3CShouldDeserializeU3Ek__BackingField_26(Predicate_1_t3905400288 * value)
	{
		___U3CShouldDeserializeU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldDeserializeU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CGetIsSpecifiedU3Ek__BackingField_27)); }
	inline Predicate_1_t3905400288 * get_U3CGetIsSpecifiedU3Ek__BackingField_27() const { return ___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline Predicate_1_t3905400288 ** get_address_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return &___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline void set_U3CGetIsSpecifiedU3Ek__BackingField_27(Predicate_1_t3905400288 * value)
	{
		___U3CGetIsSpecifiedU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetIsSpecifiedU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CSetIsSpecifiedU3Ek__BackingField_28)); }
	inline Action_2_t2470008838 * get_U3CSetIsSpecifiedU3Ek__BackingField_28() const { return ___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline Action_2_t2470008838 ** get_address_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return &___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline void set_U3CSetIsSpecifiedU3Ek__BackingField_28(Action_2_t2470008838 * value)
	{
		___U3CSetIsSpecifiedU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetIsSpecifiedU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CItemConverterU3Ek__BackingField_29)); }
	inline JsonConverter_t472504469 * get_U3CItemConverterU3Ek__BackingField_29() const { return ___U3CItemConverterU3Ek__BackingField_29; }
	inline JsonConverter_t472504469 ** get_address_of_U3CItemConverterU3Ek__BackingField_29() { return &___U3CItemConverterU3Ek__BackingField_29; }
	inline void set_U3CItemConverterU3Ek__BackingField_29(JsonConverter_t472504469 * value)
	{
		___U3CItemConverterU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CItemIsReferenceU3Ek__BackingField_30)); }
	inline Nullable_1_t1819850047  get_U3CItemIsReferenceU3Ek__BackingField_30() const { return ___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline Nullable_1_t1819850047 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_30() { return &___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_30(Nullable_1_t1819850047  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CItemTypeNameHandlingU3Ek__BackingField_31)); }
	inline Nullable_1_t1360201201  get_U3CItemTypeNameHandlingU3Ek__BackingField_31() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline Nullable_1_t1360201201 * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_31(Nullable_1_t1360201201  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonProperty_t1629976260, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32)); }
	inline Nullable_1_t3660476548  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline Nullable_1_t3660476548 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(Nullable_1_t3660476548  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTY_T1629976260_H
#ifndef EXTENSIONDATASETTER_T1236496959_H
#define EXTENSIONDATASETTER_T1236496959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.ExtensionDataSetter
struct  ExtensionDataSetter_t1236496959  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATASETTER_T1236496959_H
#ifndef JSONREADER_T1879223345_H
#define JSONREADER_T1879223345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonReader
struct  JsonReader_t1879223345  : public RuntimeObject
{
public:
	// Mapbox.Json.JsonToken Mapbox.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Mapbox.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Mapbox.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Mapbox.Json.JsonReader/State Mapbox.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Mapbox.Json.JsonPosition Mapbox.Json.JsonReader::_currentPosition
	JsonPosition_t841078794  ____currentPosition_4;
	// System.Globalization.CultureInfo Mapbox.Json.JsonReader::_culture
	CultureInfo_t4157843068 * ____culture_5;
	// Mapbox.Json.DateTimeZoneHandling Mapbox.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Mapbox.Json.JsonReader::_maxDepth
	Nullable_1_t378540539  ____maxDepth_7;
	// System.Boolean Mapbox.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Mapbox.Json.DateParseHandling Mapbox.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Mapbox.Json.FloatParseHandling Mapbox.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Mapbox.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Mapbox.Json.JsonPosition> Mapbox.Json.JsonReader::_stack
	List_1_t2313153536 * ____stack_12;
	// System.Boolean Mapbox.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Mapbox.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____currentPosition_4)); }
	inline JsonPosition_t841078794  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t841078794 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t841078794  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____culture_5)); }
	inline CultureInfo_t4157843068 * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t4157843068 * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____maxDepth_7)); }
	inline Nullable_1_t378540539  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t378540539  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____stack_12)); }
	inline List_1_t2313153536 * get__stack_12() const { return ____stack_12; }
	inline List_1_t2313153536 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_t2313153536 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T1879223345_H
#ifndef SERIALIZATIONCALLBACK_T1737529799_H
#define SERIALIZATIONCALLBACK_T1737529799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.SerializationCallback
struct  SerializationCallback_t1737529799  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONCALLBACK_T1737529799_H
#ifndef JSONWRITER_T260835314_H
#define JSONWRITER_T260835314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonWriter
struct  JsonWriter_t260835314  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.JsonPosition> Mapbox.Json.JsonWriter::_stack
	List_1_t2313153536 * ____stack_2;
	// Mapbox.Json.JsonPosition Mapbox.Json.JsonWriter::_currentPosition
	JsonPosition_t841078794  ____currentPosition_3;
	// Mapbox.Json.JsonWriter/State Mapbox.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Mapbox.Json.Formatting Mapbox.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Mapbox.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// System.Boolean Mapbox.Json.JsonWriter::<AutoCompleteOnClose>k__BackingField
	bool ___U3CAutoCompleteOnCloseU3Ek__BackingField_7;
	// Mapbox.Json.DateFormatHandling Mapbox.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_8;
	// Mapbox.Json.DateTimeZoneHandling Mapbox.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_9;
	// Mapbox.Json.StringEscapeHandling Mapbox.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_10;
	// Mapbox.Json.FloatFormatHandling Mapbox.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_11;
	// System.String Mapbox.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_12;
	// System.Globalization.CultureInfo Mapbox.Json.JsonWriter::_culture
	CultureInfo_t4157843068 * ____culture_13;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____stack_2)); }
	inline List_1_t2313153536 * get__stack_2() const { return ____stack_2; }
	inline List_1_t2313153536 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_t2313153536 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____currentPosition_3)); }
	inline JsonPosition_t841078794  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t841078794 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t841078794  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ___U3CAutoCompleteOnCloseU3Ek__BackingField_7)); }
	inline bool get_U3CAutoCompleteOnCloseU3Ek__BackingField_7() const { return ___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return &___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline void set_U3CAutoCompleteOnCloseU3Ek__BackingField_7(bool value)
	{
		___U3CAutoCompleteOnCloseU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____dateFormatHandling_8)); }
	inline int32_t get__dateFormatHandling_8() const { return ____dateFormatHandling_8; }
	inline int32_t* get_address_of__dateFormatHandling_8() { return &____dateFormatHandling_8; }
	inline void set__dateFormatHandling_8(int32_t value)
	{
		____dateFormatHandling_8 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____dateTimeZoneHandling_9)); }
	inline int32_t get__dateTimeZoneHandling_9() const { return ____dateTimeZoneHandling_9; }
	inline int32_t* get_address_of__dateTimeZoneHandling_9() { return &____dateTimeZoneHandling_9; }
	inline void set__dateTimeZoneHandling_9(int32_t value)
	{
		____dateTimeZoneHandling_9 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____stringEscapeHandling_10)); }
	inline int32_t get__stringEscapeHandling_10() const { return ____stringEscapeHandling_10; }
	inline int32_t* get_address_of__stringEscapeHandling_10() { return &____stringEscapeHandling_10; }
	inline void set__stringEscapeHandling_10(int32_t value)
	{
		____stringEscapeHandling_10 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_11() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____floatFormatHandling_11)); }
	inline int32_t get__floatFormatHandling_11() const { return ____floatFormatHandling_11; }
	inline int32_t* get_address_of__floatFormatHandling_11() { return &____floatFormatHandling_11; }
	inline void set__floatFormatHandling_11(int32_t value)
	{
		____floatFormatHandling_11 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_12() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____dateFormatString_12)); }
	inline String_t* get__dateFormatString_12() const { return ____dateFormatString_12; }
	inline String_t** get_address_of__dateFormatString_12() { return &____dateFormatString_12; }
	inline void set__dateFormatString_12(String_t* value)
	{
		____dateFormatString_12 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_12), value);
	}

	inline static int32_t get_offset_of__culture_13() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____culture_13)); }
	inline CultureInfo_t4157843068 * get__culture_13() const { return ____culture_13; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_13() { return &____culture_13; }
	inline void set__culture_13(CultureInfo_t4157843068 * value)
	{
		____culture_13 = value;
		Il2CppCodeGenWriteBarrier((&____culture_13), value);
	}
};

struct JsonWriter_t260835314_StaticFields
{
public:
	// Mapbox.Json.JsonWriter/State[][] Mapbox.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t691804306* ___StateArray_0;
	// Mapbox.Json.JsonWriter/State[][] Mapbox.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t691804306* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t691804306* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t691804306** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t691804306* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t691804306* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t691804306** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t691804306* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T260835314_H
#ifndef SERIALIZATIONERRORCALLBACK_T811918742_H
#define SERIALIZATIONERRORCALLBACK_T811918742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.SerializationErrorCallback
struct  SerializationErrorCallback_t811918742  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONERRORCALLBACK_T811918742_H
#ifndef JSONDICTIONARYCONTRACT_T170548660_H
#define JSONDICTIONARYCONTRACT_T170548660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonDictionaryContract
struct  JsonDictionaryContract_t170548660  : public JsonContainerContract_t790269270
{
public:
	// System.Func`2<System.String,System.String> Mapbox.Json.Serialization.JsonDictionaryContract::<DictionaryKeyResolver>k__BackingField
	Func_2_t3947292210 * ___U3CDictionaryKeyResolverU3Ek__BackingField_27;
	// System.Type Mapbox.Json.Serialization.JsonDictionaryContract::<DictionaryKeyType>k__BackingField
	Type_t * ___U3CDictionaryKeyTypeU3Ek__BackingField_28;
	// System.Type Mapbox.Json.Serialization.JsonDictionaryContract::<DictionaryValueType>k__BackingField
	Type_t * ___U3CDictionaryValueTypeU3Ek__BackingField_29;
	// Mapbox.Json.Serialization.JsonContract Mapbox.Json.Serialization.JsonDictionaryContract::<KeyContract>k__BackingField
	JsonContract_t968551136 * ___U3CKeyContractU3Ek__BackingField_30;
	// System.Type Mapbox.Json.Serialization.JsonDictionaryContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_31;
	// System.Type Mapbox.Json.Serialization.JsonDictionaryContract::_genericWrapperType
	Type_t * ____genericWrapperType_32;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonDictionaryContract::_genericWrapperCreator
	ObjectConstructor_1_t2523184513 * ____genericWrapperCreator_33;
	// System.Func`1<System.Object> Mapbox.Json.Serialization.JsonDictionaryContract::_genericTemporaryDictionaryCreator
	Func_1_t2509852811 * ____genericTemporaryDictionaryCreator_34;
	// System.Boolean Mapbox.Json.Serialization.JsonDictionaryContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Mapbox.Json.Serialization.JsonDictionaryContract::_parameterizedConstructor
	ConstructorInfo_t5769829 * ____parameterizedConstructor_36;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonDictionaryContract::_overrideCreator
	ObjectConstructor_1_t2523184513 * ____overrideCreator_37;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonDictionaryContract::_parameterizedCreator
	ObjectConstructor_1_t2523184513 * ____parameterizedCreator_38;
	// System.Boolean Mapbox.Json.Serialization.JsonDictionaryContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ___U3CDictionaryKeyResolverU3Ek__BackingField_27)); }
	inline Func_2_t3947292210 * get_U3CDictionaryKeyResolverU3Ek__BackingField_27() const { return ___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline Func_2_t3947292210 ** get_address_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return &___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline void set_U3CDictionaryKeyResolverU3Ek__BackingField_27(Func_2_t3947292210 * value)
	{
		___U3CDictionaryKeyResolverU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyResolverU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ___U3CDictionaryKeyTypeU3Ek__BackingField_28)); }
	inline Type_t * get_U3CDictionaryKeyTypeU3Ek__BackingField_28() const { return ___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline Type_t ** get_address_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return &___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline void set_U3CDictionaryKeyTypeU3Ek__BackingField_28(Type_t * value)
	{
		___U3CDictionaryKeyTypeU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyTypeU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ___U3CDictionaryValueTypeU3Ek__BackingField_29)); }
	inline Type_t * get_U3CDictionaryValueTypeU3Ek__BackingField_29() const { return ___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline Type_t ** get_address_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return &___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline void set_U3CDictionaryValueTypeU3Ek__BackingField_29(Type_t * value)
	{
		___U3CDictionaryValueTypeU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryValueTypeU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CKeyContractU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ___U3CKeyContractU3Ek__BackingField_30)); }
	inline JsonContract_t968551136 * get_U3CKeyContractU3Ek__BackingField_30() const { return ___U3CKeyContractU3Ek__BackingField_30; }
	inline JsonContract_t968551136 ** get_address_of_U3CKeyContractU3Ek__BackingField_30() { return &___U3CKeyContractU3Ek__BackingField_30; }
	inline void set_U3CKeyContractU3Ek__BackingField_30(JsonContract_t968551136 * value)
	{
		___U3CKeyContractU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyContractU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_31() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ____genericCollectionDefinitionType_31)); }
	inline Type_t * get__genericCollectionDefinitionType_31() const { return ____genericCollectionDefinitionType_31; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_31() { return &____genericCollectionDefinitionType_31; }
	inline void set__genericCollectionDefinitionType_31(Type_t * value)
	{
		____genericCollectionDefinitionType_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_31), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_32() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ____genericWrapperType_32)); }
	inline Type_t * get__genericWrapperType_32() const { return ____genericWrapperType_32; }
	inline Type_t ** get_address_of__genericWrapperType_32() { return &____genericWrapperType_32; }
	inline void set__genericWrapperType_32(Type_t * value)
	{
		____genericWrapperType_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_32), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_33() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ____genericWrapperCreator_33)); }
	inline ObjectConstructor_1_t2523184513 * get__genericWrapperCreator_33() const { return ____genericWrapperCreator_33; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__genericWrapperCreator_33() { return &____genericWrapperCreator_33; }
	inline void set__genericWrapperCreator_33(ObjectConstructor_1_t2523184513 * value)
	{
		____genericWrapperCreator_33 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_33), value);
	}

	inline static int32_t get_offset_of__genericTemporaryDictionaryCreator_34() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ____genericTemporaryDictionaryCreator_34)); }
	inline Func_1_t2509852811 * get__genericTemporaryDictionaryCreator_34() const { return ____genericTemporaryDictionaryCreator_34; }
	inline Func_1_t2509852811 ** get_address_of__genericTemporaryDictionaryCreator_34() { return &____genericTemporaryDictionaryCreator_34; }
	inline void set__genericTemporaryDictionaryCreator_34(Func_1_t2509852811 * value)
	{
		____genericTemporaryDictionaryCreator_34 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryDictionaryCreator_34), value);
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ___U3CShouldCreateWrapperU3Ek__BackingField_35)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_35() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return &___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_35(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t5769829 * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t5769829 ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t5769829 * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__overrideCreator_37() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ____overrideCreator_37)); }
	inline ObjectConstructor_1_t2523184513 * get__overrideCreator_37() const { return ____overrideCreator_37; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__overrideCreator_37() { return &____overrideCreator_37; }
	inline void set__overrideCreator_37(ObjectConstructor_1_t2523184513 * value)
	{
		____overrideCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_37), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_38() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ____parameterizedCreator_38)); }
	inline ObjectConstructor_1_t2523184513 * get__parameterizedCreator_38() const { return ____parameterizedCreator_38; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__parameterizedCreator_38() { return &____parameterizedCreator_38; }
	inline void set__parameterizedCreator_38(ObjectConstructor_1_t2523184513 * value)
	{
		____parameterizedCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t170548660, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDICTIONARYCONTRACT_T170548660_H
#ifndef JSONOBJECTCONTRACT_T1235679874_H
#define JSONOBJECTCONTRACT_T1235679874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonObjectContract
struct  JsonObjectContract_t1235679874  : public JsonContainerContract_t790269270
{
public:
	// Mapbox.Json.MemberSerialization Mapbox.Json.Serialization.JsonObjectContract::<MemberSerialization>k__BackingField
	int32_t ___U3CMemberSerializationU3Ek__BackingField_27;
	// System.Nullable`1<Mapbox.Json.Required> Mapbox.Json.Serialization.JsonObjectContract::<ItemRequired>k__BackingField
	Nullable_1_t2223747612  ___U3CItemRequiredU3Ek__BackingField_28;
	// Mapbox.Json.Serialization.JsonPropertyCollection Mapbox.Json.Serialization.JsonObjectContract::<Properties>k__BackingField
	JsonPropertyCollection_t1665309380 * ___U3CPropertiesU3Ek__BackingField_29;
	// Mapbox.Json.Serialization.ExtensionDataSetter Mapbox.Json.Serialization.JsonObjectContract::<ExtensionDataSetter>k__BackingField
	ExtensionDataSetter_t1236496959 * ___U3CExtensionDataSetterU3Ek__BackingField_30;
	// Mapbox.Json.Serialization.ExtensionDataGetter Mapbox.Json.Serialization.JsonObjectContract::<ExtensionDataGetter>k__BackingField
	ExtensionDataGetter_t1148678719 * ___U3CExtensionDataGetterU3Ek__BackingField_31;
	// System.Func`2<System.String,System.String> Mapbox.Json.Serialization.JsonObjectContract::<ExtensionDataNameResolver>k__BackingField
	Func_2_t3947292210 * ___U3CExtensionDataNameResolverU3Ek__BackingField_32;
	// System.Boolean Mapbox.Json.Serialization.JsonObjectContract::ExtensionDataIsJToken
	bool ___ExtensionDataIsJToken_33;
	// System.Nullable`1<System.Boolean> Mapbox.Json.Serialization.JsonObjectContract::_hasRequiredOrDefaultValueProperties
	Nullable_1_t1819850047  ____hasRequiredOrDefaultValueProperties_34;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonObjectContract::_overrideCreator
	ObjectConstructor_1_t2523184513 * ____overrideCreator_35;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonObjectContract::_parameterizedCreator
	ObjectConstructor_1_t2523184513 * ____parameterizedCreator_36;
	// Mapbox.Json.Serialization.JsonPropertyCollection Mapbox.Json.Serialization.JsonObjectContract::_creatorParameters
	JsonPropertyCollection_t1665309380 * ____creatorParameters_37;
	// System.Type Mapbox.Json.Serialization.JsonObjectContract::_extensionDataValueType
	Type_t * ____extensionDataValueType_38;

public:
	inline static int32_t get_offset_of_U3CMemberSerializationU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ___U3CMemberSerializationU3Ek__BackingField_27)); }
	inline int32_t get_U3CMemberSerializationU3Ek__BackingField_27() const { return ___U3CMemberSerializationU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CMemberSerializationU3Ek__BackingField_27() { return &___U3CMemberSerializationU3Ek__BackingField_27; }
	inline void set_U3CMemberSerializationU3Ek__BackingField_27(int32_t value)
	{
		___U3CMemberSerializationU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CItemRequiredU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ___U3CItemRequiredU3Ek__BackingField_28)); }
	inline Nullable_1_t2223747612  get_U3CItemRequiredU3Ek__BackingField_28() const { return ___U3CItemRequiredU3Ek__BackingField_28; }
	inline Nullable_1_t2223747612 * get_address_of_U3CItemRequiredU3Ek__BackingField_28() { return &___U3CItemRequiredU3Ek__BackingField_28; }
	inline void set_U3CItemRequiredU3Ek__BackingField_28(Nullable_1_t2223747612  value)
	{
		___U3CItemRequiredU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ___U3CPropertiesU3Ek__BackingField_29)); }
	inline JsonPropertyCollection_t1665309380 * get_U3CPropertiesU3Ek__BackingField_29() const { return ___U3CPropertiesU3Ek__BackingField_29; }
	inline JsonPropertyCollection_t1665309380 ** get_address_of_U3CPropertiesU3Ek__BackingField_29() { return &___U3CPropertiesU3Ek__BackingField_29; }
	inline void set_U3CPropertiesU3Ek__BackingField_29(JsonPropertyCollection_t1665309380 * value)
	{
		___U3CPropertiesU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ___U3CExtensionDataSetterU3Ek__BackingField_30)); }
	inline ExtensionDataSetter_t1236496959 * get_U3CExtensionDataSetterU3Ek__BackingField_30() const { return ___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline ExtensionDataSetter_t1236496959 ** get_address_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return &___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline void set_U3CExtensionDataSetterU3Ek__BackingField_30(ExtensionDataSetter_t1236496959 * value)
	{
		___U3CExtensionDataSetterU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataSetterU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ___U3CExtensionDataGetterU3Ek__BackingField_31)); }
	inline ExtensionDataGetter_t1148678719 * get_U3CExtensionDataGetterU3Ek__BackingField_31() const { return ___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline ExtensionDataGetter_t1148678719 ** get_address_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return &___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline void set_U3CExtensionDataGetterU3Ek__BackingField_31(ExtensionDataGetter_t1148678719 * value)
	{
		___U3CExtensionDataGetterU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataGetterU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataNameResolverU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ___U3CExtensionDataNameResolverU3Ek__BackingField_32)); }
	inline Func_2_t3947292210 * get_U3CExtensionDataNameResolverU3Ek__BackingField_32() const { return ___U3CExtensionDataNameResolverU3Ek__BackingField_32; }
	inline Func_2_t3947292210 ** get_address_of_U3CExtensionDataNameResolverU3Ek__BackingField_32() { return &___U3CExtensionDataNameResolverU3Ek__BackingField_32; }
	inline void set_U3CExtensionDataNameResolverU3Ek__BackingField_32(Func_2_t3947292210 * value)
	{
		___U3CExtensionDataNameResolverU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataNameResolverU3Ek__BackingField_32), value);
	}

	inline static int32_t get_offset_of_ExtensionDataIsJToken_33() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ___ExtensionDataIsJToken_33)); }
	inline bool get_ExtensionDataIsJToken_33() const { return ___ExtensionDataIsJToken_33; }
	inline bool* get_address_of_ExtensionDataIsJToken_33() { return &___ExtensionDataIsJToken_33; }
	inline void set_ExtensionDataIsJToken_33(bool value)
	{
		___ExtensionDataIsJToken_33 = value;
	}

	inline static int32_t get_offset_of__hasRequiredOrDefaultValueProperties_34() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ____hasRequiredOrDefaultValueProperties_34)); }
	inline Nullable_1_t1819850047  get__hasRequiredOrDefaultValueProperties_34() const { return ____hasRequiredOrDefaultValueProperties_34; }
	inline Nullable_1_t1819850047 * get_address_of__hasRequiredOrDefaultValueProperties_34() { return &____hasRequiredOrDefaultValueProperties_34; }
	inline void set__hasRequiredOrDefaultValueProperties_34(Nullable_1_t1819850047  value)
	{
		____hasRequiredOrDefaultValueProperties_34 = value;
	}

	inline static int32_t get_offset_of__overrideCreator_35() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ____overrideCreator_35)); }
	inline ObjectConstructor_1_t2523184513 * get__overrideCreator_35() const { return ____overrideCreator_35; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__overrideCreator_35() { return &____overrideCreator_35; }
	inline void set__overrideCreator_35(ObjectConstructor_1_t2523184513 * value)
	{
		____overrideCreator_35 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_35), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_36() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ____parameterizedCreator_36)); }
	inline ObjectConstructor_1_t2523184513 * get__parameterizedCreator_36() const { return ____parameterizedCreator_36; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__parameterizedCreator_36() { return &____parameterizedCreator_36; }
	inline void set__parameterizedCreator_36(ObjectConstructor_1_t2523184513 * value)
	{
		____parameterizedCreator_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_36), value);
	}

	inline static int32_t get_offset_of__creatorParameters_37() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ____creatorParameters_37)); }
	inline JsonPropertyCollection_t1665309380 * get__creatorParameters_37() const { return ____creatorParameters_37; }
	inline JsonPropertyCollection_t1665309380 ** get_address_of__creatorParameters_37() { return &____creatorParameters_37; }
	inline void set__creatorParameters_37(JsonPropertyCollection_t1665309380 * value)
	{
		____creatorParameters_37 = value;
		Il2CppCodeGenWriteBarrier((&____creatorParameters_37), value);
	}

	inline static int32_t get_offset_of__extensionDataValueType_38() { return static_cast<int32_t>(offsetof(JsonObjectContract_t1235679874, ____extensionDataValueType_38)); }
	inline Type_t * get__extensionDataValueType_38() const { return ____extensionDataValueType_38; }
	inline Type_t ** get_address_of__extensionDataValueType_38() { return &____extensionDataValueType_38; }
	inline void set__extensionDataValueType_38(Type_t * value)
	{
		____extensionDataValueType_38 = value;
		Il2CppCodeGenWriteBarrier((&____extensionDataValueType_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECTCONTRACT_T1235679874_H
#ifndef JSONISERIALIZABLECONTRACT_T2905058752_H
#define JSONISERIALIZABLECONTRACT_T2905058752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonISerializableContract
struct  JsonISerializableContract_t2905058752  : public JsonContainerContract_t790269270
{
public:
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonISerializableContract::<ISerializableCreator>k__BackingField
	ObjectConstructor_1_t2523184513 * ___U3CISerializableCreatorU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonISerializableContract_t2905058752, ___U3CISerializableCreatorU3Ek__BackingField_27)); }
	inline ObjectConstructor_1_t2523184513 * get_U3CISerializableCreatorU3Ek__BackingField_27() const { return ___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of_U3CISerializableCreatorU3Ek__BackingField_27() { return &___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline void set_U3CISerializableCreatorU3Ek__BackingField_27(ObjectConstructor_1_t2523184513 * value)
	{
		___U3CISerializableCreatorU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CISerializableCreatorU3Ek__BackingField_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONISERIALIZABLECONTRACT_T2905058752_H
#ifndef TRACEJSONWRITER_T2956702503_H
#define TRACEJSONWRITER_T2956702503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.TraceJsonWriter
struct  TraceJsonWriter_t2956702503  : public JsonWriter_t260835314
{
public:
	// Mapbox.Json.JsonWriter Mapbox.Json.Serialization.TraceJsonWriter::_innerWriter
	JsonWriter_t260835314 * ____innerWriter_14;
	// Mapbox.Json.JsonTextWriter Mapbox.Json.Serialization.TraceJsonWriter::_textWriter
	JsonTextWriter_t990966167 * ____textWriter_15;
	// System.IO.StringWriter Mapbox.Json.Serialization.TraceJsonWriter::_sw
	StringWriter_t802263757 * ____sw_16;

public:
	inline static int32_t get_offset_of__innerWriter_14() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t2956702503, ____innerWriter_14)); }
	inline JsonWriter_t260835314 * get__innerWriter_14() const { return ____innerWriter_14; }
	inline JsonWriter_t260835314 ** get_address_of__innerWriter_14() { return &____innerWriter_14; }
	inline void set__innerWriter_14(JsonWriter_t260835314 * value)
	{
		____innerWriter_14 = value;
		Il2CppCodeGenWriteBarrier((&____innerWriter_14), value);
	}

	inline static int32_t get_offset_of__textWriter_15() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t2956702503, ____textWriter_15)); }
	inline JsonTextWriter_t990966167 * get__textWriter_15() const { return ____textWriter_15; }
	inline JsonTextWriter_t990966167 ** get_address_of__textWriter_15() { return &____textWriter_15; }
	inline void set__textWriter_15(JsonTextWriter_t990966167 * value)
	{
		____textWriter_15 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_15), value);
	}

	inline static int32_t get_offset_of__sw_16() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t2956702503, ____sw_16)); }
	inline StringWriter_t802263757 * get__sw_16() const { return ____sw_16; }
	inline StringWriter_t802263757 ** get_address_of__sw_16() { return &____sw_16; }
	inline void set__sw_16(StringWriter_t802263757 * value)
	{
		____sw_16 = value;
		Il2CppCodeGenWriteBarrier((&____sw_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONWRITER_T2956702503_H
#ifndef TRACEJSONREADER_T1720844242_H
#define TRACEJSONREADER_T1720844242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.TraceJsonReader
struct  TraceJsonReader_t1720844242  : public JsonReader_t1879223345
{
public:
	// Mapbox.Json.JsonReader Mapbox.Json.Serialization.TraceJsonReader::_innerReader
	JsonReader_t1879223345 * ____innerReader_15;
	// Mapbox.Json.JsonTextWriter Mapbox.Json.Serialization.TraceJsonReader::_textWriter
	JsonTextWriter_t990966167 * ____textWriter_16;
	// System.IO.StringWriter Mapbox.Json.Serialization.TraceJsonReader::_sw
	StringWriter_t802263757 * ____sw_17;

public:
	inline static int32_t get_offset_of__innerReader_15() { return static_cast<int32_t>(offsetof(TraceJsonReader_t1720844242, ____innerReader_15)); }
	inline JsonReader_t1879223345 * get__innerReader_15() const { return ____innerReader_15; }
	inline JsonReader_t1879223345 ** get_address_of__innerReader_15() { return &____innerReader_15; }
	inline void set__innerReader_15(JsonReader_t1879223345 * value)
	{
		____innerReader_15 = value;
		Il2CppCodeGenWriteBarrier((&____innerReader_15), value);
	}

	inline static int32_t get_offset_of__textWriter_16() { return static_cast<int32_t>(offsetof(TraceJsonReader_t1720844242, ____textWriter_16)); }
	inline JsonTextWriter_t990966167 * get__textWriter_16() const { return ____textWriter_16; }
	inline JsonTextWriter_t990966167 ** get_address_of__textWriter_16() { return &____textWriter_16; }
	inline void set__textWriter_16(JsonTextWriter_t990966167 * value)
	{
		____textWriter_16 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_16), value);
	}

	inline static int32_t get_offset_of__sw_17() { return static_cast<int32_t>(offsetof(TraceJsonReader_t1720844242, ____sw_17)); }
	inline StringWriter_t802263757 * get__sw_17() const { return ____sw_17; }
	inline StringWriter_t802263757 ** get_address_of__sw_17() { return &____sw_17; }
	inline void set__sw_17(StringWriter_t802263757 * value)
	{
		____sw_17 = value;
		Il2CppCodeGenWriteBarrier((&____sw_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONREADER_T1720844242_H
#ifndef JSONARRAYCONTRACT_T3949307279_H
#define JSONARRAYCONTRACT_T3949307279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonArrayContract
struct  JsonArrayContract_t3949307279  : public JsonContainerContract_t790269270
{
public:
	// System.Type Mapbox.Json.Serialization.JsonArrayContract::<CollectionItemType>k__BackingField
	Type_t * ___U3CCollectionItemTypeU3Ek__BackingField_27;
	// System.Boolean Mapbox.Json.Serialization.JsonArrayContract::<IsMultidimensionalArray>k__BackingField
	bool ___U3CIsMultidimensionalArrayU3Ek__BackingField_28;
	// System.Type Mapbox.Json.Serialization.JsonArrayContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_29;
	// System.Type Mapbox.Json.Serialization.JsonArrayContract::_genericWrapperType
	Type_t * ____genericWrapperType_30;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonArrayContract::_genericWrapperCreator
	ObjectConstructor_1_t2523184513 * ____genericWrapperCreator_31;
	// System.Func`1<System.Object> Mapbox.Json.Serialization.JsonArrayContract::_genericTemporaryCollectionCreator
	Func_1_t2509852811 * ____genericTemporaryCollectionCreator_32;
	// System.Boolean Mapbox.Json.Serialization.JsonArrayContract::<IsArray>k__BackingField
	bool ___U3CIsArrayU3Ek__BackingField_33;
	// System.Boolean Mapbox.Json.Serialization.JsonArrayContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_34;
	// System.Boolean Mapbox.Json.Serialization.JsonArrayContract::<CanDeserialize>k__BackingField
	bool ___U3CCanDeserializeU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Mapbox.Json.Serialization.JsonArrayContract::_parameterizedConstructor
	ConstructorInfo_t5769829 * ____parameterizedConstructor_36;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonArrayContract::_parameterizedCreator
	ObjectConstructor_1_t2523184513 * ____parameterizedCreator_37;
	// Mapbox.Json.Serialization.ObjectConstructor`1<System.Object> Mapbox.Json.Serialization.JsonArrayContract::_overrideCreator
	ObjectConstructor_1_t2523184513 * ____overrideCreator_38;
	// System.Boolean Mapbox.Json.Serialization.JsonArrayContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ___U3CCollectionItemTypeU3Ek__BackingField_27)); }
	inline Type_t * get_U3CCollectionItemTypeU3Ek__BackingField_27() const { return ___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline Type_t ** get_address_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return &___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline void set_U3CCollectionItemTypeU3Ek__BackingField_27(Type_t * value)
	{
		___U3CCollectionItemTypeU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionItemTypeU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ___U3CIsMultidimensionalArrayU3Ek__BackingField_28)); }
	inline bool get_U3CIsMultidimensionalArrayU3Ek__BackingField_28() const { return ___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return &___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline void set_U3CIsMultidimensionalArrayU3Ek__BackingField_28(bool value)
	{
		___U3CIsMultidimensionalArrayU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_29() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ____genericCollectionDefinitionType_29)); }
	inline Type_t * get__genericCollectionDefinitionType_29() const { return ____genericCollectionDefinitionType_29; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_29() { return &____genericCollectionDefinitionType_29; }
	inline void set__genericCollectionDefinitionType_29(Type_t * value)
	{
		____genericCollectionDefinitionType_29 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_29), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_30() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ____genericWrapperType_30)); }
	inline Type_t * get__genericWrapperType_30() const { return ____genericWrapperType_30; }
	inline Type_t ** get_address_of__genericWrapperType_30() { return &____genericWrapperType_30; }
	inline void set__genericWrapperType_30(Type_t * value)
	{
		____genericWrapperType_30 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_30), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_31() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ____genericWrapperCreator_31)); }
	inline ObjectConstructor_1_t2523184513 * get__genericWrapperCreator_31() const { return ____genericWrapperCreator_31; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__genericWrapperCreator_31() { return &____genericWrapperCreator_31; }
	inline void set__genericWrapperCreator_31(ObjectConstructor_1_t2523184513 * value)
	{
		____genericWrapperCreator_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_31), value);
	}

	inline static int32_t get_offset_of__genericTemporaryCollectionCreator_32() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ____genericTemporaryCollectionCreator_32)); }
	inline Func_1_t2509852811 * get__genericTemporaryCollectionCreator_32() const { return ____genericTemporaryCollectionCreator_32; }
	inline Func_1_t2509852811 ** get_address_of__genericTemporaryCollectionCreator_32() { return &____genericTemporaryCollectionCreator_32; }
	inline void set__genericTemporaryCollectionCreator_32(Func_1_t2509852811 * value)
	{
		____genericTemporaryCollectionCreator_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryCollectionCreator_32), value);
	}

	inline static int32_t get_offset_of_U3CIsArrayU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ___U3CIsArrayU3Ek__BackingField_33)); }
	inline bool get_U3CIsArrayU3Ek__BackingField_33() const { return ___U3CIsArrayU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CIsArrayU3Ek__BackingField_33() { return &___U3CIsArrayU3Ek__BackingField_33; }
	inline void set_U3CIsArrayU3Ek__BackingField_33(bool value)
	{
		___U3CIsArrayU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ___U3CShouldCreateWrapperU3Ek__BackingField_34)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_34() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return &___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_34(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CCanDeserializeU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ___U3CCanDeserializeU3Ek__BackingField_35)); }
	inline bool get_U3CCanDeserializeU3Ek__BackingField_35() const { return ___U3CCanDeserializeU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CCanDeserializeU3Ek__BackingField_35() { return &___U3CCanDeserializeU3Ek__BackingField_35; }
	inline void set_U3CCanDeserializeU3Ek__BackingField_35(bool value)
	{
		___U3CCanDeserializeU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t5769829 * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t5769829 ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t5769829 * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_37() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ____parameterizedCreator_37)); }
	inline ObjectConstructor_1_t2523184513 * get__parameterizedCreator_37() const { return ____parameterizedCreator_37; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__parameterizedCreator_37() { return &____parameterizedCreator_37; }
	inline void set__parameterizedCreator_37(ObjectConstructor_1_t2523184513 * value)
	{
		____parameterizedCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_37), value);
	}

	inline static int32_t get_offset_of__overrideCreator_38() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ____overrideCreator_38)); }
	inline ObjectConstructor_1_t2523184513 * get__overrideCreator_38() const { return ____overrideCreator_38; }
	inline ObjectConstructor_1_t2523184513 ** get_address_of__overrideCreator_38() { return &____overrideCreator_38; }
	inline void set__overrideCreator_38(ObjectConstructor_1_t2523184513 * value)
	{
		____overrideCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonArrayContract_t3949307279, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAYCONTRACT_T3949307279_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (PropertyNameTable_t42095745), -1, sizeof(PropertyNameTable_t42095745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2700[4] = 
{
	PropertyNameTable_t42095745_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_t42095745::get_offset_of__count_1(),
	PropertyNameTable_t42095745::get_offset_of__entries_2(),
	PropertyNameTable_t42095745::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (Entry_t1310988683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[3] = 
{
	Entry_t1310988683::get_offset_of_Value_0(),
	Entry_t1310988683::get_offset_of_HashCode_1(),
	Entry_t1310988683::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (ReflectionDelegateFactory_t2629290932), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (LateBoundReflectionDelegateFactory_t3245749508), -1, sizeof(LateBoundReflectionDelegateFactory_t3245749508_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2703[1] = 
{
	LateBoundReflectionDelegateFactory_t3245749508_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (U3CU3Ec__DisplayClass3_0_t3305159926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[2] = 
{
	U3CU3Ec__DisplayClass3_0_t3305159926::get_offset_of_c_0(),
	U3CU3Ec__DisplayClass3_0_t3305159926::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (ReflectionMember_t168985657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	ReflectionMember_t168985657::get_offset_of_U3CMemberTypeU3Ek__BackingField_0(),
	ReflectionMember_t168985657::get_offset_of_U3CGetterU3Ek__BackingField_1(),
	ReflectionMember_t168985657::get_offset_of_U3CSetterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (ReflectionObject_t2904696228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[2] = 
{
	ReflectionObject_t2904696228::get_offset_of_U3CCreatorU3Ek__BackingField_0(),
	ReflectionObject_t2904696228::get_offset_of_U3CMembersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (U3CU3Ec__DisplayClass13_0_t3338895402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[1] = 
{
	U3CU3Ec__DisplayClass13_0_t3338895402::get_offset_of_ctor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (U3CU3Ec__DisplayClass13_1_t1382580266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[1] = 
{
	U3CU3Ec__DisplayClass13_1_t1382580266::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (U3CU3Ec__DisplayClass13_2_t2956558378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[1] = 
{
	U3CU3Ec__DisplayClass13_2_t2956558378::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (StringReference_t2330690408)+ sizeof (RuntimeObject), sizeof(StringReference_t2330690408_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2717[3] = 
{
	StringReference_t2330690408::get_offset_of__chars_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t2330690408::get_offset_of__startIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t2330690408::get_offset_of__length_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (StringReferenceExtensions_t1696722107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (PrimitiveTypeCode_t2887194885)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2721[43] = 
{
	PrimitiveTypeCode_t2887194885::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (TypeInformation_t178482004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	TypeInformation_t178482004::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	TypeInformation_t178482004::get_offset_of_U3CTypeCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (ParseResult_t2731952844)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2723[5] = 
{
	ParseResult_t2731952844::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (ConvertUtils_t1194917319), -1, sizeof(ConvertUtils_t1194917319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2724[3] = 
{
	ConvertUtils_t1194917319_StaticFields::get_offset_of_TypeCodeMap_0(),
	ConvertUtils_t1194917319_StaticFields::get_offset_of_PrimitiveTypeCodes_1(),
	ConvertUtils_t1194917319_StaticFields::get_offset_of_CastConverters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (TypeConvertKey_t1086254200)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[2] = 
{
	TypeConvertKey_t1086254200::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeConvertKey_t1086254200::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (ConvertResult_t3062627003)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2726[5] = 
{
	ConvertResult_t3062627003::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (IEEE754_t1074200465), -1, sizeof(IEEE754_t1074200465_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2727[6] = 
{
	IEEE754_t1074200465_StaticFields::get_offset_of_MultExp64Power10_0(),
	IEEE754_t1074200465_StaticFields::get_offset_of_MultVal64Power10_1(),
	IEEE754_t1074200465_StaticFields::get_offset_of_MultVal64Power10Inv_2(),
	IEEE754_t1074200465_StaticFields::get_offset_of_MultExp64Power10By16_3(),
	IEEE754_t1074200465_StaticFields::get_offset_of_MultVal64Power10By16_4(),
	IEEE754_t1074200465_StaticFields::get_offset_of_MultVal64Power10By16Inv_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (U3CU3Ec__DisplayClass9_0_t912528346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[1] = 
{
	U3CU3Ec__DisplayClass9_0_t912528346::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (DateTimeUtils_t327169568), -1, sizeof(DateTimeUtils_t327169568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2731[3] = 
{
	DateTimeUtils_t327169568_StaticFields::get_offset_of_InitialJavaScriptDateTicks_0(),
	DateTimeUtils_t327169568_StaticFields::get_offset_of_DaysToMonth365_1(),
	DateTimeUtils_t327169568_StaticFields::get_offset_of_DaysToMonth366_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (EnumUtils_t741247584), -1, sizeof(EnumUtils_t741247584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2736[1] = 
{
	EnumUtils_t741247584_StaticFields::get_offset_of_EnumMemberNamesPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (U3CU3Ec_t1388036768), -1, sizeof(U3CU3Ec_t1388036768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2737[2] = 
{
	U3CU3Ec_t1388036768_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1388036768_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (BufferUtils_t2040455361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (JavaScriptUtils_t3910841289), -1, sizeof(JavaScriptUtils_t3910841289_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2739[3] = 
{
	JavaScriptUtils_t3910841289_StaticFields::get_offset_of_SingleQuoteCharEscapeFlags_0(),
	JavaScriptUtils_t3910841289_StaticFields::get_offset_of_DoubleQuoteCharEscapeFlags_1(),
	JavaScriptUtils_t3910841289_StaticFields::get_offset_of_HtmlCharEscapeFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (StringBuffer_t3305294883)+ sizeof (RuntimeObject), sizeof(StringBuffer_t3305294883_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[2] = 
{
	StringBuffer_t3305294883::get_offset_of__buffer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringBuffer_t3305294883::get_offset_of__position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (CollectionUtils_t3138840482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (MathUtils_t1802260133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (MiscellaneousUtils_t4176861027), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (ReflectionUtils_t1035580559), -1, sizeof(ReflectionUtils_t1035580559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[1] = 
{
	ReflectionUtils_t1035580559_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (U3CU3Ec_t3985673557), -1, sizeof(U3CU3Ec_t3985673557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[5] = 
{
	U3CU3Ec_t3985673557_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3985673557_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
	U3CU3Ec_t3985673557_StaticFields::get_offset_of_U3CU3E9__30_0_2(),
	U3CU3Ec_t3985673557_StaticFields::get_offset_of_U3CU3E9__38_0_3(),
	U3CU3Ec_t3985673557_StaticFields::get_offset_of_U3CU3E9__40_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (U3CU3Ec__DisplayClass43_0_t1903311374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[1] = 
{
	U3CU3Ec__DisplayClass43_0_t1903311374::get_offset_of_subTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (U3CU3Ec__DisplayClass43_1_t3469395315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[2] = 
{
	U3CU3Ec__DisplayClass43_1_t3469395315::get_offset_of_subTypePropertyDeclaringType_0(),
	U3CU3Ec__DisplayClass43_1_t3469395315::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (TypeNameKey_t750778342)+ sizeof (RuntimeObject), sizeof(TypeNameKey_t750778342_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2748[2] = 
{
	TypeNameKey_t750778342::get_offset_of_AssemblyName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeNameKey_t750778342::get_offset_of_TypeName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (StringUtils_t1714050313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (TypeExtensions_t4252539098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (ValidationUtils_t1614134767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (JsonContainerContract_t790269270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[6] = 
{
	JsonContainerContract_t790269270::get_offset_of__itemContract_21(),
	JsonContainerContract_t790269270::get_offset_of__finalItemContract_22(),
	JsonContainerContract_t790269270::get_offset_of_U3CItemConverterU3Ek__BackingField_23(),
	JsonContainerContract_t790269270::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24(),
	JsonContainerContract_t790269270::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(),
	JsonContainerContract_t790269270::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (NamingStrategy_t3171016818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[3] = 
{
	NamingStrategy_t3171016818::get_offset_of_U3CProcessDictionaryKeysU3Ek__BackingField_0(),
	NamingStrategy_t3171016818::get_offset_of_U3CProcessExtensionDataNamesU3Ek__BackingField_1(),
	NamingStrategy_t3171016818::get_offset_of_U3COverrideSpecifiedNamesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (ReflectionAttributeProvider_t2348600233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[1] = 
{
	ReflectionAttributeProvider_t2348600233::get_offset_of__attributeProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (TraceJsonReader_t1720844242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[3] = 
{
	TraceJsonReader_t1720844242::get_offset_of__innerReader_15(),
	TraceJsonReader_t1720844242::get_offset_of__textWriter_16(),
	TraceJsonReader_t1720844242::get_offset_of__sw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (TraceJsonWriter_t2956702503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[3] = 
{
	TraceJsonWriter_t2956702503::get_offset_of__innerWriter_14(),
	TraceJsonWriter_t2956702503::get_offset_of__textWriter_15(),
	TraceJsonWriter_t2956702503::get_offset_of__sw_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (JsonFormatterConverter_t3163187665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[3] = 
{
	JsonFormatterConverter_t3163187665::get_offset_of__reader_0(),
	JsonFormatterConverter_t3163187665::get_offset_of__contract_1(),
	JsonFormatterConverter_t3163187665::get_offset_of__member_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (JsonISerializableContract_t2905058752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[1] = 
{
	JsonISerializableContract_t2905058752::get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (JsonLinqContract_t1947983261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (JsonPrimitiveContract_t959031370), -1, sizeof(JsonPrimitiveContract_t959031370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2764[2] = 
{
	JsonPrimitiveContract_t959031370::get_offset_of_U3CTypeCodeU3Ek__BackingField_21(),
	JsonPrimitiveContract_t959031370_StaticFields::get_offset_of_ReadTypeMap_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (ErrorEventArgs_t2120017399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[2] = 
{
	ErrorEventArgs_t2120017399::get_offset_of_U3CCurrentObjectU3Ek__BackingField_1(),
	ErrorEventArgs_t2120017399::get_offset_of_U3CErrorContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (DefaultReferenceResolver_t674819107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[1] = 
{
	DefaultReferenceResolver_t674819107::get_offset_of__referenceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (DefaultContractResolver_t2734327692), -1, sizeof(DefaultContractResolver_t2734327692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2767[10] = 
{
	DefaultContractResolver_t2734327692_StaticFields::get_offset_of_Instance_0(),
	DefaultContractResolver_t2734327692_StaticFields::get_offset_of_BuiltInConverters_1(),
	DefaultContractResolver_t2734327692::get_offset_of__typeContractCacheLock_2(),
	DefaultContractResolver_t2734327692::get_offset_of__nameTable_3(),
	DefaultContractResolver_t2734327692::get_offset_of__contractCache_4(),
	DefaultContractResolver_t2734327692::get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5(),
	DefaultContractResolver_t2734327692::get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6(),
	DefaultContractResolver_t2734327692::get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7(),
	DefaultContractResolver_t2734327692::get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_8(),
	DefaultContractResolver_t2734327692::get_offset_of_U3CNamingStrategyU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (U3CU3Ec_t4006129445), -1, sizeof(U3CU3Ec_t4006129445_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2770[7] = 
{
	U3CU3Ec_t4006129445_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4006129445_StaticFields::get_offset_of_U3CU3E9__29_0_1(),
	U3CU3Ec_t4006129445_StaticFields::get_offset_of_U3CU3E9__29_1_2(),
	U3CU3Ec_t4006129445_StaticFields::get_offset_of_U3CU3E9__32_0_3(),
	U3CU3Ec_t4006129445_StaticFields::get_offset_of_U3CU3E9__32_1_4(),
	U3CU3Ec_t4006129445_StaticFields::get_offset_of_U3CU3E9__35_0_5(),
	U3CU3Ec_t4006129445_StaticFields::get_offset_of_U3CU3E9__59_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (U3CU3Ec__DisplayClass31_0_t3033093971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[1] = 
{
	U3CU3Ec__DisplayClass31_0_t3033093971::get_offset_of_namingStrategy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (U3CU3Ec__DisplayClass33_0_t3033093969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[2] = 
{
	U3CU3Ec__DisplayClass33_0_t3033093969::get_offset_of_getExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass33_0_t3033093969::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (U3CU3Ec__DisplayClass33_1_t1467010028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[4] = 
{
	U3CU3Ec__DisplayClass33_1_t1467010028::get_offset_of_setExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass33_1_t1467010028::get_offset_of_createExtensionDataDictionary_1(),
	U3CU3Ec__DisplayClass33_1_t1467010028::get_offset_of_setExtensionDataDictionaryValue_2(),
	U3CU3Ec__DisplayClass33_1_t1467010028::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (U3CU3Ec__DisplayClass33_2_t4195893383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[2] = 
{
	U3CU3Ec__DisplayClass33_2_t4195893383::get_offset_of_createEnumerableWrapper_0(),
	U3CU3Ec__DisplayClass33_2_t4195893383::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (U3CU3Ec__DisplayClass47_0_t3434764109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[1] = 
{
	U3CU3Ec__DisplayClass47_0_t3434764109::get_offset_of_namingStrategy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (U3CU3Ec__DisplayClass64_0_t3702544208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[1] = 
{
	U3CU3Ec__DisplayClass64_0_t3702544208::get_offset_of_shouldSerializeCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (U3CU3Ec__DisplayClass65_0_t3702544207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[1] = 
{
	U3CU3Ec__DisplayClass65_0_t3702544207::get_offset_of_specifiedPropertyGet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (DefaultSerializationBinder_t1191207404), -1, sizeof(DefaultSerializationBinder_t1191207404_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2778[2] = 
{
	DefaultSerializationBinder_t1191207404_StaticFields::get_offset_of_Instance_0(),
	DefaultSerializationBinder_t1191207404::get_offset_of__typeCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (ErrorContext_t3853262553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[6] = 
{
	ErrorContext_t3853262553::get_offset_of_U3CTracedU3Ek__BackingField_0(),
	ErrorContext_t3853262553::get_offset_of_U3CErrorU3Ek__BackingField_1(),
	ErrorContext_t3853262553::get_offset_of_U3COriginalObjectU3Ek__BackingField_2(),
	ErrorContext_t3853262553::get_offset_of_U3CMemberU3Ek__BackingField_3(),
	ErrorContext_t3853262553::get_offset_of_U3CPathU3Ek__BackingField_4(),
	ErrorContext_t3853262553::get_offset_of_U3CHandledU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (JsonArrayContract_t3949307279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[13] = 
{
	JsonArrayContract_t3949307279::get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27(),
	JsonArrayContract_t3949307279::get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28(),
	JsonArrayContract_t3949307279::get_offset_of__genericCollectionDefinitionType_29(),
	JsonArrayContract_t3949307279::get_offset_of__genericWrapperType_30(),
	JsonArrayContract_t3949307279::get_offset_of__genericWrapperCreator_31(),
	JsonArrayContract_t3949307279::get_offset_of__genericTemporaryCollectionCreator_32(),
	JsonArrayContract_t3949307279::get_offset_of_U3CIsArrayU3Ek__BackingField_33(),
	JsonArrayContract_t3949307279::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34(),
	JsonArrayContract_t3949307279::get_offset_of_U3CCanDeserializeU3Ek__BackingField_35(),
	JsonArrayContract_t3949307279::get_offset_of__parameterizedConstructor_36(),
	JsonArrayContract_t3949307279::get_offset_of__parameterizedCreator_37(),
	JsonArrayContract_t3949307279::get_offset_of__overrideCreator_38(),
	JsonArrayContract_t3949307279::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (JsonContractType_t1860678823)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2783[10] = 
{
	JsonContractType_t1860678823::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (SerializationCallback_t1737529799), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (SerializationErrorCallback_t811918742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (ExtensionDataSetter_t1236496959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (ExtensionDataGetter_t1148678719), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (JsonContract_t968551136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[21] = 
{
	JsonContract_t968551136::get_offset_of_IsNullable_0(),
	JsonContract_t968551136::get_offset_of_IsConvertable_1(),
	JsonContract_t968551136::get_offset_of_IsEnum_2(),
	JsonContract_t968551136::get_offset_of_NonNullableUnderlyingType_3(),
	JsonContract_t968551136::get_offset_of_InternalReadType_4(),
	JsonContract_t968551136::get_offset_of_ContractType_5(),
	JsonContract_t968551136::get_offset_of_IsReadOnlyOrFixedSize_6(),
	JsonContract_t968551136::get_offset_of_IsSealed_7(),
	JsonContract_t968551136::get_offset_of_IsInstantiable_8(),
	JsonContract_t968551136::get_offset_of__onDeserializedCallbacks_9(),
	JsonContract_t968551136::get_offset_of__onDeserializingCallbacks_10(),
	JsonContract_t968551136::get_offset_of__onSerializedCallbacks_11(),
	JsonContract_t968551136::get_offset_of__onSerializingCallbacks_12(),
	JsonContract_t968551136::get_offset_of__onErrorCallbacks_13(),
	JsonContract_t968551136::get_offset_of__createdType_14(),
	JsonContract_t968551136::get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15(),
	JsonContract_t968551136::get_offset_of_U3CIsReferenceU3Ek__BackingField_16(),
	JsonContract_t968551136::get_offset_of_U3CConverterU3Ek__BackingField_17(),
	JsonContract_t968551136::get_offset_of_U3CInternalConverterU3Ek__BackingField_18(),
	JsonContract_t968551136::get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19(),
	JsonContract_t968551136::get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (U3CU3Ec__DisplayClass58_0_t3999886043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[1] = 
{
	U3CU3Ec__DisplayClass58_0_t3999886043::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (U3CU3Ec__DisplayClass59_0_t3999951579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[1] = 
{
	U3CU3Ec__DisplayClass59_0_t3999951579::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (JsonDictionaryContract_t170548660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[13] = 
{
	JsonDictionaryContract_t170548660::get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27(),
	JsonDictionaryContract_t170548660::get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28(),
	JsonDictionaryContract_t170548660::get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29(),
	JsonDictionaryContract_t170548660::get_offset_of_U3CKeyContractU3Ek__BackingField_30(),
	JsonDictionaryContract_t170548660::get_offset_of__genericCollectionDefinitionType_31(),
	JsonDictionaryContract_t170548660::get_offset_of__genericWrapperType_32(),
	JsonDictionaryContract_t170548660::get_offset_of__genericWrapperCreator_33(),
	JsonDictionaryContract_t170548660::get_offset_of__genericTemporaryDictionaryCreator_34(),
	JsonDictionaryContract_t170548660::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35(),
	JsonDictionaryContract_t170548660::get_offset_of__parameterizedConstructor_36(),
	JsonDictionaryContract_t170548660::get_offset_of__overrideCreator_37(),
	JsonDictionaryContract_t170548660::get_offset_of__parameterizedCreator_38(),
	JsonDictionaryContract_t170548660::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (JsonProperty_t1629976260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[33] = 
{
	JsonProperty_t1629976260::get_offset_of__required_0(),
	JsonProperty_t1629976260::get_offset_of__hasExplicitDefaultValue_1(),
	JsonProperty_t1629976260::get_offset_of__defaultValue_2(),
	JsonProperty_t1629976260::get_offset_of__hasGeneratedDefaultValue_3(),
	JsonProperty_t1629976260::get_offset_of__propertyName_4(),
	JsonProperty_t1629976260::get_offset_of__skipPropertyNameEscape_5(),
	JsonProperty_t1629976260::get_offset_of__propertyType_6(),
	JsonProperty_t1629976260::get_offset_of_U3CPropertyContractU3Ek__BackingField_7(),
	JsonProperty_t1629976260::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8(),
	JsonProperty_t1629976260::get_offset_of_U3COrderU3Ek__BackingField_9(),
	JsonProperty_t1629976260::get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10(),
	JsonProperty_t1629976260::get_offset_of_U3CValueProviderU3Ek__BackingField_11(),
	JsonProperty_t1629976260::get_offset_of_U3CAttributeProviderU3Ek__BackingField_12(),
	JsonProperty_t1629976260::get_offset_of_U3CConverterU3Ek__BackingField_13(),
	JsonProperty_t1629976260::get_offset_of_U3CMemberConverterU3Ek__BackingField_14(),
	JsonProperty_t1629976260::get_offset_of_U3CIgnoredU3Ek__BackingField_15(),
	JsonProperty_t1629976260::get_offset_of_U3CReadableU3Ek__BackingField_16(),
	JsonProperty_t1629976260::get_offset_of_U3CWritableU3Ek__BackingField_17(),
	JsonProperty_t1629976260::get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18(),
	JsonProperty_t1629976260::get_offset_of_U3CIsReferenceU3Ek__BackingField_19(),
	JsonProperty_t1629976260::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20(),
	JsonProperty_t1629976260::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21(),
	JsonProperty_t1629976260::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22(),
	JsonProperty_t1629976260::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23(),
	JsonProperty_t1629976260::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24(),
	JsonProperty_t1629976260::get_offset_of_U3CShouldSerializeU3Ek__BackingField_25(),
	JsonProperty_t1629976260::get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26(),
	JsonProperty_t1629976260::get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27(),
	JsonProperty_t1629976260::get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28(),
	JsonProperty_t1629976260::get_offset_of_U3CItemConverterU3Ek__BackingField_29(),
	JsonProperty_t1629976260::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30(),
	JsonProperty_t1629976260::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31(),
	JsonProperty_t1629976260::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (JsonPropertyCollection_t1665309380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[2] = 
{
	JsonPropertyCollection_t1665309380::get_offset_of__type_5(),
	JsonPropertyCollection_t1665309380::get_offset_of__list_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (JsonObjectContract_t1235679874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[12] = 
{
	JsonObjectContract_t1235679874::get_offset_of_U3CMemberSerializationU3Ek__BackingField_27(),
	JsonObjectContract_t1235679874::get_offset_of_U3CItemRequiredU3Ek__BackingField_28(),
	JsonObjectContract_t1235679874::get_offset_of_U3CPropertiesU3Ek__BackingField_29(),
	JsonObjectContract_t1235679874::get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30(),
	JsonObjectContract_t1235679874::get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31(),
	JsonObjectContract_t1235679874::get_offset_of_U3CExtensionDataNameResolverU3Ek__BackingField_32(),
	JsonObjectContract_t1235679874::get_offset_of_ExtensionDataIsJToken_33(),
	JsonObjectContract_t1235679874::get_offset_of__hasRequiredOrDefaultValueProperties_34(),
	JsonObjectContract_t1235679874::get_offset_of__overrideCreator_35(),
	JsonObjectContract_t1235679874::get_offset_of__parameterizedCreator_36(),
	JsonObjectContract_t1235679874::get_offset_of__creatorParameters_37(),
	JsonObjectContract_t1235679874::get_offset_of__extensionDataValueType_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (JsonSerializerInternalBase_t2072393995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[5] = 
{
	JsonSerializerInternalBase_t2072393995::get_offset_of__currentErrorContext_0(),
	JsonSerializerInternalBase_t2072393995::get_offset_of__mappings_1(),
	JsonSerializerInternalBase_t2072393995::get_offset_of_Serializer_2(),
	JsonSerializerInternalBase_t2072393995::get_offset_of_TraceWriter_3(),
	JsonSerializerInternalBase_t2072393995::get_offset_of_InternalSerializer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (ReferenceEqualsEqualityComparer_t742836987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (JsonSerializerInternalReader_t3284513685), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (PropertyPresence_t53006651)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2799[4] = 
{
	PropertyPresence_t53006651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
