﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DigitalRubyShared.LongPressGestureRecognizer
struct LongPressGestureRecognizer_t3980777482;
// System.Action`1<Mapbox.Directions.DirectionsResponse>
struct Action_1_t4180088745;
// Mapbox.Directions.Directions
struct Directions_t1397515081;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Mapbox.Geocoding.Feature>
struct List_1_t1283026911;
// Mapbox.Platform.IFileSource
struct IFileSource_t3859839141;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t225505033;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>>
struct Func_2_t3661664344;
// System.Func`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>>
struct Func_2_t788283625;
// System.Func`2<<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>,System.String>
struct Func_2_t4279064255;
// System.UInt64[]
struct UInt64U5BU5D_t1659327989;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<Mapbox.Directions.Intersection>
struct List_1_t611829391;
// System.Collections.Generic.List`1<Mapbox.Utils.Vector2d>
struct List_1_t3337321310;
// Mapbox.Directions.Maneuver
struct Maneuver_t1966826127;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_t3742930331;
// System.Collections.Generic.List`1<Mapbox.Directions.Step>
struct List_1_t2668084909;
// System.Collections.Generic.Queue`1<DigitalRubyShared.GestureVelocityTracker/VelocityHistory>
struct Queue_1_t1111824545;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.Collections.Generic.List`1<Mapbox.Directions.Route>
struct List_1_t3433222835;
// System.Collections.Generic.List`1<Mapbox.Directions.Waypoint>
struct List_1_t815574591;
// System.Action
struct Action_t1264377477;
// System.Collections.Generic.List`1<Mapbox.Directions.Leg>
struct List_1_t1063068110;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Func`2<TMPro.KerningPair,System.Int32>
struct Func_2_t2554309206;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.String[]
struct StringU5BU5D_t1281789340;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// UnityEngine.Material
struct Material_t340375123;
// System.Void
struct Void_t1185182177;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1569362707;
// Mapbox.Directions.RoutingProfile
struct RoutingProfile_t2080742987;
// Mapbox.Utils.Vector2d[]
struct Vector2dU5BU5D_t852968953;
// Mapbox.Utils.BearingFilter[]
struct BearingFilterU5BU5D_t1825218296;
// Mapbox.Directions.Overview
struct Overview_t3152666467;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// DigitalRubyShared.DemoScriptZoomPanCamera
struct DemoScriptZoomPanCamera_t3831420416;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch>
struct List_1_t3464476875;
// System.Collections.ObjectModel.ReadOnlyCollection`1<DigitalRubyShared.GestureTouch>
struct ReadOnlyCollection_1_t3204978420;
// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureRecognizer>
struct HashSet_1_t2248979155;
// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer>
struct List_1_t861137127;
// DigitalRubyShared.GestureVelocityTracker
struct GestureVelocityTracker_t806949657;
// DigitalRubyShared.GestureRecognizerUpdated
struct GestureRecognizerUpdated_t601711085;
// DigitalRubyShared.GestureRecognizer/CallbackMainThreadDelegate
struct CallbackMainThreadDelegate_t469493312;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point>>
struct List_1_t1060306332;
// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point>
struct List_1_t3883198886;
// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureImage>
struct List_1_t62703717;
// DigitalRubyShared.ImageGestureImage
struct ImageGestureImage_t2885596271;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// Mapbox.Geocoding.Geometry
struct Geometry_t3224508297;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct List_1_t3104781730;
// DigitalRubyShared.GestureRecognizer
struct GestureRecognizer_t3684029681;
// System.Collections.Generic.ICollection`1<DigitalRubyShared.GestureTouch>
struct ICollection_1_t525587071;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// DigitalRubyShared.FingersDPadScript
struct FingersDPadScript_t3801874975;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// DigitalRubyShared.FingersScript
struct FingersScript_t1857011421;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// DigitalRubyShared.ImageGestureRecognizer
struct ImageGestureRecognizer_t4233185475;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// System.Collections.Generic.Dictionary`2<DigitalRubyShared.ImageGestureImage,System.String>
struct Dictionary_2_t3194170630;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct List_1_t805411711;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// DigitalRubyShared.PanGestureRecognizer
struct PanGestureRecognizer_t195762396;
// DigitalRubyShared.ScaleGestureRecognizer
struct ScaleGestureRecognizer_t1137887245;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// DigitalRubyShared.TapGestureRecognizer
struct TapGestureRecognizer_t3178883670;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295;
// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.TapGestureRecognizer>
struct Action_3_t3049541042;
// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.PanGestureRecognizer>
struct Action_3_t66419768;
// DigitalRubyShared.OneTouchRotateGestureRecognizer
struct OneTouchRotateGestureRecognizer_t3272893959;
// DigitalRubyShared.OneTouchScaleGestureRecognizer
struct OneTouchScaleGestureRecognizer_t1313669683;
// System.Action`2<DigitalRubyShared.FingersJoystickScript,UnityEngine.Vector2>
struct Action_2_t1565375025;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// DigitalRubyShared.SwipeGestureRecognizer
struct SwipeGestureRecognizer_t2328511861;
// DigitalRubyShared.RotateGestureRecognizer
struct RotateGestureRecognizer_t4100246528;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Func`2<UnityEngine.GameObject,System.Nullable`1<System.Boolean>>
struct Func_2_t1671534078;
// UnityEngine.Transform
struct Transform_t3600365921;
// DigitalRubyShared.FingersJoystickScript
struct FingersJoystickScript_t2468414040;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Collections.Generic.KeyValuePair`2<System.Single,System.Single>[]
struct KeyValuePair_2U5BU5D_t1094138374;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t1474424692;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector2>
struct Dictionary_2_t1044942854;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745553_H
#define U3CMODULEU3E_T692745553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745553 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745553_H
#ifndef U3CATTEMPTTOSTARTAFTERDELAYU3EC__ANONSTOREY0_T3552537278_H
#define U3CATTEMPTTOSTARTAFTERDELAYU3EC__ANONSTOREY0_T3552537278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.LongPressGestureRecognizer/<AttemptToStartAfterDelay>c__AnonStorey0
struct  U3CAttemptToStartAfterDelayU3Ec__AnonStorey0_t3552537278  : public RuntimeObject
{
public:
	// System.Int32 DigitalRubyShared.LongPressGestureRecognizer/<AttemptToStartAfterDelay>c__AnonStorey0::tempTag
	int32_t ___tempTag_0;
	// DigitalRubyShared.LongPressGestureRecognizer DigitalRubyShared.LongPressGestureRecognizer/<AttemptToStartAfterDelay>c__AnonStorey0::$this
	LongPressGestureRecognizer_t3980777482 * ___U24this_1;

public:
	inline static int32_t get_offset_of_tempTag_0() { return static_cast<int32_t>(offsetof(U3CAttemptToStartAfterDelayU3Ec__AnonStorey0_t3552537278, ___tempTag_0)); }
	inline int32_t get_tempTag_0() const { return ___tempTag_0; }
	inline int32_t* get_address_of_tempTag_0() { return &___tempTag_0; }
	inline void set_tempTag_0(int32_t value)
	{
		___tempTag_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAttemptToStartAfterDelayU3Ec__AnonStorey0_t3552537278, ___U24this_1)); }
	inline LongPressGestureRecognizer_t3980777482 * get_U24this_1() const { return ___U24this_1; }
	inline LongPressGestureRecognizer_t3980777482 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LongPressGestureRecognizer_t3980777482 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CATTEMPTTOSTARTAFTERDELAYU3EC__ANONSTOREY0_T3552537278_H
#ifndef U3CQUERYU3EC__ANONSTOREY0_T2317980075_H
#define U3CQUERYU3EC__ANONSTOREY0_T2317980075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Directions/<Query>c__AnonStorey0
struct  U3CQueryU3Ec__AnonStorey0_t2317980075  : public RuntimeObject
{
public:
	// System.Action`1<Mapbox.Directions.DirectionsResponse> Mapbox.Directions.Directions/<Query>c__AnonStorey0::callback
	Action_1_t4180088745 * ___callback_0;
	// Mapbox.Directions.Directions Mapbox.Directions.Directions/<Query>c__AnonStorey0::$this
	Directions_t1397515081 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CQueryU3Ec__AnonStorey0_t2317980075, ___callback_0)); }
	inline Action_1_t4180088745 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t4180088745 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t4180088745 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CQueryU3Ec__AnonStorey0_t2317980075, ___U24this_1)); }
	inline Directions_t1397515081 * get_U24this_1() const { return ___U24this_1; }
	inline Directions_t1397515081 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Directions_t1397515081 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CQUERYU3EC__ANONSTOREY0_T2317980075_H
#ifndef GEOCODERESPONSE_T3526499648_H
#define GEOCODERESPONSE_T3526499648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.GeocodeResponse
struct  GeocodeResponse_t3526499648  : public RuntimeObject
{
public:
	// System.String Mapbox.Geocoding.GeocodeResponse::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Mapbox.Geocoding.Feature> Mapbox.Geocoding.GeocodeResponse::<Features>k__BackingField
	List_1_t1283026911 * ___U3CFeaturesU3Ek__BackingField_1;
	// System.String Mapbox.Geocoding.GeocodeResponse::<Attribution>k__BackingField
	String_t* ___U3CAttributionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GeocodeResponse_t3526499648, ___U3CTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CFeaturesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GeocodeResponse_t3526499648, ___U3CFeaturesU3Ek__BackingField_1)); }
	inline List_1_t1283026911 * get_U3CFeaturesU3Ek__BackingField_1() const { return ___U3CFeaturesU3Ek__BackingField_1; }
	inline List_1_t1283026911 ** get_address_of_U3CFeaturesU3Ek__BackingField_1() { return &___U3CFeaturesU3Ek__BackingField_1; }
	inline void set_U3CFeaturesU3Ek__BackingField_1(List_1_t1283026911 * value)
	{
		___U3CFeaturesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFeaturesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAttributionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GeocodeResponse_t3526499648, ___U3CAttributionU3Ek__BackingField_2)); }
	inline String_t* get_U3CAttributionU3Ek__BackingField_2() const { return ___U3CAttributionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CAttributionU3Ek__BackingField_2() { return &___U3CAttributionU3Ek__BackingField_2; }
	inline void set_U3CAttributionU3Ek__BackingField_2(String_t* value)
	{
		___U3CAttributionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAttributionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOCODERESPONSE_T3526499648_H
#ifndef GEOCODER_T3195298050_H
#define GEOCODER_T3195298050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.Geocoder
struct  Geocoder_t3195298050  : public RuntimeObject
{
public:
	// Mapbox.Platform.IFileSource Mapbox.Geocoding.Geocoder::fileSource
	RuntimeObject* ___fileSource_0;

public:
	inline static int32_t get_offset_of_fileSource_0() { return static_cast<int32_t>(offsetof(Geocoder_t3195298050, ___fileSource_0)); }
	inline RuntimeObject* get_fileSource_0() const { return ___fileSource_0; }
	inline RuntimeObject** get_address_of_fileSource_0() { return &___fileSource_0; }
	inline void set_fileSource_0(RuntimeObject* value)
	{
		___fileSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___fileSource_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOCODER_T3195298050_H
#ifndef ROUTINGPROFILE_T2080742987_H
#define ROUTINGPROFILE_T2080742987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.RoutingProfile
struct  RoutingProfile_t2080742987  : public RuntimeObject
{
public:
	// System.String Mapbox.Directions.RoutingProfile::profile
	String_t* ___profile_3;

public:
	inline static int32_t get_offset_of_profile_3() { return static_cast<int32_t>(offsetof(RoutingProfile_t2080742987, ___profile_3)); }
	inline String_t* get_profile_3() const { return ___profile_3; }
	inline String_t** get_address_of_profile_3() { return &___profile_3; }
	inline void set_profile_3(String_t* value)
	{
		___profile_3 = value;
		Il2CppCodeGenWriteBarrier((&___profile_3), value);
	}
};

struct RoutingProfile_t2080742987_StaticFields
{
public:
	// Mapbox.Directions.RoutingProfile Mapbox.Directions.RoutingProfile::Driving
	RoutingProfile_t2080742987 * ___Driving_0;
	// Mapbox.Directions.RoutingProfile Mapbox.Directions.RoutingProfile::Walking
	RoutingProfile_t2080742987 * ___Walking_1;
	// Mapbox.Directions.RoutingProfile Mapbox.Directions.RoutingProfile::Cycling
	RoutingProfile_t2080742987 * ___Cycling_2;

public:
	inline static int32_t get_offset_of_Driving_0() { return static_cast<int32_t>(offsetof(RoutingProfile_t2080742987_StaticFields, ___Driving_0)); }
	inline RoutingProfile_t2080742987 * get_Driving_0() const { return ___Driving_0; }
	inline RoutingProfile_t2080742987 ** get_address_of_Driving_0() { return &___Driving_0; }
	inline void set_Driving_0(RoutingProfile_t2080742987 * value)
	{
		___Driving_0 = value;
		Il2CppCodeGenWriteBarrier((&___Driving_0), value);
	}

	inline static int32_t get_offset_of_Walking_1() { return static_cast<int32_t>(offsetof(RoutingProfile_t2080742987_StaticFields, ___Walking_1)); }
	inline RoutingProfile_t2080742987 * get_Walking_1() const { return ___Walking_1; }
	inline RoutingProfile_t2080742987 ** get_address_of_Walking_1() { return &___Walking_1; }
	inline void set_Walking_1(RoutingProfile_t2080742987 * value)
	{
		___Walking_1 = value;
		Il2CppCodeGenWriteBarrier((&___Walking_1), value);
	}

	inline static int32_t get_offset_of_Cycling_2() { return static_cast<int32_t>(offsetof(RoutingProfile_t2080742987_StaticFields, ___Cycling_2)); }
	inline RoutingProfile_t2080742987 * get_Cycling_2() const { return ___Cycling_2; }
	inline RoutingProfile_t2080742987 ** get_address_of_Cycling_2() { return &___Cycling_2; }
	inline void set_Cycling_2(RoutingProfile_t2080742987 * value)
	{
		___Cycling_2 = value;
		Il2CppCodeGenWriteBarrier((&___Cycling_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTINGPROFILE_T2080742987_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SPRITEDATAOBJECT_T308163541_H
#define SPRITEDATAOBJECT_T308163541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject
struct  SpriteDataObject_t308163541  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData> TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject::frames
	List_1_t225505033 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t308163541, ___frames_0)); }
	inline List_1_t225505033 * get_frames_0() const { return ___frames_0; }
	inline List_1_t225505033 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t225505033 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T308163541_H
#ifndef RESOURCE_T4129330135_H
#define RESOURCE_T4129330135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Resource
struct  Resource_t4129330135  : public RuntimeObject
{
public:

public:
};

struct Resource_t4129330135_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>> Mapbox.Platform.Resource::<>f__am$cache0
	Func_2_t3661664344 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>> Mapbox.Platform.Resource::<>f__am$cache1
	Func_2_t788283625 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>,System.String> Mapbox.Platform.Resource::<>f__am$cache2
	Func_2_t4279064255 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>,System.String> Mapbox.Platform.Resource::<>f__am$cache3
	Func_2_t4279064255 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t3661664344 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t3661664344 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t3661664344 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t788283625 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t788283625 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t788283625 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t4279064255 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t4279064255 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t4279064255 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t4279064255 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t4279064255 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t4279064255 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCE_T4129330135_H
#ifndef IMAGEGESTUREIMAGE_T2885596271_H
#define IMAGEGESTUREIMAGE_T2885596271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureImage
struct  ImageGestureImage_t2885596271  : public RuntimeObject
{
public:
	// System.Int32 DigitalRubyShared.ImageGestureImage::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_8;
	// System.Int32 DigitalRubyShared.ImageGestureImage::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_9;
	// System.Int32 DigitalRubyShared.ImageGestureImage::<Size>k__BackingField
	int32_t ___U3CSizeU3Ek__BackingField_10;
	// System.UInt64[] DigitalRubyShared.ImageGestureImage::<Rows>k__BackingField
	UInt64U5BU5D_t1659327989* ___U3CRowsU3Ek__BackingField_11;
	// System.Byte[] DigitalRubyShared.ImageGestureImage::<Pixels>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CPixelsU3Ek__BackingField_12;
	// System.Single DigitalRubyShared.ImageGestureImage::<SimilarityPadding>k__BackingField
	float ___U3CSimilarityPaddingU3Ek__BackingField_13;
	// System.Single DigitalRubyShared.ImageGestureImage::<Score>k__BackingField
	float ___U3CScoreU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CWidthU3Ek__BackingField_8)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_8() const { return ___U3CWidthU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_8() { return &___U3CWidthU3Ek__BackingField_8; }
	inline void set_U3CWidthU3Ek__BackingField_8(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CHeightU3Ek__BackingField_9)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_9() const { return ___U3CHeightU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_9() { return &___U3CHeightU3Ek__BackingField_9; }
	inline void set_U3CHeightU3Ek__BackingField_9(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CSizeU3Ek__BackingField_10)); }
	inline int32_t get_U3CSizeU3Ek__BackingField_10() const { return ___U3CSizeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CSizeU3Ek__BackingField_10() { return &___U3CSizeU3Ek__BackingField_10; }
	inline void set_U3CSizeU3Ek__BackingField_10(int32_t value)
	{
		___U3CSizeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CRowsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CRowsU3Ek__BackingField_11)); }
	inline UInt64U5BU5D_t1659327989* get_U3CRowsU3Ek__BackingField_11() const { return ___U3CRowsU3Ek__BackingField_11; }
	inline UInt64U5BU5D_t1659327989** get_address_of_U3CRowsU3Ek__BackingField_11() { return &___U3CRowsU3Ek__BackingField_11; }
	inline void set_U3CRowsU3Ek__BackingField_11(UInt64U5BU5D_t1659327989* value)
	{
		___U3CRowsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowsU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CPixelsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CPixelsU3Ek__BackingField_12)); }
	inline ByteU5BU5D_t4116647657* get_U3CPixelsU3Ek__BackingField_12() const { return ___U3CPixelsU3Ek__BackingField_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CPixelsU3Ek__BackingField_12() { return &___U3CPixelsU3Ek__BackingField_12; }
	inline void set_U3CPixelsU3Ek__BackingField_12(ByteU5BU5D_t4116647657* value)
	{
		___U3CPixelsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPixelsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CSimilarityPaddingU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CSimilarityPaddingU3Ek__BackingField_13)); }
	inline float get_U3CSimilarityPaddingU3Ek__BackingField_13() const { return ___U3CSimilarityPaddingU3Ek__BackingField_13; }
	inline float* get_address_of_U3CSimilarityPaddingU3Ek__BackingField_13() { return &___U3CSimilarityPaddingU3Ek__BackingField_13; }
	inline void set_U3CSimilarityPaddingU3Ek__BackingField_13(float value)
	{
		___U3CSimilarityPaddingU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CScoreU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CScoreU3Ek__BackingField_14)); }
	inline float get_U3CScoreU3Ek__BackingField_14() const { return ___U3CScoreU3Ek__BackingField_14; }
	inline float* get_address_of_U3CScoreU3Ek__BackingField_14() { return &___U3CScoreU3Ek__BackingField_14; }
	inline void set_U3CScoreU3Ek__BackingField_14(float value)
	{
		___U3CScoreU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEGESTUREIMAGE_T2885596271_H
#ifndef TEXTUREPACKER_T3148178657_H
#define TEXTUREPACKER_T3148178657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t3148178657  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T3148178657_H
#ifndef SHADERUTILITIES_T714255158_H
#define SHADERUTILITIES_T714255158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t714255158  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t714255158_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_16;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_18;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_19;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_21;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_22;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_26;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_27;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_30;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_31;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_32;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_34;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_36;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_37;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_46;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_47;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_48;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_49;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_50;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_53;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_54;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_56;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_57;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_58;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GradientScale_16)); }
	inline int32_t get_ID_GradientScale_16() const { return ___ID_GradientScale_16; }
	inline int32_t* get_address_of_ID_GradientScale_16() { return &___ID_GradientScale_16; }
	inline void set_ID_GradientScale_16(int32_t value)
	{
		___ID_GradientScale_16 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleX_17)); }
	inline int32_t get_ID_ScaleX_17() const { return ___ID_ScaleX_17; }
	inline int32_t* get_address_of_ID_ScaleX_17() { return &___ID_ScaleX_17; }
	inline void set_ID_ScaleX_17(int32_t value)
	{
		___ID_ScaleX_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleY_18)); }
	inline int32_t get_ID_ScaleY_18() const { return ___ID_ScaleY_18; }
	inline int32_t* get_address_of_ID_ScaleY_18() { return &___ID_ScaleY_18; }
	inline void set_ID_ScaleY_18(int32_t value)
	{
		___ID_ScaleY_18 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_PerspectiveFilter_19)); }
	inline int32_t get_ID_PerspectiveFilter_19() const { return ___ID_PerspectiveFilter_19; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_19() { return &___ID_PerspectiveFilter_19; }
	inline void set_ID_PerspectiveFilter_19(int32_t value)
	{
		___ID_PerspectiveFilter_19 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureWidth_20)); }
	inline int32_t get_ID_TextureWidth_20() const { return ___ID_TextureWidth_20; }
	inline int32_t* get_address_of_ID_TextureWidth_20() { return &___ID_TextureWidth_20; }
	inline void set_ID_TextureWidth_20(int32_t value)
	{
		___ID_TextureWidth_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureHeight_21)); }
	inline int32_t get_ID_TextureHeight_21() const { return ___ID_TextureHeight_21; }
	inline int32_t* get_address_of_ID_TextureHeight_21() { return &___ID_TextureHeight_21; }
	inline void set_ID_TextureHeight_21(int32_t value)
	{
		___ID_TextureHeight_21 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_BevelAmount_22)); }
	inline int32_t get_ID_BevelAmount_22() const { return ___ID_BevelAmount_22; }
	inline int32_t* get_address_of_ID_BevelAmount_22() { return &___ID_BevelAmount_22; }
	inline void set_ID_BevelAmount_22(int32_t value)
	{
		___ID_BevelAmount_22 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowColor_23)); }
	inline int32_t get_ID_GlowColor_23() const { return ___ID_GlowColor_23; }
	inline int32_t* get_address_of_ID_GlowColor_23() { return &___ID_GlowColor_23; }
	inline void set_ID_GlowColor_23(int32_t value)
	{
		___ID_GlowColor_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOffset_24)); }
	inline int32_t get_ID_GlowOffset_24() const { return ___ID_GlowOffset_24; }
	inline int32_t* get_address_of_ID_GlowOffset_24() { return &___ID_GlowOffset_24; }
	inline void set_ID_GlowOffset_24(int32_t value)
	{
		___ID_GlowOffset_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowPower_25)); }
	inline int32_t get_ID_GlowPower_25() const { return ___ID_GlowPower_25; }
	inline int32_t* get_address_of_ID_GlowPower_25() { return &___ID_GlowPower_25; }
	inline void set_ID_GlowPower_25(int32_t value)
	{
		___ID_GlowPower_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOuter_26)); }
	inline int32_t get_ID_GlowOuter_26() const { return ___ID_GlowOuter_26; }
	inline int32_t* get_address_of_ID_GlowOuter_26() { return &___ID_GlowOuter_26; }
	inline void set_ID_GlowOuter_26(int32_t value)
	{
		___ID_GlowOuter_26 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_LightAngle_27)); }
	inline int32_t get_ID_LightAngle_27() const { return ___ID_LightAngle_27; }
	inline int32_t* get_address_of_ID_LightAngle_27() { return &___ID_LightAngle_27; }
	inline void set_ID_LightAngle_27(int32_t value)
	{
		___ID_LightAngle_27 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMap_28)); }
	inline int32_t get_ID_EnvMap_28() const { return ___ID_EnvMap_28; }
	inline int32_t* get_address_of_ID_EnvMap_28() { return &___ID_EnvMap_28; }
	inline void set_ID_EnvMap_28(int32_t value)
	{
		___ID_EnvMap_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrix_29)); }
	inline int32_t get_ID_EnvMatrix_29() const { return ___ID_EnvMatrix_29; }
	inline int32_t* get_address_of_ID_EnvMatrix_29() { return &___ID_EnvMatrix_29; }
	inline void set_ID_EnvMatrix_29(int32_t value)
	{
		___ID_EnvMatrix_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrixRotation_30)); }
	inline int32_t get_ID_EnvMatrixRotation_30() const { return ___ID_EnvMatrixRotation_30; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_30() { return &___ID_EnvMatrixRotation_30; }
	inline void set_ID_EnvMatrixRotation_30(int32_t value)
	{
		___ID_EnvMatrixRotation_30 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskCoord_31)); }
	inline int32_t get_ID_MaskCoord_31() const { return ___ID_MaskCoord_31; }
	inline int32_t* get_address_of_ID_MaskCoord_31() { return &___ID_MaskCoord_31; }
	inline void set_ID_MaskCoord_31(int32_t value)
	{
		___ID_MaskCoord_31 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ClipRect_32)); }
	inline int32_t get_ID_ClipRect_32() const { return ___ID_ClipRect_32; }
	inline int32_t* get_address_of_ID_ClipRect_32() { return &___ID_ClipRect_32; }
	inline void set_ID_ClipRect_32(int32_t value)
	{
		___ID_ClipRect_32 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessX_33)); }
	inline int32_t get_ID_MaskSoftnessX_33() const { return ___ID_MaskSoftnessX_33; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_33() { return &___ID_MaskSoftnessX_33; }
	inline void set_ID_MaskSoftnessX_33(int32_t value)
	{
		___ID_MaskSoftnessX_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessY_34)); }
	inline int32_t get_ID_MaskSoftnessY_34() const { return ___ID_MaskSoftnessY_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_34() { return &___ID_MaskSoftnessY_34; }
	inline void set_ID_MaskSoftnessY_34(int32_t value)
	{
		___ID_MaskSoftnessY_34 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetX_35)); }
	inline int32_t get_ID_VertexOffsetX_35() const { return ___ID_VertexOffsetX_35; }
	inline int32_t* get_address_of_ID_VertexOffsetX_35() { return &___ID_VertexOffsetX_35; }
	inline void set_ID_VertexOffsetX_35(int32_t value)
	{
		___ID_VertexOffsetX_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetY_36)); }
	inline int32_t get_ID_VertexOffsetY_36() const { return ___ID_VertexOffsetY_36; }
	inline int32_t* get_address_of_ID_VertexOffsetY_36() { return &___ID_VertexOffsetY_36; }
	inline void set_ID_VertexOffsetY_36(int32_t value)
	{
		___ID_VertexOffsetY_36 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UseClipRect_37)); }
	inline int32_t get_ID_UseClipRect_37() const { return ___ID_UseClipRect_37; }
	inline int32_t* get_address_of_ID_UseClipRect_37() { return &___ID_UseClipRect_37; }
	inline void set_ID_UseClipRect_37(int32_t value)
	{
		___ID_UseClipRect_37 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilID_38)); }
	inline int32_t get_ID_StencilID_38() const { return ___ID_StencilID_38; }
	inline int32_t* get_address_of_ID_StencilID_38() { return &___ID_StencilID_38; }
	inline void set_ID_StencilID_38(int32_t value)
	{
		___ID_StencilID_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilOp_39)); }
	inline int32_t get_ID_StencilOp_39() const { return ___ID_StencilOp_39; }
	inline int32_t* get_address_of_ID_StencilOp_39() { return &___ID_StencilOp_39; }
	inline void set_ID_StencilOp_39(int32_t value)
	{
		___ID_StencilOp_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilComp_40)); }
	inline int32_t get_ID_StencilComp_40() const { return ___ID_StencilComp_40; }
	inline int32_t* get_address_of_ID_StencilComp_40() { return &___ID_StencilComp_40; }
	inline void set_ID_StencilComp_40(int32_t value)
	{
		___ID_StencilComp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilReadMask_41)); }
	inline int32_t get_ID_StencilReadMask_41() const { return ___ID_StencilReadMask_41; }
	inline int32_t* get_address_of_ID_StencilReadMask_41() { return &___ID_StencilReadMask_41; }
	inline void set_ID_StencilReadMask_41(int32_t value)
	{
		___ID_StencilReadMask_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilWriteMask_42)); }
	inline int32_t get_ID_StencilWriteMask_42() const { return ___ID_StencilWriteMask_42; }
	inline int32_t* get_address_of_ID_StencilWriteMask_42() { return &___ID_StencilWriteMask_42; }
	inline void set_ID_StencilWriteMask_42(int32_t value)
	{
		___ID_StencilWriteMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ShaderFlags_43)); }
	inline int32_t get_ID_ShaderFlags_43() const { return ___ID_ShaderFlags_43; }
	inline int32_t* get_address_of_ID_ShaderFlags_43() { return &___ID_ShaderFlags_43; }
	inline void set_ID_ShaderFlags_43(int32_t value)
	{
		___ID_ShaderFlags_43 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_A_44)); }
	inline int32_t get_ID_ScaleRatio_A_44() const { return ___ID_ScaleRatio_A_44; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_44() { return &___ID_ScaleRatio_A_44; }
	inline void set_ID_ScaleRatio_A_44(int32_t value)
	{
		___ID_ScaleRatio_A_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_B_45)); }
	inline int32_t get_ID_ScaleRatio_B_45() const { return ___ID_ScaleRatio_B_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_45() { return &___ID_ScaleRatio_B_45; }
	inline void set_ID_ScaleRatio_B_45(int32_t value)
	{
		___ID_ScaleRatio_B_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_C_46)); }
	inline int32_t get_ID_ScaleRatio_C_46() const { return ___ID_ScaleRatio_C_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_46() { return &___ID_ScaleRatio_C_46; }
	inline void set_ID_ScaleRatio_C_46(int32_t value)
	{
		___ID_ScaleRatio_C_46 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Bevel_47)); }
	inline String_t* get_Keyword_Bevel_47() const { return ___Keyword_Bevel_47; }
	inline String_t** get_address_of_Keyword_Bevel_47() { return &___Keyword_Bevel_47; }
	inline void set_Keyword_Bevel_47(String_t* value)
	{
		___Keyword_Bevel_47 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_47), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Glow_48)); }
	inline String_t* get_Keyword_Glow_48() const { return ___Keyword_Glow_48; }
	inline String_t** get_address_of_Keyword_Glow_48() { return &___Keyword_Glow_48; }
	inline void set_Keyword_Glow_48(String_t* value)
	{
		___Keyword_Glow_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Underlay_49)); }
	inline String_t* get_Keyword_Underlay_49() const { return ___Keyword_Underlay_49; }
	inline String_t** get_address_of_Keyword_Underlay_49() { return &___Keyword_Underlay_49; }
	inline void set_Keyword_Underlay_49(String_t* value)
	{
		___Keyword_Underlay_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Ratios_50)); }
	inline String_t* get_Keyword_Ratios_50() const { return ___Keyword_Ratios_50; }
	inline String_t** get_address_of_Keyword_Ratios_50() { return &___Keyword_Ratios_50; }
	inline void set_Keyword_Ratios_50(String_t* value)
	{
		___Keyword_Ratios_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_50), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_SOFT_51)); }
	inline String_t* get_Keyword_MASK_SOFT_51() const { return ___Keyword_MASK_SOFT_51; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_51() { return &___Keyword_MASK_SOFT_51; }
	inline void set_Keyword_MASK_SOFT_51(String_t* value)
	{
		___Keyword_MASK_SOFT_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_HARD_52)); }
	inline String_t* get_Keyword_MASK_HARD_52() const { return ___Keyword_MASK_HARD_52; }
	inline String_t** get_address_of_Keyword_MASK_HARD_52() { return &___Keyword_MASK_HARD_52; }
	inline void set_Keyword_MASK_HARD_52(String_t* value)
	{
		___Keyword_MASK_HARD_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_TEX_53)); }
	inline String_t* get_Keyword_MASK_TEX_53() const { return ___Keyword_MASK_TEX_53; }
	inline String_t** get_address_of_Keyword_MASK_TEX_53() { return &___Keyword_MASK_TEX_53; }
	inline void set_Keyword_MASK_TEX_53(String_t* value)
	{
		___Keyword_MASK_TEX_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_53), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Outline_54)); }
	inline String_t* get_Keyword_Outline_54() const { return ___Keyword_Outline_54; }
	inline String_t** get_address_of_Keyword_Outline_54() { return &___Keyword_Outline_54; }
	inline void set_Keyword_Outline_54(String_t* value)
	{
		___Keyword_Outline_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_54), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_ZTestMode_55)); }
	inline String_t* get_ShaderTag_ZTestMode_55() const { return ___ShaderTag_ZTestMode_55; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_55() { return &___ShaderTag_ZTestMode_55; }
	inline void set_ShaderTag_ZTestMode_55(String_t* value)
	{
		___ShaderTag_ZTestMode_55 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_CullMode_56)); }
	inline String_t* get_ShaderTag_CullMode_56() const { return ___ShaderTag_CullMode_56; }
	inline String_t** get_address_of_ShaderTag_CullMode_56() { return &___ShaderTag_CullMode_56; }
	inline void set_ShaderTag_CullMode_56(String_t* value)
	{
		___ShaderTag_CullMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_56), value);
	}

	inline static int32_t get_offset_of_m_clamp_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___m_clamp_57)); }
	inline float get_m_clamp_57() const { return ___m_clamp_57; }
	inline float* get_address_of_m_clamp_57() { return &___m_clamp_57; }
	inline void set_m_clamp_57(float value)
	{
		___m_clamp_57 = value;
	}

	inline static int32_t get_offset_of_isInitialized_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___isInitialized_58)); }
	inline bool get_isInitialized_58() const { return ___isInitialized_58; }
	inline bool* get_address_of_isInitialized_58() { return &___isInitialized_58; }
	inline void set_isInitialized_58(bool value)
	{
		___isInitialized_58 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T714255158_H
#ifndef TMP_TEXTELEMENT_T129727469_H
#define TMP_TEXTELEMENT_T129727469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t129727469  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T129727469_H
#ifndef STEP_T1196010167_H
#define STEP_T1196010167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Step
struct  Step_t1196010167  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Mapbox.Directions.Intersection> Mapbox.Directions.Step::<Intersections>k__BackingField
	List_1_t611829391 * ___U3CIntersectionsU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Mapbox.Utils.Vector2d> Mapbox.Directions.Step::<Geometry>k__BackingField
	List_1_t3337321310 * ___U3CGeometryU3Ek__BackingField_1;
	// Mapbox.Directions.Maneuver Mapbox.Directions.Step::<Maneuver>k__BackingField
	Maneuver_t1966826127 * ___U3CManeuverU3Ek__BackingField_2;
	// System.Double Mapbox.Directions.Step::<Duration>k__BackingField
	double ___U3CDurationU3Ek__BackingField_3;
	// System.Double Mapbox.Directions.Step::<Distance>k__BackingField
	double ___U3CDistanceU3Ek__BackingField_4;
	// System.String Mapbox.Directions.Step::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_5;
	// System.String Mapbox.Directions.Step::<Mode>k__BackingField
	String_t* ___U3CModeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CIntersectionsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Step_t1196010167, ___U3CIntersectionsU3Ek__BackingField_0)); }
	inline List_1_t611829391 * get_U3CIntersectionsU3Ek__BackingField_0() const { return ___U3CIntersectionsU3Ek__BackingField_0; }
	inline List_1_t611829391 ** get_address_of_U3CIntersectionsU3Ek__BackingField_0() { return &___U3CIntersectionsU3Ek__BackingField_0; }
	inline void set_U3CIntersectionsU3Ek__BackingField_0(List_1_t611829391 * value)
	{
		___U3CIntersectionsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIntersectionsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGeometryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Step_t1196010167, ___U3CGeometryU3Ek__BackingField_1)); }
	inline List_1_t3337321310 * get_U3CGeometryU3Ek__BackingField_1() const { return ___U3CGeometryU3Ek__BackingField_1; }
	inline List_1_t3337321310 ** get_address_of_U3CGeometryU3Ek__BackingField_1() { return &___U3CGeometryU3Ek__BackingField_1; }
	inline void set_U3CGeometryU3Ek__BackingField_1(List_1_t3337321310 * value)
	{
		___U3CGeometryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGeometryU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CManeuverU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Step_t1196010167, ___U3CManeuverU3Ek__BackingField_2)); }
	inline Maneuver_t1966826127 * get_U3CManeuverU3Ek__BackingField_2() const { return ___U3CManeuverU3Ek__BackingField_2; }
	inline Maneuver_t1966826127 ** get_address_of_U3CManeuverU3Ek__BackingField_2() { return &___U3CManeuverU3Ek__BackingField_2; }
	inline void set_U3CManeuverU3Ek__BackingField_2(Maneuver_t1966826127 * value)
	{
		___U3CManeuverU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManeuverU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Step_t1196010167, ___U3CDurationU3Ek__BackingField_3)); }
	inline double get_U3CDurationU3Ek__BackingField_3() const { return ___U3CDurationU3Ek__BackingField_3; }
	inline double* get_address_of_U3CDurationU3Ek__BackingField_3() { return &___U3CDurationU3Ek__BackingField_3; }
	inline void set_U3CDurationU3Ek__BackingField_3(double value)
	{
		___U3CDurationU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Step_t1196010167, ___U3CDistanceU3Ek__BackingField_4)); }
	inline double get_U3CDistanceU3Ek__BackingField_4() const { return ___U3CDistanceU3Ek__BackingField_4; }
	inline double* get_address_of_U3CDistanceU3Ek__BackingField_4() { return &___U3CDistanceU3Ek__BackingField_4; }
	inline void set_U3CDistanceU3Ek__BackingField_4(double value)
	{
		___U3CDistanceU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Step_t1196010167, ___U3CNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CNameU3Ek__BackingField_5() const { return ___U3CNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_5() { return &___U3CNameU3Ek__BackingField_5; }
	inline void set_U3CNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CModeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Step_t1196010167, ___U3CModeU3Ek__BackingField_6)); }
	inline String_t* get_U3CModeU3Ek__BackingField_6() const { return ___U3CModeU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CModeU3Ek__BackingField_6() { return &___U3CModeU3Ek__BackingField_6; }
	inline void set_U3CModeU3Ek__BackingField_6(String_t* value)
	{
		___U3CModeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModeU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEP_T1196010167_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T2918531336_H
#define U3CU3EC__DISPLAYCLASS3_0_T2918531336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t2918531336  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass3_0::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass3_0::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2918531336, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2918531336, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T2918531336_H
#ifndef KERNINGTABLE_T2322366871_H
#define KERNINGTABLE_T2322366871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_t2322366871  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_t3742930331 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871, ___kerningPairs_0)); }
	inline List_1_t3742930331 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_t3742930331 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_t3742930331 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_T2322366871_H
#ifndef KERNINGPAIR_T2270855589_H
#define KERNINGPAIR_T2270855589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t2270855589  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningPair::AscII_Left
	int32_t ___AscII_Left_0;
	// System.Int32 TMPro.KerningPair::AscII_Right
	int32_t ___AscII_Right_1;
	// System.Single TMPro.KerningPair::XadvanceOffset
	float ___XadvanceOffset_2;

public:
	inline static int32_t get_offset_of_AscII_Left_0() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___AscII_Left_0)); }
	inline int32_t get_AscII_Left_0() const { return ___AscII_Left_0; }
	inline int32_t* get_address_of_AscII_Left_0() { return &___AscII_Left_0; }
	inline void set_AscII_Left_0(int32_t value)
	{
		___AscII_Left_0 = value;
	}

	inline static int32_t get_offset_of_AscII_Right_1() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___AscII_Right_1)); }
	inline int32_t get_AscII_Right_1() const { return ___AscII_Right_1; }
	inline int32_t* get_address_of_AscII_Right_1() { return &___AscII_Right_1; }
	inline void set_AscII_Right_1(int32_t value)
	{
		___AscII_Right_1 = value;
	}

	inline static int32_t get_offset_of_XadvanceOffset_2() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___XadvanceOffset_2)); }
	inline float get_XadvanceOffset_2() const { return ___XadvanceOffset_2; }
	inline float* get_address_of_XadvanceOffset_2() { return &___XadvanceOffset_2; }
	inline void set_XadvanceOffset_2(float value)
	{
		___XadvanceOffset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T2270855589_H
#ifndef OVERVIEW_T3152666467_H
#define OVERVIEW_T3152666467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Overview
struct  Overview_t3152666467  : public RuntimeObject
{
public:
	// System.String Mapbox.Directions.Overview::overview
	String_t* ___overview_3;

public:
	inline static int32_t get_offset_of_overview_3() { return static_cast<int32_t>(offsetof(Overview_t3152666467, ___overview_3)); }
	inline String_t* get_overview_3() const { return ___overview_3; }
	inline String_t** get_address_of_overview_3() { return &___overview_3; }
	inline void set_overview_3(String_t* value)
	{
		___overview_3 = value;
		Il2CppCodeGenWriteBarrier((&___overview_3), value);
	}
};

struct Overview_t3152666467_StaticFields
{
public:
	// Mapbox.Directions.Overview Mapbox.Directions.Overview::Full
	Overview_t3152666467 * ___Full_0;
	// Mapbox.Directions.Overview Mapbox.Directions.Overview::Simplified
	Overview_t3152666467 * ___Simplified_1;
	// Mapbox.Directions.Overview Mapbox.Directions.Overview::False
	Overview_t3152666467 * ___False_2;

public:
	inline static int32_t get_offset_of_Full_0() { return static_cast<int32_t>(offsetof(Overview_t3152666467_StaticFields, ___Full_0)); }
	inline Overview_t3152666467 * get_Full_0() const { return ___Full_0; }
	inline Overview_t3152666467 ** get_address_of_Full_0() { return &___Full_0; }
	inline void set_Full_0(Overview_t3152666467 * value)
	{
		___Full_0 = value;
		Il2CppCodeGenWriteBarrier((&___Full_0), value);
	}

	inline static int32_t get_offset_of_Simplified_1() { return static_cast<int32_t>(offsetof(Overview_t3152666467_StaticFields, ___Simplified_1)); }
	inline Overview_t3152666467 * get_Simplified_1() const { return ___Simplified_1; }
	inline Overview_t3152666467 ** get_address_of_Simplified_1() { return &___Simplified_1; }
	inline void set_Simplified_1(Overview_t3152666467 * value)
	{
		___Simplified_1 = value;
		Il2CppCodeGenWriteBarrier((&___Simplified_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(Overview_t3152666467_StaticFields, ___False_2)); }
	inline Overview_t3152666467 * get_False_2() const { return ___False_2; }
	inline Overview_t3152666467 ** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(Overview_t3152666467 * value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERVIEW_T3152666467_H
#ifndef LEG_T3885960664_H
#define LEG_T3885960664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Leg
struct  Leg_t3885960664  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Mapbox.Directions.Step> Mapbox.Directions.Leg::<Steps>k__BackingField
	List_1_t2668084909 * ___U3CStepsU3Ek__BackingField_0;
	// System.String Mapbox.Directions.Leg::<Summary>k__BackingField
	String_t* ___U3CSummaryU3Ek__BackingField_1;
	// System.Double Mapbox.Directions.Leg::<Duration>k__BackingField
	double ___U3CDurationU3Ek__BackingField_2;
	// System.Double Mapbox.Directions.Leg::<Distance>k__BackingField
	double ___U3CDistanceU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CStepsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Leg_t3885960664, ___U3CStepsU3Ek__BackingField_0)); }
	inline List_1_t2668084909 * get_U3CStepsU3Ek__BackingField_0() const { return ___U3CStepsU3Ek__BackingField_0; }
	inline List_1_t2668084909 ** get_address_of_U3CStepsU3Ek__BackingField_0() { return &___U3CStepsU3Ek__BackingField_0; }
	inline void set_U3CStepsU3Ek__BackingField_0(List_1_t2668084909 * value)
	{
		___U3CStepsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStepsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSummaryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Leg_t3885960664, ___U3CSummaryU3Ek__BackingField_1)); }
	inline String_t* get_U3CSummaryU3Ek__BackingField_1() const { return ___U3CSummaryU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSummaryU3Ek__BackingField_1() { return &___U3CSummaryU3Ek__BackingField_1; }
	inline void set_U3CSummaryU3Ek__BackingField_1(String_t* value)
	{
		___U3CSummaryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSummaryU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Leg_t3885960664, ___U3CDurationU3Ek__BackingField_2)); }
	inline double get_U3CDurationU3Ek__BackingField_2() const { return ___U3CDurationU3Ek__BackingField_2; }
	inline double* get_address_of_U3CDurationU3Ek__BackingField_2() { return &___U3CDurationU3Ek__BackingField_2; }
	inline void set_U3CDurationU3Ek__BackingField_2(double value)
	{
		___U3CDurationU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Leg_t3885960664, ___U3CDistanceU3Ek__BackingField_3)); }
	inline double get_U3CDistanceU3Ek__BackingField_3() const { return ___U3CDistanceU3Ek__BackingField_3; }
	inline double* get_address_of_U3CDistanceU3Ek__BackingField_3() { return &___U3CDistanceU3Ek__BackingField_3; }
	inline void set_U3CDistanceU3Ek__BackingField_3(double value)
	{
		___U3CDistanceU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEG_T3885960664_H
#ifndef GESTUREVELOCITYTRACKER_T806949657_H
#define GESTUREVELOCITYTRACKER_T806949657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureVelocityTracker
struct  GestureVelocityTracker_t806949657  : public RuntimeObject
{
public:
	// System.Collections.Generic.Queue`1<DigitalRubyShared.GestureVelocityTracker/VelocityHistory> DigitalRubyShared.GestureVelocityTracker::history
	Queue_1_t1111824545 * ___history_1;
	// System.Diagnostics.Stopwatch DigitalRubyShared.GestureVelocityTracker::timer
	Stopwatch_t305734070 * ___timer_2;
	// System.Single DigitalRubyShared.GestureVelocityTracker::previousX
	float ___previousX_3;
	// System.Single DigitalRubyShared.GestureVelocityTracker::previousY
	float ___previousY_4;
	// System.Single DigitalRubyShared.GestureVelocityTracker::<VelocityX>k__BackingField
	float ___U3CVelocityXU3Ek__BackingField_5;
	// System.Single DigitalRubyShared.GestureVelocityTracker::<VelocityY>k__BackingField
	float ___U3CVelocityYU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_history_1() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___history_1)); }
	inline Queue_1_t1111824545 * get_history_1() const { return ___history_1; }
	inline Queue_1_t1111824545 ** get_address_of_history_1() { return &___history_1; }
	inline void set_history_1(Queue_1_t1111824545 * value)
	{
		___history_1 = value;
		Il2CppCodeGenWriteBarrier((&___history_1), value);
	}

	inline static int32_t get_offset_of_timer_2() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___timer_2)); }
	inline Stopwatch_t305734070 * get_timer_2() const { return ___timer_2; }
	inline Stopwatch_t305734070 ** get_address_of_timer_2() { return &___timer_2; }
	inline void set_timer_2(Stopwatch_t305734070 * value)
	{
		___timer_2 = value;
		Il2CppCodeGenWriteBarrier((&___timer_2), value);
	}

	inline static int32_t get_offset_of_previousX_3() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___previousX_3)); }
	inline float get_previousX_3() const { return ___previousX_3; }
	inline float* get_address_of_previousX_3() { return &___previousX_3; }
	inline void set_previousX_3(float value)
	{
		___previousX_3 = value;
	}

	inline static int32_t get_offset_of_previousY_4() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___previousY_4)); }
	inline float get_previousY_4() const { return ___previousY_4; }
	inline float* get_address_of_previousY_4() { return &___previousY_4; }
	inline void set_previousY_4(float value)
	{
		___previousY_4 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityXU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___U3CVelocityXU3Ek__BackingField_5)); }
	inline float get_U3CVelocityXU3Ek__BackingField_5() const { return ___U3CVelocityXU3Ek__BackingField_5; }
	inline float* get_address_of_U3CVelocityXU3Ek__BackingField_5() { return &___U3CVelocityXU3Ek__BackingField_5; }
	inline void set_U3CVelocityXU3Ek__BackingField_5(float value)
	{
		___U3CVelocityXU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityYU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___U3CVelocityYU3Ek__BackingField_6)); }
	inline float get_U3CVelocityYU3Ek__BackingField_6() const { return ___U3CVelocityYU3Ek__BackingField_6; }
	inline float* get_address_of_U3CVelocityYU3Ek__BackingField_6() { return &___U3CVelocityYU3Ek__BackingField_6; }
	inline void set_U3CVelocityYU3Ek__BackingField_6(float value)
	{
		___U3CVelocityYU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREVELOCITYTRACKER_T806949657_H
#ifndef DIRECTIONSRESPONSE_T4007621150_H
#define DIRECTIONSRESPONSE_T4007621150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.DirectionsResponse
struct  DirectionsResponse_t4007621150  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Mapbox.Directions.Route> Mapbox.Directions.DirectionsResponse::<Routes>k__BackingField
	List_1_t3433222835 * ___U3CRoutesU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Mapbox.Directions.Waypoint> Mapbox.Directions.DirectionsResponse::<Waypoints>k__BackingField
	List_1_t815574591 * ___U3CWaypointsU3Ek__BackingField_1;
	// System.String Mapbox.Directions.DirectionsResponse::<Code>k__BackingField
	String_t* ___U3CCodeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRoutesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DirectionsResponse_t4007621150, ___U3CRoutesU3Ek__BackingField_0)); }
	inline List_1_t3433222835 * get_U3CRoutesU3Ek__BackingField_0() const { return ___U3CRoutesU3Ek__BackingField_0; }
	inline List_1_t3433222835 ** get_address_of_U3CRoutesU3Ek__BackingField_0() { return &___U3CRoutesU3Ek__BackingField_0; }
	inline void set_U3CRoutesU3Ek__BackingField_0(List_1_t3433222835 * value)
	{
		___U3CRoutesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRoutesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWaypointsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DirectionsResponse_t4007621150, ___U3CWaypointsU3Ek__BackingField_1)); }
	inline List_1_t815574591 * get_U3CWaypointsU3Ek__BackingField_1() const { return ___U3CWaypointsU3Ek__BackingField_1; }
	inline List_1_t815574591 ** get_address_of_U3CWaypointsU3Ek__BackingField_1() { return &___U3CWaypointsU3Ek__BackingField_1; }
	inline void set_U3CWaypointsU3Ek__BackingField_1(List_1_t815574591 * value)
	{
		___U3CWaypointsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWaypointsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCodeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DirectionsResponse_t4007621150, ___U3CCodeU3Ek__BackingField_2)); }
	inline String_t* get_U3CCodeU3Ek__BackingField_2() const { return ___U3CCodeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCodeU3Ek__BackingField_2() { return &___U3CCodeU3Ek__BackingField_2; }
	inline void set_U3CCodeU3Ek__BackingField_2(String_t* value)
	{
		___U3CCodeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCodeU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONSRESPONSE_T4007621150_H
#ifndef DIRECTIONS_T1397515081_H
#define DIRECTIONS_T1397515081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Directions
struct  Directions_t1397515081  : public RuntimeObject
{
public:
	// Mapbox.Platform.IFileSource Mapbox.Directions.Directions::fileSource
	RuntimeObject* ___fileSource_0;

public:
	inline static int32_t get_offset_of_fileSource_0() { return static_cast<int32_t>(offsetof(Directions_t1397515081, ___fileSource_0)); }
	inline RuntimeObject* get_fileSource_0() const { return ___fileSource_0; }
	inline RuntimeObject** get_address_of_fileSource_0() { return &___fileSource_0; }
	inline void set_fileSource_0(RuntimeObject* value)
	{
		___fileSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___fileSource_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONS_T1397515081_H
#ifndef U3CMAINTHREADCALLBACKU3EC__ITERATOR0_T2673350864_H
#define U3CMAINTHREADCALLBACKU3EC__ITERATOR0_T2673350864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0
struct  U3CMainThreadCallbackU3Ec__Iterator0_t2673350864  : public RuntimeObject
{
public:
	// System.Action DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::action
	Action_t1264377477 * ___action_0;
	// System.Diagnostics.Stopwatch DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::<w>__1
	Stopwatch_t305734070 * ___U3CwU3E__1_1;
	// System.Single DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::delay
	float ___delay_2;
	// System.Object DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___action_0)); }
	inline Action_t1264377477 * get_action_0() const { return ___action_0; }
	inline Action_t1264377477 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t1264377477 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__1_1() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U3CwU3E__1_1)); }
	inline Stopwatch_t305734070 * get_U3CwU3E__1_1() const { return ___U3CwU3E__1_1; }
	inline Stopwatch_t305734070 ** get_address_of_U3CwU3E__1_1() { return &___U3CwU3E__1_1; }
	inline void set_U3CwU3E__1_1(Stopwatch_t305734070 * value)
	{
		___U3CwU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__1_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAINTHREADCALLBACKU3EC__ITERATOR0_T2673350864_H
#ifndef DEVICEINFO_T3428345325_H
#define DEVICEINFO_T3428345325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DeviceInfo
struct  DeviceInfo_t3428345325  : public RuntimeObject
{
public:

public:
};

struct DeviceInfo_t3428345325_StaticFields
{
public:
	// System.Int32 DigitalRubyShared.DeviceInfo::<PixelsPerInch>k__BackingField
	int32_t ___U3CPixelsPerInchU3Ek__BackingField_0;
	// System.Int32 DigitalRubyShared.DeviceInfo::<UnitMultiplier>k__BackingField
	int32_t ___U3CUnitMultiplierU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPixelsPerInchU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeviceInfo_t3428345325_StaticFields, ___U3CPixelsPerInchU3Ek__BackingField_0)); }
	inline int32_t get_U3CPixelsPerInchU3Ek__BackingField_0() const { return ___U3CPixelsPerInchU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPixelsPerInchU3Ek__BackingField_0() { return &___U3CPixelsPerInchU3Ek__BackingField_0; }
	inline void set_U3CPixelsPerInchU3Ek__BackingField_0(int32_t value)
	{
		___U3CPixelsPerInchU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CUnitMultiplierU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeviceInfo_t3428345325_StaticFields, ___U3CUnitMultiplierU3Ek__BackingField_1)); }
	inline int32_t get_U3CUnitMultiplierU3Ek__BackingField_1() const { return ___U3CUnitMultiplierU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CUnitMultiplierU3Ek__BackingField_1() { return &___U3CUnitMultiplierU3Ek__BackingField_1; }
	inline void set_U3CUnitMultiplierU3Ek__BackingField_1(int32_t value)
	{
		___U3CUnitMultiplierU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEINFO_T3428345325_H
#ifndef ROUTE_T1961148093_H
#define ROUTE_T1961148093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Route
struct  Route_t1961148093  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Mapbox.Directions.Leg> Mapbox.Directions.Route::<Legs>k__BackingField
	List_1_t1063068110 * ___U3CLegsU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Mapbox.Utils.Vector2d> Mapbox.Directions.Route::<Geometry>k__BackingField
	List_1_t3337321310 * ___U3CGeometryU3Ek__BackingField_1;
	// System.Double Mapbox.Directions.Route::<Duration>k__BackingField
	double ___U3CDurationU3Ek__BackingField_2;
	// System.Double Mapbox.Directions.Route::<Distance>k__BackingField
	double ___U3CDistanceU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CLegsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Route_t1961148093, ___U3CLegsU3Ek__BackingField_0)); }
	inline List_1_t1063068110 * get_U3CLegsU3Ek__BackingField_0() const { return ___U3CLegsU3Ek__BackingField_0; }
	inline List_1_t1063068110 ** get_address_of_U3CLegsU3Ek__BackingField_0() { return &___U3CLegsU3Ek__BackingField_0; }
	inline void set_U3CLegsU3Ek__BackingField_0(List_1_t1063068110 * value)
	{
		___U3CLegsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLegsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGeometryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Route_t1961148093, ___U3CGeometryU3Ek__BackingField_1)); }
	inline List_1_t3337321310 * get_U3CGeometryU3Ek__BackingField_1() const { return ___U3CGeometryU3Ek__BackingField_1; }
	inline List_1_t3337321310 ** get_address_of_U3CGeometryU3Ek__BackingField_1() { return &___U3CGeometryU3Ek__BackingField_1; }
	inline void set_U3CGeometryU3Ek__BackingField_1(List_1_t3337321310 * value)
	{
		___U3CGeometryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGeometryU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Route_t1961148093, ___U3CDurationU3Ek__BackingField_2)); }
	inline double get_U3CDurationU3Ek__BackingField_2() const { return ___U3CDurationU3Ek__BackingField_2; }
	inline double* get_address_of_U3CDurationU3Ek__BackingField_2() { return &___U3CDurationU3Ek__BackingField_2; }
	inline void set_U3CDurationU3Ek__BackingField_2(double value)
	{
		___U3CDurationU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Route_t1961148093, ___U3CDistanceU3Ek__BackingField_3)); }
	inline double get_U3CDistanceU3Ek__BackingField_3() const { return ___U3CDistanceU3Ek__BackingField_3; }
	inline double* get_address_of_U3CDistanceU3Ek__BackingField_3() { return &___U3CDistanceU3Ek__BackingField_3; }
	inline void set_U3CDistanceU3Ek__BackingField_3(double value)
	{
		___U3CDistanceU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTE_T1961148093_H
#ifndef TMP_FONTUTILITIES_T2599150238_H
#define TMP_FONTUTILITIES_T2599150238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_t2599150238  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_t2599150238_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_t128053199 * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_t2599150238_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_t128053199 * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_t128053199 ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_t128053199 * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_T2599150238_H
#ifndef U3CU3EC_T4077839018_H
#define U3CU3EC_T4077839018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<>c
struct  U3CU3Ec_t4077839018  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4077839018_StaticFields
{
public:
	// TMPro.KerningTable/<>c TMPro.KerningTable/<>c::<>9
	U3CU3Ec_t4077839018 * ___U3CU3E9_0;
	// System.Func`2<TMPro.KerningPair,System.Int32> TMPro.KerningTable/<>c::<>9__6_0
	Func_2_t2554309206 * ___U3CU3E9__6_0_1;
	// System.Func`2<TMPro.KerningPair,System.Int32> TMPro.KerningTable/<>c::<>9__6_1
	Func_2_t2554309206 * ___U3CU3E9__6_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4077839018_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4077839018 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4077839018 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4077839018 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4077839018_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t2554309206 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t2554309206 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t2554309206 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4077839018_StaticFields, ___U3CU3E9__6_1_2)); }
	inline Func_2_t2554309206 * get_U3CU3E9__6_1_2() const { return ___U3CU3E9__6_1_2; }
	inline Func_2_t2554309206 ** get_address_of_U3CU3E9__6_1_2() { return &___U3CU3E9__6_1_2; }
	inline void set_U3CU3E9__6_1_2(Func_2_t2554309206 * value)
	{
		___U3CU3E9__6_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4077839018_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T2918531329_H
#define U3CU3EC__DISPLAYCLASS4_0_T2918531329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t2918531329  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass4_0::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<>c__DisplayClass4_0::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t2918531329, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t2918531329, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T2918531329_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef POINT_T2411124144_H
#define POINT_T2411124144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureRecognizer/Point
struct  Point_t2411124144 
{
public:
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer/Point::X
	int32_t ___X_0;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer/Point::Y
	int32_t ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(Point_t2411124144, ___X_0)); }
	inline int32_t get_X_0() const { return ___X_0; }
	inline int32_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int32_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(Point_t2411124144, ___Y_1)); }
	inline int32_t get_Y_1() const { return ___Y_1; }
	inline int32_t* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(int32_t value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T2411124144_H
#ifndef GESTURETOUCH_T1992402133_H
#define GESTURETOUCH_T1992402133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureTouch
struct  GestureTouch_t1992402133 
{
public:
	// System.Int32 DigitalRubyShared.GestureTouch::id
	int32_t ___id_1;
	// System.Single DigitalRubyShared.GestureTouch::x
	float ___x_2;
	// System.Single DigitalRubyShared.GestureTouch::y
	float ___y_3;
	// System.Single DigitalRubyShared.GestureTouch::previousX
	float ___previousX_4;
	// System.Single DigitalRubyShared.GestureTouch::previousY
	float ___previousY_5;
	// System.Single DigitalRubyShared.GestureTouch::pressure
	float ___pressure_6;
	// System.Single DigitalRubyShared.GestureTouch::screenX
	float ___screenX_7;
	// System.Single DigitalRubyShared.GestureTouch::screenY
	float ___screenY_8;
	// System.Object DigitalRubyShared.GestureTouch::platformSpecificTouch
	RuntimeObject * ___platformSpecificTouch_9;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_previousX_4() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___previousX_4)); }
	inline float get_previousX_4() const { return ___previousX_4; }
	inline float* get_address_of_previousX_4() { return &___previousX_4; }
	inline void set_previousX_4(float value)
	{
		___previousX_4 = value;
	}

	inline static int32_t get_offset_of_previousY_5() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___previousY_5)); }
	inline float get_previousY_5() const { return ___previousY_5; }
	inline float* get_address_of_previousY_5() { return &___previousY_5; }
	inline void set_previousY_5(float value)
	{
		___previousY_5 = value;
	}

	inline static int32_t get_offset_of_pressure_6() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___pressure_6)); }
	inline float get_pressure_6() const { return ___pressure_6; }
	inline float* get_address_of_pressure_6() { return &___pressure_6; }
	inline void set_pressure_6(float value)
	{
		___pressure_6 = value;
	}

	inline static int32_t get_offset_of_screenX_7() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___screenX_7)); }
	inline float get_screenX_7() const { return ___screenX_7; }
	inline float* get_address_of_screenX_7() { return &___screenX_7; }
	inline void set_screenX_7(float value)
	{
		___screenX_7 = value;
	}

	inline static int32_t get_offset_of_screenY_8() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___screenY_8)); }
	inline float get_screenY_8() const { return ___screenY_8; }
	inline float* get_address_of_screenY_8() { return &___screenY_8; }
	inline void set_screenY_8(float value)
	{
		___screenY_8 = value;
	}

	inline static int32_t get_offset_of_platformSpecificTouch_9() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___platformSpecificTouch_9)); }
	inline RuntimeObject * get_platformSpecificTouch_9() const { return ___platformSpecificTouch_9; }
	inline RuntimeObject ** get_address_of_platformSpecificTouch_9() { return &___platformSpecificTouch_9; }
	inline void set_platformSpecificTouch_9(RuntimeObject * value)
	{
		___platformSpecificTouch_9 = value;
		Il2CppCodeGenWriteBarrier((&___platformSpecificTouch_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DigitalRubyShared.GestureTouch
struct GestureTouch_t1992402133_marshaled_pinvoke
{
	int32_t ___id_1;
	float ___x_2;
	float ___y_3;
	float ___previousX_4;
	float ___previousY_5;
	float ___pressure_6;
	float ___screenX_7;
	float ___screenY_8;
	Il2CppIUnknown* ___platformSpecificTouch_9;
};
// Native definition for COM marshalling of DigitalRubyShared.GestureTouch
struct GestureTouch_t1992402133_marshaled_com
{
	int32_t ___id_1;
	float ___x_2;
	float ___y_3;
	float ___previousX_4;
	float ___previousY_5;
	float ___pressure_6;
	float ___screenX_7;
	float ___screenY_8;
	Il2CppIUnknown* ___platformSpecificTouch_9;
};
#endif // GESTURETOUCH_T1992402133_H
#ifndef TMP_GLYPH_T581847833_H
#define TMP_GLYPH_T581847833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Glyph
struct  TMP_Glyph_t581847833  : public TMP_TextElement_t129727469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPH_T581847833_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef GEOCODERESOURCE_1_T2673869757_H
#define GEOCODERESOURCE_1_T2673869757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.GeocodeResource`1<System.String>
struct  GeocodeResource_1_t2673869757  : public Resource_t4129330135
{
public:
	// System.String Mapbox.Geocoding.GeocodeResource`1::apiEndpoint
	String_t* ___apiEndpoint_5;
	// System.String Mapbox.Geocoding.GeocodeResource`1::mode
	String_t* ___mode_6;
	// System.String[] Mapbox.Geocoding.GeocodeResource`1::types
	StringU5BU5D_t1281789340* ___types_7;

public:
	inline static int32_t get_offset_of_apiEndpoint_5() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2673869757, ___apiEndpoint_5)); }
	inline String_t* get_apiEndpoint_5() const { return ___apiEndpoint_5; }
	inline String_t** get_address_of_apiEndpoint_5() { return &___apiEndpoint_5; }
	inline void set_apiEndpoint_5(String_t* value)
	{
		___apiEndpoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiEndpoint_5), value);
	}

	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2673869757, ___mode_6)); }
	inline String_t* get_mode_6() const { return ___mode_6; }
	inline String_t** get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(String_t* value)
	{
		___mode_6 = value;
		Il2CppCodeGenWriteBarrier((&___mode_6), value);
	}

	inline static int32_t get_offset_of_types_7() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2673869757, ___types_7)); }
	inline StringU5BU5D_t1281789340* get_types_7() const { return ___types_7; }
	inline StringU5BU5D_t1281789340** get_address_of_types_7() { return &___types_7; }
	inline void set_types_7(StringU5BU5D_t1281789340* value)
	{
		___types_7 = value;
		Il2CppCodeGenWriteBarrier((&___types_7), value);
	}
};

struct GeocodeResource_1_t2673869757_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> Mapbox.Geocoding.GeocodeResource`1::FeatureTypes
	List_1_t3319525431 * ___FeatureTypes_4;

public:
	inline static int32_t get_offset_of_FeatureTypes_4() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2673869757_StaticFields, ___FeatureTypes_4)); }
	inline List_1_t3319525431 * get_FeatureTypes_4() const { return ___FeatureTypes_4; }
	inline List_1_t3319525431 ** get_address_of_FeatureTypes_4() { return &___FeatureTypes_4; }
	inline void set_FeatureTypes_4(List_1_t3319525431 * value)
	{
		___FeatureTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___FeatureTypes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOCODERESOURCE_1_T2673869757_H
#ifndef TMP_SPRITEINFO_T2726321384_H
#define TMP_SPRITEINFO_T2726321384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t2726321384 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T2726321384_H
#ifndef TMP_WORDINFO_T3331066303_H
#define TMP_WORDINFO_T3331066303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t3331066303 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T3331066303_H
#ifndef TMP_LINKINFO_T1092083476_H
#define TMP_LINKINFO_T1092083476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t1092083476 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t3528271667* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkID_6)); }
	inline CharU5BU5D_t3528271667* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t3528271667** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t3528271667* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T1092083476_H
#ifndef TMP_PAGEINFO_T2608430633_H
#define TMP_PAGEINFO_T2608430633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t2608430633 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T2608430633_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef KERNINGPAIRKEY_T536493877_H
#define KERNINGPAIRKEY_T536493877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPairKey
struct  KerningPairKey_t536493877 
{
public:
	// System.Int32 TMPro.KerningPairKey::ascii_Left
	int32_t ___ascii_Left_0;
	// System.Int32 TMPro.KerningPairKey::ascii_Right
	int32_t ___ascii_Right_1;
	// System.Int32 TMPro.KerningPairKey::key
	int32_t ___key_2;

public:
	inline static int32_t get_offset_of_ascii_Left_0() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Left_0)); }
	inline int32_t get_ascii_Left_0() const { return ___ascii_Left_0; }
	inline int32_t* get_address_of_ascii_Left_0() { return &___ascii_Left_0; }
	inline void set_ascii_Left_0(int32_t value)
	{
		___ascii_Left_0 = value;
	}

	inline static int32_t get_offset_of_ascii_Right_1() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Right_1)); }
	inline int32_t get_ascii_Right_1() const { return ___ascii_Right_1; }
	inline int32_t* get_address_of_ascii_Right_1() { return &___ascii_Right_1; }
	inline void set_ascii_Right_1(int32_t value)
	{
		___ascii_Right_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIRKEY_T536493877_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FONTCREATIONSETTING_T628772060_H
#define FONTCREATIONSETTING_T628772060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontCreationSetting
struct  FontCreationSetting_t628772060 
{
public:
	// System.String TMPro.FontCreationSetting::fontSourcePath
	String_t* ___fontSourcePath_0;
	// System.Int32 TMPro.FontCreationSetting::fontSizingMode
	int32_t ___fontSizingMode_1;
	// System.Int32 TMPro.FontCreationSetting::fontSize
	int32_t ___fontSize_2;
	// System.Int32 TMPro.FontCreationSetting::fontPadding
	int32_t ___fontPadding_3;
	// System.Int32 TMPro.FontCreationSetting::fontPackingMode
	int32_t ___fontPackingMode_4;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasWidth
	int32_t ___fontAtlasWidth_5;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasHeight
	int32_t ___fontAtlasHeight_6;
	// System.Int32 TMPro.FontCreationSetting::fontCharacterSet
	int32_t ___fontCharacterSet_7;
	// System.Int32 TMPro.FontCreationSetting::fontStyle
	int32_t ___fontStyle_8;
	// System.Single TMPro.FontCreationSetting::fontStlyeModifier
	float ___fontStlyeModifier_9;
	// System.Int32 TMPro.FontCreationSetting::fontRenderMode
	int32_t ___fontRenderMode_10;
	// System.Boolean TMPro.FontCreationSetting::fontKerning
	bool ___fontKerning_11;

public:
	inline static int32_t get_offset_of_fontSourcePath_0() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSourcePath_0)); }
	inline String_t* get_fontSourcePath_0() const { return ___fontSourcePath_0; }
	inline String_t** get_address_of_fontSourcePath_0() { return &___fontSourcePath_0; }
	inline void set_fontSourcePath_0(String_t* value)
	{
		___fontSourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___fontSourcePath_0), value);
	}

	inline static int32_t get_offset_of_fontSizingMode_1() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSizingMode_1)); }
	inline int32_t get_fontSizingMode_1() const { return ___fontSizingMode_1; }
	inline int32_t* get_address_of_fontSizingMode_1() { return &___fontSizingMode_1; }
	inline void set_fontSizingMode_1(int32_t value)
	{
		___fontSizingMode_1 = value;
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_fontPadding_3() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontPadding_3)); }
	inline int32_t get_fontPadding_3() const { return ___fontPadding_3; }
	inline int32_t* get_address_of_fontPadding_3() { return &___fontPadding_3; }
	inline void set_fontPadding_3(int32_t value)
	{
		___fontPadding_3 = value;
	}

	inline static int32_t get_offset_of_fontPackingMode_4() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontPackingMode_4)); }
	inline int32_t get_fontPackingMode_4() const { return ___fontPackingMode_4; }
	inline int32_t* get_address_of_fontPackingMode_4() { return &___fontPackingMode_4; }
	inline void set_fontPackingMode_4(int32_t value)
	{
		___fontPackingMode_4 = value;
	}

	inline static int32_t get_offset_of_fontAtlasWidth_5() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontAtlasWidth_5)); }
	inline int32_t get_fontAtlasWidth_5() const { return ___fontAtlasWidth_5; }
	inline int32_t* get_address_of_fontAtlasWidth_5() { return &___fontAtlasWidth_5; }
	inline void set_fontAtlasWidth_5(int32_t value)
	{
		___fontAtlasWidth_5 = value;
	}

	inline static int32_t get_offset_of_fontAtlasHeight_6() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontAtlasHeight_6)); }
	inline int32_t get_fontAtlasHeight_6() const { return ___fontAtlasHeight_6; }
	inline int32_t* get_address_of_fontAtlasHeight_6() { return &___fontAtlasHeight_6; }
	inline void set_fontAtlasHeight_6(int32_t value)
	{
		___fontAtlasHeight_6 = value;
	}

	inline static int32_t get_offset_of_fontCharacterSet_7() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontCharacterSet_7)); }
	inline int32_t get_fontCharacterSet_7() const { return ___fontCharacterSet_7; }
	inline int32_t* get_address_of_fontCharacterSet_7() { return &___fontCharacterSet_7; }
	inline void set_fontCharacterSet_7(int32_t value)
	{
		___fontCharacterSet_7 = value;
	}

	inline static int32_t get_offset_of_fontStyle_8() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontStyle_8)); }
	inline int32_t get_fontStyle_8() const { return ___fontStyle_8; }
	inline int32_t* get_address_of_fontStyle_8() { return &___fontStyle_8; }
	inline void set_fontStyle_8(int32_t value)
	{
		___fontStyle_8 = value;
	}

	inline static int32_t get_offset_of_fontStlyeModifier_9() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontStlyeModifier_9)); }
	inline float get_fontStlyeModifier_9() const { return ___fontStlyeModifier_9; }
	inline float* get_address_of_fontStlyeModifier_9() { return &___fontStlyeModifier_9; }
	inline void set_fontStlyeModifier_9(float value)
	{
		___fontStlyeModifier_9 = value;
	}

	inline static int32_t get_offset_of_fontRenderMode_10() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontRenderMode_10)); }
	inline int32_t get_fontRenderMode_10() const { return ___fontRenderMode_10; }
	inline int32_t* get_address_of_fontRenderMode_10() { return &___fontRenderMode_10; }
	inline void set_fontRenderMode_10(int32_t value)
	{
		___fontRenderMode_10 = value;
	}

	inline static int32_t get_offset_of_fontKerning_11() { return static_cast<int32_t>(offsetof(FontCreationSetting_t628772060, ___fontKerning_11)); }
	inline bool get_fontKerning_11() const { return ___fontKerning_11; }
	inline bool* get_address_of_fontKerning_11() { return &___fontKerning_11; }
	inline void set_fontKerning_11(bool value)
	{
		___fontKerning_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t628772060_marshaled_pinvoke
{
	char* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
// Native definition for COM marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t628772060_marshaled_com
{
	Il2CppChar* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
#endif // FONTCREATIONSETTING_T628772060_H
#ifndef TAGATTRIBUTE_T688278634_H
#define TAGATTRIBUTE_T688278634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t688278634 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T688278634_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_T1547998295_H
#define __STATICARRAYINITTYPESIZEU3D40_T1547998295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct  __StaticArrayInitTypeSizeU3D40_t1547998295 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_t1547998295__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_T1547998295_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T2710994318_H
#define __STATICARRAYINITTYPESIZEU3D12_T2710994318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t2710994318 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2710994318__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T2710994318_H
#ifndef VELOCITYHISTORY_T1265565051_H
#define VELOCITYHISTORY_T1265565051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureVelocityTracker/VelocityHistory
struct  VelocityHistory_t1265565051 
{
public:
	// System.Single DigitalRubyShared.GestureVelocityTracker/VelocityHistory::VelocityX
	float ___VelocityX_0;
	// System.Single DigitalRubyShared.GestureVelocityTracker/VelocityHistory::VelocityY
	float ___VelocityY_1;
	// System.Single DigitalRubyShared.GestureVelocityTracker/VelocityHistory::Seconds
	float ___Seconds_2;

public:
	inline static int32_t get_offset_of_VelocityX_0() { return static_cast<int32_t>(offsetof(VelocityHistory_t1265565051, ___VelocityX_0)); }
	inline float get_VelocityX_0() const { return ___VelocityX_0; }
	inline float* get_address_of_VelocityX_0() { return &___VelocityX_0; }
	inline void set_VelocityX_0(float value)
	{
		___VelocityX_0 = value;
	}

	inline static int32_t get_offset_of_VelocityY_1() { return static_cast<int32_t>(offsetof(VelocityHistory_t1265565051, ___VelocityY_1)); }
	inline float get_VelocityY_1() const { return ___VelocityY_1; }
	inline float* get_address_of_VelocityY_1() { return &___VelocityY_1; }
	inline void set_VelocityY_1(float value)
	{
		___VelocityY_1 = value;
	}

	inline static int32_t get_offset_of_Seconds_2() { return static_cast<int32_t>(offsetof(VelocityHistory_t1265565051, ___Seconds_2)); }
	inline float get_Seconds_2() const { return ___Seconds_2; }
	inline float* get_address_of_Seconds_2() { return &___Seconds_2; }
	inline void set_Seconds_2(float value)
	{
		___Seconds_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VELOCITYHISTORY_T1265565051_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SPRITESIZE_T3355290999_H
#define SPRITESIZE_T3355290999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize
struct  SpriteSize_t3355290999 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t3355290999, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T3355290999_H
#ifndef VECTOR2D_T1865246568_H
#define VECTOR2D_T1865246568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Vector2d
struct  Vector2d_t1865246568 
{
public:
	// System.Double Mapbox.Utils.Vector2d::x
	double ___x_1;
	// System.Double Mapbox.Utils.Vector2d::y
	double ___y_2;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___x_1)); }
	inline double get_x_1() const { return ___x_1; }
	inline double* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(double value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___y_2)); }
	inline double get_y_2() const { return ___y_2; }
	inline double* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(double value)
	{
		___y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2D_T1865246568_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef SPRITEFRAME_T3912389194_H
#define SPRITEFRAME_T3912389194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame
struct  SpriteFrame_t3912389194 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_t3912389194, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_T3912389194_H
#ifndef INTERSECTION_T3434721945_H
#define INTERSECTION_T3434721945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Intersection
struct  Intersection_t3434721945  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Directions.Intersection::<Out>k__BackingField
	int32_t ___U3COutU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.Boolean> Mapbox.Directions.Intersection::<Entry>k__BackingField
	List_1_t1569362707 * ___U3CEntryU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.Int32> Mapbox.Directions.Intersection::<Bearings>k__BackingField
	List_1_t128053199 * ___U3CBearingsU3Ek__BackingField_2;
	// Mapbox.Utils.Vector2d Mapbox.Directions.Intersection::<Location>k__BackingField
	Vector2d_t1865246568  ___U3CLocationU3Ek__BackingField_3;
	// System.Nullable`1<System.Int32> Mapbox.Directions.Intersection::<In>k__BackingField
	Nullable_1_t378540539  ___U3CInU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3COutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Intersection_t3434721945, ___U3COutU3Ek__BackingField_0)); }
	inline int32_t get_U3COutU3Ek__BackingField_0() const { return ___U3COutU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3COutU3Ek__BackingField_0() { return &___U3COutU3Ek__BackingField_0; }
	inline void set_U3COutU3Ek__BackingField_0(int32_t value)
	{
		___U3COutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEntryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Intersection_t3434721945, ___U3CEntryU3Ek__BackingField_1)); }
	inline List_1_t1569362707 * get_U3CEntryU3Ek__BackingField_1() const { return ___U3CEntryU3Ek__BackingField_1; }
	inline List_1_t1569362707 ** get_address_of_U3CEntryU3Ek__BackingField_1() { return &___U3CEntryU3Ek__BackingField_1; }
	inline void set_U3CEntryU3Ek__BackingField_1(List_1_t1569362707 * value)
	{
		___U3CEntryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEntryU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBearingsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Intersection_t3434721945, ___U3CBearingsU3Ek__BackingField_2)); }
	inline List_1_t128053199 * get_U3CBearingsU3Ek__BackingField_2() const { return ___U3CBearingsU3Ek__BackingField_2; }
	inline List_1_t128053199 ** get_address_of_U3CBearingsU3Ek__BackingField_2() { return &___U3CBearingsU3Ek__BackingField_2; }
	inline void set_U3CBearingsU3Ek__BackingField_2(List_1_t128053199 * value)
	{
		___U3CBearingsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBearingsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CLocationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Intersection_t3434721945, ___U3CLocationU3Ek__BackingField_3)); }
	inline Vector2d_t1865246568  get_U3CLocationU3Ek__BackingField_3() const { return ___U3CLocationU3Ek__BackingField_3; }
	inline Vector2d_t1865246568 * get_address_of_U3CLocationU3Ek__BackingField_3() { return &___U3CLocationU3Ek__BackingField_3; }
	inline void set_U3CLocationU3Ek__BackingField_3(Vector2d_t1865246568  value)
	{
		___U3CLocationU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CInU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Intersection_t3434721945, ___U3CInU3Ek__BackingField_4)); }
	inline Nullable_1_t378540539  get_U3CInU3Ek__BackingField_4() const { return ___U3CInU3Ek__BackingField_4; }
	inline Nullable_1_t378540539 * get_address_of_U3CInU3Ek__BackingField_4() { return &___U3CInU3Ek__BackingField_4; }
	inline void set_U3CInU3Ek__BackingField_4(Nullable_1_t378540539  value)
	{
		___U3CInU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSECTION_T3434721945_H
#ifndef DIRECTIONRESOURCE_T3837219169_H
#define DIRECTIONRESOURCE_T3837219169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.DirectionResource
struct  DirectionResource_t3837219169  : public Resource_t4129330135
{
public:
	// System.String Mapbox.Directions.DirectionResource::apiEndpoint
	String_t* ___apiEndpoint_4;
	// Mapbox.Directions.RoutingProfile Mapbox.Directions.DirectionResource::profile
	RoutingProfile_t2080742987 * ___profile_5;
	// Mapbox.Utils.Vector2d[] Mapbox.Directions.DirectionResource::coordinates
	Vector2dU5BU5D_t852968953* ___coordinates_6;
	// System.Nullable`1<System.Boolean> Mapbox.Directions.DirectionResource::alternatives
	Nullable_1_t1819850047  ___alternatives_7;
	// Mapbox.Utils.BearingFilter[] Mapbox.Directions.DirectionResource::bearings
	BearingFilterU5BU5D_t1825218296* ___bearings_8;
	// System.Nullable`1<System.Boolean> Mapbox.Directions.DirectionResource::continueStraight
	Nullable_1_t1819850047  ___continueStraight_9;
	// Mapbox.Directions.Overview Mapbox.Directions.DirectionResource::overview
	Overview_t3152666467 * ___overview_10;
	// System.Double[] Mapbox.Directions.DirectionResource::radiuses
	DoubleU5BU5D_t3413330114* ___radiuses_11;
	// System.Nullable`1<System.Boolean> Mapbox.Directions.DirectionResource::steps
	Nullable_1_t1819850047  ___steps_12;

public:
	inline static int32_t get_offset_of_apiEndpoint_4() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___apiEndpoint_4)); }
	inline String_t* get_apiEndpoint_4() const { return ___apiEndpoint_4; }
	inline String_t** get_address_of_apiEndpoint_4() { return &___apiEndpoint_4; }
	inline void set_apiEndpoint_4(String_t* value)
	{
		___apiEndpoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___apiEndpoint_4), value);
	}

	inline static int32_t get_offset_of_profile_5() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___profile_5)); }
	inline RoutingProfile_t2080742987 * get_profile_5() const { return ___profile_5; }
	inline RoutingProfile_t2080742987 ** get_address_of_profile_5() { return &___profile_5; }
	inline void set_profile_5(RoutingProfile_t2080742987 * value)
	{
		___profile_5 = value;
		Il2CppCodeGenWriteBarrier((&___profile_5), value);
	}

	inline static int32_t get_offset_of_coordinates_6() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___coordinates_6)); }
	inline Vector2dU5BU5D_t852968953* get_coordinates_6() const { return ___coordinates_6; }
	inline Vector2dU5BU5D_t852968953** get_address_of_coordinates_6() { return &___coordinates_6; }
	inline void set_coordinates_6(Vector2dU5BU5D_t852968953* value)
	{
		___coordinates_6 = value;
		Il2CppCodeGenWriteBarrier((&___coordinates_6), value);
	}

	inline static int32_t get_offset_of_alternatives_7() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___alternatives_7)); }
	inline Nullable_1_t1819850047  get_alternatives_7() const { return ___alternatives_7; }
	inline Nullable_1_t1819850047 * get_address_of_alternatives_7() { return &___alternatives_7; }
	inline void set_alternatives_7(Nullable_1_t1819850047  value)
	{
		___alternatives_7 = value;
	}

	inline static int32_t get_offset_of_bearings_8() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___bearings_8)); }
	inline BearingFilterU5BU5D_t1825218296* get_bearings_8() const { return ___bearings_8; }
	inline BearingFilterU5BU5D_t1825218296** get_address_of_bearings_8() { return &___bearings_8; }
	inline void set_bearings_8(BearingFilterU5BU5D_t1825218296* value)
	{
		___bearings_8 = value;
		Il2CppCodeGenWriteBarrier((&___bearings_8), value);
	}

	inline static int32_t get_offset_of_continueStraight_9() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___continueStraight_9)); }
	inline Nullable_1_t1819850047  get_continueStraight_9() const { return ___continueStraight_9; }
	inline Nullable_1_t1819850047 * get_address_of_continueStraight_9() { return &___continueStraight_9; }
	inline void set_continueStraight_9(Nullable_1_t1819850047  value)
	{
		___continueStraight_9 = value;
	}

	inline static int32_t get_offset_of_overview_10() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___overview_10)); }
	inline Overview_t3152666467 * get_overview_10() const { return ___overview_10; }
	inline Overview_t3152666467 ** get_address_of_overview_10() { return &___overview_10; }
	inline void set_overview_10(Overview_t3152666467 * value)
	{
		___overview_10 = value;
		Il2CppCodeGenWriteBarrier((&___overview_10), value);
	}

	inline static int32_t get_offset_of_radiuses_11() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___radiuses_11)); }
	inline DoubleU5BU5D_t3413330114* get_radiuses_11() const { return ___radiuses_11; }
	inline DoubleU5BU5D_t3413330114** get_address_of_radiuses_11() { return &___radiuses_11; }
	inline void set_radiuses_11(DoubleU5BU5D_t3413330114* value)
	{
		___radiuses_11 = value;
		Il2CppCodeGenWriteBarrier((&___radiuses_11), value);
	}

	inline static int32_t get_offset_of_steps_12() { return static_cast<int32_t>(offsetof(DirectionResource_t3837219169, ___steps_12)); }
	inline Nullable_1_t1819850047  get_steps_12() const { return ___steps_12; }
	inline Nullable_1_t1819850047 * get_address_of_steps_12() { return &___steps_12; }
	inline void set_steps_12(Nullable_1_t1819850047  value)
	{
		___steps_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONRESOURCE_T3837219169_H
#ifndef SWIPEGESTURERECOGNIZERDIRECTION_T3225897881_H
#define SWIPEGESTURERECOGNIZERDIRECTION_T3225897881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.SwipeGestureRecognizerDirection
struct  SwipeGestureRecognizerDirection_t3225897881 
{
public:
	// System.Int32 DigitalRubyShared.SwipeGestureRecognizerDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerDirection_t3225897881, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEGESTURERECOGNIZERDIRECTION_T3225897881_H
#ifndef VECTOR2DBOUNDS_T1974840945_H
#define VECTOR2DBOUNDS_T1974840945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Vector2dBounds
struct  Vector2dBounds_t1974840945 
{
public:
	// Mapbox.Utils.Vector2d Mapbox.Utils.Vector2dBounds::SouthWest
	Vector2d_t1865246568  ___SouthWest_0;
	// Mapbox.Utils.Vector2d Mapbox.Utils.Vector2dBounds::NorthEast
	Vector2d_t1865246568  ___NorthEast_1;

public:
	inline static int32_t get_offset_of_SouthWest_0() { return static_cast<int32_t>(offsetof(Vector2dBounds_t1974840945, ___SouthWest_0)); }
	inline Vector2d_t1865246568  get_SouthWest_0() const { return ___SouthWest_0; }
	inline Vector2d_t1865246568 * get_address_of_SouthWest_0() { return &___SouthWest_0; }
	inline void set_SouthWest_0(Vector2d_t1865246568  value)
	{
		___SouthWest_0 = value;
	}

	inline static int32_t get_offset_of_NorthEast_1() { return static_cast<int32_t>(offsetof(Vector2dBounds_t1974840945, ___NorthEast_1)); }
	inline Vector2d_t1865246568  get_NorthEast_1() const { return ___NorthEast_1; }
	inline Vector2d_t1865246568 * get_address_of_NorthEast_1() { return &___NorthEast_1; }
	inline void set_NorthEast_1(Vector2d_t1865246568  value)
	{
		___NorthEast_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2DBOUNDS_T1974840945_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef NULLABLE_1_T3587808650_H
#define NULLABLE_1_T3587808650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Utils.Vector2d>
struct  Nullable_1_t3587808650 
{
public:
	// T System.Nullable`1::value
	Vector2d_t1865246568  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3587808650, ___value_0)); }
	inline Vector2d_t1865246568  get_value_0() const { return ___value_0; }
	inline Vector2d_t1865246568 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector2d_t1865246568  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3587808650, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3587808650_H
#ifndef TAGTYPE_T123236451_H
#define TAGTYPE_T123236451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t123236451 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t123236451, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T123236451_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef WAYPOINT_T3638467145_H
#define WAYPOINT_T3638467145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Waypoint
struct  Waypoint_t3638467145  : public RuntimeObject
{
public:
	// System.String Mapbox.Directions.Waypoint::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// Mapbox.Utils.Vector2d Mapbox.Directions.Waypoint::<Location>k__BackingField
	Vector2d_t1865246568  ___U3CLocationU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Waypoint_t3638467145, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLocationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Waypoint_t3638467145, ___U3CLocationU3Ek__BackingField_1)); }
	inline Vector2d_t1865246568  get_U3CLocationU3Ek__BackingField_1() const { return ___U3CLocationU3Ek__BackingField_1; }
	inline Vector2d_t1865246568 * get_address_of_U3CLocationU3Ek__BackingField_1() { return &___U3CLocationU3Ek__BackingField_1; }
	inline void set_U3CLocationU3Ek__BackingField_1(Vector2d_t1865246568  value)
	{
		___U3CLocationU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINT_T3638467145_H
#ifndef MANEUVER_T1966826127_H
#define MANEUVER_T1966826127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Directions.Maneuver
struct  Maneuver_t1966826127  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Directions.Maneuver::<BearingAfter>k__BackingField
	int32_t ___U3CBearingAfterU3Ek__BackingField_0;
	// System.String Mapbox.Directions.Maneuver::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.String Mapbox.Directions.Maneuver::<Modifier>k__BackingField
	String_t* ___U3CModifierU3Ek__BackingField_2;
	// System.Int32 Mapbox.Directions.Maneuver::<BearingBefore>k__BackingField
	int32_t ___U3CBearingBeforeU3Ek__BackingField_3;
	// Mapbox.Utils.Vector2d Mapbox.Directions.Maneuver::<Location>k__BackingField
	Vector2d_t1865246568  ___U3CLocationU3Ek__BackingField_4;
	// System.String Mapbox.Directions.Maneuver::<Instruction>k__BackingField
	String_t* ___U3CInstructionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CBearingAfterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Maneuver_t1966826127, ___U3CBearingAfterU3Ek__BackingField_0)); }
	inline int32_t get_U3CBearingAfterU3Ek__BackingField_0() const { return ___U3CBearingAfterU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CBearingAfterU3Ek__BackingField_0() { return &___U3CBearingAfterU3Ek__BackingField_0; }
	inline void set_U3CBearingAfterU3Ek__BackingField_0(int32_t value)
	{
		___U3CBearingAfterU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Maneuver_t1966826127, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CModifierU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Maneuver_t1966826127, ___U3CModifierU3Ek__BackingField_2)); }
	inline String_t* get_U3CModifierU3Ek__BackingField_2() const { return ___U3CModifierU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CModifierU3Ek__BackingField_2() { return &___U3CModifierU3Ek__BackingField_2; }
	inline void set_U3CModifierU3Ek__BackingField_2(String_t* value)
	{
		___U3CModifierU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModifierU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CBearingBeforeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Maneuver_t1966826127, ___U3CBearingBeforeU3Ek__BackingField_3)); }
	inline int32_t get_U3CBearingBeforeU3Ek__BackingField_3() const { return ___U3CBearingBeforeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CBearingBeforeU3Ek__BackingField_3() { return &___U3CBearingBeforeU3Ek__BackingField_3; }
	inline void set_U3CBearingBeforeU3Ek__BackingField_3(int32_t value)
	{
		___U3CBearingBeforeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLocationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Maneuver_t1966826127, ___U3CLocationU3Ek__BackingField_4)); }
	inline Vector2d_t1865246568  get_U3CLocationU3Ek__BackingField_4() const { return ___U3CLocationU3Ek__BackingField_4; }
	inline Vector2d_t1865246568 * get_address_of_U3CLocationU3Ek__BackingField_4() { return &___U3CLocationU3Ek__BackingField_4; }
	inline void set_U3CLocationU3Ek__BackingField_4(Vector2d_t1865246568  value)
	{
		___U3CLocationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInstructionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Maneuver_t1966826127, ___U3CInstructionU3Ek__BackingField_5)); }
	inline String_t* get_U3CInstructionU3Ek__BackingField_5() const { return ___U3CInstructionU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CInstructionU3Ek__BackingField_5() { return &___U3CInstructionU3Ek__BackingField_5; }
	inline void set_U3CInstructionU3Ek__BackingField_5(String_t* value)
	{
		___U3CInstructionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstructionU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANEUVER_T1966826127_H
#ifndef FINGERSDPADITEM_T4100703432_H
#define FINGERSDPADITEM_T4100703432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersDPadItem
struct  FingersDPadItem_t4100703432 
{
public:
	// System.Int32 DigitalRubyShared.FingersDPadItem::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FingersDPadItem_t4100703432, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSDPADITEM_T4100703432_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MESH_EXTENTS_T3388355125_H
#define MESH_EXTENTS_T3388355125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t3388355125 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T3388355125_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255370  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t2710994318  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::9E6378168821DBABB7EE3D0154346480FAC8AEF1
	__StaticArrayInitTypeSizeU3D40_t1547998295  ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t2710994318  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t2710994318 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t2710994318  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline __StaticArrayInitTypeSizeU3D40_t1547998295  get_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline __StaticArrayInitTypeSizeU3D40_t1547998295 * get_address_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(__StaticArrayInitTypeSizeU3D40_t1547998295  value)
	{
		___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#ifndef SPRITEDATA_T3048397587_H
#define SPRITEDATA_T3048397587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct  SpriteData_t3048397587 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::frame
	SpriteFrame_t3912389194  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::spriteSourceSize
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::sourceSize
	SpriteSize_t3355290999  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::pivot
	Vector2_t2156229523  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___frame_1)); }
	inline SpriteFrame_t3912389194  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_t3912389194 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_t3912389194  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___spriteSourceSize_4)); }
	inline SpriteFrame_t3912389194  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_t3912389194 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_t3912389194  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___sourceSize_5)); }
	inline SpriteSize_t3355290999  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t3355290999 * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t3355290999  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t3048397587, ___pivot_6)); }
	inline Vector2_t2156229523  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_t2156229523 * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_t2156229523  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t3048397587_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_t3912389194  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3912389194  ___spriteSourceSize_4;
	SpriteSize_t3355290999  ___sourceSize_5;
	Vector2_t2156229523  ___pivot_6;
};
#endif // SPRITEDATA_T3048397587_H
#ifndef GESTURERECOGNIZERSTATE_T650110565_H
#define GESTURERECOGNIZERSTATE_T650110565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerState
struct  GestureRecognizerState_t650110565 
{
public:
	// System.Int32 DigitalRubyShared.GestureRecognizerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GestureRecognizerState_t650110565, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERSTATE_T650110565_H
#ifndef CAPTURERESULT_T3073219507_H
#define CAPTURERESULT_T3073219507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript/CaptureResult
struct  CaptureResult_t3073219507 
{
public:
	// System.Int32 DigitalRubyShared.FingersScript/CaptureResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaptureResult_t3073219507, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURERESULT_T3073219507_H
#ifndef SPRITEASSETIMPORTFORMATS_T116390639_H
#define SPRITEASSETIMPORTFORMATS_T116390639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t116390639 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t116390639, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T116390639_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef TMP_VERTEX_T2404176824_H
#define TMP_VERTEX_T2404176824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t2404176824 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2156229523  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2156229523  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2156229523  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv_1)); }
	inline Vector2_t2156229523  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2156229523 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2156229523  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv2_2)); }
	inline Vector2_t2156229523  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2156229523 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2156229523  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv4_3)); }
	inline Vector2_t2156229523  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2156229523 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2156229523  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T2404176824_H
#ifndef GLYPH2D_T1260586688_H
#define GLYPH2D_T1260586688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Glyph2D
struct  Glyph2D_t1260586688  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 TMPro.Glyph2D::bottomLeft
	Vector3_t3722313464  ___bottomLeft_0;
	// UnityEngine.Vector3 TMPro.Glyph2D::topLeft
	Vector3_t3722313464  ___topLeft_1;
	// UnityEngine.Vector3 TMPro.Glyph2D::bottomRight
	Vector3_t3722313464  ___bottomRight_2;
	// UnityEngine.Vector3 TMPro.Glyph2D::topRight
	Vector3_t3722313464  ___topRight_3;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv0
	Vector2_t2156229523  ___uv0_4;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv1
	Vector2_t2156229523  ___uv1_5;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv2
	Vector2_t2156229523  ___uv2_6;
	// UnityEngine.Vector2 TMPro.Glyph2D::uv3
	Vector2_t2156229523  ___uv3_7;

public:
	inline static int32_t get_offset_of_bottomLeft_0() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___bottomLeft_0)); }
	inline Vector3_t3722313464  get_bottomLeft_0() const { return ___bottomLeft_0; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_0() { return &___bottomLeft_0; }
	inline void set_bottomLeft_0(Vector3_t3722313464  value)
	{
		___bottomLeft_0 = value;
	}

	inline static int32_t get_offset_of_topLeft_1() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___topLeft_1)); }
	inline Vector3_t3722313464  get_topLeft_1() const { return ___topLeft_1; }
	inline Vector3_t3722313464 * get_address_of_topLeft_1() { return &___topLeft_1; }
	inline void set_topLeft_1(Vector3_t3722313464  value)
	{
		___topLeft_1 = value;
	}

	inline static int32_t get_offset_of_bottomRight_2() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___bottomRight_2)); }
	inline Vector3_t3722313464  get_bottomRight_2() const { return ___bottomRight_2; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_2() { return &___bottomRight_2; }
	inline void set_bottomRight_2(Vector3_t3722313464  value)
	{
		___bottomRight_2 = value;
	}

	inline static int32_t get_offset_of_topRight_3() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___topRight_3)); }
	inline Vector3_t3722313464  get_topRight_3() const { return ___topRight_3; }
	inline Vector3_t3722313464 * get_address_of_topRight_3() { return &___topRight_3; }
	inline void set_topRight_3(Vector3_t3722313464  value)
	{
		___topRight_3 = value;
	}

	inline static int32_t get_offset_of_uv0_4() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv0_4)); }
	inline Vector2_t2156229523  get_uv0_4() const { return ___uv0_4; }
	inline Vector2_t2156229523 * get_address_of_uv0_4() { return &___uv0_4; }
	inline void set_uv0_4(Vector2_t2156229523  value)
	{
		___uv0_4 = value;
	}

	inline static int32_t get_offset_of_uv1_5() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv1_5)); }
	inline Vector2_t2156229523  get_uv1_5() const { return ___uv1_5; }
	inline Vector2_t2156229523 * get_address_of_uv1_5() { return &___uv1_5; }
	inline void set_uv1_5(Vector2_t2156229523  value)
	{
		___uv1_5 = value;
	}

	inline static int32_t get_offset_of_uv2_6() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv2_6)); }
	inline Vector2_t2156229523  get_uv2_6() const { return ___uv2_6; }
	inline Vector2_t2156229523 * get_address_of_uv2_6() { return &___uv2_6; }
	inline void set_uv2_6(Vector2_t2156229523  value)
	{
		___uv2_6 = value;
	}

	inline static int32_t get_offset_of_uv3_7() { return static_cast<int32_t>(offsetof(Glyph2D_t1260586688, ___uv3_7)); }
	inline Vector2_t2156229523  get_uv3_7() const { return ___uv3_7; }
	inline Vector2_t2156229523 * get_address_of_uv3_7() { return &___uv3_7; }
	inline void set_uv3_7(Vector2_t2156229523  value)
	{
		___uv3_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPH2D_T1260586688_H
#ifndef U3CANIMATIONCOROUTINEU3EC__ITERATOR0_T2868933594_H
#define U3CANIMATIONCOROUTINEU3EC__ITERATOR0_T2868933594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptZoomPanCamera/<AnimationCoRoutine>c__Iterator0
struct  U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DigitalRubyShared.DemoScriptZoomPanCamera/<AnimationCoRoutine>c__Iterator0::<start>__0
	Vector3_t3722313464  ___U3CstartU3E__0_0;
	// System.Single DigitalRubyShared.DemoScriptZoomPanCamera/<AnimationCoRoutine>c__Iterator0::<accumTime>__1
	float ___U3CaccumTimeU3E__1_1;
	// DigitalRubyShared.DemoScriptZoomPanCamera DigitalRubyShared.DemoScriptZoomPanCamera/<AnimationCoRoutine>c__Iterator0::$this
	DemoScriptZoomPanCamera_t3831420416 * ___U24this_2;
	// System.Object DigitalRubyShared.DemoScriptZoomPanCamera/<AnimationCoRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean DigitalRubyShared.DemoScriptZoomPanCamera/<AnimationCoRoutine>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 DigitalRubyShared.DemoScriptZoomPanCamera/<AnimationCoRoutine>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CstartU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594, ___U3CstartU3E__0_0)); }
	inline Vector3_t3722313464  get_U3CstartU3E__0_0() const { return ___U3CstartU3E__0_0; }
	inline Vector3_t3722313464 * get_address_of_U3CstartU3E__0_0() { return &___U3CstartU3E__0_0; }
	inline void set_U3CstartU3E__0_0(Vector3_t3722313464  value)
	{
		___U3CstartU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CaccumTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594, ___U3CaccumTimeU3E__1_1)); }
	inline float get_U3CaccumTimeU3E__1_1() const { return ___U3CaccumTimeU3E__1_1; }
	inline float* get_address_of_U3CaccumTimeU3E__1_1() { return &___U3CaccumTimeU3E__1_1; }
	inline void set_U3CaccumTimeU3E__1_1(float value)
	{
		___U3CaccumTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594, ___U24this_2)); }
	inline DemoScriptZoomPanCamera_t3831420416 * get_U24this_2() const { return ___U24this_2; }
	inline DemoScriptZoomPanCamera_t3831420416 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(DemoScriptZoomPanCamera_t3831420416 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATIONCOROUTINEU3EC__ITERATOR0_T2868933594_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#define TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_t388000256 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_t388000256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_2;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_3;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_7;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_8;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_9;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_10;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_11;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_12;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_13;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_14;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_15;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_1)); }
	inline int32_t get_visibleCharacterCount_1() const { return ___visibleCharacterCount_1; }
	inline int32_t* get_address_of_visibleCharacterCount_1() { return &___visibleCharacterCount_1; }
	inline void set_visibleCharacterCount_1(int32_t value)
	{
		___visibleCharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_spaceCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_2)); }
	inline int32_t get_spaceCount_2() const { return ___spaceCount_2; }
	inline int32_t* get_address_of_spaceCount_2() { return &___spaceCount_2; }
	inline void set_spaceCount_2(int32_t value)
	{
		___spaceCount_2 = value;
	}

	inline static int32_t get_offset_of_wordCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_3)); }
	inline int32_t get_wordCount_3() const { return ___wordCount_3; }
	inline int32_t* get_address_of_wordCount_3() { return &___wordCount_3; }
	inline void set_wordCount_3(int32_t value)
	{
		___wordCount_3 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_4)); }
	inline int32_t get_firstCharacterIndex_4() const { return ___firstCharacterIndex_4; }
	inline int32_t* get_address_of_firstCharacterIndex_4() { return &___firstCharacterIndex_4; }
	inline void set_firstCharacterIndex_4(int32_t value)
	{
		___firstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_5)); }
	inline int32_t get_firstVisibleCharacterIndex_5() const { return ___firstVisibleCharacterIndex_5; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_5() { return &___firstVisibleCharacterIndex_5; }
	inline void set_firstVisibleCharacterIndex_5(int32_t value)
	{
		___firstVisibleCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_6)); }
	inline int32_t get_lastCharacterIndex_6() const { return ___lastCharacterIndex_6; }
	inline int32_t* get_address_of_lastCharacterIndex_6() { return &___lastCharacterIndex_6; }
	inline void set_lastCharacterIndex_6(int32_t value)
	{
		___lastCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_7)); }
	inline int32_t get_lastVisibleCharacterIndex_7() const { return ___lastVisibleCharacterIndex_7; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_7() { return &___lastVisibleCharacterIndex_7; }
	inline void set_lastVisibleCharacterIndex_7(int32_t value)
	{
		___lastVisibleCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_lineHeight_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_9)); }
	inline float get_lineHeight_9() const { return ___lineHeight_9; }
	inline float* get_address_of_lineHeight_9() { return &___lineHeight_9; }
	inline void set_lineHeight_9(float value)
	{
		___lineHeight_9 = value;
	}

	inline static int32_t get_offset_of_ascender_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_10)); }
	inline float get_ascender_10() const { return ___ascender_10; }
	inline float* get_address_of_ascender_10() { return &___ascender_10; }
	inline void set_ascender_10(float value)
	{
		___ascender_10 = value;
	}

	inline static int32_t get_offset_of_baseline_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_11)); }
	inline float get_baseline_11() const { return ___baseline_11; }
	inline float* get_address_of_baseline_11() { return &___baseline_11; }
	inline void set_baseline_11(float value)
	{
		___baseline_11 = value;
	}

	inline static int32_t get_offset_of_descender_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_12)); }
	inline float get_descender_12() const { return ___descender_12; }
	inline float* get_address_of_descender_12() { return &___descender_12; }
	inline void set_descender_12(float value)
	{
		___descender_12 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_13)); }
	inline float get_maxAdvance_13() const { return ___maxAdvance_13; }
	inline float* get_address_of_maxAdvance_13() { return &___maxAdvance_13; }
	inline void set_maxAdvance_13(float value)
	{
		___maxAdvance_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_marginLeft_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_15)); }
	inline float get_marginLeft_15() const { return ___marginLeft_15; }
	inline float* get_address_of_marginLeft_15() { return &___marginLeft_15; }
	inline void set_marginLeft_15(float value)
	{
		___marginLeft_15 = value;
	}

	inline static int32_t get_offset_of_marginRight_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_16)); }
	inline float get_marginRight_16() const { return ___marginRight_16; }
	inline float* get_address_of_marginRight_16() { return &___marginRight_16; }
	inline void set_marginRight_16(float value)
	{
		___marginRight_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_18)); }
	inline Extents_t3837212874  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t3837212874 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t3837212874  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TMP_CHARACTERINFO_T3185626797_H
#define TMP_CHARACTERINFO_T3185626797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t3185626797 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int16 TMPro.TMP_CharacterInfo::index
	int16_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t129727469 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t340375123 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int16 TMPro.TMP_CharacterInfo::lineNumber
	int16_t ___lineNumber_11;
	// System.Int16 TMPro.TMP_CharacterInfo::pageNumber
	int16_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t3722313464  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t3722313464  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t3722313464  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t3722313464  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t2600501292  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___index_1)); }
	inline int16_t get_index_1() const { return ___index_1; }
	inline int16_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int16_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___textElement_3)); }
	inline TMP_TextElement_t129727469 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t129727469 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t129727469 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___fontAsset_4)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___lineNumber_11)); }
	inline int16_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int16_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int16_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pageNumber_12)); }
	inline int16_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int16_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int16_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TL_14)); }
	inline TMP_Vertex_t2404176824  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t2404176824  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BL_15)); }
	inline TMP_Vertex_t2404176824  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t2404176824  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TR_16)); }
	inline TMP_Vertex_t2404176824  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t2404176824  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BR_17)); }
	inline TMP_Vertex_t2404176824  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t2404176824  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topLeft_18)); }
	inline Vector3_t3722313464  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t3722313464 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t3722313464  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomLeft_19)); }
	inline Vector3_t3722313464  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t3722313464  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topRight_20)); }
	inline Vector3_t3722313464  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t3722313464 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t3722313464  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomRight_21)); }
	inline Vector3_t3722313464  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t3722313464  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___color_29)); }
	inline Color32_t2600501292  get_color_29() const { return ___color_29; }
	inline Color32_t2600501292 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t2600501292  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_pinvoke
{
	uint8_t ___character_0;
	int16_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int16_t ___lineNumber_11;
	int16_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_com
{
	uint8_t ___character_0;
	int16_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int16_t ___lineNumber_11;
	int16_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T3185626797_H
#ifndef NULLABLE_1_T3697403027_H
#define NULLABLE_1_T3697403027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Utils.Vector2dBounds>
struct  Nullable_1_t3697403027 
{
public:
	// T System.Nullable`1::value
	Vector2dBounds_t1974840945  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3697403027, ___value_0)); }
	inline Vector2dBounds_t1974840945  get_value_0() const { return ___value_0; }
	inline Vector2dBounds_t1974840945 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector2dBounds_t1974840945  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3697403027, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3697403027_H
#ifndef GESTURERECOGNIZER_T3684029681_H
#define GESTURERECOGNIZER_T3684029681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizer
struct  GestureRecognizer_t3684029681  : public RuntimeObject
{
public:
	// DigitalRubyShared.GestureRecognizerState DigitalRubyShared.GestureRecognizer::state
	int32_t ___state_1;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.GestureRecognizer::currentTrackedTouches
	List_1_t3464476875 * ___currentTrackedTouches_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.GestureRecognizer::currentTrackedTouchesReadOnly
	ReadOnlyCollection_1_t3204978420 * ___currentTrackedTouchesReadOnly_3;
	// DigitalRubyShared.GestureRecognizer DigitalRubyShared.GestureRecognizer::requireGestureRecognizerToFail
	GestureRecognizer_t3684029681 * ___requireGestureRecognizerToFail_4;
	// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.GestureRecognizer::failGestures
	HashSet_1_t2248979155 * ___failGestures_5;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.GestureRecognizer::simultaneousGestures
	List_1_t861137127 * ___simultaneousGestures_6;
	// DigitalRubyShared.GestureVelocityTracker DigitalRubyShared.GestureRecognizer::velocityTracker
	GestureVelocityTracker_t806949657 * ___velocityTracker_7;
	// System.Boolean DigitalRubyShared.GestureRecognizer::touchesJustEnded
	bool ___touchesJustEnded_8;
	// System.Int32 DigitalRubyShared.GestureRecognizer::minimumNumberOfTouchesToTrack
	int32_t ___minimumNumberOfTouchesToTrack_9;
	// System.Int32 DigitalRubyShared.GestureRecognizer::maximumNumberOfTouchesToTrack
	int32_t ___maximumNumberOfTouchesToTrack_10;
	// System.Single DigitalRubyShared.GestureRecognizer::<prevFocusX>k__BackingField
	float ___U3CprevFocusXU3Ek__BackingField_11;
	// System.Single DigitalRubyShared.GestureRecognizer::<prevFocusY>k__BackingField
	float ___U3CprevFocusYU3Ek__BackingField_12;
	// DigitalRubyShared.GestureRecognizerUpdated DigitalRubyShared.GestureRecognizer::Updated
	GestureRecognizerUpdated_t601711085 * ___Updated_14;
	// System.Single DigitalRubyShared.GestureRecognizer::<DeltaX>k__BackingField
	float ___U3CDeltaXU3Ek__BackingField_15;
	// System.Single DigitalRubyShared.GestureRecognizer::<DeltaY>k__BackingField
	float ___U3CDeltaYU3Ek__BackingField_16;
	// System.Single DigitalRubyShared.GestureRecognizer::<FocusX>k__BackingField
	float ___U3CFocusXU3Ek__BackingField_17;
	// System.Single DigitalRubyShared.GestureRecognizer::<FocusY>k__BackingField
	float ___U3CFocusYU3Ek__BackingField_18;
	// System.Single DigitalRubyShared.GestureRecognizer::<StartFocusX>k__BackingField
	float ___U3CStartFocusXU3Ek__BackingField_19;
	// System.Single DigitalRubyShared.GestureRecognizer::<StartFocusY>k__BackingField
	float ___U3CStartFocusYU3Ek__BackingField_20;
	// System.Single DigitalRubyShared.GestureRecognizer::<DistanceX>k__BackingField
	float ___U3CDistanceXU3Ek__BackingField_21;
	// System.Single DigitalRubyShared.GestureRecognizer::<DistanceY>k__BackingField
	float ___U3CDistanceYU3Ek__BackingField_22;
	// System.Object DigitalRubyShared.GestureRecognizer::<PlatformSpecificView>k__BackingField
	RuntimeObject * ___U3CPlatformSpecificViewU3Ek__BackingField_23;
	// System.Single DigitalRubyShared.GestureRecognizer::<PlatformSpecificViewScale>k__BackingField
	float ___U3CPlatformSpecificViewScaleU3Ek__BackingField_24;
	// System.Boolean DigitalRubyShared.GestureRecognizer::<AllowSimultaneousExecutionWithAllGestures>k__BackingField
	bool ___U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_currentTrackedTouches_2() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___currentTrackedTouches_2)); }
	inline List_1_t3464476875 * get_currentTrackedTouches_2() const { return ___currentTrackedTouches_2; }
	inline List_1_t3464476875 ** get_address_of_currentTrackedTouches_2() { return &___currentTrackedTouches_2; }
	inline void set_currentTrackedTouches_2(List_1_t3464476875 * value)
	{
		___currentTrackedTouches_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentTrackedTouches_2), value);
	}

	inline static int32_t get_offset_of_currentTrackedTouchesReadOnly_3() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___currentTrackedTouchesReadOnly_3)); }
	inline ReadOnlyCollection_1_t3204978420 * get_currentTrackedTouchesReadOnly_3() const { return ___currentTrackedTouchesReadOnly_3; }
	inline ReadOnlyCollection_1_t3204978420 ** get_address_of_currentTrackedTouchesReadOnly_3() { return &___currentTrackedTouchesReadOnly_3; }
	inline void set_currentTrackedTouchesReadOnly_3(ReadOnlyCollection_1_t3204978420 * value)
	{
		___currentTrackedTouchesReadOnly_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentTrackedTouchesReadOnly_3), value);
	}

	inline static int32_t get_offset_of_requireGestureRecognizerToFail_4() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___requireGestureRecognizerToFail_4)); }
	inline GestureRecognizer_t3684029681 * get_requireGestureRecognizerToFail_4() const { return ___requireGestureRecognizerToFail_4; }
	inline GestureRecognizer_t3684029681 ** get_address_of_requireGestureRecognizerToFail_4() { return &___requireGestureRecognizerToFail_4; }
	inline void set_requireGestureRecognizerToFail_4(GestureRecognizer_t3684029681 * value)
	{
		___requireGestureRecognizerToFail_4 = value;
		Il2CppCodeGenWriteBarrier((&___requireGestureRecognizerToFail_4), value);
	}

	inline static int32_t get_offset_of_failGestures_5() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___failGestures_5)); }
	inline HashSet_1_t2248979155 * get_failGestures_5() const { return ___failGestures_5; }
	inline HashSet_1_t2248979155 ** get_address_of_failGestures_5() { return &___failGestures_5; }
	inline void set_failGestures_5(HashSet_1_t2248979155 * value)
	{
		___failGestures_5 = value;
		Il2CppCodeGenWriteBarrier((&___failGestures_5), value);
	}

	inline static int32_t get_offset_of_simultaneousGestures_6() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___simultaneousGestures_6)); }
	inline List_1_t861137127 * get_simultaneousGestures_6() const { return ___simultaneousGestures_6; }
	inline List_1_t861137127 ** get_address_of_simultaneousGestures_6() { return &___simultaneousGestures_6; }
	inline void set_simultaneousGestures_6(List_1_t861137127 * value)
	{
		___simultaneousGestures_6 = value;
		Il2CppCodeGenWriteBarrier((&___simultaneousGestures_6), value);
	}

	inline static int32_t get_offset_of_velocityTracker_7() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___velocityTracker_7)); }
	inline GestureVelocityTracker_t806949657 * get_velocityTracker_7() const { return ___velocityTracker_7; }
	inline GestureVelocityTracker_t806949657 ** get_address_of_velocityTracker_7() { return &___velocityTracker_7; }
	inline void set_velocityTracker_7(GestureVelocityTracker_t806949657 * value)
	{
		___velocityTracker_7 = value;
		Il2CppCodeGenWriteBarrier((&___velocityTracker_7), value);
	}

	inline static int32_t get_offset_of_touchesJustEnded_8() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___touchesJustEnded_8)); }
	inline bool get_touchesJustEnded_8() const { return ___touchesJustEnded_8; }
	inline bool* get_address_of_touchesJustEnded_8() { return &___touchesJustEnded_8; }
	inline void set_touchesJustEnded_8(bool value)
	{
		___touchesJustEnded_8 = value;
	}

	inline static int32_t get_offset_of_minimumNumberOfTouchesToTrack_9() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___minimumNumberOfTouchesToTrack_9)); }
	inline int32_t get_minimumNumberOfTouchesToTrack_9() const { return ___minimumNumberOfTouchesToTrack_9; }
	inline int32_t* get_address_of_minimumNumberOfTouchesToTrack_9() { return &___minimumNumberOfTouchesToTrack_9; }
	inline void set_minimumNumberOfTouchesToTrack_9(int32_t value)
	{
		___minimumNumberOfTouchesToTrack_9 = value;
	}

	inline static int32_t get_offset_of_maximumNumberOfTouchesToTrack_10() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___maximumNumberOfTouchesToTrack_10)); }
	inline int32_t get_maximumNumberOfTouchesToTrack_10() const { return ___maximumNumberOfTouchesToTrack_10; }
	inline int32_t* get_address_of_maximumNumberOfTouchesToTrack_10() { return &___maximumNumberOfTouchesToTrack_10; }
	inline void set_maximumNumberOfTouchesToTrack_10(int32_t value)
	{
		___maximumNumberOfTouchesToTrack_10 = value;
	}

	inline static int32_t get_offset_of_U3CprevFocusXU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CprevFocusXU3Ek__BackingField_11)); }
	inline float get_U3CprevFocusXU3Ek__BackingField_11() const { return ___U3CprevFocusXU3Ek__BackingField_11; }
	inline float* get_address_of_U3CprevFocusXU3Ek__BackingField_11() { return &___U3CprevFocusXU3Ek__BackingField_11; }
	inline void set_U3CprevFocusXU3Ek__BackingField_11(float value)
	{
		___U3CprevFocusXU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CprevFocusYU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CprevFocusYU3Ek__BackingField_12)); }
	inline float get_U3CprevFocusYU3Ek__BackingField_12() const { return ___U3CprevFocusYU3Ek__BackingField_12; }
	inline float* get_address_of_U3CprevFocusYU3Ek__BackingField_12() { return &___U3CprevFocusYU3Ek__BackingField_12; }
	inline void set_U3CprevFocusYU3Ek__BackingField_12(float value)
	{
		___U3CprevFocusYU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_Updated_14() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___Updated_14)); }
	inline GestureRecognizerUpdated_t601711085 * get_Updated_14() const { return ___Updated_14; }
	inline GestureRecognizerUpdated_t601711085 ** get_address_of_Updated_14() { return &___Updated_14; }
	inline void set_Updated_14(GestureRecognizerUpdated_t601711085 * value)
	{
		___Updated_14 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_14), value);
	}

	inline static int32_t get_offset_of_U3CDeltaXU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDeltaXU3Ek__BackingField_15)); }
	inline float get_U3CDeltaXU3Ek__BackingField_15() const { return ___U3CDeltaXU3Ek__BackingField_15; }
	inline float* get_address_of_U3CDeltaXU3Ek__BackingField_15() { return &___U3CDeltaXU3Ek__BackingField_15; }
	inline void set_U3CDeltaXU3Ek__BackingField_15(float value)
	{
		___U3CDeltaXU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CDeltaYU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDeltaYU3Ek__BackingField_16)); }
	inline float get_U3CDeltaYU3Ek__BackingField_16() const { return ___U3CDeltaYU3Ek__BackingField_16; }
	inline float* get_address_of_U3CDeltaYU3Ek__BackingField_16() { return &___U3CDeltaYU3Ek__BackingField_16; }
	inline void set_U3CDeltaYU3Ek__BackingField_16(float value)
	{
		___U3CDeltaYU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CFocusXU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CFocusXU3Ek__BackingField_17)); }
	inline float get_U3CFocusXU3Ek__BackingField_17() const { return ___U3CFocusXU3Ek__BackingField_17; }
	inline float* get_address_of_U3CFocusXU3Ek__BackingField_17() { return &___U3CFocusXU3Ek__BackingField_17; }
	inline void set_U3CFocusXU3Ek__BackingField_17(float value)
	{
		___U3CFocusXU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CFocusYU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CFocusYU3Ek__BackingField_18)); }
	inline float get_U3CFocusYU3Ek__BackingField_18() const { return ___U3CFocusYU3Ek__BackingField_18; }
	inline float* get_address_of_U3CFocusYU3Ek__BackingField_18() { return &___U3CFocusYU3Ek__BackingField_18; }
	inline void set_U3CFocusYU3Ek__BackingField_18(float value)
	{
		___U3CFocusYU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CStartFocusXU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CStartFocusXU3Ek__BackingField_19)); }
	inline float get_U3CStartFocusXU3Ek__BackingField_19() const { return ___U3CStartFocusXU3Ek__BackingField_19; }
	inline float* get_address_of_U3CStartFocusXU3Ek__BackingField_19() { return &___U3CStartFocusXU3Ek__BackingField_19; }
	inline void set_U3CStartFocusXU3Ek__BackingField_19(float value)
	{
		___U3CStartFocusXU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CStartFocusYU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CStartFocusYU3Ek__BackingField_20)); }
	inline float get_U3CStartFocusYU3Ek__BackingField_20() const { return ___U3CStartFocusYU3Ek__BackingField_20; }
	inline float* get_address_of_U3CStartFocusYU3Ek__BackingField_20() { return &___U3CStartFocusYU3Ek__BackingField_20; }
	inline void set_U3CStartFocusYU3Ek__BackingField_20(float value)
	{
		___U3CStartFocusYU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceXU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDistanceXU3Ek__BackingField_21)); }
	inline float get_U3CDistanceXU3Ek__BackingField_21() const { return ___U3CDistanceXU3Ek__BackingField_21; }
	inline float* get_address_of_U3CDistanceXU3Ek__BackingField_21() { return &___U3CDistanceXU3Ek__BackingField_21; }
	inline void set_U3CDistanceXU3Ek__BackingField_21(float value)
	{
		___U3CDistanceXU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceYU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDistanceYU3Ek__BackingField_22)); }
	inline float get_U3CDistanceYU3Ek__BackingField_22() const { return ___U3CDistanceYU3Ek__BackingField_22; }
	inline float* get_address_of_U3CDistanceYU3Ek__BackingField_22() { return &___U3CDistanceYU3Ek__BackingField_22; }
	inline void set_U3CDistanceYU3Ek__BackingField_22(float value)
	{
		___U3CDistanceYU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CPlatformSpecificViewU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CPlatformSpecificViewU3Ek__BackingField_23)); }
	inline RuntimeObject * get_U3CPlatformSpecificViewU3Ek__BackingField_23() const { return ___U3CPlatformSpecificViewU3Ek__BackingField_23; }
	inline RuntimeObject ** get_address_of_U3CPlatformSpecificViewU3Ek__BackingField_23() { return &___U3CPlatformSpecificViewU3Ek__BackingField_23; }
	inline void set_U3CPlatformSpecificViewU3Ek__BackingField_23(RuntimeObject * value)
	{
		___U3CPlatformSpecificViewU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformSpecificViewU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CPlatformSpecificViewScaleU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CPlatformSpecificViewScaleU3Ek__BackingField_24)); }
	inline float get_U3CPlatformSpecificViewScaleU3Ek__BackingField_24() const { return ___U3CPlatformSpecificViewScaleU3Ek__BackingField_24; }
	inline float* get_address_of_U3CPlatformSpecificViewScaleU3Ek__BackingField_24() { return &___U3CPlatformSpecificViewScaleU3Ek__BackingField_24; }
	inline void set_U3CPlatformSpecificViewScaleU3Ek__BackingField_24(float value)
	{
		___U3CPlatformSpecificViewScaleU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25)); }
	inline bool get_U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25() const { return ___U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25() { return &___U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25; }
	inline void set_U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25(bool value)
	{
		___U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25 = value;
	}
};

struct GestureRecognizer_t3684029681_StaticFields
{
public:
	// DigitalRubyShared.GestureRecognizer DigitalRubyShared.GestureRecognizer::allGesturesReference
	GestureRecognizer_t3684029681 * ___allGesturesReference_0;
	// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.GestureRecognizer::ActiveGestures
	HashSet_1_t2248979155 * ___ActiveGestures_13;
	// DigitalRubyShared.GestureRecognizer/CallbackMainThreadDelegate DigitalRubyShared.GestureRecognizer::MainThreadCallback
	CallbackMainThreadDelegate_t469493312 * ___MainThreadCallback_26;

public:
	inline static int32_t get_offset_of_allGesturesReference_0() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681_StaticFields, ___allGesturesReference_0)); }
	inline GestureRecognizer_t3684029681 * get_allGesturesReference_0() const { return ___allGesturesReference_0; }
	inline GestureRecognizer_t3684029681 ** get_address_of_allGesturesReference_0() { return &___allGesturesReference_0; }
	inline void set_allGesturesReference_0(GestureRecognizer_t3684029681 * value)
	{
		___allGesturesReference_0 = value;
		Il2CppCodeGenWriteBarrier((&___allGesturesReference_0), value);
	}

	inline static int32_t get_offset_of_ActiveGestures_13() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681_StaticFields, ___ActiveGestures_13)); }
	inline HashSet_1_t2248979155 * get_ActiveGestures_13() const { return ___ActiveGestures_13; }
	inline HashSet_1_t2248979155 ** get_address_of_ActiveGestures_13() { return &___ActiveGestures_13; }
	inline void set_ActiveGestures_13(HashSet_1_t2248979155 * value)
	{
		___ActiveGestures_13 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveGestures_13), value);
	}

	inline static int32_t get_offset_of_MainThreadCallback_26() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681_StaticFields, ___MainThreadCallback_26)); }
	inline CallbackMainThreadDelegate_t469493312 * get_MainThreadCallback_26() const { return ___MainThreadCallback_26; }
	inline CallbackMainThreadDelegate_t469493312 ** get_address_of_MainThreadCallback_26() { return &___MainThreadCallback_26; }
	inline void set_MainThreadCallback_26(CallbackMainThreadDelegate_t469493312 * value)
	{
		___MainThreadCallback_26 = value;
		Il2CppCodeGenWriteBarrier((&___MainThreadCallback_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZER_T3684029681_H
#ifndef XML_TAGATTRIBUTE_T1174424309_H
#define XML_TAGATTRIBUTE_T1174424309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_t1174424309 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_T1174424309_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_46;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_49;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_50;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_51;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_52;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_sizeStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_38)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_38() const { return ___sizeStack_38; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_38() { return &___sizeStack_38; }
	inline void set_sizeStack_38(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_38 = value;
	}

	inline static int32_t get_offset_of_indentStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_39() const { return ___indentStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_39() { return &___indentStack_39; }
	inline void set_indentStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_39 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_40)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_40() const { return ___fontWeightStack_40; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_40() { return &___fontWeightStack_40; }
	inline void set_fontWeightStack_40(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_40 = value;
	}

	inline static int32_t get_offset_of_styleStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_41() const { return ___styleStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_41() { return &___styleStack_41; }
	inline void set_styleStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_41 = value;
	}

	inline static int32_t get_offset_of_baselineStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_42)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_42() const { return ___baselineStack_42; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_42() { return &___baselineStack_42; }
	inline void set_baselineStack_42(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_42 = value;
	}

	inline static int32_t get_offset_of_actionStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_43)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_43() const { return ___actionStack_43; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_43() { return &___actionStack_43; }
	inline void set_actionStack_43(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_43 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_44)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_44() const { return ___materialReferenceStack_44; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_44() { return &___materialReferenceStack_44; }
	inline void set_materialReferenceStack_44(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_44 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_45)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_45() const { return ___lineJustificationStack_45; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_45() { return &___lineJustificationStack_45; }
	inline void set_lineJustificationStack_45(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_45 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_46)); }
	inline int32_t get_spriteAnimationID_46() const { return ___spriteAnimationID_46; }
	inline int32_t* get_address_of_spriteAnimationID_46() { return &___spriteAnimationID_46; }
	inline void set_spriteAnimationID_46(int32_t value)
	{
		___spriteAnimationID_46 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_47)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_47() const { return ___currentFontAsset_47; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_47() { return &___currentFontAsset_47; }
	inline void set_currentFontAsset_47(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_47 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_47), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_48)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_48() const { return ___currentSpriteAsset_48; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_48() { return &___currentSpriteAsset_48; }
	inline void set_currentSpriteAsset_48(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_48), value);
	}

	inline static int32_t get_offset_of_currentMaterial_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_49)); }
	inline Material_t340375123 * get_currentMaterial_49() const { return ___currentMaterial_49; }
	inline Material_t340375123 ** get_address_of_currentMaterial_49() { return &___currentMaterial_49; }
	inline void set_currentMaterial_49(Material_t340375123 * value)
	{
		___currentMaterial_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_49), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_50)); }
	inline int32_t get_currentMaterialIndex_50() const { return ___currentMaterialIndex_50; }
	inline int32_t* get_address_of_currentMaterialIndex_50() { return &___currentMaterialIndex_50; }
	inline void set_currentMaterialIndex_50(int32_t value)
	{
		___currentMaterialIndex_50 = value;
	}

	inline static int32_t get_offset_of_meshExtents_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_51)); }
	inline Extents_t3837212874  get_meshExtents_51() const { return ___meshExtents_51; }
	inline Extents_t3837212874 * get_address_of_meshExtents_51() { return &___meshExtents_51; }
	inline void set_meshExtents_51(Extents_t3837212874  value)
	{
		___meshExtents_51 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_52)); }
	inline bool get_tagNoParsing_52() const { return ___tagNoParsing_52; }
	inline bool* get_address_of_tagNoParsing_52() { return &___tagNoParsing_52; }
	inline void set_tagNoParsing_52(bool value)
	{
		___tagNoParsing_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	int32_t ___spriteAnimationID_46;
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	Material_t340375123 * ___currentMaterial_49;
	int32_t ___currentMaterialIndex_50;
	Extents_t3837212874  ___meshExtents_51;
	int32_t ___tagNoParsing_52;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	int32_t ___spriteAnimationID_46;
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	Material_t340375123 * ___currentMaterial_49;
	int32_t ___currentMaterialIndex_50;
	Extents_t3837212874  ___meshExtents_51;
	int32_t ___tagNoParsing_52;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef IMAGEGESTURERECOGNIZER_T4233185475_H
#define IMAGEGESTURERECOGNIZER_T4233185475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureRecognizer
struct  ImageGestureRecognizer_t4233185475  : public GestureRecognizer_t3684029681
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point>> DigitalRubyShared.ImageGestureRecognizer::points
	List_1_t1060306332 * ___points_33;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::numberOfPaths
	int32_t ___numberOfPaths_34;
	// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point> DigitalRubyShared.ImageGestureRecognizer::currentList
	List_1_t3883198886 * ___currentList_35;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::minX
	int32_t ___minX_36;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::minY
	int32_t ___minY_37;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::maxX
	int32_t ___maxX_38;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::maxY
	int32_t ___maxY_39;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::<MaximumPathCount>k__BackingField
	int32_t ___U3CMaximumPathCountU3Ek__BackingField_40;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<DirectionTolerance>k__BackingField
	float ___U3CDirectionToleranceU3Ek__BackingField_41;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_42;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<MinimumDistanceBetweenPointsUnits>k__BackingField
	float ___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<SimularityMinimum>k__BackingField
	float ___U3CSimularityMinimumU3Ek__BackingField_44;
	// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureImage> DigitalRubyShared.ImageGestureRecognizer::<GestureImages>k__BackingField
	List_1_t62703717 * ___U3CGestureImagesU3Ek__BackingField_45;
	// DigitalRubyShared.ImageGestureImage DigitalRubyShared.ImageGestureRecognizer::<Image>k__BackingField
	ImageGestureImage_t2885596271 * ___U3CImageU3Ek__BackingField_46;
	// DigitalRubyShared.ImageGestureImage DigitalRubyShared.ImageGestureRecognizer::<MatchedGestureImage>k__BackingField
	ImageGestureImage_t2885596271 * ___U3CMatchedGestureImageU3Ek__BackingField_47;
	// System.EventHandler DigitalRubyShared.ImageGestureRecognizer::MaximumPathCountExceeded
	EventHandler_t1348719766 * ___MaximumPathCountExceeded_48;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<MatchedGestureCalculationTimeMilliseconds>k__BackingField
	float ___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::<MinimumPointsToRecognize>k__BackingField
	int32_t ___U3CMinimumPointsToRecognizeU3Ek__BackingField_50;

public:
	inline static int32_t get_offset_of_points_33() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___points_33)); }
	inline List_1_t1060306332 * get_points_33() const { return ___points_33; }
	inline List_1_t1060306332 ** get_address_of_points_33() { return &___points_33; }
	inline void set_points_33(List_1_t1060306332 * value)
	{
		___points_33 = value;
		Il2CppCodeGenWriteBarrier((&___points_33), value);
	}

	inline static int32_t get_offset_of_numberOfPaths_34() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___numberOfPaths_34)); }
	inline int32_t get_numberOfPaths_34() const { return ___numberOfPaths_34; }
	inline int32_t* get_address_of_numberOfPaths_34() { return &___numberOfPaths_34; }
	inline void set_numberOfPaths_34(int32_t value)
	{
		___numberOfPaths_34 = value;
	}

	inline static int32_t get_offset_of_currentList_35() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___currentList_35)); }
	inline List_1_t3883198886 * get_currentList_35() const { return ___currentList_35; }
	inline List_1_t3883198886 ** get_address_of_currentList_35() { return &___currentList_35; }
	inline void set_currentList_35(List_1_t3883198886 * value)
	{
		___currentList_35 = value;
		Il2CppCodeGenWriteBarrier((&___currentList_35), value);
	}

	inline static int32_t get_offset_of_minX_36() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___minX_36)); }
	inline int32_t get_minX_36() const { return ___minX_36; }
	inline int32_t* get_address_of_minX_36() { return &___minX_36; }
	inline void set_minX_36(int32_t value)
	{
		___minX_36 = value;
	}

	inline static int32_t get_offset_of_minY_37() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___minY_37)); }
	inline int32_t get_minY_37() const { return ___minY_37; }
	inline int32_t* get_address_of_minY_37() { return &___minY_37; }
	inline void set_minY_37(int32_t value)
	{
		___minY_37 = value;
	}

	inline static int32_t get_offset_of_maxX_38() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___maxX_38)); }
	inline int32_t get_maxX_38() const { return ___maxX_38; }
	inline int32_t* get_address_of_maxX_38() { return &___maxX_38; }
	inline void set_maxX_38(int32_t value)
	{
		___maxX_38 = value;
	}

	inline static int32_t get_offset_of_maxY_39() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___maxY_39)); }
	inline int32_t get_maxY_39() const { return ___maxY_39; }
	inline int32_t* get_address_of_maxY_39() { return &___maxY_39; }
	inline void set_maxY_39(int32_t value)
	{
		___maxY_39 = value;
	}

	inline static int32_t get_offset_of_U3CMaximumPathCountU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMaximumPathCountU3Ek__BackingField_40)); }
	inline int32_t get_U3CMaximumPathCountU3Ek__BackingField_40() const { return ___U3CMaximumPathCountU3Ek__BackingField_40; }
	inline int32_t* get_address_of_U3CMaximumPathCountU3Ek__BackingField_40() { return &___U3CMaximumPathCountU3Ek__BackingField_40; }
	inline void set_U3CMaximumPathCountU3Ek__BackingField_40(int32_t value)
	{
		___U3CMaximumPathCountU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionToleranceU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CDirectionToleranceU3Ek__BackingField_41)); }
	inline float get_U3CDirectionToleranceU3Ek__BackingField_41() const { return ___U3CDirectionToleranceU3Ek__BackingField_41; }
	inline float* get_address_of_U3CDirectionToleranceU3Ek__BackingField_41() { return &___U3CDirectionToleranceU3Ek__BackingField_41; }
	inline void set_U3CDirectionToleranceU3Ek__BackingField_41(float value)
	{
		___U3CDirectionToleranceU3Ek__BackingField_41 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CThresholdUnitsU3Ek__BackingField_42)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_42() const { return ___U3CThresholdUnitsU3Ek__BackingField_42; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_42() { return &___U3CThresholdUnitsU3Ek__BackingField_42; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_42(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_42 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43)); }
	inline float get_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43() const { return ___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43; }
	inline float* get_address_of_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43() { return &___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43; }
	inline void set_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43(float value)
	{
		___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43 = value;
	}

	inline static int32_t get_offset_of_U3CSimularityMinimumU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CSimularityMinimumU3Ek__BackingField_44)); }
	inline float get_U3CSimularityMinimumU3Ek__BackingField_44() const { return ___U3CSimularityMinimumU3Ek__BackingField_44; }
	inline float* get_address_of_U3CSimularityMinimumU3Ek__BackingField_44() { return &___U3CSimularityMinimumU3Ek__BackingField_44; }
	inline void set_U3CSimularityMinimumU3Ek__BackingField_44(float value)
	{
		___U3CSimularityMinimumU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_U3CGestureImagesU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CGestureImagesU3Ek__BackingField_45)); }
	inline List_1_t62703717 * get_U3CGestureImagesU3Ek__BackingField_45() const { return ___U3CGestureImagesU3Ek__BackingField_45; }
	inline List_1_t62703717 ** get_address_of_U3CGestureImagesU3Ek__BackingField_45() { return &___U3CGestureImagesU3Ek__BackingField_45; }
	inline void set_U3CGestureImagesU3Ek__BackingField_45(List_1_t62703717 * value)
	{
		___U3CGestureImagesU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureImagesU3Ek__BackingField_45), value);
	}

	inline static int32_t get_offset_of_U3CImageU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CImageU3Ek__BackingField_46)); }
	inline ImageGestureImage_t2885596271 * get_U3CImageU3Ek__BackingField_46() const { return ___U3CImageU3Ek__BackingField_46; }
	inline ImageGestureImage_t2885596271 ** get_address_of_U3CImageU3Ek__BackingField_46() { return &___U3CImageU3Ek__BackingField_46; }
	inline void set_U3CImageU3Ek__BackingField_46(ImageGestureImage_t2885596271 * value)
	{
		___U3CImageU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageU3Ek__BackingField_46), value);
	}

	inline static int32_t get_offset_of_U3CMatchedGestureImageU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMatchedGestureImageU3Ek__BackingField_47)); }
	inline ImageGestureImage_t2885596271 * get_U3CMatchedGestureImageU3Ek__BackingField_47() const { return ___U3CMatchedGestureImageU3Ek__BackingField_47; }
	inline ImageGestureImage_t2885596271 ** get_address_of_U3CMatchedGestureImageU3Ek__BackingField_47() { return &___U3CMatchedGestureImageU3Ek__BackingField_47; }
	inline void set_U3CMatchedGestureImageU3Ek__BackingField_47(ImageGestureImage_t2885596271 * value)
	{
		___U3CMatchedGestureImageU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMatchedGestureImageU3Ek__BackingField_47), value);
	}

	inline static int32_t get_offset_of_MaximumPathCountExceeded_48() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___MaximumPathCountExceeded_48)); }
	inline EventHandler_t1348719766 * get_MaximumPathCountExceeded_48() const { return ___MaximumPathCountExceeded_48; }
	inline EventHandler_t1348719766 ** get_address_of_MaximumPathCountExceeded_48() { return &___MaximumPathCountExceeded_48; }
	inline void set_MaximumPathCountExceeded_48(EventHandler_t1348719766 * value)
	{
		___MaximumPathCountExceeded_48 = value;
		Il2CppCodeGenWriteBarrier((&___MaximumPathCountExceeded_48), value);
	}

	inline static int32_t get_offset_of_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49)); }
	inline float get_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49() const { return ___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49; }
	inline float* get_address_of_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49() { return &___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49; }
	inline void set_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49(float value)
	{
		___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumPointsToRecognizeU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMinimumPointsToRecognizeU3Ek__BackingField_50)); }
	inline int32_t get_U3CMinimumPointsToRecognizeU3Ek__BackingField_50() const { return ___U3CMinimumPointsToRecognizeU3Ek__BackingField_50; }
	inline int32_t* get_address_of_U3CMinimumPointsToRecognizeU3Ek__BackingField_50() { return &___U3CMinimumPointsToRecognizeU3Ek__BackingField_50; }
	inline void set_U3CMinimumPointsToRecognizeU3Ek__BackingField_50(int32_t value)
	{
		___U3CMinimumPointsToRecognizeU3Ek__BackingField_50 = value;
	}
};

struct ImageGestureRecognizer_t4233185475_StaticFields
{
public:
	// System.UInt64[] DigitalRubyShared.ImageGestureRecognizer::RowBitMasks
	UInt64U5BU5D_t1659327989* ___RowBitMasks_27;
	// System.UInt64 DigitalRubyShared.ImageGestureRecognizer::RowBitmask
	uint64_t ___RowBitmask_28;

public:
	inline static int32_t get_offset_of_RowBitMasks_27() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475_StaticFields, ___RowBitMasks_27)); }
	inline UInt64U5BU5D_t1659327989* get_RowBitMasks_27() const { return ___RowBitMasks_27; }
	inline UInt64U5BU5D_t1659327989** get_address_of_RowBitMasks_27() { return &___RowBitMasks_27; }
	inline void set_RowBitMasks_27(UInt64U5BU5D_t1659327989* value)
	{
		___RowBitMasks_27 = value;
		Il2CppCodeGenWriteBarrier((&___RowBitMasks_27), value);
	}

	inline static int32_t get_offset_of_RowBitmask_28() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475_StaticFields, ___RowBitmask_28)); }
	inline uint64_t get_RowBitmask_28() const { return ___RowBitmask_28; }
	inline uint64_t* get_address_of_RowBitmask_28() { return &___RowBitmask_28; }
	inline void set_RowBitmask_28(uint64_t value)
	{
		___RowBitmask_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEGESTURERECOGNIZER_T4233185475_H
#ifndef SCALEGESTURERECOGNIZER_T1137887245_H
#define SCALEGESTURERECOGNIZER_T1137887245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ScaleGestureRecognizer
struct  ScaleGestureRecognizer_t1137887245  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::previousDistance
	float ___previousDistance_27;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::centerX
	float ___centerX_28;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::centerY
	float ___centerY_29;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ScaleMultiplier>k__BackingField
	float ___U3CScaleMultiplierU3Ek__BackingField_30;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ZoomSpeed>k__BackingField
	float ___U3CZoomSpeedU3Ek__BackingField_31;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_32;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ScaleThresholdPercent>k__BackingField
	float ___U3CScaleThresholdPercentU3Ek__BackingField_33;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ScaleFocusMoveThresholdUnits>k__BackingField
	float ___U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34;

public:
	inline static int32_t get_offset_of_previousDistance_27() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___previousDistance_27)); }
	inline float get_previousDistance_27() const { return ___previousDistance_27; }
	inline float* get_address_of_previousDistance_27() { return &___previousDistance_27; }
	inline void set_previousDistance_27(float value)
	{
		___previousDistance_27 = value;
	}

	inline static int32_t get_offset_of_centerX_28() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___centerX_28)); }
	inline float get_centerX_28() const { return ___centerX_28; }
	inline float* get_address_of_centerX_28() { return &___centerX_28; }
	inline void set_centerX_28(float value)
	{
		___centerX_28 = value;
	}

	inline static int32_t get_offset_of_centerY_29() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___centerY_29)); }
	inline float get_centerY_29() const { return ___centerY_29; }
	inline float* get_address_of_centerY_29() { return &___centerY_29; }
	inline void set_centerY_29(float value)
	{
		___centerY_29 = value;
	}

	inline static int32_t get_offset_of_U3CScaleMultiplierU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CScaleMultiplierU3Ek__BackingField_30)); }
	inline float get_U3CScaleMultiplierU3Ek__BackingField_30() const { return ___U3CScaleMultiplierU3Ek__BackingField_30; }
	inline float* get_address_of_U3CScaleMultiplierU3Ek__BackingField_30() { return &___U3CScaleMultiplierU3Ek__BackingField_30; }
	inline void set_U3CScaleMultiplierU3Ek__BackingField_30(float value)
	{
		___U3CScaleMultiplierU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CZoomSpeedU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CZoomSpeedU3Ek__BackingField_31)); }
	inline float get_U3CZoomSpeedU3Ek__BackingField_31() const { return ___U3CZoomSpeedU3Ek__BackingField_31; }
	inline float* get_address_of_U3CZoomSpeedU3Ek__BackingField_31() { return &___U3CZoomSpeedU3Ek__BackingField_31; }
	inline void set_U3CZoomSpeedU3Ek__BackingField_31(float value)
	{
		___U3CZoomSpeedU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CThresholdUnitsU3Ek__BackingField_32)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_32() const { return ___U3CThresholdUnitsU3Ek__BackingField_32; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_32() { return &___U3CThresholdUnitsU3Ek__BackingField_32; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_32(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CScaleThresholdPercentU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CScaleThresholdPercentU3Ek__BackingField_33)); }
	inline float get_U3CScaleThresholdPercentU3Ek__BackingField_33() const { return ___U3CScaleThresholdPercentU3Ek__BackingField_33; }
	inline float* get_address_of_U3CScaleThresholdPercentU3Ek__BackingField_33() { return &___U3CScaleThresholdPercentU3Ek__BackingField_33; }
	inline void set_U3CScaleThresholdPercentU3Ek__BackingField_33(float value)
	{
		___U3CScaleThresholdPercentU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34)); }
	inline float get_U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34() const { return ___U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34; }
	inline float* get_address_of_U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34() { return &___U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34; }
	inline void set_U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34(float value)
	{
		___U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEGESTURERECOGNIZER_T1137887245_H
#ifndef FEATURE_T4105919465_H
#define FEATURE_T4105919465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.Feature
struct  Feature_t4105919465  : public RuntimeObject
{
public:
	// System.String Mapbox.Geocoding.Feature::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_0;
	// System.String Mapbox.Geocoding.Feature::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.String Mapbox.Geocoding.Feature::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_2;
	// System.String Mapbox.Geocoding.Feature::<PlaceName>k__BackingField
	String_t* ___U3CPlaceNameU3Ek__BackingField_3;
	// System.Double Mapbox.Geocoding.Feature::<Relevance>k__BackingField
	double ___U3CRelevanceU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Mapbox.Geocoding.Feature::<Properties>k__BackingField
	Dictionary_2_t2865362463 * ___U3CPropertiesU3Ek__BackingField_5;
	// System.Nullable`1<Mapbox.Utils.Vector2dBounds> Mapbox.Geocoding.Feature::<Bbox>k__BackingField
	Nullable_1_t3697403027  ___U3CBboxU3Ek__BackingField_6;
	// Mapbox.Utils.Vector2d Mapbox.Geocoding.Feature::<Center>k__BackingField
	Vector2d_t1865246568  ___U3CCenterU3Ek__BackingField_7;
	// Mapbox.Geocoding.Geometry Mapbox.Geocoding.Feature::<Geometry>k__BackingField
	Geometry_t3224508297 * ___U3CGeometryU3Ek__BackingField_8;
	// System.String Mapbox.Geocoding.Feature::<Address>k__BackingField
	String_t* ___U3CAddressU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.String>> Mapbox.Geocoding.Feature::<Context>k__BackingField
	List_1_t3104781730 * ___U3CContextU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CTextU3Ek__BackingField_2() const { return ___U3CTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_2() { return &___U3CTextU3Ek__BackingField_2; }
	inline void set_U3CTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CPlaceNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CPlaceNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CPlaceNameU3Ek__BackingField_3() const { return ___U3CPlaceNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CPlaceNameU3Ek__BackingField_3() { return &___U3CPlaceNameU3Ek__BackingField_3; }
	inline void set_U3CPlaceNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CPlaceNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlaceNameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CRelevanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CRelevanceU3Ek__BackingField_4)); }
	inline double get_U3CRelevanceU3Ek__BackingField_4() const { return ___U3CRelevanceU3Ek__BackingField_4; }
	inline double* get_address_of_U3CRelevanceU3Ek__BackingField_4() { return &___U3CRelevanceU3Ek__BackingField_4; }
	inline void set_U3CRelevanceU3Ek__BackingField_4(double value)
	{
		___U3CRelevanceU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CPropertiesU3Ek__BackingField_5)); }
	inline Dictionary_2_t2865362463 * get_U3CPropertiesU3Ek__BackingField_5() const { return ___U3CPropertiesU3Ek__BackingField_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CPropertiesU3Ek__BackingField_5() { return &___U3CPropertiesU3Ek__BackingField_5; }
	inline void set_U3CPropertiesU3Ek__BackingField_5(Dictionary_2_t2865362463 * value)
	{
		___U3CPropertiesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CBboxU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CBboxU3Ek__BackingField_6)); }
	inline Nullable_1_t3697403027  get_U3CBboxU3Ek__BackingField_6() const { return ___U3CBboxU3Ek__BackingField_6; }
	inline Nullable_1_t3697403027 * get_address_of_U3CBboxU3Ek__BackingField_6() { return &___U3CBboxU3Ek__BackingField_6; }
	inline void set_U3CBboxU3Ek__BackingField_6(Nullable_1_t3697403027  value)
	{
		___U3CBboxU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CCenterU3Ek__BackingField_7)); }
	inline Vector2d_t1865246568  get_U3CCenterU3Ek__BackingField_7() const { return ___U3CCenterU3Ek__BackingField_7; }
	inline Vector2d_t1865246568 * get_address_of_U3CCenterU3Ek__BackingField_7() { return &___U3CCenterU3Ek__BackingField_7; }
	inline void set_U3CCenterU3Ek__BackingField_7(Vector2d_t1865246568  value)
	{
		___U3CCenterU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CGeometryU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CGeometryU3Ek__BackingField_8)); }
	inline Geometry_t3224508297 * get_U3CGeometryU3Ek__BackingField_8() const { return ___U3CGeometryU3Ek__BackingField_8; }
	inline Geometry_t3224508297 ** get_address_of_U3CGeometryU3Ek__BackingField_8() { return &___U3CGeometryU3Ek__BackingField_8; }
	inline void set_U3CGeometryU3Ek__BackingField_8(Geometry_t3224508297 * value)
	{
		___U3CGeometryU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGeometryU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CAddressU3Ek__BackingField_9)); }
	inline String_t* get_U3CAddressU3Ek__BackingField_9() const { return ___U3CAddressU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CAddressU3Ek__BackingField_9() { return &___U3CAddressU3Ek__BackingField_9; }
	inline void set_U3CAddressU3Ek__BackingField_9(String_t* value)
	{
		___U3CAddressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAddressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Feature_t4105919465, ___U3CContextU3Ek__BackingField_10)); }
	inline List_1_t3104781730 * get_U3CContextU3Ek__BackingField_10() const { return ___U3CContextU3Ek__BackingField_10; }
	inline List_1_t3104781730 ** get_address_of_U3CContextU3Ek__BackingField_10() { return &___U3CContextU3Ek__BackingField_10; }
	inline void set_U3CContextU3Ek__BackingField_10(List_1_t3104781730 * value)
	{
		___U3CContextU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURE_T4105919465_H
#ifndef FORWARDGEOCODERESOURCE_T367023433_H
#define FORWARDGEOCODERESOURCE_T367023433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.ForwardGeocodeResource
struct  ForwardGeocodeResource_t367023433  : public GeocodeResource_1_t2673869757
{
public:
	// System.String Mapbox.Geocoding.ForwardGeocodeResource::query
	String_t* ___query_9;
	// System.Nullable`1<System.Boolean> Mapbox.Geocoding.ForwardGeocodeResource::autocomplete
	Nullable_1_t1819850047  ___autocomplete_10;
	// System.String[] Mapbox.Geocoding.ForwardGeocodeResource::country
	StringU5BU5D_t1281789340* ___country_11;
	// System.Nullable`1<Mapbox.Utils.Vector2d> Mapbox.Geocoding.ForwardGeocodeResource::proximity
	Nullable_1_t3587808650  ___proximity_12;
	// System.Nullable`1<Mapbox.Utils.Vector2dBounds> Mapbox.Geocoding.ForwardGeocodeResource::bbox
	Nullable_1_t3697403027  ___bbox_13;

public:
	inline static int32_t get_offset_of_query_9() { return static_cast<int32_t>(offsetof(ForwardGeocodeResource_t367023433, ___query_9)); }
	inline String_t* get_query_9() const { return ___query_9; }
	inline String_t** get_address_of_query_9() { return &___query_9; }
	inline void set_query_9(String_t* value)
	{
		___query_9 = value;
		Il2CppCodeGenWriteBarrier((&___query_9), value);
	}

	inline static int32_t get_offset_of_autocomplete_10() { return static_cast<int32_t>(offsetof(ForwardGeocodeResource_t367023433, ___autocomplete_10)); }
	inline Nullable_1_t1819850047  get_autocomplete_10() const { return ___autocomplete_10; }
	inline Nullable_1_t1819850047 * get_address_of_autocomplete_10() { return &___autocomplete_10; }
	inline void set_autocomplete_10(Nullable_1_t1819850047  value)
	{
		___autocomplete_10 = value;
	}

	inline static int32_t get_offset_of_country_11() { return static_cast<int32_t>(offsetof(ForwardGeocodeResource_t367023433, ___country_11)); }
	inline StringU5BU5D_t1281789340* get_country_11() const { return ___country_11; }
	inline StringU5BU5D_t1281789340** get_address_of_country_11() { return &___country_11; }
	inline void set_country_11(StringU5BU5D_t1281789340* value)
	{
		___country_11 = value;
		Il2CppCodeGenWriteBarrier((&___country_11), value);
	}

	inline static int32_t get_offset_of_proximity_12() { return static_cast<int32_t>(offsetof(ForwardGeocodeResource_t367023433, ___proximity_12)); }
	inline Nullable_1_t3587808650  get_proximity_12() const { return ___proximity_12; }
	inline Nullable_1_t3587808650 * get_address_of_proximity_12() { return &___proximity_12; }
	inline void set_proximity_12(Nullable_1_t3587808650  value)
	{
		___proximity_12 = value;
	}

	inline static int32_t get_offset_of_bbox_13() { return static_cast<int32_t>(offsetof(ForwardGeocodeResource_t367023433, ___bbox_13)); }
	inline Nullable_1_t3697403027  get_bbox_13() const { return ___bbox_13; }
	inline Nullable_1_t3697403027 * get_address_of_bbox_13() { return &___bbox_13; }
	inline void set_bbox_13(Nullable_1_t3697403027  value)
	{
		___bbox_13 = value;
	}
};

struct ForwardGeocodeResource_t367023433_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> Mapbox.Geocoding.ForwardGeocodeResource::CountryCodes
	List_1_t3319525431 * ___CountryCodes_8;

public:
	inline static int32_t get_offset_of_CountryCodes_8() { return static_cast<int32_t>(offsetof(ForwardGeocodeResource_t367023433_StaticFields, ___CountryCodes_8)); }
	inline List_1_t3319525431 * get_CountryCodes_8() const { return ___CountryCodes_8; }
	inline List_1_t3319525431 ** get_address_of_CountryCodes_8() { return &___CountryCodes_8; }
	inline void set_CountryCodes_8(List_1_t3319525431 * value)
	{
		___CountryCodes_8 = value;
		Il2CppCodeGenWriteBarrier((&___CountryCodes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDGEOCODERESOURCE_T367023433_H
#ifndef GESTURERECOGNIZERUPDATED_T601711085_H
#define GESTURERECOGNIZERUPDATED_T601711085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerUpdated
struct  GestureRecognizerUpdated_t601711085  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERUPDATED_T601711085_H
#ifndef CALLBACKMAINTHREADDELEGATE_T469493312_H
#define CALLBACKMAINTHREADDELEGATE_T469493312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizer/CallbackMainThreadDelegate
struct  CallbackMainThreadDelegate_t469493312  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKMAINTHREADDELEGATE_T469493312_H
#ifndef LONGPRESSGESTURERECOGNIZER_T3980777482_H
#define LONGPRESSGESTURERECOGNIZER_T3980777482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.LongPressGestureRecognizer
struct  LongPressGestureRecognizer_t3980777482  : public GestureRecognizer_t3684029681
{
public:
	// System.Int32 DigitalRubyShared.LongPressGestureRecognizer::tag
	int32_t ___tag_27;
	// System.Single DigitalRubyShared.LongPressGestureRecognizer::<MinimumDurationSeconds>k__BackingField
	float ___U3CMinimumDurationSecondsU3Ek__BackingField_28;
	// System.Single DigitalRubyShared.LongPressGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_tag_27() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizer_t3980777482, ___tag_27)); }
	inline int32_t get_tag_27() const { return ___tag_27; }
	inline int32_t* get_address_of_tag_27() { return &___tag_27; }
	inline void set_tag_27(int32_t value)
	{
		___tag_27 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumDurationSecondsU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizer_t3980777482, ___U3CMinimumDurationSecondsU3Ek__BackingField_28)); }
	inline float get_U3CMinimumDurationSecondsU3Ek__BackingField_28() const { return ___U3CMinimumDurationSecondsU3Ek__BackingField_28; }
	inline float* get_address_of_U3CMinimumDurationSecondsU3Ek__BackingField_28() { return &___U3CMinimumDurationSecondsU3Ek__BackingField_28; }
	inline void set_U3CMinimumDurationSecondsU3Ek__BackingField_28(float value)
	{
		___U3CMinimumDurationSecondsU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizer_t3980777482, ___U3CThresholdUnitsU3Ek__BackingField_29)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_29() const { return ___U3CThresholdUnitsU3Ek__BackingField_29; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_29() { return &___U3CThresholdUnitsU3Ek__BackingField_29; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_29(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPRESSGESTURERECOGNIZER_T3980777482_H
#ifndef ONETOUCHSCALEGESTURERECOGNIZER_T1313669683_H
#define ONETOUCHSCALEGESTURERECOGNIZER_T1313669683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.OneTouchScaleGestureRecognizer
struct  OneTouchScaleGestureRecognizer_t1313669683  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ScaleMultiplier>k__BackingField
	float ___U3CScaleMultiplierU3Ek__BackingField_27;
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ZoomSpeed>k__BackingField
	float ___U3CZoomSpeedU3Ek__BackingField_28;
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_U3CScaleMultiplierU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CScaleMultiplierU3Ek__BackingField_27)); }
	inline float get_U3CScaleMultiplierU3Ek__BackingField_27() const { return ___U3CScaleMultiplierU3Ek__BackingField_27; }
	inline float* get_address_of_U3CScaleMultiplierU3Ek__BackingField_27() { return &___U3CScaleMultiplierU3Ek__BackingField_27; }
	inline void set_U3CScaleMultiplierU3Ek__BackingField_27(float value)
	{
		___U3CScaleMultiplierU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CZoomSpeedU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CZoomSpeedU3Ek__BackingField_28)); }
	inline float get_U3CZoomSpeedU3Ek__BackingField_28() const { return ___U3CZoomSpeedU3Ek__BackingField_28; }
	inline float* get_address_of_U3CZoomSpeedU3Ek__BackingField_28() { return &___U3CZoomSpeedU3Ek__BackingField_28; }
	inline void set_U3CZoomSpeedU3Ek__BackingField_28(float value)
	{
		___U3CZoomSpeedU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CThresholdUnitsU3Ek__BackingField_29)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_29() const { return ___U3CThresholdUnitsU3Ek__BackingField_29; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_29() { return &___U3CThresholdUnitsU3Ek__BackingField_29; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_29(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONETOUCHSCALEGESTURERECOGNIZER_T1313669683_H
#ifndef PANGESTURERECOGNIZER_T195762396_H
#define PANGESTURERECOGNIZER_T195762396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.PanGestureRecognizer
struct  PanGestureRecognizer_t195762396  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.PanGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PanGestureRecognizer_t195762396, ___U3CThresholdUnitsU3Ek__BackingField_27)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_27() const { return ___U3CThresholdUnitsU3Ek__BackingField_27; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_27() { return &___U3CThresholdUnitsU3Ek__BackingField_27; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_27(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANGESTURERECOGNIZER_T195762396_H
#ifndef TAPGESTURERECOGNIZER_T3178883670_H
#define TAPGESTURERECOGNIZER_T3178883670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.TapGestureRecognizer
struct  TapGestureRecognizer_t3178883670  : public GestureRecognizer_t3684029681
{
public:
	// System.Int32 DigitalRubyShared.TapGestureRecognizer::tapCount
	int32_t ___tapCount_27;
	// System.Diagnostics.Stopwatch DigitalRubyShared.TapGestureRecognizer::timer
	Stopwatch_t305734070 * ___timer_28;
	// System.Int32 DigitalRubyShared.TapGestureRecognizer::<NumberOfTapsRequired>k__BackingField
	int32_t ___U3CNumberOfTapsRequiredU3Ek__BackingField_29;
	// System.Single DigitalRubyShared.TapGestureRecognizer::<ThresholdSeconds>k__BackingField
	float ___U3CThresholdSecondsU3Ek__BackingField_30;
	// System.Single DigitalRubyShared.TapGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_31;
	// System.Single DigitalRubyShared.TapGestureRecognizer::<TapX>k__BackingField
	float ___U3CTapXU3Ek__BackingField_32;
	// System.Single DigitalRubyShared.TapGestureRecognizer::<TapY>k__BackingField
	float ___U3CTapYU3Ek__BackingField_33;
	// System.Boolean DigitalRubyShared.TapGestureRecognizer::<SendBeginState>k__BackingField
	bool ___U3CSendBeginStateU3Ek__BackingField_34;

public:
	inline static int32_t get_offset_of_tapCount_27() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___tapCount_27)); }
	inline int32_t get_tapCount_27() const { return ___tapCount_27; }
	inline int32_t* get_address_of_tapCount_27() { return &___tapCount_27; }
	inline void set_tapCount_27(int32_t value)
	{
		___tapCount_27 = value;
	}

	inline static int32_t get_offset_of_timer_28() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___timer_28)); }
	inline Stopwatch_t305734070 * get_timer_28() const { return ___timer_28; }
	inline Stopwatch_t305734070 ** get_address_of_timer_28() { return &___timer_28; }
	inline void set_timer_28(Stopwatch_t305734070 * value)
	{
		___timer_28 = value;
		Il2CppCodeGenWriteBarrier((&___timer_28), value);
	}

	inline static int32_t get_offset_of_U3CNumberOfTapsRequiredU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CNumberOfTapsRequiredU3Ek__BackingField_29)); }
	inline int32_t get_U3CNumberOfTapsRequiredU3Ek__BackingField_29() const { return ___U3CNumberOfTapsRequiredU3Ek__BackingField_29; }
	inline int32_t* get_address_of_U3CNumberOfTapsRequiredU3Ek__BackingField_29() { return &___U3CNumberOfTapsRequiredU3Ek__BackingField_29; }
	inline void set_U3CNumberOfTapsRequiredU3Ek__BackingField_29(int32_t value)
	{
		___U3CNumberOfTapsRequiredU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdSecondsU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CThresholdSecondsU3Ek__BackingField_30)); }
	inline float get_U3CThresholdSecondsU3Ek__BackingField_30() const { return ___U3CThresholdSecondsU3Ek__BackingField_30; }
	inline float* get_address_of_U3CThresholdSecondsU3Ek__BackingField_30() { return &___U3CThresholdSecondsU3Ek__BackingField_30; }
	inline void set_U3CThresholdSecondsU3Ek__BackingField_30(float value)
	{
		___U3CThresholdSecondsU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CThresholdUnitsU3Ek__BackingField_31)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_31() const { return ___U3CThresholdUnitsU3Ek__BackingField_31; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_31() { return &___U3CThresholdUnitsU3Ek__BackingField_31; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_31(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CTapXU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CTapXU3Ek__BackingField_32)); }
	inline float get_U3CTapXU3Ek__BackingField_32() const { return ___U3CTapXU3Ek__BackingField_32; }
	inline float* get_address_of_U3CTapXU3Ek__BackingField_32() { return &___U3CTapXU3Ek__BackingField_32; }
	inline void set_U3CTapXU3Ek__BackingField_32(float value)
	{
		___U3CTapXU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CTapYU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CTapYU3Ek__BackingField_33)); }
	inline float get_U3CTapYU3Ek__BackingField_33() const { return ___U3CTapYU3Ek__BackingField_33; }
	inline float* get_address_of_U3CTapYU3Ek__BackingField_33() { return &___U3CTapYU3Ek__BackingField_33; }
	inline void set_U3CTapYU3Ek__BackingField_33(float value)
	{
		___U3CTapYU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CSendBeginStateU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CSendBeginStateU3Ek__BackingField_34)); }
	inline bool get_U3CSendBeginStateU3Ek__BackingField_34() const { return ___U3CSendBeginStateU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CSendBeginStateU3Ek__BackingField_34() { return &___U3CSendBeginStateU3Ek__BackingField_34; }
	inline void set_U3CSendBeginStateU3Ek__BackingField_34(bool value)
	{
		___U3CSendBeginStateU3Ek__BackingField_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPGESTURERECOGNIZER_T3178883670_H
#ifndef SWIPEGESTURERECOGNIZER_T2328511861_H
#define SWIPEGESTURERECOGNIZER_T2328511861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.SwipeGestureRecognizer
struct  SwipeGestureRecognizer_t2328511861  : public GestureRecognizer_t3684029681
{
public:
	// DigitalRubyShared.SwipeGestureRecognizerDirection DigitalRubyShared.SwipeGestureRecognizer::<Direction>k__BackingField
	int32_t ___U3CDirectionU3Ek__BackingField_27;
	// System.Single DigitalRubyShared.SwipeGestureRecognizer::<MinimumDistanceUnits>k__BackingField
	float ___U3CMinimumDistanceUnitsU3Ek__BackingField_28;
	// System.Single DigitalRubyShared.SwipeGestureRecognizer::<MinimumSpeedUnits>k__BackingField
	float ___U3CMinimumSpeedUnitsU3Ek__BackingField_29;
	// System.Single DigitalRubyShared.SwipeGestureRecognizer::<DirectionThreshold>k__BackingField
	float ___U3CDirectionThresholdU3Ek__BackingField_30;
	// DigitalRubyShared.SwipeGestureRecognizerDirection DigitalRubyShared.SwipeGestureRecognizer::<EndDirection>k__BackingField
	int32_t ___U3CEndDirectionU3Ek__BackingField_31;
	// System.Boolean DigitalRubyShared.SwipeGestureRecognizer::<EndImmeditely>k__BackingField
	bool ___U3CEndImmeditelyU3Ek__BackingField_32;
	// System.Boolean DigitalRubyShared.SwipeGestureRecognizer::<FailOnDirectionChange>k__BackingField
	bool ___U3CFailOnDirectionChangeU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of_U3CDirectionU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CDirectionU3Ek__BackingField_27)); }
	inline int32_t get_U3CDirectionU3Ek__BackingField_27() const { return ___U3CDirectionU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CDirectionU3Ek__BackingField_27() { return &___U3CDirectionU3Ek__BackingField_27; }
	inline void set_U3CDirectionU3Ek__BackingField_27(int32_t value)
	{
		___U3CDirectionU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumDistanceUnitsU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CMinimumDistanceUnitsU3Ek__BackingField_28)); }
	inline float get_U3CMinimumDistanceUnitsU3Ek__BackingField_28() const { return ___U3CMinimumDistanceUnitsU3Ek__BackingField_28; }
	inline float* get_address_of_U3CMinimumDistanceUnitsU3Ek__BackingField_28() { return &___U3CMinimumDistanceUnitsU3Ek__BackingField_28; }
	inline void set_U3CMinimumDistanceUnitsU3Ek__BackingField_28(float value)
	{
		___U3CMinimumDistanceUnitsU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumSpeedUnitsU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CMinimumSpeedUnitsU3Ek__BackingField_29)); }
	inline float get_U3CMinimumSpeedUnitsU3Ek__BackingField_29() const { return ___U3CMinimumSpeedUnitsU3Ek__BackingField_29; }
	inline float* get_address_of_U3CMinimumSpeedUnitsU3Ek__BackingField_29() { return &___U3CMinimumSpeedUnitsU3Ek__BackingField_29; }
	inline void set_U3CMinimumSpeedUnitsU3Ek__BackingField_29(float value)
	{
		___U3CMinimumSpeedUnitsU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionThresholdU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CDirectionThresholdU3Ek__BackingField_30)); }
	inline float get_U3CDirectionThresholdU3Ek__BackingField_30() const { return ___U3CDirectionThresholdU3Ek__BackingField_30; }
	inline float* get_address_of_U3CDirectionThresholdU3Ek__BackingField_30() { return &___U3CDirectionThresholdU3Ek__BackingField_30; }
	inline void set_U3CDirectionThresholdU3Ek__BackingField_30(float value)
	{
		___U3CDirectionThresholdU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CEndDirectionU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CEndDirectionU3Ek__BackingField_31)); }
	inline int32_t get_U3CEndDirectionU3Ek__BackingField_31() const { return ___U3CEndDirectionU3Ek__BackingField_31; }
	inline int32_t* get_address_of_U3CEndDirectionU3Ek__BackingField_31() { return &___U3CEndDirectionU3Ek__BackingField_31; }
	inline void set_U3CEndDirectionU3Ek__BackingField_31(int32_t value)
	{
		___U3CEndDirectionU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CEndImmeditelyU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CEndImmeditelyU3Ek__BackingField_32)); }
	inline bool get_U3CEndImmeditelyU3Ek__BackingField_32() const { return ___U3CEndImmeditelyU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CEndImmeditelyU3Ek__BackingField_32() { return &___U3CEndImmeditelyU3Ek__BackingField_32; }
	inline void set_U3CEndImmeditelyU3Ek__BackingField_32(bool value)
	{
		___U3CEndImmeditelyU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CFailOnDirectionChangeU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CFailOnDirectionChangeU3Ek__BackingField_33)); }
	inline bool get_U3CFailOnDirectionChangeU3Ek__BackingField_33() const { return ___U3CFailOnDirectionChangeU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CFailOnDirectionChangeU3Ek__BackingField_33() { return &___U3CFailOnDirectionChangeU3Ek__BackingField_33; }
	inline void set_U3CFailOnDirectionChangeU3Ek__BackingField_33(bool value)
	{
		___U3CFailOnDirectionChangeU3Ek__BackingField_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEGESTURERECOGNIZER_T2328511861_H
#ifndef ROTATEGESTURERECOGNIZER_T4100246528_H
#define ROTATEGESTURERECOGNIZER_T4100246528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.RotateGestureRecognizer
struct  RotateGestureRecognizer_t4100246528  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.RotateGestureRecognizer::startAngle
	float ___startAngle_27;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::previousAngle
	float ___previousAngle_28;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<AngleThreshold>k__BackingField
	float ___U3CAngleThresholdU3Ek__BackingField_29;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_30;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<RotationRadians>k__BackingField
	float ___U3CRotationRadiansU3Ek__BackingField_31;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<RotationRadiansDelta>k__BackingField
	float ___U3CRotationRadiansDeltaU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_startAngle_27() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___startAngle_27)); }
	inline float get_startAngle_27() const { return ___startAngle_27; }
	inline float* get_address_of_startAngle_27() { return &___startAngle_27; }
	inline void set_startAngle_27(float value)
	{
		___startAngle_27 = value;
	}

	inline static int32_t get_offset_of_previousAngle_28() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___previousAngle_28)); }
	inline float get_previousAngle_28() const { return ___previousAngle_28; }
	inline float* get_address_of_previousAngle_28() { return &___previousAngle_28; }
	inline void set_previousAngle_28(float value)
	{
		___previousAngle_28 = value;
	}

	inline static int32_t get_offset_of_U3CAngleThresholdU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CAngleThresholdU3Ek__BackingField_29)); }
	inline float get_U3CAngleThresholdU3Ek__BackingField_29() const { return ___U3CAngleThresholdU3Ek__BackingField_29; }
	inline float* get_address_of_U3CAngleThresholdU3Ek__BackingField_29() { return &___U3CAngleThresholdU3Ek__BackingField_29; }
	inline void set_U3CAngleThresholdU3Ek__BackingField_29(float value)
	{
		___U3CAngleThresholdU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CThresholdUnitsU3Ek__BackingField_30)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_30() const { return ___U3CThresholdUnitsU3Ek__BackingField_30; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_30() { return &___U3CThresholdUnitsU3Ek__BackingField_30; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_30(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CRotationRadiansU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CRotationRadiansU3Ek__BackingField_31)); }
	inline float get_U3CRotationRadiansU3Ek__BackingField_31() const { return ___U3CRotationRadiansU3Ek__BackingField_31; }
	inline float* get_address_of_U3CRotationRadiansU3Ek__BackingField_31() { return &___U3CRotationRadiansU3Ek__BackingField_31; }
	inline void set_U3CRotationRadiansU3Ek__BackingField_31(float value)
	{
		___U3CRotationRadiansU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CRotationRadiansDeltaU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CRotationRadiansDeltaU3Ek__BackingField_32)); }
	inline float get_U3CRotationRadiansDeltaU3Ek__BackingField_32() const { return ___U3CRotationRadiansDeltaU3Ek__BackingField_32; }
	inline float* get_address_of_U3CRotationRadiansDeltaU3Ek__BackingField_32() { return &___U3CRotationRadiansDeltaU3Ek__BackingField_32; }
	inline void set_U3CRotationRadiansDeltaU3Ek__BackingField_32(float value)
	{
		___U3CRotationRadiansDeltaU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEGESTURERECOGNIZER_T4100246528_H
#ifndef ONETOUCHROTATEGESTURERECOGNIZER_T3272893959_H
#define ONETOUCHROTATEGESTURERECOGNIZER_T3272893959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.OneTouchRotateGestureRecognizer
struct  OneTouchRotateGestureRecognizer_t3272893959  : public RotateGestureRecognizer_t4100246528
{
public:
	// System.Single DigitalRubyShared.OneTouchRotateGestureRecognizer::AnglePointOverrideX
	float ___AnglePointOverrideX_33;
	// System.Single DigitalRubyShared.OneTouchRotateGestureRecognizer::AnglePointOverrideY
	float ___AnglePointOverrideY_34;

public:
	inline static int32_t get_offset_of_AnglePointOverrideX_33() { return static_cast<int32_t>(offsetof(OneTouchRotateGestureRecognizer_t3272893959, ___AnglePointOverrideX_33)); }
	inline float get_AnglePointOverrideX_33() const { return ___AnglePointOverrideX_33; }
	inline float* get_address_of_AnglePointOverrideX_33() { return &___AnglePointOverrideX_33; }
	inline void set_AnglePointOverrideX_33(float value)
	{
		___AnglePointOverrideX_33 = value;
	}

	inline static int32_t get_offset_of_AnglePointOverrideY_34() { return static_cast<int32_t>(offsetof(OneTouchRotateGestureRecognizer_t3272893959, ___AnglePointOverrideY_34)); }
	inline float get_AnglePointOverrideY_34() const { return ___AnglePointOverrideY_34; }
	inline float* get_address_of_AnglePointOverrideY_34() { return &___AnglePointOverrideY_34; }
	inline void set_AnglePointOverrideY_34(float value)
	{
		___AnglePointOverrideY_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONETOUCHROTATEGESTURERECOGNIZER_T3272893959_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEMOSCRIPTDPAD_T2734679325_H
#define DEMOSCRIPTDPAD_T2734679325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptDPad
struct  DemoScriptDPad_t2734679325  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersDPadScript DigitalRubyShared.DemoScriptDPad::DPadScript
	FingersDPadScript_t3801874975 * ___DPadScript_2;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptDPad::Mover
	GameObject_t1113636619 * ___Mover_3;
	// System.Single DigitalRubyShared.DemoScriptDPad::Speed
	float ___Speed_4;
	// System.Boolean DigitalRubyShared.DemoScriptDPad::MoveDPadToGestureStartLocation
	bool ___MoveDPadToGestureStartLocation_5;
	// UnityEngine.Vector3 DigitalRubyShared.DemoScriptDPad::startPos
	Vector3_t3722313464  ___startPos_6;

public:
	inline static int32_t get_offset_of_DPadScript_2() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___DPadScript_2)); }
	inline FingersDPadScript_t3801874975 * get_DPadScript_2() const { return ___DPadScript_2; }
	inline FingersDPadScript_t3801874975 ** get_address_of_DPadScript_2() { return &___DPadScript_2; }
	inline void set_DPadScript_2(FingersDPadScript_t3801874975 * value)
	{
		___DPadScript_2 = value;
		Il2CppCodeGenWriteBarrier((&___DPadScript_2), value);
	}

	inline static int32_t get_offset_of_Mover_3() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___Mover_3)); }
	inline GameObject_t1113636619 * get_Mover_3() const { return ___Mover_3; }
	inline GameObject_t1113636619 ** get_address_of_Mover_3() { return &___Mover_3; }
	inline void set_Mover_3(GameObject_t1113636619 * value)
	{
		___Mover_3 = value;
		Il2CppCodeGenWriteBarrier((&___Mover_3), value);
	}

	inline static int32_t get_offset_of_Speed_4() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___Speed_4)); }
	inline float get_Speed_4() const { return ___Speed_4; }
	inline float* get_address_of_Speed_4() { return &___Speed_4; }
	inline void set_Speed_4(float value)
	{
		___Speed_4 = value;
	}

	inline static int32_t get_offset_of_MoveDPadToGestureStartLocation_5() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___MoveDPadToGestureStartLocation_5)); }
	inline bool get_MoveDPadToGestureStartLocation_5() const { return ___MoveDPadToGestureStartLocation_5; }
	inline bool* get_address_of_MoveDPadToGestureStartLocation_5() { return &___MoveDPadToGestureStartLocation_5; }
	inline void set_MoveDPadToGestureStartLocation_5(bool value)
	{
		___MoveDPadToGestureStartLocation_5 = value;
	}

	inline static int32_t get_offset_of_startPos_6() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___startPos_6)); }
	inline Vector3_t3722313464  get_startPos_6() const { return ___startPos_6; }
	inline Vector3_t3722313464 * get_address_of_startPos_6() { return &___startPos_6; }
	inline void set_startPos_6(Vector3_t3722313464  value)
	{
		___startPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTDPAD_T2734679325_H
#ifndef FINGERSIMAGEAUTOMATIONSCRIPT_T4118243268_H
#define FINGERSIMAGEAUTOMATIONSCRIPT_T4118243268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersImageAutomationScript
struct  FingersImageAutomationScript_t4118243268  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersScript DigitalRubyShared.FingersImageAutomationScript::FingersScript
	FingersScript_t1857011421 * ___FingersScript_2;
	// UnityEngine.UI.RawImage DigitalRubyShared.FingersImageAutomationScript::Image
	RawImage_t3182918964 * ___Image_3;
	// DigitalRubyShared.ImageGestureRecognizer DigitalRubyShared.FingersImageAutomationScript::ImageGesture
	ImageGestureRecognizer_t4233185475 * ___ImageGesture_4;
	// UnityEngine.Material DigitalRubyShared.FingersImageAutomationScript::LineMaterial
	Material_t340375123 * ___LineMaterial_5;
	// UnityEngine.UI.Text DigitalRubyShared.FingersImageAutomationScript::MatchLabel
	Text_t1901882714 * ___MatchLabel_6;
	// UnityEngine.UI.InputField DigitalRubyShared.FingersImageAutomationScript::ScriptText
	InputField_t3762917431 * ___ScriptText_7;
	// DigitalRubyShared.ImageGestureImage DigitalRubyShared.FingersImageAutomationScript::<LastImage>k__BackingField
	ImageGestureImage_t2885596271 * ___U3CLastImageU3Ek__BackingField_8;
	// System.Collections.Generic.Dictionary`2<DigitalRubyShared.ImageGestureImage,System.String> DigitalRubyShared.FingersImageAutomationScript::RecognizableImages
	Dictionary_2_t3194170630 * ___RecognizableImages_9;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>> DigitalRubyShared.FingersImageAutomationScript::lineSet
	List_1_t805411711 * ___lineSet_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> DigitalRubyShared.FingersImageAutomationScript::currentPointList
	List_1_t3628304265 * ___currentPointList_11;

public:
	inline static int32_t get_offset_of_FingersScript_2() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___FingersScript_2)); }
	inline FingersScript_t1857011421 * get_FingersScript_2() const { return ___FingersScript_2; }
	inline FingersScript_t1857011421 ** get_address_of_FingersScript_2() { return &___FingersScript_2; }
	inline void set_FingersScript_2(FingersScript_t1857011421 * value)
	{
		___FingersScript_2 = value;
		Il2CppCodeGenWriteBarrier((&___FingersScript_2), value);
	}

	inline static int32_t get_offset_of_Image_3() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___Image_3)); }
	inline RawImage_t3182918964 * get_Image_3() const { return ___Image_3; }
	inline RawImage_t3182918964 ** get_address_of_Image_3() { return &___Image_3; }
	inline void set_Image_3(RawImage_t3182918964 * value)
	{
		___Image_3 = value;
		Il2CppCodeGenWriteBarrier((&___Image_3), value);
	}

	inline static int32_t get_offset_of_ImageGesture_4() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___ImageGesture_4)); }
	inline ImageGestureRecognizer_t4233185475 * get_ImageGesture_4() const { return ___ImageGesture_4; }
	inline ImageGestureRecognizer_t4233185475 ** get_address_of_ImageGesture_4() { return &___ImageGesture_4; }
	inline void set_ImageGesture_4(ImageGestureRecognizer_t4233185475 * value)
	{
		___ImageGesture_4 = value;
		Il2CppCodeGenWriteBarrier((&___ImageGesture_4), value);
	}

	inline static int32_t get_offset_of_LineMaterial_5() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___LineMaterial_5)); }
	inline Material_t340375123 * get_LineMaterial_5() const { return ___LineMaterial_5; }
	inline Material_t340375123 ** get_address_of_LineMaterial_5() { return &___LineMaterial_5; }
	inline void set_LineMaterial_5(Material_t340375123 * value)
	{
		___LineMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_5), value);
	}

	inline static int32_t get_offset_of_MatchLabel_6() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___MatchLabel_6)); }
	inline Text_t1901882714 * get_MatchLabel_6() const { return ___MatchLabel_6; }
	inline Text_t1901882714 ** get_address_of_MatchLabel_6() { return &___MatchLabel_6; }
	inline void set_MatchLabel_6(Text_t1901882714 * value)
	{
		___MatchLabel_6 = value;
		Il2CppCodeGenWriteBarrier((&___MatchLabel_6), value);
	}

	inline static int32_t get_offset_of_ScriptText_7() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___ScriptText_7)); }
	inline InputField_t3762917431 * get_ScriptText_7() const { return ___ScriptText_7; }
	inline InputField_t3762917431 ** get_address_of_ScriptText_7() { return &___ScriptText_7; }
	inline void set_ScriptText_7(InputField_t3762917431 * value)
	{
		___ScriptText_7 = value;
		Il2CppCodeGenWriteBarrier((&___ScriptText_7), value);
	}

	inline static int32_t get_offset_of_U3CLastImageU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___U3CLastImageU3Ek__BackingField_8)); }
	inline ImageGestureImage_t2885596271 * get_U3CLastImageU3Ek__BackingField_8() const { return ___U3CLastImageU3Ek__BackingField_8; }
	inline ImageGestureImage_t2885596271 ** get_address_of_U3CLastImageU3Ek__BackingField_8() { return &___U3CLastImageU3Ek__BackingField_8; }
	inline void set_U3CLastImageU3Ek__BackingField_8(ImageGestureImage_t2885596271 * value)
	{
		___U3CLastImageU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastImageU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_RecognizableImages_9() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___RecognizableImages_9)); }
	inline Dictionary_2_t3194170630 * get_RecognizableImages_9() const { return ___RecognizableImages_9; }
	inline Dictionary_2_t3194170630 ** get_address_of_RecognizableImages_9() { return &___RecognizableImages_9; }
	inline void set_RecognizableImages_9(Dictionary_2_t3194170630 * value)
	{
		___RecognizableImages_9 = value;
		Il2CppCodeGenWriteBarrier((&___RecognizableImages_9), value);
	}

	inline static int32_t get_offset_of_lineSet_10() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___lineSet_10)); }
	inline List_1_t805411711 * get_lineSet_10() const { return ___lineSet_10; }
	inline List_1_t805411711 ** get_address_of_lineSet_10() { return &___lineSet_10; }
	inline void set_lineSet_10(List_1_t805411711 * value)
	{
		___lineSet_10 = value;
		Il2CppCodeGenWriteBarrier((&___lineSet_10), value);
	}

	inline static int32_t get_offset_of_currentPointList_11() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___currentPointList_11)); }
	inline List_1_t3628304265 * get_currentPointList_11() const { return ___currentPointList_11; }
	inline List_1_t3628304265 ** get_address_of_currentPointList_11() { return &___currentPointList_11; }
	inline void set_currentPointList_11(List_1_t3628304265 * value)
	{
		___currentPointList_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentPointList_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSIMAGEAUTOMATIONSCRIPT_T4118243268_H
#ifndef DEMOSCRIPTPAN_T3102497188_H
#define DEMOSCRIPTPAN_T3102497188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptPan
struct  DemoScriptPan_t3102497188  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersScript DigitalRubyShared.DemoScriptPan::FingersScript
	FingersScript_t1857011421 * ___FingersScript_2;

public:
	inline static int32_t get_offset_of_FingersScript_2() { return static_cast<int32_t>(offsetof(DemoScriptPan_t3102497188, ___FingersScript_2)); }
	inline FingersScript_t1857011421 * get_FingersScript_2() const { return ___FingersScript_2; }
	inline FingersScript_t1857011421 ** get_address_of_FingersScript_2() { return &___FingersScript_2; }
	inline void set_FingersScript_2(FingersScript_t1857011421 * value)
	{
		___FingersScript_2 = value;
		Il2CppCodeGenWriteBarrier((&___FingersScript_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTPAN_T3102497188_H
#ifndef DEMOSCRIPTPLATFORMSPECIFICVIEW_T3383617572_H
#define DEMOSCRIPTPLATFORMSPECIFICVIEW_T3383617572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptPlatformSpecificView
struct  DemoScriptPlatformSpecificView_t3383617572  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptPlatformSpecificView::FingersScriptPrefab
	GameObject_t1113636619 * ___FingersScriptPrefab_2;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptPlatformSpecificView::LeftPanel
	GameObject_t1113636619 * ___LeftPanel_3;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptPlatformSpecificView::Cube
	GameObject_t1113636619 * ___Cube_4;

public:
	inline static int32_t get_offset_of_FingersScriptPrefab_2() { return static_cast<int32_t>(offsetof(DemoScriptPlatformSpecificView_t3383617572, ___FingersScriptPrefab_2)); }
	inline GameObject_t1113636619 * get_FingersScriptPrefab_2() const { return ___FingersScriptPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_FingersScriptPrefab_2() { return &___FingersScriptPrefab_2; }
	inline void set_FingersScriptPrefab_2(GameObject_t1113636619 * value)
	{
		___FingersScriptPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___FingersScriptPrefab_2), value);
	}

	inline static int32_t get_offset_of_LeftPanel_3() { return static_cast<int32_t>(offsetof(DemoScriptPlatformSpecificView_t3383617572, ___LeftPanel_3)); }
	inline GameObject_t1113636619 * get_LeftPanel_3() const { return ___LeftPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_LeftPanel_3() { return &___LeftPanel_3; }
	inline void set_LeftPanel_3(GameObject_t1113636619 * value)
	{
		___LeftPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___LeftPanel_3), value);
	}

	inline static int32_t get_offset_of_Cube_4() { return static_cast<int32_t>(offsetof(DemoScriptPlatformSpecificView_t3383617572, ___Cube_4)); }
	inline GameObject_t1113636619 * get_Cube_4() const { return ___Cube_4; }
	inline GameObject_t1113636619 ** get_address_of_Cube_4() { return &___Cube_4; }
	inline void set_Cube_4(GameObject_t1113636619 * value)
	{
		___Cube_4 = value;
		Il2CppCodeGenWriteBarrier((&___Cube_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTPLATFORMSPECIFICVIEW_T3383617572_H
#ifndef FINGERSDRAGDROPSCRIPT_T2960271979_H
#define FINGERSDRAGDROPSCRIPT_T2960271979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersDragDropScript
struct  FingersDragDropScript_t2960271979  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera DigitalRubyShared.FingersDragDropScript::Camera
	Camera_t4157153871 * ___Camera_2;
	// System.Boolean DigitalRubyShared.FingersDragDropScript::BringToFront
	bool ___BringToFront_3;
	// DigitalRubyShared.LongPressGestureRecognizer DigitalRubyShared.FingersDragDropScript::longPressGesture
	LongPressGestureRecognizer_t3980777482 * ___longPressGesture_4;
	// UnityEngine.Rigidbody2D DigitalRubyShared.FingersDragDropScript::rigidBody
	Rigidbody2D_t939494601 * ___rigidBody_5;
	// UnityEngine.SpriteRenderer DigitalRubyShared.FingersDragDropScript::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_6;
	// System.Int32 DigitalRubyShared.FingersDragDropScript::startSortOrder
	int32_t ___startSortOrder_7;
	// UnityEngine.Vector2 DigitalRubyShared.FingersDragDropScript::panStart
	Vector2_t2156229523  ___panStart_8;

public:
	inline static int32_t get_offset_of_Camera_2() { return static_cast<int32_t>(offsetof(FingersDragDropScript_t2960271979, ___Camera_2)); }
	inline Camera_t4157153871 * get_Camera_2() const { return ___Camera_2; }
	inline Camera_t4157153871 ** get_address_of_Camera_2() { return &___Camera_2; }
	inline void set_Camera_2(Camera_t4157153871 * value)
	{
		___Camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_2), value);
	}

	inline static int32_t get_offset_of_BringToFront_3() { return static_cast<int32_t>(offsetof(FingersDragDropScript_t2960271979, ___BringToFront_3)); }
	inline bool get_BringToFront_3() const { return ___BringToFront_3; }
	inline bool* get_address_of_BringToFront_3() { return &___BringToFront_3; }
	inline void set_BringToFront_3(bool value)
	{
		___BringToFront_3 = value;
	}

	inline static int32_t get_offset_of_longPressGesture_4() { return static_cast<int32_t>(offsetof(FingersDragDropScript_t2960271979, ___longPressGesture_4)); }
	inline LongPressGestureRecognizer_t3980777482 * get_longPressGesture_4() const { return ___longPressGesture_4; }
	inline LongPressGestureRecognizer_t3980777482 ** get_address_of_longPressGesture_4() { return &___longPressGesture_4; }
	inline void set_longPressGesture_4(LongPressGestureRecognizer_t3980777482 * value)
	{
		___longPressGesture_4 = value;
		Il2CppCodeGenWriteBarrier((&___longPressGesture_4), value);
	}

	inline static int32_t get_offset_of_rigidBody_5() { return static_cast<int32_t>(offsetof(FingersDragDropScript_t2960271979, ___rigidBody_5)); }
	inline Rigidbody2D_t939494601 * get_rigidBody_5() const { return ___rigidBody_5; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidBody_5() { return &___rigidBody_5; }
	inline void set_rigidBody_5(Rigidbody2D_t939494601 * value)
	{
		___rigidBody_5 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_5), value);
	}

	inline static int32_t get_offset_of_spriteRenderer_6() { return static_cast<int32_t>(offsetof(FingersDragDropScript_t2960271979, ___spriteRenderer_6)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_6() const { return ___spriteRenderer_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_6() { return &___spriteRenderer_6; }
	inline void set_spriteRenderer_6(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_6), value);
	}

	inline static int32_t get_offset_of_startSortOrder_7() { return static_cast<int32_t>(offsetof(FingersDragDropScript_t2960271979, ___startSortOrder_7)); }
	inline int32_t get_startSortOrder_7() const { return ___startSortOrder_7; }
	inline int32_t* get_address_of_startSortOrder_7() { return &___startSortOrder_7; }
	inline void set_startSortOrder_7(int32_t value)
	{
		___startSortOrder_7 = value;
	}

	inline static int32_t get_offset_of_panStart_8() { return static_cast<int32_t>(offsetof(FingersDragDropScript_t2960271979, ___panStart_8)); }
	inline Vector2_t2156229523  get_panStart_8() const { return ___panStart_8; }
	inline Vector2_t2156229523 * get_address_of_panStart_8() { return &___panStart_8; }
	inline void set_panStart_8(Vector2_t2156229523  value)
	{
		___panStart_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSDRAGDROPSCRIPT_T2960271979_H
#ifndef LOADPAGE_T2941907970_H
#define LOADPAGE_T2941907970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadPage
struct  LoadPage_t2941907970  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LoadPage::vibrated
	bool ___vibrated_2;

public:
	inline static int32_t get_offset_of_vibrated_2() { return static_cast<int32_t>(offsetof(LoadPage_t2941907970, ___vibrated_2)); }
	inline bool get_vibrated_2() const { return ___vibrated_2; }
	inline bool* get_address_of_vibrated_2() { return &___vibrated_2; }
	inline void set_vibrated_2(bool value)
	{
		___vibrated_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADPAGE_T2941907970_H
#ifndef FINGERSGESTURES_T1283504238_H
#define FINGERSGESTURES_T1283504238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingersGestures
struct  FingersGestures_t1283504238  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 FingersGestures::PanMinimumTouchCount
	int32_t ___PanMinimumTouchCount_2;
	// UnityEngine.GameObject FingersGestures::FingersScriptPrefab
	GameObject_t1113636619 * ___FingersScriptPrefab_3;
	// DigitalRubyShared.PanGestureRecognizer FingersGestures::panGesture
	PanGestureRecognizer_t195762396 * ___panGesture_4;
	// DigitalRubyShared.ScaleGestureRecognizer FingersGestures::scaleGesture
	ScaleGestureRecognizer_t1137887245 * ___scaleGesture_5;

public:
	inline static int32_t get_offset_of_PanMinimumTouchCount_2() { return static_cast<int32_t>(offsetof(FingersGestures_t1283504238, ___PanMinimumTouchCount_2)); }
	inline int32_t get_PanMinimumTouchCount_2() const { return ___PanMinimumTouchCount_2; }
	inline int32_t* get_address_of_PanMinimumTouchCount_2() { return &___PanMinimumTouchCount_2; }
	inline void set_PanMinimumTouchCount_2(int32_t value)
	{
		___PanMinimumTouchCount_2 = value;
	}

	inline static int32_t get_offset_of_FingersScriptPrefab_3() { return static_cast<int32_t>(offsetof(FingersGestures_t1283504238, ___FingersScriptPrefab_3)); }
	inline GameObject_t1113636619 * get_FingersScriptPrefab_3() const { return ___FingersScriptPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_FingersScriptPrefab_3() { return &___FingersScriptPrefab_3; }
	inline void set_FingersScriptPrefab_3(GameObject_t1113636619 * value)
	{
		___FingersScriptPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___FingersScriptPrefab_3), value);
	}

	inline static int32_t get_offset_of_panGesture_4() { return static_cast<int32_t>(offsetof(FingersGestures_t1283504238, ___panGesture_4)); }
	inline PanGestureRecognizer_t195762396 * get_panGesture_4() const { return ___panGesture_4; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_panGesture_4() { return &___panGesture_4; }
	inline void set_panGesture_4(PanGestureRecognizer_t195762396 * value)
	{
		___panGesture_4 = value;
		Il2CppCodeGenWriteBarrier((&___panGesture_4), value);
	}

	inline static int32_t get_offset_of_scaleGesture_5() { return static_cast<int32_t>(offsetof(FingersGestures_t1283504238, ___scaleGesture_5)); }
	inline ScaleGestureRecognizer_t1137887245 * get_scaleGesture_5() const { return ___scaleGesture_5; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_scaleGesture_5() { return &___scaleGesture_5; }
	inline void set_scaleGesture_5(ScaleGestureRecognizer_t1137887245 * value)
	{
		___scaleGesture_5 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSGESTURES_T1283504238_H
#ifndef DEMOSCRIPTZOOMABLESCROLLVIEW_T163084534_H
#define DEMOSCRIPTZOOMABLESCROLLVIEW_T163084534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptZoomableScrollView
struct  DemoScriptZoomableScrollView_t163084534  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.ScrollRect DigitalRubyShared.DemoScriptZoomableScrollView::ScrollView
	ScrollRect_t4137855814 * ___ScrollView_2;
	// UnityEngine.Canvas DigitalRubyShared.DemoScriptZoomableScrollView::Canvas
	Canvas_t3310196443 * ___Canvas_3;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::ScaleFocusMoveThresholdUnits
	float ___ScaleFocusMoveThresholdUnits_4;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::scaleStart
	float ___scaleStart_5;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::scaleEnd
	float ___scaleEnd_6;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::scaleTime
	float ___scaleTime_7;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::elapsedScaleTime
	float ___elapsedScaleTime_8;
	// UnityEngine.Vector2 DigitalRubyShared.DemoScriptZoomableScrollView::scalePosStart
	Vector2_t2156229523  ___scalePosStart_9;
	// UnityEngine.Vector2 DigitalRubyShared.DemoScriptZoomableScrollView::scalePosEnd
	Vector2_t2156229523  ___scalePosEnd_10;

public:
	inline static int32_t get_offset_of_ScrollView_2() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___ScrollView_2)); }
	inline ScrollRect_t4137855814 * get_ScrollView_2() const { return ___ScrollView_2; }
	inline ScrollRect_t4137855814 ** get_address_of_ScrollView_2() { return &___ScrollView_2; }
	inline void set_ScrollView_2(ScrollRect_t4137855814 * value)
	{
		___ScrollView_2 = value;
		Il2CppCodeGenWriteBarrier((&___ScrollView_2), value);
	}

	inline static int32_t get_offset_of_Canvas_3() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___Canvas_3)); }
	inline Canvas_t3310196443 * get_Canvas_3() const { return ___Canvas_3; }
	inline Canvas_t3310196443 ** get_address_of_Canvas_3() { return &___Canvas_3; }
	inline void set_Canvas_3(Canvas_t3310196443 * value)
	{
		___Canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___Canvas_3), value);
	}

	inline static int32_t get_offset_of_ScaleFocusMoveThresholdUnits_4() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___ScaleFocusMoveThresholdUnits_4)); }
	inline float get_ScaleFocusMoveThresholdUnits_4() const { return ___ScaleFocusMoveThresholdUnits_4; }
	inline float* get_address_of_ScaleFocusMoveThresholdUnits_4() { return &___ScaleFocusMoveThresholdUnits_4; }
	inline void set_ScaleFocusMoveThresholdUnits_4(float value)
	{
		___ScaleFocusMoveThresholdUnits_4 = value;
	}

	inline static int32_t get_offset_of_scaleStart_5() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scaleStart_5)); }
	inline float get_scaleStart_5() const { return ___scaleStart_5; }
	inline float* get_address_of_scaleStart_5() { return &___scaleStart_5; }
	inline void set_scaleStart_5(float value)
	{
		___scaleStart_5 = value;
	}

	inline static int32_t get_offset_of_scaleEnd_6() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scaleEnd_6)); }
	inline float get_scaleEnd_6() const { return ___scaleEnd_6; }
	inline float* get_address_of_scaleEnd_6() { return &___scaleEnd_6; }
	inline void set_scaleEnd_6(float value)
	{
		___scaleEnd_6 = value;
	}

	inline static int32_t get_offset_of_scaleTime_7() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scaleTime_7)); }
	inline float get_scaleTime_7() const { return ___scaleTime_7; }
	inline float* get_address_of_scaleTime_7() { return &___scaleTime_7; }
	inline void set_scaleTime_7(float value)
	{
		___scaleTime_7 = value;
	}

	inline static int32_t get_offset_of_elapsedScaleTime_8() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___elapsedScaleTime_8)); }
	inline float get_elapsedScaleTime_8() const { return ___elapsedScaleTime_8; }
	inline float* get_address_of_elapsedScaleTime_8() { return &___elapsedScaleTime_8; }
	inline void set_elapsedScaleTime_8(float value)
	{
		___elapsedScaleTime_8 = value;
	}

	inline static int32_t get_offset_of_scalePosStart_9() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scalePosStart_9)); }
	inline Vector2_t2156229523  get_scalePosStart_9() const { return ___scalePosStart_9; }
	inline Vector2_t2156229523 * get_address_of_scalePosStart_9() { return &___scalePosStart_9; }
	inline void set_scalePosStart_9(Vector2_t2156229523  value)
	{
		___scalePosStart_9 = value;
	}

	inline static int32_t get_offset_of_scalePosEnd_10() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scalePosEnd_10)); }
	inline Vector2_t2156229523  get_scalePosEnd_10() const { return ___scalePosEnd_10; }
	inline Vector2_t2156229523 * get_address_of_scalePosEnd_10() { return &___scalePosEnd_10; }
	inline void set_scalePosEnd_10(Vector2_t2156229523  value)
	{
		___scalePosEnd_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTZOOMABLESCROLLVIEW_T163084534_H
#ifndef DEMOSCRIPTZOOMPANCAMERA_T3831420416_H
#define DEMOSCRIPTZOOMPANCAMERA_T3831420416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptZoomPanCamera
struct  DemoScriptZoomPanCamera_t3831420416  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.DemoScriptZoomPanCamera::scaleGesture
	ScaleGestureRecognizer_t1137887245 * ___scaleGesture_2;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.DemoScriptZoomPanCamera::panGesture
	PanGestureRecognizer_t195762396 * ___panGesture_3;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScriptZoomPanCamera::tapGesture
	TapGestureRecognizer_t3178883670 * ___tapGesture_4;
	// UnityEngine.Vector3 DigitalRubyShared.DemoScriptZoomPanCamera::cameraAnimationTargetPosition
	Vector3_t3722313464  ___cameraAnimationTargetPosition_5;

public:
	inline static int32_t get_offset_of_scaleGesture_2() { return static_cast<int32_t>(offsetof(DemoScriptZoomPanCamera_t3831420416, ___scaleGesture_2)); }
	inline ScaleGestureRecognizer_t1137887245 * get_scaleGesture_2() const { return ___scaleGesture_2; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_scaleGesture_2() { return &___scaleGesture_2; }
	inline void set_scaleGesture_2(ScaleGestureRecognizer_t1137887245 * value)
	{
		___scaleGesture_2 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_2), value);
	}

	inline static int32_t get_offset_of_panGesture_3() { return static_cast<int32_t>(offsetof(DemoScriptZoomPanCamera_t3831420416, ___panGesture_3)); }
	inline PanGestureRecognizer_t195762396 * get_panGesture_3() const { return ___panGesture_3; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_panGesture_3() { return &___panGesture_3; }
	inline void set_panGesture_3(PanGestureRecognizer_t195762396 * value)
	{
		___panGesture_3 = value;
		Il2CppCodeGenWriteBarrier((&___panGesture_3), value);
	}

	inline static int32_t get_offset_of_tapGesture_4() { return static_cast<int32_t>(offsetof(DemoScriptZoomPanCamera_t3831420416, ___tapGesture_4)); }
	inline TapGestureRecognizer_t3178883670 * get_tapGesture_4() const { return ___tapGesture_4; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tapGesture_4() { return &___tapGesture_4; }
	inline void set_tapGesture_4(TapGestureRecognizer_t3178883670 * value)
	{
		___tapGesture_4 = value;
		Il2CppCodeGenWriteBarrier((&___tapGesture_4), value);
	}

	inline static int32_t get_offset_of_cameraAnimationTargetPosition_5() { return static_cast<int32_t>(offsetof(DemoScriptZoomPanCamera_t3831420416, ___cameraAnimationTargetPosition_5)); }
	inline Vector3_t3722313464  get_cameraAnimationTargetPosition_5() const { return ___cameraAnimationTargetPosition_5; }
	inline Vector3_t3722313464 * get_address_of_cameraAnimationTargetPosition_5() { return &___cameraAnimationTargetPosition_5; }
	inline void set_cameraAnimationTargetPosition_5(Vector3_t3722313464  value)
	{
		___cameraAnimationTargetPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTZOOMPANCAMERA_T3831420416_H
#ifndef FINGERSDPADSCRIPT_T3801874975_H
#define FINGERSDPADSCRIPT_T3801874975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersDPadScript
struct  FingersDPadScript_t3801874975  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadBackgroundImage
	Image_t2670269651 * ___DPadBackgroundImage_2;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadUpImageSelected
	Image_t2670269651 * ___DPadUpImageSelected_3;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadRightImageSelected
	Image_t2670269651 * ___DPadRightImageSelected_4;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadDownImageSelected
	Image_t2670269651 * ___DPadDownImageSelected_5;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadLeftImageSelected
	Image_t2670269651 * ___DPadLeftImageSelected_6;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadCenterImageSelected
	Image_t2670269651 * ___DPadCenterImageSelected_7;
	// System.Single DigitalRubyShared.FingersDPadScript::TouchRadiusInUnits
	float ___TouchRadiusInUnits_8;
	// UnityEngine.Collider2D[] DigitalRubyShared.FingersDPadScript::overlap
	Collider2DU5BU5D_t1693969295* ___overlap_9;
	// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.TapGestureRecognizer> DigitalRubyShared.FingersDPadScript::DPadItemTapped
	Action_3_t3049541042 * ___DPadItemTapped_10;
	// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.PanGestureRecognizer> DigitalRubyShared.FingersDPadScript::DPadItemPanned
	Action_3_t66419768 * ___DPadItemPanned_11;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersDPadScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_12;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.FingersDPadScript::<TapGesture>k__BackingField
	TapGestureRecognizer_t3178883670 * ___U3CTapGestureU3Ek__BackingField_13;
	// System.Boolean DigitalRubyShared.FingersDPadScript::<MoveDPadToGestureStartLocation>k__BackingField
	bool ___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_DPadBackgroundImage_2() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadBackgroundImage_2)); }
	inline Image_t2670269651 * get_DPadBackgroundImage_2() const { return ___DPadBackgroundImage_2; }
	inline Image_t2670269651 ** get_address_of_DPadBackgroundImage_2() { return &___DPadBackgroundImage_2; }
	inline void set_DPadBackgroundImage_2(Image_t2670269651 * value)
	{
		___DPadBackgroundImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___DPadBackgroundImage_2), value);
	}

	inline static int32_t get_offset_of_DPadUpImageSelected_3() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadUpImageSelected_3)); }
	inline Image_t2670269651 * get_DPadUpImageSelected_3() const { return ___DPadUpImageSelected_3; }
	inline Image_t2670269651 ** get_address_of_DPadUpImageSelected_3() { return &___DPadUpImageSelected_3; }
	inline void set_DPadUpImageSelected_3(Image_t2670269651 * value)
	{
		___DPadUpImageSelected_3 = value;
		Il2CppCodeGenWriteBarrier((&___DPadUpImageSelected_3), value);
	}

	inline static int32_t get_offset_of_DPadRightImageSelected_4() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadRightImageSelected_4)); }
	inline Image_t2670269651 * get_DPadRightImageSelected_4() const { return ___DPadRightImageSelected_4; }
	inline Image_t2670269651 ** get_address_of_DPadRightImageSelected_4() { return &___DPadRightImageSelected_4; }
	inline void set_DPadRightImageSelected_4(Image_t2670269651 * value)
	{
		___DPadRightImageSelected_4 = value;
		Il2CppCodeGenWriteBarrier((&___DPadRightImageSelected_4), value);
	}

	inline static int32_t get_offset_of_DPadDownImageSelected_5() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadDownImageSelected_5)); }
	inline Image_t2670269651 * get_DPadDownImageSelected_5() const { return ___DPadDownImageSelected_5; }
	inline Image_t2670269651 ** get_address_of_DPadDownImageSelected_5() { return &___DPadDownImageSelected_5; }
	inline void set_DPadDownImageSelected_5(Image_t2670269651 * value)
	{
		___DPadDownImageSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___DPadDownImageSelected_5), value);
	}

	inline static int32_t get_offset_of_DPadLeftImageSelected_6() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadLeftImageSelected_6)); }
	inline Image_t2670269651 * get_DPadLeftImageSelected_6() const { return ___DPadLeftImageSelected_6; }
	inline Image_t2670269651 ** get_address_of_DPadLeftImageSelected_6() { return &___DPadLeftImageSelected_6; }
	inline void set_DPadLeftImageSelected_6(Image_t2670269651 * value)
	{
		___DPadLeftImageSelected_6 = value;
		Il2CppCodeGenWriteBarrier((&___DPadLeftImageSelected_6), value);
	}

	inline static int32_t get_offset_of_DPadCenterImageSelected_7() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadCenterImageSelected_7)); }
	inline Image_t2670269651 * get_DPadCenterImageSelected_7() const { return ___DPadCenterImageSelected_7; }
	inline Image_t2670269651 ** get_address_of_DPadCenterImageSelected_7() { return &___DPadCenterImageSelected_7; }
	inline void set_DPadCenterImageSelected_7(Image_t2670269651 * value)
	{
		___DPadCenterImageSelected_7 = value;
		Il2CppCodeGenWriteBarrier((&___DPadCenterImageSelected_7), value);
	}

	inline static int32_t get_offset_of_TouchRadiusInUnits_8() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___TouchRadiusInUnits_8)); }
	inline float get_TouchRadiusInUnits_8() const { return ___TouchRadiusInUnits_8; }
	inline float* get_address_of_TouchRadiusInUnits_8() { return &___TouchRadiusInUnits_8; }
	inline void set_TouchRadiusInUnits_8(float value)
	{
		___TouchRadiusInUnits_8 = value;
	}

	inline static int32_t get_offset_of_overlap_9() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___overlap_9)); }
	inline Collider2DU5BU5D_t1693969295* get_overlap_9() const { return ___overlap_9; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_overlap_9() { return &___overlap_9; }
	inline void set_overlap_9(Collider2DU5BU5D_t1693969295* value)
	{
		___overlap_9 = value;
		Il2CppCodeGenWriteBarrier((&___overlap_9), value);
	}

	inline static int32_t get_offset_of_DPadItemTapped_10() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadItemTapped_10)); }
	inline Action_3_t3049541042 * get_DPadItemTapped_10() const { return ___DPadItemTapped_10; }
	inline Action_3_t3049541042 ** get_address_of_DPadItemTapped_10() { return &___DPadItemTapped_10; }
	inline void set_DPadItemTapped_10(Action_3_t3049541042 * value)
	{
		___DPadItemTapped_10 = value;
		Il2CppCodeGenWriteBarrier((&___DPadItemTapped_10), value);
	}

	inline static int32_t get_offset_of_DPadItemPanned_11() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadItemPanned_11)); }
	inline Action_3_t66419768 * get_DPadItemPanned_11() const { return ___DPadItemPanned_11; }
	inline Action_3_t66419768 ** get_address_of_DPadItemPanned_11() { return &___DPadItemPanned_11; }
	inline void set_DPadItemPanned_11(Action_3_t66419768 * value)
	{
		___DPadItemPanned_11 = value;
		Il2CppCodeGenWriteBarrier((&___DPadItemPanned_11), value);
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___U3CPanGestureU3Ek__BackingField_12)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_12() const { return ___U3CPanGestureU3Ek__BackingField_12; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_12() { return &___U3CPanGestureU3Ek__BackingField_12; }
	inline void set_U3CPanGestureU3Ek__BackingField_12(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CTapGestureU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___U3CTapGestureU3Ek__BackingField_13)); }
	inline TapGestureRecognizer_t3178883670 * get_U3CTapGestureU3Ek__BackingField_13() const { return ___U3CTapGestureU3Ek__BackingField_13; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_U3CTapGestureU3Ek__BackingField_13() { return &___U3CTapGestureU3Ek__BackingField_13; }
	inline void set_U3CTapGestureU3Ek__BackingField_13(TapGestureRecognizer_t3178883670 * value)
	{
		___U3CTapGestureU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTapGestureU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14)); }
	inline bool get_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14() const { return ___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14() { return &___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14; }
	inline void set_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14(bool value)
	{
		___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSDPADSCRIPT_T3801874975_H
#ifndef DEMOSCRIPTONEFINGER_T2754275351_H
#define DEMOSCRIPTONEFINGER_T2754275351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptOneFinger
struct  DemoScriptOneFinger_t2754275351  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptOneFinger::RotateIcon
	GameObject_t1113636619 * ___RotateIcon_2;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptOneFinger::ScaleIcon
	GameObject_t1113636619 * ___ScaleIcon_3;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptOneFinger::Earth
	GameObject_t1113636619 * ___Earth_4;
	// DigitalRubyShared.FingersScript DigitalRubyShared.DemoScriptOneFinger::FingerScript
	FingersScript_t1857011421 * ___FingerScript_5;
	// DigitalRubyShared.OneTouchRotateGestureRecognizer DigitalRubyShared.DemoScriptOneFinger::rotationGesture
	OneTouchRotateGestureRecognizer_t3272893959 * ___rotationGesture_6;
	// DigitalRubyShared.OneTouchScaleGestureRecognizer DigitalRubyShared.DemoScriptOneFinger::scaleGesture
	OneTouchScaleGestureRecognizer_t1313669683 * ___scaleGesture_7;

public:
	inline static int32_t get_offset_of_RotateIcon_2() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___RotateIcon_2)); }
	inline GameObject_t1113636619 * get_RotateIcon_2() const { return ___RotateIcon_2; }
	inline GameObject_t1113636619 ** get_address_of_RotateIcon_2() { return &___RotateIcon_2; }
	inline void set_RotateIcon_2(GameObject_t1113636619 * value)
	{
		___RotateIcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___RotateIcon_2), value);
	}

	inline static int32_t get_offset_of_ScaleIcon_3() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___ScaleIcon_3)); }
	inline GameObject_t1113636619 * get_ScaleIcon_3() const { return ___ScaleIcon_3; }
	inline GameObject_t1113636619 ** get_address_of_ScaleIcon_3() { return &___ScaleIcon_3; }
	inline void set_ScaleIcon_3(GameObject_t1113636619 * value)
	{
		___ScaleIcon_3 = value;
		Il2CppCodeGenWriteBarrier((&___ScaleIcon_3), value);
	}

	inline static int32_t get_offset_of_Earth_4() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___Earth_4)); }
	inline GameObject_t1113636619 * get_Earth_4() const { return ___Earth_4; }
	inline GameObject_t1113636619 ** get_address_of_Earth_4() { return &___Earth_4; }
	inline void set_Earth_4(GameObject_t1113636619 * value)
	{
		___Earth_4 = value;
		Il2CppCodeGenWriteBarrier((&___Earth_4), value);
	}

	inline static int32_t get_offset_of_FingerScript_5() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___FingerScript_5)); }
	inline FingersScript_t1857011421 * get_FingerScript_5() const { return ___FingerScript_5; }
	inline FingersScript_t1857011421 ** get_address_of_FingerScript_5() { return &___FingerScript_5; }
	inline void set_FingerScript_5(FingersScript_t1857011421 * value)
	{
		___FingerScript_5 = value;
		Il2CppCodeGenWriteBarrier((&___FingerScript_5), value);
	}

	inline static int32_t get_offset_of_rotationGesture_6() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___rotationGesture_6)); }
	inline OneTouchRotateGestureRecognizer_t3272893959 * get_rotationGesture_6() const { return ___rotationGesture_6; }
	inline OneTouchRotateGestureRecognizer_t3272893959 ** get_address_of_rotationGesture_6() { return &___rotationGesture_6; }
	inline void set_rotationGesture_6(OneTouchRotateGestureRecognizer_t3272893959 * value)
	{
		___rotationGesture_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotationGesture_6), value);
	}

	inline static int32_t get_offset_of_scaleGesture_7() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___scaleGesture_7)); }
	inline OneTouchScaleGestureRecognizer_t1313669683 * get_scaleGesture_7() const { return ___scaleGesture_7; }
	inline OneTouchScaleGestureRecognizer_t1313669683 ** get_address_of_scaleGesture_7() { return &___scaleGesture_7; }
	inline void set_scaleGesture_7(OneTouchScaleGestureRecognizer_t1313669683 * value)
	{
		___scaleGesture_7 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTONEFINGER_T2754275351_H
#ifndef DEMOSCRIPT3DORBIT_T3440477113_H
#define DEMOSCRIPT3DORBIT_T3440477113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScript3DOrbit
struct  DemoScript3DOrbit_t3440477113  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT3DORBIT_T3440477113_H
#ifndef FINGERSJOYSTICKSCRIPT_T2468414040_H
#define FINGERSJOYSTICKSCRIPT_T2468414040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersJoystickScript
struct  FingersJoystickScript_t2468414040  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image DigitalRubyShared.FingersJoystickScript::JoystickImage
	Image_t2670269651 * ___JoystickImage_2;
	// System.Single DigitalRubyShared.FingersJoystickScript::JoystickPower
	float ___JoystickPower_3;
	// System.Single DigitalRubyShared.FingersJoystickScript::MaxExtentPercent
	float ___MaxExtentPercent_4;
	// UnityEngine.Vector2 DigitalRubyShared.FingersJoystickScript::startCenter
	Vector2_t2156229523  ___startCenter_5;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersJoystickScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_6;
	// System.Action`2<DigitalRubyShared.FingersJoystickScript,UnityEngine.Vector2> DigitalRubyShared.FingersJoystickScript::JoystickExecuted
	Action_2_t1565375025 * ___JoystickExecuted_7;
	// System.Boolean DigitalRubyShared.FingersJoystickScript::<MoveJoystickToGestureStartLocation>k__BackingField
	bool ___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_JoystickImage_2() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___JoystickImage_2)); }
	inline Image_t2670269651 * get_JoystickImage_2() const { return ___JoystickImage_2; }
	inline Image_t2670269651 ** get_address_of_JoystickImage_2() { return &___JoystickImage_2; }
	inline void set_JoystickImage_2(Image_t2670269651 * value)
	{
		___JoystickImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickImage_2), value);
	}

	inline static int32_t get_offset_of_JoystickPower_3() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___JoystickPower_3)); }
	inline float get_JoystickPower_3() const { return ___JoystickPower_3; }
	inline float* get_address_of_JoystickPower_3() { return &___JoystickPower_3; }
	inline void set_JoystickPower_3(float value)
	{
		___JoystickPower_3 = value;
	}

	inline static int32_t get_offset_of_MaxExtentPercent_4() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___MaxExtentPercent_4)); }
	inline float get_MaxExtentPercent_4() const { return ___MaxExtentPercent_4; }
	inline float* get_address_of_MaxExtentPercent_4() { return &___MaxExtentPercent_4; }
	inline void set_MaxExtentPercent_4(float value)
	{
		___MaxExtentPercent_4 = value;
	}

	inline static int32_t get_offset_of_startCenter_5() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___startCenter_5)); }
	inline Vector2_t2156229523  get_startCenter_5() const { return ___startCenter_5; }
	inline Vector2_t2156229523 * get_address_of_startCenter_5() { return &___startCenter_5; }
	inline void set_startCenter_5(Vector2_t2156229523  value)
	{
		___startCenter_5 = value;
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___U3CPanGestureU3Ek__BackingField_6)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_6() const { return ___U3CPanGestureU3Ek__BackingField_6; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_6() { return &___U3CPanGestureU3Ek__BackingField_6; }
	inline void set_U3CPanGestureU3Ek__BackingField_6(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_JoystickExecuted_7() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___JoystickExecuted_7)); }
	inline Action_2_t1565375025 * get_JoystickExecuted_7() const { return ___JoystickExecuted_7; }
	inline Action_2_t1565375025 ** get_address_of_JoystickExecuted_7() { return &___JoystickExecuted_7; }
	inline void set_JoystickExecuted_7(Action_2_t1565375025 * value)
	{
		___JoystickExecuted_7 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickExecuted_7), value);
	}

	inline static int32_t get_offset_of_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8)); }
	inline bool get_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8() const { return ___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8() { return &___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8; }
	inline void set_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8(bool value)
	{
		___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSJOYSTICKSCRIPT_T2468414040_H
#ifndef DEMOSCRIPT_T2141982657_H
#define DEMOSCRIPT_T2141982657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScript
struct  DemoScript_t2141982657  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScript::FingersScriptPrefab
	GameObject_t1113636619 * ___FingersScriptPrefab_2;
	// UnityEngine.GameObject DigitalRubyShared.DemoScript::Earth
	GameObject_t1113636619 * ___Earth_3;
	// UnityEngine.UI.Text DigitalRubyShared.DemoScript::dpiLabel
	Text_t1901882714 * ___dpiLabel_4;
	// UnityEngine.UI.Text DigitalRubyShared.DemoScript::bottomLabel
	Text_t1901882714 * ___bottomLabel_5;
	// UnityEngine.GameObject DigitalRubyShared.DemoScript::AsteroidPrefab
	GameObject_t1113636619 * ___AsteroidPrefab_6;
	// UnityEngine.Material DigitalRubyShared.DemoScript::LineMaterial
	Material_t340375123 * ___LineMaterial_7;
	// UnityEngine.Sprite[] DigitalRubyShared.DemoScript::asteroids
	SpriteU5BU5D_t2581906349* ___asteroids_8;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScript::tapGesture
	TapGestureRecognizer_t3178883670 * ___tapGesture_9;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScript::doubleTapGesture
	TapGestureRecognizer_t3178883670 * ___doubleTapGesture_10;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScript::tripleTapGesture
	TapGestureRecognizer_t3178883670 * ___tripleTapGesture_11;
	// DigitalRubyShared.SwipeGestureRecognizer DigitalRubyShared.DemoScript::swipeGesture
	SwipeGestureRecognizer_t2328511861 * ___swipeGesture_12;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.DemoScript::panGesture
	PanGestureRecognizer_t195762396 * ___panGesture_13;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.DemoScript::scaleGesture
	ScaleGestureRecognizer_t1137887245 * ___scaleGesture_14;
	// DigitalRubyShared.RotateGestureRecognizer DigitalRubyShared.DemoScript::rotateGesture
	RotateGestureRecognizer_t4100246528 * ___rotateGesture_15;
	// DigitalRubyShared.LongPressGestureRecognizer DigitalRubyShared.DemoScript::longPressGesture
	LongPressGestureRecognizer_t3980777482 * ___longPressGesture_16;
	// System.Single DigitalRubyShared.DemoScript::nextAsteroid
	float ___nextAsteroid_17;
	// UnityEngine.GameObject DigitalRubyShared.DemoScript::draggingAsteroid
	GameObject_t1113636619 * ___draggingAsteroid_18;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DigitalRubyShared.DemoScript::swipeLines
	List_1_t899420910 * ___swipeLines_19;

public:
	inline static int32_t get_offset_of_FingersScriptPrefab_2() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___FingersScriptPrefab_2)); }
	inline GameObject_t1113636619 * get_FingersScriptPrefab_2() const { return ___FingersScriptPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_FingersScriptPrefab_2() { return &___FingersScriptPrefab_2; }
	inline void set_FingersScriptPrefab_2(GameObject_t1113636619 * value)
	{
		___FingersScriptPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___FingersScriptPrefab_2), value);
	}

	inline static int32_t get_offset_of_Earth_3() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___Earth_3)); }
	inline GameObject_t1113636619 * get_Earth_3() const { return ___Earth_3; }
	inline GameObject_t1113636619 ** get_address_of_Earth_3() { return &___Earth_3; }
	inline void set_Earth_3(GameObject_t1113636619 * value)
	{
		___Earth_3 = value;
		Il2CppCodeGenWriteBarrier((&___Earth_3), value);
	}

	inline static int32_t get_offset_of_dpiLabel_4() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___dpiLabel_4)); }
	inline Text_t1901882714 * get_dpiLabel_4() const { return ___dpiLabel_4; }
	inline Text_t1901882714 ** get_address_of_dpiLabel_4() { return &___dpiLabel_4; }
	inline void set_dpiLabel_4(Text_t1901882714 * value)
	{
		___dpiLabel_4 = value;
		Il2CppCodeGenWriteBarrier((&___dpiLabel_4), value);
	}

	inline static int32_t get_offset_of_bottomLabel_5() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___bottomLabel_5)); }
	inline Text_t1901882714 * get_bottomLabel_5() const { return ___bottomLabel_5; }
	inline Text_t1901882714 ** get_address_of_bottomLabel_5() { return &___bottomLabel_5; }
	inline void set_bottomLabel_5(Text_t1901882714 * value)
	{
		___bottomLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottomLabel_5), value);
	}

	inline static int32_t get_offset_of_AsteroidPrefab_6() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___AsteroidPrefab_6)); }
	inline GameObject_t1113636619 * get_AsteroidPrefab_6() const { return ___AsteroidPrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_AsteroidPrefab_6() { return &___AsteroidPrefab_6; }
	inline void set_AsteroidPrefab_6(GameObject_t1113636619 * value)
	{
		___AsteroidPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___AsteroidPrefab_6), value);
	}

	inline static int32_t get_offset_of_LineMaterial_7() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___LineMaterial_7)); }
	inline Material_t340375123 * get_LineMaterial_7() const { return ___LineMaterial_7; }
	inline Material_t340375123 ** get_address_of_LineMaterial_7() { return &___LineMaterial_7; }
	inline void set_LineMaterial_7(Material_t340375123 * value)
	{
		___LineMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_7), value);
	}

	inline static int32_t get_offset_of_asteroids_8() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___asteroids_8)); }
	inline SpriteU5BU5D_t2581906349* get_asteroids_8() const { return ___asteroids_8; }
	inline SpriteU5BU5D_t2581906349** get_address_of_asteroids_8() { return &___asteroids_8; }
	inline void set_asteroids_8(SpriteU5BU5D_t2581906349* value)
	{
		___asteroids_8 = value;
		Il2CppCodeGenWriteBarrier((&___asteroids_8), value);
	}

	inline static int32_t get_offset_of_tapGesture_9() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___tapGesture_9)); }
	inline TapGestureRecognizer_t3178883670 * get_tapGesture_9() const { return ___tapGesture_9; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tapGesture_9() { return &___tapGesture_9; }
	inline void set_tapGesture_9(TapGestureRecognizer_t3178883670 * value)
	{
		___tapGesture_9 = value;
		Il2CppCodeGenWriteBarrier((&___tapGesture_9), value);
	}

	inline static int32_t get_offset_of_doubleTapGesture_10() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___doubleTapGesture_10)); }
	inline TapGestureRecognizer_t3178883670 * get_doubleTapGesture_10() const { return ___doubleTapGesture_10; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_doubleTapGesture_10() { return &___doubleTapGesture_10; }
	inline void set_doubleTapGesture_10(TapGestureRecognizer_t3178883670 * value)
	{
		___doubleTapGesture_10 = value;
		Il2CppCodeGenWriteBarrier((&___doubleTapGesture_10), value);
	}

	inline static int32_t get_offset_of_tripleTapGesture_11() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___tripleTapGesture_11)); }
	inline TapGestureRecognizer_t3178883670 * get_tripleTapGesture_11() const { return ___tripleTapGesture_11; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tripleTapGesture_11() { return &___tripleTapGesture_11; }
	inline void set_tripleTapGesture_11(TapGestureRecognizer_t3178883670 * value)
	{
		___tripleTapGesture_11 = value;
		Il2CppCodeGenWriteBarrier((&___tripleTapGesture_11), value);
	}

	inline static int32_t get_offset_of_swipeGesture_12() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___swipeGesture_12)); }
	inline SwipeGestureRecognizer_t2328511861 * get_swipeGesture_12() const { return ___swipeGesture_12; }
	inline SwipeGestureRecognizer_t2328511861 ** get_address_of_swipeGesture_12() { return &___swipeGesture_12; }
	inline void set_swipeGesture_12(SwipeGestureRecognizer_t2328511861 * value)
	{
		___swipeGesture_12 = value;
		Il2CppCodeGenWriteBarrier((&___swipeGesture_12), value);
	}

	inline static int32_t get_offset_of_panGesture_13() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___panGesture_13)); }
	inline PanGestureRecognizer_t195762396 * get_panGesture_13() const { return ___panGesture_13; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_panGesture_13() { return &___panGesture_13; }
	inline void set_panGesture_13(PanGestureRecognizer_t195762396 * value)
	{
		___panGesture_13 = value;
		Il2CppCodeGenWriteBarrier((&___panGesture_13), value);
	}

	inline static int32_t get_offset_of_scaleGesture_14() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___scaleGesture_14)); }
	inline ScaleGestureRecognizer_t1137887245 * get_scaleGesture_14() const { return ___scaleGesture_14; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_scaleGesture_14() { return &___scaleGesture_14; }
	inline void set_scaleGesture_14(ScaleGestureRecognizer_t1137887245 * value)
	{
		___scaleGesture_14 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_14), value);
	}

	inline static int32_t get_offset_of_rotateGesture_15() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___rotateGesture_15)); }
	inline RotateGestureRecognizer_t4100246528 * get_rotateGesture_15() const { return ___rotateGesture_15; }
	inline RotateGestureRecognizer_t4100246528 ** get_address_of_rotateGesture_15() { return &___rotateGesture_15; }
	inline void set_rotateGesture_15(RotateGestureRecognizer_t4100246528 * value)
	{
		___rotateGesture_15 = value;
		Il2CppCodeGenWriteBarrier((&___rotateGesture_15), value);
	}

	inline static int32_t get_offset_of_longPressGesture_16() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___longPressGesture_16)); }
	inline LongPressGestureRecognizer_t3980777482 * get_longPressGesture_16() const { return ___longPressGesture_16; }
	inline LongPressGestureRecognizer_t3980777482 ** get_address_of_longPressGesture_16() { return &___longPressGesture_16; }
	inline void set_longPressGesture_16(LongPressGestureRecognizer_t3980777482 * value)
	{
		___longPressGesture_16 = value;
		Il2CppCodeGenWriteBarrier((&___longPressGesture_16), value);
	}

	inline static int32_t get_offset_of_nextAsteroid_17() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___nextAsteroid_17)); }
	inline float get_nextAsteroid_17() const { return ___nextAsteroid_17; }
	inline float* get_address_of_nextAsteroid_17() { return &___nextAsteroid_17; }
	inline void set_nextAsteroid_17(float value)
	{
		___nextAsteroid_17 = value;
	}

	inline static int32_t get_offset_of_draggingAsteroid_18() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___draggingAsteroid_18)); }
	inline GameObject_t1113636619 * get_draggingAsteroid_18() const { return ___draggingAsteroid_18; }
	inline GameObject_t1113636619 ** get_address_of_draggingAsteroid_18() { return &___draggingAsteroid_18; }
	inline void set_draggingAsteroid_18(GameObject_t1113636619 * value)
	{
		___draggingAsteroid_18 = value;
		Il2CppCodeGenWriteBarrier((&___draggingAsteroid_18), value);
	}

	inline static int32_t get_offset_of_swipeLines_19() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___swipeLines_19)); }
	inline List_1_t899420910 * get_swipeLines_19() const { return ___swipeLines_19; }
	inline List_1_t899420910 ** get_address_of_swipeLines_19() { return &___swipeLines_19; }
	inline void set_swipeLines_19(List_1_t899420910 * value)
	{
		___swipeLines_19 = value;
		Il2CppCodeGenWriteBarrier((&___swipeLines_19), value);
	}
};

struct DemoScript_t2141982657_StaticFields
{
public:
	// System.Func`2<UnityEngine.GameObject,System.Nullable`1<System.Boolean>> DigitalRubyShared.DemoScript::<>f__mg$cache0
	Func_2_t1671534078 * ___U3CU3Ef__mgU24cache0_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_20() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657_StaticFields, ___U3CU3Ef__mgU24cache0_20)); }
	inline Func_2_t1671534078 * get_U3CU3Ef__mgU24cache0_20() const { return ___U3CU3Ef__mgU24cache0_20; }
	inline Func_2_t1671534078 ** get_address_of_U3CU3Ef__mgU24cache0_20() { return &___U3CU3Ef__mgU24cache0_20; }
	inline void set_U3CU3Ef__mgU24cache0_20(Func_2_t1671534078 * value)
	{
		___U3CU3Ef__mgU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT_T2141982657_H
#ifndef ASTEROIDSCRIPT_T3043891030_H
#define ASTEROIDSCRIPT_T3043891030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.AsteroidScript
struct  AsteroidScript_t3043891030  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTEROIDSCRIPT_T3043891030_H
#ifndef ABOUTSCREENCONTROLLER_T4092912495_H
#define ABOUTSCREENCONTROLLER_T4092912495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// aboutScreenController
struct  aboutScreenController_t4092912495  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SceneManagement.Scene aboutScreenController::scene
	Scene_t2348375561  ___scene_2;

public:
	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(aboutScreenController_t4092912495, ___scene_2)); }
	inline Scene_t2348375561  get_scene_2() const { return ___scene_2; }
	inline Scene_t2348375561 * get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(Scene_t2348375561  value)
	{
		___scene_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTSCREENCONTROLLER_T4092912495_H
#ifndef FINGERSORBITSCRIPT_T2764077671_H
#define FINGERSORBITSCRIPT_T2764077671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersOrbitScript
struct  FingersOrbitScript_t2764077671  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform DigitalRubyShared.FingersOrbitScript::OrbitTarget
	Transform_t3600365921 * ___OrbitTarget_2;
	// UnityEngine.Transform DigitalRubyShared.FingersOrbitScript::Orbiter
	Transform_t3600365921 * ___Orbiter_3;
	// System.Single DigitalRubyShared.FingersOrbitScript::MinZoomDistance
	float ___MinZoomDistance_4;
	// System.Single DigitalRubyShared.FingersOrbitScript::MaxZoomDistance
	float ___MaxZoomDistance_5;
	// System.Single DigitalRubyShared.FingersOrbitScript::ZoomSpeed
	float ___ZoomSpeed_6;
	// System.Single DigitalRubyShared.FingersOrbitScript::OrbitXSpeed
	float ___OrbitXSpeed_7;
	// System.Single DigitalRubyShared.FingersOrbitScript::OrbitXMaxDegrees
	float ___OrbitXMaxDegrees_8;
	// System.Single DigitalRubyShared.FingersOrbitScript::OrbitYSpeed
	float ___OrbitYSpeed_9;
	// System.Single DigitalRubyShared.FingersOrbitScript::OrbitYMaxDegrees
	float ___OrbitYMaxDegrees_10;
	// System.Boolean DigitalRubyShared.FingersOrbitScript::AllowOrbitWhileZooming
	bool ___AllowOrbitWhileZooming_11;
	// System.Boolean DigitalRubyShared.FingersOrbitScript::allowOrbitWhileZooming
	bool ___allowOrbitWhileZooming_12;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.FingersOrbitScript::scaleGesture
	ScaleGestureRecognizer_t1137887245 * ___scaleGesture_13;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersOrbitScript::panGesture
	PanGestureRecognizer_t195762396 * ___panGesture_14;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.FingersOrbitScript::tapGesture
	TapGestureRecognizer_t3178883670 * ___tapGesture_15;
	// System.Single DigitalRubyShared.FingersOrbitScript::xDegrees
	float ___xDegrees_16;
	// System.Single DigitalRubyShared.FingersOrbitScript::yDegrees
	float ___yDegrees_17;
	// System.Action DigitalRubyShared.FingersOrbitScript::OrbitTargetTapped
	Action_t1264377477 * ___OrbitTargetTapped_18;

public:
	inline static int32_t get_offset_of_OrbitTarget_2() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___OrbitTarget_2)); }
	inline Transform_t3600365921 * get_OrbitTarget_2() const { return ___OrbitTarget_2; }
	inline Transform_t3600365921 ** get_address_of_OrbitTarget_2() { return &___OrbitTarget_2; }
	inline void set_OrbitTarget_2(Transform_t3600365921 * value)
	{
		___OrbitTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___OrbitTarget_2), value);
	}

	inline static int32_t get_offset_of_Orbiter_3() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___Orbiter_3)); }
	inline Transform_t3600365921 * get_Orbiter_3() const { return ___Orbiter_3; }
	inline Transform_t3600365921 ** get_address_of_Orbiter_3() { return &___Orbiter_3; }
	inline void set_Orbiter_3(Transform_t3600365921 * value)
	{
		___Orbiter_3 = value;
		Il2CppCodeGenWriteBarrier((&___Orbiter_3), value);
	}

	inline static int32_t get_offset_of_MinZoomDistance_4() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___MinZoomDistance_4)); }
	inline float get_MinZoomDistance_4() const { return ___MinZoomDistance_4; }
	inline float* get_address_of_MinZoomDistance_4() { return &___MinZoomDistance_4; }
	inline void set_MinZoomDistance_4(float value)
	{
		___MinZoomDistance_4 = value;
	}

	inline static int32_t get_offset_of_MaxZoomDistance_5() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___MaxZoomDistance_5)); }
	inline float get_MaxZoomDistance_5() const { return ___MaxZoomDistance_5; }
	inline float* get_address_of_MaxZoomDistance_5() { return &___MaxZoomDistance_5; }
	inline void set_MaxZoomDistance_5(float value)
	{
		___MaxZoomDistance_5 = value;
	}

	inline static int32_t get_offset_of_ZoomSpeed_6() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___ZoomSpeed_6)); }
	inline float get_ZoomSpeed_6() const { return ___ZoomSpeed_6; }
	inline float* get_address_of_ZoomSpeed_6() { return &___ZoomSpeed_6; }
	inline void set_ZoomSpeed_6(float value)
	{
		___ZoomSpeed_6 = value;
	}

	inline static int32_t get_offset_of_OrbitXSpeed_7() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___OrbitXSpeed_7)); }
	inline float get_OrbitXSpeed_7() const { return ___OrbitXSpeed_7; }
	inline float* get_address_of_OrbitXSpeed_7() { return &___OrbitXSpeed_7; }
	inline void set_OrbitXSpeed_7(float value)
	{
		___OrbitXSpeed_7 = value;
	}

	inline static int32_t get_offset_of_OrbitXMaxDegrees_8() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___OrbitXMaxDegrees_8)); }
	inline float get_OrbitXMaxDegrees_8() const { return ___OrbitXMaxDegrees_8; }
	inline float* get_address_of_OrbitXMaxDegrees_8() { return &___OrbitXMaxDegrees_8; }
	inline void set_OrbitXMaxDegrees_8(float value)
	{
		___OrbitXMaxDegrees_8 = value;
	}

	inline static int32_t get_offset_of_OrbitYSpeed_9() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___OrbitYSpeed_9)); }
	inline float get_OrbitYSpeed_9() const { return ___OrbitYSpeed_9; }
	inline float* get_address_of_OrbitYSpeed_9() { return &___OrbitYSpeed_9; }
	inline void set_OrbitYSpeed_9(float value)
	{
		___OrbitYSpeed_9 = value;
	}

	inline static int32_t get_offset_of_OrbitYMaxDegrees_10() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___OrbitYMaxDegrees_10)); }
	inline float get_OrbitYMaxDegrees_10() const { return ___OrbitYMaxDegrees_10; }
	inline float* get_address_of_OrbitYMaxDegrees_10() { return &___OrbitYMaxDegrees_10; }
	inline void set_OrbitYMaxDegrees_10(float value)
	{
		___OrbitYMaxDegrees_10 = value;
	}

	inline static int32_t get_offset_of_AllowOrbitWhileZooming_11() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___AllowOrbitWhileZooming_11)); }
	inline bool get_AllowOrbitWhileZooming_11() const { return ___AllowOrbitWhileZooming_11; }
	inline bool* get_address_of_AllowOrbitWhileZooming_11() { return &___AllowOrbitWhileZooming_11; }
	inline void set_AllowOrbitWhileZooming_11(bool value)
	{
		___AllowOrbitWhileZooming_11 = value;
	}

	inline static int32_t get_offset_of_allowOrbitWhileZooming_12() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___allowOrbitWhileZooming_12)); }
	inline bool get_allowOrbitWhileZooming_12() const { return ___allowOrbitWhileZooming_12; }
	inline bool* get_address_of_allowOrbitWhileZooming_12() { return &___allowOrbitWhileZooming_12; }
	inline void set_allowOrbitWhileZooming_12(bool value)
	{
		___allowOrbitWhileZooming_12 = value;
	}

	inline static int32_t get_offset_of_scaleGesture_13() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___scaleGesture_13)); }
	inline ScaleGestureRecognizer_t1137887245 * get_scaleGesture_13() const { return ___scaleGesture_13; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_scaleGesture_13() { return &___scaleGesture_13; }
	inline void set_scaleGesture_13(ScaleGestureRecognizer_t1137887245 * value)
	{
		___scaleGesture_13 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_13), value);
	}

	inline static int32_t get_offset_of_panGesture_14() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___panGesture_14)); }
	inline PanGestureRecognizer_t195762396 * get_panGesture_14() const { return ___panGesture_14; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_panGesture_14() { return &___panGesture_14; }
	inline void set_panGesture_14(PanGestureRecognizer_t195762396 * value)
	{
		___panGesture_14 = value;
		Il2CppCodeGenWriteBarrier((&___panGesture_14), value);
	}

	inline static int32_t get_offset_of_tapGesture_15() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___tapGesture_15)); }
	inline TapGestureRecognizer_t3178883670 * get_tapGesture_15() const { return ___tapGesture_15; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tapGesture_15() { return &___tapGesture_15; }
	inline void set_tapGesture_15(TapGestureRecognizer_t3178883670 * value)
	{
		___tapGesture_15 = value;
		Il2CppCodeGenWriteBarrier((&___tapGesture_15), value);
	}

	inline static int32_t get_offset_of_xDegrees_16() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___xDegrees_16)); }
	inline float get_xDegrees_16() const { return ___xDegrees_16; }
	inline float* get_address_of_xDegrees_16() { return &___xDegrees_16; }
	inline void set_xDegrees_16(float value)
	{
		___xDegrees_16 = value;
	}

	inline static int32_t get_offset_of_yDegrees_17() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___yDegrees_17)); }
	inline float get_yDegrees_17() const { return ___yDegrees_17; }
	inline float* get_address_of_yDegrees_17() { return &___yDegrees_17; }
	inline void set_yDegrees_17(float value)
	{
		___yDegrees_17 = value;
	}

	inline static int32_t get_offset_of_OrbitTargetTapped_18() { return static_cast<int32_t>(offsetof(FingersOrbitScript_t2764077671, ___OrbitTargetTapped_18)); }
	inline Action_t1264377477 * get_OrbitTargetTapped_18() const { return ___OrbitTargetTapped_18; }
	inline Action_t1264377477 ** get_address_of_OrbitTargetTapped_18() { return &___OrbitTargetTapped_18; }
	inline void set_OrbitTargetTapped_18(Action_t1264377477 * value)
	{
		___OrbitTargetTapped_18 = value;
		Il2CppCodeGenWriteBarrier((&___OrbitTargetTapped_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSORBITSCRIPT_T2764077671_H
#ifndef DEMOSCRIPTJOYSTICK_T699687243_H
#define DEMOSCRIPTJOYSTICK_T699687243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptJoystick
struct  DemoScriptJoystick_t699687243  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersJoystickScript DigitalRubyShared.DemoScriptJoystick::JoystickScript
	FingersJoystickScript_t2468414040 * ___JoystickScript_2;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptJoystick::Mover
	GameObject_t1113636619 * ___Mover_3;
	// System.Single DigitalRubyShared.DemoScriptJoystick::Speed
	float ___Speed_4;
	// System.Boolean DigitalRubyShared.DemoScriptJoystick::MoveJoystickToGestureStartLocation
	bool ___MoveJoystickToGestureStartLocation_5;

public:
	inline static int32_t get_offset_of_JoystickScript_2() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___JoystickScript_2)); }
	inline FingersJoystickScript_t2468414040 * get_JoystickScript_2() const { return ___JoystickScript_2; }
	inline FingersJoystickScript_t2468414040 ** get_address_of_JoystickScript_2() { return &___JoystickScript_2; }
	inline void set_JoystickScript_2(FingersJoystickScript_t2468414040 * value)
	{
		___JoystickScript_2 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickScript_2), value);
	}

	inline static int32_t get_offset_of_Mover_3() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___Mover_3)); }
	inline GameObject_t1113636619 * get_Mover_3() const { return ___Mover_3; }
	inline GameObject_t1113636619 ** get_address_of_Mover_3() { return &___Mover_3; }
	inline void set_Mover_3(GameObject_t1113636619 * value)
	{
		___Mover_3 = value;
		Il2CppCodeGenWriteBarrier((&___Mover_3), value);
	}

	inline static int32_t get_offset_of_Speed_4() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___Speed_4)); }
	inline float get_Speed_4() const { return ___Speed_4; }
	inline float* get_address_of_Speed_4() { return &___Speed_4; }
	inline void set_Speed_4(float value)
	{
		___Speed_4 = value;
	}

	inline static int32_t get_offset_of_MoveJoystickToGestureStartLocation_5() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___MoveJoystickToGestureStartLocation_5)); }
	inline bool get_MoveJoystickToGestureStartLocation_5() const { return ___MoveJoystickToGestureStartLocation_5; }
	inline bool* get_address_of_MoveJoystickToGestureStartLocation_5() { return &___MoveJoystickToGestureStartLocation_5; }
	inline void set_MoveJoystickToGestureStartLocation_5(bool value)
	{
		___MoveJoystickToGestureStartLocation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTJOYSTICK_T699687243_H
#ifndef FINGERSPANROTATESCALESCRIPT_T3928173874_H
#define FINGERSPANROTATESCALESCRIPT_T3928173874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersPanRotateScaleScript
struct  FingersPanRotateScaleScript_t3928173874  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera DigitalRubyShared.FingersPanRotateScaleScript::Camera
	Camera_t4157153871 * ___Camera_2;
	// System.Boolean DigitalRubyShared.FingersPanRotateScaleScript::BringToFront
	bool ___BringToFront_3;
	// System.Int32 DigitalRubyShared.FingersPanRotateScaleScript::PanMinimumTouchCount
	int32_t ___PanMinimumTouchCount_4;
	// UnityEngine.Rigidbody2D DigitalRubyShared.FingersPanRotateScaleScript::rigidBody
	Rigidbody2D_t939494601 * ___rigidBody_5;
	// UnityEngine.SpriteRenderer DigitalRubyShared.FingersPanRotateScaleScript::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_6;
	// System.Int32 DigitalRubyShared.FingersPanRotateScaleScript::startSortOrder
	int32_t ___startSortOrder_7;
	// UnityEngine.Vector2 DigitalRubyShared.FingersPanRotateScaleScript::panStart
	Vector2_t2156229523  ___panStart_8;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersPanRotateScaleScript::panGesture
	PanGestureRecognizer_t195762396 * ___panGesture_9;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.FingersPanRotateScaleScript::scaleGesture
	ScaleGestureRecognizer_t1137887245 * ___scaleGesture_10;
	// DigitalRubyShared.RotateGestureRecognizer DigitalRubyShared.FingersPanRotateScaleScript::rotateGesture
	RotateGestureRecognizer_t4100246528 * ___rotateGesture_11;

public:
	inline static int32_t get_offset_of_Camera_2() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___Camera_2)); }
	inline Camera_t4157153871 * get_Camera_2() const { return ___Camera_2; }
	inline Camera_t4157153871 ** get_address_of_Camera_2() { return &___Camera_2; }
	inline void set_Camera_2(Camera_t4157153871 * value)
	{
		___Camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_2), value);
	}

	inline static int32_t get_offset_of_BringToFront_3() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___BringToFront_3)); }
	inline bool get_BringToFront_3() const { return ___BringToFront_3; }
	inline bool* get_address_of_BringToFront_3() { return &___BringToFront_3; }
	inline void set_BringToFront_3(bool value)
	{
		___BringToFront_3 = value;
	}

	inline static int32_t get_offset_of_PanMinimumTouchCount_4() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___PanMinimumTouchCount_4)); }
	inline int32_t get_PanMinimumTouchCount_4() const { return ___PanMinimumTouchCount_4; }
	inline int32_t* get_address_of_PanMinimumTouchCount_4() { return &___PanMinimumTouchCount_4; }
	inline void set_PanMinimumTouchCount_4(int32_t value)
	{
		___PanMinimumTouchCount_4 = value;
	}

	inline static int32_t get_offset_of_rigidBody_5() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___rigidBody_5)); }
	inline Rigidbody2D_t939494601 * get_rigidBody_5() const { return ___rigidBody_5; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidBody_5() { return &___rigidBody_5; }
	inline void set_rigidBody_5(Rigidbody2D_t939494601 * value)
	{
		___rigidBody_5 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_5), value);
	}

	inline static int32_t get_offset_of_spriteRenderer_6() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___spriteRenderer_6)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_6() const { return ___spriteRenderer_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_6() { return &___spriteRenderer_6; }
	inline void set_spriteRenderer_6(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_6), value);
	}

	inline static int32_t get_offset_of_startSortOrder_7() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___startSortOrder_7)); }
	inline int32_t get_startSortOrder_7() const { return ___startSortOrder_7; }
	inline int32_t* get_address_of_startSortOrder_7() { return &___startSortOrder_7; }
	inline void set_startSortOrder_7(int32_t value)
	{
		___startSortOrder_7 = value;
	}

	inline static int32_t get_offset_of_panStart_8() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___panStart_8)); }
	inline Vector2_t2156229523  get_panStart_8() const { return ___panStart_8; }
	inline Vector2_t2156229523 * get_address_of_panStart_8() { return &___panStart_8; }
	inline void set_panStart_8(Vector2_t2156229523  value)
	{
		___panStart_8 = value;
	}

	inline static int32_t get_offset_of_panGesture_9() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___panGesture_9)); }
	inline PanGestureRecognizer_t195762396 * get_panGesture_9() const { return ___panGesture_9; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_panGesture_9() { return &___panGesture_9; }
	inline void set_panGesture_9(PanGestureRecognizer_t195762396 * value)
	{
		___panGesture_9 = value;
		Il2CppCodeGenWriteBarrier((&___panGesture_9), value);
	}

	inline static int32_t get_offset_of_scaleGesture_10() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___scaleGesture_10)); }
	inline ScaleGestureRecognizer_t1137887245 * get_scaleGesture_10() const { return ___scaleGesture_10; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_scaleGesture_10() { return &___scaleGesture_10; }
	inline void set_scaleGesture_10(ScaleGestureRecognizer_t1137887245 * value)
	{
		___scaleGesture_10 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_10), value);
	}

	inline static int32_t get_offset_of_rotateGesture_11() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874, ___rotateGesture_11)); }
	inline RotateGestureRecognizer_t4100246528 * get_rotateGesture_11() const { return ___rotateGesture_11; }
	inline RotateGestureRecognizer_t4100246528 ** get_address_of_rotateGesture_11() { return &___rotateGesture_11; }
	inline void set_rotateGesture_11(RotateGestureRecognizer_t4100246528 * value)
	{
		___rotateGesture_11 = value;
		Il2CppCodeGenWriteBarrier((&___rotateGesture_11), value);
	}
};

struct FingersPanRotateScaleScript_t3928173874_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> DigitalRubyShared.FingersPanRotateScaleScript::captureRaycastResults
	List_1_t537414295 * ___captureRaycastResults_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> DigitalRubyShared.FingersPanRotateScaleScript::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_captureRaycastResults_12() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874_StaticFields, ___captureRaycastResults_12)); }
	inline List_1_t537414295 * get_captureRaycastResults_12() const { return ___captureRaycastResults_12; }
	inline List_1_t537414295 ** get_address_of_captureRaycastResults_12() { return &___captureRaycastResults_12; }
	inline void set_captureRaycastResults_12(List_1_t537414295 * value)
	{
		___captureRaycastResults_12 = value;
		Il2CppCodeGenWriteBarrier((&___captureRaycastResults_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleScript_t3928173874_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSPANROTATESCALESCRIPT_T3928173874_H
#ifndef FINGERSSCRIPT_T1857011421_H
#define FINGERSSCRIPT_T1857011421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript
struct  FingersScript_t1857011421  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DigitalRubyShared.FingersScript::TreatMousePointerAsFinger
	bool ___TreatMousePointerAsFinger_2;
	// System.Boolean DigitalRubyShared.FingersScript::SimulateMouseWithTouches
	bool ___SimulateMouseWithTouches_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DigitalRubyShared.FingersScript::PassThroughObjects
	List_1_t2585711361 * ___PassThroughObjects_4;
	// UnityEngine.GameObject[] DigitalRubyShared.FingersScript::TouchCircles
	GameObjectU5BU5D_t3328599146* ___TouchCircles_5;
	// System.Boolean DigitalRubyShared.FingersScript::ShowTouches
	bool ___ShowTouches_6;
	// System.Int32 DigitalRubyShared.FingersScript::DefaultDPI
	int32_t ___DefaultDPI_7;
	// System.Boolean DigitalRubyShared.FingersScript::ClearGesturesOnLevelLoad
	bool ___ClearGesturesOnLevelLoad_8;
	// System.Collections.Generic.KeyValuePair`2<System.Single,System.Single>[] DigitalRubyShared.FingersScript::mousePrev
	KeyValuePair_2U5BU5D_t1094138374* ___mousePrev_12;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.FingersScript::gestures
	List_1_t861137127 * ___gestures_13;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.FingersScript::gesturesTemp
	List_1_t861137127 * ___gesturesTemp_14;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touchesBegan
	List_1_t3464476875 * ___touchesBegan_15;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touchesMoved
	List_1_t3464476875 * ___touchesMoved_16;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touchesEnded
	List_1_t3464476875 * ___touchesEnded_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>> DigitalRubyShared.FingersScript::gameObjectsForTouch
	Dictionary_2_t1474424692 * ___gameObjectsForTouch_18;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> DigitalRubyShared.FingersScript::captureRaycastResults
	List_1_t537414295 * ___captureRaycastResults_19;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::filteredTouches
	List_1_t3464476875 * ___filteredTouches_20;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touches
	List_1_t3464476875 * ___touches_21;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector2> DigitalRubyShared.FingersScript::previousTouchPositions
	Dictionary_2_t1044942854 * ___previousTouchPositions_22;
	// System.Collections.Generic.List`1<UnityEngine.Component> DigitalRubyShared.FingersScript::components
	List_1_t3395709193 * ___components_23;
	// System.Collections.Generic.HashSet`1<System.Type> DigitalRubyShared.FingersScript::componentTypesToDenyPassThrough
	HashSet_1_t1048894234 * ___componentTypesToDenyPassThrough_24;
	// System.Collections.Generic.HashSet`1<System.Type> DigitalRubyShared.FingersScript::componentTypesToIgnorePassThrough
	HashSet_1_t1048894234 * ___componentTypesToIgnorePassThrough_25;
	// System.Single DigitalRubyShared.FingersScript::rotateAngle
	float ___rotateAngle_26;
	// System.Single DigitalRubyShared.FingersScript::pinchScale
	float ___pinchScale_27;
	// DigitalRubyShared.GestureTouch DigitalRubyShared.FingersScript::rotatePinch1
	GestureTouch_t1992402133  ___rotatePinch1_28;
	// DigitalRubyShared.GestureTouch DigitalRubyShared.FingersScript::rotatePinch2
	GestureTouch_t1992402133  ___rotatePinch2_29;
	// System.Boolean DigitalRubyShared.FingersScript::liveForever
	bool ___liveForever_30;
	// System.Func`2<UnityEngine.GameObject,System.Nullable`1<System.Boolean>> DigitalRubyShared.FingersScript::CaptureGestureHandler
	Func_2_t1671534078 * ___CaptureGestureHandler_32;

public:
	inline static int32_t get_offset_of_TreatMousePointerAsFinger_2() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___TreatMousePointerAsFinger_2)); }
	inline bool get_TreatMousePointerAsFinger_2() const { return ___TreatMousePointerAsFinger_2; }
	inline bool* get_address_of_TreatMousePointerAsFinger_2() { return &___TreatMousePointerAsFinger_2; }
	inline void set_TreatMousePointerAsFinger_2(bool value)
	{
		___TreatMousePointerAsFinger_2 = value;
	}

	inline static int32_t get_offset_of_SimulateMouseWithTouches_3() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___SimulateMouseWithTouches_3)); }
	inline bool get_SimulateMouseWithTouches_3() const { return ___SimulateMouseWithTouches_3; }
	inline bool* get_address_of_SimulateMouseWithTouches_3() { return &___SimulateMouseWithTouches_3; }
	inline void set_SimulateMouseWithTouches_3(bool value)
	{
		___SimulateMouseWithTouches_3 = value;
	}

	inline static int32_t get_offset_of_PassThroughObjects_4() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___PassThroughObjects_4)); }
	inline List_1_t2585711361 * get_PassThroughObjects_4() const { return ___PassThroughObjects_4; }
	inline List_1_t2585711361 ** get_address_of_PassThroughObjects_4() { return &___PassThroughObjects_4; }
	inline void set_PassThroughObjects_4(List_1_t2585711361 * value)
	{
		___PassThroughObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___PassThroughObjects_4), value);
	}

	inline static int32_t get_offset_of_TouchCircles_5() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___TouchCircles_5)); }
	inline GameObjectU5BU5D_t3328599146* get_TouchCircles_5() const { return ___TouchCircles_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_TouchCircles_5() { return &___TouchCircles_5; }
	inline void set_TouchCircles_5(GameObjectU5BU5D_t3328599146* value)
	{
		___TouchCircles_5 = value;
		Il2CppCodeGenWriteBarrier((&___TouchCircles_5), value);
	}

	inline static int32_t get_offset_of_ShowTouches_6() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___ShowTouches_6)); }
	inline bool get_ShowTouches_6() const { return ___ShowTouches_6; }
	inline bool* get_address_of_ShowTouches_6() { return &___ShowTouches_6; }
	inline void set_ShowTouches_6(bool value)
	{
		___ShowTouches_6 = value;
	}

	inline static int32_t get_offset_of_DefaultDPI_7() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___DefaultDPI_7)); }
	inline int32_t get_DefaultDPI_7() const { return ___DefaultDPI_7; }
	inline int32_t* get_address_of_DefaultDPI_7() { return &___DefaultDPI_7; }
	inline void set_DefaultDPI_7(int32_t value)
	{
		___DefaultDPI_7 = value;
	}

	inline static int32_t get_offset_of_ClearGesturesOnLevelLoad_8() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___ClearGesturesOnLevelLoad_8)); }
	inline bool get_ClearGesturesOnLevelLoad_8() const { return ___ClearGesturesOnLevelLoad_8; }
	inline bool* get_address_of_ClearGesturesOnLevelLoad_8() { return &___ClearGesturesOnLevelLoad_8; }
	inline void set_ClearGesturesOnLevelLoad_8(bool value)
	{
		___ClearGesturesOnLevelLoad_8 = value;
	}

	inline static int32_t get_offset_of_mousePrev_12() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___mousePrev_12)); }
	inline KeyValuePair_2U5BU5D_t1094138374* get_mousePrev_12() const { return ___mousePrev_12; }
	inline KeyValuePair_2U5BU5D_t1094138374** get_address_of_mousePrev_12() { return &___mousePrev_12; }
	inline void set_mousePrev_12(KeyValuePair_2U5BU5D_t1094138374* value)
	{
		___mousePrev_12 = value;
		Il2CppCodeGenWriteBarrier((&___mousePrev_12), value);
	}

	inline static int32_t get_offset_of_gestures_13() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___gestures_13)); }
	inline List_1_t861137127 * get_gestures_13() const { return ___gestures_13; }
	inline List_1_t861137127 ** get_address_of_gestures_13() { return &___gestures_13; }
	inline void set_gestures_13(List_1_t861137127 * value)
	{
		___gestures_13 = value;
		Il2CppCodeGenWriteBarrier((&___gestures_13), value);
	}

	inline static int32_t get_offset_of_gesturesTemp_14() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___gesturesTemp_14)); }
	inline List_1_t861137127 * get_gesturesTemp_14() const { return ___gesturesTemp_14; }
	inline List_1_t861137127 ** get_address_of_gesturesTemp_14() { return &___gesturesTemp_14; }
	inline void set_gesturesTemp_14(List_1_t861137127 * value)
	{
		___gesturesTemp_14 = value;
		Il2CppCodeGenWriteBarrier((&___gesturesTemp_14), value);
	}

	inline static int32_t get_offset_of_touchesBegan_15() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touchesBegan_15)); }
	inline List_1_t3464476875 * get_touchesBegan_15() const { return ___touchesBegan_15; }
	inline List_1_t3464476875 ** get_address_of_touchesBegan_15() { return &___touchesBegan_15; }
	inline void set_touchesBegan_15(List_1_t3464476875 * value)
	{
		___touchesBegan_15 = value;
		Il2CppCodeGenWriteBarrier((&___touchesBegan_15), value);
	}

	inline static int32_t get_offset_of_touchesMoved_16() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touchesMoved_16)); }
	inline List_1_t3464476875 * get_touchesMoved_16() const { return ___touchesMoved_16; }
	inline List_1_t3464476875 ** get_address_of_touchesMoved_16() { return &___touchesMoved_16; }
	inline void set_touchesMoved_16(List_1_t3464476875 * value)
	{
		___touchesMoved_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchesMoved_16), value);
	}

	inline static int32_t get_offset_of_touchesEnded_17() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touchesEnded_17)); }
	inline List_1_t3464476875 * get_touchesEnded_17() const { return ___touchesEnded_17; }
	inline List_1_t3464476875 ** get_address_of_touchesEnded_17() { return &___touchesEnded_17; }
	inline void set_touchesEnded_17(List_1_t3464476875 * value)
	{
		___touchesEnded_17 = value;
		Il2CppCodeGenWriteBarrier((&___touchesEnded_17), value);
	}

	inline static int32_t get_offset_of_gameObjectsForTouch_18() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___gameObjectsForTouch_18)); }
	inline Dictionary_2_t1474424692 * get_gameObjectsForTouch_18() const { return ___gameObjectsForTouch_18; }
	inline Dictionary_2_t1474424692 ** get_address_of_gameObjectsForTouch_18() { return &___gameObjectsForTouch_18; }
	inline void set_gameObjectsForTouch_18(Dictionary_2_t1474424692 * value)
	{
		___gameObjectsForTouch_18 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectsForTouch_18), value);
	}

	inline static int32_t get_offset_of_captureRaycastResults_19() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___captureRaycastResults_19)); }
	inline List_1_t537414295 * get_captureRaycastResults_19() const { return ___captureRaycastResults_19; }
	inline List_1_t537414295 ** get_address_of_captureRaycastResults_19() { return &___captureRaycastResults_19; }
	inline void set_captureRaycastResults_19(List_1_t537414295 * value)
	{
		___captureRaycastResults_19 = value;
		Il2CppCodeGenWriteBarrier((&___captureRaycastResults_19), value);
	}

	inline static int32_t get_offset_of_filteredTouches_20() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___filteredTouches_20)); }
	inline List_1_t3464476875 * get_filteredTouches_20() const { return ___filteredTouches_20; }
	inline List_1_t3464476875 ** get_address_of_filteredTouches_20() { return &___filteredTouches_20; }
	inline void set_filteredTouches_20(List_1_t3464476875 * value)
	{
		___filteredTouches_20 = value;
		Il2CppCodeGenWriteBarrier((&___filteredTouches_20), value);
	}

	inline static int32_t get_offset_of_touches_21() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touches_21)); }
	inline List_1_t3464476875 * get_touches_21() const { return ___touches_21; }
	inline List_1_t3464476875 ** get_address_of_touches_21() { return &___touches_21; }
	inline void set_touches_21(List_1_t3464476875 * value)
	{
		___touches_21 = value;
		Il2CppCodeGenWriteBarrier((&___touches_21), value);
	}

	inline static int32_t get_offset_of_previousTouchPositions_22() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___previousTouchPositions_22)); }
	inline Dictionary_2_t1044942854 * get_previousTouchPositions_22() const { return ___previousTouchPositions_22; }
	inline Dictionary_2_t1044942854 ** get_address_of_previousTouchPositions_22() { return &___previousTouchPositions_22; }
	inline void set_previousTouchPositions_22(Dictionary_2_t1044942854 * value)
	{
		___previousTouchPositions_22 = value;
		Il2CppCodeGenWriteBarrier((&___previousTouchPositions_22), value);
	}

	inline static int32_t get_offset_of_components_23() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___components_23)); }
	inline List_1_t3395709193 * get_components_23() const { return ___components_23; }
	inline List_1_t3395709193 ** get_address_of_components_23() { return &___components_23; }
	inline void set_components_23(List_1_t3395709193 * value)
	{
		___components_23 = value;
		Il2CppCodeGenWriteBarrier((&___components_23), value);
	}

	inline static int32_t get_offset_of_componentTypesToDenyPassThrough_24() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___componentTypesToDenyPassThrough_24)); }
	inline HashSet_1_t1048894234 * get_componentTypesToDenyPassThrough_24() const { return ___componentTypesToDenyPassThrough_24; }
	inline HashSet_1_t1048894234 ** get_address_of_componentTypesToDenyPassThrough_24() { return &___componentTypesToDenyPassThrough_24; }
	inline void set_componentTypesToDenyPassThrough_24(HashSet_1_t1048894234 * value)
	{
		___componentTypesToDenyPassThrough_24 = value;
		Il2CppCodeGenWriteBarrier((&___componentTypesToDenyPassThrough_24), value);
	}

	inline static int32_t get_offset_of_componentTypesToIgnorePassThrough_25() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___componentTypesToIgnorePassThrough_25)); }
	inline HashSet_1_t1048894234 * get_componentTypesToIgnorePassThrough_25() const { return ___componentTypesToIgnorePassThrough_25; }
	inline HashSet_1_t1048894234 ** get_address_of_componentTypesToIgnorePassThrough_25() { return &___componentTypesToIgnorePassThrough_25; }
	inline void set_componentTypesToIgnorePassThrough_25(HashSet_1_t1048894234 * value)
	{
		___componentTypesToIgnorePassThrough_25 = value;
		Il2CppCodeGenWriteBarrier((&___componentTypesToIgnorePassThrough_25), value);
	}

	inline static int32_t get_offset_of_rotateAngle_26() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___rotateAngle_26)); }
	inline float get_rotateAngle_26() const { return ___rotateAngle_26; }
	inline float* get_address_of_rotateAngle_26() { return &___rotateAngle_26; }
	inline void set_rotateAngle_26(float value)
	{
		___rotateAngle_26 = value;
	}

	inline static int32_t get_offset_of_pinchScale_27() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___pinchScale_27)); }
	inline float get_pinchScale_27() const { return ___pinchScale_27; }
	inline float* get_address_of_pinchScale_27() { return &___pinchScale_27; }
	inline void set_pinchScale_27(float value)
	{
		___pinchScale_27 = value;
	}

	inline static int32_t get_offset_of_rotatePinch1_28() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___rotatePinch1_28)); }
	inline GestureTouch_t1992402133  get_rotatePinch1_28() const { return ___rotatePinch1_28; }
	inline GestureTouch_t1992402133 * get_address_of_rotatePinch1_28() { return &___rotatePinch1_28; }
	inline void set_rotatePinch1_28(GestureTouch_t1992402133  value)
	{
		___rotatePinch1_28 = value;
	}

	inline static int32_t get_offset_of_rotatePinch2_29() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___rotatePinch2_29)); }
	inline GestureTouch_t1992402133  get_rotatePinch2_29() const { return ___rotatePinch2_29; }
	inline GestureTouch_t1992402133 * get_address_of_rotatePinch2_29() { return &___rotatePinch2_29; }
	inline void set_rotatePinch2_29(GestureTouch_t1992402133  value)
	{
		___rotatePinch2_29 = value;
	}

	inline static int32_t get_offset_of_liveForever_30() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___liveForever_30)); }
	inline bool get_liveForever_30() const { return ___liveForever_30; }
	inline bool* get_address_of_liveForever_30() { return &___liveForever_30; }
	inline void set_liveForever_30(bool value)
	{
		___liveForever_30 = value;
	}

	inline static int32_t get_offset_of_CaptureGestureHandler_32() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___CaptureGestureHandler_32)); }
	inline Func_2_t1671534078 * get_CaptureGestureHandler_32() const { return ___CaptureGestureHandler_32; }
	inline Func_2_t1671534078 ** get_address_of_CaptureGestureHandler_32() { return &___CaptureGestureHandler_32; }
	inline void set_CaptureGestureHandler_32(Func_2_t1671534078 * value)
	{
		___CaptureGestureHandler_32 = value;
		Il2CppCodeGenWriteBarrier((&___CaptureGestureHandler_32), value);
	}
};

struct FingersScript_t1857011421_StaticFields
{
public:
	// DigitalRubyShared.FingersScript DigitalRubyShared.FingersScript::singleton
	FingersScript_t1857011421 * ___singleton_31;

public:
	inline static int32_t get_offset_of_singleton_31() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421_StaticFields, ___singleton_31)); }
	inline FingersScript_t1857011421 * get_singleton_31() const { return ___singleton_31; }
	inline FingersScript_t1857011421 ** get_address_of_singleton_31() { return &___singleton_31; }
	inline void set_singleton_31(FingersScript_t1857011421 * value)
	{
		___singleton_31 = value;
		Il2CppCodeGenWriteBarrier((&___singleton_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSSCRIPT_T1857011421_H
#ifndef DEMOSCRIPTSWIPE_T1996673777_H
#define DEMOSCRIPTSWIPE_T1996673777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptSwipe
struct  DemoScriptSwipe_t1996673777  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem DigitalRubyShared.DemoScriptSwipe::SwipeParticleSystem
	ParticleSystem_t1800779281 * ___SwipeParticleSystem_2;

public:
	inline static int32_t get_offset_of_SwipeParticleSystem_2() { return static_cast<int32_t>(offsetof(DemoScriptSwipe_t1996673777, ___SwipeParticleSystem_2)); }
	inline ParticleSystem_t1800779281 * get_SwipeParticleSystem_2() const { return ___SwipeParticleSystem_2; }
	inline ParticleSystem_t1800779281 ** get_address_of_SwipeParticleSystem_2() { return &___SwipeParticleSystem_2; }
	inline void set_SwipeParticleSystem_2(ParticleSystem_t1800779281 * value)
	{
		___SwipeParticleSystem_2 = value;
		Il2CppCodeGenWriteBarrier((&___SwipeParticleSystem_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTSWIPE_T1996673777_H
#ifndef DEMOSCRIPTIMAGE_T4046729131_H
#define DEMOSCRIPTIMAGE_T4046729131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptImage
struct  DemoScriptImage_t4046729131  : public FingersImageAutomationScript_t4118243268
{
public:

public:
};

struct DemoScriptImage_t4046729131_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<DigitalRubyShared.ImageGestureImage,System.String> DigitalRubyShared.DemoScriptImage::recognizableImages
	Dictionary_2_t3194170630 * ___recognizableImages_13;

public:
	inline static int32_t get_offset_of_recognizableImages_13() { return static_cast<int32_t>(offsetof(DemoScriptImage_t4046729131_StaticFields, ___recognizableImages_13)); }
	inline Dictionary_2_t3194170630 * get_recognizableImages_13() const { return ___recognizableImages_13; }
	inline Dictionary_2_t3194170630 ** get_address_of_recognizableImages_13() { return &___recognizableImages_13; }
	inline void set_recognizableImages_13(Dictionary_2_t3194170630 * value)
	{
		___recognizableImages_13 = value;
		Il2CppCodeGenWriteBarrier((&___recognizableImages_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTIMAGE_T4046729131_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (TMP_Glyph_t581847833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (FontCreationSetting_t628772060)+ sizeof (RuntimeObject), sizeof(FontCreationSetting_t628772060_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3001[12] = 
{
	FontCreationSetting_t628772060::get_offset_of_fontSourcePath_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontSizingMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontPadding_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontPackingMode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontAtlasWidth_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontAtlasHeight_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontCharacterSet_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontStyle_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontStlyeModifier_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontRenderMode_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t628772060::get_offset_of_fontKerning_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (Glyph2D_t1260586688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[8] = 
{
	Glyph2D_t1260586688::get_offset_of_bottomLeft_0(),
	Glyph2D_t1260586688::get_offset_of_topLeft_1(),
	Glyph2D_t1260586688::get_offset_of_bottomRight_2(),
	Glyph2D_t1260586688::get_offset_of_topRight_3(),
	Glyph2D_t1260586688::get_offset_of_uv0_4(),
	Glyph2D_t1260586688::get_offset_of_uv1_5(),
	Glyph2D_t1260586688::get_offset_of_uv2_6(),
	Glyph2D_t1260586688::get_offset_of_uv3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (KerningPairKey_t536493877)+ sizeof (RuntimeObject), sizeof(KerningPairKey_t536493877 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3003[3] = 
{
	KerningPairKey_t536493877::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (KerningPair_t2270855589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[3] = 
{
	KerningPair_t2270855589::get_offset_of_AscII_Left_0(),
	KerningPair_t2270855589::get_offset_of_AscII_Right_1(),
	KerningPair_t2270855589::get_offset_of_XadvanceOffset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (KerningTable_t2322366871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[1] = 
{
	KerningTable_t2322366871::get_offset_of_kerningPairs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (U3CU3Ec__DisplayClass3_0_t2918531336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[2] = 
{
	U3CU3Ec__DisplayClass3_0_t2918531336::get_offset_of_left_0(),
	U3CU3Ec__DisplayClass3_0_t2918531336::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (U3CU3Ec__DisplayClass4_0_t2918531329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[2] = 
{
	U3CU3Ec__DisplayClass4_0_t2918531329::get_offset_of_left_0(),
	U3CU3Ec__DisplayClass4_0_t2918531329::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (U3CU3Ec_t4077839018), -1, sizeof(U3CU3Ec_t4077839018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3008[3] = 
{
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9__6_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (TMP_FontUtilities_t2599150238), -1, sizeof(TMP_FontUtilities_t2599150238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3009[1] = 
{
	TMP_FontUtilities_t2599150238_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (TMP_VertexDataUpdateFlags_t388000256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3010[8] = 
{
	TMP_VertexDataUpdateFlags_t388000256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (TMP_CharacterInfo_t3185626797)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[35] = 
{
	TMP_CharacterInfo_t3185626797::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (TMP_Vertex_t2404176824)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t2404176824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3012[5] = 
{
	TMP_Vertex_t2404176824::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (VertexGradient_t345148380)+ sizeof (RuntimeObject), sizeof(VertexGradient_t345148380 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3013[4] = 
{
	VertexGradient_t345148380::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (TMP_PageInfo_t2608430633)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t2608430633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3014[5] = 
{
	TMP_PageInfo_t2608430633::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (TMP_LinkInfo_t1092083476)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[7] = 
{
	TMP_LinkInfo_t1092083476::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (TMP_WordInfo_t3331066303)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[4] = 
{
	TMP_WordInfo_t3331066303::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (TMP_SpriteInfo_t2726321384)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t2726321384 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3017[3] = 
{
	TMP_SpriteInfo_t2726321384::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (Extents_t3837212874)+ sizeof (RuntimeObject), sizeof(Extents_t3837212874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3018[2] = 
{
	Extents_t3837212874::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_t3837212874::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (Mesh_Extents_t3388355125)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t3388355125 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3019[2] = 
{
	Mesh_Extents_t3388355125::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t3388355125::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (WordWrapState_t341939652)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[53] = 
{
	WordWrapState_t341939652::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_sizeStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_indentStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontWeightStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_styleStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_actionStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_materialReferenceStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineJustificationStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_spriteAnimationID_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontAsset_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentSpriteAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterial_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterialIndex_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_meshExtents_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_tagNoParsing_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (TagAttribute_t688278634)+ sizeof (RuntimeObject), sizeof(TagAttribute_t688278634 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3021[3] = 
{
	TagAttribute_t688278634::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (XML_TagAttribute_t1174424309)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_t1174424309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3022[5] = 
{
	XML_TagAttribute_t1174424309::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (ShaderUtilities_t714255158), -1, sizeof(ShaderUtilities_t714255158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3023[59] = 
{
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GradientScale_16(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleX_17(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleY_18(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_PerspectiveFilter_19(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureWidth_20(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureHeight_21(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_BevelAmount_22(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowColor_23(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOffset_24(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowPower_25(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOuter_26(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_LightAngle_27(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMap_28(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrix_29(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrixRotation_30(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskCoord_31(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ClipRect_32(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessX_33(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessY_34(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetX_35(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetY_36(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UseClipRect_37(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilID_38(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilOp_39(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilComp_40(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilReadMask_41(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilWriteMask_42(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ShaderFlags_43(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_A_44(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_B_45(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_C_46(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Bevel_47(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Glow_48(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Underlay_49(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Ratios_50(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_SOFT_51(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_HARD_52(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_TEX_53(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Outline_54(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_ZTestMode_55(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_CullMode_56(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_m_clamp_57(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_isInitialized_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (SpriteAssetImportFormats_t116390639)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3024[3] = 
{
	SpriteAssetImportFormats_t116390639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (TexturePacker_t3148178657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (SpriteFrame_t3912389194)+ sizeof (RuntimeObject), sizeof(SpriteFrame_t3912389194 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3026[4] = 
{
	SpriteFrame_t3912389194::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3912389194::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (SpriteSize_t3355290999)+ sizeof (RuntimeObject), sizeof(SpriteSize_t3355290999 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3027[2] = 
{
	SpriteSize_t3355290999::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t3355290999::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (SpriteData_t3048397587)+ sizeof (RuntimeObject), sizeof(SpriteData_t3048397587_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3028[7] = 
{
	SpriteData_t3048397587::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t3048397587::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (SpriteDataObject_t308163541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[1] = 
{
	SpriteDataObject_t308163541::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255370), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3030[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994318)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994318 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (__StaticArrayInitTypeSizeU3D40_t1547998295)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_t1547998295 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (U3CModuleU3E_t692745553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (aboutScreenController_t4092912495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[1] = 
{
	aboutScreenController_t4092912495::get_offset_of_scene_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (AsteroidScript_t3043891030), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (DemoScript_t2141982657), -1, sizeof(DemoScript_t2141982657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3036[19] = 
{
	DemoScript_t2141982657::get_offset_of_FingersScriptPrefab_2(),
	DemoScript_t2141982657::get_offset_of_Earth_3(),
	DemoScript_t2141982657::get_offset_of_dpiLabel_4(),
	DemoScript_t2141982657::get_offset_of_bottomLabel_5(),
	DemoScript_t2141982657::get_offset_of_AsteroidPrefab_6(),
	DemoScript_t2141982657::get_offset_of_LineMaterial_7(),
	DemoScript_t2141982657::get_offset_of_asteroids_8(),
	DemoScript_t2141982657::get_offset_of_tapGesture_9(),
	DemoScript_t2141982657::get_offset_of_doubleTapGesture_10(),
	DemoScript_t2141982657::get_offset_of_tripleTapGesture_11(),
	DemoScript_t2141982657::get_offset_of_swipeGesture_12(),
	DemoScript_t2141982657::get_offset_of_panGesture_13(),
	DemoScript_t2141982657::get_offset_of_scaleGesture_14(),
	DemoScript_t2141982657::get_offset_of_rotateGesture_15(),
	DemoScript_t2141982657::get_offset_of_longPressGesture_16(),
	DemoScript_t2141982657::get_offset_of_nextAsteroid_17(),
	DemoScript_t2141982657::get_offset_of_draggingAsteroid_18(),
	DemoScript_t2141982657::get_offset_of_swipeLines_19(),
	DemoScript_t2141982657_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (DemoScript3DOrbit_t3440477113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (DemoScriptDPad_t2734679325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[5] = 
{
	DemoScriptDPad_t2734679325::get_offset_of_DPadScript_2(),
	DemoScriptDPad_t2734679325::get_offset_of_Mover_3(),
	DemoScriptDPad_t2734679325::get_offset_of_Speed_4(),
	DemoScriptDPad_t2734679325::get_offset_of_MoveDPadToGestureStartLocation_5(),
	DemoScriptDPad_t2734679325::get_offset_of_startPos_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (DemoScriptImage_t4046729131), -1, sizeof(DemoScriptImage_t4046729131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3039[2] = 
{
	0,
	DemoScriptImage_t4046729131_StaticFields::get_offset_of_recognizableImages_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (DemoScriptJoystick_t699687243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[4] = 
{
	DemoScriptJoystick_t699687243::get_offset_of_JoystickScript_2(),
	DemoScriptJoystick_t699687243::get_offset_of_Mover_3(),
	DemoScriptJoystick_t699687243::get_offset_of_Speed_4(),
	DemoScriptJoystick_t699687243::get_offset_of_MoveJoystickToGestureStartLocation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (DemoScriptOneFinger_t2754275351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[6] = 
{
	DemoScriptOneFinger_t2754275351::get_offset_of_RotateIcon_2(),
	DemoScriptOneFinger_t2754275351::get_offset_of_ScaleIcon_3(),
	DemoScriptOneFinger_t2754275351::get_offset_of_Earth_4(),
	DemoScriptOneFinger_t2754275351::get_offset_of_FingerScript_5(),
	DemoScriptOneFinger_t2754275351::get_offset_of_rotationGesture_6(),
	DemoScriptOneFinger_t2754275351::get_offset_of_scaleGesture_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (DemoScriptPan_t3102497188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[1] = 
{
	DemoScriptPan_t3102497188::get_offset_of_FingersScript_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (DemoScriptPlatformSpecificView_t3383617572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[3] = 
{
	DemoScriptPlatformSpecificView_t3383617572::get_offset_of_FingersScriptPrefab_2(),
	DemoScriptPlatformSpecificView_t3383617572::get_offset_of_LeftPanel_3(),
	DemoScriptPlatformSpecificView_t3383617572::get_offset_of_Cube_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (DemoScriptSwipe_t1996673777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[1] = 
{
	DemoScriptSwipe_t1996673777::get_offset_of_SwipeParticleSystem_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (DemoScriptZoomableScrollView_t163084534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[9] = 
{
	DemoScriptZoomableScrollView_t163084534::get_offset_of_ScrollView_2(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_Canvas_3(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_ScaleFocusMoveThresholdUnits_4(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scaleStart_5(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scaleEnd_6(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scaleTime_7(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_elapsedScaleTime_8(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scalePosStart_9(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scalePosEnd_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (DemoScriptZoomPanCamera_t3831420416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[4] = 
{
	DemoScriptZoomPanCamera_t3831420416::get_offset_of_scaleGesture_2(),
	DemoScriptZoomPanCamera_t3831420416::get_offset_of_panGesture_3(),
	DemoScriptZoomPanCamera_t3831420416::get_offset_of_tapGesture_4(),
	DemoScriptZoomPanCamera_t3831420416::get_offset_of_cameraAnimationTargetPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[6] = 
{
	U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594::get_offset_of_U3CstartU3E__0_0(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594::get_offset_of_U3CaccumTimeU3E__1_1(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594::get_offset_of_U24this_2(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594::get_offset_of_U24current_3(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594::get_offset_of_U24disposing_4(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t2868933594::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (FingersDPadItem_t4100703432)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3048[6] = 
{
	FingersDPadItem_t4100703432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (FingersDPadScript_t3801874975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[13] = 
{
	FingersDPadScript_t3801874975::get_offset_of_DPadBackgroundImage_2(),
	FingersDPadScript_t3801874975::get_offset_of_DPadUpImageSelected_3(),
	FingersDPadScript_t3801874975::get_offset_of_DPadRightImageSelected_4(),
	FingersDPadScript_t3801874975::get_offset_of_DPadDownImageSelected_5(),
	FingersDPadScript_t3801874975::get_offset_of_DPadLeftImageSelected_6(),
	FingersDPadScript_t3801874975::get_offset_of_DPadCenterImageSelected_7(),
	FingersDPadScript_t3801874975::get_offset_of_TouchRadiusInUnits_8(),
	FingersDPadScript_t3801874975::get_offset_of_overlap_9(),
	FingersDPadScript_t3801874975::get_offset_of_DPadItemTapped_10(),
	FingersDPadScript_t3801874975::get_offset_of_DPadItemPanned_11(),
	FingersDPadScript_t3801874975::get_offset_of_U3CPanGestureU3Ek__BackingField_12(),
	FingersDPadScript_t3801874975::get_offset_of_U3CTapGestureU3Ek__BackingField_13(),
	FingersDPadScript_t3801874975::get_offset_of_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (FingersDragDropScript_t2960271979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3050[7] = 
{
	FingersDragDropScript_t2960271979::get_offset_of_Camera_2(),
	FingersDragDropScript_t2960271979::get_offset_of_BringToFront_3(),
	FingersDragDropScript_t2960271979::get_offset_of_longPressGesture_4(),
	FingersDragDropScript_t2960271979::get_offset_of_rigidBody_5(),
	FingersDragDropScript_t2960271979::get_offset_of_spriteRenderer_6(),
	FingersDragDropScript_t2960271979::get_offset_of_startSortOrder_7(),
	FingersDragDropScript_t2960271979::get_offset_of_panStart_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (FingersImageAutomationScript_t4118243268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[10] = 
{
	FingersImageAutomationScript_t4118243268::get_offset_of_FingersScript_2(),
	FingersImageAutomationScript_t4118243268::get_offset_of_Image_3(),
	FingersImageAutomationScript_t4118243268::get_offset_of_ImageGesture_4(),
	FingersImageAutomationScript_t4118243268::get_offset_of_LineMaterial_5(),
	FingersImageAutomationScript_t4118243268::get_offset_of_MatchLabel_6(),
	FingersImageAutomationScript_t4118243268::get_offset_of_ScriptText_7(),
	FingersImageAutomationScript_t4118243268::get_offset_of_U3CLastImageU3Ek__BackingField_8(),
	FingersImageAutomationScript_t4118243268::get_offset_of_RecognizableImages_9(),
	FingersImageAutomationScript_t4118243268::get_offset_of_lineSet_10(),
	FingersImageAutomationScript_t4118243268::get_offset_of_currentPointList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (FingersJoystickScript_t2468414040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[7] = 
{
	FingersJoystickScript_t2468414040::get_offset_of_JoystickImage_2(),
	FingersJoystickScript_t2468414040::get_offset_of_JoystickPower_3(),
	FingersJoystickScript_t2468414040::get_offset_of_MaxExtentPercent_4(),
	FingersJoystickScript_t2468414040::get_offset_of_startCenter_5(),
	FingersJoystickScript_t2468414040::get_offset_of_U3CPanGestureU3Ek__BackingField_6(),
	FingersJoystickScript_t2468414040::get_offset_of_JoystickExecuted_7(),
	FingersJoystickScript_t2468414040::get_offset_of_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (FingersOrbitScript_t2764077671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[17] = 
{
	FingersOrbitScript_t2764077671::get_offset_of_OrbitTarget_2(),
	FingersOrbitScript_t2764077671::get_offset_of_Orbiter_3(),
	FingersOrbitScript_t2764077671::get_offset_of_MinZoomDistance_4(),
	FingersOrbitScript_t2764077671::get_offset_of_MaxZoomDistance_5(),
	FingersOrbitScript_t2764077671::get_offset_of_ZoomSpeed_6(),
	FingersOrbitScript_t2764077671::get_offset_of_OrbitXSpeed_7(),
	FingersOrbitScript_t2764077671::get_offset_of_OrbitXMaxDegrees_8(),
	FingersOrbitScript_t2764077671::get_offset_of_OrbitYSpeed_9(),
	FingersOrbitScript_t2764077671::get_offset_of_OrbitYMaxDegrees_10(),
	FingersOrbitScript_t2764077671::get_offset_of_AllowOrbitWhileZooming_11(),
	FingersOrbitScript_t2764077671::get_offset_of_allowOrbitWhileZooming_12(),
	FingersOrbitScript_t2764077671::get_offset_of_scaleGesture_13(),
	FingersOrbitScript_t2764077671::get_offset_of_panGesture_14(),
	FingersOrbitScript_t2764077671::get_offset_of_tapGesture_15(),
	FingersOrbitScript_t2764077671::get_offset_of_xDegrees_16(),
	FingersOrbitScript_t2764077671::get_offset_of_yDegrees_17(),
	FingersOrbitScript_t2764077671::get_offset_of_OrbitTargetTapped_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (FingersPanRotateScaleScript_t3928173874), -1, sizeof(FingersPanRotateScaleScript_t3928173874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3054[12] = 
{
	FingersPanRotateScaleScript_t3928173874::get_offset_of_Camera_2(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_BringToFront_3(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_PanMinimumTouchCount_4(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_rigidBody_5(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_spriteRenderer_6(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_startSortOrder_7(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_panStart_8(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_panGesture_9(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_scaleGesture_10(),
	FingersPanRotateScaleScript_t3928173874::get_offset_of_rotateGesture_11(),
	FingersPanRotateScaleScript_t3928173874_StaticFields::get_offset_of_captureRaycastResults_12(),
	FingersPanRotateScaleScript_t3928173874_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (FingersScript_t1857011421), -1, sizeof(FingersScript_t1857011421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3055[31] = 
{
	FingersScript_t1857011421::get_offset_of_TreatMousePointerAsFinger_2(),
	FingersScript_t1857011421::get_offset_of_SimulateMouseWithTouches_3(),
	FingersScript_t1857011421::get_offset_of_PassThroughObjects_4(),
	FingersScript_t1857011421::get_offset_of_TouchCircles_5(),
	FingersScript_t1857011421::get_offset_of_ShowTouches_6(),
	FingersScript_t1857011421::get_offset_of_DefaultDPI_7(),
	FingersScript_t1857011421::get_offset_of_ClearGesturesOnLevelLoad_8(),
	0,
	0,
	0,
	FingersScript_t1857011421::get_offset_of_mousePrev_12(),
	FingersScript_t1857011421::get_offset_of_gestures_13(),
	FingersScript_t1857011421::get_offset_of_gesturesTemp_14(),
	FingersScript_t1857011421::get_offset_of_touchesBegan_15(),
	FingersScript_t1857011421::get_offset_of_touchesMoved_16(),
	FingersScript_t1857011421::get_offset_of_touchesEnded_17(),
	FingersScript_t1857011421::get_offset_of_gameObjectsForTouch_18(),
	FingersScript_t1857011421::get_offset_of_captureRaycastResults_19(),
	FingersScript_t1857011421::get_offset_of_filteredTouches_20(),
	FingersScript_t1857011421::get_offset_of_touches_21(),
	FingersScript_t1857011421::get_offset_of_previousTouchPositions_22(),
	FingersScript_t1857011421::get_offset_of_components_23(),
	FingersScript_t1857011421::get_offset_of_componentTypesToDenyPassThrough_24(),
	FingersScript_t1857011421::get_offset_of_componentTypesToIgnorePassThrough_25(),
	FingersScript_t1857011421::get_offset_of_rotateAngle_26(),
	FingersScript_t1857011421::get_offset_of_pinchScale_27(),
	FingersScript_t1857011421::get_offset_of_rotatePinch1_28(),
	FingersScript_t1857011421::get_offset_of_rotatePinch2_29(),
	FingersScript_t1857011421::get_offset_of_liveForever_30(),
	FingersScript_t1857011421_StaticFields::get_offset_of_singleton_31(),
	FingersScript_t1857011421::get_offset_of_CaptureGestureHandler_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (CaptureResult_t3073219507)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3056[5] = 
{
	CaptureResult_t3073219507::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (U3CMainThreadCallbackU3Ec__Iterator0_t2673350864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[6] = 
{
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_action_0(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U3CwU3E__1_1(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_delay_2(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U24current_3(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U24disposing_4(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (DeviceInfo_t3428345325), -1, sizeof(DeviceInfo_t3428345325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3058[2] = 
{
	DeviceInfo_t3428345325_StaticFields::get_offset_of_U3CPixelsPerInchU3Ek__BackingField_0(),
	DeviceInfo_t3428345325_StaticFields::get_offset_of_U3CUnitMultiplierU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (GestureRecognizerState_t650110565)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3059[7] = 
{
	GestureRecognizerState_t650110565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (GestureTouch_t1992402133)+ sizeof (RuntimeObject), sizeof(GestureTouch_t1992402133_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3060[10] = 
{
	0,
	GestureTouch_t1992402133::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_x_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_y_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_previousX_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_previousY_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_pressure_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_screenX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_screenY_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_platformSpecificTouch_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (GestureRecognizerUpdated_t601711085), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (GestureVelocityTracker_t806949657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[7] = 
{
	0,
	GestureVelocityTracker_t806949657::get_offset_of_history_1(),
	GestureVelocityTracker_t806949657::get_offset_of_timer_2(),
	GestureVelocityTracker_t806949657::get_offset_of_previousX_3(),
	GestureVelocityTracker_t806949657::get_offset_of_previousY_4(),
	GestureVelocityTracker_t806949657::get_offset_of_U3CVelocityXU3Ek__BackingField_5(),
	GestureVelocityTracker_t806949657::get_offset_of_U3CVelocityYU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (VelocityHistory_t1265565051)+ sizeof (RuntimeObject), sizeof(VelocityHistory_t1265565051 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3063[3] = 
{
	VelocityHistory_t1265565051::get_offset_of_VelocityX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VelocityHistory_t1265565051::get_offset_of_VelocityY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VelocityHistory_t1265565051::get_offset_of_Seconds_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (GestureRecognizer_t3684029681), -1, sizeof(GestureRecognizer_t3684029681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3064[27] = 
{
	GestureRecognizer_t3684029681_StaticFields::get_offset_of_allGesturesReference_0(),
	GestureRecognizer_t3684029681::get_offset_of_state_1(),
	GestureRecognizer_t3684029681::get_offset_of_currentTrackedTouches_2(),
	GestureRecognizer_t3684029681::get_offset_of_currentTrackedTouchesReadOnly_3(),
	GestureRecognizer_t3684029681::get_offset_of_requireGestureRecognizerToFail_4(),
	GestureRecognizer_t3684029681::get_offset_of_failGestures_5(),
	GestureRecognizer_t3684029681::get_offset_of_simultaneousGestures_6(),
	GestureRecognizer_t3684029681::get_offset_of_velocityTracker_7(),
	GestureRecognizer_t3684029681::get_offset_of_touchesJustEnded_8(),
	GestureRecognizer_t3684029681::get_offset_of_minimumNumberOfTouchesToTrack_9(),
	GestureRecognizer_t3684029681::get_offset_of_maximumNumberOfTouchesToTrack_10(),
	GestureRecognizer_t3684029681::get_offset_of_U3CprevFocusXU3Ek__BackingField_11(),
	GestureRecognizer_t3684029681::get_offset_of_U3CprevFocusYU3Ek__BackingField_12(),
	GestureRecognizer_t3684029681_StaticFields::get_offset_of_ActiveGestures_13(),
	GestureRecognizer_t3684029681::get_offset_of_Updated_14(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDeltaXU3Ek__BackingField_15(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDeltaYU3Ek__BackingField_16(),
	GestureRecognizer_t3684029681::get_offset_of_U3CFocusXU3Ek__BackingField_17(),
	GestureRecognizer_t3684029681::get_offset_of_U3CFocusYU3Ek__BackingField_18(),
	GestureRecognizer_t3684029681::get_offset_of_U3CStartFocusXU3Ek__BackingField_19(),
	GestureRecognizer_t3684029681::get_offset_of_U3CStartFocusYU3Ek__BackingField_20(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDistanceXU3Ek__BackingField_21(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDistanceYU3Ek__BackingField_22(),
	GestureRecognizer_t3684029681::get_offset_of_U3CPlatformSpecificViewU3Ek__BackingField_23(),
	GestureRecognizer_t3684029681::get_offset_of_U3CPlatformSpecificViewScaleU3Ek__BackingField_24(),
	GestureRecognizer_t3684029681::get_offset_of_U3CAllowSimultaneousExecutionWithAllGesturesU3Ek__BackingField_25(),
	GestureRecognizer_t3684029681_StaticFields::get_offset_of_MainThreadCallback_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (CallbackMainThreadDelegate_t469493312), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (ImageGestureImage_t2885596271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ImageGestureImage_t2885596271::get_offset_of_U3CWidthU3Ek__BackingField_8(),
	ImageGestureImage_t2885596271::get_offset_of_U3CHeightU3Ek__BackingField_9(),
	ImageGestureImage_t2885596271::get_offset_of_U3CSizeU3Ek__BackingField_10(),
	ImageGestureImage_t2885596271::get_offset_of_U3CRowsU3Ek__BackingField_11(),
	ImageGestureImage_t2885596271::get_offset_of_U3CPixelsU3Ek__BackingField_12(),
	ImageGestureImage_t2885596271::get_offset_of_U3CSimilarityPaddingU3Ek__BackingField_13(),
	ImageGestureImage_t2885596271::get_offset_of_U3CScoreU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (ImageGestureRecognizer_t4233185475), -1, sizeof(ImageGestureRecognizer_t4233185475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3067[24] = 
{
	ImageGestureRecognizer_t4233185475_StaticFields::get_offset_of_RowBitMasks_27(),
	ImageGestureRecognizer_t4233185475_StaticFields::get_offset_of_RowBitmask_28(),
	0,
	0,
	0,
	0,
	ImageGestureRecognizer_t4233185475::get_offset_of_points_33(),
	ImageGestureRecognizer_t4233185475::get_offset_of_numberOfPaths_34(),
	ImageGestureRecognizer_t4233185475::get_offset_of_currentList_35(),
	ImageGestureRecognizer_t4233185475::get_offset_of_minX_36(),
	ImageGestureRecognizer_t4233185475::get_offset_of_minY_37(),
	ImageGestureRecognizer_t4233185475::get_offset_of_maxX_38(),
	ImageGestureRecognizer_t4233185475::get_offset_of_maxY_39(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMaximumPathCountU3Ek__BackingField_40(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CDirectionToleranceU3Ek__BackingField_41(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_42(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_43(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CSimularityMinimumU3Ek__BackingField_44(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CGestureImagesU3Ek__BackingField_45(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CImageU3Ek__BackingField_46(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMatchedGestureImageU3Ek__BackingField_47(),
	ImageGestureRecognizer_t4233185475::get_offset_of_MaximumPathCountExceeded_48(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_49(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMinimumPointsToRecognizeU3Ek__BackingField_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (Point_t2411124144)+ sizeof (RuntimeObject), sizeof(Point_t2411124144 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3068[2] = 
{
	Point_t2411124144::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Point_t2411124144::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (LongPressGestureRecognizer_t3980777482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[3] = 
{
	LongPressGestureRecognizer_t3980777482::get_offset_of_tag_27(),
	LongPressGestureRecognizer_t3980777482::get_offset_of_U3CMinimumDurationSecondsU3Ek__BackingField_28(),
	LongPressGestureRecognizer_t3980777482::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (U3CAttemptToStartAfterDelayU3Ec__AnonStorey0_t3552537278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[2] = 
{
	U3CAttemptToStartAfterDelayU3Ec__AnonStorey0_t3552537278::get_offset_of_tempTag_0(),
	U3CAttemptToStartAfterDelayU3Ec__AnonStorey0_t3552537278::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (OneTouchRotateGestureRecognizer_t3272893959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[2] = 
{
	OneTouchRotateGestureRecognizer_t3272893959::get_offset_of_AnglePointOverrideX_33(),
	OneTouchRotateGestureRecognizer_t3272893959::get_offset_of_AnglePointOverrideY_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (OneTouchScaleGestureRecognizer_t1313669683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[3] = 
{
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CScaleMultiplierU3Ek__BackingField_27(),
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CZoomSpeedU3Ek__BackingField_28(),
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (PanGestureRecognizer_t195762396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[1] = 
{
	PanGestureRecognizer_t195762396::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (RotateGestureRecognizer_t4100246528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[6] = 
{
	RotateGestureRecognizer_t4100246528::get_offset_of_startAngle_27(),
	RotateGestureRecognizer_t4100246528::get_offset_of_previousAngle_28(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CAngleThresholdU3Ek__BackingField_29(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_30(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CRotationRadiansU3Ek__BackingField_31(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CRotationRadiansDeltaU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (ScaleGestureRecognizer_t1137887245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[8] = 
{
	ScaleGestureRecognizer_t1137887245::get_offset_of_previousDistance_27(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_centerX_28(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_centerY_29(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CScaleMultiplierU3Ek__BackingField_30(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CZoomSpeedU3Ek__BackingField_31(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_32(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CScaleThresholdPercentU3Ek__BackingField_33(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CScaleFocusMoveThresholdUnitsU3Ek__BackingField_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (SwipeGestureRecognizerDirection_t3225897881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3076[6] = 
{
	SwipeGestureRecognizerDirection_t3225897881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (SwipeGestureRecognizer_t2328511861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[7] = 
{
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CDirectionU3Ek__BackingField_27(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CMinimumDistanceUnitsU3Ek__BackingField_28(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CMinimumSpeedUnitsU3Ek__BackingField_29(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CDirectionThresholdU3Ek__BackingField_30(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CEndDirectionU3Ek__BackingField_31(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CEndImmeditelyU3Ek__BackingField_32(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CFailOnDirectionChangeU3Ek__BackingField_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (TapGestureRecognizer_t3178883670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[8] = 
{
	TapGestureRecognizer_t3178883670::get_offset_of_tapCount_27(),
	TapGestureRecognizer_t3178883670::get_offset_of_timer_28(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CNumberOfTapsRequiredU3Ek__BackingField_29(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CThresholdSecondsU3Ek__BackingField_30(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_31(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CTapXU3Ek__BackingField_32(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CTapYU3Ek__BackingField_33(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CSendBeginStateU3Ek__BackingField_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (FingersGestures_t1283504238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[4] = 
{
	FingersGestures_t1283504238::get_offset_of_PanMinimumTouchCount_2(),
	FingersGestures_t1283504238::get_offset_of_FingersScriptPrefab_3(),
	FingersGestures_t1283504238::get_offset_of_panGesture_4(),
	FingersGestures_t1283504238::get_offset_of_scaleGesture_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (LoadPage_t2941907970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[1] = 
{
	LoadPage_t2941907970::get_offset_of_vibrated_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (DirectionResource_t3837219169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[9] = 
{
	DirectionResource_t3837219169::get_offset_of_apiEndpoint_4(),
	DirectionResource_t3837219169::get_offset_of_profile_5(),
	DirectionResource_t3837219169::get_offset_of_coordinates_6(),
	DirectionResource_t3837219169::get_offset_of_alternatives_7(),
	DirectionResource_t3837219169::get_offset_of_bearings_8(),
	DirectionResource_t3837219169::get_offset_of_continueStraight_9(),
	DirectionResource_t3837219169::get_offset_of_overview_10(),
	DirectionResource_t3837219169::get_offset_of_radiuses_11(),
	DirectionResource_t3837219169::get_offset_of_steps_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (Directions_t1397515081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[1] = 
{
	Directions_t1397515081::get_offset_of_fileSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (U3CQueryU3Ec__AnonStorey0_t2317980075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[2] = 
{
	U3CQueryU3Ec__AnonStorey0_t2317980075::get_offset_of_callback_0(),
	U3CQueryU3Ec__AnonStorey0_t2317980075::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (Overview_t3152666467), -1, sizeof(Overview_t3152666467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3084[4] = 
{
	Overview_t3152666467_StaticFields::get_offset_of_Full_0(),
	Overview_t3152666467_StaticFields::get_offset_of_Simplified_1(),
	Overview_t3152666467_StaticFields::get_offset_of_False_2(),
	Overview_t3152666467::get_offset_of_overview_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (DirectionsResponse_t4007621150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[3] = 
{
	DirectionsResponse_t4007621150::get_offset_of_U3CRoutesU3Ek__BackingField_0(),
	DirectionsResponse_t4007621150::get_offset_of_U3CWaypointsU3Ek__BackingField_1(),
	DirectionsResponse_t4007621150::get_offset_of_U3CCodeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (Intersection_t3434721945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3086[5] = 
{
	Intersection_t3434721945::get_offset_of_U3COutU3Ek__BackingField_0(),
	Intersection_t3434721945::get_offset_of_U3CEntryU3Ek__BackingField_1(),
	Intersection_t3434721945::get_offset_of_U3CBearingsU3Ek__BackingField_2(),
	Intersection_t3434721945::get_offset_of_U3CLocationU3Ek__BackingField_3(),
	Intersection_t3434721945::get_offset_of_U3CInU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (Leg_t3885960664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[4] = 
{
	Leg_t3885960664::get_offset_of_U3CStepsU3Ek__BackingField_0(),
	Leg_t3885960664::get_offset_of_U3CSummaryU3Ek__BackingField_1(),
	Leg_t3885960664::get_offset_of_U3CDurationU3Ek__BackingField_2(),
	Leg_t3885960664::get_offset_of_U3CDistanceU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (Maneuver_t1966826127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[6] = 
{
	Maneuver_t1966826127::get_offset_of_U3CBearingAfterU3Ek__BackingField_0(),
	Maneuver_t1966826127::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Maneuver_t1966826127::get_offset_of_U3CModifierU3Ek__BackingField_2(),
	Maneuver_t1966826127::get_offset_of_U3CBearingBeforeU3Ek__BackingField_3(),
	Maneuver_t1966826127::get_offset_of_U3CLocationU3Ek__BackingField_4(),
	Maneuver_t1966826127::get_offset_of_U3CInstructionU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (Route_t1961148093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[4] = 
{
	Route_t1961148093::get_offset_of_U3CLegsU3Ek__BackingField_0(),
	Route_t1961148093::get_offset_of_U3CGeometryU3Ek__BackingField_1(),
	Route_t1961148093::get_offset_of_U3CDurationU3Ek__BackingField_2(),
	Route_t1961148093::get_offset_of_U3CDistanceU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (Step_t1196010167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[7] = 
{
	Step_t1196010167::get_offset_of_U3CIntersectionsU3Ek__BackingField_0(),
	Step_t1196010167::get_offset_of_U3CGeometryU3Ek__BackingField_1(),
	Step_t1196010167::get_offset_of_U3CManeuverU3Ek__BackingField_2(),
	Step_t1196010167::get_offset_of_U3CDurationU3Ek__BackingField_3(),
	Step_t1196010167::get_offset_of_U3CDistanceU3Ek__BackingField_4(),
	Step_t1196010167::get_offset_of_U3CNameU3Ek__BackingField_5(),
	Step_t1196010167::get_offset_of_U3CModeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (Waypoint_t3638467145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[2] = 
{
	Waypoint_t3638467145::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Waypoint_t3638467145::get_offset_of_U3CLocationU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (RoutingProfile_t2080742987), -1, sizeof(RoutingProfile_t2080742987_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3092[4] = 
{
	RoutingProfile_t2080742987_StaticFields::get_offset_of_Driving_0(),
	RoutingProfile_t2080742987_StaticFields::get_offset_of_Walking_1(),
	RoutingProfile_t2080742987_StaticFields::get_offset_of_Cycling_2(),
	RoutingProfile_t2080742987::get_offset_of_profile_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (ForwardGeocodeResource_t367023433), -1, sizeof(ForwardGeocodeResource_t367023433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3093[6] = 
{
	ForwardGeocodeResource_t367023433_StaticFields::get_offset_of_CountryCodes_8(),
	ForwardGeocodeResource_t367023433::get_offset_of_query_9(),
	ForwardGeocodeResource_t367023433::get_offset_of_autocomplete_10(),
	ForwardGeocodeResource_t367023433::get_offset_of_country_11(),
	ForwardGeocodeResource_t367023433::get_offset_of_proximity_12(),
	ForwardGeocodeResource_t367023433::get_offset_of_bbox_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (Geocoder_t3195298050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[1] = 
{
	Geocoder_t3195298050::get_offset_of_fileSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3097[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (Feature_t4105919465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3098[11] = 
{
	Feature_t4105919465::get_offset_of_U3CIdU3Ek__BackingField_0(),
	Feature_t4105919465::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Feature_t4105919465::get_offset_of_U3CTextU3Ek__BackingField_2(),
	Feature_t4105919465::get_offset_of_U3CPlaceNameU3Ek__BackingField_3(),
	Feature_t4105919465::get_offset_of_U3CRelevanceU3Ek__BackingField_4(),
	Feature_t4105919465::get_offset_of_U3CPropertiesU3Ek__BackingField_5(),
	Feature_t4105919465::get_offset_of_U3CBboxU3Ek__BackingField_6(),
	Feature_t4105919465::get_offset_of_U3CCenterU3Ek__BackingField_7(),
	Feature_t4105919465::get_offset_of_U3CGeometryU3Ek__BackingField_8(),
	Feature_t4105919465::get_offset_of_U3CAddressU3Ek__BackingField_9(),
	Feature_t4105919465::get_offset_of_U3CContextU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (GeocodeResponse_t3526499648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3099[3] = 
{
	GeocodeResponse_t3526499648::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	GeocodeResponse_t3526499648::get_offset_of_U3CFeaturesU3Ek__BackingField_1(),
	GeocodeResponse_t3526499648::get_offset_of_U3CAttributionU3Ek__BackingField_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
