﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mapbox.Platform.FileSource
struct FileSource_t3962300185;
// Mapbox.Json.JsonConverter[]
struct JsonConverterU5BU5D_t1616679288;
// System.String
struct String_t;
// System.Action`1<Mapbox.Platform.Response>
struct Action_1_t1228140472;
// System.Collections.Generic.List`1<Mapbox.Geocoding.Feature>
struct List_1_t1283026911;
// System.Collections.Generic.Dictionary`2<System.String,Mapbox.Platform.Cache.MemoryCache/CacheItem>
struct Dictionary_2_t206654781;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Mapbox.Platform.Cache.MemoryCache/CacheItem>,System.Int64>
struct Func_2_t4023812394;
// System.Collections.Generic.Dictionary`2<System.String,Mapbox.Platform.MbTiles.MbTilesDb>
struct Dictionary_2_t2119965391;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>>
struct Func_2_t3661664344;
// System.Func`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>>
struct Func_2_t788283625;
// System.Func`2<<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>,System.String>
struct Func_2_t4279064255;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Mapbox.Platform.Response
struct Response_t1055672877;
// SQLite4Unity3d.TableMapping/Column
struct Column_t1357940583;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3070079188;
// System.Type
struct Type_t;
// SQLite4Unity3d.TableMapping/Column[]
struct ColumnU5BU5D_t4060676894;
// SQLite4Unity3d.PreparedSqlLiteInsertCommand
struct PreparedSqlLiteInsertCommand_t994658783;
// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.Boolean>
struct Func_2_t681049744;
// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.String>
struct Func_2_t2431212468;
// System.Collections.Generic.List`1<Mapbox.Platform.Cache.ICache>
struct List_1_t4166699764;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t2066740105;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct List_1_t257796462;
// Mapbox.Platform.Cache.CachingWebFileSource
struct CachingWebFileSource_t328893056;
// Mapbox.Platform.IFileSource
struct IFileSource_t3859839141;
// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteCommand/Binding>
struct List_1_t4258913814;
// System.Collections.Generic.Dictionary`2<System.String,SQLite4Unity3d.TableMapping>
struct Dictionary_2_t2061655791;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.Random
struct Random_t108471755;
// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.Int32>
struct Func_2_t166798807;
// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String>
struct Func_2_t3358271039;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.IndexedAttribute>
struct IEnumerable_1_t3659173693;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<System.Exception>
struct List_1_t2908811991;
// Mapbox.Platform.IAsyncRequest
struct IAsyncRequest_t3870285476;
// System.Action
struct Action_t1264377477;
// System.Func`2<System.Exception,System.String>
struct Func_2_t905536786;
// Mapbox.VectorTile.VectorTile
struct VectorTile_t3467883484;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping/Column>
struct IEnumerable_1_t337793472;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<Mapbox.Platform.IAsyncRequest,System.Int32>
struct Dictionary_2_t1445974517;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CWAITFORALLREQUESTSU3EC__ITERATOR0_T168843647_H
#define U3CWAITFORALLREQUESTSU3EC__ITERATOR0_T168843647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.FileSource/<WaitForAllRequests>c__Iterator0
struct  U3CWaitForAllRequestsU3Ec__Iterator0_t168843647  : public RuntimeObject
{
public:
	// System.Object Mapbox.Platform.FileSource/<WaitForAllRequests>c__Iterator0::$locvar0
	RuntimeObject * ___U24locvar0_0;
	// Mapbox.Platform.FileSource Mapbox.Platform.FileSource/<WaitForAllRequests>c__Iterator0::$this
	FileSource_t3962300185 * ___U24this_1;
	// System.Object Mapbox.Platform.FileSource/<WaitForAllRequests>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Mapbox.Platform.FileSource/<WaitForAllRequests>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Mapbox.Platform.FileSource/<WaitForAllRequests>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CWaitForAllRequestsU3Ec__Iterator0_t168843647, ___U24locvar0_0)); }
	inline RuntimeObject * get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject ** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject * value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWaitForAllRequestsU3Ec__Iterator0_t168843647, ___U24this_1)); }
	inline FileSource_t3962300185 * get_U24this_1() const { return ___U24this_1; }
	inline FileSource_t3962300185 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FileSource_t3962300185 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForAllRequestsU3Ec__Iterator0_t168843647, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitForAllRequestsU3Ec__Iterator0_t168843647, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitForAllRequestsU3Ec__Iterator0_t168843647, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORALLREQUESTSU3EC__ITERATOR0_T168843647_H
#ifndef JSONCONVERTERS_T1015645604_H
#define JSONCONVERTERS_T1015645604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.JsonConverters.JsonConverters
struct  JsonConverters_t1015645604  : public RuntimeObject
{
public:

public:
};

struct JsonConverters_t1015645604_StaticFields
{
public:
	// Mapbox.Json.JsonConverter[] Mapbox.Utils.JsonConverters.JsonConverters::converters
	JsonConverterU5BU5D_t1616679288* ___converters_0;

public:
	inline static int32_t get_offset_of_converters_0() { return static_cast<int32_t>(offsetof(JsonConverters_t1015645604_StaticFields, ___converters_0)); }
	inline JsonConverterU5BU5D_t1616679288* get_converters_0() const { return ___converters_0; }
	inline JsonConverterU5BU5D_t1616679288** get_address_of_converters_0() { return &___converters_0; }
	inline void set_converters_0(JsonConverterU5BU5D_t1616679288* value)
	{
		___converters_0 = value;
		Il2CppCodeGenWriteBarrier((&___converters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTERS_T1015645604_H
#ifndef POLYLINEUTILS_T2997409923_H
#define POLYLINEUTILS_T2997409923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.PolylineUtils
struct  PolylineUtils_t2997409923  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYLINEUTILS_T2997409923_H
#ifndef IASYNCREQUESTFACTORY_T4260832379_H
#define IASYNCREQUESTFACTORY_T4260832379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.IAsyncRequestFactory
struct  IAsyncRequestFactory_t4260832379  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IASYNCREQUESTFACTORY_T4260832379_H
#ifndef U3CFINDCOLUMNU3EC__ANONSTOREY1_T4099938701_H
#define U3CFINDCOLUMNU3EC__ANONSTOREY1_T4099938701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.TableMapping/<FindColumn>c__AnonStorey1
struct  U3CFindColumnU3Ec__AnonStorey1_t4099938701  : public RuntimeObject
{
public:
	// System.String SQLite4Unity3d.TableMapping/<FindColumn>c__AnonStorey1::columnName
	String_t* ___columnName_0;

public:
	inline static int32_t get_offset_of_columnName_0() { return static_cast<int32_t>(offsetof(U3CFindColumnU3Ec__AnonStorey1_t4099938701, ___columnName_0)); }
	inline String_t* get_columnName_0() const { return ___columnName_0; }
	inline String_t** get_address_of_columnName_0() { return &___columnName_0; }
	inline void set_columnName_0(String_t* value)
	{
		___columnName_0 = value;
		Il2CppCodeGenWriteBarrier((&___columnName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDCOLUMNU3EC__ANONSTOREY1_T4099938701_H
#ifndef U3CPROXYRESPONSEU3EC__ANONSTOREY1_T3634450083_H
#define U3CPROXYRESPONSEU3EC__ANONSTOREY1_T3634450083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.FileSource/<proxyResponse>c__AnonStorey1
struct  U3CproxyResponseU3Ec__AnonStorey1_t3634450083  : public RuntimeObject
{
public:
	// System.Action`1<Mapbox.Platform.Response> Mapbox.Platform.FileSource/<proxyResponse>c__AnonStorey1::callback
	Action_1_t1228140472 * ___callback_0;
	// Mapbox.Platform.FileSource Mapbox.Platform.FileSource/<proxyResponse>c__AnonStorey1::$this
	FileSource_t3962300185 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CproxyResponseU3Ec__AnonStorey1_t3634450083, ___callback_0)); }
	inline Action_1_t1228140472 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t1228140472 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t1228140472 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CproxyResponseU3Ec__AnonStorey1_t3634450083, ___U24this_1)); }
	inline FileSource_t3962300185 * get_U24this_1() const { return ___U24this_1; }
	inline FileSource_t3962300185 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FileSource_t3962300185 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROXYRESPONSEU3EC__ANONSTOREY1_T3634450083_H
#ifndef GEOCODERESPONSE_T3526499648_H
#define GEOCODERESPONSE_T3526499648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.GeocodeResponse
struct  GeocodeResponse_t3526499648  : public RuntimeObject
{
public:
	// System.String Mapbox.Geocoding.GeocodeResponse::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Mapbox.Geocoding.Feature> Mapbox.Geocoding.GeocodeResponse::<Features>k__BackingField
	List_1_t1283026911 * ___U3CFeaturesU3Ek__BackingField_1;
	// System.String Mapbox.Geocoding.GeocodeResponse::<Attribution>k__BackingField
	String_t* ___U3CAttributionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GeocodeResponse_t3526499648, ___U3CTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CFeaturesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GeocodeResponse_t3526499648, ___U3CFeaturesU3Ek__BackingField_1)); }
	inline List_1_t1283026911 * get_U3CFeaturesU3Ek__BackingField_1() const { return ___U3CFeaturesU3Ek__BackingField_1; }
	inline List_1_t1283026911 ** get_address_of_U3CFeaturesU3Ek__BackingField_1() { return &___U3CFeaturesU3Ek__BackingField_1; }
	inline void set_U3CFeaturesU3Ek__BackingField_1(List_1_t1283026911 * value)
	{
		___U3CFeaturesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFeaturesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAttributionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GeocodeResponse_t3526499648, ___U3CAttributionU3Ek__BackingField_2)); }
	inline String_t* get_U3CAttributionU3Ek__BackingField_2() const { return ___U3CAttributionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CAttributionU3Ek__BackingField_2() { return &___U3CAttributionU3Ek__BackingField_2; }
	inline void set_U3CAttributionU3Ek__BackingField_2(String_t* value)
	{
		___U3CAttributionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAttributionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOCODERESPONSE_T3526499648_H
#ifndef U3CCLEARU3EC__ANONSTOREY0_T1767456338_H
#define U3CCLEARU3EC__ANONSTOREY0_T1767456338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.MemoryCache/<Clear>c__AnonStorey0
struct  U3CClearU3Ec__AnonStorey0_t1767456338  : public RuntimeObject
{
public:
	// System.String Mapbox.Platform.Cache.MemoryCache/<Clear>c__AnonStorey0::mapId
	String_t* ___mapId_0;

public:
	inline static int32_t get_offset_of_mapId_0() { return static_cast<int32_t>(offsetof(U3CClearU3Ec__AnonStorey0_t1767456338, ___mapId_0)); }
	inline String_t* get_mapId_0() const { return ___mapId_0; }
	inline String_t** get_address_of_mapId_0() { return &___mapId_0; }
	inline void set_mapId_0(String_t* value)
	{
		___mapId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARU3EC__ANONSTOREY0_T1767456338_H
#ifndef MEMORYCACHE_T3711317483_H
#define MEMORYCACHE_T3711317483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.MemoryCache
struct  MemoryCache_t3711317483  : public RuntimeObject
{
public:
	// System.UInt32 Mapbox.Platform.Cache.MemoryCache::_maxCacheSize
	uint32_t ____maxCacheSize_0;
	// System.Object Mapbox.Platform.Cache.MemoryCache::_lock
	RuntimeObject * ____lock_1;
	// System.Collections.Generic.Dictionary`2<System.String,Mapbox.Platform.Cache.MemoryCache/CacheItem> Mapbox.Platform.Cache.MemoryCache::_cachedResponses
	Dictionary_2_t206654781 * ____cachedResponses_2;

public:
	inline static int32_t get_offset_of__maxCacheSize_0() { return static_cast<int32_t>(offsetof(MemoryCache_t3711317483, ____maxCacheSize_0)); }
	inline uint32_t get__maxCacheSize_0() const { return ____maxCacheSize_0; }
	inline uint32_t* get_address_of__maxCacheSize_0() { return &____maxCacheSize_0; }
	inline void set__maxCacheSize_0(uint32_t value)
	{
		____maxCacheSize_0 = value;
	}

	inline static int32_t get_offset_of__lock_1() { return static_cast<int32_t>(offsetof(MemoryCache_t3711317483, ____lock_1)); }
	inline RuntimeObject * get__lock_1() const { return ____lock_1; }
	inline RuntimeObject ** get_address_of__lock_1() { return &____lock_1; }
	inline void set__lock_1(RuntimeObject * value)
	{
		____lock_1 = value;
		Il2CppCodeGenWriteBarrier((&____lock_1), value);
	}

	inline static int32_t get_offset_of__cachedResponses_2() { return static_cast<int32_t>(offsetof(MemoryCache_t3711317483, ____cachedResponses_2)); }
	inline Dictionary_2_t206654781 * get__cachedResponses_2() const { return ____cachedResponses_2; }
	inline Dictionary_2_t206654781 ** get_address_of__cachedResponses_2() { return &____cachedResponses_2; }
	inline void set__cachedResponses_2(Dictionary_2_t206654781 * value)
	{
		____cachedResponses_2 = value;
		Il2CppCodeGenWriteBarrier((&____cachedResponses_2), value);
	}
};

struct MemoryCache_t3711317483_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Mapbox.Platform.Cache.MemoryCache/CacheItem>,System.Int64> Mapbox.Platform.Cache.MemoryCache::<>f__am$cache0
	Func_2_t4023812394 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(MemoryCache_t3711317483_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t4023812394 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t4023812394 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t4023812394 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYCACHE_T3711317483_H
#ifndef MBTILESCACHE_T1795842095_H
#define MBTILESCACHE_T1795842095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.MbTilesCache
struct  MbTilesCache_t1795842095  : public RuntimeObject
{
public:
	// System.Boolean Mapbox.Platform.Cache.MbTilesCache::_disposed
	bool ____disposed_0;
	// System.UInt32 Mapbox.Platform.Cache.MbTilesCache::_maxCacheSize
	uint32_t ____maxCacheSize_1;
	// System.Object Mapbox.Platform.Cache.MbTilesCache::_lock
	RuntimeObject * ____lock_2;
	// System.Collections.Generic.Dictionary`2<System.String,Mapbox.Platform.MbTiles.MbTilesDb> Mapbox.Platform.Cache.MbTilesCache::_mbTiles
	Dictionary_2_t2119965391 * ____mbTiles_3;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(MbTilesCache_t1795842095, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__maxCacheSize_1() { return static_cast<int32_t>(offsetof(MbTilesCache_t1795842095, ____maxCacheSize_1)); }
	inline uint32_t get__maxCacheSize_1() const { return ____maxCacheSize_1; }
	inline uint32_t* get_address_of__maxCacheSize_1() { return &____maxCacheSize_1; }
	inline void set__maxCacheSize_1(uint32_t value)
	{
		____maxCacheSize_1 = value;
	}

	inline static int32_t get_offset_of__lock_2() { return static_cast<int32_t>(offsetof(MbTilesCache_t1795842095, ____lock_2)); }
	inline RuntimeObject * get__lock_2() const { return ____lock_2; }
	inline RuntimeObject ** get_address_of__lock_2() { return &____lock_2; }
	inline void set__lock_2(RuntimeObject * value)
	{
		____lock_2 = value;
		Il2CppCodeGenWriteBarrier((&____lock_2), value);
	}

	inline static int32_t get_offset_of__mbTiles_3() { return static_cast<int32_t>(offsetof(MbTilesCache_t1795842095, ____mbTiles_3)); }
	inline Dictionary_2_t2119965391 * get__mbTiles_3() const { return ____mbTiles_3; }
	inline Dictionary_2_t2119965391 ** get_address_of__mbTiles_3() { return &____mbTiles_3; }
	inline void set__mbTiles_3(Dictionary_2_t2119965391 * value)
	{
		____mbTiles_3 = value;
		Il2CppCodeGenWriteBarrier((&____mbTiles_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MBTILESCACHE_T1795842095_H
#ifndef CONSTANTS_T3518929206_H
#define CONSTANTS_T3518929206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Constants
struct  Constants_t3518929206  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T3518929206_H
#ifndef BINDING_T2786839072_H
#define BINDING_T2786839072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteCommand/Binding
struct  Binding_t2786839072  : public RuntimeObject
{
public:
	// System.String SQLite4Unity3d.SQLiteCommand/Binding::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Object SQLite4Unity3d.SQLiteCommand/Binding::<Value>k__BackingField
	RuntimeObject * ___U3CValueU3Ek__BackingField_1;
	// System.Int32 SQLite4Unity3d.SQLiteCommand/Binding::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Binding_t2786839072, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Binding_t2786839072, ___U3CValueU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Binding_t2786839072, ___U3CIndexU3Ek__BackingField_2)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_2() const { return ___U3CIndexU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_2() { return &___U3CIndexU3Ek__BackingField_2; }
	inline void set_U3CIndexU3Ek__BackingField_2(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDING_T2786839072_H
#ifndef BASETABLEQUERY_T440437958_H
#define BASETABLEQUERY_T440437958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.BaseTableQuery
struct  BaseTableQuery_t440437958  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASETABLEQUERY_T440437958_H
#ifndef U3CNOTNULLCONSTRAINTVIOLATIONEXCEPTIONU3EC__ANONSTOREY0_T2023264365_H
#define U3CNOTNULLCONSTRAINTVIOLATIONEXCEPTIONU3EC__ANONSTOREY0_T2023264365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey0
struct  U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey0_t2023264365  : public RuntimeObject
{
public:
	// System.Object SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey0::obj
	RuntimeObject * ___obj_0;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey0_t2023264365, ___obj_0)); }
	inline RuntimeObject * get_obj_0() const { return ___obj_0; }
	inline RuntimeObject ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(RuntimeObject * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CNOTNULLCONSTRAINTVIOLATIONEXCEPTIONU3EC__ANONSTOREY0_T2023264365_H
#ifndef ORDERING_T647619184_H
#define ORDERING_T647619184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.BaseTableQuery/Ordering
struct  Ordering_t647619184  : public RuntimeObject
{
public:
	// System.String SQLite4Unity3d.BaseTableQuery/Ordering::<ColumnName>k__BackingField
	String_t* ___U3CColumnNameU3Ek__BackingField_0;
	// System.Boolean SQLite4Unity3d.BaseTableQuery/Ordering::<Ascending>k__BackingField
	bool ___U3CAscendingU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CColumnNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Ordering_t647619184, ___U3CColumnNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CColumnNameU3Ek__BackingField_0() const { return ___U3CColumnNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CColumnNameU3Ek__BackingField_0() { return &___U3CColumnNameU3Ek__BackingField_0; }
	inline void set_U3CColumnNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CColumnNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CColumnNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAscendingU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Ordering_t647619184, ___U3CAscendingU3Ek__BackingField_1)); }
	inline bool get_U3CAscendingU3Ek__BackingField_1() const { return ___U3CAscendingU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CAscendingU3Ek__BackingField_1() { return &___U3CAscendingU3Ek__BackingField_1; }
	inline void set_U3CAscendingU3Ek__BackingField_1(bool value)
	{
		___U3CAscendingU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDERING_T647619184_H
#ifndef SQLITE3_T985475636_H
#define SQLITE3_T985475636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLite3
struct  SQLite3_t985475636  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQLITE3_T985475636_H
#ifndef COMPRESSION_T3624971113_H
#define COMPRESSION_T3624971113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Compression
struct  Compression_t3624971113  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSION_T3624971113_H
#ifndef RESOURCE_T4129330135_H
#define RESOURCE_T4129330135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Resource
struct  Resource_t4129330135  : public RuntimeObject
{
public:

public:
};

struct Resource_t4129330135_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>> Mapbox.Platform.Resource::<>f__am$cache0
	Func_2_t3661664344 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>> Mapbox.Platform.Resource::<>f__am$cache1
	Func_2_t788283625 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>,System.String> Mapbox.Platform.Resource::<>f__am$cache2
	Func_2_t4279064255 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<<>__AnonType1`2<<>__AnonType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>,System.String>,System.String> Mapbox.Platform.Resource::<>f__am$cache3
	Func_2_t4279064255 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t3661664344 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t3661664344 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t3661664344 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t788283625 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t788283625 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t788283625 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t4279064255 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t4279064255 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t4279064255 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Resource_t4129330135_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t4279064255 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t4279064255 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t4279064255 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCE_T4129330135_H
#ifndef TILES_T2806189129_H
#define TILES_T2806189129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.MbTiles.tiles
struct  tiles_t2806189129  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Platform.MbTiles.tiles::<zoom_level>k__BackingField
	int32_t ___U3Czoom_levelU3Ek__BackingField_0;
	// System.Int64 Mapbox.Platform.MbTiles.tiles::<tile_column>k__BackingField
	int64_t ___U3Ctile_columnU3Ek__BackingField_1;
	// System.Int64 Mapbox.Platform.MbTiles.tiles::<tile_row>k__BackingField
	int64_t ___U3Ctile_rowU3Ek__BackingField_2;
	// System.Byte[] Mapbox.Platform.MbTiles.tiles::<tile_data>k__BackingField
	ByteU5BU5D_t4116647657* ___U3Ctile_dataU3Ek__BackingField_3;
	// System.Int32 Mapbox.Platform.MbTiles.tiles::<timestamp>k__BackingField
	int32_t ___U3CtimestampU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Czoom_levelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(tiles_t2806189129, ___U3Czoom_levelU3Ek__BackingField_0)); }
	inline int32_t get_U3Czoom_levelU3Ek__BackingField_0() const { return ___U3Czoom_levelU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Czoom_levelU3Ek__BackingField_0() { return &___U3Czoom_levelU3Ek__BackingField_0; }
	inline void set_U3Czoom_levelU3Ek__BackingField_0(int32_t value)
	{
		___U3Czoom_levelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Ctile_columnU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(tiles_t2806189129, ___U3Ctile_columnU3Ek__BackingField_1)); }
	inline int64_t get_U3Ctile_columnU3Ek__BackingField_1() const { return ___U3Ctile_columnU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3Ctile_columnU3Ek__BackingField_1() { return &___U3Ctile_columnU3Ek__BackingField_1; }
	inline void set_U3Ctile_columnU3Ek__BackingField_1(int64_t value)
	{
		___U3Ctile_columnU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Ctile_rowU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(tiles_t2806189129, ___U3Ctile_rowU3Ek__BackingField_2)); }
	inline int64_t get_U3Ctile_rowU3Ek__BackingField_2() const { return ___U3Ctile_rowU3Ek__BackingField_2; }
	inline int64_t* get_address_of_U3Ctile_rowU3Ek__BackingField_2() { return &___U3Ctile_rowU3Ek__BackingField_2; }
	inline void set_U3Ctile_rowU3Ek__BackingField_2(int64_t value)
	{
		___U3Ctile_rowU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Ctile_dataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(tiles_t2806189129, ___U3Ctile_dataU3Ek__BackingField_3)); }
	inline ByteU5BU5D_t4116647657* get_U3Ctile_dataU3Ek__BackingField_3() const { return ___U3Ctile_dataU3Ek__BackingField_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3Ctile_dataU3Ek__BackingField_3() { return &___U3Ctile_dataU3Ek__BackingField_3; }
	inline void set_U3Ctile_dataU3Ek__BackingField_3(ByteU5BU5D_t4116647657* value)
	{
		___U3Ctile_dataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctile_dataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtimestampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(tiles_t2806189129, ___U3CtimestampU3Ek__BackingField_4)); }
	inline int32_t get_U3CtimestampU3Ek__BackingField_4() const { return ___U3CtimestampU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CtimestampU3Ek__BackingField_4() { return &___U3CtimestampU3Ek__BackingField_4; }
	inline void set_U3CtimestampU3Ek__BackingField_4(int32_t value)
	{
		___U3CtimestampU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILES_T2806189129_H
#ifndef METADATAREQUIRED_T3242735516_H
#define METADATAREQUIRED_T3242735516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.MbTiles.MetaDataRequired
struct  MetaDataRequired_t3242735516  : public RuntimeObject
{
public:
	// System.String Mapbox.Platform.MbTiles.MetaDataRequired::<TilesetName>k__BackingField
	String_t* ___U3CTilesetNameU3Ek__BackingField_0;
	// System.String Mapbox.Platform.MbTiles.MetaDataRequired::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.Int32 Mapbox.Platform.MbTiles.MetaDataRequired::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_2;
	// System.String Mapbox.Platform.MbTiles.MetaDataRequired::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_3;
	// System.String Mapbox.Platform.MbTiles.MetaDataRequired::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CTilesetNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MetaDataRequired_t3242735516, ___U3CTilesetNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CTilesetNameU3Ek__BackingField_0() const { return ___U3CTilesetNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTilesetNameU3Ek__BackingField_0() { return &___U3CTilesetNameU3Ek__BackingField_0; }
	inline void set_U3CTilesetNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CTilesetNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTilesetNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetaDataRequired_t3242735516, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MetaDataRequired_t3242735516, ___U3CVersionU3Ek__BackingField_2)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_2() const { return ___U3CVersionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_2() { return &___U3CVersionU3Ek__BackingField_2; }
	inline void set_U3CVersionU3Ek__BackingField_2(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MetaDataRequired_t3242735516, ___U3CDescriptionU3Ek__BackingField_3)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_3() const { return ___U3CDescriptionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_3() { return &___U3CDescriptionU3Ek__BackingField_3; }
	inline void set_U3CDescriptionU3Ek__BackingField_3(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MetaDataRequired_t3242735516, ___U3CFormatU3Ek__BackingField_4)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_4() const { return ___U3CFormatU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_4() { return &___U3CFormatU3Ek__BackingField_4; }
	inline void set_U3CFormatU3Ek__BackingField_4(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAREQUIRED_T3242735516_H
#ifndef METADATA_T2312331196_H
#define METADATA_T2312331196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.MbTiles.metadata
struct  metadata_t2312331196  : public RuntimeObject
{
public:
	// System.String Mapbox.Platform.MbTiles.metadata::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String Mapbox.Platform.MbTiles.metadata::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(metadata_t2312331196, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(metadata_t2312331196, ___U3CvalueU3Ek__BackingField_1)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_1() const { return ___U3CvalueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_1() { return &___U3CvalueU3Ek__BackingField_1; }
	inline void set_U3CvalueU3Ek__BackingField_1(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATA_T2312331196_H
#ifndef U3CDELAYCACHEDRESPONSEU3EC__ITERATOR0_T4244230405_H
#define U3CDELAYCACHEDRESPONSEU3EC__ITERATOR0_T4244230405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.CachingWebFileSource/<DelayCachedResponse>c__Iterator0
struct  U3CDelayCachedResponseU3Ec__Iterator0_t4244230405  : public RuntimeObject
{
public:
	// System.Action`1<Mapbox.Platform.Response> Mapbox.Platform.Cache.CachingWebFileSource/<DelayCachedResponse>c__Iterator0::callback
	Action_1_t1228140472 * ___callback_0;
	// Mapbox.Platform.Response Mapbox.Platform.Cache.CachingWebFileSource/<DelayCachedResponse>c__Iterator0::response
	Response_t1055672877 * ___response_1;
	// System.Object Mapbox.Platform.Cache.CachingWebFileSource/<DelayCachedResponse>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Mapbox.Platform.Cache.CachingWebFileSource/<DelayCachedResponse>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Mapbox.Platform.Cache.CachingWebFileSource/<DelayCachedResponse>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CDelayCachedResponseU3Ec__Iterator0_t4244230405, ___callback_0)); }
	inline Action_1_t1228140472 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t1228140472 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t1228140472 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(U3CDelayCachedResponseU3Ec__Iterator0_t4244230405, ___response_1)); }
	inline Response_t1055672877 * get_response_1() const { return ___response_1; }
	inline Response_t1055672877 ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(Response_t1055672877 * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayCachedResponseU3Ec__Iterator0_t4244230405, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayCachedResponseU3Ec__Iterator0_t4244230405, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayCachedResponseU3Ec__Iterator0_t4244230405, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYCACHEDRESPONSEU3EC__ITERATOR0_T4244230405_H
#ifndef U3CUPDATEU3EC__ANONSTOREY3_T445741020_H
#define U3CUPDATEU3EC__ANONSTOREY3_T445741020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/<Update>c__AnonStorey3
struct  U3CUpdateU3Ec__AnonStorey3_t445741020  : public RuntimeObject
{
public:
	// SQLite4Unity3d.TableMapping/Column SQLite4Unity3d.SQLiteConnection/<Update>c__AnonStorey3::pk
	Column_t1357940583 * ___pk_0;
	// System.Object SQLite4Unity3d.SQLiteConnection/<Update>c__AnonStorey3::obj
	RuntimeObject * ___obj_1;

public:
	inline static int32_t get_offset_of_pk_0() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey3_t445741020, ___pk_0)); }
	inline Column_t1357940583 * get_pk_0() const { return ___pk_0; }
	inline Column_t1357940583 ** get_address_of_pk_0() { return &___pk_0; }
	inline void set_pk_0(Column_t1357940583 * value)
	{
		___pk_0 = value;
		Il2CppCodeGenWriteBarrier((&___pk_0), value);
	}

	inline static int32_t get_offset_of_obj_1() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey3_t445741020, ___obj_1)); }
	inline RuntimeObject * get_obj_1() const { return ___obj_1; }
	inline RuntimeObject ** get_address_of_obj_1() { return &___obj_1; }
	inline void set_obj_1(RuntimeObject * value)
	{
		___obj_1 = value;
		Il2CppCodeGenWriteBarrier((&___obj_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEU3EC__ANONSTOREY3_T445741020_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef U3CUPDATEALLU3EC__ANONSTOREY4_T1584503381_H
#define U3CUPDATEALLU3EC__ANONSTOREY4_T1584503381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey4
struct  U3CUpdateAllU3Ec__AnonStorey4_t1584503381  : public RuntimeObject
{
public:
	// System.Collections.IEnumerable SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey4::objects
	RuntimeObject* ___objects_0;
	// System.Int32 SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey4::c
	int32_t ___c_1;
	// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey4::$this
	SQLiteConnection_t3070079188 * ___U24this_2;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(U3CUpdateAllU3Ec__AnonStorey4_t1584503381, ___objects_0)); }
	inline RuntimeObject* get_objects_0() const { return ___objects_0; }
	inline RuntimeObject** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(RuntimeObject* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___objects_0), value);
	}

	inline static int32_t get_offset_of_c_1() { return static_cast<int32_t>(offsetof(U3CUpdateAllU3Ec__AnonStorey4_t1584503381, ___c_1)); }
	inline int32_t get_c_1() const { return ___c_1; }
	inline int32_t* get_address_of_c_1() { return &___c_1; }
	inline void set_c_1(int32_t value)
	{
		___c_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CUpdateAllU3Ec__AnonStorey4_t1584503381, ___U24this_2)); }
	inline SQLiteConnection_t3070079188 * get_U24this_2() const { return ___U24this_2; }
	inline SQLiteConnection_t3070079188 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SQLiteConnection_t3070079188 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEALLU3EC__ANONSTOREY4_T1584503381_H
#ifndef SQLITECONNECTIONSTRING_T3632030467_H
#define SQLITECONNECTIONSTRING_T3632030467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnectionString
struct  SQLiteConnectionString_t3632030467  : public RuntimeObject
{
public:
	// System.String SQLite4Unity3d.SQLiteConnectionString::<ConnectionString>k__BackingField
	String_t* ___U3CConnectionStringU3Ek__BackingField_0;
	// System.String SQLite4Unity3d.SQLiteConnectionString::<DatabasePath>k__BackingField
	String_t* ___U3CDatabasePathU3Ek__BackingField_1;
	// System.Boolean SQLite4Unity3d.SQLiteConnectionString::<StoreDateTimeAsTicks>k__BackingField
	bool ___U3CStoreDateTimeAsTicksU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CConnectionStringU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SQLiteConnectionString_t3632030467, ___U3CConnectionStringU3Ek__BackingField_0)); }
	inline String_t* get_U3CConnectionStringU3Ek__BackingField_0() const { return ___U3CConnectionStringU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CConnectionStringU3Ek__BackingField_0() { return &___U3CConnectionStringU3Ek__BackingField_0; }
	inline void set_U3CConnectionStringU3Ek__BackingField_0(String_t* value)
	{
		___U3CConnectionStringU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionStringU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDatabasePathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SQLiteConnectionString_t3632030467, ___U3CDatabasePathU3Ek__BackingField_1)); }
	inline String_t* get_U3CDatabasePathU3Ek__BackingField_1() const { return ___U3CDatabasePathU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDatabasePathU3Ek__BackingField_1() { return &___U3CDatabasePathU3Ek__BackingField_1; }
	inline void set_U3CDatabasePathU3Ek__BackingField_1(String_t* value)
	{
		___U3CDatabasePathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDatabasePathU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SQLiteConnectionString_t3632030467, ___U3CStoreDateTimeAsTicksU3Ek__BackingField_2)); }
	inline bool get_U3CStoreDateTimeAsTicksU3Ek__BackingField_2() const { return ___U3CStoreDateTimeAsTicksU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_2() { return &___U3CStoreDateTimeAsTicksU3Ek__BackingField_2; }
	inline void set_U3CStoreDateTimeAsTicksU3Ek__BackingField_2(bool value)
	{
		___U3CStoreDateTimeAsTicksU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQLITECONNECTIONSTRING_T3632030467_H
#ifndef MAPUTILS_T115449649_H
#define MAPUTILS_T115449649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.MapUtils
struct  MapUtils_t115449649  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPUTILS_T115449649_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef TABLEMAPPING_T2276399492_H
#define TABLEMAPPING_T2276399492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.TableMapping
struct  TableMapping_t2276399492  : public RuntimeObject
{
public:
	// System.Type SQLite4Unity3d.TableMapping::<MappedType>k__BackingField
	Type_t * ___U3CMappedTypeU3Ek__BackingField_0;
	// System.String SQLite4Unity3d.TableMapping::<TableName>k__BackingField
	String_t* ___U3CTableNameU3Ek__BackingField_1;
	// SQLite4Unity3d.TableMapping/Column[] SQLite4Unity3d.TableMapping::<Columns>k__BackingField
	ColumnU5BU5D_t4060676894* ___U3CColumnsU3Ek__BackingField_2;
	// SQLite4Unity3d.TableMapping/Column SQLite4Unity3d.TableMapping::<PK>k__BackingField
	Column_t1357940583 * ___U3CPKU3Ek__BackingField_3;
	// System.String SQLite4Unity3d.TableMapping::<GetByPrimaryKeySql>k__BackingField
	String_t* ___U3CGetByPrimaryKeySqlU3Ek__BackingField_4;
	// SQLite4Unity3d.TableMapping/Column SQLite4Unity3d.TableMapping::_autoPk
	Column_t1357940583 * ____autoPk_5;
	// SQLite4Unity3d.TableMapping/Column[] SQLite4Unity3d.TableMapping::_insertColumns
	ColumnU5BU5D_t4060676894* ____insertColumns_6;
	// SQLite4Unity3d.TableMapping/Column[] SQLite4Unity3d.TableMapping::_insertOrReplaceColumns
	ColumnU5BU5D_t4060676894* ____insertOrReplaceColumns_7;
	// System.Boolean SQLite4Unity3d.TableMapping::<HasAutoIncPK>k__BackingField
	bool ___U3CHasAutoIncPKU3Ek__BackingField_8;
	// SQLite4Unity3d.PreparedSqlLiteInsertCommand SQLite4Unity3d.TableMapping::_insertCommand
	PreparedSqlLiteInsertCommand_t994658783 * ____insertCommand_9;
	// System.String SQLite4Unity3d.TableMapping::_insertCommandExtra
	String_t* ____insertCommandExtra_10;

public:
	inline static int32_t get_offset_of_U3CMappedTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ___U3CMappedTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CMappedTypeU3Ek__BackingField_0() const { return ___U3CMappedTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CMappedTypeU3Ek__BackingField_0() { return &___U3CMappedTypeU3Ek__BackingField_0; }
	inline void set_U3CMappedTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CMappedTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMappedTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTableNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ___U3CTableNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CTableNameU3Ek__BackingField_1() const { return ___U3CTableNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTableNameU3Ek__BackingField_1() { return &___U3CTableNameU3Ek__BackingField_1; }
	inline void set_U3CTableNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CTableNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTableNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CColumnsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ___U3CColumnsU3Ek__BackingField_2)); }
	inline ColumnU5BU5D_t4060676894* get_U3CColumnsU3Ek__BackingField_2() const { return ___U3CColumnsU3Ek__BackingField_2; }
	inline ColumnU5BU5D_t4060676894** get_address_of_U3CColumnsU3Ek__BackingField_2() { return &___U3CColumnsU3Ek__BackingField_2; }
	inline void set_U3CColumnsU3Ek__BackingField_2(ColumnU5BU5D_t4060676894* value)
	{
		___U3CColumnsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CColumnsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CPKU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ___U3CPKU3Ek__BackingField_3)); }
	inline Column_t1357940583 * get_U3CPKU3Ek__BackingField_3() const { return ___U3CPKU3Ek__BackingField_3; }
	inline Column_t1357940583 ** get_address_of_U3CPKU3Ek__BackingField_3() { return &___U3CPKU3Ek__BackingField_3; }
	inline void set_U3CPKU3Ek__BackingField_3(Column_t1357940583 * value)
	{
		___U3CPKU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPKU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CGetByPrimaryKeySqlU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ___U3CGetByPrimaryKeySqlU3Ek__BackingField_4)); }
	inline String_t* get_U3CGetByPrimaryKeySqlU3Ek__BackingField_4() const { return ___U3CGetByPrimaryKeySqlU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CGetByPrimaryKeySqlU3Ek__BackingField_4() { return &___U3CGetByPrimaryKeySqlU3Ek__BackingField_4; }
	inline void set_U3CGetByPrimaryKeySqlU3Ek__BackingField_4(String_t* value)
	{
		___U3CGetByPrimaryKeySqlU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetByPrimaryKeySqlU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__autoPk_5() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ____autoPk_5)); }
	inline Column_t1357940583 * get__autoPk_5() const { return ____autoPk_5; }
	inline Column_t1357940583 ** get_address_of__autoPk_5() { return &____autoPk_5; }
	inline void set__autoPk_5(Column_t1357940583 * value)
	{
		____autoPk_5 = value;
		Il2CppCodeGenWriteBarrier((&____autoPk_5), value);
	}

	inline static int32_t get_offset_of__insertColumns_6() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ____insertColumns_6)); }
	inline ColumnU5BU5D_t4060676894* get__insertColumns_6() const { return ____insertColumns_6; }
	inline ColumnU5BU5D_t4060676894** get_address_of__insertColumns_6() { return &____insertColumns_6; }
	inline void set__insertColumns_6(ColumnU5BU5D_t4060676894* value)
	{
		____insertColumns_6 = value;
		Il2CppCodeGenWriteBarrier((&____insertColumns_6), value);
	}

	inline static int32_t get_offset_of__insertOrReplaceColumns_7() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ____insertOrReplaceColumns_7)); }
	inline ColumnU5BU5D_t4060676894* get__insertOrReplaceColumns_7() const { return ____insertOrReplaceColumns_7; }
	inline ColumnU5BU5D_t4060676894** get_address_of__insertOrReplaceColumns_7() { return &____insertOrReplaceColumns_7; }
	inline void set__insertOrReplaceColumns_7(ColumnU5BU5D_t4060676894* value)
	{
		____insertOrReplaceColumns_7 = value;
		Il2CppCodeGenWriteBarrier((&____insertOrReplaceColumns_7), value);
	}

	inline static int32_t get_offset_of_U3CHasAutoIncPKU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ___U3CHasAutoIncPKU3Ek__BackingField_8)); }
	inline bool get_U3CHasAutoIncPKU3Ek__BackingField_8() const { return ___U3CHasAutoIncPKU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CHasAutoIncPKU3Ek__BackingField_8() { return &___U3CHasAutoIncPKU3Ek__BackingField_8; }
	inline void set_U3CHasAutoIncPKU3Ek__BackingField_8(bool value)
	{
		___U3CHasAutoIncPKU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of__insertCommand_9() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ____insertCommand_9)); }
	inline PreparedSqlLiteInsertCommand_t994658783 * get__insertCommand_9() const { return ____insertCommand_9; }
	inline PreparedSqlLiteInsertCommand_t994658783 ** get_address_of__insertCommand_9() { return &____insertCommand_9; }
	inline void set__insertCommand_9(PreparedSqlLiteInsertCommand_t994658783 * value)
	{
		____insertCommand_9 = value;
		Il2CppCodeGenWriteBarrier((&____insertCommand_9), value);
	}

	inline static int32_t get_offset_of__insertCommandExtra_10() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492, ____insertCommandExtra_10)); }
	inline String_t* get__insertCommandExtra_10() const { return ____insertCommandExtra_10; }
	inline String_t** get_address_of__insertCommandExtra_10() { return &____insertCommandExtra_10; }
	inline void set__insertCommandExtra_10(String_t* value)
	{
		____insertCommandExtra_10 = value;
		Il2CppCodeGenWriteBarrier((&____insertCommandExtra_10), value);
	}
};

struct TableMapping_t2276399492_StaticFields
{
public:
	// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.Boolean> SQLite4Unity3d.TableMapping::<>f__am$cache0
	Func_2_t681049744 * ___U3CU3Ef__amU24cache0_11;
	// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.String> SQLite4Unity3d.TableMapping::<>f__am$cache1
	Func_2_t2431212468 * ___U3CU3Ef__amU24cache1_12;
	// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.String> SQLite4Unity3d.TableMapping::<>f__am$cache2
	Func_2_t2431212468 * ___U3CU3Ef__amU24cache2_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_2_t681049744 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_2_t681049744 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_2_t681049744 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline Func_2_t2431212468 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline Func_2_t2431212468 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(Func_2_t2431212468 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_13() { return static_cast<int32_t>(offsetof(TableMapping_t2276399492_StaticFields, ___U3CU3Ef__amU24cache2_13)); }
	inline Func_2_t2431212468 * get_U3CU3Ef__amU24cache2_13() const { return ___U3CU3Ef__amU24cache2_13; }
	inline Func_2_t2431212468 ** get_address_of_U3CU3Ef__amU24cache2_13() { return &___U3CU3Ef__amU24cache2_13; }
	inline void set_U3CU3Ef__amU24cache2_13(Func_2_t2431212468 * value)
	{
		___U3CU3Ef__amU24cache2_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEMAPPING_T2276399492_H
#ifndef JSONCONVERTER_T472504469_H
#define JSONCONVERTER_T472504469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonConverter
struct  JsonConverter_t472504469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T472504469_H
#ifndef U3CINSERTALLU3EC__ANONSTOREY2_T106977601_H
#define U3CINSERTALLU3EC__ANONSTOREY2_T106977601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey2
struct  U3CInsertAllU3Ec__AnonStorey2_t106977601  : public RuntimeObject
{
public:
	// System.Collections.IEnumerable SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey2::objects
	RuntimeObject* ___objects_0;
	// System.Type SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey2::objType
	Type_t * ___objType_1;
	// System.Int32 SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey2::c
	int32_t ___c_2;
	// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey2::$this
	SQLiteConnection_t3070079188 * ___U24this_3;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey2_t106977601, ___objects_0)); }
	inline RuntimeObject* get_objects_0() const { return ___objects_0; }
	inline RuntimeObject** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(RuntimeObject* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___objects_0), value);
	}

	inline static int32_t get_offset_of_objType_1() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey2_t106977601, ___objType_1)); }
	inline Type_t * get_objType_1() const { return ___objType_1; }
	inline Type_t ** get_address_of_objType_1() { return &___objType_1; }
	inline void set_objType_1(Type_t * value)
	{
		___objType_1 = value;
		Il2CppCodeGenWriteBarrier((&___objType_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey2_t106977601, ___c_2)); }
	inline int32_t get_c_2() const { return ___c_2; }
	inline int32_t* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(int32_t value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey2_t106977601, ___U24this_3)); }
	inline SQLiteConnection_t3070079188 * get_U24this_3() const { return ___U24this_3; }
	inline SQLiteConnection_t3070079188 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SQLiteConnection_t3070079188 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSERTALLU3EC__ANONSTOREY2_T106977601_H
#ifndef U3CFINDCOLUMNWITHPROPERTYNAMEU3EC__ANONSTOREY0_T3952487827_H
#define U3CFINDCOLUMNWITHPROPERTYNAMEU3EC__ANONSTOREY0_T3952487827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.TableMapping/<FindColumnWithPropertyName>c__AnonStorey0
struct  U3CFindColumnWithPropertyNameU3Ec__AnonStorey0_t3952487827  : public RuntimeObject
{
public:
	// System.String SQLite4Unity3d.TableMapping/<FindColumnWithPropertyName>c__AnonStorey0::propertyName
	String_t* ___propertyName_0;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(U3CFindColumnWithPropertyNameU3Ec__AnonStorey0_t3952487827, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDCOLUMNWITHPROPERTYNAMEU3EC__ANONSTOREY0_T3952487827_H
#ifndef MEMORYCACHEASYNCREQUEST_T3045496022_H
#define MEMORYCACHEASYNCREQUEST_T3045496022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.CachingWebFileSource/MemoryCacheAsyncRequest
struct  MemoryCacheAsyncRequest_t3045496022  : public RuntimeObject
{
public:
	// System.String Mapbox.Platform.Cache.CachingWebFileSource/MemoryCacheAsyncRequest::<RequestUrl>k__BackingField
	String_t* ___U3CRequestUrlU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRequestUrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MemoryCacheAsyncRequest_t3045496022, ___U3CRequestUrlU3Ek__BackingField_0)); }
	inline String_t* get_U3CRequestUrlU3Ek__BackingField_0() const { return ___U3CRequestUrlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CRequestUrlU3Ek__BackingField_0() { return &___U3CRequestUrlU3Ek__BackingField_0; }
	inline void set_U3CRequestUrlU3Ek__BackingField_0(String_t* value)
	{
		___U3CRequestUrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestUrlU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYCACHEASYNCREQUEST_T3045496022_H
#ifndef CACHINGWEBFILESOURCE_T328893056_H
#define CACHINGWEBFILESOURCE_T328893056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.CachingWebFileSource
struct  CachingWebFileSource_t328893056  : public RuntimeObject
{
public:
	// System.Boolean Mapbox.Platform.Cache.CachingWebFileSource::_disposed
	bool ____disposed_0;
	// System.Collections.Generic.List`1<Mapbox.Platform.Cache.ICache> Mapbox.Platform.Cache.CachingWebFileSource::_caches
	List_1_t4166699764 * ____caches_1;
	// System.String Mapbox.Platform.Cache.CachingWebFileSource::_accessToken
	String_t* ____accessToken_2;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(CachingWebFileSource_t328893056, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__caches_1() { return static_cast<int32_t>(offsetof(CachingWebFileSource_t328893056, ____caches_1)); }
	inline List_1_t4166699764 * get__caches_1() const { return ____caches_1; }
	inline List_1_t4166699764 ** get_address_of__caches_1() { return &____caches_1; }
	inline void set__caches_1(List_1_t4166699764 * value)
	{
		____caches_1 = value;
		Il2CppCodeGenWriteBarrier((&____caches_1), value);
	}

	inline static int32_t get_offset_of__accessToken_2() { return static_cast<int32_t>(offsetof(CachingWebFileSource_t328893056, ____accessToken_2)); }
	inline String_t* get__accessToken_2() const { return ____accessToken_2; }
	inline String_t** get_address_of__accessToken_2() { return &____accessToken_2; }
	inline void set__accessToken_2(String_t* value)
	{
		____accessToken_2 = value;
		Il2CppCodeGenWriteBarrier((&____accessToken_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGWEBFILESOURCE_T328893056_H
#ifndef COLUMNINFO_T2560133827_H
#define COLUMNINFO_T2560133827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/ColumnInfo
struct  ColumnInfo_t2560133827  : public RuntimeObject
{
public:
	// System.String SQLite4Unity3d.SQLiteConnection/ColumnInfo::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 SQLite4Unity3d.SQLiteConnection/ColumnInfo::<notnull>k__BackingField
	int32_t ___U3CnotnullU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ColumnInfo_t2560133827, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnotnullU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ColumnInfo_t2560133827, ___U3CnotnullU3Ek__BackingField_1)); }
	inline int32_t get_U3CnotnullU3Ek__BackingField_1() const { return ___U3CnotnullU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CnotnullU3Ek__BackingField_1() { return &___U3CnotnullU3Ek__BackingField_1; }
	inline void set_U3CnotnullU3Ek__BackingField_1(int32_t value)
	{
		___U3CnotnullU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMNINFO_T2560133827_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CINSERTALLU3EC__ANONSTOREY1_T106977604_H
#define U3CINSERTALLU3EC__ANONSTOREY1_T106977604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey1
struct  U3CInsertAllU3Ec__AnonStorey1_t106977604  : public RuntimeObject
{
public:
	// System.Collections.IEnumerable SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey1::objects
	RuntimeObject* ___objects_0;
	// System.String SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey1::extra
	String_t* ___extra_1;
	// System.Int32 SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey1::c
	int32_t ___c_2;
	// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey1::$this
	SQLiteConnection_t3070079188 * ___U24this_3;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey1_t106977604, ___objects_0)); }
	inline RuntimeObject* get_objects_0() const { return ___objects_0; }
	inline RuntimeObject** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(RuntimeObject* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___objects_0), value);
	}

	inline static int32_t get_offset_of_extra_1() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey1_t106977604, ___extra_1)); }
	inline String_t* get_extra_1() const { return ___extra_1; }
	inline String_t** get_address_of_extra_1() { return &___extra_1; }
	inline void set_extra_1(String_t* value)
	{
		___extra_1 = value;
		Il2CppCodeGenWriteBarrier((&___extra_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey1_t106977604, ___c_2)); }
	inline int32_t get_c_2() const { return ___c_2; }
	inline int32_t* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(int32_t value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey1_t106977604, ___U24this_3)); }
	inline SQLiteConnection_t3070079188 * get_U24this_3() const { return ___U24this_3; }
	inline SQLiteConnection_t3070079188 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SQLiteConnection_t3070079188 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSERTALLU3EC__ANONSTOREY1_T106977604_H
#ifndef ORM_T3091323797_H
#define ORM_T3091323797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.Orm
struct  Orm_t3091323797  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORM_T3091323797_H
#ifndef U3CINSERTALLU3EC__ANONSTOREY0_T106977603_H
#define U3CINSERTALLU3EC__ANONSTOREY0_T106977603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey0
struct  U3CInsertAllU3Ec__AnonStorey0_t106977603  : public RuntimeObject
{
public:
	// System.Collections.IEnumerable SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey0::objects
	RuntimeObject* ___objects_0;
	// System.Int32 SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey0::c
	int32_t ___c_1;
	// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey0::$this
	SQLiteConnection_t3070079188 * ___U24this_2;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey0_t106977603, ___objects_0)); }
	inline RuntimeObject* get_objects_0() const { return ___objects_0; }
	inline RuntimeObject** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(RuntimeObject* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___objects_0), value);
	}

	inline static int32_t get_offset_of_c_1() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey0_t106977603, ___c_1)); }
	inline int32_t get_c_1() const { return ___c_1; }
	inline int32_t* get_address_of_c_1() { return &___c_1; }
	inline void set_c_1(int32_t value)
	{
		___c_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey0_t106977603, ___U24this_2)); }
	inline SQLiteConnection_t3070079188 * get_U24this_2() const { return ___U24this_2; }
	inline SQLiteConnection_t3070079188 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SQLiteConnection_t3070079188 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSERTALLU3EC__ANONSTOREY0_T106977603_H
#ifndef TILECOVER_T2200805285_H
#define TILECOVER_T2200805285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.TileCover
struct  TileCover_t2200805285  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILECOVER_T2200805285_H
#ifndef TILERESOURCE_T4039806838_H
#define TILERESOURCE_T4039806838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.TileResource
struct  TileResource_t4039806838  : public RuntimeObject
{
public:
	// System.String Mapbox.Map.TileResource::_query
	String_t* ____query_1;

public:
	inline static int32_t get_offset_of__query_1() { return static_cast<int32_t>(offsetof(TileResource_t4039806838, ____query_1)); }
	inline String_t* get__query_1() const { return ____query_1; }
	inline String_t** get_address_of__query_1() { return &____query_1; }
	inline void set__query_1(String_t* value)
	{
		____query_1 = value;
		Il2CppCodeGenWriteBarrier((&____query_1), value);
	}
};

struct TileResource_t4039806838_StaticFields
{
public:
	// System.String Mapbox.Map.TileResource::_eventQuery
	String_t* ____eventQuery_0;

public:
	inline static int32_t get_offset_of__eventQuery_0() { return static_cast<int32_t>(offsetof(TileResource_t4039806838_StaticFields, ____eventQuery_0)); }
	inline String_t* get__eventQuery_0() const { return ____eventQuery_0; }
	inline String_t** get_address_of__eventQuery_0() { return &____eventQuery_0; }
	inline void set__eventQuery_0(String_t* value)
	{
		____eventQuery_0 = value;
		Il2CppCodeGenWriteBarrier((&____eventQuery_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILERESOURCE_T4039806838_H
#ifndef COLLATIONATTRIBUTE_T3564781786_H
#define COLLATIONATTRIBUTE_T3564781786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.CollationAttribute
struct  CollationAttribute_t3564781786  : public Attribute_t861562559
{
public:
	// System.String SQLite4Unity3d.CollationAttribute::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollationAttribute_t3564781786, ___U3CValueU3Ek__BackingField_0)); }
	inline String_t* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(String_t* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLATIONATTRIBUTE_T3564781786_H
#ifndef MAXLENGTHATTRIBUTE_T2329480580_H
#define MAXLENGTHATTRIBUTE_T2329480580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.MaxLengthAttribute
struct  MaxLengthAttribute_t2329480580  : public Attribute_t861562559
{
public:
	// System.Int32 SQLite4Unity3d.MaxLengthAttribute::<Value>k__BackingField
	int32_t ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaxLengthAttribute_t2329480580, ___U3CValueU3Ek__BackingField_0)); }
	inline int32_t get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(int32_t value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXLENGTHATTRIBUTE_T2329480580_H
#ifndef NOTNULLATTRIBUTE_T1344045378_H
#define NOTNULLATTRIBUTE_T1344045378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.NotNullAttribute
struct  NotNullAttribute_t1344045378  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTNULLATTRIBUTE_T1344045378_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T2317227445_H
#define NULLABLE_1_T2317227445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Double>
struct  Nullable_1_t2317227445 
{
public:
	// T System.Nullable`1::value
	double ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2317227445, ___value_0)); }
	inline double get_value_0() const { return ___value_0; }
	inline double* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(double value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2317227445, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2317227445_H
#ifndef CUSTOMCREATIONCONVERTER_1_T241653040_H
#define CUSTOMCREATIONCONVERTER_1_T241653040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.CustomCreationConverter`1<Mapbox.Utils.Vector2dBounds>
struct  CustomCreationConverter_1_t241653040  : public JsonConverter_t472504469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCREATIONCONVERTER_1_T241653040_H
#ifndef CUSTOMCREATIONCONVERTER_1_T132058663_H
#define CUSTOMCREATIONCONVERTER_1_T132058663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.CustomCreationConverter`1<Mapbox.Utils.Vector2d>
struct  CustomCreationConverter_1_t132058663  : public JsonConverter_t472504469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCREATIONCONVERTER_1_T132058663_H
#ifndef CUSTOMCREATIONCONVERTER_1_T1604133405_H
#define CUSTOMCREATIONCONVERTER_1_T1604133405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.CustomCreationConverter`1<System.Collections.Generic.List`1<Mapbox.Utils.Vector2d>>
struct  CustomCreationConverter_1_t1604133405  : public JsonConverter_t472504469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCREATIONCONVERTER_1_T1604133405_H
#ifndef NULLABLE_1_T4282624060_H
#define NULLABLE_1_T4282624060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.UInt32>
struct  Nullable_1_t4282624060 
{
public:
	// T System.Nullable`1::value
	uint32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4282624060, ___value_0)); }
	inline uint32_t get_value_0() const { return ___value_0; }
	inline uint32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4282624060, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4282624060_H
#ifndef REVERSEGEOCODERESPONSE_T3723437562_H
#define REVERSEGEOCODERESPONSE_T3723437562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.ReverseGeocodeResponse
struct  ReverseGeocodeResponse_t3723437562  : public GeocodeResponse_t3526499648
{
public:
	// System.Collections.Generic.List`1<System.Double> Mapbox.Geocoding.ReverseGeocodeResponse::<Query>k__BackingField
	List_1_t2066740105 * ___U3CQueryU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CQueryU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ReverseGeocodeResponse_t3723437562, ___U3CQueryU3Ek__BackingField_3)); }
	inline List_1_t2066740105 * get_U3CQueryU3Ek__BackingField_3() const { return ___U3CQueryU3Ek__BackingField_3; }
	inline List_1_t2066740105 ** get_address_of_U3CQueryU3Ek__BackingField_3() { return &___U3CQueryU3Ek__BackingField_3; }
	inline void set_U3CQueryU3Ek__BackingField_3(List_1_t2066740105 * value)
	{
		___U3CQueryU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQueryU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVERSEGEOCODERESPONSE_T3723437562_H
#ifndef VECTOR2D_T1865246568_H
#define VECTOR2D_T1865246568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Vector2d
struct  Vector2d_t1865246568 
{
public:
	// System.Double Mapbox.Utils.Vector2d::x
	double ___x_1;
	// System.Double Mapbox.Utils.Vector2d::y
	double ___y_2;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___x_1)); }
	inline double get_x_1() const { return ___x_1; }
	inline double* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(double value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___y_2)); }
	inline double get_y_2() const { return ___y_2; }
	inline double* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(double value)
	{
		___y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2D_T1865246568_H
#ifndef GEOCODERESOURCE_1_T2691665636_H
#define GEOCODERESOURCE_1_T2691665636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.GeocodeResource`1<Mapbox.Utils.Vector2d>
struct  GeocodeResource_1_t2691665636  : public Resource_t4129330135
{
public:
	// System.String Mapbox.Geocoding.GeocodeResource`1::apiEndpoint
	String_t* ___apiEndpoint_5;
	// System.String Mapbox.Geocoding.GeocodeResource`1::mode
	String_t* ___mode_6;
	// System.String[] Mapbox.Geocoding.GeocodeResource`1::types
	StringU5BU5D_t1281789340* ___types_7;

public:
	inline static int32_t get_offset_of_apiEndpoint_5() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2691665636, ___apiEndpoint_5)); }
	inline String_t* get_apiEndpoint_5() const { return ___apiEndpoint_5; }
	inline String_t** get_address_of_apiEndpoint_5() { return &___apiEndpoint_5; }
	inline void set_apiEndpoint_5(String_t* value)
	{
		___apiEndpoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___apiEndpoint_5), value);
	}

	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2691665636, ___mode_6)); }
	inline String_t* get_mode_6() const { return ___mode_6; }
	inline String_t** get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(String_t* value)
	{
		___mode_6 = value;
		Il2CppCodeGenWriteBarrier((&___mode_6), value);
	}

	inline static int32_t get_offset_of_types_7() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2691665636, ___types_7)); }
	inline StringU5BU5D_t1281789340* get_types_7() const { return ___types_7; }
	inline StringU5BU5D_t1281789340** get_address_of_types_7() { return &___types_7; }
	inline void set_types_7(StringU5BU5D_t1281789340* value)
	{
		___types_7 = value;
		Il2CppCodeGenWriteBarrier((&___types_7), value);
	}
};

struct GeocodeResource_1_t2691665636_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> Mapbox.Geocoding.GeocodeResource`1::FeatureTypes
	List_1_t3319525431 * ___FeatureTypes_4;

public:
	inline static int32_t get_offset_of_FeatureTypes_4() { return static_cast<int32_t>(offsetof(GeocodeResource_1_t2691665636_StaticFields, ___FeatureTypes_4)); }
	inline List_1_t3319525431 * get_FeatureTypes_4() const { return ___FeatureTypes_4; }
	inline List_1_t3319525431 ** get_address_of_FeatureTypes_4() { return &___FeatureTypes_4; }
	inline void set_FeatureTypes_4(List_1_t3319525431 * value)
	{
		___FeatureTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___FeatureTypes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOCODERESOURCE_1_T2691665636_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef NULLABLE_1_T1164162090_H
#define NULLABLE_1_T1164162090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t1164162090 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1164162090, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1164162090, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1164162090_H
#ifndef IGNOREATTRIBUTE_T1003697248_H
#define IGNOREATTRIBUTE_T1003697248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.IgnoreAttribute
struct  IgnoreAttribute_t1003697248  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNOREATTRIBUTE_T1003697248_H
#ifndef CACHEITEM_T421398482_H
#define CACHEITEM_T421398482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.MemoryCache/CacheItem
struct  CacheItem_t421398482 
{
public:
	// System.Int64 Mapbox.Platform.Cache.MemoryCache/CacheItem::Timestamp
	int64_t ___Timestamp_0;
	// System.Byte[] Mapbox.Platform.Cache.MemoryCache/CacheItem::Data
	ByteU5BU5D_t4116647657* ___Data_1;

public:
	inline static int32_t get_offset_of_Timestamp_0() { return static_cast<int32_t>(offsetof(CacheItem_t421398482, ___Timestamp_0)); }
	inline int64_t get_Timestamp_0() const { return ___Timestamp_0; }
	inline int64_t* get_address_of_Timestamp_0() { return &___Timestamp_0; }
	inline void set_Timestamp_0(int64_t value)
	{
		___Timestamp_0 = value;
	}

	inline static int32_t get_offset_of_Data_1() { return static_cast<int32_t>(offsetof(CacheItem_t421398482, ___Data_1)); }
	inline ByteU5BU5D_t4116647657* get_Data_1() const { return ___Data_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_Data_1() { return &___Data_1; }
	inline void set_Data_1(ByteU5BU5D_t4116647657* value)
	{
		___Data_1 = value;
		Il2CppCodeGenWriteBarrier((&___Data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Platform.Cache.MemoryCache/CacheItem
struct CacheItem_t421398482_marshaled_pinvoke
{
	int64_t ___Timestamp_0;
	uint8_t* ___Data_1;
};
// Native definition for COM marshalling of Mapbox.Platform.Cache.MemoryCache/CacheItem
struct CacheItem_t421398482_marshaled_com
{
	int64_t ___Timestamp_0;
	uint8_t* ___Data_1;
};
#endif // CACHEITEM_T421398482_H
#ifndef UNWRAPPEDTILEID_T2586853537_H
#define UNWRAPPEDTILEID_T2586853537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.UnwrappedTileId
struct  UnwrappedTileId_t2586853537 
{
public:
	// System.Int32 Mapbox.Map.UnwrappedTileId::Z
	int32_t ___Z_0;
	// System.Int32 Mapbox.Map.UnwrappedTileId::X
	int32_t ___X_1;
	// System.Int32 Mapbox.Map.UnwrappedTileId::Y
	int32_t ___Y_2;

public:
	inline static int32_t get_offset_of_Z_0() { return static_cast<int32_t>(offsetof(UnwrappedTileId_t2586853537, ___Z_0)); }
	inline int32_t get_Z_0() const { return ___Z_0; }
	inline int32_t* get_address_of_Z_0() { return &___Z_0; }
	inline void set_Z_0(int32_t value)
	{
		___Z_0 = value;
	}

	inline static int32_t get_offset_of_X_1() { return static_cast<int32_t>(offsetof(UnwrappedTileId_t2586853537, ___X_1)); }
	inline int32_t get_X_1() const { return ___X_1; }
	inline int32_t* get_address_of_X_1() { return &___X_1; }
	inline void set_X_1(int32_t value)
	{
		___X_1 = value;
	}

	inline static int32_t get_offset_of_Y_2() { return static_cast<int32_t>(offsetof(UnwrappedTileId_t2586853537, ___Y_2)); }
	inline int32_t get_Y_2() const { return ___Y_2; }
	inline int32_t* get_address_of_Y_2() { return &___Y_2; }
	inline void set_Y_2(int32_t value)
	{
		___Y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNWRAPPEDTILEID_T2586853537_H
#ifndef CANONICALTILEID_T4184902996_H
#define CANONICALTILEID_T4184902996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.CanonicalTileId
struct  CanonicalTileId_t4184902996 
{
public:
	// System.Int32 Mapbox.Map.CanonicalTileId::Z
	int32_t ___Z_0;
	// System.Int32 Mapbox.Map.CanonicalTileId::X
	int32_t ___X_1;
	// System.Int32 Mapbox.Map.CanonicalTileId::Y
	int32_t ___Y_2;

public:
	inline static int32_t get_offset_of_Z_0() { return static_cast<int32_t>(offsetof(CanonicalTileId_t4184902996, ___Z_0)); }
	inline int32_t get_Z_0() const { return ___Z_0; }
	inline int32_t* get_address_of_Z_0() { return &___Z_0; }
	inline void set_Z_0(int32_t value)
	{
		___Z_0 = value;
	}

	inline static int32_t get_offset_of_X_1() { return static_cast<int32_t>(offsetof(CanonicalTileId_t4184902996, ___X_1)); }
	inline int32_t get_X_1() const { return ___X_1; }
	inline int32_t* get_address_of_X_1() { return &___X_1; }
	inline void set_X_1(int32_t value)
	{
		___X_1 = value;
	}

	inline static int32_t get_offset_of_Y_2() { return static_cast<int32_t>(offsetof(CanonicalTileId_t4184902996, ___Y_2)); }
	inline int32_t get_Y_2() const { return ___Y_2; }
	inline int32_t* get_address_of_Y_2() { return &___Y_2; }
	inline void set_Y_2(int32_t value)
	{
		___Y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANONICALTILEID_T4184902996_H
#ifndef INDEXEDCOLUMN_T3080689016_H
#define INDEXEDCOLUMN_T3080689016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/IndexedColumn
struct  IndexedColumn_t3080689016 
{
public:
	// System.Int32 SQLite4Unity3d.SQLiteConnection/IndexedColumn::Order
	int32_t ___Order_0;
	// System.String SQLite4Unity3d.SQLiteConnection/IndexedColumn::ColumnName
	String_t* ___ColumnName_1;

public:
	inline static int32_t get_offset_of_Order_0() { return static_cast<int32_t>(offsetof(IndexedColumn_t3080689016, ___Order_0)); }
	inline int32_t get_Order_0() const { return ___Order_0; }
	inline int32_t* get_address_of_Order_0() { return &___Order_0; }
	inline void set_Order_0(int32_t value)
	{
		___Order_0 = value;
	}

	inline static int32_t get_offset_of_ColumnName_1() { return static_cast<int32_t>(offsetof(IndexedColumn_t3080689016, ___ColumnName_1)); }
	inline String_t* get_ColumnName_1() const { return ___ColumnName_1; }
	inline String_t** get_address_of_ColumnName_1() { return &___ColumnName_1; }
	inline void set_ColumnName_1(String_t* value)
	{
		___ColumnName_1 = value;
		Il2CppCodeGenWriteBarrier((&___ColumnName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SQLite4Unity3d.SQLiteConnection/IndexedColumn
struct IndexedColumn_t3080689016_marshaled_pinvoke
{
	int32_t ___Order_0;
	char* ___ColumnName_1;
};
// Native definition for COM marshalling of SQLite4Unity3d.SQLiteConnection/IndexedColumn
struct IndexedColumn_t3080689016_marshaled_com
{
	int32_t ___Order_0;
	Il2CppChar* ___ColumnName_1;
};
#endif // INDEXEDCOLUMN_T3080689016_H
#ifndef INDEXINFO_T1680796658_H
#define INDEXINFO_T1680796658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/IndexInfo
struct  IndexInfo_t1680796658 
{
public:
	// System.String SQLite4Unity3d.SQLiteConnection/IndexInfo::IndexName
	String_t* ___IndexName_0;
	// System.String SQLite4Unity3d.SQLiteConnection/IndexInfo::TableName
	String_t* ___TableName_1;
	// System.Boolean SQLite4Unity3d.SQLiteConnection/IndexInfo::Unique
	bool ___Unique_2;
	// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn> SQLite4Unity3d.SQLiteConnection/IndexInfo::Columns
	List_1_t257796462 * ___Columns_3;

public:
	inline static int32_t get_offset_of_IndexName_0() { return static_cast<int32_t>(offsetof(IndexInfo_t1680796658, ___IndexName_0)); }
	inline String_t* get_IndexName_0() const { return ___IndexName_0; }
	inline String_t** get_address_of_IndexName_0() { return &___IndexName_0; }
	inline void set_IndexName_0(String_t* value)
	{
		___IndexName_0 = value;
		Il2CppCodeGenWriteBarrier((&___IndexName_0), value);
	}

	inline static int32_t get_offset_of_TableName_1() { return static_cast<int32_t>(offsetof(IndexInfo_t1680796658, ___TableName_1)); }
	inline String_t* get_TableName_1() const { return ___TableName_1; }
	inline String_t** get_address_of_TableName_1() { return &___TableName_1; }
	inline void set_TableName_1(String_t* value)
	{
		___TableName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TableName_1), value);
	}

	inline static int32_t get_offset_of_Unique_2() { return static_cast<int32_t>(offsetof(IndexInfo_t1680796658, ___Unique_2)); }
	inline bool get_Unique_2() const { return ___Unique_2; }
	inline bool* get_address_of_Unique_2() { return &___Unique_2; }
	inline void set_Unique_2(bool value)
	{
		___Unique_2 = value;
	}

	inline static int32_t get_offset_of_Columns_3() { return static_cast<int32_t>(offsetof(IndexInfo_t1680796658, ___Columns_3)); }
	inline List_1_t257796462 * get_Columns_3() const { return ___Columns_3; }
	inline List_1_t257796462 ** get_address_of_Columns_3() { return &___Columns_3; }
	inline void set_Columns_3(List_1_t257796462 * value)
	{
		___Columns_3 = value;
		Il2CppCodeGenWriteBarrier((&___Columns_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SQLite4Unity3d.SQLiteConnection/IndexInfo
struct IndexInfo_t1680796658_marshaled_pinvoke
{
	char* ___IndexName_0;
	char* ___TableName_1;
	int32_t ___Unique_2;
	List_1_t257796462 * ___Columns_3;
};
// Native definition for COM marshalling of SQLite4Unity3d.SQLiteConnection/IndexInfo
struct IndexInfo_t1680796658_marshaled_com
{
	Il2CppChar* ___IndexName_0;
	Il2CppChar* ___TableName_1;
	int32_t ___Unique_2;
	List_1_t257796462 * ___Columns_3;
};
#endif // INDEXINFO_T1680796658_H
#ifndef TABLEATTRIBUTE_T3728577771_H
#define TABLEATTRIBUTE_T3728577771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.TableAttribute
struct  TableAttribute_t3728577771  : public Attribute_t861562559
{
public:
	// System.String SQLite4Unity3d.TableAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TableAttribute_t3728577771, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEATTRIBUTE_T3728577771_H
#ifndef INDEXEDATTRIBUTE_T384353508_H
#define INDEXEDATTRIBUTE_T384353508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.IndexedAttribute
struct  IndexedAttribute_t384353508  : public Attribute_t861562559
{
public:
	// System.String SQLite4Unity3d.IndexedAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 SQLite4Unity3d.IndexedAttribute::<Order>k__BackingField
	int32_t ___U3COrderU3Ek__BackingField_1;
	// System.Boolean SQLite4Unity3d.IndexedAttribute::<Unique>k__BackingField
	bool ___U3CUniqueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IndexedAttribute_t384353508, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COrderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(IndexedAttribute_t384353508, ___U3COrderU3Ek__BackingField_1)); }
	inline int32_t get_U3COrderU3Ek__BackingField_1() const { return ___U3COrderU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3COrderU3Ek__BackingField_1() { return &___U3COrderU3Ek__BackingField_1; }
	inline void set_U3COrderU3Ek__BackingField_1(int32_t value)
	{
		___U3COrderU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CUniqueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(IndexedAttribute_t384353508, ___U3CUniqueU3Ek__BackingField_2)); }
	inline bool get_U3CUniqueU3Ek__BackingField_2() const { return ___U3CUniqueU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CUniqueU3Ek__BackingField_2() { return &___U3CUniqueU3Ek__BackingField_2; }
	inline void set_U3CUniqueU3Ek__BackingField_2(bool value)
	{
		___U3CUniqueU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXEDATTRIBUTE_T384353508_H
#ifndef FORWARDGEOCODERESPONSE_T2959476828_H
#define FORWARDGEOCODERESPONSE_T2959476828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.ForwardGeocodeResponse
struct  ForwardGeocodeResponse_t2959476828  : public GeocodeResponse_t3526499648
{
public:
	// System.Collections.Generic.List`1<System.String> Mapbox.Geocoding.ForwardGeocodeResponse::<Query>k__BackingField
	List_1_t3319525431 * ___U3CQueryU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CQueryU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ForwardGeocodeResponse_t2959476828, ___U3CQueryU3Ek__BackingField_3)); }
	inline List_1_t3319525431 * get_U3CQueryU3Ek__BackingField_3() const { return ___U3CQueryU3Ek__BackingField_3; }
	inline List_1_t3319525431 ** get_address_of_U3CQueryU3Ek__BackingField_3() { return &___U3CQueryU3Ek__BackingField_3; }
	inline void set_U3CQueryU3Ek__BackingField_3(List_1_t3319525431 * value)
	{
		___U3CQueryU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQueryU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDGEOCODERESPONSE_T2959476828_H
#ifndef AUTOINCREMENTATTRIBUTE_T3792157881_H
#define AUTOINCREMENTATTRIBUTE_T3792157881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.AutoIncrementAttribute
struct  AutoIncrementAttribute_t3792157881  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINCREMENTATTRIBUTE_T3792157881_H
#ifndef PRIMARYKEYATTRIBUTE_T1120256698_H
#define PRIMARYKEYATTRIBUTE_T1120256698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.PrimaryKeyAttribute
struct  PrimaryKeyAttribute_t1120256698  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMARYKEYATTRIBUTE_T1120256698_H
#ifndef COLUMNATTRIBUTE_T3583764011_H
#define COLUMNATTRIBUTE_T3583764011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.ColumnAttribute
struct  ColumnAttribute_t3583764011  : public Attribute_t861562559
{
public:
	// System.String SQLite4Unity3d.ColumnAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ColumnAttribute_t3583764011, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMNATTRIBUTE_T3583764011_H
#ifndef REVERSEGEOCODERESOURCE_T2777886177_H
#define REVERSEGEOCODERESOURCE_T2777886177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.ReverseGeocodeResource
struct  ReverseGeocodeResource_t2777886177  : public GeocodeResource_1_t2691665636
{
public:
	// Mapbox.Utils.Vector2d Mapbox.Geocoding.ReverseGeocodeResource::query
	Vector2d_t1865246568  ___query_8;

public:
	inline static int32_t get_offset_of_query_8() { return static_cast<int32_t>(offsetof(ReverseGeocodeResource_t2777886177, ___query_8)); }
	inline Vector2d_t1865246568  get_query_8() const { return ___query_8; }
	inline Vector2d_t1865246568 * get_address_of_query_8() { return &___query_8; }
	inline void set_query_8(Vector2d_t1865246568  value)
	{
		___query_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVERSEGEOCODERESOURCE_T2777886177_H
#ifndef LONLATTOVECTOR2DCONVERTER_T2933574141_H
#define LONLATTOVECTOR2DCONVERTER_T2933574141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.JsonConverters.LonLatToVector2dConverter
struct  LonLatToVector2dConverter_t2933574141  : public CustomCreationConverter_1_t132058663
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONLATTOVECTOR2DCONVERTER_T2933574141_H
#ifndef U3CGETTILEU3EC__ANONSTOREY0_T2570057117_H
#define U3CGETTILEU3EC__ANONSTOREY0_T2570057117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.MbTiles.MbTilesDb/<GetTile>c__AnonStorey0
struct  U3CGetTileU3Ec__AnonStorey0_t2570057117  : public RuntimeObject
{
public:
	// Mapbox.Map.CanonicalTileId Mapbox.Platform.MbTiles.MbTilesDb/<GetTile>c__AnonStorey0::tileId
	CanonicalTileId_t4184902996  ___tileId_0;

public:
	inline static int32_t get_offset_of_tileId_0() { return static_cast<int32_t>(offsetof(U3CGetTileU3Ec__AnonStorey0_t2570057117, ___tileId_0)); }
	inline CanonicalTileId_t4184902996  get_tileId_0() const { return ___tileId_0; }
	inline CanonicalTileId_t4184902996 * get_address_of_tileId_0() { return &___tileId_0; }
	inline void set_tileId_0(CanonicalTileId_t4184902996  value)
	{
		___tileId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTILEU3EC__ANONSTOREY0_T2570057117_H
#ifndef BBOXTOVECTOR2DBOUNDSCONVERTER_T1118841236_H
#define BBOXTOVECTOR2DBOUNDSCONVERTER_T1118841236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.JsonConverters.BboxToVector2dBoundsConverter
struct  BboxToVector2dBoundsConverter_t1118841236  : public CustomCreationConverter_1_t241653040
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BBOXTOVECTOR2DBOUNDSCONVERTER_T1118841236_H
#ifndef VECTOR2DBOUNDS_T1974840945_H
#define VECTOR2DBOUNDS_T1974840945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Vector2dBounds
struct  Vector2dBounds_t1974840945 
{
public:
	// Mapbox.Utils.Vector2d Mapbox.Utils.Vector2dBounds::SouthWest
	Vector2d_t1865246568  ___SouthWest_0;
	// Mapbox.Utils.Vector2d Mapbox.Utils.Vector2dBounds::NorthEast
	Vector2d_t1865246568  ___NorthEast_1;

public:
	inline static int32_t get_offset_of_SouthWest_0() { return static_cast<int32_t>(offsetof(Vector2dBounds_t1974840945, ___SouthWest_0)); }
	inline Vector2d_t1865246568  get_SouthWest_0() const { return ___SouthWest_0; }
	inline Vector2d_t1865246568 * get_address_of_SouthWest_0() { return &___SouthWest_0; }
	inline void set_SouthWest_0(Vector2d_t1865246568  value)
	{
		___SouthWest_0 = value;
	}

	inline static int32_t get_offset_of_NorthEast_1() { return static_cast<int32_t>(offsetof(Vector2dBounds_t1974840945, ___NorthEast_1)); }
	inline Vector2d_t1865246568  get_NorthEast_1() const { return ___NorthEast_1; }
	inline Vector2d_t1865246568 * get_address_of_NorthEast_1() { return &___NorthEast_1; }
	inline void set_NorthEast_1(Vector2d_t1865246568  value)
	{
		___NorthEast_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2DBOUNDS_T1974840945_H
#ifndef POLYLINETOVECTOR2DLISTCONVERTER_T1416161534_H
#define POLYLINETOVECTOR2DLISTCONVERTER_T1416161534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.JsonConverters.PolylineToVector2dListConverter
struct  PolylineToVector2dListConverter_t1416161534  : public CustomCreationConverter_1_t1604133405
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYLINETOVECTOR2DLISTCONVERTER_T1416161534_H
#ifndef MBTILESDB_T2334709092_H
#define MBTILESDB_T2334709092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.MbTiles.MbTilesDb
struct  MbTilesDb_t2334709092  : public RuntimeObject
{
public:
	// System.Boolean Mapbox.Platform.MbTiles.MbTilesDb::_disposed
	bool ____disposed_0;
	// System.String Mapbox.Platform.MbTiles.MbTilesDb::_dbPath
	String_t* ____dbPath_1;
	// SQLite4Unity3d.SQLiteConnection Mapbox.Platform.MbTiles.MbTilesDb::_sqlite
	SQLiteConnection_t3070079188 * ____sqlite_2;
	// System.Nullable`1<System.UInt32> Mapbox.Platform.MbTiles.MbTilesDb::_maxTileCount
	Nullable_1_t4282624060  ____maxTileCount_3;
	// System.Int32 Mapbox.Platform.MbTiles.MbTilesDb::_pruneCacheCounter
	int32_t ____pruneCacheCounter_5;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(MbTilesDb_t2334709092, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__dbPath_1() { return static_cast<int32_t>(offsetof(MbTilesDb_t2334709092, ____dbPath_1)); }
	inline String_t* get__dbPath_1() const { return ____dbPath_1; }
	inline String_t** get_address_of__dbPath_1() { return &____dbPath_1; }
	inline void set__dbPath_1(String_t* value)
	{
		____dbPath_1 = value;
		Il2CppCodeGenWriteBarrier((&____dbPath_1), value);
	}

	inline static int32_t get_offset_of__sqlite_2() { return static_cast<int32_t>(offsetof(MbTilesDb_t2334709092, ____sqlite_2)); }
	inline SQLiteConnection_t3070079188 * get__sqlite_2() const { return ____sqlite_2; }
	inline SQLiteConnection_t3070079188 ** get_address_of__sqlite_2() { return &____sqlite_2; }
	inline void set__sqlite_2(SQLiteConnection_t3070079188 * value)
	{
		____sqlite_2 = value;
		Il2CppCodeGenWriteBarrier((&____sqlite_2), value);
	}

	inline static int32_t get_offset_of__maxTileCount_3() { return static_cast<int32_t>(offsetof(MbTilesDb_t2334709092, ____maxTileCount_3)); }
	inline Nullable_1_t4282624060  get__maxTileCount_3() const { return ____maxTileCount_3; }
	inline Nullable_1_t4282624060 * get_address_of__maxTileCount_3() { return &____maxTileCount_3; }
	inline void set__maxTileCount_3(Nullable_1_t4282624060  value)
	{
		____maxTileCount_3 = value;
	}

	inline static int32_t get_offset_of__pruneCacheCounter_5() { return static_cast<int32_t>(offsetof(MbTilesDb_t2334709092, ____pruneCacheCounter_5)); }
	inline int32_t get__pruneCacheCounter_5() const { return ____pruneCacheCounter_5; }
	inline int32_t* get_address_of__pruneCacheCounter_5() { return &____pruneCacheCounter_5; }
	inline void set__pruneCacheCounter_5(int32_t value)
	{
		____pruneCacheCounter_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MBTILESDB_T2334709092_H
#ifndef GEOMETRY_T3224508297_H
#define GEOMETRY_T3224508297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Geocoding.Geometry
struct  Geometry_t3224508297  : public RuntimeObject
{
public:
	// System.String Mapbox.Geocoding.Geometry::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_0;
	// Mapbox.Utils.Vector2d Mapbox.Geocoding.Geometry::<Coordinates>k__BackingField
	Vector2d_t1865246568  ___U3CCoordinatesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Geometry_t3224508297, ___U3CTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCoordinatesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Geometry_t3224508297, ___U3CCoordinatesU3Ek__BackingField_1)); }
	inline Vector2d_t1865246568  get_U3CCoordinatesU3Ek__BackingField_1() const { return ___U3CCoordinatesU3Ek__BackingField_1; }
	inline Vector2d_t1865246568 * get_address_of_U3CCoordinatesU3Ek__BackingField_1() { return &___U3CCoordinatesU3Ek__BackingField_1; }
	inline void set_U3CCoordinatesU3Ek__BackingField_1(Vector2d_t1865246568  value)
	{
		___U3CCoordinatesU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOMETRY_T3224508297_H
#ifndef U3CREQUESTU3EC__ANONSTOREY1_T3971576917_H
#define U3CREQUESTU3EC__ANONSTOREY1_T3971576917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Cache.CachingWebFileSource/<Request>c__AnonStorey1
struct  U3CRequestU3Ec__AnonStorey1_t3971576917  : public RuntimeObject
{
public:
	// System.String Mapbox.Platform.Cache.CachingWebFileSource/<Request>c__AnonStorey1::mapId
	String_t* ___mapId_0;
	// Mapbox.Map.CanonicalTileId Mapbox.Platform.Cache.CachingWebFileSource/<Request>c__AnonStorey1::tileId
	CanonicalTileId_t4184902996  ___tileId_1;
	// System.Action`1<Mapbox.Platform.Response> Mapbox.Platform.Cache.CachingWebFileSource/<Request>c__AnonStorey1::callback
	Action_1_t1228140472 * ___callback_2;
	// Mapbox.Platform.Cache.CachingWebFileSource Mapbox.Platform.Cache.CachingWebFileSource/<Request>c__AnonStorey1::$this
	CachingWebFileSource_t328893056 * ___U24this_3;

public:
	inline static int32_t get_offset_of_mapId_0() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__AnonStorey1_t3971576917, ___mapId_0)); }
	inline String_t* get_mapId_0() const { return ___mapId_0; }
	inline String_t** get_address_of_mapId_0() { return &___mapId_0; }
	inline void set_mapId_0(String_t* value)
	{
		___mapId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapId_0), value);
	}

	inline static int32_t get_offset_of_tileId_1() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__AnonStorey1_t3971576917, ___tileId_1)); }
	inline CanonicalTileId_t4184902996  get_tileId_1() const { return ___tileId_1; }
	inline CanonicalTileId_t4184902996 * get_address_of_tileId_1() { return &___tileId_1; }
	inline void set_tileId_1(CanonicalTileId_t4184902996  value)
	{
		___tileId_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__AnonStorey1_t3971576917, ___callback_2)); }
	inline Action_1_t1228140472 * get_callback_2() const { return ___callback_2; }
	inline Action_1_t1228140472 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_t1228140472 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__AnonStorey1_t3971576917, ___U24this_3)); }
	inline CachingWebFileSource_t328893056 * get_U24this_3() const { return ___U24this_3; }
	inline CachingWebFileSource_t328893056 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(CachingWebFileSource_t328893056 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREQUESTU3EC__ANONSTOREY1_T3971576917_H
#ifndef PARAMETERS_T4189861888_H
#define PARAMETERS_T4189861888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.Tile/Parameters
struct  Parameters_t4189861888 
{
public:
	// Mapbox.Map.CanonicalTileId Mapbox.Map.Tile/Parameters::Id
	CanonicalTileId_t4184902996  ___Id_0;
	// System.String Mapbox.Map.Tile/Parameters::MapId
	String_t* ___MapId_1;
	// Mapbox.Platform.IFileSource Mapbox.Map.Tile/Parameters::Fs
	RuntimeObject* ___Fs_2;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(Parameters_t4189861888, ___Id_0)); }
	inline CanonicalTileId_t4184902996  get_Id_0() const { return ___Id_0; }
	inline CanonicalTileId_t4184902996 * get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(CanonicalTileId_t4184902996  value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_MapId_1() { return static_cast<int32_t>(offsetof(Parameters_t4189861888, ___MapId_1)); }
	inline String_t* get_MapId_1() const { return ___MapId_1; }
	inline String_t** get_address_of_MapId_1() { return &___MapId_1; }
	inline void set_MapId_1(String_t* value)
	{
		___MapId_1 = value;
		Il2CppCodeGenWriteBarrier((&___MapId_1), value);
	}

	inline static int32_t get_offset_of_Fs_2() { return static_cast<int32_t>(offsetof(Parameters_t4189861888, ___Fs_2)); }
	inline RuntimeObject* get_Fs_2() const { return ___Fs_2; }
	inline RuntimeObject** get_address_of_Fs_2() { return &___Fs_2; }
	inline void set_Fs_2(RuntimeObject* value)
	{
		___Fs_2 = value;
		Il2CppCodeGenWriteBarrier((&___Fs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Map.Tile/Parameters
struct Parameters_t4189861888_marshaled_pinvoke
{
	CanonicalTileId_t4184902996  ___Id_0;
	char* ___MapId_1;
	RuntimeObject* ___Fs_2;
};
// Native definition for COM marshalling of Mapbox.Map.Tile/Parameters
struct Parameters_t4189861888_marshaled_com
{
	CanonicalTileId_t4184902996  ___Id_0;
	Il2CppChar* ___MapId_1;
	RuntimeObject* ___Fs_2;
};
#endif // PARAMETERS_T4189861888_H
#ifndef STATE_T2862325545_H
#define STATE_T2862325545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.Tile/State
struct  State_t2862325545 
{
public:
	// System.Int32 Mapbox.Map.Tile/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t2862325545, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T2862325545_H
#ifndef U3CTILEEXISTSU3EC__ANONSTOREY1_T694898448_H
#define U3CTILEEXISTSU3EC__ANONSTOREY1_T694898448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.MbTiles.MbTilesDb/<TileExists>c__AnonStorey1
struct  U3CTileExistsU3Ec__AnonStorey1_t694898448  : public RuntimeObject
{
public:
	// Mapbox.Map.CanonicalTileId Mapbox.Platform.MbTiles.MbTilesDb/<TileExists>c__AnonStorey1::tileId
	CanonicalTileId_t4184902996  ___tileId_0;

public:
	inline static int32_t get_offset_of_tileId_0() { return static_cast<int32_t>(offsetof(U3CTileExistsU3Ec__AnonStorey1_t694898448, ___tileId_0)); }
	inline CanonicalTileId_t4184902996  get_tileId_0() const { return ___tileId_0; }
	inline CanonicalTileId_t4184902996 * get_address_of_tileId_0() { return &___tileId_0; }
	inline void set_tileId_0(CanonicalTileId_t4184902996  value)
	{
		___tileId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTILEEXISTSU3EC__ANONSTOREY1_T694898448_H
#ifndef SQLITEOPENFLAGS_T2333747160_H
#define SQLITEOPENFLAGS_T2333747160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteOpenFlags
struct  SQLiteOpenFlags_t2333747160 
{
public:
	// System.Int32 SQLite4Unity3d.SQLiteOpenFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SQLiteOpenFlags_t2333747160, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQLITEOPENFLAGS_T2333747160_H
#ifndef PREPAREDSQLLITEINSERTCOMMAND_T994658783_H
#define PREPAREDSQLLITEINSERTCOMMAND_T994658783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.PreparedSqlLiteInsertCommand
struct  PreparedSqlLiteInsertCommand_t994658783  : public RuntimeObject
{
public:
	// System.Boolean SQLite4Unity3d.PreparedSqlLiteInsertCommand::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_0;
	// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.PreparedSqlLiteInsertCommand::<Connection>k__BackingField
	SQLiteConnection_t3070079188 * ___U3CConnectionU3Ek__BackingField_1;
	// System.String SQLite4Unity3d.PreparedSqlLiteInsertCommand::<CommandText>k__BackingField
	String_t* ___U3CCommandTextU3Ek__BackingField_2;
	// System.IntPtr SQLite4Unity3d.PreparedSqlLiteInsertCommand::<Statement>k__BackingField
	intptr_t ___U3CStatementU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PreparedSqlLiteInsertCommand_t994658783, ___U3CInitializedU3Ek__BackingField_0)); }
	inline bool get_U3CInitializedU3Ek__BackingField_0() const { return ___U3CInitializedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_0() { return &___U3CInitializedU3Ek__BackingField_0; }
	inline void set_U3CInitializedU3Ek__BackingField_0(bool value)
	{
		___U3CInitializedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CConnectionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PreparedSqlLiteInsertCommand_t994658783, ___U3CConnectionU3Ek__BackingField_1)); }
	inline SQLiteConnection_t3070079188 * get_U3CConnectionU3Ek__BackingField_1() const { return ___U3CConnectionU3Ek__BackingField_1; }
	inline SQLiteConnection_t3070079188 ** get_address_of_U3CConnectionU3Ek__BackingField_1() { return &___U3CConnectionU3Ek__BackingField_1; }
	inline void set_U3CConnectionU3Ek__BackingField_1(SQLiteConnection_t3070079188 * value)
	{
		___U3CConnectionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCommandTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PreparedSqlLiteInsertCommand_t994658783, ___U3CCommandTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CCommandTextU3Ek__BackingField_2() const { return ___U3CCommandTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCommandTextU3Ek__BackingField_2() { return &___U3CCommandTextU3Ek__BackingField_2; }
	inline void set_U3CCommandTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CCommandTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandTextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CStatementU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PreparedSqlLiteInsertCommand_t994658783, ___U3CStatementU3Ek__BackingField_3)); }
	inline intptr_t get_U3CStatementU3Ek__BackingField_3() const { return ___U3CStatementU3Ek__BackingField_3; }
	inline intptr_t* get_address_of_U3CStatementU3Ek__BackingField_3() { return &___U3CStatementU3Ek__BackingField_3; }
	inline void set_U3CStatementU3Ek__BackingField_3(intptr_t value)
	{
		___U3CStatementU3Ek__BackingField_3 = value;
	}
};

struct PreparedSqlLiteInsertCommand_t994658783_StaticFields
{
public:
	// System.IntPtr SQLite4Unity3d.PreparedSqlLiteInsertCommand::NullStatement
	intptr_t ___NullStatement_4;

public:
	inline static int32_t get_offset_of_NullStatement_4() { return static_cast<int32_t>(offsetof(PreparedSqlLiteInsertCommand_t994658783_StaticFields, ___NullStatement_4)); }
	inline intptr_t get_NullStatement_4() const { return ___NullStatement_4; }
	inline intptr_t* get_address_of_NullStatement_4() { return &___NullStatement_4; }
	inline void set_NullStatement_4(intptr_t value)
	{
		___NullStatement_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREPAREDSQLLITEINSERTCOMMAND_T994658783_H
#ifndef CREATEFLAGS_T2403871199_H
#define CREATEFLAGS_T2403871199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.CreateFlags
struct  CreateFlags_t2403871199 
{
public:
	// System.Int32 SQLite4Unity3d.CreateFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CreateFlags_t2403871199, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEFLAGS_T2403871199_H
#ifndef SQLITECOMMAND_T3688707480_H
#define SQLITECOMMAND_T3688707480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteCommand
struct  SQLiteCommand_t3688707480  : public RuntimeObject
{
public:
	// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.SQLiteCommand::_conn
	SQLiteConnection_t3070079188 * ____conn_0;
	// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteCommand/Binding> SQLite4Unity3d.SQLiteCommand::_bindings
	List_1_t4258913814 * ____bindings_1;
	// System.String SQLite4Unity3d.SQLiteCommand::<CommandText>k__BackingField
	String_t* ___U3CCommandTextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__conn_0() { return static_cast<int32_t>(offsetof(SQLiteCommand_t3688707480, ____conn_0)); }
	inline SQLiteConnection_t3070079188 * get__conn_0() const { return ____conn_0; }
	inline SQLiteConnection_t3070079188 ** get_address_of__conn_0() { return &____conn_0; }
	inline void set__conn_0(SQLiteConnection_t3070079188 * value)
	{
		____conn_0 = value;
		Il2CppCodeGenWriteBarrier((&____conn_0), value);
	}

	inline static int32_t get_offset_of__bindings_1() { return static_cast<int32_t>(offsetof(SQLiteCommand_t3688707480, ____bindings_1)); }
	inline List_1_t4258913814 * get__bindings_1() const { return ____bindings_1; }
	inline List_1_t4258913814 ** get_address_of__bindings_1() { return &____bindings_1; }
	inline void set__bindings_1(List_1_t4258913814 * value)
	{
		____bindings_1 = value;
		Il2CppCodeGenWriteBarrier((&____bindings_1), value);
	}

	inline static int32_t get_offset_of_U3CCommandTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SQLiteCommand_t3688707480, ___U3CCommandTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CCommandTextU3Ek__BackingField_2() const { return ___U3CCommandTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCommandTextU3Ek__BackingField_2() { return &___U3CCommandTextU3Ek__BackingField_2; }
	inline void set_U3CCommandTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CCommandTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommandTextU3Ek__BackingField_2), value);
	}
};

struct SQLiteCommand_t3688707480_StaticFields
{
public:
	// System.IntPtr SQLite4Unity3d.SQLiteCommand::NegativePointer
	intptr_t ___NegativePointer_3;

public:
	inline static int32_t get_offset_of_NegativePointer_3() { return static_cast<int32_t>(offsetof(SQLiteCommand_t3688707480_StaticFields, ___NegativePointer_3)); }
	inline intptr_t get_NegativePointer_3() const { return ___NegativePointer_3; }
	inline intptr_t* get_address_of_NegativePointer_3() { return &___NegativePointer_3; }
	inline void set_NegativePointer_3(intptr_t value)
	{
		___NegativePointer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQLITECOMMAND_T3688707480_H
#ifndef SQLITECONNECTION_T3070079188_H
#define SQLITECONNECTION_T3070079188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection
struct  SQLiteConnection_t3070079188  : public RuntimeObject
{
public:
	// System.Boolean SQLite4Unity3d.SQLiteConnection::_open
	bool ____open_0;
	// System.TimeSpan SQLite4Unity3d.SQLiteConnection::_busyTimeout
	TimeSpan_t881159249  ____busyTimeout_1;
	// System.Collections.Generic.Dictionary`2<System.String,SQLite4Unity3d.TableMapping> SQLite4Unity3d.SQLiteConnection::_mappings
	Dictionary_2_t2061655791 * ____mappings_2;
	// System.Collections.Generic.Dictionary`2<System.String,SQLite4Unity3d.TableMapping> SQLite4Unity3d.SQLiteConnection::_tables
	Dictionary_2_t2061655791 * ____tables_3;
	// System.Diagnostics.Stopwatch SQLite4Unity3d.SQLiteConnection::_sw
	Stopwatch_t305734070 * ____sw_4;
	// System.Int64 SQLite4Unity3d.SQLiteConnection::_elapsedMilliseconds
	int64_t ____elapsedMilliseconds_5;
	// System.Int32 SQLite4Unity3d.SQLiteConnection::_transactionDepth
	int32_t ____transactionDepth_6;
	// System.Random SQLite4Unity3d.SQLiteConnection::_rand
	Random_t108471755 * ____rand_7;
	// System.IntPtr SQLite4Unity3d.SQLiteConnection::<Handle>k__BackingField
	intptr_t ___U3CHandleU3Ek__BackingField_8;
	// System.String SQLite4Unity3d.SQLiteConnection::<DatabasePath>k__BackingField
	String_t* ___U3CDatabasePathU3Ek__BackingField_10;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::<TimeExecution>k__BackingField
	bool ___U3CTimeExecutionU3Ek__BackingField_11;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::<Trace>k__BackingField
	bool ___U3CTraceU3Ek__BackingField_12;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::<StoreDateTimeAsTicks>k__BackingField
	bool ___U3CStoreDateTimeAsTicksU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of__open_0() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____open_0)); }
	inline bool get__open_0() const { return ____open_0; }
	inline bool* get_address_of__open_0() { return &____open_0; }
	inline void set__open_0(bool value)
	{
		____open_0 = value;
	}

	inline static int32_t get_offset_of__busyTimeout_1() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____busyTimeout_1)); }
	inline TimeSpan_t881159249  get__busyTimeout_1() const { return ____busyTimeout_1; }
	inline TimeSpan_t881159249 * get_address_of__busyTimeout_1() { return &____busyTimeout_1; }
	inline void set__busyTimeout_1(TimeSpan_t881159249  value)
	{
		____busyTimeout_1 = value;
	}

	inline static int32_t get_offset_of__mappings_2() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____mappings_2)); }
	inline Dictionary_2_t2061655791 * get__mappings_2() const { return ____mappings_2; }
	inline Dictionary_2_t2061655791 ** get_address_of__mappings_2() { return &____mappings_2; }
	inline void set__mappings_2(Dictionary_2_t2061655791 * value)
	{
		____mappings_2 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_2), value);
	}

	inline static int32_t get_offset_of__tables_3() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____tables_3)); }
	inline Dictionary_2_t2061655791 * get__tables_3() const { return ____tables_3; }
	inline Dictionary_2_t2061655791 ** get_address_of__tables_3() { return &____tables_3; }
	inline void set__tables_3(Dictionary_2_t2061655791 * value)
	{
		____tables_3 = value;
		Il2CppCodeGenWriteBarrier((&____tables_3), value);
	}

	inline static int32_t get_offset_of__sw_4() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____sw_4)); }
	inline Stopwatch_t305734070 * get__sw_4() const { return ____sw_4; }
	inline Stopwatch_t305734070 ** get_address_of__sw_4() { return &____sw_4; }
	inline void set__sw_4(Stopwatch_t305734070 * value)
	{
		____sw_4 = value;
		Il2CppCodeGenWriteBarrier((&____sw_4), value);
	}

	inline static int32_t get_offset_of__elapsedMilliseconds_5() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____elapsedMilliseconds_5)); }
	inline int64_t get__elapsedMilliseconds_5() const { return ____elapsedMilliseconds_5; }
	inline int64_t* get_address_of__elapsedMilliseconds_5() { return &____elapsedMilliseconds_5; }
	inline void set__elapsedMilliseconds_5(int64_t value)
	{
		____elapsedMilliseconds_5 = value;
	}

	inline static int32_t get_offset_of__transactionDepth_6() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____transactionDepth_6)); }
	inline int32_t get__transactionDepth_6() const { return ____transactionDepth_6; }
	inline int32_t* get_address_of__transactionDepth_6() { return &____transactionDepth_6; }
	inline void set__transactionDepth_6(int32_t value)
	{
		____transactionDepth_6 = value;
	}

	inline static int32_t get_offset_of__rand_7() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ____rand_7)); }
	inline Random_t108471755 * get__rand_7() const { return ____rand_7; }
	inline Random_t108471755 ** get_address_of__rand_7() { return &____rand_7; }
	inline void set__rand_7(Random_t108471755 * value)
	{
		____rand_7 = value;
		Il2CppCodeGenWriteBarrier((&____rand_7), value);
	}

	inline static int32_t get_offset_of_U3CHandleU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ___U3CHandleU3Ek__BackingField_8)); }
	inline intptr_t get_U3CHandleU3Ek__BackingField_8() const { return ___U3CHandleU3Ek__BackingField_8; }
	inline intptr_t* get_address_of_U3CHandleU3Ek__BackingField_8() { return &___U3CHandleU3Ek__BackingField_8; }
	inline void set_U3CHandleU3Ek__BackingField_8(intptr_t value)
	{
		___U3CHandleU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CDatabasePathU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ___U3CDatabasePathU3Ek__BackingField_10)); }
	inline String_t* get_U3CDatabasePathU3Ek__BackingField_10() const { return ___U3CDatabasePathU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CDatabasePathU3Ek__BackingField_10() { return &___U3CDatabasePathU3Ek__BackingField_10; }
	inline void set_U3CDatabasePathU3Ek__BackingField_10(String_t* value)
	{
		___U3CDatabasePathU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDatabasePathU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTimeExecutionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ___U3CTimeExecutionU3Ek__BackingField_11)); }
	inline bool get_U3CTimeExecutionU3Ek__BackingField_11() const { return ___U3CTimeExecutionU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CTimeExecutionU3Ek__BackingField_11() { return &___U3CTimeExecutionU3Ek__BackingField_11; }
	inline void set_U3CTimeExecutionU3Ek__BackingField_11(bool value)
	{
		___U3CTimeExecutionU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CTraceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ___U3CTraceU3Ek__BackingField_12)); }
	inline bool get_U3CTraceU3Ek__BackingField_12() const { return ___U3CTraceU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CTraceU3Ek__BackingField_12() { return &___U3CTraceU3Ek__BackingField_12; }
	inline void set_U3CTraceU3Ek__BackingField_12(bool value)
	{
		___U3CTraceU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188, ___U3CStoreDateTimeAsTicksU3Ek__BackingField_13)); }
	inline bool get_U3CStoreDateTimeAsTicksU3Ek__BackingField_13() const { return ___U3CStoreDateTimeAsTicksU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_13() { return &___U3CStoreDateTimeAsTicksU3Ek__BackingField_13; }
	inline void set_U3CStoreDateTimeAsTicksU3Ek__BackingField_13(bool value)
	{
		___U3CStoreDateTimeAsTicksU3Ek__BackingField_13 = value;
	}
};

struct SQLiteConnection_t3070079188_StaticFields
{
public:
	// System.IntPtr SQLite4Unity3d.SQLiteConnection::NullHandle
	intptr_t ___NullHandle_9;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::_preserveDuringLinkMagic
	bool ____preserveDuringLinkMagic_14;
	// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.Int32> SQLite4Unity3d.SQLiteConnection::<>f__am$cache0
	Func_2_t166798807 * ___U3CU3Ef__amU24cache0_15;
	// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String> SQLite4Unity3d.SQLiteConnection::<>f__am$cache1
	Func_2_t3358271039 * ___U3CU3Ef__amU24cache1_16;
	// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.String> SQLite4Unity3d.SQLiteConnection::<>f__am$cache2
	Func_2_t2431212468 * ___U3CU3Ef__amU24cache2_17;

public:
	inline static int32_t get_offset_of_NullHandle_9() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188_StaticFields, ___NullHandle_9)); }
	inline intptr_t get_NullHandle_9() const { return ___NullHandle_9; }
	inline intptr_t* get_address_of_NullHandle_9() { return &___NullHandle_9; }
	inline void set_NullHandle_9(intptr_t value)
	{
		___NullHandle_9 = value;
	}

	inline static int32_t get_offset_of__preserveDuringLinkMagic_14() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188_StaticFields, ____preserveDuringLinkMagic_14)); }
	inline bool get__preserveDuringLinkMagic_14() const { return ____preserveDuringLinkMagic_14; }
	inline bool* get_address_of__preserveDuringLinkMagic_14() { return &____preserveDuringLinkMagic_14; }
	inline void set__preserveDuringLinkMagic_14(bool value)
	{
		____preserveDuringLinkMagic_14 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Func_2_t166798807 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Func_2_t166798807 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Func_2_t166798807 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_16() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188_StaticFields, ___U3CU3Ef__amU24cache1_16)); }
	inline Func_2_t3358271039 * get_U3CU3Ef__amU24cache1_16() const { return ___U3CU3Ef__amU24cache1_16; }
	inline Func_2_t3358271039 ** get_address_of_U3CU3Ef__amU24cache1_16() { return &___U3CU3Ef__amU24cache1_16; }
	inline void set_U3CU3Ef__amU24cache1_16(Func_2_t3358271039 * value)
	{
		___U3CU3Ef__amU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_17() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3070079188_StaticFields, ___U3CU3Ef__amU24cache2_17)); }
	inline Func_2_t2431212468 * get_U3CU3Ef__amU24cache2_17() const { return ___U3CU3Ef__amU24cache2_17; }
	inline Func_2_t2431212468 ** get_address_of_U3CU3Ef__amU24cache2_17() { return &___U3CU3Ef__amU24cache2_17; }
	inline void set_U3CU3Ef__amU24cache2_17(Func_2_t2431212468 * value)
	{
		___U3CU3Ef__amU24cache2_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQLITECONNECTION_T3070079188_H
#ifndef COLUMN_T1357940583_H
#define COLUMN_T1357940583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.TableMapping/Column
struct  Column_t1357940583  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo SQLite4Unity3d.TableMapping/Column::_prop
	PropertyInfo_t * ____prop_0;
	// System.String SQLite4Unity3d.TableMapping/Column::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Type SQLite4Unity3d.TableMapping/Column::<ColumnType>k__BackingField
	Type_t * ___U3CColumnTypeU3Ek__BackingField_2;
	// System.String SQLite4Unity3d.TableMapping/Column::<Collation>k__BackingField
	String_t* ___U3CCollationU3Ek__BackingField_3;
	// System.Boolean SQLite4Unity3d.TableMapping/Column::<IsAutoInc>k__BackingField
	bool ___U3CIsAutoIncU3Ek__BackingField_4;
	// System.Boolean SQLite4Unity3d.TableMapping/Column::<IsAutoGuid>k__BackingField
	bool ___U3CIsAutoGuidU3Ek__BackingField_5;
	// System.Boolean SQLite4Unity3d.TableMapping/Column::<IsPK>k__BackingField
	bool ___U3CIsPKU3Ek__BackingField_6;
	// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.IndexedAttribute> SQLite4Unity3d.TableMapping/Column::<Indices>k__BackingField
	RuntimeObject* ___U3CIndicesU3Ek__BackingField_7;
	// System.Boolean SQLite4Unity3d.TableMapping/Column::<IsNullable>k__BackingField
	bool ___U3CIsNullableU3Ek__BackingField_8;
	// System.Nullable`1<System.Int32> SQLite4Unity3d.TableMapping/Column::<MaxStringLength>k__BackingField
	Nullable_1_t378540539  ___U3CMaxStringLengthU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__prop_0() { return static_cast<int32_t>(offsetof(Column_t1357940583, ____prop_0)); }
	inline PropertyInfo_t * get__prop_0() const { return ____prop_0; }
	inline PropertyInfo_t ** get_address_of__prop_0() { return &____prop_0; }
	inline void set__prop_0(PropertyInfo_t * value)
	{
		____prop_0 = value;
		Il2CppCodeGenWriteBarrier((&____prop_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CColumnTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CColumnTypeU3Ek__BackingField_2)); }
	inline Type_t * get_U3CColumnTypeU3Ek__BackingField_2() const { return ___U3CColumnTypeU3Ek__BackingField_2; }
	inline Type_t ** get_address_of_U3CColumnTypeU3Ek__BackingField_2() { return &___U3CColumnTypeU3Ek__BackingField_2; }
	inline void set_U3CColumnTypeU3Ek__BackingField_2(Type_t * value)
	{
		___U3CColumnTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CColumnTypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCollationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CCollationU3Ek__BackingField_3)); }
	inline String_t* get_U3CCollationU3Ek__BackingField_3() const { return ___U3CCollationU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCollationU3Ek__BackingField_3() { return &___U3CCollationU3Ek__BackingField_3; }
	inline void set_U3CCollationU3Ek__BackingField_3(String_t* value)
	{
		___U3CCollationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollationU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CIsAutoIncU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CIsAutoIncU3Ek__BackingField_4)); }
	inline bool get_U3CIsAutoIncU3Ek__BackingField_4() const { return ___U3CIsAutoIncU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsAutoIncU3Ek__BackingField_4() { return &___U3CIsAutoIncU3Ek__BackingField_4; }
	inline void set_U3CIsAutoIncU3Ek__BackingField_4(bool value)
	{
		___U3CIsAutoIncU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsAutoGuidU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CIsAutoGuidU3Ek__BackingField_5)); }
	inline bool get_U3CIsAutoGuidU3Ek__BackingField_5() const { return ___U3CIsAutoGuidU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsAutoGuidU3Ek__BackingField_5() { return &___U3CIsAutoGuidU3Ek__BackingField_5; }
	inline void set_U3CIsAutoGuidU3Ek__BackingField_5(bool value)
	{
		___U3CIsAutoGuidU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsPKU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CIsPKU3Ek__BackingField_6)); }
	inline bool get_U3CIsPKU3Ek__BackingField_6() const { return ___U3CIsPKU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsPKU3Ek__BackingField_6() { return &___U3CIsPKU3Ek__BackingField_6; }
	inline void set_U3CIsPKU3Ek__BackingField_6(bool value)
	{
		___U3CIsPKU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIndicesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CIndicesU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CIndicesU3Ek__BackingField_7() const { return ___U3CIndicesU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CIndicesU3Ek__BackingField_7() { return &___U3CIndicesU3Ek__BackingField_7; }
	inline void set_U3CIndicesU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CIndicesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndicesU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CIsNullableU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CIsNullableU3Ek__BackingField_8)); }
	inline bool get_U3CIsNullableU3Ek__BackingField_8() const { return ___U3CIsNullableU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsNullableU3Ek__BackingField_8() { return &___U3CIsNullableU3Ek__BackingField_8; }
	inline void set_U3CIsNullableU3Ek__BackingField_8(bool value)
	{
		___U3CIsNullableU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CMaxStringLengthU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Column_t1357940583, ___U3CMaxStringLengthU3Ek__BackingField_9)); }
	inline Nullable_1_t378540539  get_U3CMaxStringLengthU3Ek__BackingField_9() const { return ___U3CMaxStringLengthU3Ek__BackingField_9; }
	inline Nullable_1_t378540539 * get_address_of_U3CMaxStringLengthU3Ek__BackingField_9() { return &___U3CMaxStringLengthU3Ek__BackingField_9; }
	inline void set_U3CMaxStringLengthU3Ek__BackingField_9(Nullable_1_t378540539  value)
	{
		___U3CMaxStringLengthU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMN_T1357940583_H
#ifndef UNIQUEATTRIBUTE_T47190802_H
#define UNIQUEATTRIBUTE_T47190802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.UniqueAttribute
struct  UniqueAttribute_t47190802  : public IndexedAttribute_t384353508
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIQUEATTRIBUTE_T47190802_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef CONFIGOPTION_T3994509900_H
#define CONFIGOPTION_T3994509900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLite3/ConfigOption
struct  ConfigOption_t3994509900 
{
public:
	// System.Int32 SQLite4Unity3d.SQLite3/ConfigOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigOption_t3994509900, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGOPTION_T3994509900_H
#ifndef EXTENDEDRESULT_T3125463630_H
#define EXTENDEDRESULT_T3125463630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLite3/ExtendedResult
struct  ExtendedResult_t3125463630 
{
public:
	// System.Int32 SQLite4Unity3d.SQLite3/ExtendedResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ExtendedResult_t3125463630, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDRESULT_T3125463630_H
#ifndef RESULT_T1053832893_H
#define RESULT_T1053832893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLite3/Result
struct  Result_t1053832893 
{
public:
	// System.Int32 SQLite4Unity3d.SQLite3/Result::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Result_t1053832893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T1053832893_H
#ifndef COLTYPE_T3265409004_H
#define COLTYPE_T3265409004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLite3/ColType
struct  ColType_t3265409004 
{
public:
	// System.Int32 SQLite4Unity3d.SQLite3/ColType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColType_t3265409004, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLTYPE_T3265409004_H
#ifndef BEARINGFILTER_T336705813_H
#define BEARINGFILTER_T336705813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.BearingFilter
struct  BearingFilter_t336705813 
{
public:
	// System.Nullable`1<System.Double> Mapbox.Utils.BearingFilter::Bearing
	Nullable_1_t2317227445  ___Bearing_0;
	// System.Nullable`1<System.Double> Mapbox.Utils.BearingFilter::Range
	Nullable_1_t2317227445  ___Range_1;

public:
	inline static int32_t get_offset_of_Bearing_0() { return static_cast<int32_t>(offsetof(BearingFilter_t336705813, ___Bearing_0)); }
	inline Nullable_1_t2317227445  get_Bearing_0() const { return ___Bearing_0; }
	inline Nullable_1_t2317227445 * get_address_of_Bearing_0() { return &___Bearing_0; }
	inline void set_Bearing_0(Nullable_1_t2317227445  value)
	{
		___Bearing_0 = value;
	}

	inline static int32_t get_offset_of_Range_1() { return static_cast<int32_t>(offsetof(BearingFilter_t336705813, ___Range_1)); }
	inline Nullable_1_t2317227445  get_Range_1() const { return ___Range_1; }
	inline Nullable_1_t2317227445 * get_address_of_Range_1() { return &___Range_1; }
	inline void set_Range_1(Nullable_1_t2317227445  value)
	{
		___Range_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Utils.BearingFilter
struct BearingFilter_t336705813_marshaled_pinvoke
{
	Nullable_1_t2317227445  ___Bearing_0;
	Nullable_1_t2317227445  ___Range_1;
};
// Native definition for COM marshalling of Mapbox.Utils.BearingFilter
struct BearingFilter_t336705813_marshaled_com
{
	Nullable_1_t2317227445  ___Bearing_0;
	Nullable_1_t2317227445  ___Range_1;
};
#endif // BEARINGFILTER_T336705813_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef SQLITEEXCEPTION_T1356363765_H
#define SQLITEEXCEPTION_T1356363765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteException
struct  SQLiteException_t1356363765  : public Exception_t
{
public:
	// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLiteException::<Result>k__BackingField
	int32_t ___U3CResultU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SQLiteException_t1356363765, ___U3CResultU3Ek__BackingField_11)); }
	inline int32_t get_U3CResultU3Ek__BackingField_11() const { return ___U3CResultU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CResultU3Ek__BackingField_11() { return &___U3CResultU3Ek__BackingField_11; }
	inline void set_U3CResultU3Ek__BackingField_11(int32_t value)
	{
		___U3CResultU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQLITEEXCEPTION_T1356363765_H
#ifndef TILE_T1097901272_H
#define TILE_T1097901272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.Tile
struct  Tile_t1097901272  : public RuntimeObject
{
public:
	// Mapbox.Map.CanonicalTileId Mapbox.Map.Tile::_id
	CanonicalTileId_t4184902996  ____id_0;
	// System.Collections.Generic.List`1<System.Exception> Mapbox.Map.Tile::_exceptions
	List_1_t2908811991 * ____exceptions_1;
	// Mapbox.Map.Tile/State Mapbox.Map.Tile::_state
	int32_t ____state_2;
	// Mapbox.Platform.IAsyncRequest Mapbox.Map.Tile::_request
	RuntimeObject* ____request_3;
	// System.Action Mapbox.Map.Tile::_callback
	Action_t1264377477 * ____callback_4;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(Tile_t1097901272, ____id_0)); }
	inline CanonicalTileId_t4184902996  get__id_0() const { return ____id_0; }
	inline CanonicalTileId_t4184902996 * get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(CanonicalTileId_t4184902996  value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__exceptions_1() { return static_cast<int32_t>(offsetof(Tile_t1097901272, ____exceptions_1)); }
	inline List_1_t2908811991 * get__exceptions_1() const { return ____exceptions_1; }
	inline List_1_t2908811991 ** get_address_of__exceptions_1() { return &____exceptions_1; }
	inline void set__exceptions_1(List_1_t2908811991 * value)
	{
		____exceptions_1 = value;
		Il2CppCodeGenWriteBarrier((&____exceptions_1), value);
	}

	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(Tile_t1097901272, ____state_2)); }
	inline int32_t get__state_2() const { return ____state_2; }
	inline int32_t* get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(int32_t value)
	{
		____state_2 = value;
	}

	inline static int32_t get_offset_of__request_3() { return static_cast<int32_t>(offsetof(Tile_t1097901272, ____request_3)); }
	inline RuntimeObject* get__request_3() const { return ____request_3; }
	inline RuntimeObject** get_address_of__request_3() { return &____request_3; }
	inline void set__request_3(RuntimeObject* value)
	{
		____request_3 = value;
		Il2CppCodeGenWriteBarrier((&____request_3), value);
	}

	inline static int32_t get_offset_of__callback_4() { return static_cast<int32_t>(offsetof(Tile_t1097901272, ____callback_4)); }
	inline Action_t1264377477 * get__callback_4() const { return ____callback_4; }
	inline Action_t1264377477 ** get_address_of__callback_4() { return &____callback_4; }
	inline void set__callback_4(Action_t1264377477 * value)
	{
		____callback_4 = value;
		Il2CppCodeGenWriteBarrier((&____callback_4), value);
	}
};

struct Tile_t1097901272_StaticFields
{
public:
	// System.Func`2<System.Exception,System.String> Mapbox.Map.Tile::<>f__am$cache0
	Func_2_t905536786 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Tile_t1097901272_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t905536786 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t905536786 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t905536786 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILE_T1097901272_H
#ifndef STYLEOPTIMIZEDVECTORTILE_T1819232013_H
#define STYLEOPTIMIZEDVECTORTILE_T1819232013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.StyleOptimizedVectorTile
struct  StyleOptimizedVectorTile_t1819232013  : public Tile_t1097901272
{
public:
	// Mapbox.VectorTile.VectorTile Mapbox.Map.StyleOptimizedVectorTile::data
	VectorTile_t3467883484 * ___data_6;
	// System.String Mapbox.Map.StyleOptimizedVectorTile::_optimizedStyleId
	String_t* ____optimizedStyleId_7;
	// System.String Mapbox.Map.StyleOptimizedVectorTile::_modifiedDate
	String_t* ____modifiedDate_8;
	// System.Boolean Mapbox.Map.StyleOptimizedVectorTile::isDisposed
	bool ___isDisposed_9;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTile_t1819232013, ___data_6)); }
	inline VectorTile_t3467883484 * get_data_6() const { return ___data_6; }
	inline VectorTile_t3467883484 ** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(VectorTile_t3467883484 * value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}

	inline static int32_t get_offset_of__optimizedStyleId_7() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTile_t1819232013, ____optimizedStyleId_7)); }
	inline String_t* get__optimizedStyleId_7() const { return ____optimizedStyleId_7; }
	inline String_t** get_address_of__optimizedStyleId_7() { return &____optimizedStyleId_7; }
	inline void set__optimizedStyleId_7(String_t* value)
	{
		____optimizedStyleId_7 = value;
		Il2CppCodeGenWriteBarrier((&____optimizedStyleId_7), value);
	}

	inline static int32_t get_offset_of__modifiedDate_8() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTile_t1819232013, ____modifiedDate_8)); }
	inline String_t* get__modifiedDate_8() const { return ____modifiedDate_8; }
	inline String_t** get_address_of__modifiedDate_8() { return &____modifiedDate_8; }
	inline void set__modifiedDate_8(String_t* value)
	{
		____modifiedDate_8 = value;
		Il2CppCodeGenWriteBarrier((&____modifiedDate_8), value);
	}

	inline static int32_t get_offset_of_isDisposed_9() { return static_cast<int32_t>(offsetof(StyleOptimizedVectorTile_t1819232013, ___isDisposed_9)); }
	inline bool get_isDisposed_9() const { return ___isDisposed_9; }
	inline bool* get_address_of_isDisposed_9() { return &___isDisposed_9; }
	inline void set_isDisposed_9(bool value)
	{
		___isDisposed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLEOPTIMIZEDVECTORTILE_T1819232013_H
#ifndef VECTORTILE_T4284514353_H
#define VECTORTILE_T4284514353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.VectorTile
struct  VectorTile_t4284514353  : public Tile_t1097901272
{
public:
	// Mapbox.VectorTile.VectorTile Mapbox.Map.VectorTile::data
	VectorTile_t3467883484 * ___data_6;
	// System.Boolean Mapbox.Map.VectorTile::isDisposed
	bool ___isDisposed_7;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(VectorTile_t4284514353, ___data_6)); }
	inline VectorTile_t3467883484 * get_data_6() const { return ___data_6; }
	inline VectorTile_t3467883484 ** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(VectorTile_t3467883484 * value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}

	inline static int32_t get_offset_of_isDisposed_7() { return static_cast<int32_t>(offsetof(VectorTile_t4284514353, ___isDisposed_7)); }
	inline bool get_isDisposed_7() const { return ___isDisposed_7; }
	inline bool* get_address_of_isDisposed_7() { return &___isDisposed_7; }
	inline void set_isDisposed_7(bool value)
	{
		___isDisposed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILE_T4284514353_H
#ifndef NOTNULLCONSTRAINTVIOLATIONEXCEPTION_T3886364687_H
#define NOTNULLCONSTRAINTVIOLATIONEXCEPTION_T3886364687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.NotNullConstraintViolationException
struct  NotNullConstraintViolationException_t3886364687  : public SQLiteException_t1356363765
{
public:
	// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping/Column> SQLite4Unity3d.NotNullConstraintViolationException::<Columns>k__BackingField
	RuntimeObject* ___U3CColumnsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CColumnsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(NotNullConstraintViolationException_t3886364687, ___U3CColumnsU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CColumnsU3Ek__BackingField_12() const { return ___U3CColumnsU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CColumnsU3Ek__BackingField_12() { return &___U3CColumnsU3Ek__BackingField_12; }
	inline void set_U3CColumnsU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CColumnsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CColumnsU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTNULLCONSTRAINTVIOLATIONEXCEPTION_T3886364687_H
#ifndef RASTERTILE_T2895285341_H
#define RASTERTILE_T2895285341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.RasterTile
struct  RasterTile_t2895285341  : public Tile_t1097901272
{
public:
	// System.Byte[] Mapbox.Map.RasterTile::data
	ByteU5BU5D_t4116647657* ___data_6;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(RasterTile_t2895285341, ___data_6)); }
	inline ByteU5BU5D_t4116647657* get_data_6() const { return ___data_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(ByteU5BU5D_t4116647657* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RASTERTILE_T2895285341_H
#ifndef NULLABLE_1_T1166124571_H
#define NULLABLE_1_T1166124571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t1166124571 
{
public:
	// T System.Nullable`1::value
	DateTime_t3738529785  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___value_0)); }
	inline DateTime_t3738529785  get_value_0() const { return ___value_0; }
	inline DateTime_t3738529785 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t3738529785  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1166124571_H
#ifndef RESPONSE_T1055672877_H
#define RESPONSE_T1055672877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.Response
struct  Response_t1055672877  : public RuntimeObject
{
public:
	// Mapbox.Platform.IAsyncRequest Mapbox.Platform.Response::<Request>k__BackingField
	RuntimeObject* ___U3CRequestU3Ek__BackingField_0;
	// System.Boolean Mapbox.Platform.Response::LoadedFromCache
	bool ___LoadedFromCache_1;
	// System.String Mapbox.Platform.Response::RequestUrl
	String_t* ___RequestUrl_2;
	// System.Nullable`1<System.Int32> Mapbox.Platform.Response::StatusCode
	Nullable_1_t378540539  ___StatusCode_3;
	// System.String Mapbox.Platform.Response::ContentType
	String_t* ___ContentType_4;
	// System.Nullable`1<System.Int32> Mapbox.Platform.Response::XRateLimitInterval
	Nullable_1_t378540539  ___XRateLimitInterval_5;
	// System.Nullable`1<System.Int64> Mapbox.Platform.Response::XRateLimitLimit
	Nullable_1_t1164162090  ___XRateLimitLimit_6;
	// System.Nullable`1<System.DateTime> Mapbox.Platform.Response::XRateLimitReset
	Nullable_1_t1166124571  ___XRateLimitReset_7;
	// System.Collections.Generic.List`1<System.Exception> Mapbox.Platform.Response::_exceptions
	List_1_t2908811991 * ____exceptions_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Mapbox.Platform.Response::Headers
	Dictionary_2_t1632706988 * ___Headers_9;
	// System.Byte[] Mapbox.Platform.Response::Data
	ByteU5BU5D_t4116647657* ___Data_10;

public:
	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___U3CRequestU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CRequestU3Ek__BackingField_0() const { return ___U3CRequestU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CRequestU3Ek__BackingField_0() { return &___U3CRequestU3Ek__BackingField_0; }
	inline void set_U3CRequestU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CRequestU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_LoadedFromCache_1() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___LoadedFromCache_1)); }
	inline bool get_LoadedFromCache_1() const { return ___LoadedFromCache_1; }
	inline bool* get_address_of_LoadedFromCache_1() { return &___LoadedFromCache_1; }
	inline void set_LoadedFromCache_1(bool value)
	{
		___LoadedFromCache_1 = value;
	}

	inline static int32_t get_offset_of_RequestUrl_2() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___RequestUrl_2)); }
	inline String_t* get_RequestUrl_2() const { return ___RequestUrl_2; }
	inline String_t** get_address_of_RequestUrl_2() { return &___RequestUrl_2; }
	inline void set_RequestUrl_2(String_t* value)
	{
		___RequestUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___RequestUrl_2), value);
	}

	inline static int32_t get_offset_of_StatusCode_3() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___StatusCode_3)); }
	inline Nullable_1_t378540539  get_StatusCode_3() const { return ___StatusCode_3; }
	inline Nullable_1_t378540539 * get_address_of_StatusCode_3() { return &___StatusCode_3; }
	inline void set_StatusCode_3(Nullable_1_t378540539  value)
	{
		___StatusCode_3 = value;
	}

	inline static int32_t get_offset_of_ContentType_4() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___ContentType_4)); }
	inline String_t* get_ContentType_4() const { return ___ContentType_4; }
	inline String_t** get_address_of_ContentType_4() { return &___ContentType_4; }
	inline void set_ContentType_4(String_t* value)
	{
		___ContentType_4 = value;
		Il2CppCodeGenWriteBarrier((&___ContentType_4), value);
	}

	inline static int32_t get_offset_of_XRateLimitInterval_5() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___XRateLimitInterval_5)); }
	inline Nullable_1_t378540539  get_XRateLimitInterval_5() const { return ___XRateLimitInterval_5; }
	inline Nullable_1_t378540539 * get_address_of_XRateLimitInterval_5() { return &___XRateLimitInterval_5; }
	inline void set_XRateLimitInterval_5(Nullable_1_t378540539  value)
	{
		___XRateLimitInterval_5 = value;
	}

	inline static int32_t get_offset_of_XRateLimitLimit_6() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___XRateLimitLimit_6)); }
	inline Nullable_1_t1164162090  get_XRateLimitLimit_6() const { return ___XRateLimitLimit_6; }
	inline Nullable_1_t1164162090 * get_address_of_XRateLimitLimit_6() { return &___XRateLimitLimit_6; }
	inline void set_XRateLimitLimit_6(Nullable_1_t1164162090  value)
	{
		___XRateLimitLimit_6 = value;
	}

	inline static int32_t get_offset_of_XRateLimitReset_7() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___XRateLimitReset_7)); }
	inline Nullable_1_t1166124571  get_XRateLimitReset_7() const { return ___XRateLimitReset_7; }
	inline Nullable_1_t1166124571 * get_address_of_XRateLimitReset_7() { return &___XRateLimitReset_7; }
	inline void set_XRateLimitReset_7(Nullable_1_t1166124571  value)
	{
		___XRateLimitReset_7 = value;
	}

	inline static int32_t get_offset_of__exceptions_8() { return static_cast<int32_t>(offsetof(Response_t1055672877, ____exceptions_8)); }
	inline List_1_t2908811991 * get__exceptions_8() const { return ____exceptions_8; }
	inline List_1_t2908811991 ** get_address_of__exceptions_8() { return &____exceptions_8; }
	inline void set__exceptions_8(List_1_t2908811991 * value)
	{
		____exceptions_8 = value;
		Il2CppCodeGenWriteBarrier((&____exceptions_8), value);
	}

	inline static int32_t get_offset_of_Headers_9() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___Headers_9)); }
	inline Dictionary_2_t1632706988 * get_Headers_9() const { return ___Headers_9; }
	inline Dictionary_2_t1632706988 ** get_address_of_Headers_9() { return &___Headers_9; }
	inline void set_Headers_9(Dictionary_2_t1632706988 * value)
	{
		___Headers_9 = value;
		Il2CppCodeGenWriteBarrier((&___Headers_9), value);
	}

	inline static int32_t get_offset_of_Data_10() { return static_cast<int32_t>(offsetof(Response_t1055672877, ___Data_10)); }
	inline ByteU5BU5D_t4116647657* get_Data_10() const { return ___Data_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_Data_10() { return &___Data_10; }
	inline void set_Data_10(ByteU5BU5D_t4116647657* value)
	{
		___Data_10 = value;
		Il2CppCodeGenWriteBarrier((&___Data_10), value);
	}
};

struct Response_t1055672877_StaticFields
{
public:
	// System.Func`2<System.Exception,System.String> Mapbox.Platform.Response::<>f__am$cache0
	Func_2_t905536786 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(Response_t1055672877_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_2_t905536786 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_2_t905536786 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_2_t905536786 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSE_T1055672877_H
#ifndef FILESOURCE_T3962300185_H
#define FILESOURCE_T3962300185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Platform.FileSource
struct  FileSource_t3962300185  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Mapbox.Platform.IAsyncRequest,System.Int32> Mapbox.Platform.FileSource::_requests
	Dictionary_2_t1445974517 * ____requests_0;
	// System.String Mapbox.Platform.FileSource::_accessToken
	String_t* ____accessToken_1;
	// System.Object Mapbox.Platform.FileSource::_lock
	RuntimeObject * ____lock_2;
	// System.Nullable`1<System.Int32> Mapbox.Platform.FileSource::XRateLimitInterval
	Nullable_1_t378540539  ___XRateLimitInterval_3;
	// System.Nullable`1<System.Int64> Mapbox.Platform.FileSource::XRateLimitLimit
	Nullable_1_t1164162090  ___XRateLimitLimit_4;
	// System.Nullable`1<System.DateTime> Mapbox.Platform.FileSource::XRateLimitReset
	Nullable_1_t1166124571  ___XRateLimitReset_5;

public:
	inline static int32_t get_offset_of__requests_0() { return static_cast<int32_t>(offsetof(FileSource_t3962300185, ____requests_0)); }
	inline Dictionary_2_t1445974517 * get__requests_0() const { return ____requests_0; }
	inline Dictionary_2_t1445974517 ** get_address_of__requests_0() { return &____requests_0; }
	inline void set__requests_0(Dictionary_2_t1445974517 * value)
	{
		____requests_0 = value;
		Il2CppCodeGenWriteBarrier((&____requests_0), value);
	}

	inline static int32_t get_offset_of__accessToken_1() { return static_cast<int32_t>(offsetof(FileSource_t3962300185, ____accessToken_1)); }
	inline String_t* get__accessToken_1() const { return ____accessToken_1; }
	inline String_t** get_address_of__accessToken_1() { return &____accessToken_1; }
	inline void set__accessToken_1(String_t* value)
	{
		____accessToken_1 = value;
		Il2CppCodeGenWriteBarrier((&____accessToken_1), value);
	}

	inline static int32_t get_offset_of__lock_2() { return static_cast<int32_t>(offsetof(FileSource_t3962300185, ____lock_2)); }
	inline RuntimeObject * get__lock_2() const { return ____lock_2; }
	inline RuntimeObject ** get_address_of__lock_2() { return &____lock_2; }
	inline void set__lock_2(RuntimeObject * value)
	{
		____lock_2 = value;
		Il2CppCodeGenWriteBarrier((&____lock_2), value);
	}

	inline static int32_t get_offset_of_XRateLimitInterval_3() { return static_cast<int32_t>(offsetof(FileSource_t3962300185, ___XRateLimitInterval_3)); }
	inline Nullable_1_t378540539  get_XRateLimitInterval_3() const { return ___XRateLimitInterval_3; }
	inline Nullable_1_t378540539 * get_address_of_XRateLimitInterval_3() { return &___XRateLimitInterval_3; }
	inline void set_XRateLimitInterval_3(Nullable_1_t378540539  value)
	{
		___XRateLimitInterval_3 = value;
	}

	inline static int32_t get_offset_of_XRateLimitLimit_4() { return static_cast<int32_t>(offsetof(FileSource_t3962300185, ___XRateLimitLimit_4)); }
	inline Nullable_1_t1164162090  get_XRateLimitLimit_4() const { return ___XRateLimitLimit_4; }
	inline Nullable_1_t1164162090 * get_address_of_XRateLimitLimit_4() { return &___XRateLimitLimit_4; }
	inline void set_XRateLimitLimit_4(Nullable_1_t1164162090  value)
	{
		___XRateLimitLimit_4 = value;
	}

	inline static int32_t get_offset_of_XRateLimitReset_5() { return static_cast<int32_t>(offsetof(FileSource_t3962300185, ___XRateLimitReset_5)); }
	inline Nullable_1_t1166124571  get_XRateLimitReset_5() const { return ___XRateLimitReset_5; }
	inline Nullable_1_t1166124571 * get_address_of_XRateLimitReset_5() { return &___XRateLimitReset_5; }
	inline void set_XRateLimitReset_5(Nullable_1_t1166124571  value)
	{
		___XRateLimitReset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESOURCE_T3962300185_H
#ifndef CLASSICRASTERTILE_T3659923687_H
#define CLASSICRASTERTILE_T3659923687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.ClassicRasterTile
struct  ClassicRasterTile_t3659923687  : public RasterTile_t2895285341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSICRASTERTILE_T3659923687_H
#ifndef RAWPNGRASTERTILE_T2301039290_H
#define RAWPNGRASTERTILE_T2301039290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.RawPngRasterTile
struct  RawPngRasterTile_t2301039290  : public RasterTile_t2895285341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWPNGRASTERTILE_T2301039290_H
#ifndef RETINARASTERTILE_T51501800_H
#define RETINARASTERTILE_T51501800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.RetinaRasterTile
struct  RetinaRasterTile_t51501800  : public RasterTile_t2895285341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETINARASTERTILE_T51501800_H
#ifndef CLASSICRETINARASTERTILE_T3131700875_H
#define CLASSICRETINARASTERTILE_T3131700875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Map.ClassicRetinaRasterTile
struct  ClassicRetinaRasterTile_t3131700875  : public ClassicRasterTile_t3659923687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSICRETINARASTERTILE_T3131700875_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (ReverseGeocodeResponse_t3723437562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[1] = 
{
	ReverseGeocodeResponse_t3723437562::get_offset_of_U3CQueryU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (ForwardGeocodeResponse_t2959476828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[1] = 
{
	ForwardGeocodeResponse_t2959476828::get_offset_of_U3CQueryU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (Geometry_t3224508297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[2] = 
{
	Geometry_t3224508297::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	Geometry_t3224508297::get_offset_of_U3CCoordinatesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (ReverseGeocodeResource_t2777886177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[1] = 
{
	ReverseGeocodeResource_t2777886177::get_offset_of_query_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (CanonicalTileId_t4184902996)+ sizeof (RuntimeObject), sizeof(CanonicalTileId_t4184902996 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3104[3] = 
{
	CanonicalTileId_t4184902996::get_offset_of_Z_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CanonicalTileId_t4184902996::get_offset_of_X_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CanonicalTileId_t4184902996::get_offset_of_Y_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (ClassicRasterTile_t3659923687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (ClassicRetinaRasterTile_t3131700875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3107[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (MapUtils_t115449649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (RasterTile_t2895285341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[1] = 
{
	RasterTile_t2895285341::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (RawPngRasterTile_t2301039290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (RetinaRasterTile_t51501800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (StyleOptimizedVectorTile_t1819232013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[4] = 
{
	StyleOptimizedVectorTile_t1819232013::get_offset_of_data_6(),
	StyleOptimizedVectorTile_t1819232013::get_offset_of__optimizedStyleId_7(),
	StyleOptimizedVectorTile_t1819232013::get_offset_of__modifiedDate_8(),
	StyleOptimizedVectorTile_t1819232013::get_offset_of_isDisposed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (Tile_t1097901272), -1, sizeof(Tile_t1097901272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3115[6] = 
{
	Tile_t1097901272::get_offset_of__id_0(),
	Tile_t1097901272::get_offset_of__exceptions_1(),
	Tile_t1097901272::get_offset_of__state_2(),
	Tile_t1097901272::get_offset_of__request_3(),
	Tile_t1097901272::get_offset_of__callback_4(),
	Tile_t1097901272_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (State_t2862325545)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3116[5] = 
{
	State_t2862325545::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (Parameters_t4189861888)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[3] = 
{
	Parameters_t4189861888::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parameters_t4189861888::get_offset_of_MapId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parameters_t4189861888::get_offset_of_Fs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (TileCover_t2200805285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (TileResource_t4039806838), -1, sizeof(TileResource_t4039806838_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3119[2] = 
{
	TileResource_t4039806838_StaticFields::get_offset_of__eventQuery_0(),
	TileResource_t4039806838::get_offset_of__query_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (UnwrappedTileId_t2586853537)+ sizeof (RuntimeObject), sizeof(UnwrappedTileId_t2586853537 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3120[3] = 
{
	UnwrappedTileId_t2586853537::get_offset_of_Z_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnwrappedTileId_t2586853537::get_offset_of_X_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnwrappedTileId_t2586853537::get_offset_of_Y_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (VectorTile_t4284514353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[2] = 
{
	VectorTile_t4284514353::get_offset_of_data_6(),
	VectorTile_t4284514353::get_offset_of_isDisposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (CachingWebFileSource_t328893056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[3] = 
{
	CachingWebFileSource_t328893056::get_offset_of__disposed_0(),
	CachingWebFileSource_t328893056::get_offset_of__caches_1(),
	CachingWebFileSource_t328893056::get_offset_of__accessToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (MemoryCacheAsyncRequest_t3045496022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[1] = 
{
	MemoryCacheAsyncRequest_t3045496022::get_offset_of_U3CRequestUrlU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (U3CRequestU3Ec__AnonStorey1_t3971576917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[4] = 
{
	U3CRequestU3Ec__AnonStorey1_t3971576917::get_offset_of_mapId_0(),
	U3CRequestU3Ec__AnonStorey1_t3971576917::get_offset_of_tileId_1(),
	U3CRequestU3Ec__AnonStorey1_t3971576917::get_offset_of_callback_2(),
	U3CRequestU3Ec__AnonStorey1_t3971576917::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (U3CDelayCachedResponseU3Ec__Iterator0_t4244230405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[5] = 
{
	U3CDelayCachedResponseU3Ec__Iterator0_t4244230405::get_offset_of_callback_0(),
	U3CDelayCachedResponseU3Ec__Iterator0_t4244230405::get_offset_of_response_1(),
	U3CDelayCachedResponseU3Ec__Iterator0_t4244230405::get_offset_of_U24current_2(),
	U3CDelayCachedResponseU3Ec__Iterator0_t4244230405::get_offset_of_U24disposing_3(),
	U3CDelayCachedResponseU3Ec__Iterator0_t4244230405::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (MbTilesCache_t1795842095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[4] = 
{
	MbTilesCache_t1795842095::get_offset_of__disposed_0(),
	MbTilesCache_t1795842095::get_offset_of__maxCacheSize_1(),
	MbTilesCache_t1795842095::get_offset_of__lock_2(),
	MbTilesCache_t1795842095::get_offset_of__mbTiles_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (MemoryCache_t3711317483), -1, sizeof(MemoryCache_t3711317483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3128[4] = 
{
	MemoryCache_t3711317483::get_offset_of__maxCacheSize_0(),
	MemoryCache_t3711317483::get_offset_of__lock_1(),
	MemoryCache_t3711317483::get_offset_of__cachedResponses_2(),
	MemoryCache_t3711317483_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (CacheItem_t421398482)+ sizeof (RuntimeObject), sizeof(CacheItem_t421398482_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3129[2] = 
{
	CacheItem_t421398482::get_offset_of_Timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CacheItem_t421398482::get_offset_of_Data_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (U3CClearU3Ec__AnonStorey0_t1767456338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[1] = 
{
	U3CClearU3Ec__AnonStorey0_t1767456338::get_offset_of_mapId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (FileSource_t3962300185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[6] = 
{
	FileSource_t3962300185::get_offset_of__requests_0(),
	FileSource_t3962300185::get_offset_of__accessToken_1(),
	FileSource_t3962300185::get_offset_of__lock_2(),
	FileSource_t3962300185::get_offset_of_XRateLimitInterval_3(),
	FileSource_t3962300185::get_offset_of_XRateLimitLimit_4(),
	FileSource_t3962300185::get_offset_of_XRateLimitReset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (U3CproxyResponseU3Ec__AnonStorey1_t3634450083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[2] = 
{
	U3CproxyResponseU3Ec__AnonStorey1_t3634450083::get_offset_of_callback_0(),
	U3CproxyResponseU3Ec__AnonStorey1_t3634450083::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (U3CWaitForAllRequestsU3Ec__Iterator0_t168843647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[5] = 
{
	U3CWaitForAllRequestsU3Ec__Iterator0_t168843647::get_offset_of_U24locvar0_0(),
	U3CWaitForAllRequestsU3Ec__Iterator0_t168843647::get_offset_of_U24this_1(),
	U3CWaitForAllRequestsU3Ec__Iterator0_t168843647::get_offset_of_U24current_2(),
	U3CWaitForAllRequestsU3Ec__Iterator0_t168843647::get_offset_of_U24disposing_3(),
	U3CWaitForAllRequestsU3Ec__Iterator0_t168843647::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (IAsyncRequestFactory_t4260832379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (MbTilesDb_t2334709092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[6] = 
{
	MbTilesDb_t2334709092::get_offset_of__disposed_0(),
	MbTilesDb_t2334709092::get_offset_of__dbPath_1(),
	MbTilesDb_t2334709092::get_offset_of__sqlite_2(),
	MbTilesDb_t2334709092::get_offset_of__maxTileCount_3(),
	0,
	MbTilesDb_t2334709092::get_offset_of__pruneCacheCounter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (U3CGetTileU3Ec__AnonStorey0_t2570057117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[1] = 
{
	U3CGetTileU3Ec__AnonStorey0_t2570057117::get_offset_of_tileId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (U3CTileExistsU3Ec__AnonStorey1_t694898448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[1] = 
{
	U3CTileExistsU3Ec__AnonStorey1_t694898448::get_offset_of_tileId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (metadata_t2312331196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[2] = 
{
	metadata_t2312331196::get_offset_of_U3CnameU3Ek__BackingField_0(),
	metadata_t2312331196::get_offset_of_U3CvalueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (MetaDataRequired_t3242735516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[5] = 
{
	MetaDataRequired_t3242735516::get_offset_of_U3CTilesetNameU3Ek__BackingField_0(),
	MetaDataRequired_t3242735516::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	MetaDataRequired_t3242735516::get_offset_of_U3CVersionU3Ek__BackingField_2(),
	MetaDataRequired_t3242735516::get_offset_of_U3CDescriptionU3Ek__BackingField_3(),
	MetaDataRequired_t3242735516::get_offset_of_U3CFormatU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (tiles_t2806189129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[5] = 
{
	tiles_t2806189129::get_offset_of_U3Czoom_levelU3Ek__BackingField_0(),
	tiles_t2806189129::get_offset_of_U3Ctile_columnU3Ek__BackingField_1(),
	tiles_t2806189129::get_offset_of_U3Ctile_rowU3Ek__BackingField_2(),
	tiles_t2806189129::get_offset_of_U3Ctile_dataU3Ek__BackingField_3(),
	tiles_t2806189129::get_offset_of_U3CtimestampU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (Resource_t4129330135), -1, sizeof(Resource_t4129330135_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3144[4] = 
{
	Resource_t4129330135_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	Resource_t4129330135_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	Resource_t4129330135_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	Resource_t4129330135_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (Response_t1055672877), -1, sizeof(Response_t1055672877_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3145[12] = 
{
	Response_t1055672877::get_offset_of_U3CRequestU3Ek__BackingField_0(),
	Response_t1055672877::get_offset_of_LoadedFromCache_1(),
	Response_t1055672877::get_offset_of_RequestUrl_2(),
	Response_t1055672877::get_offset_of_StatusCode_3(),
	Response_t1055672877::get_offset_of_ContentType_4(),
	Response_t1055672877::get_offset_of_XRateLimitInterval_5(),
	Response_t1055672877::get_offset_of_XRateLimitLimit_6(),
	Response_t1055672877::get_offset_of_XRateLimitReset_7(),
	Response_t1055672877::get_offset_of__exceptions_8(),
	Response_t1055672877::get_offset_of_Headers_9(),
	Response_t1055672877::get_offset_of_Data_10(),
	Response_t1055672877_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (SQLiteException_t1356363765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[1] = 
{
	SQLiteException_t1356363765::get_offset_of_U3CResultU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (NotNullConstraintViolationException_t3886364687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[1] = 
{
	NotNullConstraintViolationException_t3886364687::get_offset_of_U3CColumnsU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey0_t2023264365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[1] = 
{
	U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey0_t2023264365::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (SQLiteOpenFlags_t2333747160)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3149[12] = 
{
	SQLiteOpenFlags_t2333747160::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (CreateFlags_t2403871199)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3150[6] = 
{
	CreateFlags_t2403871199::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (SQLiteConnection_t3070079188), -1, sizeof(SQLiteConnection_t3070079188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3151[18] = 
{
	SQLiteConnection_t3070079188::get_offset_of__open_0(),
	SQLiteConnection_t3070079188::get_offset_of__busyTimeout_1(),
	SQLiteConnection_t3070079188::get_offset_of__mappings_2(),
	SQLiteConnection_t3070079188::get_offset_of__tables_3(),
	SQLiteConnection_t3070079188::get_offset_of__sw_4(),
	SQLiteConnection_t3070079188::get_offset_of__elapsedMilliseconds_5(),
	SQLiteConnection_t3070079188::get_offset_of__transactionDepth_6(),
	SQLiteConnection_t3070079188::get_offset_of__rand_7(),
	SQLiteConnection_t3070079188::get_offset_of_U3CHandleU3Ek__BackingField_8(),
	SQLiteConnection_t3070079188_StaticFields::get_offset_of_NullHandle_9(),
	SQLiteConnection_t3070079188::get_offset_of_U3CDatabasePathU3Ek__BackingField_10(),
	SQLiteConnection_t3070079188::get_offset_of_U3CTimeExecutionU3Ek__BackingField_11(),
	SQLiteConnection_t3070079188::get_offset_of_U3CTraceU3Ek__BackingField_12(),
	SQLiteConnection_t3070079188::get_offset_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_13(),
	SQLiteConnection_t3070079188_StaticFields::get_offset_of__preserveDuringLinkMagic_14(),
	SQLiteConnection_t3070079188_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
	SQLiteConnection_t3070079188_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_16(),
	SQLiteConnection_t3070079188_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (IndexedColumn_t3080689016)+ sizeof (RuntimeObject), sizeof(IndexedColumn_t3080689016_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3152[2] = 
{
	IndexedColumn_t3080689016::get_offset_of_Order_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IndexedColumn_t3080689016::get_offset_of_ColumnName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (IndexInfo_t1680796658)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[4] = 
{
	IndexInfo_t1680796658::get_offset_of_IndexName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IndexInfo_t1680796658::get_offset_of_TableName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IndexInfo_t1680796658::get_offset_of_Unique_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IndexInfo_t1680796658::get_offset_of_Columns_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (ColumnInfo_t2560133827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[2] = 
{
	ColumnInfo_t2560133827::get_offset_of_U3CNameU3Ek__BackingField_0(),
	ColumnInfo_t2560133827::get_offset_of_U3CnotnullU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (U3CInsertAllU3Ec__AnonStorey0_t106977603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[3] = 
{
	U3CInsertAllU3Ec__AnonStorey0_t106977603::get_offset_of_objects_0(),
	U3CInsertAllU3Ec__AnonStorey0_t106977603::get_offset_of_c_1(),
	U3CInsertAllU3Ec__AnonStorey0_t106977603::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (U3CInsertAllU3Ec__AnonStorey1_t106977604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[4] = 
{
	U3CInsertAllU3Ec__AnonStorey1_t106977604::get_offset_of_objects_0(),
	U3CInsertAllU3Ec__AnonStorey1_t106977604::get_offset_of_extra_1(),
	U3CInsertAllU3Ec__AnonStorey1_t106977604::get_offset_of_c_2(),
	U3CInsertAllU3Ec__AnonStorey1_t106977604::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (U3CInsertAllU3Ec__AnonStorey2_t106977601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[4] = 
{
	U3CInsertAllU3Ec__AnonStorey2_t106977601::get_offset_of_objects_0(),
	U3CInsertAllU3Ec__AnonStorey2_t106977601::get_offset_of_objType_1(),
	U3CInsertAllU3Ec__AnonStorey2_t106977601::get_offset_of_c_2(),
	U3CInsertAllU3Ec__AnonStorey2_t106977601::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (U3CUpdateU3Ec__AnonStorey3_t445741020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[2] = 
{
	U3CUpdateU3Ec__AnonStorey3_t445741020::get_offset_of_pk_0(),
	U3CUpdateU3Ec__AnonStorey3_t445741020::get_offset_of_obj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (U3CUpdateAllU3Ec__AnonStorey4_t1584503381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3159[3] = 
{
	U3CUpdateAllU3Ec__AnonStorey4_t1584503381::get_offset_of_objects_0(),
	U3CUpdateAllU3Ec__AnonStorey4_t1584503381::get_offset_of_c_1(),
	U3CUpdateAllU3Ec__AnonStorey4_t1584503381::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (SQLiteConnectionString_t3632030467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[3] = 
{
	SQLiteConnectionString_t3632030467::get_offset_of_U3CConnectionStringU3Ek__BackingField_0(),
	SQLiteConnectionString_t3632030467::get_offset_of_U3CDatabasePathU3Ek__BackingField_1(),
	SQLiteConnectionString_t3632030467::get_offset_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (TableAttribute_t3728577771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[1] = 
{
	TableAttribute_t3728577771::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (ColumnAttribute_t3583764011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[1] = 
{
	ColumnAttribute_t3583764011::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (PrimaryKeyAttribute_t1120256698), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (AutoIncrementAttribute_t3792157881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (IndexedAttribute_t384353508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[3] = 
{
	IndexedAttribute_t384353508::get_offset_of_U3CNameU3Ek__BackingField_0(),
	IndexedAttribute_t384353508::get_offset_of_U3COrderU3Ek__BackingField_1(),
	IndexedAttribute_t384353508::get_offset_of_U3CUniqueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (IgnoreAttribute_t1003697248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (UniqueAttribute_t47190802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (MaxLengthAttribute_t2329480580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[1] = 
{
	MaxLengthAttribute_t2329480580::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (CollationAttribute_t3564781786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[1] = 
{
	CollationAttribute_t3564781786::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (NotNullAttribute_t1344045378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (TableMapping_t2276399492), -1, sizeof(TableMapping_t2276399492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3171[14] = 
{
	TableMapping_t2276399492::get_offset_of_U3CMappedTypeU3Ek__BackingField_0(),
	TableMapping_t2276399492::get_offset_of_U3CTableNameU3Ek__BackingField_1(),
	TableMapping_t2276399492::get_offset_of_U3CColumnsU3Ek__BackingField_2(),
	TableMapping_t2276399492::get_offset_of_U3CPKU3Ek__BackingField_3(),
	TableMapping_t2276399492::get_offset_of_U3CGetByPrimaryKeySqlU3Ek__BackingField_4(),
	TableMapping_t2276399492::get_offset_of__autoPk_5(),
	TableMapping_t2276399492::get_offset_of__insertColumns_6(),
	TableMapping_t2276399492::get_offset_of__insertOrReplaceColumns_7(),
	TableMapping_t2276399492::get_offset_of_U3CHasAutoIncPKU3Ek__BackingField_8(),
	TableMapping_t2276399492::get_offset_of__insertCommand_9(),
	TableMapping_t2276399492::get_offset_of__insertCommandExtra_10(),
	TableMapping_t2276399492_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	TableMapping_t2276399492_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
	TableMapping_t2276399492_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (Column_t1357940583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[10] = 
{
	Column_t1357940583::get_offset_of__prop_0(),
	Column_t1357940583::get_offset_of_U3CNameU3Ek__BackingField_1(),
	Column_t1357940583::get_offset_of_U3CColumnTypeU3Ek__BackingField_2(),
	Column_t1357940583::get_offset_of_U3CCollationU3Ek__BackingField_3(),
	Column_t1357940583::get_offset_of_U3CIsAutoIncU3Ek__BackingField_4(),
	Column_t1357940583::get_offset_of_U3CIsAutoGuidU3Ek__BackingField_5(),
	Column_t1357940583::get_offset_of_U3CIsPKU3Ek__BackingField_6(),
	Column_t1357940583::get_offset_of_U3CIndicesU3Ek__BackingField_7(),
	Column_t1357940583::get_offset_of_U3CIsNullableU3Ek__BackingField_8(),
	Column_t1357940583::get_offset_of_U3CMaxStringLengthU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (U3CFindColumnWithPropertyNameU3Ec__AnonStorey0_t3952487827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[1] = 
{
	U3CFindColumnWithPropertyNameU3Ec__AnonStorey0_t3952487827::get_offset_of_propertyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (U3CFindColumnU3Ec__AnonStorey1_t4099938701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[1] = 
{
	U3CFindColumnU3Ec__AnonStorey1_t4099938701::get_offset_of_columnName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (Orm_t3091323797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (SQLiteCommand_t3688707480), -1, sizeof(SQLiteCommand_t3688707480_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3176[4] = 
{
	SQLiteCommand_t3688707480::get_offset_of__conn_0(),
	SQLiteCommand_t3688707480::get_offset_of__bindings_1(),
	SQLiteCommand_t3688707480::get_offset_of_U3CCommandTextU3Ek__BackingField_2(),
	SQLiteCommand_t3688707480_StaticFields::get_offset_of_NegativePointer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (Binding_t2786839072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[3] = 
{
	Binding_t2786839072::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Binding_t2786839072::get_offset_of_U3CValueU3Ek__BackingField_1(),
	Binding_t2786839072::get_offset_of_U3CIndexU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (PreparedSqlLiteInsertCommand_t994658783), -1, sizeof(PreparedSqlLiteInsertCommand_t994658783_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3179[5] = 
{
	PreparedSqlLiteInsertCommand_t994658783::get_offset_of_U3CInitializedU3Ek__BackingField_0(),
	PreparedSqlLiteInsertCommand_t994658783::get_offset_of_U3CConnectionU3Ek__BackingField_1(),
	PreparedSqlLiteInsertCommand_t994658783::get_offset_of_U3CCommandTextU3Ek__BackingField_2(),
	PreparedSqlLiteInsertCommand_t994658783::get_offset_of_U3CStatementU3Ek__BackingField_3(),
	PreparedSqlLiteInsertCommand_t994658783_StaticFields::get_offset_of_NullStatement_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (BaseTableQuery_t440437958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (Ordering_t647619184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[2] = 
{
	Ordering_t647619184::get_offset_of_U3CColumnNameU3Ek__BackingField_0(),
	Ordering_t647619184::get_offset_of_U3CAscendingU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (SQLite3_t985475636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (Result_t1053832893)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3185[32] = 
{
	Result_t1053832893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (ExtendedResult_t3125463630)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3186[46] = 
{
	ExtendedResult_t3125463630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (ConfigOption_t3994509900)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3187[4] = 
{
	ConfigOption_t3994509900::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (ColType_t3265409004)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3188[6] = 
{
	ColType_t3265409004::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (BearingFilter_t336705813)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[2] = 
{
	BearingFilter_t336705813::get_offset_of_Bearing_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BearingFilter_t336705813::get_offset_of_Range_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (Compression_t3624971113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (Constants_t3518929206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (Vector2dBounds_t1974840945)+ sizeof (RuntimeObject), sizeof(Vector2dBounds_t1974840945 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3192[2] = 
{
	Vector2dBounds_t1974840945::get_offset_of_SouthWest_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector2dBounds_t1974840945::get_offset_of_NorthEast_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (BboxToVector2dBoundsConverter_t1118841236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (JsonConverters_t1015645604), -1, sizeof(JsonConverters_t1015645604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3196[1] = 
{
	JsonConverters_t1015645604_StaticFields::get_offset_of_converters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (LonLatToVector2dConverter_t2933574141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (PolylineToVector2dListConverter_t1416161534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (PolylineUtils_t2997409923), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
