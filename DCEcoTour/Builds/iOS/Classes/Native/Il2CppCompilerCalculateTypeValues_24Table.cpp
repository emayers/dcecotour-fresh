﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Xml.Linq.XNode
struct XNode_t716131460;
// System.Xml.Linq.XContainer
struct XContainer_t3387731002;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct List_1_t2329214678;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Xml.Linq.XAttribute
struct XAttribute_t355876829;
// System.Xml.Linq.XElement
struct XElement_t4185968754;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t3995630009;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t2867327688;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t64614563;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t3256600500;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t3111972472;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t3587542510;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t1977848392;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t972960537;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t3277009892;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t2311673543;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t2886331738;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t2950825503;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t955952873;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t3373214253;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t3912835512;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t1475332338;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t2658898854;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t231414508;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t1327982029;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Linq.XNamespace>
struct Dictionary_2_t1628764469;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Linq.XName>
struct Dictionary_2_t3536712297;
// System.Xml.Linq.XNamespace
struct XNamespace_t1843508170;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XAttribute>
struct IEnumerator_1_t788447297;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t322714710;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_t3740801802;
// System.Data.DataSet
struct DataSet_t3773542243;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t3742557897;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t427880856;
// System.Data.TableStructure
struct TableStructure_t4158950525;
// System.Data.TableAdapterSchemaInfo
struct TableAdapterSchemaInfo_t1445784527;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Data.DataTable
struct DataTable_t1734486485;
// System.Data.DataColumn
struct DataColumn_t2485786120;
// System.Xml.XmlWriter
struct XmlWriter_t127905479;
// System.Data.DataTable[]
struct DataTableU5BU5D_t3044315960;
// System.Data.DataRelation[]
struct DataRelationU5BU5D_t2648313166;
// System.Data.PropertyCollection
struct PropertyCollection_t3446977085;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Xml.Linq.XNodeEqualityComparer
struct XNodeEqualityComparer_t35321779;
// System.Xml.Linq.XNodeDocumentOrderComparer
struct XNodeDocumentOrderComparer_t3558329258;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Xml.Linq.XAttribute[]
struct XAttributeU5BU5D_t61111312;
// System.Xml.Linq.XName
struct XName_t3751455998;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t3190347560;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Xml.Linq.XDeclaration
struct XDeclaration_t2907650823;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t3867320123;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Data.TableMappingCollection
struct TableMappingCollection_t719748381;
// System.Data.RelationStructureCollection
struct RelationStructureCollection_t3303280033;
// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement>
struct IEnumerable_1_t3165821643;
// System.Data.DataRowChangeEventArgs
struct DataRowChangeEventArgs_t1619997594;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Data.DataColumnChangeEventArgs
struct DataColumnChangeEventArgs_t353716546;
// System.Data.DataTableNewRowEventArgs
struct DataTableNewRowEventArgs_t4095718124;
// System.Data.MergeFailedEventArgs
struct MergeFailedEventArgs_t717811570;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t521873611;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t3491343620;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t2019268878;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2475741330;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t384203932;




#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef XDECLARATION_T2907650823_H
#define XDECLARATION_T2907650823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XDeclaration
struct  XDeclaration_t2907650823  : public RuntimeObject
{
public:
	// System.String System.Xml.Linq.XDeclaration::encoding
	String_t* ___encoding_0;
	// System.String System.Xml.Linq.XDeclaration::standalone
	String_t* ___standalone_1;
	// System.String System.Xml.Linq.XDeclaration::version
	String_t* ___version_2;

public:
	inline static int32_t get_offset_of_encoding_0() { return static_cast<int32_t>(offsetof(XDeclaration_t2907650823, ___encoding_0)); }
	inline String_t* get_encoding_0() const { return ___encoding_0; }
	inline String_t** get_address_of_encoding_0() { return &___encoding_0; }
	inline void set_encoding_0(String_t* value)
	{
		___encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_0), value);
	}

	inline static int32_t get_offset_of_standalone_1() { return static_cast<int32_t>(offsetof(XDeclaration_t2907650823, ___standalone_1)); }
	inline String_t* get_standalone_1() const { return ___standalone_1; }
	inline String_t** get_address_of_standalone_1() { return &___standalone_1; }
	inline void set_standalone_1(String_t* value)
	{
		___standalone_1 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_1), value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(XDeclaration_t2907650823, ___version_2)); }
	inline String_t* get_version_2() const { return ___version_2; }
	inline String_t** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(String_t* value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDECLARATION_T2907650823_H
#ifndef U3CNODESU3EC__ITERATOR1A_T2001237956_H
#define U3CNODESU3EC__ITERATOR1A_T2001237956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XContainer/<Nodes>c__Iterator1A
struct  U3CNodesU3Ec__Iterator1A_t2001237956  : public RuntimeObject
{
public:
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::<n>__0
	XNode_t716131460 * ___U3CnU3E__0_0;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::<next>__1
	XNode_t716131460 * ___U3CnextU3E__1_1;
	// System.Int32 System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::$PC
	int32_t ___U24PC_2;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::$current
	XNode_t716131460 * ___U24current_3;
	// System.Xml.Linq.XContainer System.Xml.Linq.XContainer/<Nodes>c__Iterator1A::<>f__this
	XContainer_t3387731002 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CnU3E__0_0() { return static_cast<int32_t>(offsetof(U3CNodesU3Ec__Iterator1A_t2001237956, ___U3CnU3E__0_0)); }
	inline XNode_t716131460 * get_U3CnU3E__0_0() const { return ___U3CnU3E__0_0; }
	inline XNode_t716131460 ** get_address_of_U3CnU3E__0_0() { return &___U3CnU3E__0_0; }
	inline void set_U3CnU3E__0_0(XNode_t716131460 * value)
	{
		___U3CnU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CnextU3E__1_1() { return static_cast<int32_t>(offsetof(U3CNodesU3Ec__Iterator1A_t2001237956, ___U3CnextU3E__1_1)); }
	inline XNode_t716131460 * get_U3CnextU3E__1_1() const { return ___U3CnextU3E__1_1; }
	inline XNode_t716131460 ** get_address_of_U3CnextU3E__1_1() { return &___U3CnextU3E__1_1; }
	inline void set_U3CnextU3E__1_1(XNode_t716131460 * value)
	{
		___U3CnextU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnextU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CNodesU3Ec__Iterator1A_t2001237956, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CNodesU3Ec__Iterator1A_t2001237956, ___U24current_3)); }
	inline XNode_t716131460 * get_U24current_3() const { return ___U24current_3; }
	inline XNode_t716131460 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(XNode_t716131460 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CNodesU3Ec__Iterator1A_t2001237956, ___U3CU3Ef__this_4)); }
	inline XContainer_t3387731002 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline XContainer_t3387731002 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(XContainer_t3387731002 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CNODESU3EC__ITERATOR1A_T2001237956_H
#ifndef MOUSESTATE_T384203932_H
#define MOUSESTATE_T384203932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseState
struct  MouseState_t384203932  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState> UnityEngine.EventSystems.PointerInputModule/MouseState::m_TrackedButtons
	List_1_t2329214678 * ___m_TrackedButtons_0;

public:
	inline static int32_t get_offset_of_m_TrackedButtons_0() { return static_cast<int32_t>(offsetof(MouseState_t384203932, ___m_TrackedButtons_0)); }
	inline List_1_t2329214678 * get_m_TrackedButtons_0() const { return ___m_TrackedButtons_0; }
	inline List_1_t2329214678 ** get_address_of_m_TrackedButtons_0() { return &___m_TrackedButtons_0; }
	inline void set_m_TrackedButtons_0(List_1_t2329214678 * value)
	{
		___m_TrackedButtons_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedButtons_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSESTATE_T384203932_H
#ifndef COLLECTIONBASE_T2727926298_H
#define COLLECTIONBASE_T2727926298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t2727926298  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t2718874744 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t2727926298, ___list_0)); }
	inline ArrayList_t2718874744 * get_list_0() const { return ___list_0; }
	inline ArrayList_t2718874744 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t2718874744 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T2727926298_H
#ifndef XNODEEQUALITYCOMPARER_T35321779_H
#define XNODEEQUALITYCOMPARER_T35321779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XNodeEqualityComparer
struct  XNodeEqualityComparer_t35321779  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNODEEQUALITYCOMPARER_T35321779_H
#ifndef U3CATTRIBUTESU3EC__ITERATOR20_T3836567394_H
#define U3CATTRIBUTESU3EC__ITERATOR20_T3836567394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XElement/<Attributes>c__Iterator20
struct  U3CAttributesU3Ec__Iterator20_t3836567394  : public RuntimeObject
{
public:
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement/<Attributes>c__Iterator20::<a>__0
	XAttribute_t355876829 * ___U3CaU3E__0_0;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement/<Attributes>c__Iterator20::<next>__1
	XAttribute_t355876829 * ___U3CnextU3E__1_1;
	// System.Int32 System.Xml.Linq.XElement/<Attributes>c__Iterator20::$PC
	int32_t ___U24PC_2;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement/<Attributes>c__Iterator20::$current
	XAttribute_t355876829 * ___U24current_3;
	// System.Xml.Linq.XElement System.Xml.Linq.XElement/<Attributes>c__Iterator20::<>f__this
	XElement_t4185968754 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CaU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAttributesU3Ec__Iterator20_t3836567394, ___U3CaU3E__0_0)); }
	inline XAttribute_t355876829 * get_U3CaU3E__0_0() const { return ___U3CaU3E__0_0; }
	inline XAttribute_t355876829 ** get_address_of_U3CaU3E__0_0() { return &___U3CaU3E__0_0; }
	inline void set_U3CaU3E__0_0(XAttribute_t355876829 * value)
	{
		___U3CaU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CnextU3E__1_1() { return static_cast<int32_t>(offsetof(U3CAttributesU3Ec__Iterator20_t3836567394, ___U3CnextU3E__1_1)); }
	inline XAttribute_t355876829 * get_U3CnextU3E__1_1() const { return ___U3CnextU3E__1_1; }
	inline XAttribute_t355876829 ** get_address_of_U3CnextU3E__1_1() { return &___U3CnextU3E__1_1; }
	inline void set_U3CnextU3E__1_1(XAttribute_t355876829 * value)
	{
		___U3CnextU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnextU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CAttributesU3Ec__Iterator20_t3836567394, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CAttributesU3Ec__Iterator20_t3836567394, ___U24current_3)); }
	inline XAttribute_t355876829 * get_U24current_3() const { return ___U24current_3; }
	inline XAttribute_t355876829 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(XAttribute_t355876829 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CAttributesU3Ec__Iterator20_t3836567394, ___U3CU3Ef__this_4)); }
	inline XElement_t4185968754 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline XElement_t4185968754 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(XElement_t4185968754 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CATTRIBUTESU3EC__ITERATOR20_T3836567394_H
#ifndef U3CEXPANDARRAYU3EC__ITERATOR25_T2040603384_H
#define U3CEXPANDARRAYU3EC__ITERATOR25_T2040603384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25
struct  U3CExpandArrayU3Ec__Iterator25_t2040603384  : public RuntimeObject
{
public:
	// System.Object System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::o
	RuntimeObject * ___o_0;
	// System.Xml.Linq.XNode System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::<n>__0
	XNode_t716131460 * ___U3CnU3E__0_1;
	// System.Collections.IEnumerator System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::<$s_86>__1
	RuntimeObject* ___U3CU24s_86U3E__1_2;
	// System.Object System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::<obj>__2
	RuntimeObject * ___U3CobjU3E__2_3;
	// System.Collections.IEnumerator System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::<$s_87>__3
	RuntimeObject* ___U3CU24s_87U3E__3_4;
	// System.Object System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::<oo>__4
	RuntimeObject * ___U3CooU3E__4_5;
	// System.Int32 System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::$PC
	int32_t ___U24PC_6;
	// System.Object System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::$current
	RuntimeObject * ___U24current_7;
	// System.Object System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::<$>o
	RuntimeObject * ___U3CU24U3Eo_8;

public:
	inline static int32_t get_offset_of_o_0() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___o_0)); }
	inline RuntimeObject * get_o_0() const { return ___o_0; }
	inline RuntimeObject ** get_address_of_o_0() { return &___o_0; }
	inline void set_o_0(RuntimeObject * value)
	{
		___o_0 = value;
		Il2CppCodeGenWriteBarrier((&___o_0), value);
	}

	inline static int32_t get_offset_of_U3CnU3E__0_1() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U3CnU3E__0_1)); }
	inline XNode_t716131460 * get_U3CnU3E__0_1() const { return ___U3CnU3E__0_1; }
	inline XNode_t716131460 ** get_address_of_U3CnU3E__0_1() { return &___U3CnU3E__0_1; }
	inline void set_U3CnU3E__0_1(XNode_t716131460 * value)
	{
		___U3CnU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CU24s_86U3E__1_2() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U3CU24s_86U3E__1_2)); }
	inline RuntimeObject* get_U3CU24s_86U3E__1_2() const { return ___U3CU24s_86U3E__1_2; }
	inline RuntimeObject** get_address_of_U3CU24s_86U3E__1_2() { return &___U3CU24s_86U3E__1_2; }
	inline void set_U3CU24s_86U3E__1_2(RuntimeObject* value)
	{
		___U3CU24s_86U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_86U3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_3() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U3CobjU3E__2_3)); }
	inline RuntimeObject * get_U3CobjU3E__2_3() const { return ___U3CobjU3E__2_3; }
	inline RuntimeObject ** get_address_of_U3CobjU3E__2_3() { return &___U3CobjU3E__2_3; }
	inline void set_U3CobjU3E__2_3(RuntimeObject * value)
	{
		___U3CobjU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U3CU24s_87U3E__3_4() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U3CU24s_87U3E__3_4)); }
	inline RuntimeObject* get_U3CU24s_87U3E__3_4() const { return ___U3CU24s_87U3E__3_4; }
	inline RuntimeObject** get_address_of_U3CU24s_87U3E__3_4() { return &___U3CU24s_87U3E__3_4; }
	inline void set_U3CU24s_87U3E__3_4(RuntimeObject* value)
	{
		___U3CU24s_87U3E__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_87U3E__3_4), value);
	}

	inline static int32_t get_offset_of_U3CooU3E__4_5() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U3CooU3E__4_5)); }
	inline RuntimeObject * get_U3CooU3E__4_5() const { return ___U3CooU3E__4_5; }
	inline RuntimeObject ** get_address_of_U3CooU3E__4_5() { return &___U3CooU3E__4_5; }
	inline void set_U3CooU3E__4_5(RuntimeObject * value)
	{
		___U3CooU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CooU3E__4_5), value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eo_8() { return static_cast<int32_t>(offsetof(U3CExpandArrayU3Ec__Iterator25_t2040603384, ___U3CU24U3Eo_8)); }
	inline RuntimeObject * get_U3CU24U3Eo_8() const { return ___U3CU24U3Eo_8; }
	inline RuntimeObject ** get_address_of_U3CU24U3Eo_8() { return &___U3CU24U3Eo_8; }
	inline void set_U3CU24U3Eo_8(RuntimeObject * value)
	{
		___U3CU24U3Eo_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3Eo_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXPANDARRAYU3EC__ITERATOR25_T2040603384_H
#ifndef XUTIL_T154644382_H
#define XUTIL_T154644382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XUtil
struct  XUtil_t154644382  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XUTIL_T154644382_H
#ifndef EXECUTEEVENTS_T3484638744_H
#define EXECUTEEVENTS_T3484638744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_t3484638744  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_t3484638744_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_t3995630009 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t2867327688 * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t64614563 * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_t3256600500 * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t3111972472 * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_t3587542510 * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t1977848392 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t972960537 * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_t3277009892 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t2311673543 * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_t2886331738 * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_t2950825503 * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_t955952873 * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_t3373214253 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_t3912835512 * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_t1475332338 * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_t2658898854 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_t231414508 * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_t777473367 * ___s_InternalTransformList_18;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache0
	EventFunction_1_t3995630009 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache1
	EventFunction_1_t2867327688 * ___U3CU3Ef__mgU24cache1_20;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache2
	EventFunction_1_t64614563 * ___U3CU3Ef__mgU24cache2_21;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache3
	EventFunction_1_t3256600500 * ___U3CU3Ef__mgU24cache3_22;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache4
	EventFunction_1_t3111972472 * ___U3CU3Ef__mgU24cache4_23;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache5
	EventFunction_1_t3587542510 * ___U3CU3Ef__mgU24cache5_24;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache6
	EventFunction_1_t1977848392 * ___U3CU3Ef__mgU24cache6_25;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache7
	EventFunction_1_t972960537 * ___U3CU3Ef__mgU24cache7_26;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache8
	EventFunction_1_t3277009892 * ___U3CU3Ef__mgU24cache8_27;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache9
	EventFunction_1_t2311673543 * ___U3CU3Ef__mgU24cache9_28;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheA
	EventFunction_1_t2886331738 * ___U3CU3Ef__mgU24cacheA_29;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheB
	EventFunction_1_t2950825503 * ___U3CU3Ef__mgU24cacheB_30;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheC
	EventFunction_1_t955952873 * ___U3CU3Ef__mgU24cacheC_31;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheD
	EventFunction_1_t3373214253 * ___U3CU3Ef__mgU24cacheD_32;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheE
	EventFunction_1_t3912835512 * ___U3CU3Ef__mgU24cacheE_33;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheF
	EventFunction_1_t1475332338 * ___U3CU3Ef__mgU24cacheF_34;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache10
	EventFunction_1_t2658898854 * ___U3CU3Ef__mgU24cache10_35;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_t3995630009 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_t3995630009 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_t3995630009 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerEnterHandler_0), value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t2867327688 * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t2867327688 ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t2867327688 * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerExitHandler_1), value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t64614563 * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t64614563 ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t64614563 * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerDownHandler_2), value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_t3256600500 * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_t3256600500 ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_t3256600500 * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerUpHandler_3), value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t3111972472 * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t3111972472 ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t3111972472 * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerClickHandler_4), value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_t3587542510 * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_t3587542510 ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_t3587542510 * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_InitializePotentialDragHandler_5), value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t1977848392 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t1977848392 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t1977848392 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_BeginDragHandler_6), value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t972960537 * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t972960537 ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t972960537 * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_DragHandler_7), value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_t3277009892 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_t3277009892 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_t3277009892 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EndDragHandler_8), value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t2311673543 * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t2311673543 ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t2311673543 * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_DropHandler_9), value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_t2886331738 * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_t2886331738 ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_t2886331738 * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ScrollHandler_10), value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_t2950825503 * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_t2950825503 ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_t2950825503 * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateSelectedHandler_11), value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_t955952873 * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_t955952873 ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_t955952873 * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_SelectHandler_12), value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_t3373214253 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_t3373214253 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_t3373214253 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_DeselectHandler_13), value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_t3912835512 * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_t3912835512 ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_t3912835512 * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_MoveHandler_14), value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_t1475332338 * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_t1475332338 ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_t1475332338 * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SubmitHandler_15), value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_t2658898854 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_t2658898854 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_t2658898854 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelHandler_16), value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_t231414508 * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_t231414508 ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_t231414508 * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_HandlerListPool_17), value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_t777473367 * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_t777473367 ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_t777473367 * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalTransformList_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline EventFunction_1_t3995630009 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline EventFunction_1_t3995630009 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(EventFunction_1_t3995630009 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline EventFunction_1_t2867327688 * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline EventFunction_1_t2867327688 ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(EventFunction_1_t2867327688 * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_21() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache2_21)); }
	inline EventFunction_1_t64614563 * get_U3CU3Ef__mgU24cache2_21() const { return ___U3CU3Ef__mgU24cache2_21; }
	inline EventFunction_1_t64614563 ** get_address_of_U3CU3Ef__mgU24cache2_21() { return &___U3CU3Ef__mgU24cache2_21; }
	inline void set_U3CU3Ef__mgU24cache2_21(EventFunction_1_t64614563 * value)
	{
		___U3CU3Ef__mgU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_22() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache3_22)); }
	inline EventFunction_1_t3256600500 * get_U3CU3Ef__mgU24cache3_22() const { return ___U3CU3Ef__mgU24cache3_22; }
	inline EventFunction_1_t3256600500 ** get_address_of_U3CU3Ef__mgU24cache3_22() { return &___U3CU3Ef__mgU24cache3_22; }
	inline void set_U3CU3Ef__mgU24cache3_22(EventFunction_1_t3256600500 * value)
	{
		___U3CU3Ef__mgU24cache3_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_23() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache4_23)); }
	inline EventFunction_1_t3111972472 * get_U3CU3Ef__mgU24cache4_23() const { return ___U3CU3Ef__mgU24cache4_23; }
	inline EventFunction_1_t3111972472 ** get_address_of_U3CU3Ef__mgU24cache4_23() { return &___U3CU3Ef__mgU24cache4_23; }
	inline void set_U3CU3Ef__mgU24cache4_23(EventFunction_1_t3111972472 * value)
	{
		___U3CU3Ef__mgU24cache4_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_24() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache5_24)); }
	inline EventFunction_1_t3587542510 * get_U3CU3Ef__mgU24cache5_24() const { return ___U3CU3Ef__mgU24cache5_24; }
	inline EventFunction_1_t3587542510 ** get_address_of_U3CU3Ef__mgU24cache5_24() { return &___U3CU3Ef__mgU24cache5_24; }
	inline void set_U3CU3Ef__mgU24cache5_24(EventFunction_1_t3587542510 * value)
	{
		___U3CU3Ef__mgU24cache5_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_25() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache6_25)); }
	inline EventFunction_1_t1977848392 * get_U3CU3Ef__mgU24cache6_25() const { return ___U3CU3Ef__mgU24cache6_25; }
	inline EventFunction_1_t1977848392 ** get_address_of_U3CU3Ef__mgU24cache6_25() { return &___U3CU3Ef__mgU24cache6_25; }
	inline void set_U3CU3Ef__mgU24cache6_25(EventFunction_1_t1977848392 * value)
	{
		___U3CU3Ef__mgU24cache6_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_26() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache7_26)); }
	inline EventFunction_1_t972960537 * get_U3CU3Ef__mgU24cache7_26() const { return ___U3CU3Ef__mgU24cache7_26; }
	inline EventFunction_1_t972960537 ** get_address_of_U3CU3Ef__mgU24cache7_26() { return &___U3CU3Ef__mgU24cache7_26; }
	inline void set_U3CU3Ef__mgU24cache7_26(EventFunction_1_t972960537 * value)
	{
		___U3CU3Ef__mgU24cache7_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_27() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache8_27)); }
	inline EventFunction_1_t3277009892 * get_U3CU3Ef__mgU24cache8_27() const { return ___U3CU3Ef__mgU24cache8_27; }
	inline EventFunction_1_t3277009892 ** get_address_of_U3CU3Ef__mgU24cache8_27() { return &___U3CU3Ef__mgU24cache8_27; }
	inline void set_U3CU3Ef__mgU24cache8_27(EventFunction_1_t3277009892 * value)
	{
		___U3CU3Ef__mgU24cache8_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_28() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache9_28)); }
	inline EventFunction_1_t2311673543 * get_U3CU3Ef__mgU24cache9_28() const { return ___U3CU3Ef__mgU24cache9_28; }
	inline EventFunction_1_t2311673543 ** get_address_of_U3CU3Ef__mgU24cache9_28() { return &___U3CU3Ef__mgU24cache9_28; }
	inline void set_U3CU3Ef__mgU24cache9_28(EventFunction_1_t2311673543 * value)
	{
		___U3CU3Ef__mgU24cache9_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_29() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheA_29)); }
	inline EventFunction_1_t2886331738 * get_U3CU3Ef__mgU24cacheA_29() const { return ___U3CU3Ef__mgU24cacheA_29; }
	inline EventFunction_1_t2886331738 ** get_address_of_U3CU3Ef__mgU24cacheA_29() { return &___U3CU3Ef__mgU24cacheA_29; }
	inline void set_U3CU3Ef__mgU24cacheA_29(EventFunction_1_t2886331738 * value)
	{
		___U3CU3Ef__mgU24cacheA_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_30() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheB_30)); }
	inline EventFunction_1_t2950825503 * get_U3CU3Ef__mgU24cacheB_30() const { return ___U3CU3Ef__mgU24cacheB_30; }
	inline EventFunction_1_t2950825503 ** get_address_of_U3CU3Ef__mgU24cacheB_30() { return &___U3CU3Ef__mgU24cacheB_30; }
	inline void set_U3CU3Ef__mgU24cacheB_30(EventFunction_1_t2950825503 * value)
	{
		___U3CU3Ef__mgU24cacheB_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_31() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheC_31)); }
	inline EventFunction_1_t955952873 * get_U3CU3Ef__mgU24cacheC_31() const { return ___U3CU3Ef__mgU24cacheC_31; }
	inline EventFunction_1_t955952873 ** get_address_of_U3CU3Ef__mgU24cacheC_31() { return &___U3CU3Ef__mgU24cacheC_31; }
	inline void set_U3CU3Ef__mgU24cacheC_31(EventFunction_1_t955952873 * value)
	{
		___U3CU3Ef__mgU24cacheC_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_32() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheD_32)); }
	inline EventFunction_1_t3373214253 * get_U3CU3Ef__mgU24cacheD_32() const { return ___U3CU3Ef__mgU24cacheD_32; }
	inline EventFunction_1_t3373214253 ** get_address_of_U3CU3Ef__mgU24cacheD_32() { return &___U3CU3Ef__mgU24cacheD_32; }
	inline void set_U3CU3Ef__mgU24cacheD_32(EventFunction_1_t3373214253 * value)
	{
		___U3CU3Ef__mgU24cacheD_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_33() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheE_33)); }
	inline EventFunction_1_t3912835512 * get_U3CU3Ef__mgU24cacheE_33() const { return ___U3CU3Ef__mgU24cacheE_33; }
	inline EventFunction_1_t3912835512 ** get_address_of_U3CU3Ef__mgU24cacheE_33() { return &___U3CU3Ef__mgU24cacheE_33; }
	inline void set_U3CU3Ef__mgU24cacheE_33(EventFunction_1_t3912835512 * value)
	{
		___U3CU3Ef__mgU24cacheE_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_34() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheF_34)); }
	inline EventFunction_1_t1475332338 * get_U3CU3Ef__mgU24cacheF_34() const { return ___U3CU3Ef__mgU24cacheF_34; }
	inline EventFunction_1_t1475332338 ** get_address_of_U3CU3Ef__mgU24cacheF_34() { return &___U3CU3Ef__mgU24cacheF_34; }
	inline void set_U3CU3Ef__mgU24cacheF_34(EventFunction_1_t1475332338 * value)
	{
		___U3CU3Ef__mgU24cacheF_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_35() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache10_35)); }
	inline EventFunction_1_t2658898854 * get_U3CU3Ef__mgU24cache10_35() const { return ___U3CU3Ef__mgU24cache10_35; }
	inline EventFunction_1_t2658898854 ** get_address_of_U3CU3Ef__mgU24cache10_35() { return &___U3CU3Ef__mgU24cache10_35; }
	inline void set_U3CU3Ef__mgU24cache10_35(EventFunction_1_t2658898854 * value)
	{
		___U3CU3Ef__mgU24cache10_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEEVENTS_T3484638744_H
#ifndef XOBJECT_T1119084474_H
#define XOBJECT_T1119084474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XObject
struct  XObject_t1119084474  : public RuntimeObject
{
public:
	// System.Xml.Linq.XContainer System.Xml.Linq.XObject::owner
	XContainer_t3387731002 * ___owner_0;
	// System.String System.Xml.Linq.XObject::baseuri
	String_t* ___baseuri_1;
	// System.Int32 System.Xml.Linq.XObject::line
	int32_t ___line_2;
	// System.Int32 System.Xml.Linq.XObject::column
	int32_t ___column_3;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(XObject_t1119084474, ___owner_0)); }
	inline XContainer_t3387731002 * get_owner_0() const { return ___owner_0; }
	inline XContainer_t3387731002 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(XContainer_t3387731002 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier((&___owner_0), value);
	}

	inline static int32_t get_offset_of_baseuri_1() { return static_cast<int32_t>(offsetof(XObject_t1119084474, ___baseuri_1)); }
	inline String_t* get_baseuri_1() const { return ___baseuri_1; }
	inline String_t** get_address_of_baseuri_1() { return &___baseuri_1; }
	inline void set_baseuri_1(String_t* value)
	{
		___baseuri_1 = value;
		Il2CppCodeGenWriteBarrier((&___baseuri_1), value);
	}

	inline static int32_t get_offset_of_line_2() { return static_cast<int32_t>(offsetof(XObject_t1119084474, ___line_2)); }
	inline int32_t get_line_2() const { return ___line_2; }
	inline int32_t* get_address_of_line_2() { return &___line_2; }
	inline void set_line_2(int32_t value)
	{
		___line_2 = value;
	}

	inline static int32_t get_offset_of_column_3() { return static_cast<int32_t>(offsetof(XObject_t1119084474, ___column_3)); }
	inline int32_t get_column_3() const { return ___column_3; }
	inline int32_t* get_address_of_column_3() { return &___column_3; }
	inline void set_column_3(int32_t value)
	{
		___column_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECT_T1119084474_H
#ifndef RAYCASTERMANAGER_T2536340562_H
#define RAYCASTERMANAGER_T2536340562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t2536340562  : public RuntimeObject
{
public:

public:
};

struct RaycasterManager_t2536340562_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t1327982029 * ___s_Raycasters_0;

public:
	inline static int32_t get_offset_of_s_Raycasters_0() { return static_cast<int32_t>(offsetof(RaycasterManager_t2536340562_StaticFields, ___s_Raycasters_0)); }
	inline List_1_t1327982029 * get_s_Raycasters_0() const { return ___s_Raycasters_0; }
	inline List_1_t1327982029 ** get_address_of_s_Raycasters_0() { return &___s_Raycasters_0; }
	inline void set_s_Raycasters_0(List_1_t1327982029 * value)
	{
		___s_Raycasters_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Raycasters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTERMANAGER_T2536340562_H
#ifndef XNODEDOCUMENTORDERCOMPARER_T3558329258_H
#define XNODEDOCUMENTORDERCOMPARER_T3558329258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XNodeDocumentOrderComparer
struct  XNodeDocumentOrderComparer_t3558329258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNODEDOCUMENTORDERCOMPARER_T3558329258_H
#ifndef XNAMESPACE_T1843508170_H
#define XNAMESPACE_T1843508170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XNamespace
struct  XNamespace_t1843508170  : public RuntimeObject
{
public:
	// System.String System.Xml.Linq.XNamespace::uri
	String_t* ___uri_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Linq.XName> System.Xml.Linq.XNamespace::table
	Dictionary_2_t3536712297 * ___table_5;

public:
	inline static int32_t get_offset_of_uri_4() { return static_cast<int32_t>(offsetof(XNamespace_t1843508170, ___uri_4)); }
	inline String_t* get_uri_4() const { return ___uri_4; }
	inline String_t** get_address_of_uri_4() { return &___uri_4; }
	inline void set_uri_4(String_t* value)
	{
		___uri_4 = value;
		Il2CppCodeGenWriteBarrier((&___uri_4), value);
	}

	inline static int32_t get_offset_of_table_5() { return static_cast<int32_t>(offsetof(XNamespace_t1843508170, ___table_5)); }
	inline Dictionary_2_t3536712297 * get_table_5() const { return ___table_5; }
	inline Dictionary_2_t3536712297 ** get_address_of_table_5() { return &___table_5; }
	inline void set_table_5(Dictionary_2_t3536712297 * value)
	{
		___table_5 = value;
		Il2CppCodeGenWriteBarrier((&___table_5), value);
	}
};

struct XNamespace_t1843508170_StaticFields
{
public:
	// System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::blank
	XNamespace_t1843508170 * ___blank_0;
	// System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::xml
	XNamespace_t1843508170 * ___xml_1;
	// System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::xmlns
	XNamespace_t1843508170 * ___xmlns_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Linq.XNamespace> System.Xml.Linq.XNamespace::nstable
	Dictionary_2_t1628764469 * ___nstable_3;

public:
	inline static int32_t get_offset_of_blank_0() { return static_cast<int32_t>(offsetof(XNamespace_t1843508170_StaticFields, ___blank_0)); }
	inline XNamespace_t1843508170 * get_blank_0() const { return ___blank_0; }
	inline XNamespace_t1843508170 ** get_address_of_blank_0() { return &___blank_0; }
	inline void set_blank_0(XNamespace_t1843508170 * value)
	{
		___blank_0 = value;
		Il2CppCodeGenWriteBarrier((&___blank_0), value);
	}

	inline static int32_t get_offset_of_xml_1() { return static_cast<int32_t>(offsetof(XNamespace_t1843508170_StaticFields, ___xml_1)); }
	inline XNamespace_t1843508170 * get_xml_1() const { return ___xml_1; }
	inline XNamespace_t1843508170 ** get_address_of_xml_1() { return &___xml_1; }
	inline void set_xml_1(XNamespace_t1843508170 * value)
	{
		___xml_1 = value;
		Il2CppCodeGenWriteBarrier((&___xml_1), value);
	}

	inline static int32_t get_offset_of_xmlns_2() { return static_cast<int32_t>(offsetof(XNamespace_t1843508170_StaticFields, ___xmlns_2)); }
	inline XNamespace_t1843508170 * get_xmlns_2() const { return ___xmlns_2; }
	inline XNamespace_t1843508170 ** get_address_of_xmlns_2() { return &___xmlns_2; }
	inline void set_xmlns_2(XNamespace_t1843508170 * value)
	{
		___xmlns_2 = value;
		Il2CppCodeGenWriteBarrier((&___xmlns_2), value);
	}

	inline static int32_t get_offset_of_nstable_3() { return static_cast<int32_t>(offsetof(XNamespace_t1843508170_StaticFields, ___nstable_3)); }
	inline Dictionary_2_t1628764469 * get_nstable_3() const { return ___nstable_3; }
	inline Dictionary_2_t1628764469 ** get_address_of_nstable_3() { return &___nstable_3; }
	inline void set_nstable_3(Dictionary_2_t1628764469 * value)
	{
		___nstable_3 = value;
		Il2CppCodeGenWriteBarrier((&___nstable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNAMESPACE_T1843508170_H
#ifndef XNAME_T3751455998_H
#define XNAME_T3751455998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XName
struct  XName_t3751455998  : public RuntimeObject
{
public:
	// System.String System.Xml.Linq.XName::local
	String_t* ___local_0;
	// System.Xml.Linq.XNamespace System.Xml.Linq.XName::ns
	XNamespace_t1843508170 * ___ns_1;

public:
	inline static int32_t get_offset_of_local_0() { return static_cast<int32_t>(offsetof(XName_t3751455998, ___local_0)); }
	inline String_t* get_local_0() const { return ___local_0; }
	inline String_t** get_address_of_local_0() { return &___local_0; }
	inline void set_local_0(String_t* value)
	{
		___local_0 = value;
		Il2CppCodeGenWriteBarrier((&___local_0), value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(XName_t3751455998, ___ns_1)); }
	inline XNamespace_t1843508170 * get_ns_1() const { return ___ns_1; }
	inline XNamespace_t1843508170 ** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(XNamespace_t1843508170 * value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNAME_T3751455998_H
#ifndef U3CGETPREFIXOFNAMESPACECOREU3EC__ITERATOR23_T674090361_H
#define U3CGETPREFIXOFNAMESPACECOREU3EC__ITERATOR23_T674090361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23
struct  U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361  : public RuntimeObject
{
public:
	// System.Xml.Linq.XElement System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::<el>__0
	XElement_t4185968754 * ___U3CelU3E__0_0;
	// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XAttribute> System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::<$s_68>__1
	RuntimeObject* ___U3CU24s_68U3E__1_1;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::<a>__2
	XAttribute_t355876829 * ___U3CaU3E__2_2;
	// System.Xml.Linq.XNamespace System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::ns
	XNamespace_t1843508170 * ___ns_3;
	// System.Int32 System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::$PC
	int32_t ___U24PC_4;
	// System.String System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::$current
	String_t* ___U24current_5;
	// System.Xml.Linq.XNamespace System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::<$>ns
	XNamespace_t1843508170 * ___U3CU24U3Ens_6;
	// System.Xml.Linq.XElement System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator23::<>f__this
	XElement_t4185968754 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CelU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___U3CelU3E__0_0)); }
	inline XElement_t4185968754 * get_U3CelU3E__0_0() const { return ___U3CelU3E__0_0; }
	inline XElement_t4185968754 ** get_address_of_U3CelU3E__0_0() { return &___U3CelU3E__0_0; }
	inline void set_U3CelU3E__0_0(XElement_t4185968754 * value)
	{
		___U3CelU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CelU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CU24s_68U3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___U3CU24s_68U3E__1_1)); }
	inline RuntimeObject* get_U3CU24s_68U3E__1_1() const { return ___U3CU24s_68U3E__1_1; }
	inline RuntimeObject** get_address_of_U3CU24s_68U3E__1_1() { return &___U3CU24s_68U3E__1_1; }
	inline void set_U3CU24s_68U3E__1_1(RuntimeObject* value)
	{
		___U3CU24s_68U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_68U3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CaU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___U3CaU3E__2_2)); }
	inline XAttribute_t355876829 * get_U3CaU3E__2_2() const { return ___U3CaU3E__2_2; }
	inline XAttribute_t355876829 ** get_address_of_U3CaU3E__2_2() { return &___U3CaU3E__2_2; }
	inline void set_U3CaU3E__2_2(XAttribute_t355876829 * value)
	{
		___U3CaU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaU3E__2_2), value);
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___ns_3)); }
	inline XNamespace_t1843508170 * get_ns_3() const { return ___ns_3; }
	inline XNamespace_t1843508170 ** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(XNamespace_t1843508170 * value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((&___ns_3), value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___U24current_5)); }
	inline String_t* get_U24current_5() const { return ___U24current_5; }
	inline String_t** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(String_t* value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ens_6() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___U3CU24U3Ens_6)); }
	inline XNamespace_t1843508170 * get_U3CU24U3Ens_6() const { return ___U3CU24U3Ens_6; }
	inline XNamespace_t1843508170 ** get_address_of_U3CU24U3Ens_6() { return &___U3CU24U3Ens_6; }
	inline void set_U3CU24U3Ens_6(XNamespace_t1843508170 * value)
	{
		___U3CU24U3Ens_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3Ens_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361, ___U3CU3Ef__this_7)); }
	inline XElement_t4185968754 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline XElement_t4185968754 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(XElement_t4185968754 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPREFIXOFNAMESPACECOREU3EC__ITERATOR23_T674090361_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CONSTRAINTSTRUCTURE_T3230168582_H
#define CONSTRAINTSTRUCTURE_T3230168582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintStructure
struct  ConstraintStructure_t3230168582  : public RuntimeObject
{
public:
	// System.String System.Data.ConstraintStructure::TableName
	String_t* ___TableName_0;
	// System.String[] System.Data.ConstraintStructure::Columns
	StringU5BU5D_t1281789340* ___Columns_1;
	// System.Boolean[] System.Data.ConstraintStructure::IsAttribute
	BooleanU5BU5D_t2897418192* ___IsAttribute_2;
	// System.String System.Data.ConstraintStructure::ConstraintName
	String_t* ___ConstraintName_3;
	// System.Boolean System.Data.ConstraintStructure::IsPrimaryKey
	bool ___IsPrimaryKey_4;
	// System.String System.Data.ConstraintStructure::ReferName
	String_t* ___ReferName_5;
	// System.Boolean System.Data.ConstraintStructure::IsNested
	bool ___IsNested_6;
	// System.Boolean System.Data.ConstraintStructure::IsConstraintOnly
	bool ___IsConstraintOnly_7;

public:
	inline static int32_t get_offset_of_TableName_0() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___TableName_0)); }
	inline String_t* get_TableName_0() const { return ___TableName_0; }
	inline String_t** get_address_of_TableName_0() { return &___TableName_0; }
	inline void set_TableName_0(String_t* value)
	{
		___TableName_0 = value;
		Il2CppCodeGenWriteBarrier((&___TableName_0), value);
	}

	inline static int32_t get_offset_of_Columns_1() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___Columns_1)); }
	inline StringU5BU5D_t1281789340* get_Columns_1() const { return ___Columns_1; }
	inline StringU5BU5D_t1281789340** get_address_of_Columns_1() { return &___Columns_1; }
	inline void set_Columns_1(StringU5BU5D_t1281789340* value)
	{
		___Columns_1 = value;
		Il2CppCodeGenWriteBarrier((&___Columns_1), value);
	}

	inline static int32_t get_offset_of_IsAttribute_2() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___IsAttribute_2)); }
	inline BooleanU5BU5D_t2897418192* get_IsAttribute_2() const { return ___IsAttribute_2; }
	inline BooleanU5BU5D_t2897418192** get_address_of_IsAttribute_2() { return &___IsAttribute_2; }
	inline void set_IsAttribute_2(BooleanU5BU5D_t2897418192* value)
	{
		___IsAttribute_2 = value;
		Il2CppCodeGenWriteBarrier((&___IsAttribute_2), value);
	}

	inline static int32_t get_offset_of_ConstraintName_3() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___ConstraintName_3)); }
	inline String_t* get_ConstraintName_3() const { return ___ConstraintName_3; }
	inline String_t** get_address_of_ConstraintName_3() { return &___ConstraintName_3; }
	inline void set_ConstraintName_3(String_t* value)
	{
		___ConstraintName_3 = value;
		Il2CppCodeGenWriteBarrier((&___ConstraintName_3), value);
	}

	inline static int32_t get_offset_of_IsPrimaryKey_4() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___IsPrimaryKey_4)); }
	inline bool get_IsPrimaryKey_4() const { return ___IsPrimaryKey_4; }
	inline bool* get_address_of_IsPrimaryKey_4() { return &___IsPrimaryKey_4; }
	inline void set_IsPrimaryKey_4(bool value)
	{
		___IsPrimaryKey_4 = value;
	}

	inline static int32_t get_offset_of_ReferName_5() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___ReferName_5)); }
	inline String_t* get_ReferName_5() const { return ___ReferName_5; }
	inline String_t** get_address_of_ReferName_5() { return &___ReferName_5; }
	inline void set_ReferName_5(String_t* value)
	{
		___ReferName_5 = value;
		Il2CppCodeGenWriteBarrier((&___ReferName_5), value);
	}

	inline static int32_t get_offset_of_IsNested_6() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___IsNested_6)); }
	inline bool get_IsNested_6() const { return ___IsNested_6; }
	inline bool* get_address_of_IsNested_6() { return &___IsNested_6; }
	inline void set_IsNested_6(bool value)
	{
		___IsNested_6 = value;
	}

	inline static int32_t get_offset_of_IsConstraintOnly_7() { return static_cast<int32_t>(offsetof(ConstraintStructure_t3230168582, ___IsConstraintOnly_7)); }
	inline bool get_IsConstraintOnly_7() const { return ___IsConstraintOnly_7; }
	inline bool* get_address_of_IsConstraintOnly_7() { return &___IsConstraintOnly_7; }
	inline void set_IsConstraintOnly_7(bool value)
	{
		___IsConstraintOnly_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTSTRUCTURE_T3230168582_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef XMLSCHEMADATAIMPORTER_T2556819020_H
#define XMLSCHEMADATAIMPORTER_T2556819020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlSchemaDataImporter
struct  XmlSchemaDataImporter_t2556819020  : public RuntimeObject
{
public:
	// System.Data.DataSet System.Data.XmlSchemaDataImporter::dataset
	DataSet_t3773542243 * ___dataset_3;
	// System.Boolean System.Data.XmlSchemaDataImporter::forDataSet
	bool ___forDataSet_4;
	// System.Xml.Schema.XmlSchema System.Data.XmlSchemaDataImporter::schema
	XmlSchema_t3742557897 * ___schema_5;
	// System.Collections.ArrayList System.Data.XmlSchemaDataImporter::relations
	ArrayList_t2718874744 * ___relations_6;
	// System.Collections.Hashtable System.Data.XmlSchemaDataImporter::reservedConstraints
	Hashtable_t1853889766 * ___reservedConstraints_7;
	// System.Xml.Schema.XmlSchemaElement System.Data.XmlSchemaDataImporter::datasetElement
	XmlSchemaElement_t427880856 * ___datasetElement_8;
	// System.Collections.ArrayList System.Data.XmlSchemaDataImporter::topLevelElements
	ArrayList_t2718874744 * ___topLevelElements_9;
	// System.Collections.ArrayList System.Data.XmlSchemaDataImporter::targetElements
	ArrayList_t2718874744 * ___targetElements_10;
	// System.Data.TableStructure System.Data.XmlSchemaDataImporter::currentTable
	TableStructure_t4158950525 * ___currentTable_11;
	// System.Data.TableAdapterSchemaInfo System.Data.XmlSchemaDataImporter::currentAdapter
	TableAdapterSchemaInfo_t1445784527 * ___currentAdapter_12;

public:
	inline static int32_t get_offset_of_dataset_3() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___dataset_3)); }
	inline DataSet_t3773542243 * get_dataset_3() const { return ___dataset_3; }
	inline DataSet_t3773542243 ** get_address_of_dataset_3() { return &___dataset_3; }
	inline void set_dataset_3(DataSet_t3773542243 * value)
	{
		___dataset_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataset_3), value);
	}

	inline static int32_t get_offset_of_forDataSet_4() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___forDataSet_4)); }
	inline bool get_forDataSet_4() const { return ___forDataSet_4; }
	inline bool* get_address_of_forDataSet_4() { return &___forDataSet_4; }
	inline void set_forDataSet_4(bool value)
	{
		___forDataSet_4 = value;
	}

	inline static int32_t get_offset_of_schema_5() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___schema_5)); }
	inline XmlSchema_t3742557897 * get_schema_5() const { return ___schema_5; }
	inline XmlSchema_t3742557897 ** get_address_of_schema_5() { return &___schema_5; }
	inline void set_schema_5(XmlSchema_t3742557897 * value)
	{
		___schema_5 = value;
		Il2CppCodeGenWriteBarrier((&___schema_5), value);
	}

	inline static int32_t get_offset_of_relations_6() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___relations_6)); }
	inline ArrayList_t2718874744 * get_relations_6() const { return ___relations_6; }
	inline ArrayList_t2718874744 ** get_address_of_relations_6() { return &___relations_6; }
	inline void set_relations_6(ArrayList_t2718874744 * value)
	{
		___relations_6 = value;
		Il2CppCodeGenWriteBarrier((&___relations_6), value);
	}

	inline static int32_t get_offset_of_reservedConstraints_7() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___reservedConstraints_7)); }
	inline Hashtable_t1853889766 * get_reservedConstraints_7() const { return ___reservedConstraints_7; }
	inline Hashtable_t1853889766 ** get_address_of_reservedConstraints_7() { return &___reservedConstraints_7; }
	inline void set_reservedConstraints_7(Hashtable_t1853889766 * value)
	{
		___reservedConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___reservedConstraints_7), value);
	}

	inline static int32_t get_offset_of_datasetElement_8() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___datasetElement_8)); }
	inline XmlSchemaElement_t427880856 * get_datasetElement_8() const { return ___datasetElement_8; }
	inline XmlSchemaElement_t427880856 ** get_address_of_datasetElement_8() { return &___datasetElement_8; }
	inline void set_datasetElement_8(XmlSchemaElement_t427880856 * value)
	{
		___datasetElement_8 = value;
		Il2CppCodeGenWriteBarrier((&___datasetElement_8), value);
	}

	inline static int32_t get_offset_of_topLevelElements_9() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___topLevelElements_9)); }
	inline ArrayList_t2718874744 * get_topLevelElements_9() const { return ___topLevelElements_9; }
	inline ArrayList_t2718874744 ** get_address_of_topLevelElements_9() { return &___topLevelElements_9; }
	inline void set_topLevelElements_9(ArrayList_t2718874744 * value)
	{
		___topLevelElements_9 = value;
		Il2CppCodeGenWriteBarrier((&___topLevelElements_9), value);
	}

	inline static int32_t get_offset_of_targetElements_10() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___targetElements_10)); }
	inline ArrayList_t2718874744 * get_targetElements_10() const { return ___targetElements_10; }
	inline ArrayList_t2718874744 ** get_address_of_targetElements_10() { return &___targetElements_10; }
	inline void set_targetElements_10(ArrayList_t2718874744 * value)
	{
		___targetElements_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetElements_10), value);
	}

	inline static int32_t get_offset_of_currentTable_11() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___currentTable_11)); }
	inline TableStructure_t4158950525 * get_currentTable_11() const { return ___currentTable_11; }
	inline TableStructure_t4158950525 ** get_address_of_currentTable_11() { return &___currentTable_11; }
	inline void set_currentTable_11(TableStructure_t4158950525 * value)
	{
		___currentTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentTable_11), value);
	}

	inline static int32_t get_offset_of_currentAdapter_12() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020, ___currentAdapter_12)); }
	inline TableAdapterSchemaInfo_t1445784527 * get_currentAdapter_12() const { return ___currentAdapter_12; }
	inline TableAdapterSchemaInfo_t1445784527 ** get_address_of_currentAdapter_12() { return &___currentAdapter_12; }
	inline void set_currentAdapter_12(TableAdapterSchemaInfo_t1445784527 * value)
	{
		___currentAdapter_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentAdapter_12), value);
	}
};

struct XmlSchemaDataImporter_t2556819020_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDatatype System.Data.XmlSchemaDataImporter::schemaIntegerType
	XmlSchemaDatatype_t322714710 * ___schemaIntegerType_0;
	// System.Xml.Schema.XmlSchemaDatatype System.Data.XmlSchemaDataImporter::schemaDecimalType
	XmlSchemaDatatype_t322714710 * ___schemaDecimalType_1;
	// System.Xml.Schema.XmlSchemaComplexType System.Data.XmlSchemaDataImporter::schemaAnyType
	XmlSchemaComplexType_t3740801802 * ___schemaAnyType_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.XmlSchemaDataImporter::<>f__switch$map9
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map9_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.XmlSchemaDataImporter::<>f__switch$mapA
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapA_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.XmlSchemaDataImporter::<>f__switch$mapB
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapB_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.XmlSchemaDataImporter::<>f__switch$mapC
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapC_16;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.XmlSchemaDataImporter::<>f__switch$mapD
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapD_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.XmlSchemaDataImporter::<>f__switch$mapE
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapE_18;

public:
	inline static int32_t get_offset_of_schemaIntegerType_0() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___schemaIntegerType_0)); }
	inline XmlSchemaDatatype_t322714710 * get_schemaIntegerType_0() const { return ___schemaIntegerType_0; }
	inline XmlSchemaDatatype_t322714710 ** get_address_of_schemaIntegerType_0() { return &___schemaIntegerType_0; }
	inline void set_schemaIntegerType_0(XmlSchemaDatatype_t322714710 * value)
	{
		___schemaIntegerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaIntegerType_0), value);
	}

	inline static int32_t get_offset_of_schemaDecimalType_1() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___schemaDecimalType_1)); }
	inline XmlSchemaDatatype_t322714710 * get_schemaDecimalType_1() const { return ___schemaDecimalType_1; }
	inline XmlSchemaDatatype_t322714710 ** get_address_of_schemaDecimalType_1() { return &___schemaDecimalType_1; }
	inline void set_schemaDecimalType_1(XmlSchemaDatatype_t322714710 * value)
	{
		___schemaDecimalType_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaDecimalType_1), value);
	}

	inline static int32_t get_offset_of_schemaAnyType_2() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___schemaAnyType_2)); }
	inline XmlSchemaComplexType_t3740801802 * get_schemaAnyType_2() const { return ___schemaAnyType_2; }
	inline XmlSchemaComplexType_t3740801802 ** get_address_of_schemaAnyType_2() { return &___schemaAnyType_2; }
	inline void set_schemaAnyType_2(XmlSchemaComplexType_t3740801802 * value)
	{
		___schemaAnyType_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemaAnyType_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_13() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___U3CU3Ef__switchU24map9_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map9_13() const { return ___U3CU3Ef__switchU24map9_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map9_13() { return &___U3CU3Ef__switchU24map9_13; }
	inline void set_U3CU3Ef__switchU24map9_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map9_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map9_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_14() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___U3CU3Ef__switchU24mapA_14)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapA_14() const { return ___U3CU3Ef__switchU24mapA_14; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapA_14() { return &___U3CU3Ef__switchU24mapA_14; }
	inline void set_U3CU3Ef__switchU24mapA_14(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapA_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapA_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_15() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___U3CU3Ef__switchU24mapB_15)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapB_15() const { return ___U3CU3Ef__switchU24mapB_15; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapB_15() { return &___U3CU3Ef__switchU24mapB_15; }
	inline void set_U3CU3Ef__switchU24mapB_15(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapB_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_16() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___U3CU3Ef__switchU24mapC_16)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapC_16() const { return ___U3CU3Ef__switchU24mapC_16; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapC_16() { return &___U3CU3Ef__switchU24mapC_16; }
	inline void set_U3CU3Ef__switchU24mapC_16(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapC_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapC_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_17() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___U3CU3Ef__switchU24mapD_17)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapD_17() const { return ___U3CU3Ef__switchU24mapD_17; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapD_17() { return &___U3CU3Ef__switchU24mapD_17; }
	inline void set_U3CU3Ef__switchU24mapD_17(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapD_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapD_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_18() { return static_cast<int32_t>(offsetof(XmlSchemaDataImporter_t2556819020_StaticFields, ___U3CU3Ef__switchU24mapE_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapE_18() const { return ___U3CU3Ef__switchU24mapE_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapE_18() { return &___U3CU3Ef__switchU24mapE_18; }
	inline void set_U3CU3Ef__switchU24mapE_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapE_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapE_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATAIMPORTER_T2556819020_H
#ifndef TABLESTRUCTURE_T4158950525_H
#define TABLESTRUCTURE_T4158950525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.TableStructure
struct  TableStructure_t4158950525  : public RuntimeObject
{
public:
	// System.Data.DataTable System.Data.TableStructure::Table
	DataTable_t1734486485 * ___Table_0;
	// System.Collections.Hashtable System.Data.TableStructure::OrdinalColumns
	Hashtable_t1853889766 * ___OrdinalColumns_1;
	// System.Collections.ArrayList System.Data.TableStructure::NonOrdinalColumns
	ArrayList_t2718874744 * ___NonOrdinalColumns_2;
	// System.Data.DataColumn System.Data.TableStructure::PrimaryKey
	DataColumn_t2485786120 * ___PrimaryKey_3;

public:
	inline static int32_t get_offset_of_Table_0() { return static_cast<int32_t>(offsetof(TableStructure_t4158950525, ___Table_0)); }
	inline DataTable_t1734486485 * get_Table_0() const { return ___Table_0; }
	inline DataTable_t1734486485 ** get_address_of_Table_0() { return &___Table_0; }
	inline void set_Table_0(DataTable_t1734486485 * value)
	{
		___Table_0 = value;
		Il2CppCodeGenWriteBarrier((&___Table_0), value);
	}

	inline static int32_t get_offset_of_OrdinalColumns_1() { return static_cast<int32_t>(offsetof(TableStructure_t4158950525, ___OrdinalColumns_1)); }
	inline Hashtable_t1853889766 * get_OrdinalColumns_1() const { return ___OrdinalColumns_1; }
	inline Hashtable_t1853889766 ** get_address_of_OrdinalColumns_1() { return &___OrdinalColumns_1; }
	inline void set_OrdinalColumns_1(Hashtable_t1853889766 * value)
	{
		___OrdinalColumns_1 = value;
		Il2CppCodeGenWriteBarrier((&___OrdinalColumns_1), value);
	}

	inline static int32_t get_offset_of_NonOrdinalColumns_2() { return static_cast<int32_t>(offsetof(TableStructure_t4158950525, ___NonOrdinalColumns_2)); }
	inline ArrayList_t2718874744 * get_NonOrdinalColumns_2() const { return ___NonOrdinalColumns_2; }
	inline ArrayList_t2718874744 ** get_address_of_NonOrdinalColumns_2() { return &___NonOrdinalColumns_2; }
	inline void set_NonOrdinalColumns_2(ArrayList_t2718874744 * value)
	{
		___NonOrdinalColumns_2 = value;
		Il2CppCodeGenWriteBarrier((&___NonOrdinalColumns_2), value);
	}

	inline static int32_t get_offset_of_PrimaryKey_3() { return static_cast<int32_t>(offsetof(TableStructure_t4158950525, ___PrimaryKey_3)); }
	inline DataColumn_t2485786120 * get_PrimaryKey_3() const { return ___PrimaryKey_3; }
	inline DataColumn_t2485786120 ** get_address_of_PrimaryKey_3() { return &___PrimaryKey_3; }
	inline void set_PrimaryKey_3(DataColumn_t2485786120 * value)
	{
		___PrimaryKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___PrimaryKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLESTRUCTURE_T4158950525_H
#ifndef XMLSCHEMAWRITER_T4200114175_H
#define XMLSCHEMAWRITER_T4200114175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlSchemaWriter
struct  XmlSchemaWriter_t4200114175  : public RuntimeObject
{
public:
	// System.Xml.XmlWriter System.Data.XmlSchemaWriter::w
	XmlWriter_t127905479 * ___w_0;
	// System.Data.DataTable[] System.Data.XmlSchemaWriter::tables
	DataTableU5BU5D_t3044315960* ___tables_1;
	// System.Data.DataRelation[] System.Data.XmlSchemaWriter::relations
	DataRelationU5BU5D_t2648313166* ___relations_2;
	// System.String System.Data.XmlSchemaWriter::mainDataTable
	String_t* ___mainDataTable_3;
	// System.String System.Data.XmlSchemaWriter::dataSetName
	String_t* ___dataSetName_4;
	// System.String System.Data.XmlSchemaWriter::dataSetNamespace
	String_t* ___dataSetNamespace_5;
	// System.Data.PropertyCollection System.Data.XmlSchemaWriter::dataSetProperties
	PropertyCollection_t3446977085 * ___dataSetProperties_6;
	// System.Globalization.CultureInfo System.Data.XmlSchemaWriter::dataSetLocale
	CultureInfo_t4157843068 * ___dataSetLocale_7;
	// System.Collections.ArrayList System.Data.XmlSchemaWriter::globalTypeTables
	ArrayList_t2718874744 * ___globalTypeTables_8;
	// System.Collections.Hashtable System.Data.XmlSchemaWriter::additionalNamespaces
	Hashtable_t1853889766 * ___additionalNamespaces_9;
	// System.Collections.ArrayList System.Data.XmlSchemaWriter::annotation
	ArrayList_t2718874744 * ___annotation_10;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___w_0)); }
	inline XmlWriter_t127905479 * get_w_0() const { return ___w_0; }
	inline XmlWriter_t127905479 ** get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(XmlWriter_t127905479 * value)
	{
		___w_0 = value;
		Il2CppCodeGenWriteBarrier((&___w_0), value);
	}

	inline static int32_t get_offset_of_tables_1() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___tables_1)); }
	inline DataTableU5BU5D_t3044315960* get_tables_1() const { return ___tables_1; }
	inline DataTableU5BU5D_t3044315960** get_address_of_tables_1() { return &___tables_1; }
	inline void set_tables_1(DataTableU5BU5D_t3044315960* value)
	{
		___tables_1 = value;
		Il2CppCodeGenWriteBarrier((&___tables_1), value);
	}

	inline static int32_t get_offset_of_relations_2() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___relations_2)); }
	inline DataRelationU5BU5D_t2648313166* get_relations_2() const { return ___relations_2; }
	inline DataRelationU5BU5D_t2648313166** get_address_of_relations_2() { return &___relations_2; }
	inline void set_relations_2(DataRelationU5BU5D_t2648313166* value)
	{
		___relations_2 = value;
		Il2CppCodeGenWriteBarrier((&___relations_2), value);
	}

	inline static int32_t get_offset_of_mainDataTable_3() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___mainDataTable_3)); }
	inline String_t* get_mainDataTable_3() const { return ___mainDataTable_3; }
	inline String_t** get_address_of_mainDataTable_3() { return &___mainDataTable_3; }
	inline void set_mainDataTable_3(String_t* value)
	{
		___mainDataTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___mainDataTable_3), value);
	}

	inline static int32_t get_offset_of_dataSetName_4() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___dataSetName_4)); }
	inline String_t* get_dataSetName_4() const { return ___dataSetName_4; }
	inline String_t** get_address_of_dataSetName_4() { return &___dataSetName_4; }
	inline void set_dataSetName_4(String_t* value)
	{
		___dataSetName_4 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetName_4), value);
	}

	inline static int32_t get_offset_of_dataSetNamespace_5() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___dataSetNamespace_5)); }
	inline String_t* get_dataSetNamespace_5() const { return ___dataSetNamespace_5; }
	inline String_t** get_address_of_dataSetNamespace_5() { return &___dataSetNamespace_5; }
	inline void set_dataSetNamespace_5(String_t* value)
	{
		___dataSetNamespace_5 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetNamespace_5), value);
	}

	inline static int32_t get_offset_of_dataSetProperties_6() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___dataSetProperties_6)); }
	inline PropertyCollection_t3446977085 * get_dataSetProperties_6() const { return ___dataSetProperties_6; }
	inline PropertyCollection_t3446977085 ** get_address_of_dataSetProperties_6() { return &___dataSetProperties_6; }
	inline void set_dataSetProperties_6(PropertyCollection_t3446977085 * value)
	{
		___dataSetProperties_6 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetProperties_6), value);
	}

	inline static int32_t get_offset_of_dataSetLocale_7() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___dataSetLocale_7)); }
	inline CultureInfo_t4157843068 * get_dataSetLocale_7() const { return ___dataSetLocale_7; }
	inline CultureInfo_t4157843068 ** get_address_of_dataSetLocale_7() { return &___dataSetLocale_7; }
	inline void set_dataSetLocale_7(CultureInfo_t4157843068 * value)
	{
		___dataSetLocale_7 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetLocale_7), value);
	}

	inline static int32_t get_offset_of_globalTypeTables_8() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___globalTypeTables_8)); }
	inline ArrayList_t2718874744 * get_globalTypeTables_8() const { return ___globalTypeTables_8; }
	inline ArrayList_t2718874744 ** get_address_of_globalTypeTables_8() { return &___globalTypeTables_8; }
	inline void set_globalTypeTables_8(ArrayList_t2718874744 * value)
	{
		___globalTypeTables_8 = value;
		Il2CppCodeGenWriteBarrier((&___globalTypeTables_8), value);
	}

	inline static int32_t get_offset_of_additionalNamespaces_9() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___additionalNamespaces_9)); }
	inline Hashtable_t1853889766 * get_additionalNamespaces_9() const { return ___additionalNamespaces_9; }
	inline Hashtable_t1853889766 ** get_address_of_additionalNamespaces_9() { return &___additionalNamespaces_9; }
	inline void set_additionalNamespaces_9(Hashtable_t1853889766 * value)
	{
		___additionalNamespaces_9 = value;
		Il2CppCodeGenWriteBarrier((&___additionalNamespaces_9), value);
	}

	inline static int32_t get_offset_of_annotation_10() { return static_cast<int32_t>(offsetof(XmlSchemaWriter_t4200114175, ___annotation_10)); }
	inline ArrayList_t2718874744 * get_annotation_10() const { return ___annotation_10; }
	inline ArrayList_t2718874744 ** get_address_of_annotation_10() { return &___annotation_10; }
	inline void set_annotation_10(ArrayList_t2718874744 * value)
	{
		___annotation_10 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAWRITER_T4200114175_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef XMLHELPER_T906262798_H
#define XMLHELPER_T906262798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XmlHelper
struct  XmlHelper_t906262798  : public RuntimeObject
{
public:

public:
};

struct XmlHelper_t906262798_StaticFields
{
public:
	// System.Collections.Hashtable XmlHelper::localSchemaNameCache
	Hashtable_t1853889766 * ___localSchemaNameCache_0;
	// System.Collections.Hashtable XmlHelper::localXmlNameCache
	Hashtable_t1853889766 * ___localXmlNameCache_1;

public:
	inline static int32_t get_offset_of_localSchemaNameCache_0() { return static_cast<int32_t>(offsetof(XmlHelper_t906262798_StaticFields, ___localSchemaNameCache_0)); }
	inline Hashtable_t1853889766 * get_localSchemaNameCache_0() const { return ___localSchemaNameCache_0; }
	inline Hashtable_t1853889766 ** get_address_of_localSchemaNameCache_0() { return &___localSchemaNameCache_0; }
	inline void set_localSchemaNameCache_0(Hashtable_t1853889766 * value)
	{
		___localSchemaNameCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___localSchemaNameCache_0), value);
	}

	inline static int32_t get_offset_of_localXmlNameCache_1() { return static_cast<int32_t>(offsetof(XmlHelper_t906262798_StaticFields, ___localXmlNameCache_1)); }
	inline Hashtable_t1853889766 * get_localXmlNameCache_1() const { return ___localXmlNameCache_1; }
	inline Hashtable_t1853889766 ** get_address_of_localXmlNameCache_1() { return &___localXmlNameCache_1; }
	inline void set_localXmlNameCache_1(Hashtable_t1853889766 * value)
	{
		___localXmlNameCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___localXmlNameCache_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLHELPER_T906262798_H
#ifndef XMLDIFFLOADER_T2749071105_H
#define XMLDIFFLOADER_T2749071105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlDiffLoader
struct  XmlDiffLoader_t2749071105  : public RuntimeObject
{
public:
	// System.Data.DataSet System.Data.XmlDiffLoader::DSet
	DataSet_t3773542243 * ___DSet_0;
	// System.Data.DataTable System.Data.XmlDiffLoader::table
	DataTable_t1734486485 * ___table_1;
	// System.Collections.Hashtable System.Data.XmlDiffLoader::DiffGrRows
	Hashtable_t1853889766 * ___DiffGrRows_2;
	// System.Collections.Hashtable System.Data.XmlDiffLoader::ErrorRows
	Hashtable_t1853889766 * ___ErrorRows_3;

public:
	inline static int32_t get_offset_of_DSet_0() { return static_cast<int32_t>(offsetof(XmlDiffLoader_t2749071105, ___DSet_0)); }
	inline DataSet_t3773542243 * get_DSet_0() const { return ___DSet_0; }
	inline DataSet_t3773542243 ** get_address_of_DSet_0() { return &___DSet_0; }
	inline void set_DSet_0(DataSet_t3773542243 * value)
	{
		___DSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___DSet_0), value);
	}

	inline static int32_t get_offset_of_table_1() { return static_cast<int32_t>(offsetof(XmlDiffLoader_t2749071105, ___table_1)); }
	inline DataTable_t1734486485 * get_table_1() const { return ___table_1; }
	inline DataTable_t1734486485 ** get_address_of_table_1() { return &___table_1; }
	inline void set_table_1(DataTable_t1734486485 * value)
	{
		___table_1 = value;
		Il2CppCodeGenWriteBarrier((&___table_1), value);
	}

	inline static int32_t get_offset_of_DiffGrRows_2() { return static_cast<int32_t>(offsetof(XmlDiffLoader_t2749071105, ___DiffGrRows_2)); }
	inline Hashtable_t1853889766 * get_DiffGrRows_2() const { return ___DiffGrRows_2; }
	inline Hashtable_t1853889766 ** get_address_of_DiffGrRows_2() { return &___DiffGrRows_2; }
	inline void set_DiffGrRows_2(Hashtable_t1853889766 * value)
	{
		___DiffGrRows_2 = value;
		Il2CppCodeGenWriteBarrier((&___DiffGrRows_2), value);
	}

	inline static int32_t get_offset_of_ErrorRows_3() { return static_cast<int32_t>(offsetof(XmlDiffLoader_t2749071105, ___ErrorRows_3)); }
	inline Hashtable_t1853889766 * get_ErrorRows_3() const { return ___ErrorRows_3; }
	inline Hashtable_t1853889766 ** get_address_of_ErrorRows_3() { return &___ErrorRows_3; }
	inline void set_ErrorRows_3(Hashtable_t1853889766 * value)
	{
		___ErrorRows_3 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorRows_3), value);
	}
};

struct XmlDiffLoader_t2749071105_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.XmlDiffLoader::<>f__switch$map8
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map8_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_4() { return static_cast<int32_t>(offsetof(XmlDiffLoader_t2749071105_StaticFields, ___U3CU3Ef__switchU24map8_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map8_4() const { return ___U3CU3Ef__switchU24map8_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map8_4() { return &___U3CU3Ef__switchU24map8_4; }
	inline void set_U3CU3Ef__switchU24map8_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map8_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map8_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDIFFLOADER_T2749071105_H
#ifndef XMLTABLEWRITER_T2048085353_H
#define XMLTABLEWRITER_T2048085353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XmlTableWriter
struct  XmlTableWriter_t2048085353  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTABLEWRITER_T2048085353_H
#ifndef XMLDATALOADER_T1358168730_H
#define XMLDATALOADER_T1358168730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlDataLoader
struct  XmlDataLoader_t1358168730  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATALOADER_T1358168730_H
#ifndef RELATIONSTRUCTURE_T1029267685_H
#define RELATIONSTRUCTURE_T1029267685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.RelationStructure
struct  RelationStructure_t1029267685  : public RuntimeObject
{
public:
	// System.String System.Data.RelationStructure::ExplicitName
	String_t* ___ExplicitName_0;
	// System.String System.Data.RelationStructure::ParentTableName
	String_t* ___ParentTableName_1;
	// System.String System.Data.RelationStructure::ChildTableName
	String_t* ___ChildTableName_2;
	// System.String System.Data.RelationStructure::ParentColumnName
	String_t* ___ParentColumnName_3;
	// System.String System.Data.RelationStructure::ChildColumnName
	String_t* ___ChildColumnName_4;
	// System.Boolean System.Data.RelationStructure::IsNested
	bool ___IsNested_5;
	// System.Boolean System.Data.RelationStructure::CreateConstraint
	bool ___CreateConstraint_6;

public:
	inline static int32_t get_offset_of_ExplicitName_0() { return static_cast<int32_t>(offsetof(RelationStructure_t1029267685, ___ExplicitName_0)); }
	inline String_t* get_ExplicitName_0() const { return ___ExplicitName_0; }
	inline String_t** get_address_of_ExplicitName_0() { return &___ExplicitName_0; }
	inline void set_ExplicitName_0(String_t* value)
	{
		___ExplicitName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExplicitName_0), value);
	}

	inline static int32_t get_offset_of_ParentTableName_1() { return static_cast<int32_t>(offsetof(RelationStructure_t1029267685, ___ParentTableName_1)); }
	inline String_t* get_ParentTableName_1() const { return ___ParentTableName_1; }
	inline String_t** get_address_of_ParentTableName_1() { return &___ParentTableName_1; }
	inline void set_ParentTableName_1(String_t* value)
	{
		___ParentTableName_1 = value;
		Il2CppCodeGenWriteBarrier((&___ParentTableName_1), value);
	}

	inline static int32_t get_offset_of_ChildTableName_2() { return static_cast<int32_t>(offsetof(RelationStructure_t1029267685, ___ChildTableName_2)); }
	inline String_t* get_ChildTableName_2() const { return ___ChildTableName_2; }
	inline String_t** get_address_of_ChildTableName_2() { return &___ChildTableName_2; }
	inline void set_ChildTableName_2(String_t* value)
	{
		___ChildTableName_2 = value;
		Il2CppCodeGenWriteBarrier((&___ChildTableName_2), value);
	}

	inline static int32_t get_offset_of_ParentColumnName_3() { return static_cast<int32_t>(offsetof(RelationStructure_t1029267685, ___ParentColumnName_3)); }
	inline String_t* get_ParentColumnName_3() const { return ___ParentColumnName_3; }
	inline String_t** get_address_of_ParentColumnName_3() { return &___ParentColumnName_3; }
	inline void set_ParentColumnName_3(String_t* value)
	{
		___ParentColumnName_3 = value;
		Il2CppCodeGenWriteBarrier((&___ParentColumnName_3), value);
	}

	inline static int32_t get_offset_of_ChildColumnName_4() { return static_cast<int32_t>(offsetof(RelationStructure_t1029267685, ___ChildColumnName_4)); }
	inline String_t* get_ChildColumnName_4() const { return ___ChildColumnName_4; }
	inline String_t** get_address_of_ChildColumnName_4() { return &___ChildColumnName_4; }
	inline void set_ChildColumnName_4(String_t* value)
	{
		___ChildColumnName_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChildColumnName_4), value);
	}

	inline static int32_t get_offset_of_IsNested_5() { return static_cast<int32_t>(offsetof(RelationStructure_t1029267685, ___IsNested_5)); }
	inline bool get_IsNested_5() const { return ___IsNested_5; }
	inline bool* get_address_of_IsNested_5() { return &___IsNested_5; }
	inline void set_IsNested_5(bool value)
	{
		___IsNested_5 = value;
	}

	inline static int32_t get_offset_of_CreateConstraint_6() { return static_cast<int32_t>(offsetof(RelationStructure_t1029267685, ___CreateConstraint_6)); }
	inline bool get_CreateConstraint_6() const { return ___CreateConstraint_6; }
	inline bool* get_address_of_CreateConstraint_6() { return &___CreateConstraint_6; }
	inline void set_CreateConstraint_6(bool value)
	{
		___CreateConstraint_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATIONSTRUCTURE_T1029267685_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef XNODE_T716131460_H
#define XNODE_T716131460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XNode
struct  XNode_t716131460  : public XObject_t1119084474
{
public:
	// System.Xml.Linq.XNode System.Xml.Linq.XNode::previous
	XNode_t716131460 * ___previous_6;
	// System.Xml.Linq.XNode System.Xml.Linq.XNode::next
	XNode_t716131460 * ___next_7;

public:
	inline static int32_t get_offset_of_previous_6() { return static_cast<int32_t>(offsetof(XNode_t716131460, ___previous_6)); }
	inline XNode_t716131460 * get_previous_6() const { return ___previous_6; }
	inline XNode_t716131460 ** get_address_of_previous_6() { return &___previous_6; }
	inline void set_previous_6(XNode_t716131460 * value)
	{
		___previous_6 = value;
		Il2CppCodeGenWriteBarrier((&___previous_6), value);
	}

	inline static int32_t get_offset_of_next_7() { return static_cast<int32_t>(offsetof(XNode_t716131460, ___next_7)); }
	inline XNode_t716131460 * get_next_7() const { return ___next_7; }
	inline XNode_t716131460 ** get_address_of_next_7() { return &___next_7; }
	inline void set_next_7(XNode_t716131460 * value)
	{
		___next_7 = value;
		Il2CppCodeGenWriteBarrier((&___next_7), value);
	}
};

struct XNode_t716131460_StaticFields
{
public:
	// System.Xml.Linq.XNodeEqualityComparer System.Xml.Linq.XNode::eq_comparer
	XNodeEqualityComparer_t35321779 * ___eq_comparer_4;
	// System.Xml.Linq.XNodeDocumentOrderComparer System.Xml.Linq.XNode::order_comparer
	XNodeDocumentOrderComparer_t3558329258 * ___order_comparer_5;

public:
	inline static int32_t get_offset_of_eq_comparer_4() { return static_cast<int32_t>(offsetof(XNode_t716131460_StaticFields, ___eq_comparer_4)); }
	inline XNodeEqualityComparer_t35321779 * get_eq_comparer_4() const { return ___eq_comparer_4; }
	inline XNodeEqualityComparer_t35321779 ** get_address_of_eq_comparer_4() { return &___eq_comparer_4; }
	inline void set_eq_comparer_4(XNodeEqualityComparer_t35321779 * value)
	{
		___eq_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___eq_comparer_4), value);
	}

	inline static int32_t get_offset_of_order_comparer_5() { return static_cast<int32_t>(offsetof(XNode_t716131460_StaticFields, ___order_comparer_5)); }
	inline XNodeDocumentOrderComparer_t3558329258 * get_order_comparer_5() const { return ___order_comparer_5; }
	inline XNodeDocumentOrderComparer_t3558329258 ** get_address_of_order_comparer_5() { return &___order_comparer_5; }
	inline void set_order_comparer_5(XNodeDocumentOrderComparer_t3558329258 * value)
	{
		___order_comparer_5 = value;
		Il2CppCodeGenWriteBarrier((&___order_comparer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XNODE_T716131460_H
#ifndef U24ARRAYTYPEU24168_T758788411_H
#define U24ARRAYTYPEU24168_T758788411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$168
struct  U24ArrayTypeU24168_t758788411 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24168_t758788411__padding[168];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24168_T758788411_H
#ifndef RELATIONSTRUCTURECOLLECTION_T3303280033_H
#define RELATIONSTRUCTURECOLLECTION_T3303280033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.RelationStructureCollection
struct  RelationStructureCollection_t3303280033  : public CollectionBase_t2727926298
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATIONSTRUCTURECOLLECTION_T3303280033_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U24ARRAYTYPEU2416_T3254766647_H
#define U24ARRAYTYPEU2416_T3254766647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$16
struct  U24ArrayTypeU2416_t3254766647 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2416_t3254766647__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2416_T3254766647_H
#ifndef DATAMEMBERATTRIBUTE_T1339837472_H
#define DATAMEMBERATTRIBUTE_T1339837472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataMemberAttribute
struct  DataMemberAttribute_t1339837472  : public Attribute_t861562559
{
public:
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::is_required
	bool ___is_required_0;
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::emit_default
	bool ___emit_default_1;
	// System.String System.Runtime.Serialization.DataMemberAttribute::name
	String_t* ___name_2;
	// System.Int32 System.Runtime.Serialization.DataMemberAttribute::order
	int32_t ___order_3;

public:
	inline static int32_t get_offset_of_is_required_0() { return static_cast<int32_t>(offsetof(DataMemberAttribute_t1339837472, ___is_required_0)); }
	inline bool get_is_required_0() const { return ___is_required_0; }
	inline bool* get_address_of_is_required_0() { return &___is_required_0; }
	inline void set_is_required_0(bool value)
	{
		___is_required_0 = value;
	}

	inline static int32_t get_offset_of_emit_default_1() { return static_cast<int32_t>(offsetof(DataMemberAttribute_t1339837472, ___emit_default_1)); }
	inline bool get_emit_default_1() const { return ___emit_default_1; }
	inline bool* get_address_of_emit_default_1() { return &___emit_default_1; }
	inline void set_emit_default_1(bool value)
	{
		___emit_default_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(DataMemberAttribute_t1339837472, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_order_3() { return static_cast<int32_t>(offsetof(DataMemberAttribute_t1339837472, ___order_3)); }
	inline int32_t get_order_3() const { return ___order_3; }
	inline int32_t* get_address_of_order_3() { return &___order_3; }
	inline void set_order_3(int32_t value)
	{
		___order_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMEMBERATTRIBUTE_T1339837472_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef U24ARRAYTYPEU241084_T3907531067_H
#define U24ARRAYTYPEU241084_T3907531067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$1084
struct  U24ArrayTypeU241084_t3907531067 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241084_t3907531067__padding[1084];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241084_T3907531067_H
#ifndef DATACONTRACTATTRIBUTE_T412496005_H
#define DATACONTRACTATTRIBUTE_T412496005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataContractAttribute
struct  DataContractAttribute_t412496005  : public Attribute_t861562559
{
public:
	// System.Boolean System.Runtime.Serialization.DataContractAttribute::<IsReference>k__BackingField
	bool ___U3CIsReferenceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DataContractAttribute_t412496005, ___U3CIsReferenceU3Ek__BackingField_0)); }
	inline bool get_U3CIsReferenceU3Ek__BackingField_0() const { return ___U3CIsReferenceU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsReferenceU3Ek__BackingField_0() { return &___U3CIsReferenceU3Ek__BackingField_0; }
	inline void set_U3CIsReferenceU3Ek__BackingField_0(bool value)
	{
		___U3CIsReferenceU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACONTRACTATTRIBUTE_T412496005_H
#ifndef U24ARRAYTYPEU2464_T499776627_H
#define U24ARRAYTYPEU2464_T499776627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$64
struct  U24ArrayTypeU2464_t499776627 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2464_t499776627__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2464_T499776627_H
#ifndef ENUMMEMBERATTRIBUTE_T1084128815_H
#define ENUMMEMBERATTRIBUTE_T1084128815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.EnumMemberAttribute
struct  EnumMemberAttribute_t1084128815  : public Attribute_t861562559
{
public:
	// System.String System.Runtime.Serialization.EnumMemberAttribute::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(EnumMemberAttribute_t1084128815, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMMEMBERATTRIBUTE_T1084128815_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef U24ARRAYTYPEU24328_T1156913777_H
#define U24ARRAYTYPEU24328_T1156913777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$328
struct  U24ArrayTypeU24328_t1156913777 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24328_t1156913777__padding[328];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24328_T1156913777_H
#ifndef U24ARRAYTYPEU2440_T2867270459_H
#define U24ARRAYTYPEU2440_T2867270459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$40
struct  U24ArrayTypeU2440_t2867270459 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2440_t2867270459__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2440_T2867270459_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef UNITYEVENT_1_T489719741_H
#define UNITYEVENT_1_T489719741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityEvent_1_t489719741  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t489719741, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T489719741_H
#ifndef XATTRIBUTE_T355876829_H
#define XATTRIBUTE_T355876829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XAttribute
struct  XAttribute_t355876829  : public XObject_t1119084474
{
public:
	// System.Xml.Linq.XName System.Xml.Linq.XAttribute::name
	XName_t3751455998 * ___name_5;
	// System.String System.Xml.Linq.XAttribute::value
	String_t* ___value_6;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XAttribute::next
	XAttribute_t355876829 * ___next_7;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XAttribute::previous
	XAttribute_t355876829 * ___previous_8;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XAttribute_t355876829, ___name_5)); }
	inline XName_t3751455998 * get_name_5() const { return ___name_5; }
	inline XName_t3751455998 ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XName_t3751455998 * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(XAttribute_t355876829, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}

	inline static int32_t get_offset_of_next_7() { return static_cast<int32_t>(offsetof(XAttribute_t355876829, ___next_7)); }
	inline XAttribute_t355876829 * get_next_7() const { return ___next_7; }
	inline XAttribute_t355876829 ** get_address_of_next_7() { return &___next_7; }
	inline void set_next_7(XAttribute_t355876829 * value)
	{
		___next_7 = value;
		Il2CppCodeGenWriteBarrier((&___next_7), value);
	}

	inline static int32_t get_offset_of_previous_8() { return static_cast<int32_t>(offsetof(XAttribute_t355876829, ___previous_8)); }
	inline XAttribute_t355876829 * get_previous_8() const { return ___previous_8; }
	inline XAttribute_t355876829 ** get_address_of_previous_8() { return &___previous_8; }
	inline void set_previous_8(XAttribute_t355876829 * value)
	{
		___previous_8 = value;
		Il2CppCodeGenWriteBarrier((&___previous_8), value);
	}
};

struct XAttribute_t355876829_StaticFields
{
public:
	// System.Xml.Linq.XAttribute[] System.Xml.Linq.XAttribute::empty_array
	XAttributeU5BU5D_t61111312* ___empty_array_4;
	// System.Char[] System.Xml.Linq.XAttribute::escapeChars
	CharU5BU5D_t3528271667* ___escapeChars_9;

public:
	inline static int32_t get_offset_of_empty_array_4() { return static_cast<int32_t>(offsetof(XAttribute_t355876829_StaticFields, ___empty_array_4)); }
	inline XAttributeU5BU5D_t61111312* get_empty_array_4() const { return ___empty_array_4; }
	inline XAttributeU5BU5D_t61111312** get_address_of_empty_array_4() { return &___empty_array_4; }
	inline void set_empty_array_4(XAttributeU5BU5D_t61111312* value)
	{
		___empty_array_4 = value;
		Il2CppCodeGenWriteBarrier((&___empty_array_4), value);
	}

	inline static int32_t get_offset_of_escapeChars_9() { return static_cast<int32_t>(offsetof(XAttribute_t355876829_StaticFields, ___escapeChars_9)); }
	inline CharU5BU5D_t3528271667* get_escapeChars_9() const { return ___escapeChars_9; }
	inline CharU5BU5D_t3528271667** get_address_of_escapeChars_9() { return &___escapeChars_9; }
	inline void set_escapeChars_9(CharU5BU5D_t3528271667* value)
	{
		___escapeChars_9 = value;
		Il2CppCodeGenWriteBarrier((&___escapeChars_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTE_T355876829_H
#ifndef TRIGGEREVENT_T3867320123_H
#define TRIGGEREVENT_T3867320123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct  TriggerEvent_t3867320123  : public UnityEvent_1_t489719741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T3867320123_H
#ifndef EVENTTRIGGERTYPE_T55832929_H
#define EVENTTRIGGERTYPE_T55832929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTriggerType
struct  EventTriggerType_t55832929 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventTriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventTriggerType_t55832929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGERTYPE_T55832929_H
#ifndef MOVEDIRECTION_T1216237838_H
#define MOVEDIRECTION_T1216237838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t1216237838 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MoveDirection_t1216237838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T1216237838_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef FRAMEPRESSSTATE_T3039385657_H
#define FRAMEPRESSSTATE_T3039385657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/FramePressState
struct  FramePressState_t3039385657 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/FramePressState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FramePressState_t3039385657, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEPRESSSTATE_T3039385657_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef XMLREADMODE_T173510697_H
#define XMLREADMODE_T173510697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlReadMode
struct  XmlReadMode_t173510697 
{
public:
	// System.Int32 System.Data.XmlReadMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlReadMode_t173510697, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADMODE_T173510697_H
#ifndef XCOMMENT_T1832251191_H
#define XCOMMENT_T1832251191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XComment
struct  XComment_t1832251191  : public XNode_t716131460
{
public:
	// System.String System.Xml.Linq.XComment::value
	String_t* ___value_8;

public:
	inline static int32_t get_offset_of_value_8() { return static_cast<int32_t>(offsetof(XComment_t1832251191, ___value_8)); }
	inline String_t* get_value_8() const { return ___value_8; }
	inline String_t** get_address_of_value_8() { return &___value_8; }
	inline void set_value_8(String_t* value)
	{
		___value_8 = value;
		Il2CppCodeGenWriteBarrier((&___value_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCOMMENT_T1832251191_H
#ifndef XMLWRITEMODE_T421248975_H
#define XMLWRITEMODE_T421248975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlWriteMode
struct  XmlWriteMode_t421248975 
{
public:
	// System.Int32 System.Data.XmlWriteMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlWriteMode_t421248975, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITEMODE_T421248975_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$168 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU24168_t758788411  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$168 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU24168_t758788411  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$328 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU24328_t1156913777  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU2464_t499776627  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$328 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU24328_t1156913777  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU2464_t499776627  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$1084 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU241084_t3907531067  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$1084 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU241084_t3907531067  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU2440_t2867270459  ___U24U24fieldU2D8_8;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU24168_t758788411  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU24168_t758788411 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU24168_t758788411  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU24168_t758788411  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU24168_t758788411 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU24168_t758788411  value)
	{
		___U24U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D2_2)); }
	inline U24ArrayTypeU24328_t1156913777  get_U24U24fieldU2D2_2() const { return ___U24U24fieldU2D2_2; }
	inline U24ArrayTypeU24328_t1156913777 * get_address_of_U24U24fieldU2D2_2() { return &___U24U24fieldU2D2_2; }
	inline void set_U24U24fieldU2D2_2(U24ArrayTypeU24328_t1156913777  value)
	{
		___U24U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D3_3)); }
	inline U24ArrayTypeU2464_t499776627  get_U24U24fieldU2D3_3() const { return ___U24U24fieldU2D3_3; }
	inline U24ArrayTypeU2464_t499776627 * get_address_of_U24U24fieldU2D3_3() { return &___U24U24fieldU2D3_3; }
	inline void set_U24U24fieldU2D3_3(U24ArrayTypeU2464_t499776627  value)
	{
		___U24U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D4_4)); }
	inline U24ArrayTypeU24328_t1156913777  get_U24U24fieldU2D4_4() const { return ___U24U24fieldU2D4_4; }
	inline U24ArrayTypeU24328_t1156913777 * get_address_of_U24U24fieldU2D4_4() { return &___U24U24fieldU2D4_4; }
	inline void set_U24U24fieldU2D4_4(U24ArrayTypeU24328_t1156913777  value)
	{
		___U24U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D5_5)); }
	inline U24ArrayTypeU2464_t499776627  get_U24U24fieldU2D5_5() const { return ___U24U24fieldU2D5_5; }
	inline U24ArrayTypeU2464_t499776627 * get_address_of_U24U24fieldU2D5_5() { return &___U24U24fieldU2D5_5; }
	inline void set_U24U24fieldU2D5_5(U24ArrayTypeU2464_t499776627  value)
	{
		___U24U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D6_6)); }
	inline U24ArrayTypeU241084_t3907531067  get_U24U24fieldU2D6_6() const { return ___U24U24fieldU2D6_6; }
	inline U24ArrayTypeU241084_t3907531067 * get_address_of_U24U24fieldU2D6_6() { return &___U24U24fieldU2D6_6; }
	inline void set_U24U24fieldU2D6_6(U24ArrayTypeU241084_t3907531067  value)
	{
		___U24U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D7_7)); }
	inline U24ArrayTypeU241084_t3907531067  get_U24U24fieldU2D7_7() const { return ___U24U24fieldU2D7_7; }
	inline U24ArrayTypeU241084_t3907531067 * get_address_of_U24U24fieldU2D7_7() { return &___U24U24fieldU2D7_7; }
	inline void set_U24U24fieldU2D7_7(U24ArrayTypeU241084_t3907531067  value)
	{
		___U24U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24U24fieldU2D8_8)); }
	inline U24ArrayTypeU2440_t2867270459  get_U24U24fieldU2D8_8() const { return ___U24U24fieldU2D8_8; }
	inline U24ArrayTypeU2440_t2867270459 * get_address_of_U24U24fieldU2D8_8() { return &___U24U24fieldU2D8_8; }
	inline void set_U24U24fieldU2D8_8(U24ArrayTypeU2440_t2867270459  value)
	{
		___U24U24fieldU2D8_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef XDOCUMENTTYPE_T1853592271_H
#define XDOCUMENTTYPE_T1853592271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XDocumentType
struct  XDocumentType_t1853592271  : public XNode_t716131460
{
public:
	// System.String System.Xml.Linq.XDocumentType::name
	String_t* ___name_8;
	// System.String System.Xml.Linq.XDocumentType::pubid
	String_t* ___pubid_9;
	// System.String System.Xml.Linq.XDocumentType::sysid
	String_t* ___sysid_10;
	// System.String System.Xml.Linq.XDocumentType::intSubset
	String_t* ___intSubset_11;

public:
	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(XDocumentType_t1853592271, ___name_8)); }
	inline String_t* get_name_8() const { return ___name_8; }
	inline String_t** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(String_t* value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier((&___name_8), value);
	}

	inline static int32_t get_offset_of_pubid_9() { return static_cast<int32_t>(offsetof(XDocumentType_t1853592271, ___pubid_9)); }
	inline String_t* get_pubid_9() const { return ___pubid_9; }
	inline String_t** get_address_of_pubid_9() { return &___pubid_9; }
	inline void set_pubid_9(String_t* value)
	{
		___pubid_9 = value;
		Il2CppCodeGenWriteBarrier((&___pubid_9), value);
	}

	inline static int32_t get_offset_of_sysid_10() { return static_cast<int32_t>(offsetof(XDocumentType_t1853592271, ___sysid_10)); }
	inline String_t* get_sysid_10() const { return ___sysid_10; }
	inline String_t** get_address_of_sysid_10() { return &___sysid_10; }
	inline void set_sysid_10(String_t* value)
	{
		___sysid_10 = value;
		Il2CppCodeGenWriteBarrier((&___sysid_10), value);
	}

	inline static int32_t get_offset_of_intSubset_11() { return static_cast<int32_t>(offsetof(XDocumentType_t1853592271, ___intSubset_11)); }
	inline String_t* get_intSubset_11() const { return ___intSubset_11; }
	inline String_t** get_address_of_intSubset_11() { return &___intSubset_11; }
	inline void set_intSubset_11(String_t* value)
	{
		___intSubset_11 = value;
		Il2CppCodeGenWriteBarrier((&___intSubset_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTTYPE_T1853592271_H
#ifndef LOADOPTIONS_T4182560034_H
#define LOADOPTIONS_T4182560034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.LoadOptions
struct  LoadOptions_t4182560034 
{
public:
	// System.Int32 System.Xml.Linq.LoadOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadOptions_t4182560034, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADOPTIONS_T4182560034_H
#ifndef XCONTAINER_T3387731002_H
#define XCONTAINER_T3387731002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XContainer
struct  XContainer_t3387731002  : public XNode_t716131460
{
public:
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer::first
	XNode_t716131460 * ___first_8;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer::last
	XNode_t716131460 * ___last_9;

public:
	inline static int32_t get_offset_of_first_8() { return static_cast<int32_t>(offsetof(XContainer_t3387731002, ___first_8)); }
	inline XNode_t716131460 * get_first_8() const { return ___first_8; }
	inline XNode_t716131460 ** get_address_of_first_8() { return &___first_8; }
	inline void set_first_8(XNode_t716131460 * value)
	{
		___first_8 = value;
		Il2CppCodeGenWriteBarrier((&___first_8), value);
	}

	inline static int32_t get_offset_of_last_9() { return static_cast<int32_t>(offsetof(XContainer_t3387731002, ___last_9)); }
	inline XNode_t716131460 * get_last_9() const { return ___last_9; }
	inline XNode_t716131460 ** get_address_of_last_9() { return &___last_9; }
	inline void set_last_9(XNode_t716131460 * value)
	{
		___last_9 = value;
		Il2CppCodeGenWriteBarrier((&___last_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCONTAINER_T3387731002_H
#ifndef SAVEOPTIONS_T2609998520_H
#define SAVEOPTIONS_T2609998520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.SaveOptions
struct  SaveOptions_t2609998520 
{
public:
	// System.Int32 System.Xml.Linq.SaveOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SaveOptions_t2609998520, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEOPTIONS_T2609998520_H
#ifndef COMPARERESULT_T200929839_H
#define COMPARERESULT_T200929839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XNodeDocumentOrderComparer/CompareResult
struct  CompareResult_t200929839 
{
public:
	// System.Int32 System.Xml.Linq.XNodeDocumentOrderComparer/CompareResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompareResult_t200929839, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARERESULT_T200929839_H
#ifndef EVENTHANDLE_T600343995_H
#define EVENTHANDLE_T600343995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_t600343995 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventHandle_t600343995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_T600343995_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU2416_t3254766647  ___U24U24fieldU2D0_0;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU2416_t3254766647  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU2416_t3254766647 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU2416_t3254766647  value)
	{
		___U24U24fieldU2D0_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef XTEXT_T1448860321_H
#define XTEXT_T1448860321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XText
struct  XText_t1448860321  : public XNode_t716131460
{
public:
	// System.String System.Xml.Linq.XText::value
	String_t* ___value_8;

public:
	inline static int32_t get_offset_of_value_8() { return static_cast<int32_t>(offsetof(XText_t1448860321, ___value_8)); }
	inline String_t* get_value_8() const { return ___value_8; }
	inline String_t** get_address_of_value_8() { return &___value_8; }
	inline void set_value_8(String_t* value)
	{
		___value_8 = value;
		Il2CppCodeGenWriteBarrier((&___value_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XTEXT_T1448860321_H
#ifndef XPROCESSINGINSTRUCTION_T2943691487_H
#define XPROCESSINGINSTRUCTION_T2943691487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XProcessingInstruction
struct  XProcessingInstruction_t2943691487  : public XNode_t716131460
{
public:
	// System.String System.Xml.Linq.XProcessingInstruction::name
	String_t* ___name_8;
	// System.String System.Xml.Linq.XProcessingInstruction::data
	String_t* ___data_9;

public:
	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(XProcessingInstruction_t2943691487, ___name_8)); }
	inline String_t* get_name_8() const { return ___name_8; }
	inline String_t** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(String_t* value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier((&___name_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(XProcessingInstruction_t2943691487, ___data_9)); }
	inline String_t* get_data_9() const { return ___data_9; }
	inline String_t** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(String_t* value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROCESSINGINSTRUCTION_T2943691487_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MOUSEBUTTONEVENTDATA_T3190347560_H
#define MOUSEBUTTONEVENTDATA_T3190347560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct  MouseButtonEventData_t3190347560  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/FramePressState UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonState
	int32_t ___buttonState_0;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonData
	PointerEventData_t3807901092 * ___buttonData_1;

public:
	inline static int32_t get_offset_of_buttonState_0() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3190347560, ___buttonState_0)); }
	inline int32_t get_buttonState_0() const { return ___buttonState_0; }
	inline int32_t* get_address_of_buttonState_0() { return &___buttonState_0; }
	inline void set_buttonState_0(int32_t value)
	{
		___buttonState_0 = value;
	}

	inline static int32_t get_offset_of_buttonData_1() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3190347560, ___buttonData_1)); }
	inline PointerEventData_t3807901092 * get_buttonData_1() const { return ___buttonData_1; }
	inline PointerEventData_t3807901092 ** get_address_of_buttonData_1() { return &___buttonData_1; }
	inline void set_buttonData_1(PointerEventData_t3807901092 * value)
	{
		___buttonData_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttonData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONEVENTDATA_T3190347560_H
#ifndef XCDATA_T832623270_H
#define XCDATA_T832623270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XCData
struct  XCData_t832623270  : public XText_t1448860321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCDATA_T832623270_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef XMLDATAREADER_T4085121624_H
#define XMLDATAREADER_T4085121624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlDataReader
struct  XmlDataReader_t4085121624  : public RuntimeObject
{
public:
	// System.Data.DataSet System.Data.XmlDataReader::dataset
	DataSet_t3773542243 * ___dataset_0;
	// System.Xml.XmlReader System.Data.XmlDataReader::reader
	XmlReader_t3121518892 * ___reader_1;
	// System.Data.XmlReadMode System.Data.XmlDataReader::mode
	int32_t ___mode_2;

public:
	inline static int32_t get_offset_of_dataset_0() { return static_cast<int32_t>(offsetof(XmlDataReader_t4085121624, ___dataset_0)); }
	inline DataSet_t3773542243 * get_dataset_0() const { return ___dataset_0; }
	inline DataSet_t3773542243 ** get_address_of_dataset_0() { return &___dataset_0; }
	inline void set_dataset_0(DataSet_t3773542243 * value)
	{
		___dataset_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataset_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlDataReader_t4085121624, ___reader_1)); }
	inline XmlReader_t3121518892 * get_reader_1() const { return ___reader_1; }
	inline XmlReader_t3121518892 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(XmlReader_t3121518892 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(XmlDataReader_t4085121624, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATAREADER_T4085121624_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BUTTONSTATE_T857139936_H
#define BUTTONSTATE_T857139936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct  ButtonState_t857139936  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerInputModule/ButtonState::m_Button
	int32_t ___m_Button_0;
	// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData UnityEngine.EventSystems.PointerInputModule/ButtonState::m_EventData
	MouseButtonEventData_t3190347560 * ___m_EventData_1;

public:
	inline static int32_t get_offset_of_m_Button_0() { return static_cast<int32_t>(offsetof(ButtonState_t857139936, ___m_Button_0)); }
	inline int32_t get_m_Button_0() const { return ___m_Button_0; }
	inline int32_t* get_address_of_m_Button_0() { return &___m_Button_0; }
	inline void set_m_Button_0(int32_t value)
	{
		___m_Button_0 = value;
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(ButtonState_t857139936, ___m_EventData_1)); }
	inline MouseButtonEventData_t3190347560 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline MouseButtonEventData_t3190347560 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(MouseButtonEventData_t3190347560 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATE_T857139936_H
#ifndef POINTEREVENTDATA_T3807901092_H
#define POINTEREVENTDATA_T3807901092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t3807901092  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1113636619 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1113636619 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1113636619 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1113636619 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1113636619 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2585711361 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2156229523  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2156229523  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2156229523  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t3722313464  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t3722313464  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2156229523  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___m_PointerPress_3)); }
	inline GameObject_t1113636619 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1113636619 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1113636619 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1113636619 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1113636619 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1113636619 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1113636619 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t3360306849  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t3360306849  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t3360306849  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t3360306849  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___hovered_9)); }
	inline List_1_t2585711361 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2585711361 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2585711361 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2156229523  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2156229523 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2156229523  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t3722313464  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t3722313464 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t3722313464  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2156229523  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2156229523 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2156229523  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T3807901092_H
#ifndef XDOCUMENT_T1376419702_H
#define XDOCUMENT_T1376419702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XDocument
struct  XDocument_t1376419702  : public XContainer_t3387731002
{
public:
	// System.Xml.Linq.XDeclaration System.Xml.Linq.XDocument::xmldecl
	XDeclaration_t2907650823 * ___xmldecl_10;

public:
	inline static int32_t get_offset_of_xmldecl_10() { return static_cast<int32_t>(offsetof(XDocument_t1376419702, ___xmldecl_10)); }
	inline XDeclaration_t2907650823 * get_xmldecl_10() const { return ___xmldecl_10; }
	inline XDeclaration_t2907650823 ** get_address_of_xmldecl_10() { return &___xmldecl_10; }
	inline void set_xmldecl_10(XDeclaration_t2907650823 * value)
	{
		___xmldecl_10 = value;
		Il2CppCodeGenWriteBarrier((&___xmldecl_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENT_T1376419702_H
#ifndef AXISEVENTDATA_T2331243652_H
#define AXISEVENTDATA_T2331243652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AxisEventData
struct  AxisEventData_t2331243652  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.AxisEventData::<moveVector>k__BackingField
	Vector2_t2156229523  ___U3CmoveVectorU3Ek__BackingField_2;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.EventSystems.AxisEventData::<moveDir>k__BackingField
	int32_t ___U3CmoveDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmoveVectorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AxisEventData_t2331243652, ___U3CmoveVectorU3Ek__BackingField_2)); }
	inline Vector2_t2156229523  get_U3CmoveVectorU3Ek__BackingField_2() const { return ___U3CmoveVectorU3Ek__BackingField_2; }
	inline Vector2_t2156229523 * get_address_of_U3CmoveVectorU3Ek__BackingField_2() { return &___U3CmoveVectorU3Ek__BackingField_2; }
	inline void set_U3CmoveVectorU3Ek__BackingField_2(Vector2_t2156229523  value)
	{
		___U3CmoveVectorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmoveDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AxisEventData_t2331243652, ___U3CmoveDirU3Ek__BackingField_3)); }
	inline int32_t get_U3CmoveDirU3Ek__BackingField_3() const { return ___U3CmoveDirU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmoveDirU3Ek__BackingField_3() { return &___U3CmoveDirU3Ek__BackingField_3; }
	inline void set_U3CmoveDirU3Ek__BackingField_3(int32_t value)
	{
		___U3CmoveDirU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEVENTDATA_T2331243652_H
#ifndef ENTRY_T3344766165_H
#define ENTRY_T3344766165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger/Entry
struct  Entry_t3344766165  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UnityEngine.EventSystems.EventTrigger/Entry::eventID
	int32_t ___eventID_0;
	// UnityEngine.EventSystems.EventTrigger/TriggerEvent UnityEngine.EventSystems.EventTrigger/Entry::callback
	TriggerEvent_t3867320123 * ___callback_1;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(Entry_t3344766165, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Entry_t3344766165, ___callback_1)); }
	inline TriggerEvent_t3867320123 * get_callback_1() const { return ___callback_1; }
	inline TriggerEvent_t3867320123 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(TriggerEvent_t3867320123 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3344766165_H
#ifndef XMLDATAINFERENCELOADER_T3104297037_H
#define XMLDATAINFERENCELOADER_T3104297037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.XmlDataInferenceLoader
struct  XmlDataInferenceLoader_t3104297037  : public RuntimeObject
{
public:
	// System.Data.DataSet System.Data.XmlDataInferenceLoader::dataset
	DataSet_t3773542243 * ___dataset_0;
	// System.Xml.XmlDocument System.Data.XmlDataInferenceLoader::document
	XmlDocument_t2837193595 * ___document_1;
	// System.Data.XmlReadMode System.Data.XmlDataInferenceLoader::mode
	int32_t ___mode_2;
	// System.Collections.ArrayList System.Data.XmlDataInferenceLoader::ignoredNamespaces
	ArrayList_t2718874744 * ___ignoredNamespaces_3;
	// System.Data.TableMappingCollection System.Data.XmlDataInferenceLoader::tables
	TableMappingCollection_t719748381 * ___tables_4;
	// System.Data.RelationStructureCollection System.Data.XmlDataInferenceLoader::relations
	RelationStructureCollection_t3303280033 * ___relations_5;

public:
	inline static int32_t get_offset_of_dataset_0() { return static_cast<int32_t>(offsetof(XmlDataInferenceLoader_t3104297037, ___dataset_0)); }
	inline DataSet_t3773542243 * get_dataset_0() const { return ___dataset_0; }
	inline DataSet_t3773542243 ** get_address_of_dataset_0() { return &___dataset_0; }
	inline void set_dataset_0(DataSet_t3773542243 * value)
	{
		___dataset_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataset_0), value);
	}

	inline static int32_t get_offset_of_document_1() { return static_cast<int32_t>(offsetof(XmlDataInferenceLoader_t3104297037, ___document_1)); }
	inline XmlDocument_t2837193595 * get_document_1() const { return ___document_1; }
	inline XmlDocument_t2837193595 ** get_address_of_document_1() { return &___document_1; }
	inline void set_document_1(XmlDocument_t2837193595 * value)
	{
		___document_1 = value;
		Il2CppCodeGenWriteBarrier((&___document_1), value);
	}

	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(XmlDataInferenceLoader_t3104297037, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_ignoredNamespaces_3() { return static_cast<int32_t>(offsetof(XmlDataInferenceLoader_t3104297037, ___ignoredNamespaces_3)); }
	inline ArrayList_t2718874744 * get_ignoredNamespaces_3() const { return ___ignoredNamespaces_3; }
	inline ArrayList_t2718874744 ** get_address_of_ignoredNamespaces_3() { return &___ignoredNamespaces_3; }
	inline void set_ignoredNamespaces_3(ArrayList_t2718874744 * value)
	{
		___ignoredNamespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___ignoredNamespaces_3), value);
	}

	inline static int32_t get_offset_of_tables_4() { return static_cast<int32_t>(offsetof(XmlDataInferenceLoader_t3104297037, ___tables_4)); }
	inline TableMappingCollection_t719748381 * get_tables_4() const { return ___tables_4; }
	inline TableMappingCollection_t719748381 ** get_address_of_tables_4() { return &___tables_4; }
	inline void set_tables_4(TableMappingCollection_t719748381 * value)
	{
		___tables_4 = value;
		Il2CppCodeGenWriteBarrier((&___tables_4), value);
	}

	inline static int32_t get_offset_of_relations_5() { return static_cast<int32_t>(offsetof(XmlDataInferenceLoader_t3104297037, ___relations_5)); }
	inline RelationStructureCollection_t3303280033 * get_relations_5() const { return ___relations_5; }
	inline RelationStructureCollection_t3303280033 ** get_address_of_relations_5() { return &___relations_5; }
	inline void set_relations_5(RelationStructureCollection_t3303280033 * value)
	{
		___relations_5 = value;
		Il2CppCodeGenWriteBarrier((&___relations_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATAINFERENCELOADER_T3104297037_H
#ifndef XELEMENT_T4185968754_H
#define XELEMENT_T4185968754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XElement
struct  XElement_t4185968754  : public XContainer_t3387731002
{
public:
	// System.Xml.Linq.XName System.Xml.Linq.XElement::name
	XName_t3751455998 * ___name_11;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement::attr_first
	XAttribute_t355876829 * ___attr_first_12;
	// System.Xml.Linq.XAttribute System.Xml.Linq.XElement::attr_last
	XAttribute_t355876829 * ___attr_last_13;
	// System.Boolean System.Xml.Linq.XElement::explicit_is_empty
	bool ___explicit_is_empty_14;

public:
	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(XElement_t4185968754, ___name_11)); }
	inline XName_t3751455998 * get_name_11() const { return ___name_11; }
	inline XName_t3751455998 ** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(XName_t3751455998 * value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier((&___name_11), value);
	}

	inline static int32_t get_offset_of_attr_first_12() { return static_cast<int32_t>(offsetof(XElement_t4185968754, ___attr_first_12)); }
	inline XAttribute_t355876829 * get_attr_first_12() const { return ___attr_first_12; }
	inline XAttribute_t355876829 ** get_address_of_attr_first_12() { return &___attr_first_12; }
	inline void set_attr_first_12(XAttribute_t355876829 * value)
	{
		___attr_first_12 = value;
		Il2CppCodeGenWriteBarrier((&___attr_first_12), value);
	}

	inline static int32_t get_offset_of_attr_last_13() { return static_cast<int32_t>(offsetof(XElement_t4185968754, ___attr_last_13)); }
	inline XAttribute_t355876829 * get_attr_last_13() const { return ___attr_last_13; }
	inline XAttribute_t355876829 ** get_address_of_attr_last_13() { return &___attr_last_13; }
	inline void set_attr_last_13(XAttribute_t355876829 * value)
	{
		___attr_last_13 = value;
		Il2CppCodeGenWriteBarrier((&___attr_last_13), value);
	}

	inline static int32_t get_offset_of_explicit_is_empty_14() { return static_cast<int32_t>(offsetof(XElement_t4185968754, ___explicit_is_empty_14)); }
	inline bool get_explicit_is_empty_14() const { return ___explicit_is_empty_14; }
	inline bool* get_address_of_explicit_is_empty_14() { return &___explicit_is_empty_14; }
	inline void set_explicit_is_empty_14(bool value)
	{
		___explicit_is_empty_14 = value;
	}
};

struct XElement_t4185968754_StaticFields
{
public:
	// System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XElement::emptySequence
	RuntimeObject* ___emptySequence_10;

public:
	inline static int32_t get_offset_of_emptySequence_10() { return static_cast<int32_t>(offsetof(XElement_t4185968754_StaticFields, ___emptySequence_10)); }
	inline RuntimeObject* get_emptySequence_10() const { return ___emptySequence_10; }
	inline RuntimeObject** get_address_of_emptySequence_10() { return &___emptySequence_10; }
	inline void set_emptySequence_10(RuntimeObject* value)
	{
		___emptySequence_10 = value;
		Il2CppCodeGenWriteBarrier((&___emptySequence_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENT_T4185968754_H
#ifndef DATAROWCHANGEEVENTHANDLER_T1758895939_H
#define DATAROWCHANGEEVENTHANDLER_T1758895939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowChangeEventHandler
struct  DataRowChangeEventHandler_t1758895939  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWCHANGEEVENTHANDLER_T1758895939_H
#ifndef DATACOLUMNCHANGEEVENTHANDLER_T1943750078_H
#define DATACOLUMNCHANGEEVENTHANDLER_T1943750078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnChangeEventHandler
struct  DataColumnChangeEventHandler_t1943750078  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNCHANGEEVENTHANDLER_T1943750078_H
#ifndef DELEGATECONSTRAINTNAMECHANGE_T3435241820_H
#define DELEGATECONSTRAINTNAMECHANGE_T3435241820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DelegateConstraintNameChange
struct  DelegateConstraintNameChange_t3435241820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATECONSTRAINTNAMECHANGE_T3435241820_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef DATATABLENEWROWEVENTHANDLER_T4233335338_H
#define DATATABLENEWROWEVENTHANDLER_T4233335338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataTableNewRowEventHandler
struct  DataTableNewRowEventHandler_t4233335338  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLENEWROWEVENTHANDLER_T4233335338_H
#ifndef MERGEFAILEDEVENTHANDLER_T645599195_H
#define MERGEFAILEDEVENTHANDLER_T645599195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.MergeFailedEventHandler
struct  MergeFailedEventHandler_t645599195  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEFAILEDEVENTHANDLER_T645599195_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef EVENTTRIGGER_T1076084509_H
#define EVENTTRIGGER_T1076084509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger
struct  EventTrigger_t1076084509  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry> UnityEngine.EventSystems.EventTrigger::m_Delegates
	List_1_t521873611 * ___m_Delegates_2;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry> UnityEngine.EventSystems.EventTrigger::delegates
	List_1_t521873611 * ___delegates_3;

public:
	inline static int32_t get_offset_of_m_Delegates_2() { return static_cast<int32_t>(offsetof(EventTrigger_t1076084509, ___m_Delegates_2)); }
	inline List_1_t521873611 * get_m_Delegates_2() const { return ___m_Delegates_2; }
	inline List_1_t521873611 ** get_address_of_m_Delegates_2() { return &___m_Delegates_2; }
	inline void set_m_Delegates_2(List_1_t521873611 * value)
	{
		___m_Delegates_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegates_2), value);
	}

	inline static int32_t get_offset_of_delegates_3() { return static_cast<int32_t>(offsetof(EventTrigger_t1076084509, ___delegates_3)); }
	inline List_1_t521873611 * get_delegates_3() const { return ___delegates_3; }
	inline List_1_t521873611 ** get_address_of_delegates_3() { return &___delegates_3; }
	inline void set_delegates_3(List_1_t521873611 * value)
	{
		___delegates_3 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T1076084509_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_2)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_3)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_5)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_6)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_7)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef BASEINPUT_T3630163547_H
#define BASEINPUT_T3630163547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInput
struct  BaseInput_t3630163547  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_T3630163547_H
#ifndef EVENTSYSTEM_T1003666588_H
#define EVENTSYSTEM_T1003666588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t1003666588  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t3491343620 * ___m_SystemInputModules_2;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t2019268878 * ___m_CurrentInputModule_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1113636619 * ___m_FirstSelected_5;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_6;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_7;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1113636619 * ___m_CurrentSelected_8;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_9;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_10;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t3903027533 * ___m_DummyData_11;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_2() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SystemInputModules_2)); }
	inline List_1_t3491343620 * get_m_SystemInputModules_2() const { return ___m_SystemInputModules_2; }
	inline List_1_t3491343620 ** get_address_of_m_SystemInputModules_2() { return &___m_SystemInputModules_2; }
	inline void set_m_SystemInputModules_2(List_1_t3491343620 * value)
	{
		___m_SystemInputModules_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_3() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentInputModule_3)); }
	inline BaseInputModule_t2019268878 * get_m_CurrentInputModule_3() const { return ___m_CurrentInputModule_3; }
	inline BaseInputModule_t2019268878 ** get_address_of_m_CurrentInputModule_3() { return &___m_CurrentInputModule_3; }
	inline void set_m_CurrentInputModule_3(BaseInputModule_t2019268878 * value)
	{
		___m_CurrentInputModule_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_3), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_5() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_FirstSelected_5)); }
	inline GameObject_t1113636619 * get_m_FirstSelected_5() const { return ___m_FirstSelected_5; }
	inline GameObject_t1113636619 ** get_address_of_m_FirstSelected_5() { return &___m_FirstSelected_5; }
	inline void set_m_FirstSelected_5(GameObject_t1113636619 * value)
	{
		___m_FirstSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_5), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_6() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_sendNavigationEvents_6)); }
	inline bool get_m_sendNavigationEvents_6() const { return ___m_sendNavigationEvents_6; }
	inline bool* get_address_of_m_sendNavigationEvents_6() { return &___m_sendNavigationEvents_6; }
	inline void set_m_sendNavigationEvents_6(bool value)
	{
		___m_sendNavigationEvents_6 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_7() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DragThreshold_7)); }
	inline int32_t get_m_DragThreshold_7() const { return ___m_DragThreshold_7; }
	inline int32_t* get_address_of_m_DragThreshold_7() { return &___m_DragThreshold_7; }
	inline void set_m_DragThreshold_7(int32_t value)
	{
		___m_DragThreshold_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_8() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentSelected_8)); }
	inline GameObject_t1113636619 * get_m_CurrentSelected_8() const { return ___m_CurrentSelected_8; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentSelected_8() { return &___m_CurrentSelected_8; }
	inline void set_m_CurrentSelected_8(GameObject_t1113636619 * value)
	{
		___m_CurrentSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_8), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_9() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_HasFocus_9)); }
	inline bool get_m_HasFocus_9() const { return ___m_HasFocus_9; }
	inline bool* get_address_of_m_HasFocus_9() { return &___m_HasFocus_9; }
	inline void set_m_HasFocus_9(bool value)
	{
		___m_HasFocus_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_10() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SelectionGuard_10)); }
	inline bool get_m_SelectionGuard_10() const { return ___m_SelectionGuard_10; }
	inline bool* get_address_of_m_SelectionGuard_10() { return &___m_SelectionGuard_10; }
	inline void set_m_SelectionGuard_10(bool value)
	{
		___m_SelectionGuard_10 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_11() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DummyData_11)); }
	inline BaseEventData_t3903027533 * get_m_DummyData_11() const { return ___m_DummyData_11; }
	inline BaseEventData_t3903027533 ** get_address_of_m_DummyData_11() { return &___m_DummyData_11; }
	inline void set_m_DummyData_11(BaseEventData_t3903027533 * value)
	{
		___m_DummyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_11), value);
	}
};

struct EventSystem_t1003666588_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2475741330 * ___m_EventSystems_4;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t3135238028 * ___s_RaycastComparer_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_m_EventSystems_4() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___m_EventSystems_4)); }
	inline List_1_t2475741330 * get_m_EventSystems_4() const { return ___m_EventSystems_4; }
	inline List_1_t2475741330 ** get_address_of_m_EventSystems_4() { return &___m_EventSystems_4; }
	inline void set_m_EventSystems_4(List_1_t2475741330 * value)
	{
		___m_EventSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_4), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_12() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___s_RaycastComparer_12)); }
	inline Comparison_1_t3135238028 * get_s_RaycastComparer_12() const { return ___s_RaycastComparer_12; }
	inline Comparison_1_t3135238028 ** get_address_of_s_RaycastComparer_12() { return &___s_RaycastComparer_12; }
	inline void set_s_RaycastComparer_12(Comparison_1_t3135238028 * value)
	{
		___s_RaycastComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T1003666588_H
#ifndef POINTERINPUTMODULE_T3453173740_H
#define POINTERINPUTMODULE_T3453173740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t3453173740  : public BaseInputModule_t2019268878
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t2696614423 * ___m_PointerData_12;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t384203932 * ___m_MouseState_13;

public:
	inline static int32_t get_offset_of_m_PointerData_12() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_PointerData_12)); }
	inline Dictionary_2_t2696614423 * get_m_PointerData_12() const { return ___m_PointerData_12; }
	inline Dictionary_2_t2696614423 ** get_address_of_m_PointerData_12() { return &___m_PointerData_12; }
	inline void set_m_PointerData_12(Dictionary_2_t2696614423 * value)
	{
		___m_PointerData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_12), value);
	}

	inline static int32_t get_offset_of_m_MouseState_13() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_MouseState_13)); }
	inline MouseState_t384203932 * get_m_MouseState_13() const { return ___m_MouseState_13; }
	inline MouseState_t384203932 ** get_address_of_m_MouseState_13() { return &___m_MouseState_13; }
	inline void set_m_MouseState_13(MouseState_t384203932 * value)
	{
		___m_MouseState_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T3453173740_H
#ifndef STANDALONEINPUTMODULE_T2760469101_H
#define STANDALONEINPUTMODULE_T2760469101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.StandaloneInputModule
struct  StandaloneInputModule_t2760469101  : public PointerInputModule_t3453173740
{
public:
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_PrevActionTime
	float ___m_PrevActionTime_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMoveVector
	Vector2_t2156229523  ___m_LastMoveVector_15;
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMousePosition
	Vector2_t2156229523  ___m_LastMousePosition_17;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_MousePosition
	Vector2_t2156229523  ___m_MousePosition_18;
	// UnityEngine.GameObject UnityEngine.EventSystems.StandaloneInputModule::m_CurrentFocusedGameObject
	GameObject_t1113636619 * ___m_CurrentFocusedGameObject_19;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_20;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_21;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_22;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_CancelButton
	String_t* ___m_CancelButton_23;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_24;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_RepeatDelay
	float ___m_RepeatDelay_25;
	// System.Boolean UnityEngine.EventSystems.StandaloneInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_26;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_14() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_PrevActionTime_14)); }
	inline float get_m_PrevActionTime_14() const { return ___m_PrevActionTime_14; }
	inline float* get_address_of_m_PrevActionTime_14() { return &___m_PrevActionTime_14; }
	inline void set_m_PrevActionTime_14(float value)
	{
		___m_PrevActionTime_14 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_15() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMoveVector_15)); }
	inline Vector2_t2156229523  get_m_LastMoveVector_15() const { return ___m_LastMoveVector_15; }
	inline Vector2_t2156229523 * get_address_of_m_LastMoveVector_15() { return &___m_LastMoveVector_15; }
	inline void set_m_LastMoveVector_15(Vector2_t2156229523  value)
	{
		___m_LastMoveVector_15 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_16() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ConsecutiveMoveCount_16)); }
	inline int32_t get_m_ConsecutiveMoveCount_16() const { return ___m_ConsecutiveMoveCount_16; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_16() { return &___m_ConsecutiveMoveCount_16; }
	inline void set_m_ConsecutiveMoveCount_16(int32_t value)
	{
		___m_ConsecutiveMoveCount_16 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_17() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMousePosition_17)); }
	inline Vector2_t2156229523  get_m_LastMousePosition_17() const { return ___m_LastMousePosition_17; }
	inline Vector2_t2156229523 * get_address_of_m_LastMousePosition_17() { return &___m_LastMousePosition_17; }
	inline void set_m_LastMousePosition_17(Vector2_t2156229523  value)
	{
		___m_LastMousePosition_17 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_18() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_MousePosition_18)); }
	inline Vector2_t2156229523  get_m_MousePosition_18() const { return ___m_MousePosition_18; }
	inline Vector2_t2156229523 * get_address_of_m_MousePosition_18() { return &___m_MousePosition_18; }
	inline void set_m_MousePosition_18(Vector2_t2156229523  value)
	{
		___m_MousePosition_18 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFocusedGameObject_19() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CurrentFocusedGameObject_19)); }
	inline GameObject_t1113636619 * get_m_CurrentFocusedGameObject_19() const { return ___m_CurrentFocusedGameObject_19; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentFocusedGameObject_19() { return &___m_CurrentFocusedGameObject_19; }
	inline void set_m_CurrentFocusedGameObject_19(GameObject_t1113636619 * value)
	{
		___m_CurrentFocusedGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentFocusedGameObject_19), value);
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_20() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_HorizontalAxis_20)); }
	inline String_t* get_m_HorizontalAxis_20() const { return ___m_HorizontalAxis_20; }
	inline String_t** get_address_of_m_HorizontalAxis_20() { return &___m_HorizontalAxis_20; }
	inline void set_m_HorizontalAxis_20(String_t* value)
	{
		___m_HorizontalAxis_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_20), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_21() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_VerticalAxis_21)); }
	inline String_t* get_m_VerticalAxis_21() const { return ___m_VerticalAxis_21; }
	inline String_t** get_address_of_m_VerticalAxis_21() { return &___m_VerticalAxis_21; }
	inline void set_m_VerticalAxis_21(String_t* value)
	{
		___m_VerticalAxis_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_21), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_22() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_SubmitButton_22)); }
	inline String_t* get_m_SubmitButton_22() const { return ___m_SubmitButton_22; }
	inline String_t** get_address_of_m_SubmitButton_22() { return &___m_SubmitButton_22; }
	inline void set_m_SubmitButton_22(String_t* value)
	{
		___m_SubmitButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_22), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_23() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CancelButton_23)); }
	inline String_t* get_m_CancelButton_23() const { return ___m_CancelButton_23; }
	inline String_t** get_address_of_m_CancelButton_23() { return &___m_CancelButton_23; }
	inline void set_m_CancelButton_23(String_t* value)
	{
		___m_CancelButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_23), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_24() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_InputActionsPerSecond_24)); }
	inline float get_m_InputActionsPerSecond_24() const { return ___m_InputActionsPerSecond_24; }
	inline float* get_address_of_m_InputActionsPerSecond_24() { return &___m_InputActionsPerSecond_24; }
	inline void set_m_InputActionsPerSecond_24(float value)
	{
		___m_InputActionsPerSecond_24 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_25() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_RepeatDelay_25)); }
	inline float get_m_RepeatDelay_25() const { return ___m_RepeatDelay_25; }
	inline float* get_address_of_m_RepeatDelay_25() { return &___m_RepeatDelay_25; }
	inline void set_m_RepeatDelay_25(float value)
	{
		___m_RepeatDelay_25 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_26() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ForceModuleActive_26)); }
	inline bool get_m_ForceModuleActive_26() const { return ___m_ForceModuleActive_26; }
	inline bool* get_address_of_m_ForceModuleActive_26() { return &___m_ForceModuleActive_26; }
	inline void set_m_ForceModuleActive_26(bool value)
	{
		___m_ForceModuleActive_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUTMODULE_T2760469101_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (XmlDataInferenceLoader_t3104297037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[6] = 
{
	XmlDataInferenceLoader_t3104297037::get_offset_of_dataset_0(),
	XmlDataInferenceLoader_t3104297037::get_offset_of_document_1(),
	XmlDataInferenceLoader_t3104297037::get_offset_of_mode_2(),
	XmlDataInferenceLoader_t3104297037::get_offset_of_ignoredNamespaces_3(),
	XmlDataInferenceLoader_t3104297037::get_offset_of_tables_4(),
	XmlDataInferenceLoader_t3104297037::get_offset_of_relations_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (XmlDataLoader_t1358168730), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (XmlDataReader_t4085121624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[3] = 
{
	XmlDataReader_t4085121624::get_offset_of_dataset_0(),
	XmlDataReader_t4085121624::get_offset_of_reader_1(),
	XmlDataReader_t4085121624::get_offset_of_mode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (XmlDiffLoader_t2749071105), -1, sizeof(XmlDiffLoader_t2749071105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2403[5] = 
{
	XmlDiffLoader_t2749071105::get_offset_of_DSet_0(),
	XmlDiffLoader_t2749071105::get_offset_of_table_1(),
	XmlDiffLoader_t2749071105::get_offset_of_DiffGrRows_2(),
	XmlDiffLoader_t2749071105::get_offset_of_ErrorRows_3(),
	XmlDiffLoader_t2749071105_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (XmlHelper_t906262798), -1, sizeof(XmlHelper_t906262798_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2404[2] = 
{
	XmlHelper_t906262798_StaticFields::get_offset_of_localSchemaNameCache_0(),
	XmlHelper_t906262798_StaticFields::get_offset_of_localXmlNameCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (XmlReadMode_t173510697)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2405[8] = 
{
	XmlReadMode_t173510697::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (RelationStructureCollection_t3303280033), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (TableStructure_t4158950525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[4] = 
{
	TableStructure_t4158950525::get_offset_of_Table_0(),
	TableStructure_t4158950525::get_offset_of_OrdinalColumns_1(),
	TableStructure_t4158950525::get_offset_of_NonOrdinalColumns_2(),
	TableStructure_t4158950525::get_offset_of_PrimaryKey_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (RelationStructure_t1029267685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[7] = 
{
	RelationStructure_t1029267685::get_offset_of_ExplicitName_0(),
	RelationStructure_t1029267685::get_offset_of_ParentTableName_1(),
	RelationStructure_t1029267685::get_offset_of_ChildTableName_2(),
	RelationStructure_t1029267685::get_offset_of_ParentColumnName_3(),
	RelationStructure_t1029267685::get_offset_of_ChildColumnName_4(),
	RelationStructure_t1029267685::get_offset_of_IsNested_5(),
	RelationStructure_t1029267685::get_offset_of_CreateConstraint_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (ConstraintStructure_t3230168582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[8] = 
{
	ConstraintStructure_t3230168582::get_offset_of_TableName_0(),
	ConstraintStructure_t3230168582::get_offset_of_Columns_1(),
	ConstraintStructure_t3230168582::get_offset_of_IsAttribute_2(),
	ConstraintStructure_t3230168582::get_offset_of_ConstraintName_3(),
	ConstraintStructure_t3230168582::get_offset_of_IsPrimaryKey_4(),
	ConstraintStructure_t3230168582::get_offset_of_ReferName_5(),
	ConstraintStructure_t3230168582::get_offset_of_IsNested_6(),
	ConstraintStructure_t3230168582::get_offset_of_IsConstraintOnly_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (XmlSchemaDataImporter_t2556819020), -1, sizeof(XmlSchemaDataImporter_t2556819020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2410[19] = 
{
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_schemaIntegerType_0(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_schemaDecimalType_1(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_schemaAnyType_2(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_dataset_3(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_forDataSet_4(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_schema_5(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_relations_6(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_reservedConstraints_7(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_datasetElement_8(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_topLevelElements_9(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_targetElements_10(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_currentTable_11(),
	XmlSchemaDataImporter_t2556819020::get_offset_of_currentAdapter_12(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_13(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_14(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_15(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_16(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_17(),
	XmlSchemaDataImporter_t2556819020_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (XmlSchemaWriter_t4200114175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[11] = 
{
	XmlSchemaWriter_t4200114175::get_offset_of_w_0(),
	XmlSchemaWriter_t4200114175::get_offset_of_tables_1(),
	XmlSchemaWriter_t4200114175::get_offset_of_relations_2(),
	XmlSchemaWriter_t4200114175::get_offset_of_mainDataTable_3(),
	XmlSchemaWriter_t4200114175::get_offset_of_dataSetName_4(),
	XmlSchemaWriter_t4200114175::get_offset_of_dataSetNamespace_5(),
	XmlSchemaWriter_t4200114175::get_offset_of_dataSetProperties_6(),
	XmlSchemaWriter_t4200114175::get_offset_of_dataSetLocale_7(),
	XmlSchemaWriter_t4200114175::get_offset_of_globalTypeTables_8(),
	XmlSchemaWriter_t4200114175::get_offset_of_additionalNamespaces_9(),
	XmlSchemaWriter_t4200114175::get_offset_of_annotation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (XmlTableWriter_t2048085353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (XmlWriteMode_t421248975)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[4] = 
{
	XmlWriteMode_t421248975::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (DelegateConstraintNameChange_t3435241820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (DataColumnChangeEventHandler_t1943750078), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (DataRowChangeEventHandler_t1758895939), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (DataTableNewRowEventHandler_t4233335338), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (MergeFailedEventHandler_t645599195), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2419[9] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (U24ArrayTypeU24168_t758788411)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24168_t758788411 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (U24ArrayTypeU24328_t1156913777)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24328_t1156913777 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (U24ArrayTypeU2464_t499776627)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2464_t499776627 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (U24ArrayTypeU241084_t3907531067)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU241084_t3907531067 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (U24ArrayTypeU2440_t2867270459)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2440_t2867270459 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (DataContractAttribute_t412496005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[1] = 
{
	DataContractAttribute_t412496005::get_offset_of_U3CIsReferenceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (DataMemberAttribute_t1339837472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[4] = 
{
	DataMemberAttribute_t1339837472::get_offset_of_is_required_0(),
	DataMemberAttribute_t1339837472::get_offset_of_emit_default_1(),
	DataMemberAttribute_t1339837472::get_offset_of_name_2(),
	DataMemberAttribute_t1339837472::get_offset_of_order_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (EnumMemberAttribute_t1084128815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[1] = 
{
	EnumMemberAttribute_t1084128815::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (LoadOptions_t4182560034)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2430[5] = 
{
	LoadOptions_t4182560034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (SaveOptions_t2609998520)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2431[3] = 
{
	SaveOptions_t2609998520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (XAttribute_t355876829), -1, sizeof(XAttribute_t355876829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[6] = 
{
	XAttribute_t355876829_StaticFields::get_offset_of_empty_array_4(),
	XAttribute_t355876829::get_offset_of_name_5(),
	XAttribute_t355876829::get_offset_of_value_6(),
	XAttribute_t355876829::get_offset_of_next_7(),
	XAttribute_t355876829::get_offset_of_previous_8(),
	XAttribute_t355876829_StaticFields::get_offset_of_escapeChars_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (XCData_t832623270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (XComment_t1832251191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[1] = 
{
	XComment_t1832251191::get_offset_of_value_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (XContainer_t3387731002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[2] = 
{
	XContainer_t3387731002::get_offset_of_first_8(),
	XContainer_t3387731002::get_offset_of_last_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (U3CNodesU3Ec__Iterator1A_t2001237956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[5] = 
{
	U3CNodesU3Ec__Iterator1A_t2001237956::get_offset_of_U3CnU3E__0_0(),
	U3CNodesU3Ec__Iterator1A_t2001237956::get_offset_of_U3CnextU3E__1_1(),
	U3CNodesU3Ec__Iterator1A_t2001237956::get_offset_of_U24PC_2(),
	U3CNodesU3Ec__Iterator1A_t2001237956::get_offset_of_U24current_3(),
	U3CNodesU3Ec__Iterator1A_t2001237956::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (XDeclaration_t2907650823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[3] = 
{
	XDeclaration_t2907650823::get_offset_of_encoding_0(),
	XDeclaration_t2907650823::get_offset_of_standalone_1(),
	XDeclaration_t2907650823::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (XDocument_t1376419702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	XDocument_t1376419702::get_offset_of_xmldecl_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (XDocumentType_t1853592271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[4] = 
{
	XDocumentType_t1853592271::get_offset_of_name_8(),
	XDocumentType_t1853592271::get_offset_of_pubid_9(),
	XDocumentType_t1853592271::get_offset_of_sysid_10(),
	XDocumentType_t1853592271::get_offset_of_intSubset_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (XElement_t4185968754), -1, sizeof(XElement_t4185968754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2440[5] = 
{
	XElement_t4185968754_StaticFields::get_offset_of_emptySequence_10(),
	XElement_t4185968754::get_offset_of_name_11(),
	XElement_t4185968754::get_offset_of_attr_first_12(),
	XElement_t4185968754::get_offset_of_attr_last_13(),
	XElement_t4185968754::get_offset_of_explicit_is_empty_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (U3CAttributesU3Ec__Iterator20_t3836567394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[5] = 
{
	U3CAttributesU3Ec__Iterator20_t3836567394::get_offset_of_U3CaU3E__0_0(),
	U3CAttributesU3Ec__Iterator20_t3836567394::get_offset_of_U3CnextU3E__1_1(),
	U3CAttributesU3Ec__Iterator20_t3836567394::get_offset_of_U24PC_2(),
	U3CAttributesU3Ec__Iterator20_t3836567394::get_offset_of_U24current_3(),
	U3CAttributesU3Ec__Iterator20_t3836567394::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[8] = 
{
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_U3CelU3E__0_0(),
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_U3CU24s_68U3E__1_1(),
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_U3CaU3E__2_2(),
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_ns_3(),
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_U24PC_4(),
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_U24current_5(),
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_U3CU24U3Ens_6(),
	U3CGetPrefixOfNamespaceCoreU3Ec__Iterator23_t674090361::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (XName_t3751455998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[2] = 
{
	XName_t3751455998::get_offset_of_local_0(),
	XName_t3751455998::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (XNamespace_t1843508170), -1, sizeof(XNamespace_t1843508170_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[6] = 
{
	XNamespace_t1843508170_StaticFields::get_offset_of_blank_0(),
	XNamespace_t1843508170_StaticFields::get_offset_of_xml_1(),
	XNamespace_t1843508170_StaticFields::get_offset_of_xmlns_2(),
	XNamespace_t1843508170_StaticFields::get_offset_of_nstable_3(),
	XNamespace_t1843508170::get_offset_of_uri_4(),
	XNamespace_t1843508170::get_offset_of_table_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (XNode_t716131460), -1, sizeof(XNode_t716131460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[4] = 
{
	XNode_t716131460_StaticFields::get_offset_of_eq_comparer_4(),
	XNode_t716131460_StaticFields::get_offset_of_order_comparer_5(),
	XNode_t716131460::get_offset_of_previous_6(),
	XNode_t716131460::get_offset_of_next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (XNodeDocumentOrderComparer_t3558329258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (CompareResult_t200929839)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[9] = 
{
	CompareResult_t200929839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (XNodeEqualityComparer_t35321779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (XObject_t1119084474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[4] = 
{
	XObject_t1119084474::get_offset_of_owner_0(),
	XObject_t1119084474::get_offset_of_baseuri_1(),
	XObject_t1119084474::get_offset_of_line_2(),
	XObject_t1119084474::get_offset_of_column_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (XProcessingInstruction_t2943691487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[2] = 
{
	XProcessingInstruction_t2943691487::get_offset_of_name_8(),
	XProcessingInstruction_t2943691487::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (XText_t1448860321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[1] = 
{
	XText_t1448860321::get_offset_of_value_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (XUtil_t154644382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (U3CExpandArrayU3Ec__Iterator25_t2040603384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[9] = 
{
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_o_0(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U3CnU3E__0_1(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U3CU24s_86U3E__1_2(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U3CobjU3E__2_3(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U3CU24s_87U3E__3_4(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U3CooU3E__4_5(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U24PC_6(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U24current_7(),
	U3CExpandArrayU3Ec__Iterator25_t2040603384::get_offset_of_U3CU24U3Eo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (U24ArrayTypeU2416_t3254766647)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2416_t3254766647 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (EventHandle_t600343995)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2457[3] = 
{
	EventHandle_t600343995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (EventSystem_t1003666588), -1, sizeof(EventSystem_t1003666588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2476[12] = 
{
	EventSystem_t1003666588::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t1003666588::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t1003666588_StaticFields::get_offset_of_m_EventSystems_4(),
	EventSystem_t1003666588::get_offset_of_m_FirstSelected_5(),
	EventSystem_t1003666588::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t1003666588::get_offset_of_m_DragThreshold_7(),
	EventSystem_t1003666588::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t1003666588::get_offset_of_m_HasFocus_9(),
	EventSystem_t1003666588::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t1003666588::get_offset_of_m_DummyData_11(),
	EventSystem_t1003666588_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (EventTrigger_t1076084509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[2] = 
{
	EventTrigger_t1076084509::get_offset_of_m_Delegates_2(),
	EventTrigger_t1076084509::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (TriggerEvent_t3867320123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (Entry_t3344766165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[2] = 
{
	Entry_t3344766165::get_offset_of_eventID_0(),
	Entry_t3344766165::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (EventTriggerType_t55832929)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2480[18] = 
{
	EventTriggerType_t55832929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (ExecuteEvents_t3484638744), -1, sizeof(ExecuteEvents_t3484638744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2481[36] = 
{
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (MoveDirection_t1216237838)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2483[6] = 
{
	MoveDirection_t1216237838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (RaycasterManager_t2536340562), -1, sizeof(RaycasterManager_t2536340562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2484[1] = 
{
	RaycasterManager_t2536340562_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (RaycastResult_t3360306849)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[10] = 
{
	RaycastResult_t3360306849::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_module_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_index_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (UIBehaviour_t3495933518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (AxisEventData_t2331243652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[2] = 
{
	AxisEventData_t2331243652::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t2331243652::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (AbstractEventData_t4171500731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[1] = 
{
	AbstractEventData_t4171500731::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (BaseEventData_t3903027533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[1] = 
{
	BaseEventData_t3903027533::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (PointerEventData_t3807901092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[21] = 
{
	PointerEventData_t3807901092::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t3807901092::get_offset_of_m_PointerPress_3(),
	PointerEventData_t3807901092::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t3807901092::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t3807901092::get_offset_of_hovered_9(),
	PointerEventData_t3807901092::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t3807901092::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t3807901092::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t3807901092::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t3807901092::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t3807901092::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t3807901092::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t3807901092::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t3807901092::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t3807901092::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t3807901092::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t3807901092::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (InputButton_t3704011348)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2491[4] = 
{
	InputButton_t3704011348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (FramePressState_t3039385657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2492[5] = 
{
	FramePressState_t3039385657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (BaseInput_t3630163547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (BaseInputModule_t2019268878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[6] = 
{
	BaseInputModule_t2019268878::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t2019268878::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t2019268878::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t2019268878::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t2019268878::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t2019268878::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (PointerInputModule_t3453173740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3453173740::get_offset_of_m_PointerData_12(),
	PointerInputModule_t3453173740::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (ButtonState_t857139936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[2] = 
{
	ButtonState_t857139936::get_offset_of_m_Button_0(),
	ButtonState_t857139936::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (MouseState_t384203932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[1] = 
{
	MouseState_t384203932::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (MouseButtonEventData_t3190347560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[2] = 
{
	MouseButtonEventData_t3190347560::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3190347560::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (StandaloneInputModule_t2760469101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[13] = 
{
	StandaloneInputModule_t2760469101::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t2760469101::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t2760469101::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t2760469101::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t2760469101::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t2760469101::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ForceModuleActive_26(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
