﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_t139615908;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlNodeListChildren
struct XmlNodeListChildren_t1082692789;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Data.DataRow[]
struct DataRowU5BU5D_t3122106530;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// Mono.Data.SqlExpressions.yydebug.yyDebug
struct yyDebug_t3389324927;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int16[]
struct Int16U5BU5D_t3686840178;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t3314986516;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.String
struct String_t;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1809665003;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t2186285234;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Data.Common.Key
struct Key_t1531728703;
// System.Type
struct Type_t;
// System.Collections.BitArray
struct BitArray_t4087883509;
// System.Data.DataColumn
struct DataColumn_t2485786120;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Globalization.CompareInfo
struct CompareInfo_t1092934962;
// System.Data.Common.DataColumnMappingCollection
struct DataColumnMappingCollection_t2426444188;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t3455035481;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// Mono.Data.SqlExpressions.IExpression
struct IExpression_t1903229978;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// System.SByte[]
struct SByteU5BU5D_t2651576203;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.UInt64[]
struct UInt64U5BU5D_t1659327989;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Net.ICredentials
struct ICredentials_t725721261;
// System.Void
struct Void_t1185182177;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.ComponentModel.ISite
struct ISite_t4006303512;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Data.Common.DataTableMappingCollection
struct DataTableMappingCollection_t2364717775;
// System.Collections.IList
struct IList_t2094931216;
// Mono.Data.SqlExpressions.ColumnReference
struct ColumnReference_t94354283;
// System.IConvertible
struct IConvertible_t2977365677;
// System.Data.DataRowChangeEventHandler
struct DataRowChangeEventHandler_t1758895939;
// System.Data.DataTable
struct DataTable_t1734486485;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.StringWriter
struct StringWriter_t802263757;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.Xml.XmlTextWriter/XmlNodeInfo[]
struct XmlNodeInfoU5BU5D_t431163322;
// System.Data.IDbCommand
struct IDbCommand_t2192592793;
// System.Data.DataRelation
struct DataRelation_t2257224951;
// Mono.Xml2.XmlTextReader/XmlTokenInfo
struct XmlTokenInfo_t2519673037;
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo
struct XmlAttributeTokenInfo_t384315108;
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo[]
struct XmlAttributeTokenInfoU5BU5D_t1089758477;
// Mono.Xml2.XmlTextReader/XmlTokenInfo[]
struct XmlTokenInfoU5BU5D_t2522587488;
// System.Xml.XmlParserContext
struct XmlParserContext_t2544895291;
// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;
// Mono.Xml2.XmlTextReader/TagName[]
struct TagNameU5BU5D_t2055670886;
// System.IO.TextReader
struct TextReader_t283511965;
// System.Xml.XmlReaderBinarySupport/CharGetter
struct CharGetter_t1703763694;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Xml.NameTable
struct NameTable_t3178203267;
// Mono.Xml2.XmlTextReader/DtdInputStateStack
struct DtdInputStateStack_t339956957;
// System.Xml.Schema.ValidationEventArgs
struct ValidationEventArgs_t2784773869;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Xml.XmlNodeChangedEventArgs
struct XmlNodeChangedEventArgs_t2486095928;




#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NUMERIC_T3836385610_H
#define NUMERIC_T3836385610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Numeric
struct  Numeric_t3836385610  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERIC_T3836385610_H
#ifndef XMLNODE_T3767805227_H
#define XMLNODE_T3767805227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t3767805227  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNode::ownerDocument
	XmlDocument_t2837193595 * ___ownerDocument_1;
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t3767805227 * ___parentNode_2;
	// System.Xml.XmlNodeListChildren System.Xml.XmlNode::childNodes
	XmlNodeListChildren_t1082692789 * ___childNodes_3;

public:
	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___ownerDocument_1)); }
	inline XmlDocument_t2837193595 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline XmlDocument_t2837193595 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(XmlDocument_t2837193595 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parentNode_2() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___parentNode_2)); }
	inline XmlNode_t3767805227 * get_parentNode_2() const { return ___parentNode_2; }
	inline XmlNode_t3767805227 ** get_address_of_parentNode_2() { return &___parentNode_2; }
	inline void set_parentNode_2(XmlNode_t3767805227 * value)
	{
		___parentNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_2), value);
	}

	inline static int32_t get_offset_of_childNodes_3() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___childNodes_3)); }
	inline XmlNodeListChildren_t1082692789 * get_childNodes_3() const { return ___childNodes_3; }
	inline XmlNodeListChildren_t1082692789 ** get_address_of_childNodes_3() { return &___childNodes_3; }
	inline void set_childNodes_3(XmlNodeListChildren_t1082692789 * value)
	{
		___childNodes_3 = value;
		Il2CppCodeGenWriteBarrier((&___childNodes_3), value);
	}
};

struct XmlNode_t3767805227_StaticFields
{
public:
	// System.Xml.XmlNode/EmptyNodeList System.Xml.XmlNode::emptyList
	EmptyNodeList_t139615908 * ___emptyList_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map44
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map44_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map46
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map46_5;

public:
	inline static int32_t get_offset_of_emptyList_0() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___emptyList_0)); }
	inline EmptyNodeList_t139615908 * get_emptyList_0() const { return ___emptyList_0; }
	inline EmptyNodeList_t139615908 ** get_address_of_emptyList_0() { return &___emptyList_0; }
	inline void set_emptyList_0(EmptyNodeList_t139615908 * value)
	{
		___emptyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map44_4() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___U3CU3Ef__switchU24map44_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map44_4() const { return ___U3CU3Ef__switchU24map44_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map44_4() { return &___U3CU3Ef__switchU24map44_4; }
	inline void set_U3CU3Ef__switchU24map44_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map44_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map44_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map46_5() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___U3CU3Ef__switchU24map46_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map46_5() const { return ___U3CU3Ef__switchU24map46_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map46_5() { return &___U3CU3Ef__switchU24map46_5; }
	inline void set_U3CU3Ef__switchU24map46_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map46_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map46_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T3767805227_H
#ifndef BASEEXPRESSION_T4150592444_H
#define BASEEXPRESSION_T4150592444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.BaseExpression
struct  BaseExpression_t4150592444  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEXPRESSION_T4150592444_H
#ifndef LOCALE_T4128636111_H
#define LOCALE_T4128636111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Locale
struct  Locale_t4128636111  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALE_T4128636111_H
#ifndef YYDEBUGSIMPLE_T3881228540_H
#define YYDEBUGSIMPLE_T3881228540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.yydebug.yyDebugSimple
struct  yyDebugSimple_t3881228540  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYDEBUGSIMPLE_T3881228540_H
#ifndef PARSER_T2410290639_H
#define PARSER_T2410290639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Parser
struct  Parser_t2410290639  : public RuntimeObject
{
public:
	// System.Boolean Mono.Data.SqlExpressions.Parser::cacheAggregationResults
	bool ___cacheAggregationResults_0;
	// System.Data.DataRow[] Mono.Data.SqlExpressions.Parser::aggregationRows
	DataRowU5BU5D_t3122106530* ___aggregationRows_1;
	// System.IO.TextWriter Mono.Data.SqlExpressions.Parser::ErrorOutput
	TextWriter_t3478189236 * ___ErrorOutput_3;
	// System.Int32 Mono.Data.SqlExpressions.Parser::eof_token
	int32_t ___eof_token_4;
	// Mono.Data.SqlExpressions.yydebug.yyDebug Mono.Data.SqlExpressions.Parser::debug
	RuntimeObject* ___debug_5;
	// System.Int32 Mono.Data.SqlExpressions.Parser::yyExpectingState
	int32_t ___yyExpectingState_8;
	// System.Int32 Mono.Data.SqlExpressions.Parser::yyMax
	int32_t ___yyMax_9;

public:
	inline static int32_t get_offset_of_cacheAggregationResults_0() { return static_cast<int32_t>(offsetof(Parser_t2410290639, ___cacheAggregationResults_0)); }
	inline bool get_cacheAggregationResults_0() const { return ___cacheAggregationResults_0; }
	inline bool* get_address_of_cacheAggregationResults_0() { return &___cacheAggregationResults_0; }
	inline void set_cacheAggregationResults_0(bool value)
	{
		___cacheAggregationResults_0 = value;
	}

	inline static int32_t get_offset_of_aggregationRows_1() { return static_cast<int32_t>(offsetof(Parser_t2410290639, ___aggregationRows_1)); }
	inline DataRowU5BU5D_t3122106530* get_aggregationRows_1() const { return ___aggregationRows_1; }
	inline DataRowU5BU5D_t3122106530** get_address_of_aggregationRows_1() { return &___aggregationRows_1; }
	inline void set_aggregationRows_1(DataRowU5BU5D_t3122106530* value)
	{
		___aggregationRows_1 = value;
		Il2CppCodeGenWriteBarrier((&___aggregationRows_1), value);
	}

	inline static int32_t get_offset_of_ErrorOutput_3() { return static_cast<int32_t>(offsetof(Parser_t2410290639, ___ErrorOutput_3)); }
	inline TextWriter_t3478189236 * get_ErrorOutput_3() const { return ___ErrorOutput_3; }
	inline TextWriter_t3478189236 ** get_address_of_ErrorOutput_3() { return &___ErrorOutput_3; }
	inline void set_ErrorOutput_3(TextWriter_t3478189236 * value)
	{
		___ErrorOutput_3 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOutput_3), value);
	}

	inline static int32_t get_offset_of_eof_token_4() { return static_cast<int32_t>(offsetof(Parser_t2410290639, ___eof_token_4)); }
	inline int32_t get_eof_token_4() const { return ___eof_token_4; }
	inline int32_t* get_address_of_eof_token_4() { return &___eof_token_4; }
	inline void set_eof_token_4(int32_t value)
	{
		___eof_token_4 = value;
	}

	inline static int32_t get_offset_of_debug_5() { return static_cast<int32_t>(offsetof(Parser_t2410290639, ___debug_5)); }
	inline RuntimeObject* get_debug_5() const { return ___debug_5; }
	inline RuntimeObject** get_address_of_debug_5() { return &___debug_5; }
	inline void set_debug_5(RuntimeObject* value)
	{
		___debug_5 = value;
		Il2CppCodeGenWriteBarrier((&___debug_5), value);
	}

	inline static int32_t get_offset_of_yyExpectingState_8() { return static_cast<int32_t>(offsetof(Parser_t2410290639, ___yyExpectingState_8)); }
	inline int32_t get_yyExpectingState_8() const { return ___yyExpectingState_8; }
	inline int32_t* get_address_of_yyExpectingState_8() { return &___yyExpectingState_8; }
	inline void set_yyExpectingState_8(int32_t value)
	{
		___yyExpectingState_8 = value;
	}

	inline static int32_t get_offset_of_yyMax_9() { return static_cast<int32_t>(offsetof(Parser_t2410290639, ___yyMax_9)); }
	inline int32_t get_yyMax_9() const { return ___yyMax_9; }
	inline int32_t* get_address_of_yyMax_9() { return &___yyMax_9; }
	inline void set_yyMax_9(int32_t value)
	{
		___yyMax_9 = value;
	}
};

struct Parser_t2410290639_StaticFields
{
public:
	// System.Int32 Mono.Data.SqlExpressions.Parser::yacc_verbose_flag
	int32_t ___yacc_verbose_flag_2;
	// System.Int32 Mono.Data.SqlExpressions.Parser::yyFinal
	int32_t ___yyFinal_6;
	// System.String[] Mono.Data.SqlExpressions.Parser::yyNames
	StringU5BU5D_t1281789340* ___yyNames_7;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyLhs
	Int16U5BU5D_t3686840178* ___yyLhs_10;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyLen
	Int16U5BU5D_t3686840178* ___yyLen_11;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyDefRed
	Int16U5BU5D_t3686840178* ___yyDefRed_12;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyDgoto
	Int16U5BU5D_t3686840178* ___yyDgoto_13;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yySindex
	Int16U5BU5D_t3686840178* ___yySindex_14;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyRindex
	Int16U5BU5D_t3686840178* ___yyRindex_15;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyGindex
	Int16U5BU5D_t3686840178* ___yyGindex_16;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyTable
	Int16U5BU5D_t3686840178* ___yyTable_17;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyCheck
	Int16U5BU5D_t3686840178* ___yyCheck_18;

public:
	inline static int32_t get_offset_of_yacc_verbose_flag_2() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yacc_verbose_flag_2)); }
	inline int32_t get_yacc_verbose_flag_2() const { return ___yacc_verbose_flag_2; }
	inline int32_t* get_address_of_yacc_verbose_flag_2() { return &___yacc_verbose_flag_2; }
	inline void set_yacc_verbose_flag_2(int32_t value)
	{
		___yacc_verbose_flag_2 = value;
	}

	inline static int32_t get_offset_of_yyFinal_6() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyFinal_6)); }
	inline int32_t get_yyFinal_6() const { return ___yyFinal_6; }
	inline int32_t* get_address_of_yyFinal_6() { return &___yyFinal_6; }
	inline void set_yyFinal_6(int32_t value)
	{
		___yyFinal_6 = value;
	}

	inline static int32_t get_offset_of_yyNames_7() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyNames_7)); }
	inline StringU5BU5D_t1281789340* get_yyNames_7() const { return ___yyNames_7; }
	inline StringU5BU5D_t1281789340** get_address_of_yyNames_7() { return &___yyNames_7; }
	inline void set_yyNames_7(StringU5BU5D_t1281789340* value)
	{
		___yyNames_7 = value;
		Il2CppCodeGenWriteBarrier((&___yyNames_7), value);
	}

	inline static int32_t get_offset_of_yyLhs_10() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyLhs_10)); }
	inline Int16U5BU5D_t3686840178* get_yyLhs_10() const { return ___yyLhs_10; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyLhs_10() { return &___yyLhs_10; }
	inline void set_yyLhs_10(Int16U5BU5D_t3686840178* value)
	{
		___yyLhs_10 = value;
		Il2CppCodeGenWriteBarrier((&___yyLhs_10), value);
	}

	inline static int32_t get_offset_of_yyLen_11() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyLen_11)); }
	inline Int16U5BU5D_t3686840178* get_yyLen_11() const { return ___yyLen_11; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyLen_11() { return &___yyLen_11; }
	inline void set_yyLen_11(Int16U5BU5D_t3686840178* value)
	{
		___yyLen_11 = value;
		Il2CppCodeGenWriteBarrier((&___yyLen_11), value);
	}

	inline static int32_t get_offset_of_yyDefRed_12() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyDefRed_12)); }
	inline Int16U5BU5D_t3686840178* get_yyDefRed_12() const { return ___yyDefRed_12; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyDefRed_12() { return &___yyDefRed_12; }
	inline void set_yyDefRed_12(Int16U5BU5D_t3686840178* value)
	{
		___yyDefRed_12 = value;
		Il2CppCodeGenWriteBarrier((&___yyDefRed_12), value);
	}

	inline static int32_t get_offset_of_yyDgoto_13() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyDgoto_13)); }
	inline Int16U5BU5D_t3686840178* get_yyDgoto_13() const { return ___yyDgoto_13; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyDgoto_13() { return &___yyDgoto_13; }
	inline void set_yyDgoto_13(Int16U5BU5D_t3686840178* value)
	{
		___yyDgoto_13 = value;
		Il2CppCodeGenWriteBarrier((&___yyDgoto_13), value);
	}

	inline static int32_t get_offset_of_yySindex_14() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yySindex_14)); }
	inline Int16U5BU5D_t3686840178* get_yySindex_14() const { return ___yySindex_14; }
	inline Int16U5BU5D_t3686840178** get_address_of_yySindex_14() { return &___yySindex_14; }
	inline void set_yySindex_14(Int16U5BU5D_t3686840178* value)
	{
		___yySindex_14 = value;
		Il2CppCodeGenWriteBarrier((&___yySindex_14), value);
	}

	inline static int32_t get_offset_of_yyRindex_15() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyRindex_15)); }
	inline Int16U5BU5D_t3686840178* get_yyRindex_15() const { return ___yyRindex_15; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyRindex_15() { return &___yyRindex_15; }
	inline void set_yyRindex_15(Int16U5BU5D_t3686840178* value)
	{
		___yyRindex_15 = value;
		Il2CppCodeGenWriteBarrier((&___yyRindex_15), value);
	}

	inline static int32_t get_offset_of_yyGindex_16() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyGindex_16)); }
	inline Int16U5BU5D_t3686840178* get_yyGindex_16() const { return ___yyGindex_16; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyGindex_16() { return &___yyGindex_16; }
	inline void set_yyGindex_16(Int16U5BU5D_t3686840178* value)
	{
		___yyGindex_16 = value;
		Il2CppCodeGenWriteBarrier((&___yyGindex_16), value);
	}

	inline static int32_t get_offset_of_yyTable_17() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyTable_17)); }
	inline Int16U5BU5D_t3686840178* get_yyTable_17() const { return ___yyTable_17; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyTable_17() { return &___yyTable_17; }
	inline void set_yyTable_17(Int16U5BU5D_t3686840178* value)
	{
		___yyTable_17 = value;
		Il2CppCodeGenWriteBarrier((&___yyTable_17), value);
	}

	inline static int32_t get_offset_of_yyCheck_18() { return static_cast<int32_t>(offsetof(Parser_t2410290639_StaticFields, ___yyCheck_18)); }
	inline Int16U5BU5D_t3686840178* get_yyCheck_18() const { return ___yyCheck_18; }
	inline Int16U5BU5D_t3686840178** get_address_of_yyCheck_18() { return &___yyCheck_18; }
	inline void set_yyCheck_18(Int16U5BU5D_t3686840178* value)
	{
		___yyCheck_18 = value;
		Il2CppCodeGenWriteBarrier((&___yyCheck_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T2410290639_H
#ifndef XMLWRITER_T127905479_H
#define XMLWRITER_T127905479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_t127905479  : public RuntimeObject
{
public:
	// System.Xml.XmlWriterSettings System.Xml.XmlWriter::settings
	XmlWriterSettings_t3314986516 * ___settings_0;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(XmlWriter_t127905479, ___settings_0)); }
	inline XmlWriterSettings_t3314986516 * get_settings_0() const { return ___settings_0; }
	inline XmlWriterSettings_t3314986516 ** get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(XmlWriterSettings_t3314986516 * value)
	{
		___settings_0 = value;
		Il2CppCodeGenWriteBarrier((&___settings_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_T127905479_H
#ifndef TOKENIZER_T4122578263_H
#define TOKENIZER_T4122578263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Tokenizer
struct  Tokenizer_t4122578263  : public RuntimeObject
{
public:
	// System.Char[] Mono.Data.SqlExpressions.Tokenizer::input
	CharU5BU5D_t3528271667* ___input_2;
	// System.Int32 Mono.Data.SqlExpressions.Tokenizer::pos
	int32_t ___pos_3;
	// System.Int32 Mono.Data.SqlExpressions.Tokenizer::tok
	int32_t ___tok_4;
	// System.Object Mono.Data.SqlExpressions.Tokenizer::val
	RuntimeObject * ___val_5;

public:
	inline static int32_t get_offset_of_input_2() { return static_cast<int32_t>(offsetof(Tokenizer_t4122578263, ___input_2)); }
	inline CharU5BU5D_t3528271667* get_input_2() const { return ___input_2; }
	inline CharU5BU5D_t3528271667** get_address_of_input_2() { return &___input_2; }
	inline void set_input_2(CharU5BU5D_t3528271667* value)
	{
		___input_2 = value;
		Il2CppCodeGenWriteBarrier((&___input_2), value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(Tokenizer_t4122578263, ___pos_3)); }
	inline int32_t get_pos_3() const { return ___pos_3; }
	inline int32_t* get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(int32_t value)
	{
		___pos_3 = value;
	}

	inline static int32_t get_offset_of_tok_4() { return static_cast<int32_t>(offsetof(Tokenizer_t4122578263, ___tok_4)); }
	inline int32_t get_tok_4() const { return ___tok_4; }
	inline int32_t* get_address_of_tok_4() { return &___tok_4; }
	inline void set_tok_4(int32_t value)
	{
		___tok_4 = value;
	}

	inline static int32_t get_offset_of_val_5() { return static_cast<int32_t>(offsetof(Tokenizer_t4122578263, ___val_5)); }
	inline RuntimeObject * get_val_5() const { return ___val_5; }
	inline RuntimeObject ** get_address_of_val_5() { return &___val_5; }
	inline void set_val_5(RuntimeObject * value)
	{
		___val_5 = value;
		Il2CppCodeGenWriteBarrier((&___val_5), value);
	}
};

struct Tokenizer_t4122578263_StaticFields
{
public:
	// System.Collections.IDictionary Mono.Data.SqlExpressions.Tokenizer::tokenMap
	RuntimeObject* ___tokenMap_0;
	// System.Object[] Mono.Data.SqlExpressions.Tokenizer::tokens
	ObjectU5BU5D_t2843939325* ___tokens_1;

public:
	inline static int32_t get_offset_of_tokenMap_0() { return static_cast<int32_t>(offsetof(Tokenizer_t4122578263_StaticFields, ___tokenMap_0)); }
	inline RuntimeObject* get_tokenMap_0() const { return ___tokenMap_0; }
	inline RuntimeObject** get_address_of_tokenMap_0() { return &___tokenMap_0; }
	inline void set_tokenMap_0(RuntimeObject* value)
	{
		___tokenMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___tokenMap_0), value);
	}

	inline static int32_t get_offset_of_tokens_1() { return static_cast<int32_t>(offsetof(Tokenizer_t4122578263_StaticFields, ___tokens_1)); }
	inline ObjectU5BU5D_t2843939325* get_tokens_1() const { return ___tokens_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of_tokens_1() { return &___tokens_1; }
	inline void set_tokens_1(ObjectU5BU5D_t2843939325* value)
	{
		___tokens_1 = value;
		Il2CppCodeGenWriteBarrier((&___tokens_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIZER_T4122578263_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef XMLREADER_T3121518892_H
#define XMLREADER_T3121518892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3121518892  : public RuntimeObject
{
public:
	// System.Text.StringBuilder System.Xml.XmlReader::readStringBuffer
	StringBuilder_t * ___readStringBuffer_0;
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1809665003 * ___binary_1;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t2186285234 * ___settings_2;

public:
	inline static int32_t get_offset_of_readStringBuffer_0() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___readStringBuffer_0)); }
	inline StringBuilder_t * get_readStringBuffer_0() const { return ___readStringBuffer_0; }
	inline StringBuilder_t ** get_address_of_readStringBuffer_0() { return &___readStringBuffer_0; }
	inline void set_readStringBuffer_0(StringBuilder_t * value)
	{
		___readStringBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___readStringBuffer_0), value);
	}

	inline static int32_t get_offset_of_binary_1() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___binary_1)); }
	inline XmlReaderBinarySupport_t1809665003 * get_binary_1() const { return ___binary_1; }
	inline XmlReaderBinarySupport_t1809665003 ** get_address_of_binary_1() { return &___binary_1; }
	inline void set_binary_1(XmlReaderBinarySupport_t1809665003 * value)
	{
		___binary_1 = value;
		Il2CppCodeGenWriteBarrier((&___binary_1), value);
	}

	inline static int32_t get_offset_of_settings_2() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___settings_2)); }
	inline XmlReaderSettings_t2186285234 * get_settings_2() const { return ___settings_2; }
	inline XmlReaderSettings_t2186285234 ** get_address_of_settings_2() { return &___settings_2; }
	inline void set_settings_2(XmlReaderSettings_t2186285234 * value)
	{
		___settings_2 = value;
		Il2CppCodeGenWriteBarrier((&___settings_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3121518892_H
#ifndef INDEX_T4155063583_H
#define INDEX_T4155063583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.Index
struct  Index_t4155063583  : public RuntimeObject
{
public:
	// System.Int32[] System.Data.Common.Index::_array
	Int32U5BU5D_t385246372* ____array_1;
	// System.Int32 System.Data.Common.Index::_size
	int32_t ____size_2;
	// System.Data.Common.Key System.Data.Common.Index::_key
	Key_t1531728703 * ____key_3;
	// System.Int32 System.Data.Common.Index::_refCount
	int32_t ____refCount_4;
	// System.Boolean System.Data.Common.Index::know_have_duplicates
	bool ___know_have_duplicates_5;
	// System.Boolean System.Data.Common.Index::know_no_duplicates
	bool ___know_no_duplicates_6;

public:
	inline static int32_t get_offset_of__array_1() { return static_cast<int32_t>(offsetof(Index_t4155063583, ____array_1)); }
	inline Int32U5BU5D_t385246372* get__array_1() const { return ____array_1; }
	inline Int32U5BU5D_t385246372** get_address_of__array_1() { return &____array_1; }
	inline void set__array_1(Int32U5BU5D_t385246372* value)
	{
		____array_1 = value;
		Il2CppCodeGenWriteBarrier((&____array_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(Index_t4155063583, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__key_3() { return static_cast<int32_t>(offsetof(Index_t4155063583, ____key_3)); }
	inline Key_t1531728703 * get__key_3() const { return ____key_3; }
	inline Key_t1531728703 ** get_address_of__key_3() { return &____key_3; }
	inline void set__key_3(Key_t1531728703 * value)
	{
		____key_3 = value;
		Il2CppCodeGenWriteBarrier((&____key_3), value);
	}

	inline static int32_t get_offset_of__refCount_4() { return static_cast<int32_t>(offsetof(Index_t4155063583, ____refCount_4)); }
	inline int32_t get__refCount_4() const { return ____refCount_4; }
	inline int32_t* get_address_of__refCount_4() { return &____refCount_4; }
	inline void set__refCount_4(int32_t value)
	{
		____refCount_4 = value;
	}

	inline static int32_t get_offset_of_know_have_duplicates_5() { return static_cast<int32_t>(offsetof(Index_t4155063583, ___know_have_duplicates_5)); }
	inline bool get_know_have_duplicates_5() const { return ___know_have_duplicates_5; }
	inline bool* get_address_of_know_have_duplicates_5() { return &___know_have_duplicates_5; }
	inline void set_know_have_duplicates_5(bool value)
	{
		___know_have_duplicates_5 = value;
	}

	inline static int32_t get_offset_of_know_no_duplicates_6() { return static_cast<int32_t>(offsetof(Index_t4155063583, ___know_no_duplicates_6)); }
	inline bool get_know_no_duplicates_6() const { return ___know_no_duplicates_6; }
	inline bool* get_address_of_know_no_duplicates_6() { return &___know_no_duplicates_6; }
	inline void set_know_no_duplicates_6(bool value)
	{
		___know_no_duplicates_6 = value;
	}
};

struct Index_t4155063583_StaticFields
{
public:
	// System.Int32[] System.Data.Common.Index::empty
	Int32U5BU5D_t385246372* ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(Index_t4155063583_StaticFields, ___empty_0)); }
	inline Int32U5BU5D_t385246372* get_empty_0() const { return ___empty_0; }
	inline Int32U5BU5D_t385246372** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(Int32U5BU5D_t385246372* value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEX_T4155063583_H
#ifndef DBTYPES_T3951852654_H
#define DBTYPES_T3951852654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbTypes
struct  DbTypes_t3951852654  : public RuntimeObject
{
public:

public:
};

struct DbTypes_t3951852654_StaticFields
{
public:
	// System.Type System.Data.Common.DbTypes::TypeOfBoolean
	Type_t * ___TypeOfBoolean_0;
	// System.Type System.Data.Common.DbTypes::TypeOfSByte
	Type_t * ___TypeOfSByte_1;
	// System.Type System.Data.Common.DbTypes::TypeOfChar
	Type_t * ___TypeOfChar_2;
	// System.Type System.Data.Common.DbTypes::TypeOfInt16
	Type_t * ___TypeOfInt16_3;
	// System.Type System.Data.Common.DbTypes::TypeOfInt32
	Type_t * ___TypeOfInt32_4;
	// System.Type System.Data.Common.DbTypes::TypeOfInt64
	Type_t * ___TypeOfInt64_5;
	// System.Type System.Data.Common.DbTypes::TypeOfByte
	Type_t * ___TypeOfByte_6;
	// System.Type System.Data.Common.DbTypes::TypeOfUInt16
	Type_t * ___TypeOfUInt16_7;
	// System.Type System.Data.Common.DbTypes::TypeOfUInt32
	Type_t * ___TypeOfUInt32_8;
	// System.Type System.Data.Common.DbTypes::TypeOfUInt64
	Type_t * ___TypeOfUInt64_9;
	// System.Type System.Data.Common.DbTypes::TypeOfDouble
	Type_t * ___TypeOfDouble_10;
	// System.Type System.Data.Common.DbTypes::TypeOfSingle
	Type_t * ___TypeOfSingle_11;
	// System.Type System.Data.Common.DbTypes::TypeOfDecimal
	Type_t * ___TypeOfDecimal_12;
	// System.Type System.Data.Common.DbTypes::TypeOfString
	Type_t * ___TypeOfString_13;
	// System.Type System.Data.Common.DbTypes::TypeOfDateTime
	Type_t * ___TypeOfDateTime_14;
	// System.Type System.Data.Common.DbTypes::TypeOfObject
	Type_t * ___TypeOfObject_15;
	// System.Type System.Data.Common.DbTypes::TypeOfGuid
	Type_t * ___TypeOfGuid_16;
	// System.Type System.Data.Common.DbTypes::TypeOfType
	Type_t * ___TypeOfType_17;
	// System.Type System.Data.Common.DbTypes::TypeOfByteArray
	Type_t * ___TypeOfByteArray_18;
	// System.Type System.Data.Common.DbTypes::TypeOfFloat
	Type_t * ___TypeOfFloat_19;
	// System.Type System.Data.Common.DbTypes::TypeOfTimespan
	Type_t * ___TypeOfTimespan_20;

public:
	inline static int32_t get_offset_of_TypeOfBoolean_0() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfBoolean_0)); }
	inline Type_t * get_TypeOfBoolean_0() const { return ___TypeOfBoolean_0; }
	inline Type_t ** get_address_of_TypeOfBoolean_0() { return &___TypeOfBoolean_0; }
	inline void set_TypeOfBoolean_0(Type_t * value)
	{
		___TypeOfBoolean_0 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfBoolean_0), value);
	}

	inline static int32_t get_offset_of_TypeOfSByte_1() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfSByte_1)); }
	inline Type_t * get_TypeOfSByte_1() const { return ___TypeOfSByte_1; }
	inline Type_t ** get_address_of_TypeOfSByte_1() { return &___TypeOfSByte_1; }
	inline void set_TypeOfSByte_1(Type_t * value)
	{
		___TypeOfSByte_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfSByte_1), value);
	}

	inline static int32_t get_offset_of_TypeOfChar_2() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfChar_2)); }
	inline Type_t * get_TypeOfChar_2() const { return ___TypeOfChar_2; }
	inline Type_t ** get_address_of_TypeOfChar_2() { return &___TypeOfChar_2; }
	inline void set_TypeOfChar_2(Type_t * value)
	{
		___TypeOfChar_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfChar_2), value);
	}

	inline static int32_t get_offset_of_TypeOfInt16_3() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfInt16_3)); }
	inline Type_t * get_TypeOfInt16_3() const { return ___TypeOfInt16_3; }
	inline Type_t ** get_address_of_TypeOfInt16_3() { return &___TypeOfInt16_3; }
	inline void set_TypeOfInt16_3(Type_t * value)
	{
		___TypeOfInt16_3 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfInt16_3), value);
	}

	inline static int32_t get_offset_of_TypeOfInt32_4() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfInt32_4)); }
	inline Type_t * get_TypeOfInt32_4() const { return ___TypeOfInt32_4; }
	inline Type_t ** get_address_of_TypeOfInt32_4() { return &___TypeOfInt32_4; }
	inline void set_TypeOfInt32_4(Type_t * value)
	{
		___TypeOfInt32_4 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfInt32_4), value);
	}

	inline static int32_t get_offset_of_TypeOfInt64_5() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfInt64_5)); }
	inline Type_t * get_TypeOfInt64_5() const { return ___TypeOfInt64_5; }
	inline Type_t ** get_address_of_TypeOfInt64_5() { return &___TypeOfInt64_5; }
	inline void set_TypeOfInt64_5(Type_t * value)
	{
		___TypeOfInt64_5 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfInt64_5), value);
	}

	inline static int32_t get_offset_of_TypeOfByte_6() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfByte_6)); }
	inline Type_t * get_TypeOfByte_6() const { return ___TypeOfByte_6; }
	inline Type_t ** get_address_of_TypeOfByte_6() { return &___TypeOfByte_6; }
	inline void set_TypeOfByte_6(Type_t * value)
	{
		___TypeOfByte_6 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfByte_6), value);
	}

	inline static int32_t get_offset_of_TypeOfUInt16_7() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfUInt16_7)); }
	inline Type_t * get_TypeOfUInt16_7() const { return ___TypeOfUInt16_7; }
	inline Type_t ** get_address_of_TypeOfUInt16_7() { return &___TypeOfUInt16_7; }
	inline void set_TypeOfUInt16_7(Type_t * value)
	{
		___TypeOfUInt16_7 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfUInt16_7), value);
	}

	inline static int32_t get_offset_of_TypeOfUInt32_8() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfUInt32_8)); }
	inline Type_t * get_TypeOfUInt32_8() const { return ___TypeOfUInt32_8; }
	inline Type_t ** get_address_of_TypeOfUInt32_8() { return &___TypeOfUInt32_8; }
	inline void set_TypeOfUInt32_8(Type_t * value)
	{
		___TypeOfUInt32_8 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfUInt32_8), value);
	}

	inline static int32_t get_offset_of_TypeOfUInt64_9() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfUInt64_9)); }
	inline Type_t * get_TypeOfUInt64_9() const { return ___TypeOfUInt64_9; }
	inline Type_t ** get_address_of_TypeOfUInt64_9() { return &___TypeOfUInt64_9; }
	inline void set_TypeOfUInt64_9(Type_t * value)
	{
		___TypeOfUInt64_9 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfUInt64_9), value);
	}

	inline static int32_t get_offset_of_TypeOfDouble_10() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfDouble_10)); }
	inline Type_t * get_TypeOfDouble_10() const { return ___TypeOfDouble_10; }
	inline Type_t ** get_address_of_TypeOfDouble_10() { return &___TypeOfDouble_10; }
	inline void set_TypeOfDouble_10(Type_t * value)
	{
		___TypeOfDouble_10 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfDouble_10), value);
	}

	inline static int32_t get_offset_of_TypeOfSingle_11() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfSingle_11)); }
	inline Type_t * get_TypeOfSingle_11() const { return ___TypeOfSingle_11; }
	inline Type_t ** get_address_of_TypeOfSingle_11() { return &___TypeOfSingle_11; }
	inline void set_TypeOfSingle_11(Type_t * value)
	{
		___TypeOfSingle_11 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfSingle_11), value);
	}

	inline static int32_t get_offset_of_TypeOfDecimal_12() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfDecimal_12)); }
	inline Type_t * get_TypeOfDecimal_12() const { return ___TypeOfDecimal_12; }
	inline Type_t ** get_address_of_TypeOfDecimal_12() { return &___TypeOfDecimal_12; }
	inline void set_TypeOfDecimal_12(Type_t * value)
	{
		___TypeOfDecimal_12 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfDecimal_12), value);
	}

	inline static int32_t get_offset_of_TypeOfString_13() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfString_13)); }
	inline Type_t * get_TypeOfString_13() const { return ___TypeOfString_13; }
	inline Type_t ** get_address_of_TypeOfString_13() { return &___TypeOfString_13; }
	inline void set_TypeOfString_13(Type_t * value)
	{
		___TypeOfString_13 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfString_13), value);
	}

	inline static int32_t get_offset_of_TypeOfDateTime_14() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfDateTime_14)); }
	inline Type_t * get_TypeOfDateTime_14() const { return ___TypeOfDateTime_14; }
	inline Type_t ** get_address_of_TypeOfDateTime_14() { return &___TypeOfDateTime_14; }
	inline void set_TypeOfDateTime_14(Type_t * value)
	{
		___TypeOfDateTime_14 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfDateTime_14), value);
	}

	inline static int32_t get_offset_of_TypeOfObject_15() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfObject_15)); }
	inline Type_t * get_TypeOfObject_15() const { return ___TypeOfObject_15; }
	inline Type_t ** get_address_of_TypeOfObject_15() { return &___TypeOfObject_15; }
	inline void set_TypeOfObject_15(Type_t * value)
	{
		___TypeOfObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfObject_15), value);
	}

	inline static int32_t get_offset_of_TypeOfGuid_16() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfGuid_16)); }
	inline Type_t * get_TypeOfGuid_16() const { return ___TypeOfGuid_16; }
	inline Type_t ** get_address_of_TypeOfGuid_16() { return &___TypeOfGuid_16; }
	inline void set_TypeOfGuid_16(Type_t * value)
	{
		___TypeOfGuid_16 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfGuid_16), value);
	}

	inline static int32_t get_offset_of_TypeOfType_17() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfType_17)); }
	inline Type_t * get_TypeOfType_17() const { return ___TypeOfType_17; }
	inline Type_t ** get_address_of_TypeOfType_17() { return &___TypeOfType_17; }
	inline void set_TypeOfType_17(Type_t * value)
	{
		___TypeOfType_17 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfType_17), value);
	}

	inline static int32_t get_offset_of_TypeOfByteArray_18() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfByteArray_18)); }
	inline Type_t * get_TypeOfByteArray_18() const { return ___TypeOfByteArray_18; }
	inline Type_t ** get_address_of_TypeOfByteArray_18() { return &___TypeOfByteArray_18; }
	inline void set_TypeOfByteArray_18(Type_t * value)
	{
		___TypeOfByteArray_18 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfByteArray_18), value);
	}

	inline static int32_t get_offset_of_TypeOfFloat_19() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfFloat_19)); }
	inline Type_t * get_TypeOfFloat_19() const { return ___TypeOfFloat_19; }
	inline Type_t ** get_address_of_TypeOfFloat_19() { return &___TypeOfFloat_19; }
	inline void set_TypeOfFloat_19(Type_t * value)
	{
		___TypeOfFloat_19 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfFloat_19), value);
	}

	inline static int32_t get_offset_of_TypeOfTimespan_20() { return static_cast<int32_t>(offsetof(DbTypes_t3951852654_StaticFields, ___TypeOfTimespan_20)); }
	inline Type_t * get_TypeOfTimespan_20() const { return ___TypeOfTimespan_20; }
	inline Type_t ** get_address_of_TypeOfTimespan_20() { return &___TypeOfTimespan_20; }
	inline void set_TypeOfTimespan_20(Type_t * value)
	{
		___TypeOfTimespan_20 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfTimespan_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBTYPES_T3951852654_H
#ifndef DBPROVIDERFACTORY_T3108676059_H
#define DBPROVIDERFACTORY_T3108676059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbProviderFactory
struct  DbProviderFactory_t3108676059  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBPROVIDERFACTORY_T3108676059_H
#ifndef DATACONTAINER_T2712869782_H
#define DATACONTAINER_T2712869782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DataContainer
struct  DataContainer_t2712869782  : public RuntimeObject
{
public:
	// System.Collections.BitArray System.Data.Common.DataContainer::null_values
	BitArray_t4087883509 * ___null_values_0;
	// System.Type System.Data.Common.DataContainer::_type
	Type_t * ____type_1;
	// System.Data.DataColumn System.Data.Common.DataContainer::_column
	DataColumn_t2485786120 * ____column_2;

public:
	inline static int32_t get_offset_of_null_values_0() { return static_cast<int32_t>(offsetof(DataContainer_t2712869782, ___null_values_0)); }
	inline BitArray_t4087883509 * get_null_values_0() const { return ___null_values_0; }
	inline BitArray_t4087883509 ** get_address_of_null_values_0() { return &___null_values_0; }
	inline void set_null_values_0(BitArray_t4087883509 * value)
	{
		___null_values_0 = value;
		Il2CppCodeGenWriteBarrier((&___null_values_0), value);
	}

	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(DataContainer_t2712869782, ____type_1)); }
	inline Type_t * get__type_1() const { return ____type_1; }
	inline Type_t ** get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(Type_t * value)
	{
		____type_1 = value;
		Il2CppCodeGenWriteBarrier((&____type_1), value);
	}

	inline static int32_t get_offset_of__column_2() { return static_cast<int32_t>(offsetof(DataContainer_t2712869782, ____column_2)); }
	inline DataColumn_t2485786120 * get__column_2() const { return ____column_2; }
	inline DataColumn_t2485786120 ** get_address_of__column_2() { return &____column_2; }
	inline void set__column_2(DataColumn_t2485786120 * value)
	{
		____column_2 = value;
		Il2CppCodeGenWriteBarrier((&____column_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACONTAINER_T2712869782_H
#ifndef DTDINPUTSTATESTACK_T339956957_H
#define DTDINPUTSTATESTACK_T339956957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/DtdInputStateStack
struct  DtdInputStateStack_t339956957  : public RuntimeObject
{
public:
	// System.Collections.Stack Mono.Xml2.XmlTextReader/DtdInputStateStack::intern
	Stack_t2329662280 * ___intern_0;

public:
	inline static int32_t get_offset_of_intern_0() { return static_cast<int32_t>(offsetof(DtdInputStateStack_t339956957, ___intern_0)); }
	inline Stack_t2329662280 * get_intern_0() const { return ___intern_0; }
	inline Stack_t2329662280 ** get_address_of_intern_0() { return &___intern_0; }
	inline void set_intern_0(Stack_t2329662280 * value)
	{
		___intern_0 = value;
		Il2CppCodeGenWriteBarrier((&___intern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINPUTSTATESTACK_T339956957_H
#ifndef STRINGUTIL_T564231417_H
#define STRINGUTIL_T564231417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/StringUtil
struct  StringUtil_t564231417  : public RuntimeObject
{
public:

public:
};

struct StringUtil_t564231417_StaticFields
{
public:
	// System.Globalization.CultureInfo System.Xml.XmlTextWriter/StringUtil::cul
	CultureInfo_t4157843068 * ___cul_0;
	// System.Globalization.CompareInfo System.Xml.XmlTextWriter/StringUtil::cmp
	CompareInfo_t1092934962 * ___cmp_1;

public:
	inline static int32_t get_offset_of_cul_0() { return static_cast<int32_t>(offsetof(StringUtil_t564231417_StaticFields, ___cul_0)); }
	inline CultureInfo_t4157843068 * get_cul_0() const { return ___cul_0; }
	inline CultureInfo_t4157843068 ** get_address_of_cul_0() { return &___cul_0; }
	inline void set_cul_0(CultureInfo_t4157843068 * value)
	{
		___cul_0 = value;
		Il2CppCodeGenWriteBarrier((&___cul_0), value);
	}

	inline static int32_t get_offset_of_cmp_1() { return static_cast<int32_t>(offsetof(StringUtil_t564231417_StaticFields, ___cmp_1)); }
	inline CompareInfo_t1092934962 * get_cmp_1() const { return ___cmp_1; }
	inline CompareInfo_t1092934962 ** get_address_of_cmp_1() { return &___cmp_1; }
	inline void set_cmp_1(CompareInfo_t1092934962 * value)
	{
		___cmp_1 = value;
		Il2CppCodeGenWriteBarrier((&___cmp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTIL_T564231417_H
#ifndef XMLRESOLVER_T626023767_H
#define XMLRESOLVER_T626023767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlResolver
struct  XmlResolver_t626023767  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRESOLVER_T626023767_H
#ifndef DATATABLEMAPPING_T2453262560_H
#define DATATABLEMAPPING_T2453262560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DataTableMapping
struct  DataTableMapping_t2453262560  : public MarshalByRefObject_t2760389100
{
public:
	// System.String System.Data.Common.DataTableMapping::sourceTable
	String_t* ___sourceTable_1;
	// System.String System.Data.Common.DataTableMapping::dataSetTable
	String_t* ___dataSetTable_2;
	// System.Data.Common.DataColumnMappingCollection System.Data.Common.DataTableMapping::columnMappings
	DataColumnMappingCollection_t2426444188 * ___columnMappings_3;

public:
	inline static int32_t get_offset_of_sourceTable_1() { return static_cast<int32_t>(offsetof(DataTableMapping_t2453262560, ___sourceTable_1)); }
	inline String_t* get_sourceTable_1() const { return ___sourceTable_1; }
	inline String_t** get_address_of_sourceTable_1() { return &___sourceTable_1; }
	inline void set_sourceTable_1(String_t* value)
	{
		___sourceTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTable_1), value);
	}

	inline static int32_t get_offset_of_dataSetTable_2() { return static_cast<int32_t>(offsetof(DataTableMapping_t2453262560, ___dataSetTable_2)); }
	inline String_t* get_dataSetTable_2() const { return ___dataSetTable_2; }
	inline String_t** get_address_of_dataSetTable_2() { return &___dataSetTable_2; }
	inline void set_dataSetTable_2(String_t* value)
	{
		___dataSetTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetTable_2), value);
	}

	inline static int32_t get_offset_of_columnMappings_3() { return static_cast<int32_t>(offsetof(DataTableMapping_t2453262560, ___columnMappings_3)); }
	inline DataColumnMappingCollection_t2426444188 * get_columnMappings_3() const { return ___columnMappings_3; }
	inline DataColumnMappingCollection_t2426444188 ** get_address_of_columnMappings_3() { return &___columnMappings_3; }
	inline void set_columnMappings_3(DataColumnMappingCollection_t2426444188 * value)
	{
		___columnMappings_3 = value;
		Il2CppCodeGenWriteBarrier((&___columnMappings_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLEMAPPING_T2453262560_H
#ifndef LITERAL_T2610857326_H
#define LITERAL_T2610857326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Literal
struct  Literal_t2610857326  : public BaseExpression_t4150592444
{
public:
	// System.Object Mono.Data.SqlExpressions.Literal::val
	RuntimeObject * ___val_0;

public:
	inline static int32_t get_offset_of_val_0() { return static_cast<int32_t>(offsetof(Literal_t2610857326, ___val_0)); }
	inline RuntimeObject * get_val_0() const { return ___val_0; }
	inline RuntimeObject ** get_address_of_val_0() { return &___val_0; }
	inline void set_val_0(RuntimeObject * value)
	{
		___val_0 = value;
		Il2CppCodeGenWriteBarrier((&___val_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LITERAL_T2610857326_H
#ifndef TAGNAME_T2891256255_H
#define TAGNAME_T2891256255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/TagName
struct  TagName_t2891256255 
{
public:
	// System.String Mono.Xml2.XmlTextReader/TagName::Name
	String_t* ___Name_0;
	// System.String Mono.Xml2.XmlTextReader/TagName::LocalName
	String_t* ___LocalName_1;
	// System.String Mono.Xml2.XmlTextReader/TagName::Prefix
	String_t* ___Prefix_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TagName_t2891256255, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(TagName_t2891256255, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_Prefix_2() { return static_cast<int32_t>(offsetof(TagName_t2891256255, ___Prefix_2)); }
	inline String_t* get_Prefix_2() const { return ___Prefix_2; }
	inline String_t** get_address_of_Prefix_2() { return &___Prefix_2; }
	inline void set_Prefix_2(String_t* value)
	{
		___Prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Xml2.XmlTextReader/TagName
struct TagName_t2891256255_marshaled_pinvoke
{
	char* ___Name_0;
	char* ___LocalName_1;
	char* ___Prefix_2;
};
// Native definition for COM marshalling of Mono.Xml2.XmlTextReader/TagName
struct TagName_t2891256255_marshaled_com
{
	Il2CppChar* ___Name_0;
	Il2CppChar* ___LocalName_1;
	Il2CppChar* ___Prefix_2;
};
#endif // TAGNAME_T2891256255_H
#ifndef OBJECTDATACONTAINER_T3603397982_H
#define OBJECTDATACONTAINER_T3603397982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.ObjectDataContainer
struct  ObjectDataContainer_t3603397982  : public DataContainer_t2712869782
{
public:
	// System.Object[] System.Data.Common.ObjectDataContainer::_values
	ObjectU5BU5D_t2843939325* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(ObjectDataContainer_t3603397982, ____values_3)); }
	inline ObjectU5BU5D_t2843939325* get__values_3() const { return ____values_3; }
	inline ObjectU5BU5D_t2843939325** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(ObjectU5BU5D_t2843939325* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDATACONTAINER_T3603397982_H
#ifndef DATATABLEMAPPINGCOLLECTION_T2364717775_H
#define DATATABLEMAPPINGCOLLECTION_T2364717775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DataTableMappingCollection
struct  DataTableMappingCollection_t2364717775  : public MarshalByRefObject_t2760389100
{
public:
	// System.Collections.ArrayList System.Data.Common.DataTableMappingCollection::mappings
	ArrayList_t2718874744 * ___mappings_1;
	// System.Collections.Hashtable System.Data.Common.DataTableMappingCollection::sourceTables
	Hashtable_t1853889766 * ___sourceTables_2;
	// System.Collections.Hashtable System.Data.Common.DataTableMappingCollection::dataSetTables
	Hashtable_t1853889766 * ___dataSetTables_3;

public:
	inline static int32_t get_offset_of_mappings_1() { return static_cast<int32_t>(offsetof(DataTableMappingCollection_t2364717775, ___mappings_1)); }
	inline ArrayList_t2718874744 * get_mappings_1() const { return ___mappings_1; }
	inline ArrayList_t2718874744 ** get_address_of_mappings_1() { return &___mappings_1; }
	inline void set_mappings_1(ArrayList_t2718874744 * value)
	{
		___mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&___mappings_1), value);
	}

	inline static int32_t get_offset_of_sourceTables_2() { return static_cast<int32_t>(offsetof(DataTableMappingCollection_t2364717775, ___sourceTables_2)); }
	inline Hashtable_t1853889766 * get_sourceTables_2() const { return ___sourceTables_2; }
	inline Hashtable_t1853889766 ** get_address_of_sourceTables_2() { return &___sourceTables_2; }
	inline void set_sourceTables_2(Hashtable_t1853889766 * value)
	{
		___sourceTables_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTables_2), value);
	}

	inline static int32_t get_offset_of_dataSetTables_3() { return static_cast<int32_t>(offsetof(DataTableMappingCollection_t2364717775, ___dataSetTables_3)); }
	inline Hashtable_t1853889766 * get_dataSetTables_3() const { return ___dataSetTables_3; }
	inline Hashtable_t1853889766 ** get_address_of_dataSetTables_3() { return &___dataSetTables_3; }
	inline void set_dataSetTables_3(Hashtable_t1853889766 * value)
	{
		___dataSetTables_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetTables_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLEMAPPINGCOLLECTION_T2364717775_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef XMLTEXTREADER_T4233384356_H
#define XMLTEXTREADER_T4233384356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_t4233384356  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlTextReader System.Xml.XmlTextReader::entity
	XmlTextReader_t4233384356 * ___entity_3;
	// Mono.Xml2.XmlTextReader System.Xml.XmlTextReader::source
	XmlTextReader_t3455035481 * ___source_4;
	// System.Boolean System.Xml.XmlTextReader::entityInsideAttribute
	bool ___entityInsideAttribute_5;
	// System.Boolean System.Xml.XmlTextReader::insideAttribute
	bool ___insideAttribute_6;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlTextReader::entityNameStack
	Stack_1_t2690840144 * ___entityNameStack_7;

public:
	inline static int32_t get_offset_of_entity_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___entity_3)); }
	inline XmlTextReader_t4233384356 * get_entity_3() const { return ___entity_3; }
	inline XmlTextReader_t4233384356 ** get_address_of_entity_3() { return &___entity_3; }
	inline void set_entity_3(XmlTextReader_t4233384356 * value)
	{
		___entity_3 = value;
		Il2CppCodeGenWriteBarrier((&___entity_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___source_4)); }
	inline XmlTextReader_t3455035481 * get_source_4() const { return ___source_4; }
	inline XmlTextReader_t3455035481 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(XmlTextReader_t3455035481 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_entityInsideAttribute_5() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___entityInsideAttribute_5)); }
	inline bool get_entityInsideAttribute_5() const { return ___entityInsideAttribute_5; }
	inline bool* get_address_of_entityInsideAttribute_5() { return &___entityInsideAttribute_5; }
	inline void set_entityInsideAttribute_5(bool value)
	{
		___entityInsideAttribute_5 = value;
	}

	inline static int32_t get_offset_of_insideAttribute_6() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___insideAttribute_6)); }
	inline bool get_insideAttribute_6() const { return ___insideAttribute_6; }
	inline bool* get_address_of_insideAttribute_6() { return &___insideAttribute_6; }
	inline void set_insideAttribute_6(bool value)
	{
		___insideAttribute_6 = value;
	}

	inline static int32_t get_offset_of_entityNameStack_7() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___entityNameStack_7)); }
	inline Stack_1_t2690840144 * get_entityNameStack_7() const { return ___entityNameStack_7; }
	inline Stack_1_t2690840144 ** get_address_of_entityNameStack_7() { return &___entityNameStack_7; }
	inline void set_entityNameStack_7(Stack_1_t2690840144 * value)
	{
		___entityNameStack_7 = value;
		Il2CppCodeGenWriteBarrier((&___entityNameStack_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T4233384356_H
#ifndef UNARYEXPRESSION_T1939092167_H
#define UNARYEXPRESSION_T1939092167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.UnaryExpression
struct  UnaryExpression_t1939092167  : public BaseExpression_t4150592444
{
public:
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.UnaryExpression::expr
	RuntimeObject* ___expr_0;

public:
	inline static int32_t get_offset_of_expr_0() { return static_cast<int32_t>(offsetof(UnaryExpression_t1939092167, ___expr_0)); }
	inline RuntimeObject* get_expr_0() const { return ___expr_0; }
	inline RuntimeObject** get_address_of_expr_0() { return &___expr_0; }
	inline void set_expr_0(RuntimeObject* value)
	{
		___expr_0 = value;
		Il2CppCodeGenWriteBarrier((&___expr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNARYEXPRESSION_T1939092167_H
#ifndef BINARYEXPRESSION_T4181220238_H
#define BINARYEXPRESSION_T4181220238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.BinaryExpression
struct  BinaryExpression_t4181220238  : public BaseExpression_t4150592444
{
public:
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.BinaryExpression::expr1
	RuntimeObject* ___expr1_0;
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.BinaryExpression::expr2
	RuntimeObject* ___expr2_1;

public:
	inline static int32_t get_offset_of_expr1_0() { return static_cast<int32_t>(offsetof(BinaryExpression_t4181220238, ___expr1_0)); }
	inline RuntimeObject* get_expr1_0() const { return ___expr1_0; }
	inline RuntimeObject** get_address_of_expr1_0() { return &___expr1_0; }
	inline void set_expr1_0(RuntimeObject* value)
	{
		___expr1_0 = value;
		Il2CppCodeGenWriteBarrier((&___expr1_0), value);
	}

	inline static int32_t get_offset_of_expr2_1() { return static_cast<int32_t>(offsetof(BinaryExpression_t4181220238, ___expr2_1)); }
	inline RuntimeObject* get_expr2_1() const { return ___expr2_1; }
	inline RuntimeObject** get_address_of_expr2_1() { return &___expr2_1; }
	inline void set_expr2_1(RuntimeObject* value)
	{
		___expr2_1 = value;
		Il2CppCodeGenWriteBarrier((&___expr2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYEXPRESSION_T4181220238_H
#ifndef DBPARAMETERCOLLECTION_T2951779354_H
#define DBPARAMETERCOLLECTION_T2951779354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbParameterCollection
struct  DbParameterCollection_t2951779354  : public MarshalByRefObject_t2760389100
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBPARAMETERCOLLECTION_T2951779354_H
#ifndef DBPARAMETER_T3789779732_H
#define DBPARAMETER_T3789779732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbParameter
struct  DbParameter_t3789779732  : public MarshalByRefObject_t2760389100
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBPARAMETER_T3789779732_H
#ifndef DOUBLEDATACONTAINER_T430033574_H
#define DOUBLEDATACONTAINER_T430033574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DoubleDataContainer
struct  DoubleDataContainer_t430033574  : public DataContainer_t2712869782
{
public:
	// System.Double[] System.Data.Common.DoubleDataContainer::_values
	DoubleU5BU5D_t3413330114* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(DoubleDataContainer_t430033574, ____values_3)); }
	inline DoubleU5BU5D_t3413330114* get__values_3() const { return ____values_3; }
	inline DoubleU5BU5D_t3413330114** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(DoubleU5BU5D_t3413330114* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEDATACONTAINER_T430033574_H
#ifndef UINT16DATACONTAINER_T2155991586_H
#define UINT16DATACONTAINER_T2155991586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.UInt16DataContainer
struct  UInt16DataContainer_t2155991586  : public DataContainer_t2712869782
{
public:
	// System.UInt16[] System.Data.Common.UInt16DataContainer::_values
	UInt16U5BU5D_t3326319531* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(UInt16DataContainer_t2155991586, ____values_3)); }
	inline UInt16U5BU5D_t3326319531* get__values_3() const { return ____values_3; }
	inline UInt16U5BU5D_t3326319531** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(UInt16U5BU5D_t3326319531* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16DATACONTAINER_T2155991586_H
#ifndef INT16DATACONTAINER_T272545927_H
#define INT16DATACONTAINER_T272545927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.Int16DataContainer
struct  Int16DataContainer_t272545927  : public DataContainer_t2712869782
{
public:
	// System.Int16[] System.Data.Common.Int16DataContainer::_values
	Int16U5BU5D_t3686840178* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(Int16DataContainer_t272545927, ____values_3)); }
	inline Int16U5BU5D_t3686840178* get__values_3() const { return ____values_3; }
	inline Int16U5BU5D_t3686840178** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(Int16U5BU5D_t3686840178* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16DATACONTAINER_T272545927_H
#ifndef SBYTEDATACONTAINER_T4173937374_H
#define SBYTEDATACONTAINER_T4173937374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.SByteDataContainer
struct  SByteDataContainer_t4173937374  : public DataContainer_t2712869782
{
public:
	// System.SByte[] System.Data.Common.SByteDataContainer::_values
	SByteU5BU5D_t2651576203* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(SByteDataContainer_t4173937374, ____values_3)); }
	inline SByteU5BU5D_t2651576203* get__values_3() const { return ____values_3; }
	inline SByteU5BU5D_t2651576203** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(SByteU5BU5D_t2651576203* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SBYTEDATACONTAINER_T4173937374_H
#ifndef BYTEDATACONTAINER_T2113316179_H
#define BYTEDATACONTAINER_T2113316179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.ByteDataContainer
struct  ByteDataContainer_t2113316179  : public DataContainer_t2712869782
{
public:
	// System.Byte[] System.Data.Common.ByteDataContainer::_values
	ByteU5BU5D_t4116647657* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(ByteDataContainer_t2113316179, ____values_3)); }
	inline ByteU5BU5D_t4116647657* get__values_3() const { return ____values_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(ByteU5BU5D_t4116647657* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEDATACONTAINER_T2113316179_H
#ifndef CHARDATACONTAINER_T4070775190_H
#define CHARDATACONTAINER_T4070775190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.CharDataContainer
struct  CharDataContainer_t4070775190  : public DataContainer_t2712869782
{
public:
	// System.Char[] System.Data.Common.CharDataContainer::_values
	CharU5BU5D_t3528271667* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(CharDataContainer_t4070775190, ____values_3)); }
	inline CharU5BU5D_t3528271667* get__values_3() const { return ____values_3; }
	inline CharU5BU5D_t3528271667** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(CharU5BU5D_t3528271667* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARDATACONTAINER_T4070775190_H
#ifndef BITDATACONTAINER_T2674678255_H
#define BITDATACONTAINER_T2674678255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.BitDataContainer
struct  BitDataContainer_t2674678255  : public DataContainer_t2712869782
{
public:
	// System.Collections.BitArray System.Data.Common.BitDataContainer::_values
	BitArray_t4087883509 * ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(BitDataContainer_t2674678255, ____values_3)); }
	inline BitArray_t4087883509 * get__values_3() const { return ____values_3; }
	inline BitArray_t4087883509 ** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(BitArray_t4087883509 * value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITDATACONTAINER_T2674678255_H
#ifndef SINGLEDATACONTAINER_T2640744932_H
#define SINGLEDATACONTAINER_T2640744932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.SingleDataContainer
struct  SingleDataContainer_t2640744932  : public DataContainer_t2712869782
{
public:
	// System.Single[] System.Data.Common.SingleDataContainer::_values
	SingleU5BU5D_t1444911251* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(SingleDataContainer_t2640744932, ____values_3)); }
	inline SingleU5BU5D_t1444911251* get__values_3() const { return ____values_3; }
	inline SingleU5BU5D_t1444911251** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(SingleU5BU5D_t1444911251* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEDATACONTAINER_T2640744932_H
#ifndef UINT64DATACONTAINER_T3774578615_H
#define UINT64DATACONTAINER_T3774578615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.UInt64DataContainer
struct  UInt64DataContainer_t3774578615  : public DataContainer_t2712869782
{
public:
	// System.UInt64[] System.Data.Common.UInt64DataContainer::_values
	UInt64U5BU5D_t1659327989* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(UInt64DataContainer_t3774578615, ____values_3)); }
	inline UInt64U5BU5D_t1659327989* get__values_3() const { return ____values_3; }
	inline UInt64U5BU5D_t1659327989** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(UInt64U5BU5D_t1659327989* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64DATACONTAINER_T3774578615_H
#ifndef INT64DATACONTAINER_T1137787704_H
#define INT64DATACONTAINER_T1137787704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.Int64DataContainer
struct  Int64DataContainer_t1137787704  : public DataContainer_t2712869782
{
public:
	// System.Int64[] System.Data.Common.Int64DataContainer::_values
	Int64U5BU5D_t2559172825* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(Int64DataContainer_t1137787704, ____values_3)); }
	inline Int64U5BU5D_t2559172825* get__values_3() const { return ____values_3; }
	inline Int64U5BU5D_t2559172825** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(Int64U5BU5D_t2559172825* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64DATACONTAINER_T1137787704_H
#ifndef DATACOLUMNMAPPING_T1915060192_H
#define DATACOLUMNMAPPING_T1915060192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DataColumnMapping
struct  DataColumnMapping_t1915060192  : public MarshalByRefObject_t2760389100
{
public:
	// System.String System.Data.Common.DataColumnMapping::sourceColumn
	String_t* ___sourceColumn_1;
	// System.String System.Data.Common.DataColumnMapping::dataSetColumn
	String_t* ___dataSetColumn_2;

public:
	inline static int32_t get_offset_of_sourceColumn_1() { return static_cast<int32_t>(offsetof(DataColumnMapping_t1915060192, ___sourceColumn_1)); }
	inline String_t* get_sourceColumn_1() const { return ___sourceColumn_1; }
	inline String_t** get_address_of_sourceColumn_1() { return &___sourceColumn_1; }
	inline void set_sourceColumn_1(String_t* value)
	{
		___sourceColumn_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceColumn_1), value);
	}

	inline static int32_t get_offset_of_dataSetColumn_2() { return static_cast<int32_t>(offsetof(DataColumnMapping_t1915060192, ___dataSetColumn_2)); }
	inline String_t* get_dataSetColumn_2() const { return ___dataSetColumn_2; }
	inline String_t** get_address_of_dataSetColumn_2() { return &___dataSetColumn_2; }
	inline void set_dataSetColumn_2(String_t* value)
	{
		___dataSetColumn_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetColumn_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNMAPPING_T1915060192_H
#ifndef DATACOLUMNMAPPINGCOLLECTION_T2426444188_H
#define DATACOLUMNMAPPINGCOLLECTION_T2426444188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DataColumnMappingCollection
struct  DataColumnMappingCollection_t2426444188  : public MarshalByRefObject_t2760389100
{
public:
	// System.Collections.ArrayList System.Data.Common.DataColumnMappingCollection::list
	ArrayList_t2718874744 * ___list_1;
	// System.Collections.Hashtable System.Data.Common.DataColumnMappingCollection::sourceColumns
	Hashtable_t1853889766 * ___sourceColumns_2;
	// System.Collections.Hashtable System.Data.Common.DataColumnMappingCollection::dataSetColumns
	Hashtable_t1853889766 * ___dataSetColumns_3;

public:
	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(DataColumnMappingCollection_t2426444188, ___list_1)); }
	inline ArrayList_t2718874744 * get_list_1() const { return ___list_1; }
	inline ArrayList_t2718874744 ** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(ArrayList_t2718874744 * value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier((&___list_1), value);
	}

	inline static int32_t get_offset_of_sourceColumns_2() { return static_cast<int32_t>(offsetof(DataColumnMappingCollection_t2426444188, ___sourceColumns_2)); }
	inline Hashtable_t1853889766 * get_sourceColumns_2() const { return ___sourceColumns_2; }
	inline Hashtable_t1853889766 ** get_address_of_sourceColumns_2() { return &___sourceColumns_2; }
	inline void set_sourceColumns_2(Hashtable_t1853889766 * value)
	{
		___sourceColumns_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceColumns_2), value);
	}

	inline static int32_t get_offset_of_dataSetColumns_3() { return static_cast<int32_t>(offsetof(DataColumnMappingCollection_t2426444188, ___dataSetColumns_3)); }
	inline Hashtable_t1853889766 * get_dataSetColumns_3() const { return ___dataSetColumns_3; }
	inline Hashtable_t1853889766 ** get_address_of_dataSetColumns_3() { return &___dataSetColumns_3; }
	inline void set_dataSetColumns_3(Hashtable_t1853889766 * value)
	{
		___dataSetColumns_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetColumns_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNMAPPINGCOLLECTION_T2426444188_H
#ifndef UINT32DATACONTAINER_T1292766739_H
#define UINT32DATACONTAINER_T1292766739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.UInt32DataContainer
struct  UInt32DataContainer_t1292766739  : public DataContainer_t2712869782
{
public:
	// System.UInt32[] System.Data.Common.UInt32DataContainer::_values
	UInt32U5BU5D_t2770800703* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(UInt32DataContainer_t1292766739, ____values_3)); }
	inline UInt32U5BU5D_t2770800703* get__values_3() const { return ____values_3; }
	inline UInt32U5BU5D_t2770800703** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(UInt32U5BU5D_t2770800703* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32DATACONTAINER_T1292766739_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT32DATACONTAINER_T3302098759_H
#define INT32DATACONTAINER_T3302098759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.Int32DataContainer
struct  Int32DataContainer_t3302098759  : public DataContainer_t2712869782
{
public:
	// System.Int32[] System.Data.Common.Int32DataContainer::_values
	Int32U5BU5D_t385246372* ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(Int32DataContainer_t3302098759, ____values_3)); }
	inline Int32U5BU5D_t385246372* get__values_3() const { return ____values_3; }
	inline Int32U5BU5D_t385246372** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(Int32U5BU5D_t385246372* value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32DATACONTAINER_T3302098759_H
#ifndef YYEXCEPTION_T2856183173_H
#define YYEXCEPTION_T2856183173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.yyParser.yyException
struct  yyException_t2856183173  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYEXCEPTION_T2856183173_H
#ifndef YYRULES_T1272530553_H
#define YYRULES_T1272530553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Parser/YYRules
struct  YYRules_t1272530553  : public MarshalByRefObject_t2760389100
{
public:

public:
};

struct YYRules_t1272530553_StaticFields
{
public:
	// System.String[] Mono.Data.SqlExpressions.Parser/YYRules::yyRule
	StringU5BU5D_t1281789340* ___yyRule_1;

public:
	inline static int32_t get_offset_of_yyRule_1() { return static_cast<int32_t>(offsetof(YYRules_t1272530553_StaticFields, ___yyRule_1)); }
	inline StringU5BU5D_t1281789340* get_yyRule_1() const { return ___yyRule_1; }
	inline StringU5BU5D_t1281789340** get_address_of_yyRule_1() { return &___yyRule_1; }
	inline void set_yyRule_1(StringU5BU5D_t1281789340* value)
	{
		___yyRule_1 = value;
		Il2CppCodeGenWriteBarrier((&___yyRule_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYRULES_T1272530553_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef U24ARRAYTYPEU241280_T4290130235_H
#define U24ARRAYTYPEU241280_T4290130235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$1280
struct  U24ArrayTypeU241280_t4290130235 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241280_t4290130235__padding[1280];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241280_T4290130235_H
#ifndef U24ARRAYTYPEU24256_T1929481985_H
#define U24ARRAYTYPEU24256_T1929481985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$256
struct  U24ArrayTypeU24256_t1929481985 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t1929481985__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24256_T1929481985_H
#ifndef XMLURLRESOLVER_T817895037_H
#define XMLURLRESOLVER_T817895037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver
struct  XmlUrlResolver_t817895037  : public XmlResolver_t626023767
{
public:
	// System.Net.ICredentials System.Xml.XmlUrlResolver::credential
	RuntimeObject* ___credential_0;

public:
	inline static int32_t get_offset_of_credential_0() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t817895037, ___credential_0)); }
	inline RuntimeObject* get_credential_0() const { return ___credential_0; }
	inline RuntimeObject** get_address_of_credential_0() { return &___credential_0; }
	inline void set_credential_0(RuntimeObject* value)
	{
		___credential_0 = value;
		Il2CppCodeGenWriteBarrier((&___credential_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLURLRESOLVER_T817895037_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef XMLLINKEDNODE_T1437094927_H
#define XMLLINKEDNODE_T1437094927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1437094927  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::nextSibling
	XmlLinkedNode_t1437094927 * ___nextSibling_6;

public:
	inline static int32_t get_offset_of_nextSibling_6() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1437094927, ___nextSibling_6)); }
	inline XmlLinkedNode_t1437094927 * get_nextSibling_6() const { return ___nextSibling_6; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_nextSibling_6() { return &___nextSibling_6; }
	inline void set_nextSibling_6(XmlLinkedNode_t1437094927 * value)
	{
		___nextSibling_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextSibling_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T1437094927_H
#ifndef COMPONENT_T3620823400_H
#define COMPONENT_T3620823400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t3620823400  : public MarshalByRefObject_t2760389100
{
public:
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::event_handlers
	EventHandlerList_t1108123056 * ___event_handlers_1;
	// System.ComponentModel.ISite System.ComponentModel.Component::mySite
	RuntimeObject* ___mySite_2;
	// System.Object System.ComponentModel.Component::disposedEvent
	RuntimeObject * ___disposedEvent_3;

public:
	inline static int32_t get_offset_of_event_handlers_1() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___event_handlers_1)); }
	inline EventHandlerList_t1108123056 * get_event_handlers_1() const { return ___event_handlers_1; }
	inline EventHandlerList_t1108123056 ** get_address_of_event_handlers_1() { return &___event_handlers_1; }
	inline void set_event_handlers_1(EventHandlerList_t1108123056 * value)
	{
		___event_handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___event_handlers_1), value);
	}

	inline static int32_t get_offset_of_mySite_2() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___mySite_2)); }
	inline RuntimeObject* get_mySite_2() const { return ___mySite_2; }
	inline RuntimeObject** get_address_of_mySite_2() { return &___mySite_2; }
	inline void set_mySite_2(RuntimeObject* value)
	{
		___mySite_2 = value;
		Il2CppCodeGenWriteBarrier((&___mySite_2), value);
	}

	inline static int32_t get_offset_of_disposedEvent_3() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___disposedEvent_3)); }
	inline RuntimeObject * get_disposedEvent_3() const { return ___disposedEvent_3; }
	inline RuntimeObject ** get_address_of_disposedEvent_3() { return &___disposedEvent_3; }
	inline void set_disposedEvent_3(RuntimeObject * value)
	{
		___disposedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___disposedEvent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3620823400_H
#ifndef U24ARRAYTYPEU2412_T2490092599_H
#define U24ARRAYTYPEU2412_T2490092599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$12
struct  U24ArrayTypeU2412_t2490092599 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2412_t2490092599__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2412_T2490092599_H
#ifndef U24ARRAYTYPEU248_T3244137464_H
#define U24ARRAYTYPEU248_T3244137464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$8
struct  U24ArrayTypeU248_t3244137464 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU248_t3244137464__padding[8];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU248_T3244137464_H
#ifndef MONOTODOATTRIBUTE_T4131080585_H
#define MONOTODOATTRIBUTE_T4131080585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t4131080585  : public Attribute_t861562559
{
public:
	// System.String System.MonoTODOAttribute::comment
	String_t* ___comment_0;

public:
	inline static int32_t get_offset_of_comment_0() { return static_cast<int32_t>(offsetof(MonoTODOAttribute_t4131080585, ___comment_0)); }
	inline String_t* get_comment_0() const { return ___comment_0; }
	inline String_t** get_address_of_comment_0() { return &___comment_0; }
	inline void set_comment_0(String_t* value)
	{
		___comment_0 = value;
		Il2CppCodeGenWriteBarrier((&___comment_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T4131080585_H
#ifndef WRITESTATE_T3983380671_H
#define WRITESTATE_T3983380671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t3983380671 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t3983380671, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T3983380671_H
#ifndef WHITESPACEHANDLING_T784045650_H
#define WHITESPACEHANDLING_T784045650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t784045650 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t784045650, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T784045650_H
#ifndef XMLNODETYPE_T1672767151_H
#define XMLNODETYPE_T1672767151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t1672767151 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_t1672767151, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T1672767151_H
#ifndef READSTATE_T944984020_H
#define READSTATE_T944984020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_t944984020 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t944984020, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T944984020_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef XMLSPACE_T3324193251_H
#define XMLSPACE_T3324193251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_t3324193251 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSpace_t3324193251, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_T3324193251_H
#ifndef XMLCHARACTERDATA_T1167807131_H
#define XMLCHARACTERDATA_T1167807131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t1167807131  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_7;

public:
	inline static int32_t get_offset_of_data_7() { return static_cast<int32_t>(offsetof(XmlCharacterData_t1167807131, ___data_7)); }
	inline String_t* get_data_7() const { return ___data_7; }
	inline String_t** get_address_of_data_7() { return &___data_7; }
	inline void set_data_7(String_t* value)
	{
		___data_7 = value;
		Il2CppCodeGenWriteBarrier((&___data_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T1167807131_H
#ifndef CONFORMANCELEVEL_T3899847875_H
#define CONFORMANCELEVEL_T3899847875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t3899847875 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_t3899847875, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T3899847875_H
#ifndef DBCONNECTION_T1716984062_H
#define DBCONNECTION_T1716984062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbConnection
struct  DbConnection_t1716984062  : public Component_t3620823400
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBCONNECTION_T1716984062_H
#ifndef DECIMALDATACONTAINER_T1211020912_H
#define DECIMALDATACONTAINER_T1211020912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DecimalDataContainer
struct  DecimalDataContainer_t1211020912  : public ObjectDataContainer_t3603397982
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALDATACONTAINER_T1211020912_H
#ifndef NAMESPACEHANDLING_T4087553436_H
#define NAMESPACEHANDLING_T4087553436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t4087553436 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t4087553436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T4087553436_H
#ifndef STRINGDATACONTAINER_T3348382435_H
#define STRINGDATACONTAINER_T3348382435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.StringDataContainer
struct  StringDataContainer_t3348382435  : public ObjectDataContainer_t3603397982
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGDATACONTAINER_T3348382435_H
#ifndef XMLOUTPUTMETHOD_T2185361861_H
#define XMLOUTPUTMETHOD_T2185361861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_t2185361861 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlOutputMethod_t2185361861, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLOUTPUTMETHOD_T2185361861_H
#ifndef NEWLINEHANDLING_T850339274_H
#define NEWLINEHANDLING_T850339274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t850339274 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_t850339274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T850339274_H
#ifndef ENTITYHANDLING_T1047276436_H
#define ENTITYHANDLING_T1047276436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t1047276436 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityHandling_t1047276436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T1047276436_H
#ifndef DBCOMMAND_T187824494_H
#define DBCOMMAND_T187824494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbCommand
struct  DbCommand_t187824494  : public Component_t3620823400
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBCOMMAND_T187824494_H
#ifndef DATETIMEDATACONTAINER_T145609085_H
#define DATETIMEDATACONTAINER_T145609085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DateTimeDataContainer
struct  DateTimeDataContainer_t145609085  : public ObjectDataContainer_t3603397982
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEDATACONTAINER_T145609085_H
#ifndef ISNULLFUNCTION_T3547063577_H
#define ISNULLFUNCTION_T3547063577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.IsNullFunction
struct  IsNullFunction_t3547063577  : public UnaryExpression_t1939092167
{
public:
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.IsNullFunction::defaultExpr
	RuntimeObject* ___defaultExpr_1;

public:
	inline static int32_t get_offset_of_defaultExpr_1() { return static_cast<int32_t>(offsetof(IsNullFunction_t3547063577, ___defaultExpr_1)); }
	inline RuntimeObject* get_defaultExpr_1() const { return ___defaultExpr_1; }
	inline RuntimeObject** get_address_of_defaultExpr_1() { return &___defaultExpr_1; }
	inline void set_defaultExpr_1(RuntimeObject* value)
	{
		___defaultExpr_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultExpr_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISNULLFUNCTION_T3547063577_H
#ifndef IIFFUNCTION_T332110100_H
#define IIFFUNCTION_T332110100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.IifFunction
struct  IifFunction_t332110100  : public UnaryExpression_t1939092167
{
public:
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.IifFunction::trueExpr
	RuntimeObject* ___trueExpr_1;
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.IifFunction::falseExpr
	RuntimeObject* ___falseExpr_2;

public:
	inline static int32_t get_offset_of_trueExpr_1() { return static_cast<int32_t>(offsetof(IifFunction_t332110100, ___trueExpr_1)); }
	inline RuntimeObject* get_trueExpr_1() const { return ___trueExpr_1; }
	inline RuntimeObject** get_address_of_trueExpr_1() { return &___trueExpr_1; }
	inline void set_trueExpr_1(RuntimeObject* value)
	{
		___trueExpr_1 = value;
		Il2CppCodeGenWriteBarrier((&___trueExpr_1), value);
	}

	inline static int32_t get_offset_of_falseExpr_2() { return static_cast<int32_t>(offsetof(IifFunction_t332110100, ___falseExpr_2)); }
	inline RuntimeObject* get_falseExpr_2() const { return ___falseExpr_2; }
	inline RuntimeObject** get_address_of_falseExpr_2() { return &___falseExpr_2; }
	inline void set_falseExpr_2(RuntimeObject* value)
	{
		___falseExpr_2 = value;
		Il2CppCodeGenWriteBarrier((&___falseExpr_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IIFFUNCTION_T332110100_H
#ifndef XMLDECLSTATE_T1683299469_H
#define XMLDECLSTATE_T1683299469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/XmlDeclState
struct  XmlDeclState_t1683299469 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/XmlDeclState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlDeclState_t1683299469, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLSTATE_T1683299469_H
#ifndef OPERATION_T1922278484_H
#define OPERATION_T1922278484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Operation
struct  Operation_t1922278484 
{
public:
	// System.Int32 Mono.Data.SqlExpressions.Operation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Operation_t1922278484, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATION_T1922278484_H
#ifndef YYUNEXPECTEDEOF_T141146907_H
#define YYUNEXPECTEDEOF_T141146907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.yyParser.yyUnexpectedEof
struct  yyUnexpectedEof_t141146907  : public yyException_t2856183173
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYUNEXPECTEDEOF_T141146907_H
#ifndef MONONOTSUPPORTEDATTRIBUTE_T2563528020_H
#define MONONOTSUPPORTEDATTRIBUTE_T2563528020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoNotSupportedAttribute
struct  MonoNotSupportedAttribute_t2563528020  : public MonoTODOAttribute_t4131080585
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONONOTSUPPORTEDATTRIBUTE_T2563528020_H
#ifndef AGGREGATIONFUNCTION_T2631941729_H
#define AGGREGATIONFUNCTION_T2631941729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.AggregationFunction
struct  AggregationFunction_t2631941729 
{
public:
	// System.Int32 Mono.Data.SqlExpressions.AggregationFunction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AggregationFunction_t2631941729, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATIONFUNCTION_T2631941729_H
#ifndef NEGATIVE_T3692748921_H
#define NEGATIVE_T3692748921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Negative
struct  Negative_t3692748921  : public UnaryExpression_t1939092167
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGATIVE_T3692748921_H
#ifndef REFERENCEDTABLE_T3986959547_H
#define REFERENCEDTABLE_T3986959547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.ReferencedTable
struct  ReferencedTable_t3986959547 
{
public:
	// System.Int32 Mono.Data.SqlExpressions.ReferencedTable::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReferencedTable_t3986959547, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEDTABLE_T3986959547_H
#ifndef CONVERTFUNCTION_T4260638137_H
#define CONVERTFUNCTION_T4260638137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.ConvertFunction
struct  ConvertFunction_t4260638137  : public UnaryExpression_t1939092167
{
public:
	// System.Type Mono.Data.SqlExpressions.ConvertFunction::targetType
	Type_t * ___targetType_1;

public:
	inline static int32_t get_offset_of_targetType_1() { return static_cast<int32_t>(offsetof(ConvertFunction_t4260638137, ___targetType_1)); }
	inline Type_t * get_targetType_1() const { return ___targetType_1; }
	inline Type_t ** get_address_of_targetType_1() { return &___targetType_1; }
	inline void set_targetType_1(Type_t * value)
	{
		___targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&___targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTFUNCTION_T4260638137_H
#ifndef COMMANDSTATE_T1020432923_H
#define COMMANDSTATE_T1020432923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport/CommandState
struct  CommandState_t1020432923 
{
public:
	// System.Int32 System.Xml.XmlReaderBinarySupport/CommandState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CommandState_t1020432923, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDSTATE_T1020432923_H
#ifndef DATAADAPTER_T2689867586_H
#define DATAADAPTER_T2689867586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DataAdapter
struct  DataAdapter_t2689867586  : public Component_t3620823400
{
public:
	// System.Data.Common.DataTableMappingCollection System.Data.Common.DataAdapter::tableMappings
	DataTableMappingCollection_t2364717775 * ___tableMappings_4;

public:
	inline static int32_t get_offset_of_tableMappings_4() { return static_cast<int32_t>(offsetof(DataAdapter_t2689867586, ___tableMappings_4)); }
	inline DataTableMappingCollection_t2364717775 * get_tableMappings_4() const { return ___tableMappings_4; }
	inline DataTableMappingCollection_t2364717775 ** get_address_of_tableMappings_4() { return &___tableMappings_4; }
	inline void set_tableMappings_4(DataTableMappingCollection_t2364717775 * value)
	{
		___tableMappings_4 = value;
		Il2CppCodeGenWriteBarrier((&___tableMappings_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAADAPTER_T2689867586_H
#ifndef DTDINPUTSTATE_T1766821130_H
#define DTDINPUTSTATE_T1766821130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/DtdInputState
struct  DtdInputState_t1766821130 
{
public:
	// System.Int32 Mono.Xml2.XmlTextReader/DtdInputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DtdInputState_t1766821130, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINPUTSTATE_T1766821130_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-23
	U24ArrayTypeU248_t3244137464  ___U24U24fieldU2D23_0;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-24
	U24ArrayTypeU248_t3244137464  ___U24U24fieldU2D24_1;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-26
	U24ArrayTypeU248_t3244137464  ___U24U24fieldU2D26_2;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-27
	U24ArrayTypeU24256_t1929481985  ___U24U24fieldU2D27_3;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-28
	U24ArrayTypeU24256_t1929481985  ___U24U24fieldU2D28_4;
	// <PrivateImplementationDetails>/$ArrayType$1280 <PrivateImplementationDetails>::$$field-29
	U24ArrayTypeU241280_t4290130235  ___U24U24fieldU2D29_5;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-43
	U24ArrayTypeU2412_t2490092599  ___U24U24fieldU2D43_6;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-44
	U24ArrayTypeU2412_t2490092599  ___U24U24fieldU2D44_7;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D23_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D23_0)); }
	inline U24ArrayTypeU248_t3244137464  get_U24U24fieldU2D23_0() const { return ___U24U24fieldU2D23_0; }
	inline U24ArrayTypeU248_t3244137464 * get_address_of_U24U24fieldU2D23_0() { return &___U24U24fieldU2D23_0; }
	inline void set_U24U24fieldU2D23_0(U24ArrayTypeU248_t3244137464  value)
	{
		___U24U24fieldU2D23_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D24_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D24_1)); }
	inline U24ArrayTypeU248_t3244137464  get_U24U24fieldU2D24_1() const { return ___U24U24fieldU2D24_1; }
	inline U24ArrayTypeU248_t3244137464 * get_address_of_U24U24fieldU2D24_1() { return &___U24U24fieldU2D24_1; }
	inline void set_U24U24fieldU2D24_1(U24ArrayTypeU248_t3244137464  value)
	{
		___U24U24fieldU2D24_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D26_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D26_2)); }
	inline U24ArrayTypeU248_t3244137464  get_U24U24fieldU2D26_2() const { return ___U24U24fieldU2D26_2; }
	inline U24ArrayTypeU248_t3244137464 * get_address_of_U24U24fieldU2D26_2() { return &___U24U24fieldU2D26_2; }
	inline void set_U24U24fieldU2D26_2(U24ArrayTypeU248_t3244137464  value)
	{
		___U24U24fieldU2D26_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D27_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D27_3)); }
	inline U24ArrayTypeU24256_t1929481985  get_U24U24fieldU2D27_3() const { return ___U24U24fieldU2D27_3; }
	inline U24ArrayTypeU24256_t1929481985 * get_address_of_U24U24fieldU2D27_3() { return &___U24U24fieldU2D27_3; }
	inline void set_U24U24fieldU2D27_3(U24ArrayTypeU24256_t1929481985  value)
	{
		___U24U24fieldU2D27_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D28_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D28_4)); }
	inline U24ArrayTypeU24256_t1929481985  get_U24U24fieldU2D28_4() const { return ___U24U24fieldU2D28_4; }
	inline U24ArrayTypeU24256_t1929481985 * get_address_of_U24U24fieldU2D28_4() { return &___U24U24fieldU2D28_4; }
	inline void set_U24U24fieldU2D28_4(U24ArrayTypeU24256_t1929481985  value)
	{
		___U24U24fieldU2D28_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D29_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D29_5)); }
	inline U24ArrayTypeU241280_t4290130235  get_U24U24fieldU2D29_5() const { return ___U24U24fieldU2D29_5; }
	inline U24ArrayTypeU241280_t4290130235 * get_address_of_U24U24fieldU2D29_5() { return &___U24U24fieldU2D29_5; }
	inline void set_U24U24fieldU2D29_5(U24ArrayTypeU241280_t4290130235  value)
	{
		___U24U24fieldU2D29_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D43_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D43_6)); }
	inline U24ArrayTypeU2412_t2490092599  get_U24U24fieldU2D43_6() const { return ___U24U24fieldU2D43_6; }
	inline U24ArrayTypeU2412_t2490092599 * get_address_of_U24U24fieldU2D43_6() { return &___U24U24fieldU2D43_6; }
	inline void set_U24U24fieldU2D43_6(U24ArrayTypeU2412_t2490092599  value)
	{
		___U24U24fieldU2D43_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D44_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24U24fieldU2D44_7)); }
	inline U24ArrayTypeU2412_t2490092599  get_U24U24fieldU2D44_7() const { return ___U24U24fieldU2D44_7; }
	inline U24ArrayTypeU2412_t2490092599 * get_address_of_U24U24fieldU2D44_7() { return &___U24U24fieldU2D44_7; }
	inline void set_U24U24fieldU2D44_7(U24ArrayTypeU2412_t2490092599  value)
	{
		___U24U24fieldU2D44_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef NEGATION_T1647173762_H
#define NEGATION_T1647173762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Negation
struct  Negation_t1647173762  : public UnaryExpression_t1939092167
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGATION_T1647173762_H
#ifndef IN_T2577005558_H
#define IN_T2577005558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.In
struct  In_t2577005558  : public UnaryExpression_t1939092167
{
public:
	// System.Collections.IList Mono.Data.SqlExpressions.In::set
	RuntimeObject* ___set_1;

public:
	inline static int32_t get_offset_of_set_1() { return static_cast<int32_t>(offsetof(In_t2577005558, ___set_1)); }
	inline RuntimeObject* get_set_1() const { return ___set_1; }
	inline RuntimeObject** get_address_of_set_1() { return &___set_1; }
	inline void set_set_1(RuntimeObject* value)
	{
		___set_1 = value;
		Il2CppCodeGenWriteBarrier((&___set_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IN_T2577005558_H
#ifndef XMLTOKENIZEDTYPE_T3348692225_H
#define XMLTOKENIZEDTYPE_T3348692225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTokenizedType
struct  XmlTokenizedType_t3348692225 
{
public:
	// System.Int32 System.Xml.XmlTokenizedType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlTokenizedType_t3348692225, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTOKENIZEDTYPE_T3348692225_H
#ifndef LIKE_T3391805775_H
#define LIKE_T3391805775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Like
struct  Like_t3391805775  : public UnaryExpression_t1939092167
{
public:
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.Like::_pattern
	RuntimeObject* ____pattern_1;

public:
	inline static int32_t get_offset_of__pattern_1() { return static_cast<int32_t>(offsetof(Like_t3391805775, ____pattern_1)); }
	inline RuntimeObject* get__pattern_1() const { return ____pattern_1; }
	inline RuntimeObject** get_address_of__pattern_1() { return &____pattern_1; }
	inline void set__pattern_1(RuntimeObject* value)
	{
		____pattern_1 = value;
		Il2CppCodeGenWriteBarrier((&____pattern_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIKE_T3391805775_H
#ifndef STRINGFUNCTION_T522225446_H
#define STRINGFUNCTION_T522225446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.StringFunction
struct  StringFunction_t522225446  : public UnaryExpression_t1939092167
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGFUNCTION_T522225446_H
#ifndef AGGREGATION_T2105721107_H
#define AGGREGATION_T2105721107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Aggregation
struct  Aggregation_t2105721107  : public BaseExpression_t4150592444
{
public:
	// System.Boolean Mono.Data.SqlExpressions.Aggregation::cacheResults
	bool ___cacheResults_0;
	// System.Data.DataRow[] Mono.Data.SqlExpressions.Aggregation::rows
	DataRowU5BU5D_t3122106530* ___rows_1;
	// Mono.Data.SqlExpressions.ColumnReference Mono.Data.SqlExpressions.Aggregation::column
	ColumnReference_t94354283 * ___column_2;
	// Mono.Data.SqlExpressions.AggregationFunction Mono.Data.SqlExpressions.Aggregation::function
	int32_t ___function_3;
	// System.Int32 Mono.Data.SqlExpressions.Aggregation::count
	int32_t ___count_4;
	// System.IConvertible Mono.Data.SqlExpressions.Aggregation::result
	RuntimeObject* ___result_5;
	// System.Data.DataRowChangeEventHandler Mono.Data.SqlExpressions.Aggregation::RowChangeHandler
	DataRowChangeEventHandler_t1758895939 * ___RowChangeHandler_6;
	// System.Data.DataTable Mono.Data.SqlExpressions.Aggregation::table
	DataTable_t1734486485 * ___table_7;

public:
	inline static int32_t get_offset_of_cacheResults_0() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___cacheResults_0)); }
	inline bool get_cacheResults_0() const { return ___cacheResults_0; }
	inline bool* get_address_of_cacheResults_0() { return &___cacheResults_0; }
	inline void set_cacheResults_0(bool value)
	{
		___cacheResults_0 = value;
	}

	inline static int32_t get_offset_of_rows_1() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___rows_1)); }
	inline DataRowU5BU5D_t3122106530* get_rows_1() const { return ___rows_1; }
	inline DataRowU5BU5D_t3122106530** get_address_of_rows_1() { return &___rows_1; }
	inline void set_rows_1(DataRowU5BU5D_t3122106530* value)
	{
		___rows_1 = value;
		Il2CppCodeGenWriteBarrier((&___rows_1), value);
	}

	inline static int32_t get_offset_of_column_2() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___column_2)); }
	inline ColumnReference_t94354283 * get_column_2() const { return ___column_2; }
	inline ColumnReference_t94354283 ** get_address_of_column_2() { return &___column_2; }
	inline void set_column_2(ColumnReference_t94354283 * value)
	{
		___column_2 = value;
		Il2CppCodeGenWriteBarrier((&___column_2), value);
	}

	inline static int32_t get_offset_of_function_3() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___function_3)); }
	inline int32_t get_function_3() const { return ___function_3; }
	inline int32_t* get_address_of_function_3() { return &___function_3; }
	inline void set_function_3(int32_t value)
	{
		___function_3 = value;
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_result_5() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___result_5)); }
	inline RuntimeObject* get_result_5() const { return ___result_5; }
	inline RuntimeObject** get_address_of_result_5() { return &___result_5; }
	inline void set_result_5(RuntimeObject* value)
	{
		___result_5 = value;
		Il2CppCodeGenWriteBarrier((&___result_5), value);
	}

	inline static int32_t get_offset_of_RowChangeHandler_6() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___RowChangeHandler_6)); }
	inline DataRowChangeEventHandler_t1758895939 * get_RowChangeHandler_6() const { return ___RowChangeHandler_6; }
	inline DataRowChangeEventHandler_t1758895939 ** get_address_of_RowChangeHandler_6() { return &___RowChangeHandler_6; }
	inline void set_RowChangeHandler_6(DataRowChangeEventHandler_t1758895939 * value)
	{
		___RowChangeHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___RowChangeHandler_6), value);
	}

	inline static int32_t get_offset_of_table_7() { return static_cast<int32_t>(offsetof(Aggregation_t2105721107, ___table_7)); }
	inline DataTable_t1734486485 * get_table_7() const { return ___table_7; }
	inline DataTable_t1734486485 ** get_address_of_table_7() { return &___table_7; }
	inline void set_table_7(DataTable_t1734486485 * value)
	{
		___table_7 = value;
		Il2CppCodeGenWriteBarrier((&___table_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATION_T2105721107_H
#ifndef XMLNODEINFO_T4030693883_H
#define XMLNODEINFO_T4030693883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/XmlNodeInfo
struct  XmlNodeInfo_t4030693883  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::NS
	String_t* ___NS_2;
	// System.Boolean System.Xml.XmlTextWriter/XmlNodeInfo::HasSimple
	bool ___HasSimple_3;
	// System.Boolean System.Xml.XmlTextWriter/XmlNodeInfo::HasElements
	bool ___HasElements_4;
	// System.String System.Xml.XmlTextWriter/XmlNodeInfo::XmlLang
	String_t* ___XmlLang_5;
	// System.Xml.XmlSpace System.Xml.XmlTextWriter/XmlNodeInfo::XmlSpace
	int32_t ___XmlSpace_6;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t4030693883, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t4030693883, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t4030693883, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_HasSimple_3() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t4030693883, ___HasSimple_3)); }
	inline bool get_HasSimple_3() const { return ___HasSimple_3; }
	inline bool* get_address_of_HasSimple_3() { return &___HasSimple_3; }
	inline void set_HasSimple_3(bool value)
	{
		___HasSimple_3 = value;
	}

	inline static int32_t get_offset_of_HasElements_4() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t4030693883, ___HasElements_4)); }
	inline bool get_HasElements_4() const { return ___HasElements_4; }
	inline bool* get_address_of_HasElements_4() { return &___HasElements_4; }
	inline void set_HasElements_4(bool value)
	{
		___HasElements_4 = value;
	}

	inline static int32_t get_offset_of_XmlLang_5() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t4030693883, ___XmlLang_5)); }
	inline String_t* get_XmlLang_5() const { return ___XmlLang_5; }
	inline String_t** get_address_of_XmlLang_5() { return &___XmlLang_5; }
	inline void set_XmlLang_5(String_t* value)
	{
		___XmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___XmlLang_5), value);
	}

	inline static int32_t get_offset_of_XmlSpace_6() { return static_cast<int32_t>(offsetof(XmlNodeInfo_t4030693883, ___XmlSpace_6)); }
	inline int32_t get_XmlSpace_6() const { return ___XmlSpace_6; }
	inline int32_t* get_address_of_XmlSpace_6() { return &___XmlSpace_6; }
	inline void set_XmlSpace_6(int32_t value)
	{
		___XmlSpace_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEINFO_T4030693883_H
#ifndef XMLWRITERSETTINGS_T3314986516_H
#define XMLWRITERSETTINGS_T3314986516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriterSettings
struct  XmlWriterSettings_t3314986516  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlWriterSettings::checkCharacters
	bool ___checkCharacters_0;
	// System.Boolean System.Xml.XmlWriterSettings::closeOutput
	bool ___closeOutput_1;
	// System.Xml.ConformanceLevel System.Xml.XmlWriterSettings::conformance
	int32_t ___conformance_2;
	// System.Text.Encoding System.Xml.XmlWriterSettings::encoding
	Encoding_t1523322056 * ___encoding_3;
	// System.Boolean System.Xml.XmlWriterSettings::indent
	bool ___indent_4;
	// System.String System.Xml.XmlWriterSettings::indentChars
	String_t* ___indentChars_5;
	// System.String System.Xml.XmlWriterSettings::newLineChars
	String_t* ___newLineChars_6;
	// System.Boolean System.Xml.XmlWriterSettings::newLineOnAttributes
	bool ___newLineOnAttributes_7;
	// System.Xml.NewLineHandling System.Xml.XmlWriterSettings::newLineHandling
	int32_t ___newLineHandling_8;
	// System.Boolean System.Xml.XmlWriterSettings::omitXmlDeclaration
	bool ___omitXmlDeclaration_9;
	// System.Xml.XmlOutputMethod System.Xml.XmlWriterSettings::outputMethod
	int32_t ___outputMethod_10;
	// System.Xml.NamespaceHandling System.Xml.XmlWriterSettings::<NamespaceHandling>k__BackingField
	int32_t ___U3CNamespaceHandlingU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_checkCharacters_0() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___checkCharacters_0)); }
	inline bool get_checkCharacters_0() const { return ___checkCharacters_0; }
	inline bool* get_address_of_checkCharacters_0() { return &___checkCharacters_0; }
	inline void set_checkCharacters_0(bool value)
	{
		___checkCharacters_0 = value;
	}

	inline static int32_t get_offset_of_closeOutput_1() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___closeOutput_1)); }
	inline bool get_closeOutput_1() const { return ___closeOutput_1; }
	inline bool* get_address_of_closeOutput_1() { return &___closeOutput_1; }
	inline void set_closeOutput_1(bool value)
	{
		___closeOutput_1 = value;
	}

	inline static int32_t get_offset_of_conformance_2() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___conformance_2)); }
	inline int32_t get_conformance_2() const { return ___conformance_2; }
	inline int32_t* get_address_of_conformance_2() { return &___conformance_2; }
	inline void set_conformance_2(int32_t value)
	{
		___conformance_2 = value;
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___encoding_3)); }
	inline Encoding_t1523322056 * get_encoding_3() const { return ___encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(Encoding_t1523322056 * value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_indent_4() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___indent_4)); }
	inline bool get_indent_4() const { return ___indent_4; }
	inline bool* get_address_of_indent_4() { return &___indent_4; }
	inline void set_indent_4(bool value)
	{
		___indent_4 = value;
	}

	inline static int32_t get_offset_of_indentChars_5() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___indentChars_5)); }
	inline String_t* get_indentChars_5() const { return ___indentChars_5; }
	inline String_t** get_address_of_indentChars_5() { return &___indentChars_5; }
	inline void set_indentChars_5(String_t* value)
	{
		___indentChars_5 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_5), value);
	}

	inline static int32_t get_offset_of_newLineChars_6() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___newLineChars_6)); }
	inline String_t* get_newLineChars_6() const { return ___newLineChars_6; }
	inline String_t** get_address_of_newLineChars_6() { return &___newLineChars_6; }
	inline void set_newLineChars_6(String_t* value)
	{
		___newLineChars_6 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_6), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_7() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___newLineOnAttributes_7)); }
	inline bool get_newLineOnAttributes_7() const { return ___newLineOnAttributes_7; }
	inline bool* get_address_of_newLineOnAttributes_7() { return &___newLineOnAttributes_7; }
	inline void set_newLineOnAttributes_7(bool value)
	{
		___newLineOnAttributes_7 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_8() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___newLineHandling_8)); }
	inline int32_t get_newLineHandling_8() const { return ___newLineHandling_8; }
	inline int32_t* get_address_of_newLineHandling_8() { return &___newLineHandling_8; }
	inline void set_newLineHandling_8(int32_t value)
	{
		___newLineHandling_8 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_9() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___omitXmlDeclaration_9)); }
	inline bool get_omitXmlDeclaration_9() const { return ___omitXmlDeclaration_9; }
	inline bool* get_address_of_omitXmlDeclaration_9() { return &___omitXmlDeclaration_9; }
	inline void set_omitXmlDeclaration_9(bool value)
	{
		___omitXmlDeclaration_9 = value;
	}

	inline static int32_t get_offset_of_outputMethod_10() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___outputMethod_10)); }
	inline int32_t get_outputMethod_10() const { return ___outputMethod_10; }
	inline int32_t* get_address_of_outputMethod_10() { return &___outputMethod_10; }
	inline void set_outputMethod_10(int32_t value)
	{
		___outputMethod_10 = value;
	}

	inline static int32_t get_offset_of_U3CNamespaceHandlingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(XmlWriterSettings_t3314986516, ___U3CNamespaceHandlingU3Ek__BackingField_11)); }
	inline int32_t get_U3CNamespaceHandlingU3Ek__BackingField_11() const { return ___U3CNamespaceHandlingU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CNamespaceHandlingU3Ek__BackingField_11() { return &___U3CNamespaceHandlingU3Ek__BackingField_11; }
	inline void set_U3CNamespaceHandlingU3Ek__BackingField_11(int32_t value)
	{
		___U3CNamespaceHandlingU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITERSETTINGS_T3314986516_H
#ifndef XMLTEXTWRITER_T2114213153_H
#define XMLTEXTWRITER_T2114213153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter
struct  XmlTextWriter_t2114213153  : public XmlWriter_t127905479
{
public:
	// System.IO.Stream System.Xml.XmlTextWriter::base_stream
	Stream_t1273022909 * ___base_stream_4;
	// System.IO.TextWriter System.Xml.XmlTextWriter::source
	TextWriter_t3478189236 * ___source_5;
	// System.IO.TextWriter System.Xml.XmlTextWriter::writer
	TextWriter_t3478189236 * ___writer_6;
	// System.IO.StringWriter System.Xml.XmlTextWriter::preserver
	StringWriter_t802263757 * ___preserver_7;
	// System.String System.Xml.XmlTextWriter::preserved_name
	String_t* ___preserved_name_8;
	// System.Boolean System.Xml.XmlTextWriter::is_preserved_xmlns
	bool ___is_preserved_xmlns_9;
	// System.Boolean System.Xml.XmlTextWriter::allow_doc_fragment
	bool ___allow_doc_fragment_10;
	// System.Boolean System.Xml.XmlTextWriter::close_output_stream
	bool ___close_output_stream_11;
	// System.Boolean System.Xml.XmlTextWriter::ignore_encoding
	bool ___ignore_encoding_12;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_13;
	// System.Xml.XmlTextWriter/XmlDeclState System.Xml.XmlTextWriter::xmldecl_state
	int32_t ___xmldecl_state_14;
	// System.Boolean System.Xml.XmlTextWriter::check_character_validity
	bool ___check_character_validity_15;
	// System.Xml.NewLineHandling System.Xml.XmlTextWriter::newline_handling
	int32_t ___newline_handling_16;
	// System.Boolean System.Xml.XmlTextWriter::is_document_entity
	bool ___is_document_entity_17;
	// System.Xml.WriteState System.Xml.XmlTextWriter::state
	int32_t ___state_18;
	// System.Xml.XmlNodeType System.Xml.XmlTextWriter::node_state
	int32_t ___node_state_19;
	// System.Xml.XmlNamespaceManager System.Xml.XmlTextWriter::nsmanager
	XmlNamespaceManager_t418790500 * ___nsmanager_20;
	// System.Int32 System.Xml.XmlTextWriter::open_count
	int32_t ___open_count_21;
	// System.Xml.XmlTextWriter/XmlNodeInfo[] System.Xml.XmlTextWriter::elements
	XmlNodeInfoU5BU5D_t431163322* ___elements_22;
	// System.Collections.Stack System.Xml.XmlTextWriter::new_local_namespaces
	Stack_t2329662280 * ___new_local_namespaces_23;
	// System.Collections.ArrayList System.Xml.XmlTextWriter::explicit_nsdecls
	ArrayList_t2718874744 * ___explicit_nsdecls_24;
	// System.Xml.NamespaceHandling System.Xml.XmlTextWriter::namespace_handling
	int32_t ___namespace_handling_25;
	// System.Boolean System.Xml.XmlTextWriter::indent
	bool ___indent_26;
	// System.Int32 System.Xml.XmlTextWriter::indent_count
	int32_t ___indent_count_27;
	// System.Char System.Xml.XmlTextWriter::indent_char
	Il2CppChar ___indent_char_28;
	// System.String System.Xml.XmlTextWriter::indent_string
	String_t* ___indent_string_29;
	// System.String System.Xml.XmlTextWriter::newline
	String_t* ___newline_30;
	// System.Boolean System.Xml.XmlTextWriter::indent_attributes
	bool ___indent_attributes_31;
	// System.Char System.Xml.XmlTextWriter::quote_char
	Il2CppChar ___quote_char_32;
	// System.Boolean System.Xml.XmlTextWriter::v2
	bool ___v2_33;

public:
	inline static int32_t get_offset_of_base_stream_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___base_stream_4)); }
	inline Stream_t1273022909 * get_base_stream_4() const { return ___base_stream_4; }
	inline Stream_t1273022909 ** get_address_of_base_stream_4() { return &___base_stream_4; }
	inline void set_base_stream_4(Stream_t1273022909 * value)
	{
		___base_stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_4), value);
	}

	inline static int32_t get_offset_of_source_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___source_5)); }
	inline TextWriter_t3478189236 * get_source_5() const { return ___source_5; }
	inline TextWriter_t3478189236 ** get_address_of_source_5() { return &___source_5; }
	inline void set_source_5(TextWriter_t3478189236 * value)
	{
		___source_5 = value;
		Il2CppCodeGenWriteBarrier((&___source_5), value);
	}

	inline static int32_t get_offset_of_writer_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___writer_6)); }
	inline TextWriter_t3478189236 * get_writer_6() const { return ___writer_6; }
	inline TextWriter_t3478189236 ** get_address_of_writer_6() { return &___writer_6; }
	inline void set_writer_6(TextWriter_t3478189236 * value)
	{
		___writer_6 = value;
		Il2CppCodeGenWriteBarrier((&___writer_6), value);
	}

	inline static int32_t get_offset_of_preserver_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___preserver_7)); }
	inline StringWriter_t802263757 * get_preserver_7() const { return ___preserver_7; }
	inline StringWriter_t802263757 ** get_address_of_preserver_7() { return &___preserver_7; }
	inline void set_preserver_7(StringWriter_t802263757 * value)
	{
		___preserver_7 = value;
		Il2CppCodeGenWriteBarrier((&___preserver_7), value);
	}

	inline static int32_t get_offset_of_preserved_name_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___preserved_name_8)); }
	inline String_t* get_preserved_name_8() const { return ___preserved_name_8; }
	inline String_t** get_address_of_preserved_name_8() { return &___preserved_name_8; }
	inline void set_preserved_name_8(String_t* value)
	{
		___preserved_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___preserved_name_8), value);
	}

	inline static int32_t get_offset_of_is_preserved_xmlns_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___is_preserved_xmlns_9)); }
	inline bool get_is_preserved_xmlns_9() const { return ___is_preserved_xmlns_9; }
	inline bool* get_address_of_is_preserved_xmlns_9() { return &___is_preserved_xmlns_9; }
	inline void set_is_preserved_xmlns_9(bool value)
	{
		___is_preserved_xmlns_9 = value;
	}

	inline static int32_t get_offset_of_allow_doc_fragment_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___allow_doc_fragment_10)); }
	inline bool get_allow_doc_fragment_10() const { return ___allow_doc_fragment_10; }
	inline bool* get_address_of_allow_doc_fragment_10() { return &___allow_doc_fragment_10; }
	inline void set_allow_doc_fragment_10(bool value)
	{
		___allow_doc_fragment_10 = value;
	}

	inline static int32_t get_offset_of_close_output_stream_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___close_output_stream_11)); }
	inline bool get_close_output_stream_11() const { return ___close_output_stream_11; }
	inline bool* get_address_of_close_output_stream_11() { return &___close_output_stream_11; }
	inline void set_close_output_stream_11(bool value)
	{
		___close_output_stream_11 = value;
	}

	inline static int32_t get_offset_of_ignore_encoding_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___ignore_encoding_12)); }
	inline bool get_ignore_encoding_12() const { return ___ignore_encoding_12; }
	inline bool* get_address_of_ignore_encoding_12() { return &___ignore_encoding_12; }
	inline void set_ignore_encoding_12(bool value)
	{
		___ignore_encoding_12 = value;
	}

	inline static int32_t get_offset_of_namespaces_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___namespaces_13)); }
	inline bool get_namespaces_13() const { return ___namespaces_13; }
	inline bool* get_address_of_namespaces_13() { return &___namespaces_13; }
	inline void set_namespaces_13(bool value)
	{
		___namespaces_13 = value;
	}

	inline static int32_t get_offset_of_xmldecl_state_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___xmldecl_state_14)); }
	inline int32_t get_xmldecl_state_14() const { return ___xmldecl_state_14; }
	inline int32_t* get_address_of_xmldecl_state_14() { return &___xmldecl_state_14; }
	inline void set_xmldecl_state_14(int32_t value)
	{
		___xmldecl_state_14 = value;
	}

	inline static int32_t get_offset_of_check_character_validity_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___check_character_validity_15)); }
	inline bool get_check_character_validity_15() const { return ___check_character_validity_15; }
	inline bool* get_address_of_check_character_validity_15() { return &___check_character_validity_15; }
	inline void set_check_character_validity_15(bool value)
	{
		___check_character_validity_15 = value;
	}

	inline static int32_t get_offset_of_newline_handling_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___newline_handling_16)); }
	inline int32_t get_newline_handling_16() const { return ___newline_handling_16; }
	inline int32_t* get_address_of_newline_handling_16() { return &___newline_handling_16; }
	inline void set_newline_handling_16(int32_t value)
	{
		___newline_handling_16 = value;
	}

	inline static int32_t get_offset_of_is_document_entity_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___is_document_entity_17)); }
	inline bool get_is_document_entity_17() const { return ___is_document_entity_17; }
	inline bool* get_address_of_is_document_entity_17() { return &___is_document_entity_17; }
	inline void set_is_document_entity_17(bool value)
	{
		___is_document_entity_17 = value;
	}

	inline static int32_t get_offset_of_state_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___state_18)); }
	inline int32_t get_state_18() const { return ___state_18; }
	inline int32_t* get_address_of_state_18() { return &___state_18; }
	inline void set_state_18(int32_t value)
	{
		___state_18 = value;
	}

	inline static int32_t get_offset_of_node_state_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___node_state_19)); }
	inline int32_t get_node_state_19() const { return ___node_state_19; }
	inline int32_t* get_address_of_node_state_19() { return &___node_state_19; }
	inline void set_node_state_19(int32_t value)
	{
		___node_state_19 = value;
	}

	inline static int32_t get_offset_of_nsmanager_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___nsmanager_20)); }
	inline XmlNamespaceManager_t418790500 * get_nsmanager_20() const { return ___nsmanager_20; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_nsmanager_20() { return &___nsmanager_20; }
	inline void set_nsmanager_20(XmlNamespaceManager_t418790500 * value)
	{
		___nsmanager_20 = value;
		Il2CppCodeGenWriteBarrier((&___nsmanager_20), value);
	}

	inline static int32_t get_offset_of_open_count_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___open_count_21)); }
	inline int32_t get_open_count_21() const { return ___open_count_21; }
	inline int32_t* get_address_of_open_count_21() { return &___open_count_21; }
	inline void set_open_count_21(int32_t value)
	{
		___open_count_21 = value;
	}

	inline static int32_t get_offset_of_elements_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___elements_22)); }
	inline XmlNodeInfoU5BU5D_t431163322* get_elements_22() const { return ___elements_22; }
	inline XmlNodeInfoU5BU5D_t431163322** get_address_of_elements_22() { return &___elements_22; }
	inline void set_elements_22(XmlNodeInfoU5BU5D_t431163322* value)
	{
		___elements_22 = value;
		Il2CppCodeGenWriteBarrier((&___elements_22), value);
	}

	inline static int32_t get_offset_of_new_local_namespaces_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___new_local_namespaces_23)); }
	inline Stack_t2329662280 * get_new_local_namespaces_23() const { return ___new_local_namespaces_23; }
	inline Stack_t2329662280 ** get_address_of_new_local_namespaces_23() { return &___new_local_namespaces_23; }
	inline void set_new_local_namespaces_23(Stack_t2329662280 * value)
	{
		___new_local_namespaces_23 = value;
		Il2CppCodeGenWriteBarrier((&___new_local_namespaces_23), value);
	}

	inline static int32_t get_offset_of_explicit_nsdecls_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___explicit_nsdecls_24)); }
	inline ArrayList_t2718874744 * get_explicit_nsdecls_24() const { return ___explicit_nsdecls_24; }
	inline ArrayList_t2718874744 ** get_address_of_explicit_nsdecls_24() { return &___explicit_nsdecls_24; }
	inline void set_explicit_nsdecls_24(ArrayList_t2718874744 * value)
	{
		___explicit_nsdecls_24 = value;
		Il2CppCodeGenWriteBarrier((&___explicit_nsdecls_24), value);
	}

	inline static int32_t get_offset_of_namespace_handling_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___namespace_handling_25)); }
	inline int32_t get_namespace_handling_25() const { return ___namespace_handling_25; }
	inline int32_t* get_address_of_namespace_handling_25() { return &___namespace_handling_25; }
	inline void set_namespace_handling_25(int32_t value)
	{
		___namespace_handling_25 = value;
	}

	inline static int32_t get_offset_of_indent_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indent_26)); }
	inline bool get_indent_26() const { return ___indent_26; }
	inline bool* get_address_of_indent_26() { return &___indent_26; }
	inline void set_indent_26(bool value)
	{
		___indent_26 = value;
	}

	inline static int32_t get_offset_of_indent_count_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indent_count_27)); }
	inline int32_t get_indent_count_27() const { return ___indent_count_27; }
	inline int32_t* get_address_of_indent_count_27() { return &___indent_count_27; }
	inline void set_indent_count_27(int32_t value)
	{
		___indent_count_27 = value;
	}

	inline static int32_t get_offset_of_indent_char_28() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indent_char_28)); }
	inline Il2CppChar get_indent_char_28() const { return ___indent_char_28; }
	inline Il2CppChar* get_address_of_indent_char_28() { return &___indent_char_28; }
	inline void set_indent_char_28(Il2CppChar value)
	{
		___indent_char_28 = value;
	}

	inline static int32_t get_offset_of_indent_string_29() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indent_string_29)); }
	inline String_t* get_indent_string_29() const { return ___indent_string_29; }
	inline String_t** get_address_of_indent_string_29() { return &___indent_string_29; }
	inline void set_indent_string_29(String_t* value)
	{
		___indent_string_29 = value;
		Il2CppCodeGenWriteBarrier((&___indent_string_29), value);
	}

	inline static int32_t get_offset_of_newline_30() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___newline_30)); }
	inline String_t* get_newline_30() const { return ___newline_30; }
	inline String_t** get_address_of_newline_30() { return &___newline_30; }
	inline void set_newline_30(String_t* value)
	{
		___newline_30 = value;
		Il2CppCodeGenWriteBarrier((&___newline_30), value);
	}

	inline static int32_t get_offset_of_indent_attributes_31() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___indent_attributes_31)); }
	inline bool get_indent_attributes_31() const { return ___indent_attributes_31; }
	inline bool* get_address_of_indent_attributes_31() { return &___indent_attributes_31; }
	inline void set_indent_attributes_31(bool value)
	{
		___indent_attributes_31 = value;
	}

	inline static int32_t get_offset_of_quote_char_32() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___quote_char_32)); }
	inline Il2CppChar get_quote_char_32() const { return ___quote_char_32; }
	inline Il2CppChar* get_address_of_quote_char_32() { return &___quote_char_32; }
	inline void set_quote_char_32(Il2CppChar value)
	{
		___quote_char_32 = value;
	}

	inline static int32_t get_offset_of_v2_33() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153, ___v2_33)); }
	inline bool get_v2_33() const { return ___v2_33; }
	inline bool* get_address_of_v2_33() { return &___v2_33; }
	inline void set_v2_33(bool value)
	{
		___v2_33 = value;
	}
};

struct XmlTextWriter_t2114213153_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlTextWriter::unmarked_utf8encoding
	Encoding_t1523322056 * ___unmarked_utf8encoding_1;
	// System.Char[] System.Xml.XmlTextWriter::escaped_text_chars
	CharU5BU5D_t3528271667* ___escaped_text_chars_2;
	// System.Char[] System.Xml.XmlTextWriter::escaped_attr_chars
	CharU5BU5D_t3528271667* ___escaped_attr_chars_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::<>f__switch$map53
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map53_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::<>f__switch$map54
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map54_35;

public:
	inline static int32_t get_offset_of_unmarked_utf8encoding_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___unmarked_utf8encoding_1)); }
	inline Encoding_t1523322056 * get_unmarked_utf8encoding_1() const { return ___unmarked_utf8encoding_1; }
	inline Encoding_t1523322056 ** get_address_of_unmarked_utf8encoding_1() { return &___unmarked_utf8encoding_1; }
	inline void set_unmarked_utf8encoding_1(Encoding_t1523322056 * value)
	{
		___unmarked_utf8encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unmarked_utf8encoding_1), value);
	}

	inline static int32_t get_offset_of_escaped_text_chars_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___escaped_text_chars_2)); }
	inline CharU5BU5D_t3528271667* get_escaped_text_chars_2() const { return ___escaped_text_chars_2; }
	inline CharU5BU5D_t3528271667** get_address_of_escaped_text_chars_2() { return &___escaped_text_chars_2; }
	inline void set_escaped_text_chars_2(CharU5BU5D_t3528271667* value)
	{
		___escaped_text_chars_2 = value;
		Il2CppCodeGenWriteBarrier((&___escaped_text_chars_2), value);
	}

	inline static int32_t get_offset_of_escaped_attr_chars_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___escaped_attr_chars_3)); }
	inline CharU5BU5D_t3528271667* get_escaped_attr_chars_3() const { return ___escaped_attr_chars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_escaped_attr_chars_3() { return &___escaped_attr_chars_3; }
	inline void set_escaped_attr_chars_3(CharU5BU5D_t3528271667* value)
	{
		___escaped_attr_chars_3 = value;
		Il2CppCodeGenWriteBarrier((&___escaped_attr_chars_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map53_34() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___U3CU3Ef__switchU24map53_34)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map53_34() const { return ___U3CU3Ef__switchU24map53_34; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map53_34() { return &___U3CU3Ef__switchU24map53_34; }
	inline void set_U3CU3Ef__switchU24map53_34(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map53_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map53_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map54_35() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2114213153_StaticFields, ___U3CU3Ef__switchU24map54_35)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map54_35() const { return ___U3CU3Ef__switchU24map54_35; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map54_35() { return &___U3CU3Ef__switchU24map54_35; }
	inline void set_U3CU3Ef__switchU24map54_35(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map54_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map54_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITER_T2114213153_H
#ifndef XMLREADERSETTINGS_T2186285234_H
#define XMLREADERSETTINGS_T2186285234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderSettings
struct  XmlReaderSettings_t2186285234  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlReaderSettings::checkCharacters
	bool ___checkCharacters_0;
	// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::conformance
	int32_t ___conformance_1;

public:
	inline static int32_t get_offset_of_checkCharacters_0() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___checkCharacters_0)); }
	inline bool get_checkCharacters_0() const { return ___checkCharacters_0; }
	inline bool* get_address_of_checkCharacters_0() { return &___checkCharacters_0; }
	inline void set_checkCharacters_0(bool value)
	{
		___checkCharacters_0 = value;
	}

	inline static int32_t get_offset_of_conformance_1() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t2186285234, ___conformance_1)); }
	inline int32_t get_conformance_1() const { return ___conformance_1; }
	inline int32_t* get_address_of_conformance_1() { return &___conformance_1; }
	inline void set_conformance_1(int32_t value)
	{
		___conformance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERSETTINGS_T2186285234_H
#ifndef XMLSIGNIFICANTWHITESPACE_T1052520128_H
#define XMLSIGNIFICANTWHITESPACE_T1052520128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSignificantWhitespace
struct  XmlSignificantWhitespace_t1052520128  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSIGNIFICANTWHITESPACE_T1052520128_H
#ifndef XMLWHITESPACE_T131741354_H
#define XMLWHITESPACE_T131741354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWhitespace
struct  XmlWhitespace_t131741354  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWHITESPACE_T131741354_H
#ifndef BINARYOPEXPRESSION_T3223617729_H
#define BINARYOPEXPRESSION_T3223617729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.BinaryOpExpression
struct  BinaryOpExpression_t3223617729  : public BinaryExpression_t4181220238
{
public:
	// Mono.Data.SqlExpressions.Operation Mono.Data.SqlExpressions.BinaryOpExpression::op
	int32_t ___op_2;

public:
	inline static int32_t get_offset_of_op_2() { return static_cast<int32_t>(offsetof(BinaryOpExpression_t3223617729, ___op_2)); }
	inline int32_t get_op_2() const { return ___op_2; }
	inline int32_t* get_address_of_op_2() { return &___op_2; }
	inline void set_op_2(int32_t value)
	{
		___op_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOPEXPRESSION_T3223617729_H
#ifndef DBDATAADAPTER_T1706016184_H
#define DBDATAADAPTER_T1706016184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbDataAdapter
struct  DbDataAdapter_t1706016184  : public DataAdapter_t2689867586
{
public:
	// System.Data.IDbCommand System.Data.Common.DbDataAdapter::_selectCommand
	RuntimeObject* ____selectCommand_5;
	// System.Data.IDbCommand System.Data.Common.DbDataAdapter::_updateCommand
	RuntimeObject* ____updateCommand_6;
	// System.Data.IDbCommand System.Data.Common.DbDataAdapter::_deleteCommand
	RuntimeObject* ____deleteCommand_7;
	// System.Data.IDbCommand System.Data.Common.DbDataAdapter::_insertCommand
	RuntimeObject* ____insertCommand_8;

public:
	inline static int32_t get_offset_of__selectCommand_5() { return static_cast<int32_t>(offsetof(DbDataAdapter_t1706016184, ____selectCommand_5)); }
	inline RuntimeObject* get__selectCommand_5() const { return ____selectCommand_5; }
	inline RuntimeObject** get_address_of__selectCommand_5() { return &____selectCommand_5; }
	inline void set__selectCommand_5(RuntimeObject* value)
	{
		____selectCommand_5 = value;
		Il2CppCodeGenWriteBarrier((&____selectCommand_5), value);
	}

	inline static int32_t get_offset_of__updateCommand_6() { return static_cast<int32_t>(offsetof(DbDataAdapter_t1706016184, ____updateCommand_6)); }
	inline RuntimeObject* get__updateCommand_6() const { return ____updateCommand_6; }
	inline RuntimeObject** get_address_of__updateCommand_6() { return &____updateCommand_6; }
	inline void set__updateCommand_6(RuntimeObject* value)
	{
		____updateCommand_6 = value;
		Il2CppCodeGenWriteBarrier((&____updateCommand_6), value);
	}

	inline static int32_t get_offset_of__deleteCommand_7() { return static_cast<int32_t>(offsetof(DbDataAdapter_t1706016184, ____deleteCommand_7)); }
	inline RuntimeObject* get__deleteCommand_7() const { return ____deleteCommand_7; }
	inline RuntimeObject** get_address_of__deleteCommand_7() { return &____deleteCommand_7; }
	inline void set__deleteCommand_7(RuntimeObject* value)
	{
		____deleteCommand_7 = value;
		Il2CppCodeGenWriteBarrier((&____deleteCommand_7), value);
	}

	inline static int32_t get_offset_of__insertCommand_8() { return static_cast<int32_t>(offsetof(DbDataAdapter_t1706016184, ____insertCommand_8)); }
	inline RuntimeObject* get__insertCommand_8() const { return ____insertCommand_8; }
	inline RuntimeObject** get_address_of__insertCommand_8() { return &____insertCommand_8; }
	inline void set__insertCommand_8(RuntimeObject* value)
	{
		____insertCommand_8 = value;
		Il2CppCodeGenWriteBarrier((&____insertCommand_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBDATAADAPTER_T1706016184_H
#ifndef CONCATFUNCTION_T3472055775_H
#define CONCATFUNCTION_T3472055775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.ConcatFunction
struct  ConcatFunction_t3472055775  : public StringFunction_t522225446
{
public:
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.ConcatFunction::_add
	RuntimeObject* ____add_1;

public:
	inline static int32_t get_offset_of__add_1() { return static_cast<int32_t>(offsetof(ConcatFunction_t3472055775, ____add_1)); }
	inline RuntimeObject* get__add_1() const { return ____add_1; }
	inline RuntimeObject** get_address_of__add_1() { return &____add_1; }
	inline void set__add_1(RuntimeObject* value)
	{
		____add_1 = value;
		Il2CppCodeGenWriteBarrier((&____add_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCATFUNCTION_T3472055775_H
#ifndef SUBSTRINGFUNCTION_T1161052992_H
#define SUBSTRINGFUNCTION_T1161052992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.SubstringFunction
struct  SubstringFunction_t1161052992  : public StringFunction_t522225446
{
public:
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.SubstringFunction::start
	RuntimeObject* ___start_1;
	// Mono.Data.SqlExpressions.IExpression Mono.Data.SqlExpressions.SubstringFunction::len
	RuntimeObject* ___len_2;

public:
	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(SubstringFunction_t1161052992, ___start_1)); }
	inline RuntimeObject* get_start_1() const { return ___start_1; }
	inline RuntimeObject** get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(RuntimeObject* value)
	{
		___start_1 = value;
		Il2CppCodeGenWriteBarrier((&___start_1), value);
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(SubstringFunction_t1161052992, ___len_2)); }
	inline RuntimeObject* get_len_2() const { return ___len_2; }
	inline RuntimeObject** get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(RuntimeObject* value)
	{
		___len_2 = value;
		Il2CppCodeGenWriteBarrier((&___len_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSTRINGFUNCTION_T1161052992_H
#ifndef LENFUNCTION_T3499950232_H
#define LENFUNCTION_T3499950232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.LenFunction
struct  LenFunction_t3499950232  : public StringFunction_t522225446
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENFUNCTION_T3499950232_H
#ifndef TRIMFUNCTION_T1155110299_H
#define TRIMFUNCTION_T1155110299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.TrimFunction
struct  TrimFunction_t1155110299  : public StringFunction_t522225446
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIMFUNCTION_T1155110299_H
#ifndef XMLTOKENINFO_T2519673037_H
#define XMLTOKENINFO_T2519673037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/XmlTokenInfo
struct  XmlTokenInfo_t2519673037  : public RuntimeObject
{
public:
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::valueCache
	String_t* ___valueCache_0;
	// Mono.Xml2.XmlTextReader Mono.Xml2.XmlTextReader/XmlTokenInfo::Reader
	XmlTextReader_t3455035481 * ___Reader_1;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::Name
	String_t* ___Name_2;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::LocalName
	String_t* ___LocalName_3;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::Prefix
	String_t* ___Prefix_4;
	// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::NamespaceURI
	String_t* ___NamespaceURI_5;
	// System.Boolean Mono.Xml2.XmlTextReader/XmlTokenInfo::IsEmptyElement
	bool ___IsEmptyElement_6;
	// System.Char Mono.Xml2.XmlTextReader/XmlTokenInfo::QuoteChar
	Il2CppChar ___QuoteChar_7;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::LineNumber
	int32_t ___LineNumber_8;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::LinePosition
	int32_t ___LinePosition_9;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::ValueBufferStart
	int32_t ___ValueBufferStart_10;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlTokenInfo::ValueBufferEnd
	int32_t ___ValueBufferEnd_11;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader/XmlTokenInfo::NodeType
	int32_t ___NodeType_12;

public:
	inline static int32_t get_offset_of_valueCache_0() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___valueCache_0)); }
	inline String_t* get_valueCache_0() const { return ___valueCache_0; }
	inline String_t** get_address_of_valueCache_0() { return &___valueCache_0; }
	inline void set_valueCache_0(String_t* value)
	{
		___valueCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___valueCache_0), value);
	}

	inline static int32_t get_offset_of_Reader_1() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___Reader_1)); }
	inline XmlTextReader_t3455035481 * get_Reader_1() const { return ___Reader_1; }
	inline XmlTextReader_t3455035481 ** get_address_of_Reader_1() { return &___Reader_1; }
	inline void set_Reader_1(XmlTextReader_t3455035481 * value)
	{
		___Reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___Reader_1), value);
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_LocalName_3() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___LocalName_3)); }
	inline String_t* get_LocalName_3() const { return ___LocalName_3; }
	inline String_t** get_address_of_LocalName_3() { return &___LocalName_3; }
	inline void set_LocalName_3(String_t* value)
	{
		___LocalName_3 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_3), value);
	}

	inline static int32_t get_offset_of_Prefix_4() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___Prefix_4)); }
	inline String_t* get_Prefix_4() const { return ___Prefix_4; }
	inline String_t** get_address_of_Prefix_4() { return &___Prefix_4; }
	inline void set_Prefix_4(String_t* value)
	{
		___Prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_4), value);
	}

	inline static int32_t get_offset_of_NamespaceURI_5() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___NamespaceURI_5)); }
	inline String_t* get_NamespaceURI_5() const { return ___NamespaceURI_5; }
	inline String_t** get_address_of_NamespaceURI_5() { return &___NamespaceURI_5; }
	inline void set_NamespaceURI_5(String_t* value)
	{
		___NamespaceURI_5 = value;
		Il2CppCodeGenWriteBarrier((&___NamespaceURI_5), value);
	}

	inline static int32_t get_offset_of_IsEmptyElement_6() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___IsEmptyElement_6)); }
	inline bool get_IsEmptyElement_6() const { return ___IsEmptyElement_6; }
	inline bool* get_address_of_IsEmptyElement_6() { return &___IsEmptyElement_6; }
	inline void set_IsEmptyElement_6(bool value)
	{
		___IsEmptyElement_6 = value;
	}

	inline static int32_t get_offset_of_QuoteChar_7() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___QuoteChar_7)); }
	inline Il2CppChar get_QuoteChar_7() const { return ___QuoteChar_7; }
	inline Il2CppChar* get_address_of_QuoteChar_7() { return &___QuoteChar_7; }
	inline void set_QuoteChar_7(Il2CppChar value)
	{
		___QuoteChar_7 = value;
	}

	inline static int32_t get_offset_of_LineNumber_8() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___LineNumber_8)); }
	inline int32_t get_LineNumber_8() const { return ___LineNumber_8; }
	inline int32_t* get_address_of_LineNumber_8() { return &___LineNumber_8; }
	inline void set_LineNumber_8(int32_t value)
	{
		___LineNumber_8 = value;
	}

	inline static int32_t get_offset_of_LinePosition_9() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___LinePosition_9)); }
	inline int32_t get_LinePosition_9() const { return ___LinePosition_9; }
	inline int32_t* get_address_of_LinePosition_9() { return &___LinePosition_9; }
	inline void set_LinePosition_9(int32_t value)
	{
		___LinePosition_9 = value;
	}

	inline static int32_t get_offset_of_ValueBufferStart_10() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___ValueBufferStart_10)); }
	inline int32_t get_ValueBufferStart_10() const { return ___ValueBufferStart_10; }
	inline int32_t* get_address_of_ValueBufferStart_10() { return &___ValueBufferStart_10; }
	inline void set_ValueBufferStart_10(int32_t value)
	{
		___ValueBufferStart_10 = value;
	}

	inline static int32_t get_offset_of_ValueBufferEnd_11() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___ValueBufferEnd_11)); }
	inline int32_t get_ValueBufferEnd_11() const { return ___ValueBufferEnd_11; }
	inline int32_t* get_address_of_ValueBufferEnd_11() { return &___ValueBufferEnd_11; }
	inline void set_ValueBufferEnd_11(int32_t value)
	{
		___ValueBufferEnd_11 = value;
	}

	inline static int32_t get_offset_of_NodeType_12() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2519673037, ___NodeType_12)); }
	inline int32_t get_NodeType_12() const { return ___NodeType_12; }
	inline int32_t* get_address_of_NodeType_12() { return &___NodeType_12; }
	inline void set_NodeType_12(int32_t value)
	{
		___NodeType_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTOKENINFO_T2519673037_H
#ifndef XMLTEXT_T2682211705_H
#define XMLTEXT_T2682211705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlText
struct  XmlText_t2682211705  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXT_T2682211705_H
#ifndef COLUMNREFERENCE_T94354283_H
#define COLUMNREFERENCE_T94354283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.ColumnReference
struct  ColumnReference_t94354283  : public BaseExpression_t4150592444
{
public:
	// Mono.Data.SqlExpressions.ReferencedTable Mono.Data.SqlExpressions.ColumnReference::refTable
	int32_t ___refTable_0;
	// System.String Mono.Data.SqlExpressions.ColumnReference::relationName
	String_t* ___relationName_1;
	// System.String Mono.Data.SqlExpressions.ColumnReference::columnName
	String_t* ___columnName_2;
	// System.Data.DataColumn Mono.Data.SqlExpressions.ColumnReference::_cachedColumn
	DataColumn_t2485786120 * ____cachedColumn_3;
	// System.Data.DataRelation Mono.Data.SqlExpressions.ColumnReference::_cachedRelation
	DataRelation_t2257224951 * ____cachedRelation_4;

public:
	inline static int32_t get_offset_of_refTable_0() { return static_cast<int32_t>(offsetof(ColumnReference_t94354283, ___refTable_0)); }
	inline int32_t get_refTable_0() const { return ___refTable_0; }
	inline int32_t* get_address_of_refTable_0() { return &___refTable_0; }
	inline void set_refTable_0(int32_t value)
	{
		___refTable_0 = value;
	}

	inline static int32_t get_offset_of_relationName_1() { return static_cast<int32_t>(offsetof(ColumnReference_t94354283, ___relationName_1)); }
	inline String_t* get_relationName_1() const { return ___relationName_1; }
	inline String_t** get_address_of_relationName_1() { return &___relationName_1; }
	inline void set_relationName_1(String_t* value)
	{
		___relationName_1 = value;
		Il2CppCodeGenWriteBarrier((&___relationName_1), value);
	}

	inline static int32_t get_offset_of_columnName_2() { return static_cast<int32_t>(offsetof(ColumnReference_t94354283, ___columnName_2)); }
	inline String_t* get_columnName_2() const { return ___columnName_2; }
	inline String_t** get_address_of_columnName_2() { return &___columnName_2; }
	inline void set_columnName_2(String_t* value)
	{
		___columnName_2 = value;
		Il2CppCodeGenWriteBarrier((&___columnName_2), value);
	}

	inline static int32_t get_offset_of__cachedColumn_3() { return static_cast<int32_t>(offsetof(ColumnReference_t94354283, ____cachedColumn_3)); }
	inline DataColumn_t2485786120 * get__cachedColumn_3() const { return ____cachedColumn_3; }
	inline DataColumn_t2485786120 ** get_address_of__cachedColumn_3() { return &____cachedColumn_3; }
	inline void set__cachedColumn_3(DataColumn_t2485786120 * value)
	{
		____cachedColumn_3 = value;
		Il2CppCodeGenWriteBarrier((&____cachedColumn_3), value);
	}

	inline static int32_t get_offset_of__cachedRelation_4() { return static_cast<int32_t>(offsetof(ColumnReference_t94354283, ____cachedRelation_4)); }
	inline DataRelation_t2257224951 * get__cachedRelation_4() const { return ____cachedRelation_4; }
	inline DataRelation_t2257224951 ** get_address_of__cachedRelation_4() { return &____cachedRelation_4; }
	inline void set__cachedRelation_4(DataRelation_t2257224951 * value)
	{
		____cachedRelation_4 = value;
		Il2CppCodeGenWriteBarrier((&____cachedRelation_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMNREFERENCE_T94354283_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef XMLTEXTREADER_T3455035481_H
#define XMLTEXTREADER_T3455035481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader
struct  XmlTextReader_t3455035481  : public XmlReader_t3121518892
{
public:
	// Mono.Xml2.XmlTextReader/XmlTokenInfo Mono.Xml2.XmlTextReader::cursorToken
	XmlTokenInfo_t2519673037 * ___cursorToken_3;
	// Mono.Xml2.XmlTextReader/XmlTokenInfo Mono.Xml2.XmlTextReader::currentToken
	XmlTokenInfo_t2519673037 * ___currentToken_4;
	// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo Mono.Xml2.XmlTextReader::currentAttributeToken
	XmlAttributeTokenInfo_t384315108 * ___currentAttributeToken_5;
	// Mono.Xml2.XmlTextReader/XmlTokenInfo Mono.Xml2.XmlTextReader::currentAttributeValueToken
	XmlTokenInfo_t2519673037 * ___currentAttributeValueToken_6;
	// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo[] Mono.Xml2.XmlTextReader::attributeTokens
	XmlAttributeTokenInfoU5BU5D_t1089758477* ___attributeTokens_7;
	// Mono.Xml2.XmlTextReader/XmlTokenInfo[] Mono.Xml2.XmlTextReader::attributeValueTokens
	XmlTokenInfoU5BU5D_t2522587488* ___attributeValueTokens_8;
	// System.Int32 Mono.Xml2.XmlTextReader::currentAttribute
	int32_t ___currentAttribute_9;
	// System.Int32 Mono.Xml2.XmlTextReader::currentAttributeValue
	int32_t ___currentAttributeValue_10;
	// System.Int32 Mono.Xml2.XmlTextReader::attributeCount
	int32_t ___attributeCount_11;
	// System.Xml.XmlParserContext Mono.Xml2.XmlTextReader::parserContext
	XmlParserContext_t2544895291 * ___parserContext_12;
	// System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::nameTable
	XmlNameTable_t71772148 * ___nameTable_13;
	// System.Xml.XmlNamespaceManager Mono.Xml2.XmlTextReader::nsmgr
	XmlNamespaceManager_t418790500 * ___nsmgr_14;
	// System.Xml.ReadState Mono.Xml2.XmlTextReader::readState
	int32_t ___readState_15;
	// System.Boolean Mono.Xml2.XmlTextReader::disallowReset
	bool ___disallowReset_16;
	// System.Int32 Mono.Xml2.XmlTextReader::depth
	int32_t ___depth_17;
	// System.Int32 Mono.Xml2.XmlTextReader::elementDepth
	int32_t ___elementDepth_18;
	// System.Boolean Mono.Xml2.XmlTextReader::depthUp
	bool ___depthUp_19;
	// System.Boolean Mono.Xml2.XmlTextReader::popScope
	bool ___popScope_20;
	// Mono.Xml2.XmlTextReader/TagName[] Mono.Xml2.XmlTextReader::elementNames
	TagNameU5BU5D_t2055670886* ___elementNames_21;
	// System.Int32 Mono.Xml2.XmlTextReader::elementNameStackPos
	int32_t ___elementNameStackPos_22;
	// System.Boolean Mono.Xml2.XmlTextReader::allowMultipleRoot
	bool ___allowMultipleRoot_23;
	// System.Boolean Mono.Xml2.XmlTextReader::isStandalone
	bool ___isStandalone_24;
	// System.Boolean Mono.Xml2.XmlTextReader::returnEntityReference
	bool ___returnEntityReference_25;
	// System.String Mono.Xml2.XmlTextReader::entityReferenceName
	String_t* ___entityReferenceName_26;
	// System.Text.StringBuilder Mono.Xml2.XmlTextReader::valueBuffer
	StringBuilder_t * ___valueBuffer_27;
	// System.IO.TextReader Mono.Xml2.XmlTextReader::reader
	TextReader_t283511965 * ___reader_28;
	// System.Char[] Mono.Xml2.XmlTextReader::peekChars
	CharU5BU5D_t3528271667* ___peekChars_29;
	// System.Int32 Mono.Xml2.XmlTextReader::peekCharsIndex
	int32_t ___peekCharsIndex_30;
	// System.Int32 Mono.Xml2.XmlTextReader::peekCharsLength
	int32_t ___peekCharsLength_31;
	// System.Int32 Mono.Xml2.XmlTextReader::curNodePeekIndex
	int32_t ___curNodePeekIndex_32;
	// System.Boolean Mono.Xml2.XmlTextReader::preserveCurrentTag
	bool ___preserveCurrentTag_33;
	// System.Int32 Mono.Xml2.XmlTextReader::line
	int32_t ___line_34;
	// System.Int32 Mono.Xml2.XmlTextReader::column
	int32_t ___column_35;
	// System.Int32 Mono.Xml2.XmlTextReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_36;
	// System.Int32 Mono.Xml2.XmlTextReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_37;
	// System.Boolean Mono.Xml2.XmlTextReader::useProceedingLineInfo
	bool ___useProceedingLineInfo_38;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::startNodeType
	int32_t ___startNodeType_39;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::currentState
	int32_t ___currentState_40;
	// System.Int32 Mono.Xml2.XmlTextReader::nestLevel
	int32_t ___nestLevel_41;
	// System.Boolean Mono.Xml2.XmlTextReader::readCharsInProgress
	bool ___readCharsInProgress_42;
	// System.Xml.XmlReaderBinarySupport/CharGetter Mono.Xml2.XmlTextReader::binaryCharGetter
	CharGetter_t1703763694 * ___binaryCharGetter_43;
	// System.Boolean Mono.Xml2.XmlTextReader::namespaces
	bool ___namespaces_44;
	// System.Xml.WhitespaceHandling Mono.Xml2.XmlTextReader::whitespaceHandling
	int32_t ___whitespaceHandling_45;
	// System.Xml.XmlResolver Mono.Xml2.XmlTextReader::resolver
	XmlResolver_t626023767 * ___resolver_46;
	// System.Boolean Mono.Xml2.XmlTextReader::normalization
	bool ___normalization_47;
	// System.Boolean Mono.Xml2.XmlTextReader::checkCharacters
	bool ___checkCharacters_48;
	// System.Boolean Mono.Xml2.XmlTextReader::prohibitDtd
	bool ___prohibitDtd_49;
	// System.Boolean Mono.Xml2.XmlTextReader::closeInput
	bool ___closeInput_50;
	// System.Xml.EntityHandling Mono.Xml2.XmlTextReader::entityHandling
	int32_t ___entityHandling_51;
	// System.Xml.NameTable Mono.Xml2.XmlTextReader::whitespacePool
	NameTable_t3178203267 * ___whitespacePool_52;
	// System.Char[] Mono.Xml2.XmlTextReader::whitespaceCache
	CharU5BU5D_t3528271667* ___whitespaceCache_53;
	// Mono.Xml2.XmlTextReader/DtdInputStateStack Mono.Xml2.XmlTextReader::stateStack
	DtdInputStateStack_t339956957 * ___stateStack_54;

public:
	inline static int32_t get_offset_of_cursorToken_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___cursorToken_3)); }
	inline XmlTokenInfo_t2519673037 * get_cursorToken_3() const { return ___cursorToken_3; }
	inline XmlTokenInfo_t2519673037 ** get_address_of_cursorToken_3() { return &___cursorToken_3; }
	inline void set_cursorToken_3(XmlTokenInfo_t2519673037 * value)
	{
		___cursorToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___cursorToken_3), value);
	}

	inline static int32_t get_offset_of_currentToken_4() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentToken_4)); }
	inline XmlTokenInfo_t2519673037 * get_currentToken_4() const { return ___currentToken_4; }
	inline XmlTokenInfo_t2519673037 ** get_address_of_currentToken_4() { return &___currentToken_4; }
	inline void set_currentToken_4(XmlTokenInfo_t2519673037 * value)
	{
		___currentToken_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentToken_4), value);
	}

	inline static int32_t get_offset_of_currentAttributeToken_5() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentAttributeToken_5)); }
	inline XmlAttributeTokenInfo_t384315108 * get_currentAttributeToken_5() const { return ___currentAttributeToken_5; }
	inline XmlAttributeTokenInfo_t384315108 ** get_address_of_currentAttributeToken_5() { return &___currentAttributeToken_5; }
	inline void set_currentAttributeToken_5(XmlAttributeTokenInfo_t384315108 * value)
	{
		___currentAttributeToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttributeToken_5), value);
	}

	inline static int32_t get_offset_of_currentAttributeValueToken_6() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentAttributeValueToken_6)); }
	inline XmlTokenInfo_t2519673037 * get_currentAttributeValueToken_6() const { return ___currentAttributeValueToken_6; }
	inline XmlTokenInfo_t2519673037 ** get_address_of_currentAttributeValueToken_6() { return &___currentAttributeValueToken_6; }
	inline void set_currentAttributeValueToken_6(XmlTokenInfo_t2519673037 * value)
	{
		___currentAttributeValueToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttributeValueToken_6), value);
	}

	inline static int32_t get_offset_of_attributeTokens_7() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___attributeTokens_7)); }
	inline XmlAttributeTokenInfoU5BU5D_t1089758477* get_attributeTokens_7() const { return ___attributeTokens_7; }
	inline XmlAttributeTokenInfoU5BU5D_t1089758477** get_address_of_attributeTokens_7() { return &___attributeTokens_7; }
	inline void set_attributeTokens_7(XmlAttributeTokenInfoU5BU5D_t1089758477* value)
	{
		___attributeTokens_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributeTokens_7), value);
	}

	inline static int32_t get_offset_of_attributeValueTokens_8() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___attributeValueTokens_8)); }
	inline XmlTokenInfoU5BU5D_t2522587488* get_attributeValueTokens_8() const { return ___attributeValueTokens_8; }
	inline XmlTokenInfoU5BU5D_t2522587488** get_address_of_attributeValueTokens_8() { return &___attributeValueTokens_8; }
	inline void set_attributeValueTokens_8(XmlTokenInfoU5BU5D_t2522587488* value)
	{
		___attributeValueTokens_8 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValueTokens_8), value);
	}

	inline static int32_t get_offset_of_currentAttribute_9() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentAttribute_9)); }
	inline int32_t get_currentAttribute_9() const { return ___currentAttribute_9; }
	inline int32_t* get_address_of_currentAttribute_9() { return &___currentAttribute_9; }
	inline void set_currentAttribute_9(int32_t value)
	{
		___currentAttribute_9 = value;
	}

	inline static int32_t get_offset_of_currentAttributeValue_10() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentAttributeValue_10)); }
	inline int32_t get_currentAttributeValue_10() const { return ___currentAttributeValue_10; }
	inline int32_t* get_address_of_currentAttributeValue_10() { return &___currentAttributeValue_10; }
	inline void set_currentAttributeValue_10(int32_t value)
	{
		___currentAttributeValue_10 = value;
	}

	inline static int32_t get_offset_of_attributeCount_11() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___attributeCount_11)); }
	inline int32_t get_attributeCount_11() const { return ___attributeCount_11; }
	inline int32_t* get_address_of_attributeCount_11() { return &___attributeCount_11; }
	inline void set_attributeCount_11(int32_t value)
	{
		___attributeCount_11 = value;
	}

	inline static int32_t get_offset_of_parserContext_12() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___parserContext_12)); }
	inline XmlParserContext_t2544895291 * get_parserContext_12() const { return ___parserContext_12; }
	inline XmlParserContext_t2544895291 ** get_address_of_parserContext_12() { return &___parserContext_12; }
	inline void set_parserContext_12(XmlParserContext_t2544895291 * value)
	{
		___parserContext_12 = value;
		Il2CppCodeGenWriteBarrier((&___parserContext_12), value);
	}

	inline static int32_t get_offset_of_nameTable_13() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___nameTable_13)); }
	inline XmlNameTable_t71772148 * get_nameTable_13() const { return ___nameTable_13; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_13() { return &___nameTable_13; }
	inline void set_nameTable_13(XmlNameTable_t71772148 * value)
	{
		___nameTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_13), value);
	}

	inline static int32_t get_offset_of_nsmgr_14() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___nsmgr_14)); }
	inline XmlNamespaceManager_t418790500 * get_nsmgr_14() const { return ___nsmgr_14; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_nsmgr_14() { return &___nsmgr_14; }
	inline void set_nsmgr_14(XmlNamespaceManager_t418790500 * value)
	{
		___nsmgr_14 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_14), value);
	}

	inline static int32_t get_offset_of_readState_15() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___readState_15)); }
	inline int32_t get_readState_15() const { return ___readState_15; }
	inline int32_t* get_address_of_readState_15() { return &___readState_15; }
	inline void set_readState_15(int32_t value)
	{
		___readState_15 = value;
	}

	inline static int32_t get_offset_of_disallowReset_16() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___disallowReset_16)); }
	inline bool get_disallowReset_16() const { return ___disallowReset_16; }
	inline bool* get_address_of_disallowReset_16() { return &___disallowReset_16; }
	inline void set_disallowReset_16(bool value)
	{
		___disallowReset_16 = value;
	}

	inline static int32_t get_offset_of_depth_17() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___depth_17)); }
	inline int32_t get_depth_17() const { return ___depth_17; }
	inline int32_t* get_address_of_depth_17() { return &___depth_17; }
	inline void set_depth_17(int32_t value)
	{
		___depth_17 = value;
	}

	inline static int32_t get_offset_of_elementDepth_18() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___elementDepth_18)); }
	inline int32_t get_elementDepth_18() const { return ___elementDepth_18; }
	inline int32_t* get_address_of_elementDepth_18() { return &___elementDepth_18; }
	inline void set_elementDepth_18(int32_t value)
	{
		___elementDepth_18 = value;
	}

	inline static int32_t get_offset_of_depthUp_19() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___depthUp_19)); }
	inline bool get_depthUp_19() const { return ___depthUp_19; }
	inline bool* get_address_of_depthUp_19() { return &___depthUp_19; }
	inline void set_depthUp_19(bool value)
	{
		___depthUp_19 = value;
	}

	inline static int32_t get_offset_of_popScope_20() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___popScope_20)); }
	inline bool get_popScope_20() const { return ___popScope_20; }
	inline bool* get_address_of_popScope_20() { return &___popScope_20; }
	inline void set_popScope_20(bool value)
	{
		___popScope_20 = value;
	}

	inline static int32_t get_offset_of_elementNames_21() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___elementNames_21)); }
	inline TagNameU5BU5D_t2055670886* get_elementNames_21() const { return ___elementNames_21; }
	inline TagNameU5BU5D_t2055670886** get_address_of_elementNames_21() { return &___elementNames_21; }
	inline void set_elementNames_21(TagNameU5BU5D_t2055670886* value)
	{
		___elementNames_21 = value;
		Il2CppCodeGenWriteBarrier((&___elementNames_21), value);
	}

	inline static int32_t get_offset_of_elementNameStackPos_22() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___elementNameStackPos_22)); }
	inline int32_t get_elementNameStackPos_22() const { return ___elementNameStackPos_22; }
	inline int32_t* get_address_of_elementNameStackPos_22() { return &___elementNameStackPos_22; }
	inline void set_elementNameStackPos_22(int32_t value)
	{
		___elementNameStackPos_22 = value;
	}

	inline static int32_t get_offset_of_allowMultipleRoot_23() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___allowMultipleRoot_23)); }
	inline bool get_allowMultipleRoot_23() const { return ___allowMultipleRoot_23; }
	inline bool* get_address_of_allowMultipleRoot_23() { return &___allowMultipleRoot_23; }
	inline void set_allowMultipleRoot_23(bool value)
	{
		___allowMultipleRoot_23 = value;
	}

	inline static int32_t get_offset_of_isStandalone_24() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___isStandalone_24)); }
	inline bool get_isStandalone_24() const { return ___isStandalone_24; }
	inline bool* get_address_of_isStandalone_24() { return &___isStandalone_24; }
	inline void set_isStandalone_24(bool value)
	{
		___isStandalone_24 = value;
	}

	inline static int32_t get_offset_of_returnEntityReference_25() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___returnEntityReference_25)); }
	inline bool get_returnEntityReference_25() const { return ___returnEntityReference_25; }
	inline bool* get_address_of_returnEntityReference_25() { return &___returnEntityReference_25; }
	inline void set_returnEntityReference_25(bool value)
	{
		___returnEntityReference_25 = value;
	}

	inline static int32_t get_offset_of_entityReferenceName_26() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___entityReferenceName_26)); }
	inline String_t* get_entityReferenceName_26() const { return ___entityReferenceName_26; }
	inline String_t** get_address_of_entityReferenceName_26() { return &___entityReferenceName_26; }
	inline void set_entityReferenceName_26(String_t* value)
	{
		___entityReferenceName_26 = value;
		Il2CppCodeGenWriteBarrier((&___entityReferenceName_26), value);
	}

	inline static int32_t get_offset_of_valueBuffer_27() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___valueBuffer_27)); }
	inline StringBuilder_t * get_valueBuffer_27() const { return ___valueBuffer_27; }
	inline StringBuilder_t ** get_address_of_valueBuffer_27() { return &___valueBuffer_27; }
	inline void set_valueBuffer_27(StringBuilder_t * value)
	{
		___valueBuffer_27 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_27), value);
	}

	inline static int32_t get_offset_of_reader_28() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___reader_28)); }
	inline TextReader_t283511965 * get_reader_28() const { return ___reader_28; }
	inline TextReader_t283511965 ** get_address_of_reader_28() { return &___reader_28; }
	inline void set_reader_28(TextReader_t283511965 * value)
	{
		___reader_28 = value;
		Il2CppCodeGenWriteBarrier((&___reader_28), value);
	}

	inline static int32_t get_offset_of_peekChars_29() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___peekChars_29)); }
	inline CharU5BU5D_t3528271667* get_peekChars_29() const { return ___peekChars_29; }
	inline CharU5BU5D_t3528271667** get_address_of_peekChars_29() { return &___peekChars_29; }
	inline void set_peekChars_29(CharU5BU5D_t3528271667* value)
	{
		___peekChars_29 = value;
		Il2CppCodeGenWriteBarrier((&___peekChars_29), value);
	}

	inline static int32_t get_offset_of_peekCharsIndex_30() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___peekCharsIndex_30)); }
	inline int32_t get_peekCharsIndex_30() const { return ___peekCharsIndex_30; }
	inline int32_t* get_address_of_peekCharsIndex_30() { return &___peekCharsIndex_30; }
	inline void set_peekCharsIndex_30(int32_t value)
	{
		___peekCharsIndex_30 = value;
	}

	inline static int32_t get_offset_of_peekCharsLength_31() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___peekCharsLength_31)); }
	inline int32_t get_peekCharsLength_31() const { return ___peekCharsLength_31; }
	inline int32_t* get_address_of_peekCharsLength_31() { return &___peekCharsLength_31; }
	inline void set_peekCharsLength_31(int32_t value)
	{
		___peekCharsLength_31 = value;
	}

	inline static int32_t get_offset_of_curNodePeekIndex_32() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___curNodePeekIndex_32)); }
	inline int32_t get_curNodePeekIndex_32() const { return ___curNodePeekIndex_32; }
	inline int32_t* get_address_of_curNodePeekIndex_32() { return &___curNodePeekIndex_32; }
	inline void set_curNodePeekIndex_32(int32_t value)
	{
		___curNodePeekIndex_32 = value;
	}

	inline static int32_t get_offset_of_preserveCurrentTag_33() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___preserveCurrentTag_33)); }
	inline bool get_preserveCurrentTag_33() const { return ___preserveCurrentTag_33; }
	inline bool* get_address_of_preserveCurrentTag_33() { return &___preserveCurrentTag_33; }
	inline void set_preserveCurrentTag_33(bool value)
	{
		___preserveCurrentTag_33 = value;
	}

	inline static int32_t get_offset_of_line_34() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___line_34)); }
	inline int32_t get_line_34() const { return ___line_34; }
	inline int32_t* get_address_of_line_34() { return &___line_34; }
	inline void set_line_34(int32_t value)
	{
		___line_34 = value;
	}

	inline static int32_t get_offset_of_column_35() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___column_35)); }
	inline int32_t get_column_35() const { return ___column_35; }
	inline int32_t* get_address_of_column_35() { return &___column_35; }
	inline void set_column_35(int32_t value)
	{
		___column_35 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_36() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentLinkedNodeLineNumber_36)); }
	inline int32_t get_currentLinkedNodeLineNumber_36() const { return ___currentLinkedNodeLineNumber_36; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_36() { return &___currentLinkedNodeLineNumber_36; }
	inline void set_currentLinkedNodeLineNumber_36(int32_t value)
	{
		___currentLinkedNodeLineNumber_36 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_37() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentLinkedNodeLinePosition_37)); }
	inline int32_t get_currentLinkedNodeLinePosition_37() const { return ___currentLinkedNodeLinePosition_37; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_37() { return &___currentLinkedNodeLinePosition_37; }
	inline void set_currentLinkedNodeLinePosition_37(int32_t value)
	{
		___currentLinkedNodeLinePosition_37 = value;
	}

	inline static int32_t get_offset_of_useProceedingLineInfo_38() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___useProceedingLineInfo_38)); }
	inline bool get_useProceedingLineInfo_38() const { return ___useProceedingLineInfo_38; }
	inline bool* get_address_of_useProceedingLineInfo_38() { return &___useProceedingLineInfo_38; }
	inline void set_useProceedingLineInfo_38(bool value)
	{
		___useProceedingLineInfo_38 = value;
	}

	inline static int32_t get_offset_of_startNodeType_39() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___startNodeType_39)); }
	inline int32_t get_startNodeType_39() const { return ___startNodeType_39; }
	inline int32_t* get_address_of_startNodeType_39() { return &___startNodeType_39; }
	inline void set_startNodeType_39(int32_t value)
	{
		___startNodeType_39 = value;
	}

	inline static int32_t get_offset_of_currentState_40() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___currentState_40)); }
	inline int32_t get_currentState_40() const { return ___currentState_40; }
	inline int32_t* get_address_of_currentState_40() { return &___currentState_40; }
	inline void set_currentState_40(int32_t value)
	{
		___currentState_40 = value;
	}

	inline static int32_t get_offset_of_nestLevel_41() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___nestLevel_41)); }
	inline int32_t get_nestLevel_41() const { return ___nestLevel_41; }
	inline int32_t* get_address_of_nestLevel_41() { return &___nestLevel_41; }
	inline void set_nestLevel_41(int32_t value)
	{
		___nestLevel_41 = value;
	}

	inline static int32_t get_offset_of_readCharsInProgress_42() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___readCharsInProgress_42)); }
	inline bool get_readCharsInProgress_42() const { return ___readCharsInProgress_42; }
	inline bool* get_address_of_readCharsInProgress_42() { return &___readCharsInProgress_42; }
	inline void set_readCharsInProgress_42(bool value)
	{
		___readCharsInProgress_42 = value;
	}

	inline static int32_t get_offset_of_binaryCharGetter_43() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___binaryCharGetter_43)); }
	inline CharGetter_t1703763694 * get_binaryCharGetter_43() const { return ___binaryCharGetter_43; }
	inline CharGetter_t1703763694 ** get_address_of_binaryCharGetter_43() { return &___binaryCharGetter_43; }
	inline void set_binaryCharGetter_43(CharGetter_t1703763694 * value)
	{
		___binaryCharGetter_43 = value;
		Il2CppCodeGenWriteBarrier((&___binaryCharGetter_43), value);
	}

	inline static int32_t get_offset_of_namespaces_44() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___namespaces_44)); }
	inline bool get_namespaces_44() const { return ___namespaces_44; }
	inline bool* get_address_of_namespaces_44() { return &___namespaces_44; }
	inline void set_namespaces_44(bool value)
	{
		___namespaces_44 = value;
	}

	inline static int32_t get_offset_of_whitespaceHandling_45() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___whitespaceHandling_45)); }
	inline int32_t get_whitespaceHandling_45() const { return ___whitespaceHandling_45; }
	inline int32_t* get_address_of_whitespaceHandling_45() { return &___whitespaceHandling_45; }
	inline void set_whitespaceHandling_45(int32_t value)
	{
		___whitespaceHandling_45 = value;
	}

	inline static int32_t get_offset_of_resolver_46() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___resolver_46)); }
	inline XmlResolver_t626023767 * get_resolver_46() const { return ___resolver_46; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_46() { return &___resolver_46; }
	inline void set_resolver_46(XmlResolver_t626023767 * value)
	{
		___resolver_46 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_46), value);
	}

	inline static int32_t get_offset_of_normalization_47() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___normalization_47)); }
	inline bool get_normalization_47() const { return ___normalization_47; }
	inline bool* get_address_of_normalization_47() { return &___normalization_47; }
	inline void set_normalization_47(bool value)
	{
		___normalization_47 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_48() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___checkCharacters_48)); }
	inline bool get_checkCharacters_48() const { return ___checkCharacters_48; }
	inline bool* get_address_of_checkCharacters_48() { return &___checkCharacters_48; }
	inline void set_checkCharacters_48(bool value)
	{
		___checkCharacters_48 = value;
	}

	inline static int32_t get_offset_of_prohibitDtd_49() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___prohibitDtd_49)); }
	inline bool get_prohibitDtd_49() const { return ___prohibitDtd_49; }
	inline bool* get_address_of_prohibitDtd_49() { return &___prohibitDtd_49; }
	inline void set_prohibitDtd_49(bool value)
	{
		___prohibitDtd_49 = value;
	}

	inline static int32_t get_offset_of_closeInput_50() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___closeInput_50)); }
	inline bool get_closeInput_50() const { return ___closeInput_50; }
	inline bool* get_address_of_closeInput_50() { return &___closeInput_50; }
	inline void set_closeInput_50(bool value)
	{
		___closeInput_50 = value;
	}

	inline static int32_t get_offset_of_entityHandling_51() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___entityHandling_51)); }
	inline int32_t get_entityHandling_51() const { return ___entityHandling_51; }
	inline int32_t* get_address_of_entityHandling_51() { return &___entityHandling_51; }
	inline void set_entityHandling_51(int32_t value)
	{
		___entityHandling_51 = value;
	}

	inline static int32_t get_offset_of_whitespacePool_52() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___whitespacePool_52)); }
	inline NameTable_t3178203267 * get_whitespacePool_52() const { return ___whitespacePool_52; }
	inline NameTable_t3178203267 ** get_address_of_whitespacePool_52() { return &___whitespacePool_52; }
	inline void set_whitespacePool_52(NameTable_t3178203267 * value)
	{
		___whitespacePool_52 = value;
		Il2CppCodeGenWriteBarrier((&___whitespacePool_52), value);
	}

	inline static int32_t get_offset_of_whitespaceCache_53() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___whitespaceCache_53)); }
	inline CharU5BU5D_t3528271667* get_whitespaceCache_53() const { return ___whitespaceCache_53; }
	inline CharU5BU5D_t3528271667** get_address_of_whitespaceCache_53() { return &___whitespaceCache_53; }
	inline void set_whitespaceCache_53(CharU5BU5D_t3528271667* value)
	{
		___whitespaceCache_53 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceCache_53), value);
	}

	inline static int32_t get_offset_of_stateStack_54() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481, ___stateStack_54)); }
	inline DtdInputStateStack_t339956957 * get_stateStack_54() const { return ___stateStack_54; }
	inline DtdInputStateStack_t339956957 ** get_address_of_stateStack_54() { return &___stateStack_54; }
	inline void set_stateStack_54(DtdInputStateStack_t339956957 * value)
	{
		___stateStack_54 = value;
		Il2CppCodeGenWriteBarrier((&___stateStack_54), value);
	}
};

struct XmlTextReader_t3455035481_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml2.XmlTextReader::<>f__switch$map51
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map51_55;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml2.XmlTextReader::<>f__switch$map52
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map52_56;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map51_55() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481_StaticFields, ___U3CU3Ef__switchU24map51_55)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map51_55() const { return ___U3CU3Ef__switchU24map51_55; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map51_55() { return &___U3CU3Ef__switchU24map51_55; }
	inline void set_U3CU3Ef__switchU24map51_55(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map51_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map51_55), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map52_56() { return static_cast<int32_t>(offsetof(XmlTextReader_t3455035481_StaticFields, ___U3CU3Ef__switchU24map52_56)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map52_56() const { return ___U3CU3Ef__switchU24map52_56; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map52_56() { return &___U3CU3Ef__switchU24map52_56; }
	inline void set_U3CU3Ef__switchU24map52_56(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map52_56 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map52_56), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T3455035481_H
#ifndef BOOLOPERATION_T828331492_H
#define BOOLOPERATION_T828331492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.BoolOperation
struct  BoolOperation_t828331492  : public BinaryOpExpression_t3223617729
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLOPERATION_T828331492_H
#ifndef VALIDATIONEVENTHANDLER_T791314227_H
#define VALIDATIONEVENTHANDLER_T791314227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationEventHandler
struct  ValidationEventHandler_t791314227  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTHANDLER_T791314227_H
#ifndef XMLNODECHANGEDEVENTHANDLER_T1533444722_H
#define XMLNODECHANGEDEVENTHANDLER_T1533444722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventHandler
struct  XmlNodeChangedEventHandler_t1533444722  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTHANDLER_T1533444722_H
#ifndef ARITHMETICOPERATION_T381249678_H
#define ARITHMETICOPERATION_T381249678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.ArithmeticOperation
struct  ArithmeticOperation_t381249678  : public BinaryOpExpression_t3223617729
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARITHMETICOPERATION_T381249678_H
#ifndef CHARGETTER_T1703763694_H
#define CHARGETTER_T1703763694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport/CharGetter
struct  CharGetter_t1703763694  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGETTER_T1703763694_H
#ifndef COMPARISON_T685263300_H
#define COMPARISON_T685263300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Comparison
struct  Comparison_t685263300  : public BinaryOpExpression_t3223617729
{
public:

public:
};

struct Comparison_t685263300_StaticFields
{
public:
	// System.Char[] Mono.Data.SqlExpressions.Comparison::IgnoredTrailingChars
	CharU5BU5D_t3528271667* ___IgnoredTrailingChars_3;

public:
	inline static int32_t get_offset_of_IgnoredTrailingChars_3() { return static_cast<int32_t>(offsetof(Comparison_t685263300_StaticFields, ___IgnoredTrailingChars_3)); }
	inline CharU5BU5D_t3528271667* get_IgnoredTrailingChars_3() const { return ___IgnoredTrailingChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_IgnoredTrailingChars_3() { return &___IgnoredTrailingChars_3; }
	inline void set_IgnoredTrailingChars_3(CharU5BU5D_t3528271667* value)
	{
		___IgnoredTrailingChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___IgnoredTrailingChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARISON_T685263300_H
#ifndef XMLATTRIBUTETOKENINFO_T384315108_H
#define XMLATTRIBUTETOKENINFO_T384315108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo
struct  XmlAttributeTokenInfo_t384315108  : public XmlTokenInfo_t2519673037
{
public:
	// System.Int32 Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::ValueTokenStartIndex
	int32_t ___ValueTokenStartIndex_13;
	// System.Int32 Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::ValueTokenEndIndex
	int32_t ___ValueTokenEndIndex_14;
	// System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::valueCache
	String_t* ___valueCache_15;
	// System.Text.StringBuilder Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::tmpBuilder
	StringBuilder_t * ___tmpBuilder_16;

public:
	inline static int32_t get_offset_of_ValueTokenStartIndex_13() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t384315108, ___ValueTokenStartIndex_13)); }
	inline int32_t get_ValueTokenStartIndex_13() const { return ___ValueTokenStartIndex_13; }
	inline int32_t* get_address_of_ValueTokenStartIndex_13() { return &___ValueTokenStartIndex_13; }
	inline void set_ValueTokenStartIndex_13(int32_t value)
	{
		___ValueTokenStartIndex_13 = value;
	}

	inline static int32_t get_offset_of_ValueTokenEndIndex_14() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t384315108, ___ValueTokenEndIndex_14)); }
	inline int32_t get_ValueTokenEndIndex_14() const { return ___ValueTokenEndIndex_14; }
	inline int32_t* get_address_of_ValueTokenEndIndex_14() { return &___ValueTokenEndIndex_14; }
	inline void set_ValueTokenEndIndex_14(int32_t value)
	{
		___ValueTokenEndIndex_14 = value;
	}

	inline static int32_t get_offset_of_valueCache_15() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t384315108, ___valueCache_15)); }
	inline String_t* get_valueCache_15() const { return ___valueCache_15; }
	inline String_t** get_address_of_valueCache_15() { return &___valueCache_15; }
	inline void set_valueCache_15(String_t* value)
	{
		___valueCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___valueCache_15), value);
	}

	inline static int32_t get_offset_of_tmpBuilder_16() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_t384315108, ___tmpBuilder_16)); }
	inline StringBuilder_t * get_tmpBuilder_16() const { return ___tmpBuilder_16; }
	inline StringBuilder_t ** get_address_of_tmpBuilder_16() { return &___tmpBuilder_16; }
	inline void set_tmpBuilder_16(StringBuilder_t * value)
	{
		___tmpBuilder_16 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuilder_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTETOKENINFO_T384315108_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (CommandState_t1020432923)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2200[6] = 
{
	CommandState_t1020432923::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (CharGetter_t1703763694), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (XmlReaderSettings_t2186285234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[2] = 
{
	XmlReaderSettings_t2186285234::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t2186285234::get_offset_of_conformance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (XmlResolver_t626023767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (XmlSignificantWhitespace_t1052520128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (XmlSpace_t3324193251)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2205[4] = 
{
	XmlSpace_t3324193251::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (XmlText_t2682211705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (XmlTextReader_t3455035481), -1, sizeof(XmlTextReader_t3455035481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2207[54] = 
{
	XmlTextReader_t3455035481::get_offset_of_cursorToken_3(),
	XmlTextReader_t3455035481::get_offset_of_currentToken_4(),
	XmlTextReader_t3455035481::get_offset_of_currentAttributeToken_5(),
	XmlTextReader_t3455035481::get_offset_of_currentAttributeValueToken_6(),
	XmlTextReader_t3455035481::get_offset_of_attributeTokens_7(),
	XmlTextReader_t3455035481::get_offset_of_attributeValueTokens_8(),
	XmlTextReader_t3455035481::get_offset_of_currentAttribute_9(),
	XmlTextReader_t3455035481::get_offset_of_currentAttributeValue_10(),
	XmlTextReader_t3455035481::get_offset_of_attributeCount_11(),
	XmlTextReader_t3455035481::get_offset_of_parserContext_12(),
	XmlTextReader_t3455035481::get_offset_of_nameTable_13(),
	XmlTextReader_t3455035481::get_offset_of_nsmgr_14(),
	XmlTextReader_t3455035481::get_offset_of_readState_15(),
	XmlTextReader_t3455035481::get_offset_of_disallowReset_16(),
	XmlTextReader_t3455035481::get_offset_of_depth_17(),
	XmlTextReader_t3455035481::get_offset_of_elementDepth_18(),
	XmlTextReader_t3455035481::get_offset_of_depthUp_19(),
	XmlTextReader_t3455035481::get_offset_of_popScope_20(),
	XmlTextReader_t3455035481::get_offset_of_elementNames_21(),
	XmlTextReader_t3455035481::get_offset_of_elementNameStackPos_22(),
	XmlTextReader_t3455035481::get_offset_of_allowMultipleRoot_23(),
	XmlTextReader_t3455035481::get_offset_of_isStandalone_24(),
	XmlTextReader_t3455035481::get_offset_of_returnEntityReference_25(),
	XmlTextReader_t3455035481::get_offset_of_entityReferenceName_26(),
	XmlTextReader_t3455035481::get_offset_of_valueBuffer_27(),
	XmlTextReader_t3455035481::get_offset_of_reader_28(),
	XmlTextReader_t3455035481::get_offset_of_peekChars_29(),
	XmlTextReader_t3455035481::get_offset_of_peekCharsIndex_30(),
	XmlTextReader_t3455035481::get_offset_of_peekCharsLength_31(),
	XmlTextReader_t3455035481::get_offset_of_curNodePeekIndex_32(),
	XmlTextReader_t3455035481::get_offset_of_preserveCurrentTag_33(),
	XmlTextReader_t3455035481::get_offset_of_line_34(),
	XmlTextReader_t3455035481::get_offset_of_column_35(),
	XmlTextReader_t3455035481::get_offset_of_currentLinkedNodeLineNumber_36(),
	XmlTextReader_t3455035481::get_offset_of_currentLinkedNodeLinePosition_37(),
	XmlTextReader_t3455035481::get_offset_of_useProceedingLineInfo_38(),
	XmlTextReader_t3455035481::get_offset_of_startNodeType_39(),
	XmlTextReader_t3455035481::get_offset_of_currentState_40(),
	XmlTextReader_t3455035481::get_offset_of_nestLevel_41(),
	XmlTextReader_t3455035481::get_offset_of_readCharsInProgress_42(),
	XmlTextReader_t3455035481::get_offset_of_binaryCharGetter_43(),
	XmlTextReader_t3455035481::get_offset_of_namespaces_44(),
	XmlTextReader_t3455035481::get_offset_of_whitespaceHandling_45(),
	XmlTextReader_t3455035481::get_offset_of_resolver_46(),
	XmlTextReader_t3455035481::get_offset_of_normalization_47(),
	XmlTextReader_t3455035481::get_offset_of_checkCharacters_48(),
	XmlTextReader_t3455035481::get_offset_of_prohibitDtd_49(),
	XmlTextReader_t3455035481::get_offset_of_closeInput_50(),
	XmlTextReader_t3455035481::get_offset_of_entityHandling_51(),
	XmlTextReader_t3455035481::get_offset_of_whitespacePool_52(),
	XmlTextReader_t3455035481::get_offset_of_whitespaceCache_53(),
	XmlTextReader_t3455035481::get_offset_of_stateStack_54(),
	XmlTextReader_t3455035481_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_55(),
	XmlTextReader_t3455035481_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (XmlTokenInfo_t2519673037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[13] = 
{
	XmlTokenInfo_t2519673037::get_offset_of_valueCache_0(),
	XmlTokenInfo_t2519673037::get_offset_of_Reader_1(),
	XmlTokenInfo_t2519673037::get_offset_of_Name_2(),
	XmlTokenInfo_t2519673037::get_offset_of_LocalName_3(),
	XmlTokenInfo_t2519673037::get_offset_of_Prefix_4(),
	XmlTokenInfo_t2519673037::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t2519673037::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t2519673037::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t2519673037::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t2519673037::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t2519673037::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t2519673037::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t2519673037::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (XmlAttributeTokenInfo_t384315108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[4] = 
{
	XmlAttributeTokenInfo_t384315108::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t384315108::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t384315108::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t384315108::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (TagName_t2891256255)+ sizeof (RuntimeObject), sizeof(TagName_t2891256255_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	TagName_t2891256255::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagName_t2891256255::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagName_t2891256255::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (DtdInputState_t1766821130)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2211[10] = 
{
	DtdInputState_t1766821130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (DtdInputStateStack_t339956957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	DtdInputStateStack_t339956957::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (XmlTextReader_t4233384356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[5] = 
{
	XmlTextReader_t4233384356::get_offset_of_entity_3(),
	XmlTextReader_t4233384356::get_offset_of_source_4(),
	XmlTextReader_t4233384356::get_offset_of_entityInsideAttribute_5(),
	XmlTextReader_t4233384356::get_offset_of_insideAttribute_6(),
	XmlTextReader_t4233384356::get_offset_of_entityNameStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (XmlTextWriter_t2114213153), -1, sizeof(XmlTextWriter_t2114213153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[35] = 
{
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_unmarked_utf8encoding_1(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_escaped_text_chars_2(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_escaped_attr_chars_3(),
	XmlTextWriter_t2114213153::get_offset_of_base_stream_4(),
	XmlTextWriter_t2114213153::get_offset_of_source_5(),
	XmlTextWriter_t2114213153::get_offset_of_writer_6(),
	XmlTextWriter_t2114213153::get_offset_of_preserver_7(),
	XmlTextWriter_t2114213153::get_offset_of_preserved_name_8(),
	XmlTextWriter_t2114213153::get_offset_of_is_preserved_xmlns_9(),
	XmlTextWriter_t2114213153::get_offset_of_allow_doc_fragment_10(),
	XmlTextWriter_t2114213153::get_offset_of_close_output_stream_11(),
	XmlTextWriter_t2114213153::get_offset_of_ignore_encoding_12(),
	XmlTextWriter_t2114213153::get_offset_of_namespaces_13(),
	XmlTextWriter_t2114213153::get_offset_of_xmldecl_state_14(),
	XmlTextWriter_t2114213153::get_offset_of_check_character_validity_15(),
	XmlTextWriter_t2114213153::get_offset_of_newline_handling_16(),
	XmlTextWriter_t2114213153::get_offset_of_is_document_entity_17(),
	XmlTextWriter_t2114213153::get_offset_of_state_18(),
	XmlTextWriter_t2114213153::get_offset_of_node_state_19(),
	XmlTextWriter_t2114213153::get_offset_of_nsmanager_20(),
	XmlTextWriter_t2114213153::get_offset_of_open_count_21(),
	XmlTextWriter_t2114213153::get_offset_of_elements_22(),
	XmlTextWriter_t2114213153::get_offset_of_new_local_namespaces_23(),
	XmlTextWriter_t2114213153::get_offset_of_explicit_nsdecls_24(),
	XmlTextWriter_t2114213153::get_offset_of_namespace_handling_25(),
	XmlTextWriter_t2114213153::get_offset_of_indent_26(),
	XmlTextWriter_t2114213153::get_offset_of_indent_count_27(),
	XmlTextWriter_t2114213153::get_offset_of_indent_char_28(),
	XmlTextWriter_t2114213153::get_offset_of_indent_string_29(),
	XmlTextWriter_t2114213153::get_offset_of_newline_30(),
	XmlTextWriter_t2114213153::get_offset_of_indent_attributes_31(),
	XmlTextWriter_t2114213153::get_offset_of_quote_char_32(),
	XmlTextWriter_t2114213153::get_offset_of_v2_33(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_34(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (XmlNodeInfo_t4030693883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[7] = 
{
	XmlNodeInfo_t4030693883::get_offset_of_Prefix_0(),
	XmlNodeInfo_t4030693883::get_offset_of_LocalName_1(),
	XmlNodeInfo_t4030693883::get_offset_of_NS_2(),
	XmlNodeInfo_t4030693883::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t4030693883::get_offset_of_HasElements_4(),
	XmlNodeInfo_t4030693883::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t4030693883::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (StringUtil_t564231417), -1, sizeof(StringUtil_t564231417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2216[2] = 
{
	StringUtil_t564231417_StaticFields::get_offset_of_cul_0(),
	StringUtil_t564231417_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (XmlDeclState_t1683299469)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2217[5] = 
{
	XmlDeclState_t1683299469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (XmlTokenizedType_t3348692225)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2218[14] = 
{
	XmlTokenizedType_t3348692225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (XmlUrlResolver_t817895037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[1] = 
{
	XmlUrlResolver_t817895037::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (XmlWhitespace_t131741354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (XmlWriter_t127905479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[1] = 
{
	XmlWriter_t127905479::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (XmlWriterSettings_t3314986516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[12] = 
{
	XmlWriterSettings_t3314986516::get_offset_of_checkCharacters_0(),
	XmlWriterSettings_t3314986516::get_offset_of_closeOutput_1(),
	XmlWriterSettings_t3314986516::get_offset_of_conformance_2(),
	XmlWriterSettings_t3314986516::get_offset_of_encoding_3(),
	XmlWriterSettings_t3314986516::get_offset_of_indent_4(),
	XmlWriterSettings_t3314986516::get_offset_of_indentChars_5(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineChars_6(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineHandling_8(),
	XmlWriterSettings_t3314986516::get_offset_of_omitXmlDeclaration_9(),
	XmlWriterSettings_t3314986516::get_offset_of_outputMethod_10(),
	XmlWriterSettings_t3314986516::get_offset_of_U3CNamespaceHandlingU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (ValidationEventHandler_t791314227), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (XmlNodeChangedEventHandler_t1533444722), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[8] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D23_0(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D24_1(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D26_2(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D27_3(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D28_4(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D29_5(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D43_6(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D44_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U24ArrayTypeU2412_t2490092599)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2412_t2490092599 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (U24ArrayTypeU248_t3244137464)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU248_t3244137464 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U24ArrayTypeU24256_t1929481985)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24256_t1929481985 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (U24ArrayTypeU241280_t4290130235)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU241280_t4290130235 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (Parser_t2410290639), -1, sizeof(Parser_t2410290639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[19] = 
{
	Parser_t2410290639::get_offset_of_cacheAggregationResults_0(),
	Parser_t2410290639::get_offset_of_aggregationRows_1(),
	Parser_t2410290639_StaticFields::get_offset_of_yacc_verbose_flag_2(),
	Parser_t2410290639::get_offset_of_ErrorOutput_3(),
	Parser_t2410290639::get_offset_of_eof_token_4(),
	Parser_t2410290639::get_offset_of_debug_5(),
	Parser_t2410290639_StaticFields::get_offset_of_yyFinal_6(),
	Parser_t2410290639_StaticFields::get_offset_of_yyNames_7(),
	Parser_t2410290639::get_offset_of_yyExpectingState_8(),
	Parser_t2410290639::get_offset_of_yyMax_9(),
	Parser_t2410290639_StaticFields::get_offset_of_yyLhs_10(),
	Parser_t2410290639_StaticFields::get_offset_of_yyLen_11(),
	Parser_t2410290639_StaticFields::get_offset_of_yyDefRed_12(),
	Parser_t2410290639_StaticFields::get_offset_of_yyDgoto_13(),
	Parser_t2410290639_StaticFields::get_offset_of_yySindex_14(),
	Parser_t2410290639_StaticFields::get_offset_of_yyRindex_15(),
	Parser_t2410290639_StaticFields::get_offset_of_yyGindex_16(),
	Parser_t2410290639_StaticFields::get_offset_of_yyTable_17(),
	Parser_t2410290639_StaticFields::get_offset_of_yyCheck_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (YYRules_t1272530553), -1, sizeof(YYRules_t1272530553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2233[1] = 
{
	YYRules_t1272530553_StaticFields::get_offset_of_yyRule_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (yyDebugSimple_t3881228540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (yyException_t2856183173), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (yyUnexpectedEof_t141146907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (Locale_t4128636111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (MonoTODOAttribute_t4131080585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[1] = 
{
	MonoTODOAttribute_t4131080585::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (MonoNotSupportedAttribute_t2563528020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (AggregationFunction_t2631941729)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2242[8] = 
{
	AggregationFunction_t2631941729::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (Aggregation_t2105721107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[8] = 
{
	Aggregation_t2105721107::get_offset_of_cacheResults_0(),
	Aggregation_t2105721107::get_offset_of_rows_1(),
	Aggregation_t2105721107::get_offset_of_column_2(),
	Aggregation_t2105721107::get_offset_of_function_3(),
	Aggregation_t2105721107::get_offset_of_count_4(),
	Aggregation_t2105721107::get_offset_of_result_5(),
	Aggregation_t2105721107::get_offset_of_RowChangeHandler_6(),
	Aggregation_t2105721107::get_offset_of_table_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (Negative_t3692748921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (ArithmeticOperation_t381249678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (Negation_t1647173762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (BoolOperation_t828331492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (ReferencedTable_t3986959547)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2248[4] = 
{
	ReferencedTable_t3986959547::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (ColumnReference_t94354283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[5] = 
{
	ColumnReference_t94354283::get_offset_of_refTable_0(),
	ColumnReference_t94354283::get_offset_of_relationName_1(),
	ColumnReference_t94354283::get_offset_of_columnName_2(),
	ColumnReference_t94354283::get_offset_of__cachedColumn_3(),
	ColumnReference_t94354283::get_offset_of__cachedRelation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (Comparison_t685263300), -1, sizeof(Comparison_t685263300_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2250[1] = 
{
	Comparison_t685263300_StaticFields::get_offset_of_IgnoredTrailingChars_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (BaseExpression_t4150592444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (UnaryExpression_t1939092167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[1] = 
{
	UnaryExpression_t1939092167::get_offset_of_expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (BinaryExpression_t4181220238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[2] = 
{
	BinaryExpression_t4181220238::get_offset_of_expr1_0(),
	BinaryExpression_t4181220238::get_offset_of_expr2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (Operation_t1922278484)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[14] = 
{
	Operation_t1922278484::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (BinaryOpExpression_t3223617729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[1] = 
{
	BinaryOpExpression_t3223617729::get_offset_of_op_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (IifFunction_t332110100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[2] = 
{
	IifFunction_t332110100::get_offset_of_trueExpr_1(),
	IifFunction_t332110100::get_offset_of_falseExpr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (IsNullFunction_t3547063577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[1] = 
{
	IsNullFunction_t3547063577::get_offset_of_defaultExpr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (ConvertFunction_t4260638137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	ConvertFunction_t4260638137::get_offset_of_targetType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (In_t2577005558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[1] = 
{
	In_t2577005558::get_offset_of_set_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (Like_t3391805775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[1] = 
{
	Like_t3391805775::get_offset_of__pattern_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (Literal_t2610857326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[1] = 
{
	Literal_t2610857326::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (Numeric_t3836385610), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (StringFunction_t522225446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (ConcatFunction_t3472055775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	ConcatFunction_t3472055775::get_offset_of__add_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (SubstringFunction_t1161052992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[2] = 
{
	SubstringFunction_t1161052992::get_offset_of_start_1(),
	SubstringFunction_t1161052992::get_offset_of_len_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (LenFunction_t3499950232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (TrimFunction_t1155110299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (Tokenizer_t4122578263), -1, sizeof(Tokenizer_t4122578263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2269[6] = 
{
	Tokenizer_t4122578263_StaticFields::get_offset_of_tokenMap_0(),
	Tokenizer_t4122578263_StaticFields::get_offset_of_tokens_1(),
	Tokenizer_t4122578263::get_offset_of_input_2(),
	Tokenizer_t4122578263::get_offset_of_pos_3(),
	Tokenizer_t4122578263::get_offset_of_tok_4(),
	Tokenizer_t4122578263::get_offset_of_val_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (DataAdapter_t2689867586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[1] = 
{
	DataAdapter_t2689867586::get_offset_of_tableMappings_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (DataColumnMapping_t1915060192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[2] = 
{
	DataColumnMapping_t1915060192::get_offset_of_sourceColumn_1(),
	DataColumnMapping_t1915060192::get_offset_of_dataSetColumn_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (DataColumnMappingCollection_t2426444188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[3] = 
{
	DataColumnMappingCollection_t2426444188::get_offset_of_list_1(),
	DataColumnMappingCollection_t2426444188::get_offset_of_sourceColumns_2(),
	DataColumnMappingCollection_t2426444188::get_offset_of_dataSetColumns_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (DataContainer_t2712869782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[3] = 
{
	DataContainer_t2712869782::get_offset_of_null_values_0(),
	DataContainer_t2712869782::get_offset_of__type_1(),
	DataContainer_t2712869782::get_offset_of__column_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (BitDataContainer_t2674678255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[1] = 
{
	BitDataContainer_t2674678255::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (CharDataContainer_t4070775190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[1] = 
{
	CharDataContainer_t4070775190::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (ByteDataContainer_t2113316179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[1] = 
{
	ByteDataContainer_t2113316179::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (SByteDataContainer_t4173937374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[1] = 
{
	SByteDataContainer_t4173937374::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (Int16DataContainer_t272545927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[1] = 
{
	Int16DataContainer_t272545927::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (UInt16DataContainer_t2155991586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[1] = 
{
	UInt16DataContainer_t2155991586::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (Int32DataContainer_t3302098759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[1] = 
{
	Int32DataContainer_t3302098759::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (UInt32DataContainer_t1292766739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[1] = 
{
	UInt32DataContainer_t1292766739::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (Int64DataContainer_t1137787704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[1] = 
{
	Int64DataContainer_t1137787704::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (UInt64DataContainer_t3774578615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[1] = 
{
	UInt64DataContainer_t3774578615::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (SingleDataContainer_t2640744932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[1] = 
{
	SingleDataContainer_t2640744932::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (DoubleDataContainer_t430033574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[1] = 
{
	DoubleDataContainer_t430033574::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (ObjectDataContainer_t3603397982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[1] = 
{
	ObjectDataContainer_t3603397982::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (DateTimeDataContainer_t145609085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (DecimalDataContainer_t1211020912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (StringDataContainer_t3348382435), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (DataTableMapping_t2453262560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[3] = 
{
	DataTableMapping_t2453262560::get_offset_of_sourceTable_1(),
	DataTableMapping_t2453262560::get_offset_of_dataSetTable_2(),
	DataTableMapping_t2453262560::get_offset_of_columnMappings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (DataTableMappingCollection_t2364717775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[3] = 
{
	DataTableMappingCollection_t2364717775::get_offset_of_mappings_1(),
	DataTableMappingCollection_t2364717775::get_offset_of_sourceTables_2(),
	DataTableMappingCollection_t2364717775::get_offset_of_dataSetTables_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (DbCommand_t187824494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (DbConnection_t1716984062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (DbDataAdapter_t1706016184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[4] = 
{
	DbDataAdapter_t1706016184::get_offset_of__selectCommand_5(),
	DbDataAdapter_t1706016184::get_offset_of__updateCommand_6(),
	DbDataAdapter_t1706016184::get_offset_of__deleteCommand_7(),
	DbDataAdapter_t1706016184::get_offset_of__insertCommand_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (DbParameter_t3789779732), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (DbParameterCollection_t2951779354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (DbProviderFactory_t3108676059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (DbTypes_t3951852654), -1, sizeof(DbTypes_t3951852654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2298[21] = 
{
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfBoolean_0(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfSByte_1(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfChar_2(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfInt16_3(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfInt32_4(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfInt64_5(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfByte_6(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfUInt16_7(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfUInt32_8(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfUInt64_9(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfDouble_10(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfSingle_11(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfDecimal_12(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfString_13(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfDateTime_14(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfObject_15(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfGuid_16(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfType_17(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfByteArray_18(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfFloat_19(),
	DbTypes_t3951852654_StaticFields::get_offset_of_TypeOfTimespan_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (Index_t4155063583), -1, sizeof(Index_t4155063583_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2299[7] = 
{
	Index_t4155063583_StaticFields::get_offset_of_empty_0(),
	Index_t4155063583::get_offset_of__array_1(),
	Index_t4155063583::get_offset_of__size_2(),
	Index_t4155063583::get_offset_of__key_3(),
	Index_t4155063583::get_offset_of__refCount_4(),
	Index_t4155063583::get_offset_of_know_have_duplicates_5(),
	Index_t4155063583::get_offset_of_know_no_duplicates_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
