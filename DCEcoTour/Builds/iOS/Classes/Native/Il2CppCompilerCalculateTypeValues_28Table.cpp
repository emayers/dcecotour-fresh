﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t3524055750;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset>
struct Dictionary_2_t3548062253;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset>
struct Dictionary_2_t3668501260;
// Mapbox.Json.Bson.BsonString
struct BsonString_t848819904;
// Mapbox.Json.Bson.BsonToken
struct BsonToken_t3974775022;
// Mapbox.Json.Serialization.ErrorContext
struct ErrorContext_t3853262553;
// Mapbox.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_t2891093856;
// Mapbox.Json.JsonSerializer
struct JsonSerializer_t2280177768;
// Mapbox.Json.Serialization.ITraceWriter
struct ITraceWriter_t420207159;
// Mapbox.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t2669598769;
// System.Collections.Generic.LinkedList`1<System.Action>
struct LinkedList_1_t104023486;
// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>>
struct Dictionary_2_t4196256347;
// Mapbox.Json.Linq.JToken
struct JToken_t2379863307;
// Mapbox.Json.Linq.JProperty/JPropertyList
struct JPropertyList_t362393847;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Mapbox.Json.Linq.JContainer
struct JContainer_t3802169109;
// Mapbox.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t3715042731;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Collections.Generic.List`1<Mapbox.Json.Converters.IXmlNode>
struct List_1_t548158954;
// System.Func`2<System.Object,System.Type>
struct Func_2_t1850968970;
// System.Type
struct Type_t;
// System.Func`1<System.Object>
struct Func_1_t2509852811;
// System.Xml.Linq.XObject
struct XObject_t1119084474;
// System.Func`2<Mapbox.Json.Serialization.JsonProperty,System.String>
struct Func_2_t540211779;
// System.Func`2<Mapbox.Json.Serialization.JsonProperty,Mapbox.Json.Serialization.JsonProperty>
struct Func_2_t322737350;
// System.Func`2<Mapbox.Json.Serialization.JsonProperty,Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Func_2_t3040735037;
// Mapbox.Json.Serialization.JsonProperty
struct JsonProperty_t1629976260;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.Generic.IList`1<Mapbox.Json.Linq.JToken>
struct IList_1_t4195183090;
// System.Collections.Generic.List`1<Mapbox.Json.Bson.BsonToken>
struct List_1_t1151882468;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Mapbox.Json.Bson.BsonProperty>
struct List_1_t1688264791;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// UnityEngine.Material
struct Material_t340375123;
// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,Mapbox.Json.Utilities.ReflectionObject>
struct ThreadSafeStore_2_t907930224;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlElement
struct XmlElement_t561603118;
// System.Xml.XmlDeclaration
struct XmlDeclaration_t679870411;
// Mapbox.Json.Utilities.ReflectionObject
struct ReflectionObject_t2904696228;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.Dictionary`2<System.String,Mapbox.Json.Linq.JToken>
struct Dictionary_2_t2165119606;
// System.Xml.XmlDocumentType
struct XmlDocumentType_t4112370061;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Xml.Linq.XDocumentType
struct XDocumentType_t1853592271;
// System.Xml.Linq.XDeclaration
struct XDeclaration_t2907650823;
// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],System.Object>>
struct ThreadSafeStore_2_t2733207365;
// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>
struct ThreadSafeStore_2_t487178756;
// System.Collections.Generic.List`1<Mapbox.Json.Linq.JToken>
struct List_1_t3851938049;
// Mapbox.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_t3049044894;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3836340606;
// Mapbox.Json.Linq.JObject
struct JObject_t877230766;
// System.Collections.Generic.IEnumerator`1<Mapbox.Json.Linq.JToken>
struct IEnumerator_1_t2812433775;
// Mapbox.Json.Linq.JProperty
struct JProperty_t1073345108;
// System.Collections.Generic.List`1<Mapbox.Json.Serialization.SerializationCallback>
struct List_1_t3209604541;
// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback>
struct IList_1_t3552849582;
// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationErrorCallback>
struct IList_1_t2627238525;
// Mapbox.Json.JsonConverter
struct JsonConverter_t472504469;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.BinaryWriter
struct BinaryWriter_t3992595042;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Collections.Generic.List`1<Mapbox.Json.JsonPosition>
struct List_1_t2313153536;
// Mapbox.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t691804306;
// Mapbox.Json.JsonConverterCollection
struct JsonConverterCollection_t2916355996;
// Mapbox.Json.Serialization.IContractResolver
struct IContractResolver_t2761620673;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// Mapbox.Json.Serialization.ISerializationBinder
struct ISerializationBinder_t2131676634;
// Mapbox.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t780537910;
// System.EventHandler`1<Mapbox.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t44176832;
// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.ReadType>
struct Dictionary_2_t2631974886;
// Mapbox.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t3284513685;
// Mapbox.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t2365347854;
// Mapbox.Json.Linq.JValue
struct JValue_t981543937;
// Mapbox.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t3219674374;
// TMPro.InlineGraphic
struct InlineGraphic_t2901727699;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Texture
struct Texture_t3661962703;
// TMPro.InlineGraphicManager
struct InlineGraphicManager_t2871008645;




#ifndef U3CMODULEU3E_T692745552_H
#define U3CMODULEU3E_T692745552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745552 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745552_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LINEINFOANNOTATION_T3056736104_H
#define LINEINFOANNOTATION_T3056736104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JToken/LineInfoAnnotation
struct  LineInfoAnnotation_t3056736104  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Json.Linq.JToken/LineInfoAnnotation::LineNumber
	int32_t ___LineNumber_0;
	// System.Int32 Mapbox.Json.Linq.JToken/LineInfoAnnotation::LinePosition
	int32_t ___LinePosition_1;

public:
	inline static int32_t get_offset_of_LineNumber_0() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t3056736104, ___LineNumber_0)); }
	inline int32_t get_LineNumber_0() const { return ___LineNumber_0; }
	inline int32_t* get_address_of_LineNumber_0() { return &___LineNumber_0; }
	inline void set_LineNumber_0(int32_t value)
	{
		___LineNumber_0 = value;
	}

	inline static int32_t get_offset_of_LinePosition_1() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t3056736104, ___LinePosition_1)); }
	inline int32_t get_LinePosition_1() const { return ___LinePosition_1; }
	inline int32_t* get_address_of_LinePosition_1() { return &___LinePosition_1; }
	inline void set_LinePosition_1(int32_t value)
	{
		___LinePosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOANNOTATION_T3056736104_H
#ifndef MATERIALREFERENCEMANAGER_T2114976864_H
#define MATERIALREFERENCEMANAGER_T2114976864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReferenceManager
struct  MaterialReferenceManager_t2114976864  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> TMPro.MaterialReferenceManager::m_FontMaterialReferenceLookup
	Dictionary_2_t3524055750 * ___m_FontMaterialReferenceLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset> TMPro.MaterialReferenceManager::m_FontAssetReferenceLookup
	Dictionary_2_t3548062253 * ___m_FontAssetReferenceLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset> TMPro.MaterialReferenceManager::m_SpriteAssetReferenceLookup
	Dictionary_2_t3668501260 * ___m_SpriteAssetReferenceLookup_3;

public:
	inline static int32_t get_offset_of_m_FontMaterialReferenceLookup_1() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864, ___m_FontMaterialReferenceLookup_1)); }
	inline Dictionary_2_t3524055750 * get_m_FontMaterialReferenceLookup_1() const { return ___m_FontMaterialReferenceLookup_1; }
	inline Dictionary_2_t3524055750 ** get_address_of_m_FontMaterialReferenceLookup_1() { return &___m_FontMaterialReferenceLookup_1; }
	inline void set_m_FontMaterialReferenceLookup_1(Dictionary_2_t3524055750 * value)
	{
		___m_FontMaterialReferenceLookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontMaterialReferenceLookup_1), value);
	}

	inline static int32_t get_offset_of_m_FontAssetReferenceLookup_2() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864, ___m_FontAssetReferenceLookup_2)); }
	inline Dictionary_2_t3548062253 * get_m_FontAssetReferenceLookup_2() const { return ___m_FontAssetReferenceLookup_2; }
	inline Dictionary_2_t3548062253 ** get_address_of_m_FontAssetReferenceLookup_2() { return &___m_FontAssetReferenceLookup_2; }
	inline void set_m_FontAssetReferenceLookup_2(Dictionary_2_t3548062253 * value)
	{
		___m_FontAssetReferenceLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontAssetReferenceLookup_2), value);
	}

	inline static int32_t get_offset_of_m_SpriteAssetReferenceLookup_3() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864, ___m_SpriteAssetReferenceLookup_3)); }
	inline Dictionary_2_t3668501260 * get_m_SpriteAssetReferenceLookup_3() const { return ___m_SpriteAssetReferenceLookup_3; }
	inline Dictionary_2_t3668501260 ** get_address_of_m_SpriteAssetReferenceLookup_3() { return &___m_SpriteAssetReferenceLookup_3; }
	inline void set_m_SpriteAssetReferenceLookup_3(Dictionary_2_t3668501260 * value)
	{
		___m_SpriteAssetReferenceLookup_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteAssetReferenceLookup_3), value);
	}
};

struct MaterialReferenceManager_t2114976864_StaticFields
{
public:
	// TMPro.MaterialReferenceManager TMPro.MaterialReferenceManager::s_Instance
	MaterialReferenceManager_t2114976864 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t2114976864_StaticFields, ___s_Instance_0)); }
	inline MaterialReferenceManager_t2114976864 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline MaterialReferenceManager_t2114976864 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(MaterialReferenceManager_t2114976864 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALREFERENCEMANAGER_T2114976864_H
#ifndef BSONPROPERTY_T216190049_H
#define BSONPROPERTY_T216190049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonProperty
struct  BsonProperty_t216190049  : public RuntimeObject
{
public:
	// Mapbox.Json.Bson.BsonString Mapbox.Json.Bson.BsonProperty::<Name>k__BackingField
	BsonString_t848819904 * ___U3CNameU3Ek__BackingField_0;
	// Mapbox.Json.Bson.BsonToken Mapbox.Json.Bson.BsonProperty::<Value>k__BackingField
	BsonToken_t3974775022 * ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonProperty_t216190049, ___U3CNameU3Ek__BackingField_0)); }
	inline BsonString_t848819904 * get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline BsonString_t848819904 ** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(BsonString_t848819904 * value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonProperty_t216190049, ___U3CValueU3Ek__BackingField_1)); }
	inline BsonToken_t3974775022 * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline BsonToken_t3974775022 ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(BsonToken_t3974775022 * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONPROPERTY_T216190049_H
#ifndef JSONSERIALIZERINTERNALBASE_T2072393995_H
#define JSONSERIALIZERINTERNALBASE_T2072393995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_t2072393995  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.ErrorContext Mapbox.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_t3853262553 * ____currentErrorContext_0;
	// Mapbox.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Mapbox.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_t2891093856 * ____mappings_1;
	// Mapbox.Json.JsonSerializer Mapbox.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t2280177768 * ___Serializer_2;
	// Mapbox.Json.Serialization.ITraceWriter Mapbox.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	RuntimeObject* ___TraceWriter_3;
	// Mapbox.Json.Serialization.JsonSerializerProxy Mapbox.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t2669598769 * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ____currentErrorContext_0)); }
	inline ErrorContext_t3853262553 * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_t3853262553 ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_t3853262553 * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentErrorContext_0), value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ____mappings_1)); }
	inline BidirectionalDictionary_2_t2891093856 * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_t2891093856 ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_t2891093856 * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_1), value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ___Serializer_2)); }
	inline JsonSerializer_t2280177768 * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t2280177768 ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t2280177768 * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_2), value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ___TraceWriter_3)); }
	inline RuntimeObject* get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline RuntimeObject** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(RuntimeObject* value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___TraceWriter_3), value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2072393995, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t2669598769 * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t2669598769 ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t2669598769 * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InternalSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALBASE_T2072393995_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T1214529541_H
#define U3CU3EC__DISPLAYCLASS29_0_T1214529541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t1214529541  : public RuntimeObject
{
public:
	// System.Int32 TMPro.InlineGraphicManager/<>c__DisplayClass29_0::index
	int32_t ___index_0;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t1214529541, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T1214529541_H
#ifndef FASTACTION_T3491443480_H
#define FASTACTION_T3491443480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FastAction
struct  FastAction_t3491443480  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<System.Action> TMPro.FastAction::delegates
	LinkedList_1_t104023486 * ___delegates_0;
	// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>> TMPro.FastAction::lookup
	Dictionary_2_t4196256347 * ___lookup_1;

public:
	inline static int32_t get_offset_of_delegates_0() { return static_cast<int32_t>(offsetof(FastAction_t3491443480, ___delegates_0)); }
	inline LinkedList_1_t104023486 * get_delegates_0() const { return ___delegates_0; }
	inline LinkedList_1_t104023486 ** get_address_of_delegates_0() { return &___delegates_0; }
	inline void set_delegates_0(LinkedList_1_t104023486 * value)
	{
		___delegates_0 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_0), value);
	}

	inline static int32_t get_offset_of_lookup_1() { return static_cast<int32_t>(offsetof(FastAction_t3491443480, ___lookup_1)); }
	inline Dictionary_2_t4196256347 * get_lookup_1() const { return ___lookup_1; }
	inline Dictionary_2_t4196256347 ** get_address_of_lookup_1() { return &___lookup_1; }
	inline void set_lookup_1(Dictionary_2_t4196256347 * value)
	{
		___lookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTACTION_T3491443480_H
#ifndef U3CGETENUMERATORU3ED__1_T1218601922_H
#define U3CGETENUMERATORU3ED__1_T1218601922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1
struct  U3CGetEnumeratorU3Ed__1_t1218601922  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>2__current
	JToken_t2379863307 * ___U3CU3E2__current_1;
	// Mapbox.Json.Linq.JProperty/JPropertyList Mapbox.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>4__this
	JPropertyList_t362393847 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t1218601922, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t1218601922, ___U3CU3E2__current_1)); }
	inline JToken_t2379863307 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t2379863307 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t2379863307 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t1218601922, ___U3CU3E4__this_2)); }
	inline JPropertyList_t362393847 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JPropertyList_t362393847 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JPropertyList_t362393847 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__1_T1218601922_H
#ifndef JPROPERTYLIST_T362393847_H
#define JPROPERTYLIST_T362393847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JProperty/JPropertyList
struct  JPropertyList_t362393847  : public RuntimeObject
{
public:
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JProperty/JPropertyList::_token
	JToken_t2379863307 * ____token_0;

public:
	inline static int32_t get_offset_of__token_0() { return static_cast<int32_t>(offsetof(JPropertyList_t362393847, ____token_0)); }
	inline JToken_t2379863307 * get__token_0() const { return ____token_0; }
	inline JToken_t2379863307 ** get_address_of__token_0() { return &____token_0; }
	inline void set__token_0(JToken_t2379863307 * value)
	{
		____token_0 = value;
		Il2CppCodeGenWriteBarrier((&____token_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYLIST_T362393847_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T1214464005_H
#define U3CU3EC__DISPLAYCLASS28_0_T1214464005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager/<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t1214464005  : public RuntimeObject
{
public:
	// System.Int32 TMPro.InlineGraphicManager/<>c__DisplayClass28_0::hashCode
	int32_t ___hashCode_0;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t1214464005, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T1214464005_H
#ifndef BSONOBJECTID_T1370814579_H
#define BSONOBJECTID_T1370814579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonObjectId
struct  BsonObjectId_t1370814579  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.Json.Bson.BsonObjectId::<Value>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonObjectId_t1370814579, ___U3CValueU3Ek__BackingField_0)); }
	inline ByteU5BU5D_t4116647657* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(ByteU5BU5D_t4116647657* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTID_T1370814579_H
#ifndef JTOKEN_T2379863307_H
#define JTOKEN_T2379863307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JToken
struct  JToken_t2379863307  : public RuntimeObject
{
public:
	// Mapbox.Json.Linq.JContainer Mapbox.Json.Linq.JToken::_parent
	JContainer_t3802169109 * ____parent_0;
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JToken::_previous
	JToken_t2379863307 * ____previous_1;
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JToken::_next
	JToken_t2379863307 * ____next_2;
	// System.Object Mapbox.Json.Linq.JToken::_annotations
	RuntimeObject * ____annotations_3;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(JToken_t2379863307, ____parent_0)); }
	inline JContainer_t3802169109 * get__parent_0() const { return ____parent_0; }
	inline JContainer_t3802169109 ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(JContainer_t3802169109 * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__previous_1() { return static_cast<int32_t>(offsetof(JToken_t2379863307, ____previous_1)); }
	inline JToken_t2379863307 * get__previous_1() const { return ____previous_1; }
	inline JToken_t2379863307 ** get_address_of__previous_1() { return &____previous_1; }
	inline void set__previous_1(JToken_t2379863307 * value)
	{
		____previous_1 = value;
		Il2CppCodeGenWriteBarrier((&____previous_1), value);
	}

	inline static int32_t get_offset_of__next_2() { return static_cast<int32_t>(offsetof(JToken_t2379863307, ____next_2)); }
	inline JToken_t2379863307 * get__next_2() const { return ____next_2; }
	inline JToken_t2379863307 ** get_address_of__next_2() { return &____next_2; }
	inline void set__next_2(JToken_t2379863307 * value)
	{
		____next_2 = value;
		Il2CppCodeGenWriteBarrier((&____next_2), value);
	}

	inline static int32_t get_offset_of__annotations_3() { return static_cast<int32_t>(offsetof(JToken_t2379863307, ____annotations_3)); }
	inline RuntimeObject * get__annotations_3() const { return ____annotations_3; }
	inline RuntimeObject ** get_address_of__annotations_3() { return &____annotations_3; }
	inline void set__annotations_3(RuntimeObject * value)
	{
		____annotations_3 = value;
		Il2CppCodeGenWriteBarrier((&____annotations_3), value);
	}
};

struct JToken_t2379863307_StaticFields
{
public:
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::BooleanTypes
	JTokenTypeU5BU5D_t3715042731* ___BooleanTypes_4;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::NumberTypes
	JTokenTypeU5BU5D_t3715042731* ___NumberTypes_5;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::StringTypes
	JTokenTypeU5BU5D_t3715042731* ___StringTypes_6;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::GuidTypes
	JTokenTypeU5BU5D_t3715042731* ___GuidTypes_7;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::TimeSpanTypes
	JTokenTypeU5BU5D_t3715042731* ___TimeSpanTypes_8;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::UriTypes
	JTokenTypeU5BU5D_t3715042731* ___UriTypes_9;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::CharTypes
	JTokenTypeU5BU5D_t3715042731* ___CharTypes_10;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::DateTimeTypes
	JTokenTypeU5BU5D_t3715042731* ___DateTimeTypes_11;
	// Mapbox.Json.Linq.JTokenType[] Mapbox.Json.Linq.JToken::BytesTypes
	JTokenTypeU5BU5D_t3715042731* ___BytesTypes_12;

public:
	inline static int32_t get_offset_of_BooleanTypes_4() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___BooleanTypes_4)); }
	inline JTokenTypeU5BU5D_t3715042731* get_BooleanTypes_4() const { return ___BooleanTypes_4; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_BooleanTypes_4() { return &___BooleanTypes_4; }
	inline void set_BooleanTypes_4(JTokenTypeU5BU5D_t3715042731* value)
	{
		___BooleanTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___BooleanTypes_4), value);
	}

	inline static int32_t get_offset_of_NumberTypes_5() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___NumberTypes_5)); }
	inline JTokenTypeU5BU5D_t3715042731* get_NumberTypes_5() const { return ___NumberTypes_5; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_NumberTypes_5() { return &___NumberTypes_5; }
	inline void set_NumberTypes_5(JTokenTypeU5BU5D_t3715042731* value)
	{
		___NumberTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___NumberTypes_5), value);
	}

	inline static int32_t get_offset_of_StringTypes_6() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___StringTypes_6)); }
	inline JTokenTypeU5BU5D_t3715042731* get_StringTypes_6() const { return ___StringTypes_6; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_StringTypes_6() { return &___StringTypes_6; }
	inline void set_StringTypes_6(JTokenTypeU5BU5D_t3715042731* value)
	{
		___StringTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___StringTypes_6), value);
	}

	inline static int32_t get_offset_of_GuidTypes_7() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___GuidTypes_7)); }
	inline JTokenTypeU5BU5D_t3715042731* get_GuidTypes_7() const { return ___GuidTypes_7; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_GuidTypes_7() { return &___GuidTypes_7; }
	inline void set_GuidTypes_7(JTokenTypeU5BU5D_t3715042731* value)
	{
		___GuidTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___GuidTypes_7), value);
	}

	inline static int32_t get_offset_of_TimeSpanTypes_8() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___TimeSpanTypes_8)); }
	inline JTokenTypeU5BU5D_t3715042731* get_TimeSpanTypes_8() const { return ___TimeSpanTypes_8; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_TimeSpanTypes_8() { return &___TimeSpanTypes_8; }
	inline void set_TimeSpanTypes_8(JTokenTypeU5BU5D_t3715042731* value)
	{
		___TimeSpanTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimeSpanTypes_8), value);
	}

	inline static int32_t get_offset_of_UriTypes_9() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___UriTypes_9)); }
	inline JTokenTypeU5BU5D_t3715042731* get_UriTypes_9() const { return ___UriTypes_9; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_UriTypes_9() { return &___UriTypes_9; }
	inline void set_UriTypes_9(JTokenTypeU5BU5D_t3715042731* value)
	{
		___UriTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriTypes_9), value);
	}

	inline static int32_t get_offset_of_CharTypes_10() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___CharTypes_10)); }
	inline JTokenTypeU5BU5D_t3715042731* get_CharTypes_10() const { return ___CharTypes_10; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_CharTypes_10() { return &___CharTypes_10; }
	inline void set_CharTypes_10(JTokenTypeU5BU5D_t3715042731* value)
	{
		___CharTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___CharTypes_10), value);
	}

	inline static int32_t get_offset_of_DateTimeTypes_11() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___DateTimeTypes_11)); }
	inline JTokenTypeU5BU5D_t3715042731* get_DateTimeTypes_11() const { return ___DateTimeTypes_11; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_DateTimeTypes_11() { return &___DateTimeTypes_11; }
	inline void set_DateTimeTypes_11(JTokenTypeU5BU5D_t3715042731* value)
	{
		___DateTimeTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeTypes_11), value);
	}

	inline static int32_t get_offset_of_BytesTypes_12() { return static_cast<int32_t>(offsetof(JToken_t2379863307_StaticFields, ___BytesTypes_12)); }
	inline JTokenTypeU5BU5D_t3715042731* get_BytesTypes_12() const { return ___BytesTypes_12; }
	inline JTokenTypeU5BU5D_t3715042731** get_address_of_BytesTypes_12() { return &___BytesTypes_12; }
	inline void set_BytesTypes_12(JTokenTypeU5BU5D_t3715042731* value)
	{
		___BytesTypes_12 = value;
		Il2CppCodeGenWriteBarrier((&___BytesTypes_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKEN_T2379863307_H
#ifndef XMLNODEWRAPPER_T1329998361_H
#define XMLNODEWRAPPER_T1329998361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XmlNodeWrapper
struct  XmlNodeWrapper_t1329998361  : public RuntimeObject
{
public:
	// System.Xml.XmlNode Mapbox.Json.Converters.XmlNodeWrapper::_node
	XmlNode_t3767805227 * ____node_0;
	// System.Collections.Generic.List`1<Mapbox.Json.Converters.IXmlNode> Mapbox.Json.Converters.XmlNodeWrapper::_childNodes
	List_1_t548158954 * ____childNodes_1;
	// System.Collections.Generic.List`1<Mapbox.Json.Converters.IXmlNode> Mapbox.Json.Converters.XmlNodeWrapper::_attributes
	List_1_t548158954 * ____attributes_2;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t1329998361, ____node_0)); }
	inline XmlNode_t3767805227 * get__node_0() const { return ____node_0; }
	inline XmlNode_t3767805227 ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(XmlNode_t3767805227 * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t1329998361, ____childNodes_1)); }
	inline List_1_t548158954 * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_t548158954 ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_t548158954 * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}

	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t1329998361, ____attributes_2)); }
	inline List_1_t548158954 * get__attributes_2() const { return ____attributes_2; }
	inline List_1_t548158954 ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_t548158954 * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEWRAPPER_T1329998361_H
#ifndef U3CU3EC_T1312267609_H
#define U3CU3EC_T1312267609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonTypeReflector/<>c
struct  U3CU3Ec_t1312267609  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1312267609_StaticFields
{
public:
	// Mapbox.Json.Serialization.JsonTypeReflector/<>c Mapbox.Json.Serialization.JsonTypeReflector/<>c::<>9
	U3CU3Ec_t1312267609 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.Type> Mapbox.Json.Serialization.JsonTypeReflector/<>c::<>9__21_1
	Func_2_t1850968970 * ___U3CU3E9__21_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1312267609_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1312267609 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1312267609 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1312267609 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__21_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1312267609_StaticFields, ___U3CU3E9__21_1_1)); }
	inline Func_2_t1850968970 * get_U3CU3E9__21_1_1() const { return ___U3CU3E9__21_1_1; }
	inline Func_2_t1850968970 ** get_address_of_U3CU3E9__21_1_1() { return &___U3CU3E9__21_1_1; }
	inline void set_U3CU3E9__21_1_1(Func_2_t1850968970 * value)
	{
		___U3CU3E9__21_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__21_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1312267609_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T596474585_H
#define U3CU3EC__DISPLAYCLASS21_0_T596474585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonTypeReflector/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t596474585  : public RuntimeObject
{
public:
	// System.Type Mapbox.Json.Serialization.JsonTypeReflector/<>c__DisplayClass21_0::type
	Type_t * ___type_0;
	// System.Func`1<System.Object> Mapbox.Json.Serialization.JsonTypeReflector/<>c__DisplayClass21_0::defaultConstructor
	Func_1_t2509852811 * ___defaultConstructor_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t596474585, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_defaultConstructor_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t596474585, ___defaultConstructor_1)); }
	inline Func_1_t2509852811 * get_defaultConstructor_1() const { return ___defaultConstructor_1; }
	inline Func_1_t2509852811 ** get_address_of_defaultConstructor_1() { return &___defaultConstructor_1; }
	inline void set_defaultConstructor_1(Func_1_t2509852811 * value)
	{
		___defaultConstructor_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultConstructor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T596474585_H
#ifndef BSONTOKEN_T3974775022_H
#define BSONTOKEN_T3974775022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonToken
struct  BsonToken_t3974775022  : public RuntimeObject
{
public:
	// Mapbox.Json.Bson.BsonToken Mapbox.Json.Bson.BsonToken::<Parent>k__BackingField
	BsonToken_t3974775022 * ___U3CParentU3Ek__BackingField_0;
	// System.Int32 Mapbox.Json.Bson.BsonToken::<CalculatedSize>k__BackingField
	int32_t ___U3CCalculatedSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonToken_t3974775022, ___U3CParentU3Ek__BackingField_0)); }
	inline BsonToken_t3974775022 * get_U3CParentU3Ek__BackingField_0() const { return ___U3CParentU3Ek__BackingField_0; }
	inline BsonToken_t3974775022 ** get_address_of_U3CParentU3Ek__BackingField_0() { return &___U3CParentU3Ek__BackingField_0; }
	inline void set_U3CParentU3Ek__BackingField_0(BsonToken_t3974775022 * value)
	{
		___U3CParentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonToken_t3974775022, ___U3CCalculatedSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCalculatedSizeU3Ek__BackingField_1() const { return ___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCalculatedSizeU3Ek__BackingField_1() { return &___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline void set_U3CCalculatedSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCalculatedSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTOKEN_T3974775022_H
#ifndef XOBJECTWRAPPER_T542134543_H
#define XOBJECTWRAPPER_T542134543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XObjectWrapper
struct  XObjectWrapper_t542134543  : public RuntimeObject
{
public:
	// System.Xml.Linq.XObject Mapbox.Json.Converters.XObjectWrapper::_xmlObject
	XObject_t1119084474 * ____xmlObject_0;

public:
	inline static int32_t get_offset_of__xmlObject_0() { return static_cast<int32_t>(offsetof(XObjectWrapper_t542134543, ____xmlObject_0)); }
	inline XObject_t1119084474 * get__xmlObject_0() const { return ____xmlObject_0; }
	inline XObject_t1119084474 ** get_address_of__xmlObject_0() { return &____xmlObject_0; }
	inline void set__xmlObject_0(XObject_t1119084474 * value)
	{
		____xmlObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____xmlObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECTWRAPPER_T542134543_H
#ifndef U3CU3EC_T1267612383_H
#define U3CU3EC_T1267612383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c
struct  U3CU3Ec_t1267612383  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1267612383_StaticFields
{
public:
	// Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c::<>9
	U3CU3Ec_t1267612383 * ___U3CU3E9_0;
	// System.Func`2<Mapbox.Json.Serialization.JsonProperty,System.String> Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__36_0
	Func_2_t540211779 * ___U3CU3E9__36_0_1;
	// System.Func`2<Mapbox.Json.Serialization.JsonProperty,System.String> Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__36_2
	Func_2_t540211779 * ___U3CU3E9__36_2_2;
	// System.Func`2<Mapbox.Json.Serialization.JsonProperty,Mapbox.Json.Serialization.JsonProperty> Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__41_0
	Func_2_t322737350 * ___U3CU3E9__41_0_3;
	// System.Func`2<Mapbox.Json.Serialization.JsonProperty,Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence> Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__41_1
	Func_2_t3040735037 * ___U3CU3E9__41_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1267612383_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1267612383 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1267612383 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1267612383 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1267612383_StaticFields, ___U3CU3E9__36_0_1)); }
	inline Func_2_t540211779 * get_U3CU3E9__36_0_1() const { return ___U3CU3E9__36_0_1; }
	inline Func_2_t540211779 ** get_address_of_U3CU3E9__36_0_1() { return &___U3CU3E9__36_0_1; }
	inline void set_U3CU3E9__36_0_1(Func_2_t540211779 * value)
	{
		___U3CU3E9__36_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1267612383_StaticFields, ___U3CU3E9__36_2_2)); }
	inline Func_2_t540211779 * get_U3CU3E9__36_2_2() const { return ___U3CU3E9__36_2_2; }
	inline Func_2_t540211779 ** get_address_of_U3CU3E9__36_2_2() { return &___U3CU3E9__36_2_2; }
	inline void set_U3CU3E9__36_2_2(Func_2_t540211779 * value)
	{
		___U3CU3E9__36_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1267612383_StaticFields, ___U3CU3E9__41_0_3)); }
	inline Func_2_t322737350 * get_U3CU3E9__41_0_3() const { return ___U3CU3E9__41_0_3; }
	inline Func_2_t322737350 ** get_address_of_U3CU3E9__41_0_3() { return &___U3CU3E9__41_0_3; }
	inline void set_U3CU3E9__41_0_3(Func_2_t322737350 * value)
	{
		___U3CU3E9__41_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1267612383_StaticFields, ___U3CU3E9__41_1_4)); }
	inline Func_2_t3040735037 * get_U3CU3E9__41_1_4() const { return ___U3CU3E9__41_1_4; }
	inline Func_2_t3040735037 ** get_address_of_U3CU3E9__41_1_4() { return &___U3CU3E9__41_1_4; }
	inline void set_U3CU3E9__41_1_4(Func_2_t3040735037 * value)
	{
		___U3CU3E9__41_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1267612383_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T1058909527_H
#define U3CU3EC__DISPLAYCLASS36_0_T1058909527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t1058909527  : public RuntimeObject
{
public:
	// Mapbox.Json.Serialization.JsonProperty Mapbox.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0::property
	JsonProperty_t1629976260 * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1058909527, ___property_0)); }
	inline JsonProperty_t1629976260 * get_property_0() const { return ___property_0; }
	inline JsonProperty_t1629976260 ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(JsonProperty_t1629976260 * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T1058909527_H
#ifndef REFLECTIONVALUEPROVIDER_T1842305502_H
#define REFLECTIONVALUEPROVIDER_T1842305502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.ReflectionValueProvider
struct  ReflectionValueProvider_t1842305502  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Mapbox.Json.Serialization.ReflectionValueProvider::_memberInfo
	MemberInfo_t * ____memberInfo_0;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(ReflectionValueProvider_t1842305502, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONVALUEPROVIDER_T1842305502_H
#ifndef COLLECTION_1_T1324219225_H
#define COLLECTION_1_T1324219225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Mapbox.Json.Linq.JToken>
struct  Collection_1_t1324219225  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t1324219225, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t1324219225, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T1324219225_H
#ifndef JSONCONVERTER_T472504469_H
#define JSONCONVERTER_T472504469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonConverter
struct  JsonConverter_t472504469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T472504469_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#define __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t2710994317 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2710994317__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T1548194903_H
#define __STATICARRAYINITTYPESIZEU3D10_T1548194903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct  __StaticArrayInitTypeSizeU3D10_t1548194903 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t1548194903__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T1548194903_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T1904621871_H
#define __STATICARRAYINITTYPESIZEU3D28_T1904621871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_t1904621871 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t1904621871__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T1904621871_H
#ifndef BSONARRAY_T3023973585_H
#define BSONARRAY_T3023973585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonArray
struct  BsonArray_t3023973585  : public BsonToken_t3974775022
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.Bson.BsonToken> Mapbox.Json.Bson.BsonArray::_children
	List_1_t1151882468 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonArray_t3023973585, ____children_2)); }
	inline List_1_t1151882468 * get__children_2() const { return ____children_2; }
	inline List_1_t1151882468 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t1151882468 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONARRAY_T3023973585_H
#ifndef BSONREGEX_T2801632588_H
#define BSONREGEX_T2801632588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonRegex
struct  BsonRegex_t2801632588  : public BsonToken_t3974775022
{
public:
	// Mapbox.Json.Bson.BsonString Mapbox.Json.Bson.BsonRegex::<Pattern>k__BackingField
	BsonString_t848819904 * ___U3CPatternU3Ek__BackingField_2;
	// Mapbox.Json.Bson.BsonString Mapbox.Json.Bson.BsonRegex::<Options>k__BackingField
	BsonString_t848819904 * ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BsonRegex_t2801632588, ___U3CPatternU3Ek__BackingField_2)); }
	inline BsonString_t848819904 * get_U3CPatternU3Ek__BackingField_2() const { return ___U3CPatternU3Ek__BackingField_2; }
	inline BsonString_t848819904 ** get_address_of_U3CPatternU3Ek__BackingField_2() { return &___U3CPatternU3Ek__BackingField_2; }
	inline void set_U3CPatternU3Ek__BackingField_2(BsonString_t848819904 * value)
	{
		___U3CPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonRegex_t2801632588, ___U3COptionsU3Ek__BackingField_3)); }
	inline BsonString_t848819904 * get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline BsonString_t848819904 ** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(BsonString_t848819904 * value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREGEX_T2801632588_H
#ifndef XATTRIBUTEWRAPPER_T1809740052_H
#define XATTRIBUTEWRAPPER_T1809740052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XAttributeWrapper
struct  XAttributeWrapper_t1809740052  : public XObjectWrapper_t542134543
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTEWRAPPER_T1809740052_H
#ifndef XMLNODECONVERTER_T568579346_H
#define XMLNODECONVERTER_T568579346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XmlNodeConverter
struct  XmlNodeConverter_t568579346  : public JsonConverter_t472504469
{
public:
	// System.String Mapbox.Json.Converters.XmlNodeConverter::<DeserializeRootElementName>k__BackingField
	String_t* ___U3CDeserializeRootElementNameU3Ek__BackingField_1;
	// System.Boolean Mapbox.Json.Converters.XmlNodeConverter::<WriteArrayAttribute>k__BackingField
	bool ___U3CWriteArrayAttributeU3Ek__BackingField_2;
	// System.Boolean Mapbox.Json.Converters.XmlNodeConverter::<OmitRootObject>k__BackingField
	bool ___U3COmitRootObjectU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t568579346, ___U3CDeserializeRootElementNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CDeserializeRootElementNameU3Ek__BackingField_1() const { return ___U3CDeserializeRootElementNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDeserializeRootElementNameU3Ek__BackingField_1() { return &___U3CDeserializeRootElementNameU3Ek__BackingField_1; }
	inline void set_U3CDeserializeRootElementNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CDeserializeRootElementNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeserializeRootElementNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t568579346, ___U3CWriteArrayAttributeU3Ek__BackingField_2)); }
	inline bool get_U3CWriteArrayAttributeU3Ek__BackingField_2() const { return ___U3CWriteArrayAttributeU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CWriteArrayAttributeU3Ek__BackingField_2() { return &___U3CWriteArrayAttributeU3Ek__BackingField_2; }
	inline void set_U3CWriteArrayAttributeU3Ek__BackingField_2(bool value)
	{
		___U3CWriteArrayAttributeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3COmitRootObjectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t568579346, ___U3COmitRootObjectU3Ek__BackingField_3)); }
	inline bool get_U3COmitRootObjectU3Ek__BackingField_3() const { return ___U3COmitRootObjectU3Ek__BackingField_3; }
	inline bool* get_address_of_U3COmitRootObjectU3Ek__BackingField_3() { return &___U3COmitRootObjectU3Ek__BackingField_3; }
	inline void set_U3COmitRootObjectU3Ek__BackingField_3(bool value)
	{
		___U3COmitRootObjectU3Ek__BackingField_3 = value;
	}
};

struct XmlNodeConverter_t568579346_StaticFields
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.Converters.IXmlNode> Mapbox.Json.Converters.XmlNodeConverter::EmptyChildNodes
	List_1_t548158954 * ___EmptyChildNodes_0;

public:
	inline static int32_t get_offset_of_EmptyChildNodes_0() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t568579346_StaticFields, ___EmptyChildNodes_0)); }
	inline List_1_t548158954 * get_EmptyChildNodes_0() const { return ___EmptyChildNodes_0; }
	inline List_1_t548158954 ** get_address_of_EmptyChildNodes_0() { return &___EmptyChildNodes_0; }
	inline void set_EmptyChildNodes_0(List_1_t548158954 * value)
	{
		___EmptyChildNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChildNodes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECONVERTER_T568579346_H
#ifndef BSONOBJECT_T2543849103_H
#define BSONOBJECT_T2543849103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonObject
struct  BsonObject_t2543849103  : public BsonToken_t3974775022
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.Bson.BsonProperty> Mapbox.Json.Bson.BsonObject::_children
	List_1_t1688264791 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonObject_t2543849103, ____children_2)); }
	inline List_1_t1688264791 * get__children_2() const { return ____children_2; }
	inline List_1_t1688264791 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t1688264791 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECT_T2543849103_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_T2710732173_H
#define __STATICARRAYINITTYPESIZEU3D52_T2710732173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52
struct  __StaticArrayInitTypeSizeU3D52_t2710732173 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_t2710732173__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_T2710732173_H
#ifndef KEYVALUEPAIR_2_T267824477_H
#define KEYVALUEPAIR_2_T267824477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Mapbox.Json.Linq.JToken>
struct  KeyValuePair_2_t267824477 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JToken_t2379863307 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t267824477, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t267824477, ___value_1)); }
	inline JToken_t2379863307 * get_value_1() const { return ___value_1; }
	inline JToken_t2379863307 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JToken_t2379863307 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T267824477_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef __STATICARRAYINITTYPESIZEU3D60_T1548129367_H
#define __STATICARRAYINITTYPESIZEU3D60_T1548129367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60
struct  __StaticArrayInitTypeSizeU3D60_t1548129367 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D60_t1548129367__padding[60];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D60_T1548129367_H
#ifndef __STATICARRAYINITTYPESIZEU3D84_T3518153195_H
#define __STATICARRAYINITTYPESIZEU3D84_T3518153195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=84
struct  __StaticArrayInitTypeSizeU3D84_t3518153195 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D84_t3518153195__padding[84];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D84_T3518153195_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef __STATICARRAYINITTYPESIZEU3D168_T2500897569_H
#define __STATICARRAYINITTYPESIZEU3D168_T2500897569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=168
struct  __StaticArrayInitTypeSizeU3D168_t2500897569 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D168_t2500897569__padding[168];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D168_T2500897569_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#define __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_t3297148301 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t3297148301__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef XCONTAINERWRAPPER_T652000426_H
#define XCONTAINERWRAPPER_T652000426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XContainerWrapper
struct  XContainerWrapper_t652000426  : public XObjectWrapper_t542134543
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.Converters.IXmlNode> Mapbox.Json.Converters.XContainerWrapper::_childNodes
	List_1_t548158954 * ____childNodes_1;

public:
	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XContainerWrapper_t652000426, ____childNodes_1)); }
	inline List_1_t548158954 * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_t548158954 ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_t548158954 * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCONTAINERWRAPPER_T652000426_H
#ifndef KEYVALUEPAIRCONVERTER_T3765595880_H
#define KEYVALUEPAIRCONVERTER_T3765595880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.KeyValuePairConverter
struct  KeyValuePairConverter_t3765595880  : public JsonConverter_t472504469
{
public:

public:
};

struct KeyValuePairConverter_t3765595880_StaticFields
{
public:
	// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,Mapbox.Json.Utilities.ReflectionObject> Mapbox.Json.Converters.KeyValuePairConverter::ReflectionObjectPerType
	ThreadSafeStore_2_t907930224 * ___ReflectionObjectPerType_0;

public:
	inline static int32_t get_offset_of_ReflectionObjectPerType_0() { return static_cast<int32_t>(offsetof(KeyValuePairConverter_t3765595880_StaticFields, ___ReflectionObjectPerType_0)); }
	inline ThreadSafeStore_2_t907930224 * get_ReflectionObjectPerType_0() const { return ___ReflectionObjectPerType_0; }
	inline ThreadSafeStore_2_t907930224 ** get_address_of_ReflectionObjectPerType_0() { return &___ReflectionObjectPerType_0; }
	inline void set_ReflectionObjectPerType_0(ThreadSafeStore_2_t907930224 * value)
	{
		___ReflectionObjectPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionObjectPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIRCONVERTER_T3765595880_H
#ifndef BSONOBJECTIDCONVERTER_T3375601290_H
#define BSONOBJECTIDCONVERTER_T3375601290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.BsonObjectIdConverter
struct  BsonObjectIdConverter_t3375601290  : public JsonConverter_t472504469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTIDCONVERTER_T3375601290_H
#ifndef REGEXCONVERTER_T2756728157_H
#define REGEXCONVERTER_T2756728157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.RegexConverter
struct  RegexConverter_t2756728157  : public JsonConverter_t472504469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCONVERTER_T2756728157_H
#ifndef XMLDOCUMENTWRAPPER_T607387303_H
#define XMLDOCUMENTWRAPPER_T607387303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XmlDocumentWrapper
struct  XmlDocumentWrapper_t607387303  : public XmlNodeWrapper_t1329998361
{
public:
	// System.Xml.XmlDocument Mapbox.Json.Converters.XmlDocumentWrapper::_document
	XmlDocument_t2837193595 * ____document_3;

public:
	inline static int32_t get_offset_of__document_3() { return static_cast<int32_t>(offsetof(XmlDocumentWrapper_t607387303, ____document_3)); }
	inline XmlDocument_t2837193595 * get__document_3() const { return ____document_3; }
	inline XmlDocument_t2837193595 ** get_address_of__document_3() { return &____document_3; }
	inline void set__document_3(XmlDocument_t2837193595 * value)
	{
		____document_3 = value;
		Il2CppCodeGenWriteBarrier((&____document_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTWRAPPER_T607387303_H
#ifndef XMLELEMENTWRAPPER_T547753851_H
#define XMLELEMENTWRAPPER_T547753851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XmlElementWrapper
struct  XmlElementWrapper_t547753851  : public XmlNodeWrapper_t1329998361
{
public:
	// System.Xml.XmlElement Mapbox.Json.Converters.XmlElementWrapper::_element
	XmlElement_t561603118 * ____element_3;

public:
	inline static int32_t get_offset_of__element_3() { return static_cast<int32_t>(offsetof(XmlElementWrapper_t547753851, ____element_3)); }
	inline XmlElement_t561603118 * get__element_3() const { return ____element_3; }
	inline XmlElement_t561603118 ** get_address_of__element_3() { return &____element_3; }
	inline void set__element_3(XmlElement_t561603118 * value)
	{
		____element_3 = value;
		Il2CppCodeGenWriteBarrier((&____element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTWRAPPER_T547753851_H
#ifndef XMLDECLARATIONWRAPPER_T3559561692_H
#define XMLDECLARATIONWRAPPER_T3559561692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XmlDeclarationWrapper
struct  XmlDeclarationWrapper_t3559561692  : public XmlNodeWrapper_t1329998361
{
public:
	// System.Xml.XmlDeclaration Mapbox.Json.Converters.XmlDeclarationWrapper::_declaration
	XmlDeclaration_t679870411 * ____declaration_3;

public:
	inline static int32_t get_offset_of__declaration_3() { return static_cast<int32_t>(offsetof(XmlDeclarationWrapper_t3559561692, ____declaration_3)); }
	inline XmlDeclaration_t679870411 * get__declaration_3() const { return ____declaration_3; }
	inline XmlDeclaration_t679870411 ** get_address_of__declaration_3() { return &____declaration_3; }
	inline void set__declaration_3(XmlDeclaration_t679870411 * value)
	{
		____declaration_3 = value;
		Il2CppCodeGenWriteBarrier((&____declaration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATIONWRAPPER_T3559561692_H
#ifndef ENTITYKEYMEMBERCONVERTER_T2931919244_H
#define ENTITYKEYMEMBERCONVERTER_T2931919244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.EntityKeyMemberConverter
struct  EntityKeyMemberConverter_t2931919244  : public JsonConverter_t472504469
{
public:

public:
};

struct EntityKeyMemberConverter_t2931919244_StaticFields
{
public:
	// Mapbox.Json.Utilities.ReflectionObject Mapbox.Json.Converters.EntityKeyMemberConverter::_reflectionObject
	ReflectionObject_t2904696228 * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(EntityKeyMemberConverter_t2931919244_StaticFields, ____reflectionObject_0)); }
	inline ReflectionObject_t2904696228 * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_t2904696228 ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_t2904696228 * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYKEYMEMBERCONVERTER_T2931919244_H
#ifndef JPROPERTYKEYEDCOLLECTION_T3049044894_H
#define JPROPERTYKEYEDCOLLECTION_T3049044894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JPropertyKeyedCollection
struct  JPropertyKeyedCollection_t3049044894  : public Collection_1_t1324219225
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Mapbox.Json.Linq.JToken> Mapbox.Json.Linq.JPropertyKeyedCollection::_dictionary
	Dictionary_2_t2165119606 * ____dictionary_3;

public:
	inline static int32_t get_offset_of__dictionary_3() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_t3049044894, ____dictionary_3)); }
	inline Dictionary_2_t2165119606 * get__dictionary_3() const { return ____dictionary_3; }
	inline Dictionary_2_t2165119606 ** get_address_of__dictionary_3() { return &____dictionary_3; }
	inline void set__dictionary_3(Dictionary_2_t2165119606 * value)
	{
		____dictionary_3 = value;
		Il2CppCodeGenWriteBarrier((&____dictionary_3), value);
	}
};

struct JPropertyKeyedCollection_t3049044894_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.String> Mapbox.Json.Linq.JPropertyKeyedCollection::Comparer
	RuntimeObject* ___Comparer_2;

public:
	inline static int32_t get_offset_of_Comparer_2() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_t3049044894_StaticFields, ___Comparer_2)); }
	inline RuntimeObject* get_Comparer_2() const { return ___Comparer_2; }
	inline RuntimeObject** get_address_of_Comparer_2() { return &___Comparer_2; }
	inline void set_Comparer_2(RuntimeObject* value)
	{
		___Comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Comparer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYKEYEDCOLLECTION_T3049044894_H
#ifndef ONERRORATTRIBUTE_T2846995309_H
#define ONERRORATTRIBUTE_T2846995309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.OnErrorAttribute
struct  OnErrorAttribute_t2846995309  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORATTRIBUTE_T2846995309_H
#ifndef JCONTAINER_T3802169109_H
#define JCONTAINER_T3802169109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JContainer
struct  JContainer_t3802169109  : public JToken_t2379863307
{
public:
	// System.Object Mapbox.Json.Linq.JContainer::_syncRoot
	RuntimeObject * ____syncRoot_13;

public:
	inline static int32_t get_offset_of__syncRoot_13() { return static_cast<int32_t>(offsetof(JContainer_t3802169109, ____syncRoot_13)); }
	inline RuntimeObject * get__syncRoot_13() const { return ____syncRoot_13; }
	inline RuntimeObject ** get_address_of__syncRoot_13() { return &____syncRoot_13; }
	inline void set__syncRoot_13(RuntimeObject * value)
	{
		____syncRoot_13 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONTAINER_T3802169109_H
#ifndef BINARYCONVERTER_T3784994590_H
#define BINARYCONVERTER_T3784994590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.BinaryConverter
struct  BinaryConverter_t3784994590  : public JsonConverter_t472504469
{
public:

public:
};

struct BinaryConverter_t3784994590_StaticFields
{
public:
	// Mapbox.Json.Utilities.ReflectionObject Mapbox.Json.Converters.BinaryConverter::_reflectionObject
	ReflectionObject_t2904696228 * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(BinaryConverter_t3784994590_StaticFields, ____reflectionObject_0)); }
	inline ReflectionObject_t2904696228 * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_t2904696228 ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_t2904696228 * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCONVERTER_T3784994590_H
#ifndef DATASETCONVERTER_T2458331083_H
#define DATASETCONVERTER_T2458331083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.DataSetConverter
struct  DataSetConverter_t2458331083  : public JsonConverter_t472504469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETCONVERTER_T2458331083_H
#ifndef DATATABLECONVERTER_T1000687607_H
#define DATATABLECONVERTER_T1000687607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.DataTableConverter
struct  DataTableConverter_t1000687607  : public JsonConverter_t472504469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLECONVERTER_T1000687607_H
#ifndef XMLDOCUMENTTYPEWRAPPER_T518648022_H
#define XMLDOCUMENTTYPEWRAPPER_T518648022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XmlDocumentTypeWrapper
struct  XmlDocumentTypeWrapper_t518648022  : public XmlNodeWrapper_t1329998361
{
public:
	// System.Xml.XmlDocumentType Mapbox.Json.Converters.XmlDocumentTypeWrapper::_documentType
	XmlDocumentType_t4112370061 * ____documentType_3;

public:
	inline static int32_t get_offset_of__documentType_3() { return static_cast<int32_t>(offsetof(XmlDocumentTypeWrapper_t518648022, ____documentType_3)); }
	inline XmlDocumentType_t4112370061 * get__documentType_3() const { return ____documentType_3; }
	inline XmlDocumentType_t4112370061 ** get_address_of__documentType_3() { return &____documentType_3; }
	inline void set__documentType_3(XmlDocumentType_t4112370061 * value)
	{
		____documentType_3 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPEWRAPPER_T518648022_H
#ifndef XTEXTWRAPPER_T2743256062_H
#define XTEXTWRAPPER_T2743256062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XTextWrapper
struct  XTextWrapper_t2743256062  : public XObjectWrapper_t542134543
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XTEXTWRAPPER_T2743256062_H
#ifndef JSONSERIALIZERINTERNALWRITER_T2365347854_H
#define JSONSERIALIZERINTERNALWRITER_T2365347854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalWriter
struct  JsonSerializerInternalWriter_t2365347854  : public JsonSerializerInternalBase_t2072393995
{
public:
	// System.Type Mapbox.Json.Serialization.JsonSerializerInternalWriter::_rootType
	Type_t * ____rootType_5;
	// System.Int32 Mapbox.Json.Serialization.JsonSerializerInternalWriter::_rootLevel
	int32_t ____rootLevel_6;
	// System.Collections.Generic.List`1<System.Object> Mapbox.Json.Serialization.JsonSerializerInternalWriter::_serializeStack
	List_1_t257213610 * ____serializeStack_7;

public:
	inline static int32_t get_offset_of__rootType_5() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t2365347854, ____rootType_5)); }
	inline Type_t * get__rootType_5() const { return ____rootType_5; }
	inline Type_t ** get_address_of__rootType_5() { return &____rootType_5; }
	inline void set__rootType_5(Type_t * value)
	{
		____rootType_5 = value;
		Il2CppCodeGenWriteBarrier((&____rootType_5), value);
	}

	inline static int32_t get_offset_of__rootLevel_6() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t2365347854, ____rootLevel_6)); }
	inline int32_t get__rootLevel_6() const { return ____rootLevel_6; }
	inline int32_t* get_address_of__rootLevel_6() { return &____rootLevel_6; }
	inline void set__rootLevel_6(int32_t value)
	{
		____rootLevel_6 = value;
	}

	inline static int32_t get_offset_of__serializeStack_7() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t2365347854, ____serializeStack_7)); }
	inline List_1_t257213610 * get__serializeStack_7() const { return ____serializeStack_7; }
	inline List_1_t257213610 ** get_address_of__serializeStack_7() { return &____serializeStack_7; }
	inline void set__serializeStack_7(List_1_t257213610 * value)
	{
		____serializeStack_7 = value;
		Il2CppCodeGenWriteBarrier((&____serializeStack_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALWRITER_T2365347854_H
#ifndef XDOCUMENTTYPEWRAPPER_T2186808262_H
#define XDOCUMENTTYPEWRAPPER_T2186808262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XDocumentTypeWrapper
struct  XDocumentTypeWrapper_t2186808262  : public XObjectWrapper_t542134543
{
public:
	// System.Xml.Linq.XDocumentType Mapbox.Json.Converters.XDocumentTypeWrapper::_documentType
	XDocumentType_t1853592271 * ____documentType_1;

public:
	inline static int32_t get_offset_of__documentType_1() { return static_cast<int32_t>(offsetof(XDocumentTypeWrapper_t2186808262, ____documentType_1)); }
	inline XDocumentType_t1853592271 * get__documentType_1() const { return ____documentType_1; }
	inline XDocumentType_t1853592271 ** get_address_of__documentType_1() { return &____documentType_1; }
	inline void set__documentType_1(XDocumentType_t1853592271 * value)
	{
		____documentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTTYPEWRAPPER_T2186808262_H
#ifndef XDECLARATIONWRAPPER_T2232240502_H
#define XDECLARATIONWRAPPER_T2232240502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XDeclarationWrapper
struct  XDeclarationWrapper_t2232240502  : public XObjectWrapper_t542134543
{
public:
	// System.Xml.Linq.XDeclaration Mapbox.Json.Converters.XDeclarationWrapper::<Declaration>k__BackingField
	XDeclaration_t2907650823 * ___U3CDeclarationU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDeclarationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XDeclarationWrapper_t2232240502, ___U3CDeclarationU3Ek__BackingField_1)); }
	inline XDeclaration_t2907650823 * get_U3CDeclarationU3Ek__BackingField_1() const { return ___U3CDeclarationU3Ek__BackingField_1; }
	inline XDeclaration_t2907650823 ** get_address_of_U3CDeclarationU3Ek__BackingField_1() { return &___U3CDeclarationU3Ek__BackingField_1; }
	inline void set_U3CDeclarationU3Ek__BackingField_1(XDeclaration_t2907650823 * value)
	{
		___U3CDeclarationU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclarationU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDECLARATIONWRAPPER_T2232240502_H
#ifndef XCOMMENTWRAPPER_T298428753_H
#define XCOMMENTWRAPPER_T298428753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XCommentWrapper
struct  XCommentWrapper_t298428753  : public XObjectWrapper_t542134543
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCOMMENTWRAPPER_T298428753_H
#ifndef XPROCESSINGINSTRUCTIONWRAPPER_T218556294_H
#define XPROCESSINGINSTRUCTIONWRAPPER_T218556294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XProcessingInstructionWrapper
struct  XProcessingInstructionWrapper_t218556294  : public XObjectWrapper_t542134543
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROCESSINGINSTRUCTIONWRAPPER_T218556294_H
#ifndef REFERENCELOOPHANDLING_T1937914466_H
#define REFERENCELOOPHANDLING_T1937914466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_t1937914466 
{
public:
	// System.Int32 Mapbox.Json.ReferenceLoopHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_t1937914466, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_T1937914466_H
#ifndef MISSINGMEMBERHANDLING_T2057357684_H
#define MISSINGMEMBERHANDLING_T2057357684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.MissingMemberHandling
struct  MissingMemberHandling_t2057357684 
{
public:
	// System.Int32 Mapbox.Json.MissingMemberHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t2057357684, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T2057357684_H
#ifndef JSONTYPEREFLECTOR_T2360025382_H
#define JSONTYPEREFLECTOR_T2360025382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonTypeReflector
struct  JsonTypeReflector_t2360025382  : public RuntimeObject
{
public:

public:
};

struct JsonTypeReflector_t2360025382_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> Mapbox.Json.Serialization.JsonTypeReflector::_fullyTrusted
	Nullable_1_t1819850047  ____fullyTrusted_0;
	// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],System.Object>> Mapbox.Json.Serialization.JsonTypeReflector::CreatorCache
	ThreadSafeStore_2_t2733207365 * ___CreatorCache_1;
	// Mapbox.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type> Mapbox.Json.Serialization.JsonTypeReflector::AssociatedMetadataTypesCache
	ThreadSafeStore_2_t487178756 * ___AssociatedMetadataTypesCache_2;
	// Mapbox.Json.Utilities.ReflectionObject Mapbox.Json.Serialization.JsonTypeReflector::_metadataTypeAttributeReflectionObject
	ReflectionObject_t2904696228 * ____metadataTypeAttributeReflectionObject_3;

public:
	inline static int32_t get_offset_of__fullyTrusted_0() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t2360025382_StaticFields, ____fullyTrusted_0)); }
	inline Nullable_1_t1819850047  get__fullyTrusted_0() const { return ____fullyTrusted_0; }
	inline Nullable_1_t1819850047 * get_address_of__fullyTrusted_0() { return &____fullyTrusted_0; }
	inline void set__fullyTrusted_0(Nullable_1_t1819850047  value)
	{
		____fullyTrusted_0 = value;
	}

	inline static int32_t get_offset_of_CreatorCache_1() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t2360025382_StaticFields, ___CreatorCache_1)); }
	inline ThreadSafeStore_2_t2733207365 * get_CreatorCache_1() const { return ___CreatorCache_1; }
	inline ThreadSafeStore_2_t2733207365 ** get_address_of_CreatorCache_1() { return &___CreatorCache_1; }
	inline void set_CreatorCache_1(ThreadSafeStore_2_t2733207365 * value)
	{
		___CreatorCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___CreatorCache_1), value);
	}

	inline static int32_t get_offset_of_AssociatedMetadataTypesCache_2() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t2360025382_StaticFields, ___AssociatedMetadataTypesCache_2)); }
	inline ThreadSafeStore_2_t487178756 * get_AssociatedMetadataTypesCache_2() const { return ___AssociatedMetadataTypesCache_2; }
	inline ThreadSafeStore_2_t487178756 ** get_address_of_AssociatedMetadataTypesCache_2() { return &___AssociatedMetadataTypesCache_2; }
	inline void set_AssociatedMetadataTypesCache_2(ThreadSafeStore_2_t487178756 * value)
	{
		___AssociatedMetadataTypesCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___AssociatedMetadataTypesCache_2), value);
	}

	inline static int32_t get_offset_of__metadataTypeAttributeReflectionObject_3() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t2360025382_StaticFields, ____metadataTypeAttributeReflectionObject_3)); }
	inline ReflectionObject_t2904696228 * get__metadataTypeAttributeReflectionObject_3() const { return ____metadataTypeAttributeReflectionObject_3; }
	inline ReflectionObject_t2904696228 ** get_address_of__metadataTypeAttributeReflectionObject_3() { return &____metadataTypeAttributeReflectionObject_3; }
	inline void set__metadataTypeAttributeReflectionObject_3(ReflectionObject_t2904696228 * value)
	{
		____metadataTypeAttributeReflectionObject_3 = value;
		Il2CppCodeGenWriteBarrier((&____metadataTypeAttributeReflectionObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPEREFLECTOR_T2360025382_H
#ifndef OBJECTCREATIONHANDLING_T848553946_H
#define OBJECTCREATIONHANDLING_T848553946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t848553946 
{
public:
	// System.Int32 Mapbox.Json.ObjectCreationHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t848553946, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T848553946_H
#ifndef NULLVALUEHANDLING_T225534363_H
#define NULLVALUEHANDLING_T225534363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.NullValueHandling
struct  NullValueHandling_t225534363 
{
public:
	// System.Int32 Mapbox.Json.NullValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NullValueHandling_t225534363, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T225534363_H
#ifndef DEFAULTVALUEHANDLING_T1787134227_H
#define DEFAULTVALUEHANDLING_T1787134227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DefaultValueHandling
struct  DefaultValueHandling_t1787134227 
{
public:
	// System.Int32 Mapbox.Json.DefaultValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t1787134227, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_T1787134227_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef PROPERTYPRESENCE_T53006651_H
#define PROPERTYPRESENCE_T53006651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence
struct  PropertyPresence_t53006651 
{
public:
	// System.Int32 Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyPresence_t53006651, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYPRESENCE_T53006651_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TYPENAMEHANDLING_T3932606415_H
#define TYPENAMEHANDLING_T3932606415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.TypeNameHandling
struct  TypeNameHandling_t3932606415 
{
public:
	// System.Int32 Mapbox.Json.TypeNameHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeNameHandling_t3932606415, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T3932606415_H
#ifndef TYPENAMEASSEMBLYFORMATHANDLING_T1336341744_H
#define TYPENAMEASSEMBLYFORMATHANDLING_T1336341744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.TypeNameAssemblyFormatHandling
struct  TypeNameAssemblyFormatHandling_t1336341744 
{
public:
	// System.Int32 Mapbox.Json.TypeNameAssemblyFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeNameAssemblyFormatHandling_t1336341744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEASSEMBLYFORMATHANDLING_T1336341744_H
#ifndef PRESERVEREFERENCESHANDLING_T2574024936_H
#define PRESERVEREFERENCESHANDLING_T2574024936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_t2574024936 
{
public:
	// System.Int32 Mapbox.Json.PreserveReferencesHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_t2574024936, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_T2574024936_H
#ifndef COMMENTHANDLING_T1085241148_H
#define COMMENTHANDLING_T1085241148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.CommentHandling
struct  CommentHandling_t1085241148 
{
public:
	// System.Int32 Mapbox.Json.Linq.CommentHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CommentHandling_t1085241148, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENTHANDLING_T1085241148_H
#ifndef CONSTRUCTORHANDLING_T3076865779_H
#define CONSTRUCTORHANDLING_T3076865779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.ConstructorHandling
struct  ConstructorHandling_t3076865779 
{
public:
	// System.Int32 Mapbox.Json.ConstructorHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConstructorHandling_t3076865779, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_T3076865779_H
#ifndef STREAMINGCONTEXTSTATES_T3580100459_H
#define STREAMINGCONTEXTSTATES_T3580100459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t3580100459 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t3580100459, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T3580100459_H
#ifndef FLOATPARSEHANDLING_T2358379491_H
#define FLOATPARSEHANDLING_T2358379491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.FloatParseHandling
struct  FloatParseHandling_t2358379491 
{
public:
	// System.Int32 Mapbox.Json.FloatParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatParseHandling_t2358379491, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T2358379491_H
#ifndef FLOATFORMATHANDLING_T496253539_H
#define FLOATFORMATHANDLING_T496253539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.FloatFormatHandling
struct  FloatFormatHandling_t496253539 
{
public:
	// System.Int32 Mapbox.Json.FloatFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatFormatHandling_t496253539, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_T496253539_H
#ifndef STATE_T664584870_H
#define STATE_T664584870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonWriter/State
struct  State_t664584870 
{
public:
	// System.Int32 Mapbox.Json.JsonWriter/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t664584870, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T664584870_H
#ifndef FORMATTING_T2995144369_H
#define FORMATTING_T2995144369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Formatting
struct  Formatting_t2995144369 
{
public:
	// System.Int32 Mapbox.Json.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t2995144369, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T2995144369_H
#ifndef STRINGESCAPEHANDLING_T524910018_H
#define STRINGESCAPEHANDLING_T524910018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.StringEscapeHandling
struct  StringEscapeHandling_t524910018 
{
public:
	// System.Int32 Mapbox.Json.StringEscapeHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t524910018, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T524910018_H
#ifndef DATEFORMATHANDLING_T4226114183_H
#define DATEFORMATHANDLING_T4226114183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DateFormatHandling
struct  DateFormatHandling_t4226114183 
{
public:
	// System.Int32 Mapbox.Json.DateFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateFormatHandling_t4226114183, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T4226114183_H
#ifndef READTYPE_T187627822_H
#define READTYPE_T187627822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.ReadType
struct  ReadType_t187627822 
{
public:
	// System.Int32 Mapbox.Json.ReadType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadType_t187627822, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T187627822_H
#ifndef METADATAPROPERTYHANDLING_T1846941205_H
#define METADATAPROPERTYHANDLING_T1846941205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t1846941205 
{
public:
	// System.Int32 Mapbox.Json.MetadataPropertyHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t1846941205, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T1846941205_H
#ifndef PRIMITIVETYPECODE_T2887194885_H
#define PRIMITIVETYPECODE_T2887194885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t2887194885 
{
public:
	// System.Int32 Mapbox.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t2887194885, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T2887194885_H
#ifndef JSONTOKEN_T339275982_H
#define JSONTOKEN_T339275982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonToken
struct  JsonToken_t339275982 
{
public:
	// System.Int32 Mapbox.Json.JsonToken::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonToken_t339275982, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T339275982_H
#ifndef STATE_T433406742_H
#define STATE_T433406742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonReader/State
struct  State_t433406742 
{
public:
	// System.Int32 Mapbox.Json.JsonReader/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t433406742, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T433406742_H
#ifndef DATETIMEZONEHANDLING_T1164602176_H
#define DATETIMEZONEHANDLING_T1164602176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t1164602176 
{
public:
	// System.Int32 Mapbox.Json.DateTimeZoneHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t1164602176, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T1164602176_H
#ifndef DATEPARSEHANDLING_T2175047150_H
#define DATEPARSEHANDLING_T2175047150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.DateParseHandling
struct  DateParseHandling_t2175047150 
{
public:
	// System.Int32 Mapbox.Json.DateParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateParseHandling_t2175047150, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T2175047150_H
#ifndef JSONCONTAINERTYPE_T4094137386_H
#define JSONCONTAINERTYPE_T4094137386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonContainerType
struct  JsonContainerType_t4094137386 
{
public:
	// System.Int32 Mapbox.Json.JsonContainerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContainerType_t4094137386, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T4094137386_H
#ifndef JSONCONTRACTTYPE_T1860678823_H
#define JSONCONTRACTTYPE_T1860678823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonContractType
struct  JsonContractType_t1860678823 
{
public:
	// System.Int32 Mapbox.Json.Serialization.JsonContractType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContractType_t1860678823, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_T1860678823_H
#ifndef LINEINFOHANDLING_T1348595058_H
#define LINEINFOHANDLING_T1348595058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.LineInfoHandling
struct  LineInfoHandling_t1348595058 
{
public:
	// System.Int32 Mapbox.Json.Linq.LineInfoHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineInfoHandling_t1348595058, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOHANDLING_T1348595058_H
#ifndef JCONSTRUCTOR_T3420863255_H
#define JCONSTRUCTOR_T3420863255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JConstructor
struct  JConstructor_t3420863255  : public JContainer_t3802169109
{
public:
	// System.String Mapbox.Json.Linq.JConstructor::_name
	String_t* ____name_14;
	// System.Collections.Generic.List`1<Mapbox.Json.Linq.JToken> Mapbox.Json.Linq.JConstructor::_values
	List_1_t3851938049 * ____values_15;

public:
	inline static int32_t get_offset_of__name_14() { return static_cast<int32_t>(offsetof(JConstructor_t3420863255, ____name_14)); }
	inline String_t* get__name_14() const { return ____name_14; }
	inline String_t** get_address_of__name_14() { return &____name_14; }
	inline void set__name_14(String_t* value)
	{
		____name_14 = value;
		Il2CppCodeGenWriteBarrier((&____name_14), value);
	}

	inline static int32_t get_offset_of__values_15() { return static_cast<int32_t>(offsetof(JConstructor_t3420863255, ____values_15)); }
	inline List_1_t3851938049 * get__values_15() const { return ____values_15; }
	inline List_1_t3851938049 ** get_address_of__values_15() { return &____values_15; }
	inline void set__values_15(List_1_t3851938049 * value)
	{
		____values_15 = value;
		Il2CppCodeGenWriteBarrier((&____values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONSTRUCTOR_T3420863255_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255369  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=84 <PrivateImplementationDetails>::2BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63
	__StaticArrayInitTypeSizeU3D84_t3518153195  ___2BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::309F1F566327763634CE1DC6323D5D9C398F4D42
	__StaticArrayInitTypeSizeU3D120_t3297148301  ___309F1F566327763634CE1DC6323D5D9C398F4D42_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=168 <PrivateImplementationDetails>::55357EC07A4C0DA4B54C468ED12E82299968130F
	__StaticArrayInitTypeSizeU3D168_t2500897569  ___55357EC07A4C0DA4B54C468ED12E82299968130F_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=60 <PrivateImplementationDetails>::8D8384B5ECAC0436BBA54D23E278830A8800475A
	__StaticArrayInitTypeSizeU3D60_t1548129367  ___8D8384B5ECAC0436BBA54D23E278830A8800475A_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::93F6CC875194F5F37568D800D647ED31D6E33EC3
	__StaticArrayInitTypeSizeU3D120_t3297148301  ___93F6CC875194F5F37568D800D647ED31D6E33EC3_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_t1904621871  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::ADFD2E1C801C825415DD53F4F2F72A13B389313C
	__StaticArrayInitTypeSizeU3D12_t2710994317  ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB
	__StaticArrayInitTypeSizeU3D10_t1548194903  ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::DD3AEFEADB1CD615F3017763F1568179FEE640B0
	__StaticArrayInitTypeSizeU3D52_t2710732173  ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::E92B39D8233061927D9ACDE54665E68E7535635A
	__StaticArrayInitTypeSizeU3D52_t2710732173  ___E92B39D8233061927D9ACDE54665E68E7535635A_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=168 <PrivateImplementationDetails>::F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B
	__StaticArrayInitTypeSizeU3D168_t2500897569  ___F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10;

public:
	inline static int32_t get_offset_of_U32BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___2BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0)); }
	inline __StaticArrayInitTypeSizeU3D84_t3518153195  get_U32BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0() const { return ___2BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0; }
	inline __StaticArrayInitTypeSizeU3D84_t3518153195 * get_address_of_U32BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0() { return &___2BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0; }
	inline void set_U32BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0(__StaticArrayInitTypeSizeU3D84_t3518153195  value)
	{
		___2BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0 = value;
	}

	inline static int32_t get_offset_of_U3309F1F566327763634CE1DC6323D5D9C398F4D42_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___309F1F566327763634CE1DC6323D5D9C398F4D42_1)); }
	inline __StaticArrayInitTypeSizeU3D120_t3297148301  get_U3309F1F566327763634CE1DC6323D5D9C398F4D42_1() const { return ___309F1F566327763634CE1DC6323D5D9C398F4D42_1; }
	inline __StaticArrayInitTypeSizeU3D120_t3297148301 * get_address_of_U3309F1F566327763634CE1DC6323D5D9C398F4D42_1() { return &___309F1F566327763634CE1DC6323D5D9C398F4D42_1; }
	inline void set_U3309F1F566327763634CE1DC6323D5D9C398F4D42_1(__StaticArrayInitTypeSizeU3D120_t3297148301  value)
	{
		___309F1F566327763634CE1DC6323D5D9C398F4D42_1 = value;
	}

	inline static int32_t get_offset_of_U355357EC07A4C0DA4B54C468ED12E82299968130F_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___55357EC07A4C0DA4B54C468ED12E82299968130F_2)); }
	inline __StaticArrayInitTypeSizeU3D168_t2500897569  get_U355357EC07A4C0DA4B54C468ED12E82299968130F_2() const { return ___55357EC07A4C0DA4B54C468ED12E82299968130F_2; }
	inline __StaticArrayInitTypeSizeU3D168_t2500897569 * get_address_of_U355357EC07A4C0DA4B54C468ED12E82299968130F_2() { return &___55357EC07A4C0DA4B54C468ED12E82299968130F_2; }
	inline void set_U355357EC07A4C0DA4B54C468ED12E82299968130F_2(__StaticArrayInitTypeSizeU3D168_t2500897569  value)
	{
		___55357EC07A4C0DA4B54C468ED12E82299968130F_2 = value;
	}

	inline static int32_t get_offset_of_U38D8384B5ECAC0436BBA54D23E278830A8800475A_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___8D8384B5ECAC0436BBA54D23E278830A8800475A_3)); }
	inline __StaticArrayInitTypeSizeU3D60_t1548129367  get_U38D8384B5ECAC0436BBA54D23E278830A8800475A_3() const { return ___8D8384B5ECAC0436BBA54D23E278830A8800475A_3; }
	inline __StaticArrayInitTypeSizeU3D60_t1548129367 * get_address_of_U38D8384B5ECAC0436BBA54D23E278830A8800475A_3() { return &___8D8384B5ECAC0436BBA54D23E278830A8800475A_3; }
	inline void set_U38D8384B5ECAC0436BBA54D23E278830A8800475A_3(__StaticArrayInitTypeSizeU3D60_t1548129367  value)
	{
		___8D8384B5ECAC0436BBA54D23E278830A8800475A_3 = value;
	}

	inline static int32_t get_offset_of_U393F6CC875194F5F37568D800D647ED31D6E33EC3_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___93F6CC875194F5F37568D800D647ED31D6E33EC3_4)); }
	inline __StaticArrayInitTypeSizeU3D120_t3297148301  get_U393F6CC875194F5F37568D800D647ED31D6E33EC3_4() const { return ___93F6CC875194F5F37568D800D647ED31D6E33EC3_4; }
	inline __StaticArrayInitTypeSizeU3D120_t3297148301 * get_address_of_U393F6CC875194F5F37568D800D647ED31D6E33EC3_4() { return &___93F6CC875194F5F37568D800D647ED31D6E33EC3_4; }
	inline void set_U393F6CC875194F5F37568D800D647ED31D6E33EC3_4(__StaticArrayInitTypeSizeU3D120_t3297148301  value)
	{
		___93F6CC875194F5F37568D800D647ED31D6E33EC3_4 = value;
	}

	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_5)); }
	inline __StaticArrayInitTypeSizeU3D28_t1904621871  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_5() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_5; }
	inline __StaticArrayInitTypeSizeU3D28_t1904621871 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_5() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_5; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_5(__StaticArrayInitTypeSizeU3D28_t1904621871  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_5 = value;
	}

	inline static int32_t get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_6)); }
	inline __StaticArrayInitTypeSizeU3D12_t2710994317  get_ADFD2E1C801C825415DD53F4F2F72A13B389313C_6() const { return ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_6; }
	inline __StaticArrayInitTypeSizeU3D12_t2710994317 * get_address_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_6() { return &___ADFD2E1C801C825415DD53F4F2F72A13B389313C_6; }
	inline void set_ADFD2E1C801C825415DD53F4F2F72A13B389313C_6(__StaticArrayInitTypeSizeU3D12_t2710994317  value)
	{
		___ADFD2E1C801C825415DD53F4F2F72A13B389313C_6 = value;
	}

	inline static int32_t get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7)); }
	inline __StaticArrayInitTypeSizeU3D10_t1548194903  get_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7() const { return ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7; }
	inline __StaticArrayInitTypeSizeU3D10_t1548194903 * get_address_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7() { return &___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7; }
	inline void set_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7(__StaticArrayInitTypeSizeU3D10_t1548194903  value)
	{
		___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7 = value;
	}

	inline static int32_t get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_8)); }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173  get_DD3AEFEADB1CD615F3017763F1568179FEE640B0_8() const { return ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_8; }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173 * get_address_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_8() { return &___DD3AEFEADB1CD615F3017763F1568179FEE640B0_8; }
	inline void set_DD3AEFEADB1CD615F3017763F1568179FEE640B0_8(__StaticArrayInitTypeSizeU3D52_t2710732173  value)
	{
		___DD3AEFEADB1CD615F3017763F1568179FEE640B0_8 = value;
	}

	inline static int32_t get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___E92B39D8233061927D9ACDE54665E68E7535635A_9)); }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173  get_E92B39D8233061927D9ACDE54665E68E7535635A_9() const { return ___E92B39D8233061927D9ACDE54665E68E7535635A_9; }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173 * get_address_of_E92B39D8233061927D9ACDE54665E68E7535635A_9() { return &___E92B39D8233061927D9ACDE54665E68E7535635A_9; }
	inline void set_E92B39D8233061927D9ACDE54665E68E7535635A_9(__StaticArrayInitTypeSizeU3D52_t2710732173  value)
	{
		___E92B39D8233061927D9ACDE54665E68E7535635A_9 = value;
	}

	inline static int32_t get_offset_of_F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10)); }
	inline __StaticArrayInitTypeSizeU3D168_t2500897569  get_F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10() const { return ___F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10; }
	inline __StaticArrayInitTypeSizeU3D168_t2500897569 * get_address_of_F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10() { return &___F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10; }
	inline void set_F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10(__StaticArrayInitTypeSizeU3D168_t2500897569  value)
	{
		___F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifndef JPROPERTY_T1073345108_H
#define JPROPERTY_T1073345108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JProperty
struct  JProperty_t1073345108  : public JContainer_t3802169109
{
public:
	// Mapbox.Json.Linq.JProperty/JPropertyList Mapbox.Json.Linq.JProperty::_content
	JPropertyList_t362393847 * ____content_14;
	// System.String Mapbox.Json.Linq.JProperty::_name
	String_t* ____name_15;

public:
	inline static int32_t get_offset_of__content_14() { return static_cast<int32_t>(offsetof(JProperty_t1073345108, ____content_14)); }
	inline JPropertyList_t362393847 * get__content_14() const { return ____content_14; }
	inline JPropertyList_t362393847 ** get_address_of__content_14() { return &____content_14; }
	inline void set__content_14(JPropertyList_t362393847 * value)
	{
		____content_14 = value;
		Il2CppCodeGenWriteBarrier((&____content_14), value);
	}

	inline static int32_t get_offset_of__name_15() { return static_cast<int32_t>(offsetof(JProperty_t1073345108, ____name_15)); }
	inline String_t* get__name_15() const { return ____name_15; }
	inline String_t** get_address_of__name_15() { return &____name_15; }
	inline void set__name_15(String_t* value)
	{
		____name_15 = value;
		Il2CppCodeGenWriteBarrier((&____name_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTY_T1073345108_H
#ifndef JOBJECT_T877230766_H
#define JOBJECT_T877230766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JObject
struct  JObject_t877230766  : public JContainer_t3802169109
{
public:
	// Mapbox.Json.Linq.JPropertyKeyedCollection Mapbox.Json.Linq.JObject::_properties
	JPropertyKeyedCollection_t3049044894 * ____properties_14;
	// System.ComponentModel.PropertyChangedEventHandler Mapbox.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t3836340606 * ___PropertyChanged_15;

public:
	inline static int32_t get_offset_of__properties_14() { return static_cast<int32_t>(offsetof(JObject_t877230766, ____properties_14)); }
	inline JPropertyKeyedCollection_t3049044894 * get__properties_14() const { return ____properties_14; }
	inline JPropertyKeyedCollection_t3049044894 ** get_address_of__properties_14() { return &____properties_14; }
	inline void set__properties_14(JPropertyKeyedCollection_t3049044894 * value)
	{
		____properties_14 = value;
		Il2CppCodeGenWriteBarrier((&____properties_14), value);
	}

	inline static int32_t get_offset_of_PropertyChanged_15() { return static_cast<int32_t>(offsetof(JObject_t877230766, ___PropertyChanged_15)); }
	inline PropertyChangedEventHandler_t3836340606 * get_PropertyChanged_15() const { return ___PropertyChanged_15; }
	inline PropertyChangedEventHandler_t3836340606 ** get_address_of_PropertyChanged_15() { return &___PropertyChanged_15; }
	inline void set_PropertyChanged_15(PropertyChangedEventHandler_t3836340606 * value)
	{
		___PropertyChanged_15 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanged_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBJECT_T877230766_H
#ifndef JTOKENTYPE_T1759864862_H
#define JTOKENTYPE_T1759864862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JTokenType
struct  JTokenType_t1759864862 
{
public:
	// System.Int32 Mapbox.Json.Linq.JTokenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JTokenType_t1759864862, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENTYPE_T1759864862_H
#ifndef U3CGETENUMERATORU3ED__55_T2619545197_H
#define U3CGETENUMERATORU3ED__55_T2619545197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JObject/<GetEnumerator>d__55
struct  U3CGetEnumeratorU3Ed__55_t2619545197  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.Json.Linq.JObject/<GetEnumerator>d__55::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Mapbox.Json.Linq.JToken> Mapbox.Json.Linq.JObject/<GetEnumerator>d__55::<>2__current
	KeyValuePair_2_t267824477  ___U3CU3E2__current_1;
	// Mapbox.Json.Linq.JObject Mapbox.Json.Linq.JObject/<GetEnumerator>d__55::<>4__this
	JObject_t877230766 * ___U3CU3E4__this_2;
	// System.Collections.Generic.IEnumerator`1<Mapbox.Json.Linq.JToken> Mapbox.Json.Linq.JObject/<GetEnumerator>d__55::<>s__1
	RuntimeObject* ___U3CU3Es__1_3;
	// Mapbox.Json.Linq.JProperty Mapbox.Json.Linq.JObject/<GetEnumerator>d__55::<property>5__2
	JProperty_t1073345108 * ___U3CpropertyU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__55_t2619545197, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__55_t2619545197, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t267824477  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t267824477 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t267824477  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__55_t2619545197, ___U3CU3E4__this_2)); }
	inline JObject_t877230766 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JObject_t877230766 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JObject_t877230766 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__55_t2619545197, ___U3CU3Es__1_3)); }
	inline RuntimeObject* get_U3CU3Es__1_3() const { return ___U3CU3Es__1_3; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_3() { return &___U3CU3Es__1_3; }
	inline void set_U3CU3Es__1_3(RuntimeObject* value)
	{
		___U3CU3Es__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_3), value);
	}

	inline static int32_t get_offset_of_U3CpropertyU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__55_t2619545197, ___U3CpropertyU3E5__2_4)); }
	inline JProperty_t1073345108 * get_U3CpropertyU3E5__2_4() const { return ___U3CpropertyU3E5__2_4; }
	inline JProperty_t1073345108 ** get_address_of_U3CpropertyU3E5__2_4() { return &___U3CpropertyU3E5__2_4; }
	inline void set_U3CpropertyU3E5__2_4(JProperty_t1073345108 * value)
	{
		___U3CpropertyU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpropertyU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__55_T2619545197_H
#ifndef JARRAY_T3513210838_H
#define JARRAY_T3513210838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JArray
struct  JArray_t3513210838  : public JContainer_t3802169109
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.Linq.JToken> Mapbox.Json.Linq.JArray::_values
	List_1_t3851938049 * ____values_14;

public:
	inline static int32_t get_offset_of__values_14() { return static_cast<int32_t>(offsetof(JArray_t3513210838, ____values_14)); }
	inline List_1_t3851938049 * get__values_14() const { return ____values_14; }
	inline List_1_t3851938049 ** get_address_of__values_14() { return &____values_14; }
	inline void set__values_14(List_1_t3851938049 * value)
	{
		____values_14 = value;
		Il2CppCodeGenWriteBarrier((&____values_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JARRAY_T3513210838_H
#ifndef BSONTYPE_T2223064640_H
#define BSONTYPE_T2223064640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonType
struct  BsonType_t2223064640 
{
public:
	// System.SByte Mapbox.Json.Bson.BsonType::value__
	int8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BsonType_t2223064640, ___value___1)); }
	inline int8_t get_value___1() const { return ___value___1; }
	inline int8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTYPE_T2223064640_H
#ifndef XELEMENTWRAPPER_T1376508173_H
#define XELEMENTWRAPPER_T1376508173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XElementWrapper
struct  XElementWrapper_t1376508173  : public XContainerWrapper_t652000426
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.Converters.IXmlNode> Mapbox.Json.Converters.XElementWrapper::_attributes
	List_1_t548158954 * ____attributes_2;

public:
	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XElementWrapper_t1376508173, ____attributes_2)); }
	inline List_1_t548158954 * get__attributes_2() const { return ____attributes_2; }
	inline List_1_t548158954 ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_t548158954 * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTWRAPPER_T1376508173_H
#ifndef BSONBINARYTYPE_T1803869950_H
#define BSONBINARYTYPE_T1803869950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonBinaryType
struct  BsonBinaryType_t1803869950 
{
public:
	// System.Byte Mapbox.Json.Bson.BsonBinaryType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BsonBinaryType_t1803869950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYTYPE_T1803869950_H
#ifndef TEXTCONTAINERANCHORS_T945851193_H
#define TEXTCONTAINERANCHORS_T945851193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainerAnchors
struct  TextContainerAnchors_t945851193 
{
public:
	// System.Int32 TMPro.TextContainerAnchors::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextContainerAnchors_t945851193, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINERANCHORS_T945851193_H
#ifndef XDOCUMENTWRAPPER_T3693494325_H
#define XDOCUMENTWRAPPER_T3693494325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Converters.XDocumentWrapper
struct  XDocumentWrapper_t3693494325  : public XContainerWrapper_t652000426
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTWRAPPER_T3693494325_H
#ifndef NULLABLE_1_T2247472100_H
#define NULLABLE_1_T2247472100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.StringEscapeHandling>
struct  Nullable_1_t2247472100 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2247472100, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2247472100, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2247472100_H
#ifndef NULLABLE_1_T2218815621_H
#define NULLABLE_1_T2218815621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.FloatFormatHandling>
struct  Nullable_1_t2218815621 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2218815621, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2218815621, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2218815621_H
#ifndef JSONCONTRACT_T968551136_H
#define JSONCONTRACT_T968551136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonContract
struct  JsonContract_t968551136  : public RuntimeObject
{
public:
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Mapbox.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Mapbox.Json.ReadType Mapbox.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Mapbox.Json.Serialization.JsonContractType Mapbox.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_t3209604541 * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationCallback> Mapbox.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Mapbox.Json.Serialization.SerializationErrorCallback> Mapbox.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Mapbox.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Mapbox.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Mapbox.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t1819850047  ___U3CIsReferenceU3Ek__BackingField_16;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t472504469 * ___U3CConverterU3Ek__BackingField_17;
	// Mapbox.Json.JsonConverter Mapbox.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t472504469 * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Mapbox.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t2509852811 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Mapbox.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onDeserializedCallbacks_9)); }
	inline List_1_t3209604541 * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_t3209604541 ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_t3209604541 * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t1819850047  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t1819850047 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t1819850047  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t472504469 * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t472504469 ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t472504469 * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t472504469 * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t472504469 ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t472504469 * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t2509852811 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t2509852811 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t2509852811 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_t968551136, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_T968551136_H
#ifndef NULLABLE_1_T4080941573_H
#define NULLABLE_1_T4080941573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.FloatParseHandling>
struct  Nullable_1_t4080941573 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4080941573, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4080941573, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4080941573_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BSONBINARYWRITER_T3219674374_H
#define BSONBINARYWRITER_T3219674374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonBinaryWriter
struct  BsonBinaryWriter_t3219674374  : public RuntimeObject
{
public:
	// System.IO.BinaryWriter Mapbox.Json.Bson.BsonBinaryWriter::_writer
	BinaryWriter_t3992595042 * ____writer_1;
	// System.Byte[] Mapbox.Json.Bson.BsonBinaryWriter::_largeByteBuffer
	ByteU5BU5D_t4116647657* ____largeByteBuffer_2;
	// System.DateTimeKind Mapbox.Json.Bson.BsonBinaryWriter::<DateTimeKindHandling>k__BackingField
	int32_t ___U3CDateTimeKindHandlingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3219674374, ____writer_1)); }
	inline BinaryWriter_t3992595042 * get__writer_1() const { return ____writer_1; }
	inline BinaryWriter_t3992595042 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(BinaryWriter_t3992595042 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__largeByteBuffer_2() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3219674374, ____largeByteBuffer_2)); }
	inline ByteU5BU5D_t4116647657* get__largeByteBuffer_2() const { return ____largeByteBuffer_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__largeByteBuffer_2() { return &____largeByteBuffer_2; }
	inline void set__largeByteBuffer_2(ByteU5BU5D_t4116647657* value)
	{
		____largeByteBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_2), value);
	}

	inline static int32_t get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3219674374, ___U3CDateTimeKindHandlingU3Ek__BackingField_3)); }
	inline int32_t get_U3CDateTimeKindHandlingU3Ek__BackingField_3() const { return ___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return &___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline void set_U3CDateTimeKindHandlingU3Ek__BackingField_3(int32_t value)
	{
		___U3CDateTimeKindHandlingU3Ek__BackingField_3 = value;
	}
};

struct BsonBinaryWriter_t3219674374_StaticFields
{
public:
	// System.Text.Encoding Mapbox.Json.Bson.BsonBinaryWriter::Encoding
	Encoding_t1523322056 * ___Encoding_0;

public:
	inline static int32_t get_offset_of_Encoding_0() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3219674374_StaticFields, ___Encoding_0)); }
	inline Encoding_t1523322056 * get_Encoding_0() const { return ___Encoding_0; }
	inline Encoding_t1523322056 ** get_address_of_Encoding_0() { return &___Encoding_0; }
	inline void set_Encoding_0(Encoding_t1523322056 * value)
	{
		___Encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___Encoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYWRITER_T3219674374_H
#ifndef BSONEMPTY_T3444714005_H
#define BSONEMPTY_T3444714005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonEmpty
struct  BsonEmpty_t3444714005  : public BsonToken_t3974775022
{
public:
	// Mapbox.Json.Bson.BsonType Mapbox.Json.Bson.BsonEmpty::<Type>k__BackingField
	int8_t ___U3CTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonEmpty_t3444714005, ___U3CTypeU3Ek__BackingField_4)); }
	inline int8_t get_U3CTypeU3Ek__BackingField_4() const { return ___U3CTypeU3Ek__BackingField_4; }
	inline int8_t* get_address_of_U3CTypeU3Ek__BackingField_4() { return &___U3CTypeU3Ek__BackingField_4; }
	inline void set_U3CTypeU3Ek__BackingField_4(int8_t value)
	{
		___U3CTypeU3Ek__BackingField_4 = value;
	}
};

struct BsonEmpty_t3444714005_StaticFields
{
public:
	// Mapbox.Json.Bson.BsonToken Mapbox.Json.Bson.BsonEmpty::Null
	BsonToken_t3974775022 * ___Null_2;
	// Mapbox.Json.Bson.BsonToken Mapbox.Json.Bson.BsonEmpty::Undefined
	BsonToken_t3974775022 * ___Undefined_3;

public:
	inline static int32_t get_offset_of_Null_2() { return static_cast<int32_t>(offsetof(BsonEmpty_t3444714005_StaticFields, ___Null_2)); }
	inline BsonToken_t3974775022 * get_Null_2() const { return ___Null_2; }
	inline BsonToken_t3974775022 ** get_address_of_Null_2() { return &___Null_2; }
	inline void set_Null_2(BsonToken_t3974775022 * value)
	{
		___Null_2 = value;
		Il2CppCodeGenWriteBarrier((&___Null_2), value);
	}

	inline static int32_t get_offset_of_Undefined_3() { return static_cast<int32_t>(offsetof(BsonEmpty_t3444714005_StaticFields, ___Undefined_3)); }
	inline BsonToken_t3974775022 * get_Undefined_3() const { return ___Undefined_3; }
	inline BsonToken_t3974775022 ** get_address_of_Undefined_3() { return &___Undefined_3; }
	inline void set_Undefined_3(BsonToken_t3974775022 * value)
	{
		___Undefined_3 = value;
		Il2CppCodeGenWriteBarrier((&___Undefined_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONEMPTY_T3444714005_H
#ifndef BSONVALUE_T1632196520_H
#define BSONVALUE_T1632196520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonValue
struct  BsonValue_t1632196520  : public BsonToken_t3974775022
{
public:
	// System.Object Mapbox.Json.Bson.BsonValue::_value
	RuntimeObject * ____value_2;
	// Mapbox.Json.Bson.BsonType Mapbox.Json.Bson.BsonValue::_type
	int8_t ____type_3;

public:
	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(BsonValue_t1632196520, ____value_2)); }
	inline RuntimeObject * get__value_2() const { return ____value_2; }
	inline RuntimeObject ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(RuntimeObject * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier((&____value_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(BsonValue_t1632196520, ____type_3)); }
	inline int8_t get__type_3() const { return ____type_3; }
	inline int8_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int8_t value)
	{
		____type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONVALUE_T1632196520_H
#ifndef JVALUE_T981543937_H
#define JVALUE_T981543937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JValue
struct  JValue_t981543937  : public JToken_t2379863307
{
public:
	// Mapbox.Json.Linq.JTokenType Mapbox.Json.Linq.JValue::_valueType
	int32_t ____valueType_13;
	// System.Object Mapbox.Json.Linq.JValue::_value
	RuntimeObject * ____value_14;

public:
	inline static int32_t get_offset_of__valueType_13() { return static_cast<int32_t>(offsetof(JValue_t981543937, ____valueType_13)); }
	inline int32_t get__valueType_13() const { return ____valueType_13; }
	inline int32_t* get_address_of__valueType_13() { return &____valueType_13; }
	inline void set__valueType_13(int32_t value)
	{
		____valueType_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(JValue_t981543937, ____value_14)); }
	inline RuntimeObject * get__value_14() const { return ____value_14; }
	inline RuntimeObject ** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(RuntimeObject * value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JVALUE_T981543937_H
#ifndef JSONPOSITION_T841078794_H
#define JSONPOSITION_T841078794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonPosition
struct  JsonPosition_t841078794 
{
public:
	// Mapbox.Json.JsonContainerType Mapbox.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Mapbox.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Mapbox.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Mapbox.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t841078794_StaticFields
{
public:
	// System.Char[] Mapbox.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t3528271667* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t841078794_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t3528271667* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t3528271667** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t3528271667* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mapbox.Json.JsonPosition
struct JsonPosition_t841078794_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Mapbox.Json.JsonPosition
struct JsonPosition_t841078794_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T841078794_H
#ifndef NULLABLE_1_T3897609232_H
#define NULLABLE_1_T3897609232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.DateParseHandling>
struct  Nullable_1_t3897609232 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3897609232, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3897609232, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3897609232_H
#ifndef NULLABLE_1_T1775568733_H
#define NULLABLE_1_T1775568733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct  Nullable_1_t1775568733 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1775568733, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1775568733, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1775568733_H
#ifndef JSONLOADSETTINGS_T1158337977_H
#define JSONLOADSETTINGS_T1158337977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JsonLoadSettings
struct  JsonLoadSettings_t1158337977  : public RuntimeObject
{
public:
	// Mapbox.Json.Linq.CommentHandling Mapbox.Json.Linq.JsonLoadSettings::_commentHandling
	int32_t ____commentHandling_0;
	// Mapbox.Json.Linq.LineInfoHandling Mapbox.Json.Linq.JsonLoadSettings::_lineInfoHandling
	int32_t ____lineInfoHandling_1;

public:
	inline static int32_t get_offset_of__commentHandling_0() { return static_cast<int32_t>(offsetof(JsonLoadSettings_t1158337977, ____commentHandling_0)); }
	inline int32_t get__commentHandling_0() const { return ____commentHandling_0; }
	inline int32_t* get_address_of__commentHandling_0() { return &____commentHandling_0; }
	inline void set__commentHandling_0(int32_t value)
	{
		____commentHandling_0 = value;
	}

	inline static int32_t get_offset_of__lineInfoHandling_1() { return static_cast<int32_t>(offsetof(JsonLoadSettings_t1158337977, ____lineInfoHandling_1)); }
	inline int32_t get__lineInfoHandling_1() const { return ____lineInfoHandling_1; }
	inline int32_t* get_address_of__lineInfoHandling_1() { return &____lineInfoHandling_1; }
	inline void set__lineInfoHandling_1(int32_t value)
	{
		____lineInfoHandling_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLOADSETTINGS_T1158337977_H
#ifndef STREAMINGCONTEXT_T3711869237_H
#define STREAMINGCONTEXT_T3711869237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t3711869237 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T3711869237_H
#ifndef NULLABLE_1_T422739155_H
#define NULLABLE_1_T422739155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.Formatting>
struct  Nullable_1_t422739155 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t422739155, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t422739155, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T422739155_H
#ifndef NULLABLE_1_T1653708969_H
#define NULLABLE_1_T1653708969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.DateFormatHandling>
struct  Nullable_1_t1653708969 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1653708969, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1653708969, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1653708969_H
#ifndef NULLABLE_1_T2887164258_H
#define NULLABLE_1_T2887164258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mapbox.Json.DateTimeZoneHandling>
struct  Nullable_1_t2887164258 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2887164258, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2887164258, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2887164258_H
#ifndef BSONBINARY_T4010727797_H
#define BSONBINARY_T4010727797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonBinary
struct  BsonBinary_t4010727797  : public BsonValue_t1632196520
{
public:
	// Mapbox.Json.Bson.BsonBinaryType Mapbox.Json.Bson.BsonBinary::<BinaryType>k__BackingField
	uint8_t ___U3CBinaryTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBinaryTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonBinary_t4010727797, ___U3CBinaryTypeU3Ek__BackingField_4)); }
	inline uint8_t get_U3CBinaryTypeU3Ek__BackingField_4() const { return ___U3CBinaryTypeU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CBinaryTypeU3Ek__BackingField_4() { return &___U3CBinaryTypeU3Ek__BackingField_4; }
	inline void set_U3CBinaryTypeU3Ek__BackingField_4(uint8_t value)
	{
		___U3CBinaryTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARY_T4010727797_H
#ifndef BSONSTRING_T848819904_H
#define BSONSTRING_T848819904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonString
struct  BsonString_t848819904  : public BsonValue_t1632196520
{
public:
	// System.Int32 Mapbox.Json.Bson.BsonString::<ByteCount>k__BackingField
	int32_t ___U3CByteCountU3Ek__BackingField_4;
	// System.Boolean Mapbox.Json.Bson.BsonString::<IncludeLength>k__BackingField
	bool ___U3CIncludeLengthU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CByteCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonString_t848819904, ___U3CByteCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CByteCountU3Ek__BackingField_4() const { return ___U3CByteCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CByteCountU3Ek__BackingField_4() { return &___U3CByteCountU3Ek__BackingField_4; }
	inline void set_U3CByteCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CByteCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeLengthU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BsonString_t848819904, ___U3CIncludeLengthU3Ek__BackingField_5)); }
	inline bool get_U3CIncludeLengthU3Ek__BackingField_5() const { return ___U3CIncludeLengthU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIncludeLengthU3Ek__BackingField_5() { return &___U3CIncludeLengthU3Ek__BackingField_5; }
	inline void set_U3CIncludeLengthU3Ek__BackingField_5(bool value)
	{
		___U3CIncludeLengthU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONSTRING_T848819904_H
#ifndef BSONBOOLEAN_T654920352_H
#define BSONBOOLEAN_T654920352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonBoolean
struct  BsonBoolean_t654920352  : public BsonValue_t1632196520
{
public:

public:
};

struct BsonBoolean_t654920352_StaticFields
{
public:
	// Mapbox.Json.Bson.BsonBoolean Mapbox.Json.Bson.BsonBoolean::False
	BsonBoolean_t654920352 * ___False_4;
	// Mapbox.Json.Bson.BsonBoolean Mapbox.Json.Bson.BsonBoolean::True
	BsonBoolean_t654920352 * ___True_5;

public:
	inline static int32_t get_offset_of_False_4() { return static_cast<int32_t>(offsetof(BsonBoolean_t654920352_StaticFields, ___False_4)); }
	inline BsonBoolean_t654920352 * get_False_4() const { return ___False_4; }
	inline BsonBoolean_t654920352 ** get_address_of_False_4() { return &___False_4; }
	inline void set_False_4(BsonBoolean_t654920352 * value)
	{
		___False_4 = value;
		Il2CppCodeGenWriteBarrier((&___False_4), value);
	}

	inline static int32_t get_offset_of_True_5() { return static_cast<int32_t>(offsetof(BsonBoolean_t654920352_StaticFields, ___True_5)); }
	inline BsonBoolean_t654920352 * get_True_5() const { return ___True_5; }
	inline BsonBoolean_t654920352 ** get_address_of_True_5() { return &___True_5; }
	inline void set_True_5(BsonBoolean_t654920352 * value)
	{
		___True_5 = value;
		Il2CppCodeGenWriteBarrier((&___True_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBOOLEAN_T654920352_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef JRAW_T2314760112_H
#define JRAW_T2314760112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JRaw
struct  JRaw_t2314760112  : public JValue_t981543937
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JRAW_T2314760112_H
#ifndef JSONREADER_T1879223345_H
#define JSONREADER_T1879223345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonReader
struct  JsonReader_t1879223345  : public RuntimeObject
{
public:
	// Mapbox.Json.JsonToken Mapbox.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Mapbox.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Mapbox.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Mapbox.Json.JsonReader/State Mapbox.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Mapbox.Json.JsonPosition Mapbox.Json.JsonReader::_currentPosition
	JsonPosition_t841078794  ____currentPosition_4;
	// System.Globalization.CultureInfo Mapbox.Json.JsonReader::_culture
	CultureInfo_t4157843068 * ____culture_5;
	// Mapbox.Json.DateTimeZoneHandling Mapbox.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Mapbox.Json.JsonReader::_maxDepth
	Nullable_1_t378540539  ____maxDepth_7;
	// System.Boolean Mapbox.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Mapbox.Json.DateParseHandling Mapbox.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Mapbox.Json.FloatParseHandling Mapbox.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Mapbox.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Mapbox.Json.JsonPosition> Mapbox.Json.JsonReader::_stack
	List_1_t2313153536 * ____stack_12;
	// System.Boolean Mapbox.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Mapbox.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____currentPosition_4)); }
	inline JsonPosition_t841078794  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t841078794 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t841078794  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____culture_5)); }
	inline CultureInfo_t4157843068 * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t4157843068 * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____maxDepth_7)); }
	inline Nullable_1_t378540539  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t378540539  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ____stack_12)); }
	inline List_1_t2313153536 * get__stack_12() const { return ____stack_12; }
	inline List_1_t2313153536 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_t2313153536 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t1879223345, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T1879223345_H
#ifndef JSONWRITER_T260835314_H
#define JSONWRITER_T260835314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonWriter
struct  JsonWriter_t260835314  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Mapbox.Json.JsonPosition> Mapbox.Json.JsonWriter::_stack
	List_1_t2313153536 * ____stack_2;
	// Mapbox.Json.JsonPosition Mapbox.Json.JsonWriter::_currentPosition
	JsonPosition_t841078794  ____currentPosition_3;
	// Mapbox.Json.JsonWriter/State Mapbox.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Mapbox.Json.Formatting Mapbox.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Mapbox.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// System.Boolean Mapbox.Json.JsonWriter::<AutoCompleteOnClose>k__BackingField
	bool ___U3CAutoCompleteOnCloseU3Ek__BackingField_7;
	// Mapbox.Json.DateFormatHandling Mapbox.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_8;
	// Mapbox.Json.DateTimeZoneHandling Mapbox.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_9;
	// Mapbox.Json.StringEscapeHandling Mapbox.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_10;
	// Mapbox.Json.FloatFormatHandling Mapbox.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_11;
	// System.String Mapbox.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_12;
	// System.Globalization.CultureInfo Mapbox.Json.JsonWriter::_culture
	CultureInfo_t4157843068 * ____culture_13;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____stack_2)); }
	inline List_1_t2313153536 * get__stack_2() const { return ____stack_2; }
	inline List_1_t2313153536 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_t2313153536 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____currentPosition_3)); }
	inline JsonPosition_t841078794  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t841078794 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t841078794  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ___U3CAutoCompleteOnCloseU3Ek__BackingField_7)); }
	inline bool get_U3CAutoCompleteOnCloseU3Ek__BackingField_7() const { return ___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return &___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline void set_U3CAutoCompleteOnCloseU3Ek__BackingField_7(bool value)
	{
		___U3CAutoCompleteOnCloseU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____dateFormatHandling_8)); }
	inline int32_t get__dateFormatHandling_8() const { return ____dateFormatHandling_8; }
	inline int32_t* get_address_of__dateFormatHandling_8() { return &____dateFormatHandling_8; }
	inline void set__dateFormatHandling_8(int32_t value)
	{
		____dateFormatHandling_8 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____dateTimeZoneHandling_9)); }
	inline int32_t get__dateTimeZoneHandling_9() const { return ____dateTimeZoneHandling_9; }
	inline int32_t* get_address_of__dateTimeZoneHandling_9() { return &____dateTimeZoneHandling_9; }
	inline void set__dateTimeZoneHandling_9(int32_t value)
	{
		____dateTimeZoneHandling_9 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____stringEscapeHandling_10)); }
	inline int32_t get__stringEscapeHandling_10() const { return ____stringEscapeHandling_10; }
	inline int32_t* get_address_of__stringEscapeHandling_10() { return &____stringEscapeHandling_10; }
	inline void set__stringEscapeHandling_10(int32_t value)
	{
		____stringEscapeHandling_10 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_11() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____floatFormatHandling_11)); }
	inline int32_t get__floatFormatHandling_11() const { return ____floatFormatHandling_11; }
	inline int32_t* get_address_of__floatFormatHandling_11() { return &____floatFormatHandling_11; }
	inline void set__floatFormatHandling_11(int32_t value)
	{
		____floatFormatHandling_11 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_12() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____dateFormatString_12)); }
	inline String_t* get__dateFormatString_12() const { return ____dateFormatString_12; }
	inline String_t** get_address_of__dateFormatString_12() { return &____dateFormatString_12; }
	inline void set__dateFormatString_12(String_t* value)
	{
		____dateFormatString_12 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_12), value);
	}

	inline static int32_t get_offset_of__culture_13() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314, ____culture_13)); }
	inline CultureInfo_t4157843068 * get__culture_13() const { return ____culture_13; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_13() { return &____culture_13; }
	inline void set__culture_13(CultureInfo_t4157843068 * value)
	{
		____culture_13 = value;
		Il2CppCodeGenWriteBarrier((&____culture_13), value);
	}
};

struct JsonWriter_t260835314_StaticFields
{
public:
	// Mapbox.Json.JsonWriter/State[][] Mapbox.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t691804306* ___StateArray_0;
	// Mapbox.Json.JsonWriter/State[][] Mapbox.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t691804306* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t691804306* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t691804306** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t691804306* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t260835314_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t691804306* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t691804306** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t691804306* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T260835314_H
#ifndef CREATORPROPERTYCONTEXT_T2845345356_H
#define CREATORPROPERTYCONTEXT_T2845345356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext
struct  CreatorPropertyContext_t2845345356  : public RuntimeObject
{
public:
	// System.String Mapbox.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Name
	String_t* ___Name_0;
	// Mapbox.Json.Serialization.JsonProperty Mapbox.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Property
	JsonProperty_t1629976260 * ___Property_1;
	// Mapbox.Json.Serialization.JsonProperty Mapbox.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::ConstructorProperty
	JsonProperty_t1629976260 * ___ConstructorProperty_2;
	// System.Nullable`1<Mapbox.Json.Serialization.JsonSerializerInternalReader/PropertyPresence> Mapbox.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Presence
	Nullable_1_t1775568733  ___Presence_3;
	// System.Object Mapbox.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Value
	RuntimeObject * ___Value_4;
	// System.Boolean Mapbox.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Used
	bool ___Used_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t2845345356, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Property_1() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t2845345356, ___Property_1)); }
	inline JsonProperty_t1629976260 * get_Property_1() const { return ___Property_1; }
	inline JsonProperty_t1629976260 ** get_address_of_Property_1() { return &___Property_1; }
	inline void set_Property_1(JsonProperty_t1629976260 * value)
	{
		___Property_1 = value;
		Il2CppCodeGenWriteBarrier((&___Property_1), value);
	}

	inline static int32_t get_offset_of_ConstructorProperty_2() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t2845345356, ___ConstructorProperty_2)); }
	inline JsonProperty_t1629976260 * get_ConstructorProperty_2() const { return ___ConstructorProperty_2; }
	inline JsonProperty_t1629976260 ** get_address_of_ConstructorProperty_2() { return &___ConstructorProperty_2; }
	inline void set_ConstructorProperty_2(JsonProperty_t1629976260 * value)
	{
		___ConstructorProperty_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorProperty_2), value);
	}

	inline static int32_t get_offset_of_Presence_3() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t2845345356, ___Presence_3)); }
	inline Nullable_1_t1775568733  get_Presence_3() const { return ___Presence_3; }
	inline Nullable_1_t1775568733 * get_address_of_Presence_3() { return &___Presence_3; }
	inline void set_Presence_3(Nullable_1_t1775568733  value)
	{
		___Presence_3 = value;
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t2845345356, ___Value_4)); }
	inline RuntimeObject * get_Value_4() const { return ___Value_4; }
	inline RuntimeObject ** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(RuntimeObject * value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_Used_5() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t2845345356, ___Used_5)); }
	inline bool get_Used_5() const { return ___Used_5; }
	inline bool* get_address_of_Used_5() { return &___Used_5; }
	inline void set_Used_5(bool value)
	{
		___Used_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORPROPERTYCONTEXT_T2845345356_H
#ifndef JSONSERIALIZER_T2280177768_H
#define JSONSERIALIZER_T2280177768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.JsonSerializer
struct  JsonSerializer_t2280177768  : public RuntimeObject
{
public:
	// Mapbox.Json.TypeNameHandling Mapbox.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// Mapbox.Json.TypeNameAssemblyFormatHandling Mapbox.Json.JsonSerializer::_typeNameAssemblyFormatHandling
	int32_t ____typeNameAssemblyFormatHandling_1;
	// Mapbox.Json.PreserveReferencesHandling Mapbox.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Mapbox.Json.ReferenceLoopHandling Mapbox.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Mapbox.Json.MissingMemberHandling Mapbox.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Mapbox.Json.ObjectCreationHandling Mapbox.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Mapbox.Json.NullValueHandling Mapbox.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Mapbox.Json.DefaultValueHandling Mapbox.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Mapbox.Json.ConstructorHandling Mapbox.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Mapbox.Json.MetadataPropertyHandling Mapbox.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Mapbox.Json.JsonConverterCollection Mapbox.Json.JsonSerializer::_converters
	JsonConverterCollection_t2916355996 * ____converters_10;
	// Mapbox.Json.Serialization.IContractResolver Mapbox.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Mapbox.Json.Serialization.ITraceWriter Mapbox.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Mapbox.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// Mapbox.Json.Serialization.ISerializationBinder Mapbox.Json.JsonSerializer::_serializationBinder
	RuntimeObject* ____serializationBinder_14;
	// System.Runtime.Serialization.StreamingContext Mapbox.Json.JsonSerializer::_context
	StreamingContext_t3711869237  ____context_15;
	// Mapbox.Json.Serialization.IReferenceResolver Mapbox.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Mapbox.Json.Formatting> Mapbox.Json.JsonSerializer::_formatting
	Nullable_1_t422739155  ____formatting_17;
	// System.Nullable`1<Mapbox.Json.DateFormatHandling> Mapbox.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_t1653708969  ____dateFormatHandling_18;
	// System.Nullable`1<Mapbox.Json.DateTimeZoneHandling> Mapbox.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_t2887164258  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Mapbox.Json.DateParseHandling> Mapbox.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t3897609232  ____dateParseHandling_20;
	// System.Nullable`1<Mapbox.Json.FloatFormatHandling> Mapbox.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t2218815621  ____floatFormatHandling_21;
	// System.Nullable`1<Mapbox.Json.FloatParseHandling> Mapbox.Json.JsonSerializer::_floatParseHandling
	Nullable_1_t4080941573  ____floatParseHandling_22;
	// System.Nullable`1<Mapbox.Json.StringEscapeHandling> Mapbox.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_t2247472100  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Mapbox.Json.JsonSerializer::_culture
	CultureInfo_t4157843068 * ____culture_24;
	// System.Nullable`1<System.Int32> Mapbox.Json.JsonSerializer::_maxDepth
	Nullable_1_t378540539  ____maxDepth_25;
	// System.Boolean Mapbox.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Mapbox.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t1819850047  ____checkAdditionalContent_27;
	// System.String Mapbox.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Mapbox.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Mapbox.Json.Serialization.ErrorEventArgs> Mapbox.Json.JsonSerializer::Error
	EventHandler_1_t44176832 * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormatHandling_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____typeNameAssemblyFormatHandling_1)); }
	inline int32_t get__typeNameAssemblyFormatHandling_1() const { return ____typeNameAssemblyFormatHandling_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormatHandling_1() { return &____typeNameAssemblyFormatHandling_1; }
	inline void set__typeNameAssemblyFormatHandling_1(int32_t value)
	{
		____typeNameAssemblyFormatHandling_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____converters_10)); }
	inline JsonConverterCollection_t2916355996 * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_t2916355996 ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_t2916355996 * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__serializationBinder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____serializationBinder_14)); }
	inline RuntimeObject* get__serializationBinder_14() const { return ____serializationBinder_14; }
	inline RuntimeObject** get_address_of__serializationBinder_14() { return &____serializationBinder_14; }
	inline void set__serializationBinder_14(RuntimeObject* value)
	{
		____serializationBinder_14 = value;
		Il2CppCodeGenWriteBarrier((&____serializationBinder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____context_15)); }
	inline StreamingContext_t3711869237  get__context_15() const { return ____context_15; }
	inline StreamingContext_t3711869237 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t3711869237  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____formatting_17)); }
	inline Nullable_1_t422739155  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t422739155 * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t422739155  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____dateFormatHandling_18)); }
	inline Nullable_1_t1653708969  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_t1653708969 * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_t1653708969  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_t2887164258  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_t2887164258 * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_t2887164258  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____dateParseHandling_20)); }
	inline Nullable_1_t3897609232  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t3897609232 * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t3897609232  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____floatFormatHandling_21)); }
	inline Nullable_1_t2218815621  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t2218815621 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t2218815621  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____floatParseHandling_22)); }
	inline Nullable_1_t4080941573  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_t4080941573 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_t4080941573  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____stringEscapeHandling_23)); }
	inline Nullable_1_t2247472100  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_t2247472100 * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_t2247472100  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____culture_24)); }
	inline CultureInfo_t4157843068 * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t4157843068 * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____maxDepth_25)); }
	inline Nullable_1_t378540539  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t378540539  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____checkAdditionalContent_27)); }
	inline Nullable_1_t1819850047  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t1819850047 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t1819850047  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t2280177768, ___Error_30)); }
	inline EventHandler_1_t44176832 * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_t44176832 ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_t44176832 * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T2280177768_H
#ifndef JSONPRIMITIVECONTRACT_T959031370_H
#define JSONPRIMITIVECONTRACT_T959031370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonPrimitiveContract
struct  JsonPrimitiveContract_t959031370  : public JsonContract_t968551136
{
public:
	// Mapbox.Json.Utilities.PrimitiveTypeCode Mapbox.Json.Serialization.JsonPrimitiveContract::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t959031370, ___U3CTypeCodeU3Ek__BackingField_21)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_21() const { return ___U3CTypeCodeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_21() { return &___U3CTypeCodeU3Ek__BackingField_21; }
	inline void set_U3CTypeCodeU3Ek__BackingField_21(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_21 = value;
	}
};

struct JsonPrimitiveContract_t959031370_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Mapbox.Json.ReadType> Mapbox.Json.Serialization.JsonPrimitiveContract::ReadTypeMap
	Dictionary_2_t2631974886 * ___ReadTypeMap_22;

public:
	inline static int32_t get_offset_of_ReadTypeMap_22() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t959031370_StaticFields, ___ReadTypeMap_22)); }
	inline Dictionary_2_t2631974886 * get_ReadTypeMap_22() const { return ___ReadTypeMap_22; }
	inline Dictionary_2_t2631974886 ** get_address_of_ReadTypeMap_22() { return &___ReadTypeMap_22; }
	inline void set_ReadTypeMap_22(Dictionary_2_t2631974886 * value)
	{
		___ReadTypeMap_22 = value;
		Il2CppCodeGenWriteBarrier((&___ReadTypeMap_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPRIMITIVECONTRACT_T959031370_H
#ifndef JSONSERIALIZERPROXY_T2669598769_H
#define JSONSERIALIZERPROXY_T2669598769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonSerializerProxy
struct  JsonSerializerProxy_t2669598769  : public JsonSerializer_t2280177768
{
public:
	// Mapbox.Json.Serialization.JsonSerializerInternalReader Mapbox.Json.Serialization.JsonSerializerProxy::_serializerReader
	JsonSerializerInternalReader_t3284513685 * ____serializerReader_31;
	// Mapbox.Json.Serialization.JsonSerializerInternalWriter Mapbox.Json.Serialization.JsonSerializerProxy::_serializerWriter
	JsonSerializerInternalWriter_t2365347854 * ____serializerWriter_32;
	// Mapbox.Json.JsonSerializer Mapbox.Json.Serialization.JsonSerializerProxy::_serializer
	JsonSerializer_t2280177768 * ____serializer_33;

public:
	inline static int32_t get_offset_of__serializerReader_31() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t2669598769, ____serializerReader_31)); }
	inline JsonSerializerInternalReader_t3284513685 * get__serializerReader_31() const { return ____serializerReader_31; }
	inline JsonSerializerInternalReader_t3284513685 ** get_address_of__serializerReader_31() { return &____serializerReader_31; }
	inline void set__serializerReader_31(JsonSerializerInternalReader_t3284513685 * value)
	{
		____serializerReader_31 = value;
		Il2CppCodeGenWriteBarrier((&____serializerReader_31), value);
	}

	inline static int32_t get_offset_of__serializerWriter_32() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t2669598769, ____serializerWriter_32)); }
	inline JsonSerializerInternalWriter_t2365347854 * get__serializerWriter_32() const { return ____serializerWriter_32; }
	inline JsonSerializerInternalWriter_t2365347854 ** get_address_of__serializerWriter_32() { return &____serializerWriter_32; }
	inline void set__serializerWriter_32(JsonSerializerInternalWriter_t2365347854 * value)
	{
		____serializerWriter_32 = value;
		Il2CppCodeGenWriteBarrier((&____serializerWriter_32), value);
	}

	inline static int32_t get_offset_of__serializer_33() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t2669598769, ____serializer_33)); }
	inline JsonSerializer_t2280177768 * get__serializer_33() const { return ____serializer_33; }
	inline JsonSerializer_t2280177768 ** get_address_of__serializer_33() { return &____serializer_33; }
	inline void set__serializer_33(JsonSerializer_t2280177768 * value)
	{
		____serializer_33 = value;
		Il2CppCodeGenWriteBarrier((&____serializer_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERPROXY_T2669598769_H
#ifndef JTOKENWRITER_T939300635_H
#define JTOKENWRITER_T939300635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JTokenWriter
struct  JTokenWriter_t939300635  : public JsonWriter_t260835314
{
public:
	// Mapbox.Json.Linq.JContainer Mapbox.Json.Linq.JTokenWriter::_token
	JContainer_t3802169109 * ____token_14;
	// Mapbox.Json.Linq.JContainer Mapbox.Json.Linq.JTokenWriter::_parent
	JContainer_t3802169109 * ____parent_15;
	// Mapbox.Json.Linq.JValue Mapbox.Json.Linq.JTokenWriter::_value
	JValue_t981543937 * ____value_16;
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JTokenWriter::_current
	JToken_t2379863307 * ____current_17;

public:
	inline static int32_t get_offset_of__token_14() { return static_cast<int32_t>(offsetof(JTokenWriter_t939300635, ____token_14)); }
	inline JContainer_t3802169109 * get__token_14() const { return ____token_14; }
	inline JContainer_t3802169109 ** get_address_of__token_14() { return &____token_14; }
	inline void set__token_14(JContainer_t3802169109 * value)
	{
		____token_14 = value;
		Il2CppCodeGenWriteBarrier((&____token_14), value);
	}

	inline static int32_t get_offset_of__parent_15() { return static_cast<int32_t>(offsetof(JTokenWriter_t939300635, ____parent_15)); }
	inline JContainer_t3802169109 * get__parent_15() const { return ____parent_15; }
	inline JContainer_t3802169109 ** get_address_of__parent_15() { return &____parent_15; }
	inline void set__parent_15(JContainer_t3802169109 * value)
	{
		____parent_15 = value;
		Il2CppCodeGenWriteBarrier((&____parent_15), value);
	}

	inline static int32_t get_offset_of__value_16() { return static_cast<int32_t>(offsetof(JTokenWriter_t939300635, ____value_16)); }
	inline JValue_t981543937 * get__value_16() const { return ____value_16; }
	inline JValue_t981543937 ** get_address_of__value_16() { return &____value_16; }
	inline void set__value_16(JValue_t981543937 * value)
	{
		____value_16 = value;
		Il2CppCodeGenWriteBarrier((&____value_16), value);
	}

	inline static int32_t get_offset_of__current_17() { return static_cast<int32_t>(offsetof(JTokenWriter_t939300635, ____current_17)); }
	inline JToken_t2379863307 * get__current_17() const { return ____current_17; }
	inline JToken_t2379863307 ** get_address_of__current_17() { return &____current_17; }
	inline void set__current_17(JToken_t2379863307 * value)
	{
		____current_17 = value;
		Il2CppCodeGenWriteBarrier((&____current_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENWRITER_T939300635_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef JTOKENREADER_T468116380_H
#define JTOKENREADER_T468116380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Linq.JTokenReader
struct  JTokenReader_t468116380  : public JsonReader_t1879223345
{
public:
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JTokenReader::_root
	JToken_t2379863307 * ____root_15;
	// System.String Mapbox.Json.Linq.JTokenReader::_initialPath
	String_t* ____initialPath_16;
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JTokenReader::_parent
	JToken_t2379863307 * ____parent_17;
	// Mapbox.Json.Linq.JToken Mapbox.Json.Linq.JTokenReader::_current
	JToken_t2379863307 * ____current_18;

public:
	inline static int32_t get_offset_of__root_15() { return static_cast<int32_t>(offsetof(JTokenReader_t468116380, ____root_15)); }
	inline JToken_t2379863307 * get__root_15() const { return ____root_15; }
	inline JToken_t2379863307 ** get_address_of__root_15() { return &____root_15; }
	inline void set__root_15(JToken_t2379863307 * value)
	{
		____root_15 = value;
		Il2CppCodeGenWriteBarrier((&____root_15), value);
	}

	inline static int32_t get_offset_of__initialPath_16() { return static_cast<int32_t>(offsetof(JTokenReader_t468116380, ____initialPath_16)); }
	inline String_t* get__initialPath_16() const { return ____initialPath_16; }
	inline String_t** get_address_of__initialPath_16() { return &____initialPath_16; }
	inline void set__initialPath_16(String_t* value)
	{
		____initialPath_16 = value;
		Il2CppCodeGenWriteBarrier((&____initialPath_16), value);
	}

	inline static int32_t get_offset_of__parent_17() { return static_cast<int32_t>(offsetof(JTokenReader_t468116380, ____parent_17)); }
	inline JToken_t2379863307 * get__parent_17() const { return ____parent_17; }
	inline JToken_t2379863307 ** get_address_of__parent_17() { return &____parent_17; }
	inline void set__parent_17(JToken_t2379863307 * value)
	{
		____parent_17 = value;
		Il2CppCodeGenWriteBarrier((&____parent_17), value);
	}

	inline static int32_t get_offset_of__current_18() { return static_cast<int32_t>(offsetof(JTokenReader_t468116380, ____current_18)); }
	inline JToken_t2379863307 * get__current_18() const { return ____current_18; }
	inline JToken_t2379863307 ** get_address_of__current_18() { return &____current_18; }
	inline void set__current_18(JToken_t2379863307 * value)
	{
		____current_18 = value;
		Il2CppCodeGenWriteBarrier((&____current_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENREADER_T468116380_H
#ifndef BSONWRITER_T3981304838_H
#define BSONWRITER_T3981304838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Bson.BsonWriter
struct  BsonWriter_t3981304838  : public JsonWriter_t260835314
{
public:
	// Mapbox.Json.Bson.BsonBinaryWriter Mapbox.Json.Bson.BsonWriter::_writer
	BsonBinaryWriter_t3219674374 * ____writer_14;
	// Mapbox.Json.Bson.BsonToken Mapbox.Json.Bson.BsonWriter::_root
	BsonToken_t3974775022 * ____root_15;
	// Mapbox.Json.Bson.BsonToken Mapbox.Json.Bson.BsonWriter::_parent
	BsonToken_t3974775022 * ____parent_16;
	// System.String Mapbox.Json.Bson.BsonWriter::_propertyName
	String_t* ____propertyName_17;

public:
	inline static int32_t get_offset_of__writer_14() { return static_cast<int32_t>(offsetof(BsonWriter_t3981304838, ____writer_14)); }
	inline BsonBinaryWriter_t3219674374 * get__writer_14() const { return ____writer_14; }
	inline BsonBinaryWriter_t3219674374 ** get_address_of__writer_14() { return &____writer_14; }
	inline void set__writer_14(BsonBinaryWriter_t3219674374 * value)
	{
		____writer_14 = value;
		Il2CppCodeGenWriteBarrier((&____writer_14), value);
	}

	inline static int32_t get_offset_of__root_15() { return static_cast<int32_t>(offsetof(BsonWriter_t3981304838, ____root_15)); }
	inline BsonToken_t3974775022 * get__root_15() const { return ____root_15; }
	inline BsonToken_t3974775022 ** get_address_of__root_15() { return &____root_15; }
	inline void set__root_15(BsonToken_t3974775022 * value)
	{
		____root_15 = value;
		Il2CppCodeGenWriteBarrier((&____root_15), value);
	}

	inline static int32_t get_offset_of__parent_16() { return static_cast<int32_t>(offsetof(BsonWriter_t3981304838, ____parent_16)); }
	inline BsonToken_t3974775022 * get__parent_16() const { return ____parent_16; }
	inline BsonToken_t3974775022 ** get_address_of__parent_16() { return &____parent_16; }
	inline void set__parent_16(BsonToken_t3974775022 * value)
	{
		____parent_16 = value;
		Il2CppCodeGenWriteBarrier((&____parent_16), value);
	}

	inline static int32_t get_offset_of__propertyName_17() { return static_cast<int32_t>(offsetof(BsonWriter_t3981304838, ____propertyName_17)); }
	inline String_t* get__propertyName_17() const { return ____propertyName_17; }
	inline String_t** get_address_of__propertyName_17() { return &____propertyName_17; }
	inline void set__propertyName_17(String_t* value)
	{
		____propertyName_17 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONWRITER_T3981304838_H
#ifndef JSONSTRINGCONTRACT_T2445709326_H
#define JSONSTRINGCONTRACT_T2445709326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Json.Serialization.JsonStringContract
struct  JsonStringContract_t2445709326  : public JsonPrimitiveContract_t959031370
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTRINGCONTRACT_T2445709326_H
#ifndef INLINEGRAPHICMANAGER_T2871008645_H
#define INLINEGRAPHICMANAGER_T2871008645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager
struct  InlineGraphicManager_t2871008645  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_SpriteAsset TMPro.InlineGraphicManager::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_2;
	// TMPro.InlineGraphic TMPro.InlineGraphicManager::m_inlineGraphic
	InlineGraphic_t2901727699 * ___m_inlineGraphic_3;
	// UnityEngine.CanvasRenderer TMPro.InlineGraphicManager::m_inlineGraphicCanvasRenderer
	CanvasRenderer_t2598313366 * ___m_inlineGraphicCanvasRenderer_4;
	// UnityEngine.UIVertex[] TMPro.InlineGraphicManager::m_uiVertex
	UIVertexU5BU5D_t1981460040* ___m_uiVertex_5;
	// UnityEngine.RectTransform TMPro.InlineGraphicManager::m_inlineGraphicRectTransform
	RectTransform_t3704657025 * ___m_inlineGraphicRectTransform_6;
	// TMPro.TMP_Text TMPro.InlineGraphicManager::m_textComponent
	TMP_Text_t2599618874 * ___m_textComponent_7;
	// System.Boolean TMPro.InlineGraphicManager::m_isInitialized
	bool ___m_isInitialized_8;

public:
	inline static int32_t get_offset_of_m_spriteAsset_2() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_2() const { return ___m_spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_2() { return &___m_spriteAsset_2; }
	inline void set_m_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_m_inlineGraphic_3() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphic_3)); }
	inline InlineGraphic_t2901727699 * get_m_inlineGraphic_3() const { return ___m_inlineGraphic_3; }
	inline InlineGraphic_t2901727699 ** get_address_of_m_inlineGraphic_3() { return &___m_inlineGraphic_3; }
	inline void set_m_inlineGraphic_3(InlineGraphic_t2901727699 * value)
	{
		___m_inlineGraphic_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_inlineGraphic_3), value);
	}

	inline static int32_t get_offset_of_m_inlineGraphicCanvasRenderer_4() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphicCanvasRenderer_4)); }
	inline CanvasRenderer_t2598313366 * get_m_inlineGraphicCanvasRenderer_4() const { return ___m_inlineGraphicCanvasRenderer_4; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_inlineGraphicCanvasRenderer_4() { return &___m_inlineGraphicCanvasRenderer_4; }
	inline void set_m_inlineGraphicCanvasRenderer_4(CanvasRenderer_t2598313366 * value)
	{
		___m_inlineGraphicCanvasRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_inlineGraphicCanvasRenderer_4), value);
	}

	inline static int32_t get_offset_of_m_uiVertex_5() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_uiVertex_5)); }
	inline UIVertexU5BU5D_t1981460040* get_m_uiVertex_5() const { return ___m_uiVertex_5; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_uiVertex_5() { return &___m_uiVertex_5; }
	inline void set_m_uiVertex_5(UIVertexU5BU5D_t1981460040* value)
	{
		___m_uiVertex_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_uiVertex_5), value);
	}

	inline static int32_t get_offset_of_m_inlineGraphicRectTransform_6() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphicRectTransform_6)); }
	inline RectTransform_t3704657025 * get_m_inlineGraphicRectTransform_6() const { return ___m_inlineGraphicRectTransform_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_inlineGraphicRectTransform_6() { return &___m_inlineGraphicRectTransform_6; }
	inline void set_m_inlineGraphicRectTransform_6(RectTransform_t3704657025 * value)
	{
		___m_inlineGraphicRectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_inlineGraphicRectTransform_6), value);
	}

	inline static int32_t get_offset_of_m_textComponent_7() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_textComponent_7)); }
	inline TMP_Text_t2599618874 * get_m_textComponent_7() const { return ___m_textComponent_7; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textComponent_7() { return &___m_textComponent_7; }
	inline void set_m_textComponent_7(TMP_Text_t2599618874 * value)
	{
		___m_textComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textComponent_7), value);
	}

	inline static int32_t get_offset_of_m_isInitialized_8() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_isInitialized_8)); }
	inline bool get_m_isInitialized_8() const { return ___m_isInitialized_8; }
	inline bool* get_address_of_m_isInitialized_8() { return &___m_isInitialized_8; }
	inline void set_m_isInitialized_8(bool value)
	{
		___m_isInitialized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEGRAPHICMANAGER_T2871008645_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TEXTCONTAINER_T97923372_H
#define TEXTCONTAINER_T97923372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainer
struct  TextContainer_t97923372  : public UIBehaviour_t3495933518
{
public:
	// System.Boolean TMPro.TextContainer::m_hasChanged
	bool ___m_hasChanged_2;
	// UnityEngine.Vector2 TMPro.TextContainer::m_pivot
	Vector2_t2156229523  ___m_pivot_3;
	// TMPro.TextContainerAnchors TMPro.TextContainer::m_anchorPosition
	int32_t ___m_anchorPosition_4;
	// UnityEngine.Rect TMPro.TextContainer::m_rect
	Rect_t2360479859  ___m_rect_5;
	// System.Boolean TMPro.TextContainer::m_isDefaultWidth
	bool ___m_isDefaultWidth_6;
	// System.Boolean TMPro.TextContainer::m_isDefaultHeight
	bool ___m_isDefaultHeight_7;
	// System.Boolean TMPro.TextContainer::m_isAutoFitting
	bool ___m_isAutoFitting_8;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_corners
	Vector3U5BU5D_t1718750761* ___m_corners_9;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_worldCorners
	Vector3U5BU5D_t1718750761* ___m_worldCorners_10;
	// UnityEngine.Vector4 TMPro.TextContainer::m_margins
	Vector4_t3319028937  ___m_margins_11;
	// UnityEngine.RectTransform TMPro.TextContainer::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_12;
	// TMPro.TextMeshPro TMPro.TextContainer::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_14;

public:
	inline static int32_t get_offset_of_m_hasChanged_2() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_hasChanged_2)); }
	inline bool get_m_hasChanged_2() const { return ___m_hasChanged_2; }
	inline bool* get_address_of_m_hasChanged_2() { return &___m_hasChanged_2; }
	inline void set_m_hasChanged_2(bool value)
	{
		___m_hasChanged_2 = value;
	}

	inline static int32_t get_offset_of_m_pivot_3() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_pivot_3)); }
	inline Vector2_t2156229523  get_m_pivot_3() const { return ___m_pivot_3; }
	inline Vector2_t2156229523 * get_address_of_m_pivot_3() { return &___m_pivot_3; }
	inline void set_m_pivot_3(Vector2_t2156229523  value)
	{
		___m_pivot_3 = value;
	}

	inline static int32_t get_offset_of_m_anchorPosition_4() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_anchorPosition_4)); }
	inline int32_t get_m_anchorPosition_4() const { return ___m_anchorPosition_4; }
	inline int32_t* get_address_of_m_anchorPosition_4() { return &___m_anchorPosition_4; }
	inline void set_m_anchorPosition_4(int32_t value)
	{
		___m_anchorPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_rect_5() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_rect_5)); }
	inline Rect_t2360479859  get_m_rect_5() const { return ___m_rect_5; }
	inline Rect_t2360479859 * get_address_of_m_rect_5() { return &___m_rect_5; }
	inline void set_m_rect_5(Rect_t2360479859  value)
	{
		___m_rect_5 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultWidth_6() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_isDefaultWidth_6)); }
	inline bool get_m_isDefaultWidth_6() const { return ___m_isDefaultWidth_6; }
	inline bool* get_address_of_m_isDefaultWidth_6() { return &___m_isDefaultWidth_6; }
	inline void set_m_isDefaultWidth_6(bool value)
	{
		___m_isDefaultWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultHeight_7() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_isDefaultHeight_7)); }
	inline bool get_m_isDefaultHeight_7() const { return ___m_isDefaultHeight_7; }
	inline bool* get_address_of_m_isDefaultHeight_7() { return &___m_isDefaultHeight_7; }
	inline void set_m_isDefaultHeight_7(bool value)
	{
		___m_isDefaultHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_isAutoFitting_8() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_isAutoFitting_8)); }
	inline bool get_m_isAutoFitting_8() const { return ___m_isAutoFitting_8; }
	inline bool* get_address_of_m_isAutoFitting_8() { return &___m_isAutoFitting_8; }
	inline void set_m_isAutoFitting_8(bool value)
	{
		___m_isAutoFitting_8 = value;
	}

	inline static int32_t get_offset_of_m_corners_9() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_corners_9)); }
	inline Vector3U5BU5D_t1718750761* get_m_corners_9() const { return ___m_corners_9; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_corners_9() { return &___m_corners_9; }
	inline void set_m_corners_9(Vector3U5BU5D_t1718750761* value)
	{
		___m_corners_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_corners_9), value);
	}

	inline static int32_t get_offset_of_m_worldCorners_10() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_worldCorners_10)); }
	inline Vector3U5BU5D_t1718750761* get_m_worldCorners_10() const { return ___m_worldCorners_10; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_worldCorners_10() { return &___m_worldCorners_10; }
	inline void set_m_worldCorners_10(Vector3U5BU5D_t1718750761* value)
	{
		___m_worldCorners_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_worldCorners_10), value);
	}

	inline static int32_t get_offset_of_m_margins_11() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_margins_11)); }
	inline Vector4_t3319028937  get_m_margins_11() const { return ___m_margins_11; }
	inline Vector4_t3319028937 * get_address_of_m_margins_11() { return &___m_margins_11; }
	inline void set_m_margins_11(Vector4_t3319028937  value)
	{
		___m_margins_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_rectTransform_12)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_14() { return static_cast<int32_t>(offsetof(TextContainer_t97923372, ___m_textMeshPro_14)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_14() const { return ___m_textMeshPro_14; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_14() { return &___m_textMeshPro_14; }
	inline void set_m_textMeshPro_14(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_14), value);
	}
};

struct TextContainer_t97923372_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TextContainer::k_defaultSize
	Vector2_t2156229523  ___k_defaultSize_13;

public:
	inline static int32_t get_offset_of_k_defaultSize_13() { return static_cast<int32_t>(offsetof(TextContainer_t97923372_StaticFields, ___k_defaultSize_13)); }
	inline Vector2_t2156229523  get_k_defaultSize_13() const { return ___k_defaultSize_13; }
	inline Vector2_t2156229523 * get_address_of_k_defaultSize_13() { return &___k_defaultSize_13; }
	inline void set_k_defaultSize_13(Vector2_t2156229523  value)
	{
		___k_defaultSize_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINER_T97923372_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef INLINEGRAPHIC_T2901727699_H
#define INLINEGRAPHIC_T2901727699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphic
struct  InlineGraphic_t2901727699  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Texture TMPro.InlineGraphic::texture
	Texture_t3661962703 * ___texture_28;
	// TMPro.InlineGraphicManager TMPro.InlineGraphic::m_manager
	InlineGraphicManager_t2871008645 * ___m_manager_29;
	// UnityEngine.RectTransform TMPro.InlineGraphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_30;
	// UnityEngine.RectTransform TMPro.InlineGraphic::m_ParentRectTransform
	RectTransform_t3704657025 * ___m_ParentRectTransform_31;

public:
	inline static int32_t get_offset_of_texture_28() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___texture_28)); }
	inline Texture_t3661962703 * get_texture_28() const { return ___texture_28; }
	inline Texture_t3661962703 ** get_address_of_texture_28() { return &___texture_28; }
	inline void set_texture_28(Texture_t3661962703 * value)
	{
		___texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___texture_28), value);
	}

	inline static int32_t get_offset_of_m_manager_29() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___m_manager_29)); }
	inline InlineGraphicManager_t2871008645 * get_m_manager_29() const { return ___m_manager_29; }
	inline InlineGraphicManager_t2871008645 ** get_address_of_m_manager_29() { return &___m_manager_29; }
	inline void set_m_manager_29(InlineGraphicManager_t2871008645 * value)
	{
		___m_manager_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_manager_29), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_30() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___m_RectTransform_30)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_30() const { return ___m_RectTransform_30; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_30() { return &___m_RectTransform_30; }
	inline void set_m_RectTransform_30(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_30), value);
	}

	inline static int32_t get_offset_of_m_ParentRectTransform_31() { return static_cast<int32_t>(offsetof(InlineGraphic_t2901727699, ___m_ParentRectTransform_31)); }
	inline RectTransform_t3704657025 * get_m_ParentRectTransform_31() const { return ___m_ParentRectTransform_31; }
	inline RectTransform_t3704657025 ** get_address_of_m_ParentRectTransform_31() { return &___m_ParentRectTransform_31; }
	inline void set_m_ParentRectTransform_31(RectTransform_t3704657025 * value)
	{
		___m_ParentRectTransform_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentRectTransform_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEGRAPHIC_T2901727699_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (CreatorPropertyContext_t2845345356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[6] = 
{
	CreatorPropertyContext_t2845345356::get_offset_of_Name_0(),
	CreatorPropertyContext_t2845345356::get_offset_of_Property_1(),
	CreatorPropertyContext_t2845345356::get_offset_of_ConstructorProperty_2(),
	CreatorPropertyContext_t2845345356::get_offset_of_Presence_3(),
	CreatorPropertyContext_t2845345356::get_offset_of_Value_4(),
	CreatorPropertyContext_t2845345356::get_offset_of_Used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (U3CU3Ec__DisplayClass36_0_t1058909527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[1] = 
{
	U3CU3Ec__DisplayClass36_0_t1058909527::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (U3CU3Ec_t1267612383), -1, sizeof(U3CU3Ec_t1267612383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2802[5] = 
{
	U3CU3Ec_t1267612383_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1267612383_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t1267612383_StaticFields::get_offset_of_U3CU3E9__36_2_2(),
	U3CU3Ec_t1267612383_StaticFields::get_offset_of_U3CU3E9__41_0_3(),
	U3CU3Ec_t1267612383_StaticFields::get_offset_of_U3CU3E9__41_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (JsonSerializerInternalWriter_t2365347854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[3] = 
{
	JsonSerializerInternalWriter_t2365347854::get_offset_of__rootType_5(),
	JsonSerializerInternalWriter_t2365347854::get_offset_of__rootLevel_6(),
	JsonSerializerInternalWriter_t2365347854::get_offset_of__serializeStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (JsonSerializerProxy_t2669598769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[3] = 
{
	JsonSerializerProxy_t2669598769::get_offset_of__serializerReader_31(),
	JsonSerializerProxy_t2669598769::get_offset_of__serializerWriter_32(),
	JsonSerializerProxy_t2669598769::get_offset_of__serializer_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (JsonStringContract_t2445709326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (JsonTypeReflector_t2360025382), -1, sizeof(JsonTypeReflector_t2360025382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2806[4] = 
{
	JsonTypeReflector_t2360025382_StaticFields::get_offset_of__fullyTrusted_0(),
	JsonTypeReflector_t2360025382_StaticFields::get_offset_of_CreatorCache_1(),
	JsonTypeReflector_t2360025382_StaticFields::get_offset_of_AssociatedMetadataTypesCache_2(),
	JsonTypeReflector_t2360025382_StaticFields::get_offset_of__metadataTypeAttributeReflectionObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (U3CU3Ec__DisplayClass21_0_t596474585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[2] = 
{
	U3CU3Ec__DisplayClass21_0_t596474585::get_offset_of_type_0(),
	U3CU3Ec__DisplayClass21_0_t596474585::get_offset_of_defaultConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (U3CU3Ec_t1312267609), -1, sizeof(U3CU3Ec_t1312267609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2808[2] = 
{
	U3CU3Ec_t1312267609_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1312267609_StaticFields::get_offset_of_U3CU3E9__21_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (ReflectionValueProvider_t1842305502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[1] = 
{
	ReflectionValueProvider_t1842305502::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (OnErrorAttribute_t2846995309), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (CommentHandling_t1085241148)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2813[3] = 
{
	CommentHandling_t1085241148::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (LineInfoHandling_t1348595058)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2814[3] = 
{
	LineInfoHandling_t1348595058::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (JPropertyKeyedCollection_t3049044894), -1, sizeof(JPropertyKeyedCollection_t3049044894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2815[2] = 
{
	JPropertyKeyedCollection_t3049044894_StaticFields::get_offset_of_Comparer_2(),
	JPropertyKeyedCollection_t3049044894::get_offset_of__dictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (JsonLoadSettings_t1158337977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[2] = 
{
	JsonLoadSettings_t1158337977::get_offset_of__commentHandling_0(),
	JsonLoadSettings_t1158337977::get_offset_of__lineInfoHandling_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (JRaw_t2314760112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (JConstructor_t3420863255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[2] = 
{
	JConstructor_t3420863255::get_offset_of__name_14(),
	JConstructor_t3420863255::get_offset_of__values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (JContainer_t3802169109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[1] = 
{
	JContainer_t3802169109::get_offset_of__syncRoot_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (JObject_t877230766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[2] = 
{
	JObject_t877230766::get_offset_of__properties_14(),
	JObject_t877230766::get_offset_of_PropertyChanged_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (U3CGetEnumeratorU3Ed__55_t2619545197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[5] = 
{
	U3CGetEnumeratorU3Ed__55_t2619545197::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__55_t2619545197::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__55_t2619545197::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__55_t2619545197::get_offset_of_U3CU3Es__1_3(),
	U3CGetEnumeratorU3Ed__55_t2619545197::get_offset_of_U3CpropertyU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (JArray_t3513210838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[1] = 
{
	JArray_t3513210838::get_offset_of__values_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (JTokenReader_t468116380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[4] = 
{
	JTokenReader_t468116380::get_offset_of__root_15(),
	JTokenReader_t468116380::get_offset_of__initialPath_16(),
	JTokenReader_t468116380::get_offset_of__parent_17(),
	JTokenReader_t468116380::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (JTokenWriter_t939300635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[4] = 
{
	JTokenWriter_t939300635::get_offset_of__token_14(),
	JTokenWriter_t939300635::get_offset_of__parent_15(),
	JTokenWriter_t939300635::get_offset_of__value_16(),
	JTokenWriter_t939300635::get_offset_of__current_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (JToken_t2379863307), -1, sizeof(JToken_t2379863307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2827[13] = 
{
	JToken_t2379863307::get_offset_of__parent_0(),
	JToken_t2379863307::get_offset_of__previous_1(),
	JToken_t2379863307::get_offset_of__next_2(),
	JToken_t2379863307::get_offset_of__annotations_3(),
	JToken_t2379863307_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_t2379863307_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_t2379863307_StaticFields::get_offset_of_StringTypes_6(),
	JToken_t2379863307_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_t2379863307_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_t2379863307_StaticFields::get_offset_of_UriTypes_9(),
	JToken_t2379863307_StaticFields::get_offset_of_CharTypes_10(),
	JToken_t2379863307_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_t2379863307_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (LineInfoAnnotation_t3056736104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[2] = 
{
	LineInfoAnnotation_t3056736104::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_t3056736104::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (JProperty_t1073345108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[2] = 
{
	JProperty_t1073345108::get_offset_of__content_14(),
	JProperty_t1073345108::get_offset_of__name_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (JPropertyList_t362393847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[1] = 
{
	JPropertyList_t362393847::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (U3CGetEnumeratorU3Ed__1_t1218601922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[3] = 
{
	U3CGetEnumeratorU3Ed__1_t1218601922::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_t1218601922::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_t1218601922::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (JTokenType_t1759864862)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2832[19] = 
{
	JTokenType_t1759864862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (JValue_t981543937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[2] = 
{
	JValue_t981543937::get_offset_of__valueType_13(),
	JValue_t981543937::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (BinaryConverter_t3784994590), -1, sizeof(BinaryConverter_t3784994590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2834[1] = 
{
	BinaryConverter_t3784994590_StaticFields::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (DataSetConverter_t2458331083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (DataTableConverter_t1000687607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (EntityKeyMemberConverter_t2931919244), -1, sizeof(EntityKeyMemberConverter_t2931919244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2838[1] = 
{
	EntityKeyMemberConverter_t2931919244_StaticFields::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (KeyValuePairConverter_t3765595880), -1, sizeof(KeyValuePairConverter_t3765595880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2839[1] = 
{
	KeyValuePairConverter_t3765595880_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (BsonObjectIdConverter_t3375601290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (RegexConverter_t2756728157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (XmlDocumentWrapper_t607387303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[1] = 
{
	XmlDocumentWrapper_t607387303::get_offset_of__document_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (XmlElementWrapper_t547753851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[1] = 
{
	XmlElementWrapper_t547753851::get_offset_of__element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (XmlDeclarationWrapper_t3559561692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[1] = 
{
	XmlDeclarationWrapper_t3559561692::get_offset_of__declaration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (XmlDocumentTypeWrapper_t518648022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[1] = 
{
	XmlDocumentTypeWrapper_t518648022::get_offset_of__documentType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (XmlNodeWrapper_t1329998361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[3] = 
{
	XmlNodeWrapper_t1329998361::get_offset_of__node_0(),
	XmlNodeWrapper_t1329998361::get_offset_of__childNodes_1(),
	XmlNodeWrapper_t1329998361::get_offset_of__attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (XDeclarationWrapper_t2232240502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[1] = 
{
	XDeclarationWrapper_t2232240502::get_offset_of_U3CDeclarationU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (XDocumentTypeWrapper_t2186808262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[1] = 
{
	XDocumentTypeWrapper_t2186808262::get_offset_of__documentType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (XDocumentWrapper_t3693494325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (XTextWrapper_t2743256062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (XCommentWrapper_t298428753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (XProcessingInstructionWrapper_t218556294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (XContainerWrapper_t652000426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[1] = 
{
	XContainerWrapper_t652000426::get_offset_of__childNodes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (XObjectWrapper_t542134543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[1] = 
{
	XObjectWrapper_t542134543::get_offset_of__xmlObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (XAttributeWrapper_t1809740052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (XElementWrapper_t1376508173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[1] = 
{
	XElementWrapper_t1376508173::get_offset_of__attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (XmlNodeConverter_t568579346), -1, sizeof(XmlNodeConverter_t568579346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2862[4] = 
{
	XmlNodeConverter_t568579346_StaticFields::get_offset_of_EmptyChildNodes_0(),
	XmlNodeConverter_t568579346::get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_1(),
	XmlNodeConverter_t568579346::get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_2(),
	XmlNodeConverter_t568579346::get_offset_of_U3COmitRootObjectU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (BsonBinaryType_t1803869950)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2863[8] = 
{
	BsonBinaryType_t1803869950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (BsonBinaryWriter_t3219674374), -1, sizeof(BsonBinaryWriter_t3219674374_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2864[4] = 
{
	BsonBinaryWriter_t3219674374_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t3219674374::get_offset_of__writer_1(),
	BsonBinaryWriter_t3219674374::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t3219674374::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (BsonToken_t3974775022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[2] = 
{
	BsonToken_t3974775022::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_t3974775022::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (BsonObject_t2543849103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[1] = 
{
	BsonObject_t2543849103::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (BsonArray_t3023973585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[1] = 
{
	BsonArray_t3023973585::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (BsonEmpty_t3444714005), -1, sizeof(BsonEmpty_t3444714005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2868[3] = 
{
	BsonEmpty_t3444714005_StaticFields::get_offset_of_Null_2(),
	BsonEmpty_t3444714005_StaticFields::get_offset_of_Undefined_3(),
	BsonEmpty_t3444714005::get_offset_of_U3CTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (BsonValue_t1632196520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[2] = 
{
	BsonValue_t1632196520::get_offset_of__value_2(),
	BsonValue_t1632196520::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (BsonBoolean_t654920352), -1, sizeof(BsonBoolean_t654920352_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2870[2] = 
{
	BsonBoolean_t654920352_StaticFields::get_offset_of_False_4(),
	BsonBoolean_t654920352_StaticFields::get_offset_of_True_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (BsonString_t848819904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[2] = 
{
	BsonString_t848819904::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t848819904::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (BsonBinary_t4010727797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[1] = 
{
	BsonBinary_t4010727797::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (BsonRegex_t2801632588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[2] = 
{
	BsonRegex_t2801632588::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t2801632588::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (BsonProperty_t216190049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[2] = 
{
	BsonProperty_t216190049::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t216190049::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (BsonType_t2223064640)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2875[21] = 
{
	BsonType_t2223064640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (BsonWriter_t3981304838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[4] = 
{
	BsonWriter_t3981304838::get_offset_of__writer_14(),
	BsonWriter_t3981304838::get_offset_of__root_15(),
	BsonWriter_t3981304838::get_offset_of__parent_16(),
	BsonWriter_t3981304838::get_offset_of__propertyName_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (BsonObjectId_t1370814579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[1] = 
{
	BsonObjectId_t1370814579::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2878[11] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U32BCA00E758D64214B1CD1A6C1F5AFE3E50FF4E63_0(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U3309F1F566327763634CE1DC6323D5D9C398F4D42_1(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U355357EC07A4C0DA4B54C468ED12E82299968130F_2(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U38D8384B5ECAC0436BBA54D23E278830A8800475A_3(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U393F6CC875194F5F37568D800D647ED31D6E33EC3_4(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_5(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_6(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_7(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_8(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_9(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_F0325B26CE0D81D2D40DD8FD25FA68C1D9EBA59B_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (__StaticArrayInitTypeSizeU3D10_t1548194903)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_t1548194903 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994317)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994317 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (__StaticArrayInitTypeSizeU3D28_t1904621871)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t1904621871 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (__StaticArrayInitTypeSizeU3D52_t2710732173)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D52_t2710732173 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (__StaticArrayInitTypeSizeU3D60_t1548129367)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D60_t1548129367 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (__StaticArrayInitTypeSizeU3D84_t3518153195)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D84_t3518153195 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (__StaticArrayInitTypeSizeU3D120_t3297148301)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t3297148301 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (__StaticArrayInitTypeSizeU3D168_t2500897569)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D168_t2500897569 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (U3CModuleU3E_t692745552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (FastAction_t3491443480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[2] = 
{
	FastAction_t3491443480::get_offset_of_delegates_0(),
	FastAction_t3491443480::get_offset_of_lookup_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (InlineGraphic_t2901727699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[4] = 
{
	InlineGraphic_t2901727699::get_offset_of_texture_28(),
	InlineGraphic_t2901727699::get_offset_of_m_manager_29(),
	InlineGraphic_t2901727699::get_offset_of_m_RectTransform_30(),
	InlineGraphic_t2901727699::get_offset_of_m_ParentRectTransform_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (InlineGraphicManager_t2871008645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[7] = 
{
	InlineGraphicManager_t2871008645::get_offset_of_m_spriteAsset_2(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphic_3(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphicCanvasRenderer_4(),
	InlineGraphicManager_t2871008645::get_offset_of_m_uiVertex_5(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphicRectTransform_6(),
	InlineGraphicManager_t2871008645::get_offset_of_m_textComponent_7(),
	InlineGraphicManager_t2871008645::get_offset_of_m_isInitialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (U3CU3Ec__DisplayClass28_0_t1214464005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1214464005::get_offset_of_hashCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (U3CU3Ec__DisplayClass29_0_t1214529541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[1] = 
{
	U3CU3Ec__DisplayClass29_0_t1214529541::get_offset_of_index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (MaterialReferenceManager_t2114976864), -1, sizeof(MaterialReferenceManager_t2114976864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2896[4] = 
{
	MaterialReferenceManager_t2114976864_StaticFields::get_offset_of_s_Instance_0(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_FontMaterialReferenceLookup_1(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_FontAssetReferenceLookup_2(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_SpriteAssetReferenceLookup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (MaterialReference_t1952344632)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[9] = 
{
	MaterialReference_t1952344632::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_fontAsset_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_spriteAsset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_isDefaultMaterial_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_isFallbackMaterial_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_fallbackMaterial_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_padding_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t1952344632::get_offset_of_referenceCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (TextContainerAnchors_t945851193)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2898[11] = 
{
	TextContainerAnchors_t945851193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (TextContainer_t97923372), -1, sizeof(TextContainer_t97923372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2899[13] = 
{
	TextContainer_t97923372::get_offset_of_m_hasChanged_2(),
	TextContainer_t97923372::get_offset_of_m_pivot_3(),
	TextContainer_t97923372::get_offset_of_m_anchorPosition_4(),
	TextContainer_t97923372::get_offset_of_m_rect_5(),
	TextContainer_t97923372::get_offset_of_m_isDefaultWidth_6(),
	TextContainer_t97923372::get_offset_of_m_isDefaultHeight_7(),
	TextContainer_t97923372::get_offset_of_m_isAutoFitting_8(),
	TextContainer_t97923372::get_offset_of_m_corners_9(),
	TextContainer_t97923372::get_offset_of_m_worldCorners_10(),
	TextContainer_t97923372::get_offset_of_m_margins_11(),
	TextContainer_t97923372::get_offset_of_m_rectTransform_12(),
	TextContainer_t97923372_StaticFields::get_offset_of_k_defaultSize_13(),
	TextContainer_t97923372::get_offset_of_m_textMeshPro_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
