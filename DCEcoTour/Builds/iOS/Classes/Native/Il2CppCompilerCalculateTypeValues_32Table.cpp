﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Mapbox.IO.Compression.FastEncoderWindow
struct FastEncoderWindow_t57591660;
// Mapbox.IO.Compression.Match
struct Match_t352475233;
// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>
struct Dictionary_2_t3901903956;
// System.Func`2<System.UInt32,System.Int32>
struct Func_2_t2244831341;
// System.Func`3<System.Int32,System.Int32,System.Boolean>
struct Func_3_t491204050;
// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima
struct LocalMinima_t86068969;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge>>
struct List_1_t343237081;
// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Scanbeam
struct Scanbeam_t3952834741;
// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec>
struct List_1_t1788952413;
// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge
struct TEdge_t1694054893;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t736164020;
// Mapbox.VectorTile.VectorTileReader
struct VectorTileReader_t1753322980;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1293755103;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t120437468;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// System.Int16[]
struct Int16U5BU5D_t3686840178;
// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt
struct OutPt_t2591102706;
// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode
struct PolyNode_t1300984468;
// Mapbox.VectorTile.VectorTileFeature
struct VectorTileFeature_t4093669591;
// System.Func`2<System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.LatLng>,System.Collections.Generic.IEnumerable`1<Mapbox.VectorTile.Geometry.LatLng>>
struct Func_2_t1001585409;
// System.Func`2<Mapbox.VectorTile.Geometry.LatLng,System.String>
struct Func_2_t3663939823;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Mapbox.IO.Compression.DeflateStream
struct DeflateStream_t2796728099;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint>>
struct List_1_t976755323;
// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint>
struct List_1_t3799647877;
// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/DoublePoint>
struct List_1_t3080002113;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// Mapbox.IO.Compression.OutputWindow
struct OutputWindow_t1296654655;
// Mapbox.IO.Compression.InputBuffer
struct InputBuffer_t333709416;
// Mapbox.IO.Compression.HuffmanTree
struct HuffmanTree_t857975559;
// Mapbox.IO.Compression.IFileFormatReader
struct IFileFormatReader_t1171368011;
// Mapbox.IO.Compression.FastEncoder
struct FastEncoder_t887805019;
// Mapbox.IO.Compression.CopyEncoder
struct CopyEncoder_t1719707359;
// Mapbox.IO.Compression.DeflateInput
struct DeflateInput_t3892891873;
// Mapbox.IO.Compression.OutputBuffer
struct OutputBuffer_t1331609326;
// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode>
struct List_1_t2773059210;
// Mapbox.VectorTile.VectorTileLayer
struct VectorTileLayer_t873169949;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t4032136720;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima
struct Maxima_t4278896992;
// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode>
struct List_1_t556621665;
// System.Collections.Generic.IComparer`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode>
struct IComparer_1_t338812402;
// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Join>
struct List_1_t3821086104;
// System.IO.Stream
struct Stream_t1273022909;
// Mapbox.IO.Compression.Inflater
struct Inflater_t10910524;
// Mapbox.IO.Compression.IDeflater
struct IDeflater_t2072509016;
// Mapbox.IO.Compression.DeflateStream/AsyncWriteDelegate
struct AsyncWriteDelegate_t47908213;
// Mapbox.IO.Compression.IFileFormatWriter
struct IFileFormatWriter_t4031231164;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// UnityEngine.UI.Text
struct Text_t1901882714;
// Mapbox.Examples.ForwardGeocodeUserInput
struct ForwardGeocodeUserInput_t2575136032;
// Mapbox.Directions.Directions
struct Directions_t1397515081;
// Mapbox.Utils.Vector2d[]
struct Vector2dU5BU5D_t852968953;
// Mapbox.Directions.DirectionResource
struct DirectionResource_t3837219169;
// UnityEngine.Camera
struct Camera_t4157153871;
// Mapbox.Unity.Map.AbstractMap
struct AbstractMap_t3082917158;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// Mapbox.Map.Map`1<Mapbox.Map.RasterTile>
struct Map_1_t1665010825;
// System.String[]
struct StringU5BU5D_t1281789340;
// Mapbox.Examples.ReverseGeocodeUserInput
struct ReverseGeocodeUserInput_t2632079094;
// Mapbox.Map.Map`1<Mapbox.Map.VectorTile>
struct Map_1_t3054239837;
// Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory
struct FlatSphereTerrainFactory_t1099794750;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef UNIXTIMESTAMPUTILS_T2933311910_H
#define UNIXTIMESTAMPUTILS_T2933311910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.UnixTimestampUtils
struct  UnixTimestampUtils_t2933311910  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIXTIMESTAMPUTILS_T2933311910_H
#ifndef COPYENCODER_T1719707359_H
#define COPYENCODER_T1719707359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.CopyEncoder
struct  CopyEncoder_t1719707359  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYENCODER_T1719707359_H
#ifndef CRC32HELPER_T2911080199_H
#define CRC32HELPER_T2911080199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.Crc32Helper
struct  Crc32Helper_t2911080199  : public RuntimeObject
{
public:

public:
};

struct Crc32Helper_t2911080199_StaticFields
{
public:
	// System.UInt32[] Mapbox.IO.Compression.Crc32Helper::crcTable
	UInt32U5BU5D_t2770800703* ___crcTable_0;

public:
	inline static int32_t get_offset_of_crcTable_0() { return static_cast<int32_t>(offsetof(Crc32Helper_t2911080199_StaticFields, ___crcTable_0)); }
	inline UInt32U5BU5D_t2770800703* get_crcTable_0() const { return ___crcTable_0; }
	inline UInt32U5BU5D_t2770800703** get_address_of_crcTable_0() { return &___crcTable_0; }
	inline void set_crcTable_0(UInt32U5BU5D_t2770800703* value)
	{
		___crcTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___crcTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32HELPER_T2911080199_H
#ifndef DEFLATEINPUT_T3892891873_H
#define DEFLATEINPUT_T3892891873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflateInput
struct  DeflateInput_t3892891873  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.IO.Compression.DeflateInput::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 Mapbox.IO.Compression.DeflateInput::count
	int32_t ___count_1;
	// System.Int32 Mapbox.IO.Compression.DeflateInput::startIndex
	int32_t ___startIndex_2;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(DeflateInput_t3892891873, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(DeflateInput_t3892891873, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_startIndex_2() { return static_cast<int32_t>(offsetof(DeflateInput_t3892891873, ___startIndex_2)); }
	inline int32_t get_startIndex_2() const { return ___startIndex_2; }
	inline int32_t* get_address_of_startIndex_2() { return &___startIndex_2; }
	inline void set_startIndex_2(int32_t value)
	{
		___startIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATEINPUT_T3892891873_H
#ifndef DEFLATESTREAMASYNCRESULT_T1893920535_H
#define DEFLATESTREAMASYNCRESULT_T1893920535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflateStreamAsyncResult
struct  DeflateStreamAsyncResult_t1893920535  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.IO.Compression.DeflateStreamAsyncResult::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 Mapbox.IO.Compression.DeflateStreamAsyncResult::offset
	int32_t ___offset_1;
	// System.Int32 Mapbox.IO.Compression.DeflateStreamAsyncResult::count
	int32_t ___count_2;
	// System.Boolean Mapbox.IO.Compression.DeflateStreamAsyncResult::isWrite
	bool ___isWrite_3;
	// System.Object Mapbox.IO.Compression.DeflateStreamAsyncResult::m_AsyncObject
	RuntimeObject * ___m_AsyncObject_4;
	// System.Object Mapbox.IO.Compression.DeflateStreamAsyncResult::m_AsyncState
	RuntimeObject * ___m_AsyncState_5;
	// System.AsyncCallback Mapbox.IO.Compression.DeflateStreamAsyncResult::m_AsyncCallback
	AsyncCallback_t3962456242 * ___m_AsyncCallback_6;
	// System.Object Mapbox.IO.Compression.DeflateStreamAsyncResult::m_Result
	RuntimeObject * ___m_Result_7;
	// System.Boolean Mapbox.IO.Compression.DeflateStreamAsyncResult::m_CompletedSynchronously
	bool ___m_CompletedSynchronously_8;
	// System.Int32 Mapbox.IO.Compression.DeflateStreamAsyncResult::m_InvokedCallback
	int32_t ___m_InvokedCallback_9;
	// System.Int32 Mapbox.IO.Compression.DeflateStreamAsyncResult::m_Completed
	int32_t ___m_Completed_10;
	// System.Object Mapbox.IO.Compression.DeflateStreamAsyncResult::m_Event
	RuntimeObject * ___m_Event_11;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_offset_1() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___offset_1)); }
	inline int32_t get_offset_1() const { return ___offset_1; }
	inline int32_t* get_address_of_offset_1() { return &___offset_1; }
	inline void set_offset_1(int32_t value)
	{
		___offset_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_isWrite_3() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___isWrite_3)); }
	inline bool get_isWrite_3() const { return ___isWrite_3; }
	inline bool* get_address_of_isWrite_3() { return &___isWrite_3; }
	inline void set_isWrite_3(bool value)
	{
		___isWrite_3 = value;
	}

	inline static int32_t get_offset_of_m_AsyncObject_4() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_AsyncObject_4)); }
	inline RuntimeObject * get_m_AsyncObject_4() const { return ___m_AsyncObject_4; }
	inline RuntimeObject ** get_address_of_m_AsyncObject_4() { return &___m_AsyncObject_4; }
	inline void set_m_AsyncObject_4(RuntimeObject * value)
	{
		___m_AsyncObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncObject_4), value);
	}

	inline static int32_t get_offset_of_m_AsyncState_5() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_AsyncState_5)); }
	inline RuntimeObject * get_m_AsyncState_5() const { return ___m_AsyncState_5; }
	inline RuntimeObject ** get_address_of_m_AsyncState_5() { return &___m_AsyncState_5; }
	inline void set_m_AsyncState_5(RuntimeObject * value)
	{
		___m_AsyncState_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncState_5), value);
	}

	inline static int32_t get_offset_of_m_AsyncCallback_6() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_AsyncCallback_6)); }
	inline AsyncCallback_t3962456242 * get_m_AsyncCallback_6() const { return ___m_AsyncCallback_6; }
	inline AsyncCallback_t3962456242 ** get_address_of_m_AsyncCallback_6() { return &___m_AsyncCallback_6; }
	inline void set_m_AsyncCallback_6(AsyncCallback_t3962456242 * value)
	{
		___m_AsyncCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncCallback_6), value);
	}

	inline static int32_t get_offset_of_m_Result_7() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_Result_7)); }
	inline RuntimeObject * get_m_Result_7() const { return ___m_Result_7; }
	inline RuntimeObject ** get_address_of_m_Result_7() { return &___m_Result_7; }
	inline void set_m_Result_7(RuntimeObject * value)
	{
		___m_Result_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_7), value);
	}

	inline static int32_t get_offset_of_m_CompletedSynchronously_8() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_CompletedSynchronously_8)); }
	inline bool get_m_CompletedSynchronously_8() const { return ___m_CompletedSynchronously_8; }
	inline bool* get_address_of_m_CompletedSynchronously_8() { return &___m_CompletedSynchronously_8; }
	inline void set_m_CompletedSynchronously_8(bool value)
	{
		___m_CompletedSynchronously_8 = value;
	}

	inline static int32_t get_offset_of_m_InvokedCallback_9() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_InvokedCallback_9)); }
	inline int32_t get_m_InvokedCallback_9() const { return ___m_InvokedCallback_9; }
	inline int32_t* get_address_of_m_InvokedCallback_9() { return &___m_InvokedCallback_9; }
	inline void set_m_InvokedCallback_9(int32_t value)
	{
		___m_InvokedCallback_9 = value;
	}

	inline static int32_t get_offset_of_m_Completed_10() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_Completed_10)); }
	inline int32_t get_m_Completed_10() const { return ___m_Completed_10; }
	inline int32_t* get_address_of_m_Completed_10() { return &___m_Completed_10; }
	inline void set_m_Completed_10(int32_t value)
	{
		___m_Completed_10 = value;
	}

	inline static int32_t get_offset_of_m_Event_11() { return static_cast<int32_t>(offsetof(DeflateStreamAsyncResult_t1893920535, ___m_Event_11)); }
	inline RuntimeObject * get_m_Event_11() const { return ___m_Event_11; }
	inline RuntimeObject ** get_address_of_m_Event_11() { return &___m_Event_11; }
	inline void set_m_Event_11(RuntimeObject * value)
	{
		___m_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Event_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAMASYNCRESULT_T1893920535_H
#ifndef FASTENCODER_T887805019_H
#define FASTENCODER_T887805019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.FastEncoder
struct  FastEncoder_t887805019  : public RuntimeObject
{
public:
	// Mapbox.IO.Compression.FastEncoderWindow Mapbox.IO.Compression.FastEncoder::inputWindow
	FastEncoderWindow_t57591660 * ___inputWindow_0;
	// Mapbox.IO.Compression.Match Mapbox.IO.Compression.FastEncoder::currentMatch
	Match_t352475233 * ___currentMatch_1;
	// System.Double Mapbox.IO.Compression.FastEncoder::lastCompressionRatio
	double ___lastCompressionRatio_2;

public:
	inline static int32_t get_offset_of_inputWindow_0() { return static_cast<int32_t>(offsetof(FastEncoder_t887805019, ___inputWindow_0)); }
	inline FastEncoderWindow_t57591660 * get_inputWindow_0() const { return ___inputWindow_0; }
	inline FastEncoderWindow_t57591660 ** get_address_of_inputWindow_0() { return &___inputWindow_0; }
	inline void set_inputWindow_0(FastEncoderWindow_t57591660 * value)
	{
		___inputWindow_0 = value;
		Il2CppCodeGenWriteBarrier((&___inputWindow_0), value);
	}

	inline static int32_t get_offset_of_currentMatch_1() { return static_cast<int32_t>(offsetof(FastEncoder_t887805019, ___currentMatch_1)); }
	inline Match_t352475233 * get_currentMatch_1() const { return ___currentMatch_1; }
	inline Match_t352475233 ** get_address_of_currentMatch_1() { return &___currentMatch_1; }
	inline void set_currentMatch_1(Match_t352475233 * value)
	{
		___currentMatch_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentMatch_1), value);
	}

	inline static int32_t get_offset_of_lastCompressionRatio_2() { return static_cast<int32_t>(offsetof(FastEncoder_t887805019, ___lastCompressionRatio_2)); }
	inline double get_lastCompressionRatio_2() const { return ___lastCompressionRatio_2; }
	inline double* get_address_of_lastCompressionRatio_2() { return &___lastCompressionRatio_2; }
	inline void set_lastCompressionRatio_2(double value)
	{
		___lastCompressionRatio_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTENCODER_T887805019_H
#ifndef VECTORTILEREADER_T1753322980_H
#define VECTORTILEREADER_T1753322980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.VectorTileReader
struct  VectorTileReader_t1753322980  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]> Mapbox.VectorTile.VectorTileReader::_Layers
	Dictionary_2_t3901903956 * ____Layers_0;
	// System.Boolean Mapbox.VectorTile.VectorTileReader::_Validate
	bool ____Validate_1;

public:
	inline static int32_t get_offset_of__Layers_0() { return static_cast<int32_t>(offsetof(VectorTileReader_t1753322980, ____Layers_0)); }
	inline Dictionary_2_t3901903956 * get__Layers_0() const { return ____Layers_0; }
	inline Dictionary_2_t3901903956 ** get_address_of__Layers_0() { return &____Layers_0; }
	inline void set__Layers_0(Dictionary_2_t3901903956 * value)
	{
		____Layers_0 = value;
		Il2CppCodeGenWriteBarrier((&____Layers_0), value);
	}

	inline static int32_t get_offset_of__Validate_1() { return static_cast<int32_t>(offsetof(VectorTileReader_t1753322980, ____Validate_1)); }
	inline bool get__Validate_1() const { return ____Validate_1; }
	inline bool* get_address_of__Validate_1() { return &____Validate_1; }
	inline void set__Validate_1(bool value)
	{
		____Validate_1 = value;
	}
};

struct VectorTileReader_t1753322980_StaticFields
{
public:
	// System.Func`2<System.UInt32,System.Int32> Mapbox.VectorTile.VectorTileReader::<>f__am$cache0
	Func_2_t2244831341 * ___U3CU3Ef__amU24cache0_2;
	// System.Func`3<System.Int32,System.Int32,System.Boolean> Mapbox.VectorTile.VectorTileReader::<>f__am$cache1
	Func_3_t491204050 * ___U3CU3Ef__amU24cache1_3;
	// System.Func`3<System.Int32,System.Int32,System.Boolean> Mapbox.VectorTile.VectorTileReader::<>f__am$cache2
	Func_3_t491204050 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(VectorTileReader_t1753322980_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t2244831341 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t2244831341 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t2244831341 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(VectorTileReader_t1753322980_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Func_3_t491204050 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Func_3_t491204050 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Func_3_t491204050 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(VectorTileReader_t1753322980_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Func_3_t491204050 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Func_3_t491204050 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Func_3_t491204050 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILEREADER_T1753322980_H
#ifndef CLIPPERBASE_T2411222589_H
#define CLIPPERBASE_T2411222589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase
struct  ClipperBase_t2411222589  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_MinimaList
	LocalMinima_t86068969 * ___m_MinimaList_6;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_CurrentLM
	LocalMinima_t86068969 * ___m_CurrentLM_7;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge>> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_edges
	List_1_t343237081 * ___m_edges_8;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Scanbeam Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_Scanbeam
	Scanbeam_t3952834741 * ___m_Scanbeam_9;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_PolyOuts
	List_1_t1788952413 * ___m_PolyOuts_10;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_ActiveEdges
	TEdge_t1694054893 * ___m_ActiveEdges_11;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_UseFullRange
	bool ___m_UseFullRange_12;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::m_HasOpenPaths
	bool ___m_HasOpenPaths_13;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperBase::<PreserveCollinear>k__BackingField
	bool ___U3CPreserveCollinearU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_m_MinimaList_6() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_MinimaList_6)); }
	inline LocalMinima_t86068969 * get_m_MinimaList_6() const { return ___m_MinimaList_6; }
	inline LocalMinima_t86068969 ** get_address_of_m_MinimaList_6() { return &___m_MinimaList_6; }
	inline void set_m_MinimaList_6(LocalMinima_t86068969 * value)
	{
		___m_MinimaList_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MinimaList_6), value);
	}

	inline static int32_t get_offset_of_m_CurrentLM_7() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_CurrentLM_7)); }
	inline LocalMinima_t86068969 * get_m_CurrentLM_7() const { return ___m_CurrentLM_7; }
	inline LocalMinima_t86068969 ** get_address_of_m_CurrentLM_7() { return &___m_CurrentLM_7; }
	inline void set_m_CurrentLM_7(LocalMinima_t86068969 * value)
	{
		___m_CurrentLM_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentLM_7), value);
	}

	inline static int32_t get_offset_of_m_edges_8() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_edges_8)); }
	inline List_1_t343237081 * get_m_edges_8() const { return ___m_edges_8; }
	inline List_1_t343237081 ** get_address_of_m_edges_8() { return &___m_edges_8; }
	inline void set_m_edges_8(List_1_t343237081 * value)
	{
		___m_edges_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_edges_8), value);
	}

	inline static int32_t get_offset_of_m_Scanbeam_9() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_Scanbeam_9)); }
	inline Scanbeam_t3952834741 * get_m_Scanbeam_9() const { return ___m_Scanbeam_9; }
	inline Scanbeam_t3952834741 ** get_address_of_m_Scanbeam_9() { return &___m_Scanbeam_9; }
	inline void set_m_Scanbeam_9(Scanbeam_t3952834741 * value)
	{
		___m_Scanbeam_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Scanbeam_9), value);
	}

	inline static int32_t get_offset_of_m_PolyOuts_10() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_PolyOuts_10)); }
	inline List_1_t1788952413 * get_m_PolyOuts_10() const { return ___m_PolyOuts_10; }
	inline List_1_t1788952413 ** get_address_of_m_PolyOuts_10() { return &___m_PolyOuts_10; }
	inline void set_m_PolyOuts_10(List_1_t1788952413 * value)
	{
		___m_PolyOuts_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_PolyOuts_10), value);
	}

	inline static int32_t get_offset_of_m_ActiveEdges_11() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_ActiveEdges_11)); }
	inline TEdge_t1694054893 * get_m_ActiveEdges_11() const { return ___m_ActiveEdges_11; }
	inline TEdge_t1694054893 ** get_address_of_m_ActiveEdges_11() { return &___m_ActiveEdges_11; }
	inline void set_m_ActiveEdges_11(TEdge_t1694054893 * value)
	{
		___m_ActiveEdges_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveEdges_11), value);
	}

	inline static int32_t get_offset_of_m_UseFullRange_12() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_UseFullRange_12)); }
	inline bool get_m_UseFullRange_12() const { return ___m_UseFullRange_12; }
	inline bool* get_address_of_m_UseFullRange_12() { return &___m_UseFullRange_12; }
	inline void set_m_UseFullRange_12(bool value)
	{
		___m_UseFullRange_12 = value;
	}

	inline static int32_t get_offset_of_m_HasOpenPaths_13() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___m_HasOpenPaths_13)); }
	inline bool get_m_HasOpenPaths_13() const { return ___m_HasOpenPaths_13; }
	inline bool* get_address_of_m_HasOpenPaths_13() { return &___m_HasOpenPaths_13; }
	inline void set_m_HasOpenPaths_13(bool value)
	{
		___m_HasOpenPaths_13 = value;
	}

	inline static int32_t get_offset_of_U3CPreserveCollinearU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ClipperBase_t2411222589, ___U3CPreserveCollinearU3Ek__BackingField_14)); }
	inline bool get_U3CPreserveCollinearU3Ek__BackingField_14() const { return ___U3CPreserveCollinearU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CPreserveCollinearU3Ek__BackingField_14() { return &___U3CPreserveCollinearU3Ek__BackingField_14; }
	inline void set_U3CPreserveCollinearU3Ek__BackingField_14(bool value)
	{
		___U3CPreserveCollinearU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPERBASE_T2411222589_H
#ifndef DECODEGEOMETRY_T3735437420_H
#define DECODEGEOMETRY_T3735437420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.DecodeGeometry
struct  DecodeGeometry_t3735437420  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEGEOMETRY_T3735437420_H
#ifndef UTILGEOM_T2066125609_H
#define UTILGEOM_T2066125609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.UtilGeom
struct  UtilGeom_t2066125609  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILGEOM_T2066125609_H
#ifndef CONSTANTSASDICTIONARY_T107503724_H
#define CONSTANTSASDICTIONARY_T107503724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Contants.ConstantsAsDictionary
struct  ConstantsAsDictionary_t107503724  : public RuntimeObject
{
public:

public:
};

struct ConstantsAsDictionary_t107503724_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> Mapbox.VectorTile.Contants.ConstantsAsDictionary::TileType
	Dictionary_2_t736164020 * ___TileType_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> Mapbox.VectorTile.Contants.ConstantsAsDictionary::LayerType
	Dictionary_2_t736164020 * ___LayerType_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> Mapbox.VectorTile.Contants.ConstantsAsDictionary::FeatureType
	Dictionary_2_t736164020 * ___FeatureType_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> Mapbox.VectorTile.Contants.ConstantsAsDictionary::GeomType
	Dictionary_2_t736164020 * ___GeomType_3;

public:
	inline static int32_t get_offset_of_TileType_0() { return static_cast<int32_t>(offsetof(ConstantsAsDictionary_t107503724_StaticFields, ___TileType_0)); }
	inline Dictionary_2_t736164020 * get_TileType_0() const { return ___TileType_0; }
	inline Dictionary_2_t736164020 ** get_address_of_TileType_0() { return &___TileType_0; }
	inline void set_TileType_0(Dictionary_2_t736164020 * value)
	{
		___TileType_0 = value;
		Il2CppCodeGenWriteBarrier((&___TileType_0), value);
	}

	inline static int32_t get_offset_of_LayerType_1() { return static_cast<int32_t>(offsetof(ConstantsAsDictionary_t107503724_StaticFields, ___LayerType_1)); }
	inline Dictionary_2_t736164020 * get_LayerType_1() const { return ___LayerType_1; }
	inline Dictionary_2_t736164020 ** get_address_of_LayerType_1() { return &___LayerType_1; }
	inline void set_LayerType_1(Dictionary_2_t736164020 * value)
	{
		___LayerType_1 = value;
		Il2CppCodeGenWriteBarrier((&___LayerType_1), value);
	}

	inline static int32_t get_offset_of_FeatureType_2() { return static_cast<int32_t>(offsetof(ConstantsAsDictionary_t107503724_StaticFields, ___FeatureType_2)); }
	inline Dictionary_2_t736164020 * get_FeatureType_2() const { return ___FeatureType_2; }
	inline Dictionary_2_t736164020 ** get_address_of_FeatureType_2() { return &___FeatureType_2; }
	inline void set_FeatureType_2(Dictionary_2_t736164020 * value)
	{
		___FeatureType_2 = value;
		Il2CppCodeGenWriteBarrier((&___FeatureType_2), value);
	}

	inline static int32_t get_offset_of_GeomType_3() { return static_cast<int32_t>(offsetof(ConstantsAsDictionary_t107503724_StaticFields, ___GeomType_3)); }
	inline Dictionary_2_t736164020 * get_GeomType_3() const { return ___GeomType_3; }
	inline Dictionary_2_t736164020 ** get_address_of_GeomType_3() { return &___GeomType_3; }
	inline void set_GeomType_3(Dictionary_2_t736164020 * value)
	{
		___GeomType_3 = value;
		Il2CppCodeGenWriteBarrier((&___GeomType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTSASDICTIONARY_T107503724_H
#ifndef VECTORTILE_T3467883484_H
#define VECTORTILE_T3467883484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.VectorTile
struct  VectorTile_t3467883484  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.VectorTileReader Mapbox.VectorTile.VectorTile::_VTR
	VectorTileReader_t1753322980 * ____VTR_0;

public:
	inline static int32_t get_offset_of__VTR_0() { return static_cast<int32_t>(offsetof(VectorTile_t3467883484, ____VTR_0)); }
	inline VectorTileReader_t1753322980 * get__VTR_0() const { return ____VTR_0; }
	inline VectorTileReader_t1753322980 ** get_address_of__VTR_0() { return &____VTR_0; }
	inline void set__VTR_0(VectorTileReader_t1753322980 * value)
	{
		____VTR_0 = value;
		Il2CppCodeGenWriteBarrier((&____VTR_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILE_T3467883484_H
#ifndef VECTORTILELAYER_T873169949_H
#define VECTORTILELAYER_T873169949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.VectorTileLayer
struct  VectorTileLayer_t873169949  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.VectorTile.VectorTileLayer::<Data>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CDataU3Ek__BackingField_0;
	// System.String Mapbox.VectorTile.VectorTileLayer::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.UInt64 Mapbox.VectorTile.VectorTileLayer::<Version>k__BackingField
	uint64_t ___U3CVersionU3Ek__BackingField_2;
	// System.UInt64 Mapbox.VectorTile.VectorTileLayer::<Extent>k__BackingField
	uint64_t ___U3CExtentU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.Byte[]> Mapbox.VectorTile.VectorTileLayer::<_FeaturesData>k__BackingField
	List_1_t1293755103 * ___U3C_FeaturesDataU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.Object> Mapbox.VectorTile.VectorTileLayer::<Values>k__BackingField
	List_1_t257213610 * ___U3CValuesU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> Mapbox.VectorTile.VectorTileLayer::<Keys>k__BackingField
	List_1_t3319525431 * ___U3CKeysU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VectorTileLayer_t873169949, ___U3CDataU3Ek__BackingField_0)); }
	inline ByteU5BU5D_t4116647657* get_U3CDataU3Ek__BackingField_0() const { return ___U3CDataU3Ek__BackingField_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CDataU3Ek__BackingField_0() { return &___U3CDataU3Ek__BackingField_0; }
	inline void set_U3CDataU3Ek__BackingField_0(ByteU5BU5D_t4116647657* value)
	{
		___U3CDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VectorTileLayer_t873169949, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VectorTileLayer_t873169949, ___U3CVersionU3Ek__BackingField_2)); }
	inline uint64_t get_U3CVersionU3Ek__BackingField_2() const { return ___U3CVersionU3Ek__BackingField_2; }
	inline uint64_t* get_address_of_U3CVersionU3Ek__BackingField_2() { return &___U3CVersionU3Ek__BackingField_2; }
	inline void set_U3CVersionU3Ek__BackingField_2(uint64_t value)
	{
		___U3CVersionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CExtentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VectorTileLayer_t873169949, ___U3CExtentU3Ek__BackingField_3)); }
	inline uint64_t get_U3CExtentU3Ek__BackingField_3() const { return ___U3CExtentU3Ek__BackingField_3; }
	inline uint64_t* get_address_of_U3CExtentU3Ek__BackingField_3() { return &___U3CExtentU3Ek__BackingField_3; }
	inline void set_U3CExtentU3Ek__BackingField_3(uint64_t value)
	{
		___U3CExtentU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3C_FeaturesDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VectorTileLayer_t873169949, ___U3C_FeaturesDataU3Ek__BackingField_4)); }
	inline List_1_t1293755103 * get_U3C_FeaturesDataU3Ek__BackingField_4() const { return ___U3C_FeaturesDataU3Ek__BackingField_4; }
	inline List_1_t1293755103 ** get_address_of_U3C_FeaturesDataU3Ek__BackingField_4() { return &___U3C_FeaturesDataU3Ek__BackingField_4; }
	inline void set_U3C_FeaturesDataU3Ek__BackingField_4(List_1_t1293755103 * value)
	{
		___U3C_FeaturesDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_FeaturesDataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CValuesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VectorTileLayer_t873169949, ___U3CValuesU3Ek__BackingField_5)); }
	inline List_1_t257213610 * get_U3CValuesU3Ek__BackingField_5() const { return ___U3CValuesU3Ek__BackingField_5; }
	inline List_1_t257213610 ** get_address_of_U3CValuesU3Ek__BackingField_5() { return &___U3CValuesU3Ek__BackingField_5; }
	inline void set_U3CValuesU3Ek__BackingField_5(List_1_t257213610 * value)
	{
		___U3CValuesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValuesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CKeysU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(VectorTileLayer_t873169949, ___U3CKeysU3Ek__BackingField_6)); }
	inline List_1_t3319525431 * get_U3CKeysU3Ek__BackingField_6() const { return ___U3CKeysU3Ek__BackingField_6; }
	inline List_1_t3319525431 ** get_address_of_U3CKeysU3Ek__BackingField_6() { return &___U3CKeysU3Ek__BackingField_6; }
	inline void set_U3CKeysU3Ek__BackingField_6(List_1_t3319525431 * value)
	{
		___U3CKeysU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeysU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILELAYER_T873169949_H
#ifndef FASTENCODERSTATICS_T1128548993_H
#define FASTENCODERSTATICS_T1128548993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.FastEncoderStatics
struct  FastEncoderStatics_t1128548993  : public RuntimeObject
{
public:

public:
};

struct FastEncoderStatics_t1128548993_StaticFields
{
public:
	// System.Byte[] Mapbox.IO.Compression.FastEncoderStatics::FastEncoderTreeStructureData
	ByteU5BU5D_t4116647657* ___FastEncoderTreeStructureData_0;
	// System.Byte[] Mapbox.IO.Compression.FastEncoderStatics::BFinalFastEncoderTreeStructureData
	ByteU5BU5D_t4116647657* ___BFinalFastEncoderTreeStructureData_1;
	// System.UInt32[] Mapbox.IO.Compression.FastEncoderStatics::FastEncoderLiteralCodeInfo
	UInt32U5BU5D_t2770800703* ___FastEncoderLiteralCodeInfo_2;
	// System.UInt32[] Mapbox.IO.Compression.FastEncoderStatics::FastEncoderDistanceCodeInfo
	UInt32U5BU5D_t2770800703* ___FastEncoderDistanceCodeInfo_3;
	// System.UInt32[] Mapbox.IO.Compression.FastEncoderStatics::BitMask
	UInt32U5BU5D_t2770800703* ___BitMask_4;
	// System.Byte[] Mapbox.IO.Compression.FastEncoderStatics::ExtraLengthBits
	ByteU5BU5D_t4116647657* ___ExtraLengthBits_5;
	// System.Byte[] Mapbox.IO.Compression.FastEncoderStatics::ExtraDistanceBits
	ByteU5BU5D_t4116647657* ___ExtraDistanceBits_6;
	// System.Byte[] Mapbox.IO.Compression.FastEncoderStatics::distLookup
	ByteU5BU5D_t4116647657* ___distLookup_17;

public:
	inline static int32_t get_offset_of_FastEncoderTreeStructureData_0() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___FastEncoderTreeStructureData_0)); }
	inline ByteU5BU5D_t4116647657* get_FastEncoderTreeStructureData_0() const { return ___FastEncoderTreeStructureData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_FastEncoderTreeStructureData_0() { return &___FastEncoderTreeStructureData_0; }
	inline void set_FastEncoderTreeStructureData_0(ByteU5BU5D_t4116647657* value)
	{
		___FastEncoderTreeStructureData_0 = value;
		Il2CppCodeGenWriteBarrier((&___FastEncoderTreeStructureData_0), value);
	}

	inline static int32_t get_offset_of_BFinalFastEncoderTreeStructureData_1() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___BFinalFastEncoderTreeStructureData_1)); }
	inline ByteU5BU5D_t4116647657* get_BFinalFastEncoderTreeStructureData_1() const { return ___BFinalFastEncoderTreeStructureData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_BFinalFastEncoderTreeStructureData_1() { return &___BFinalFastEncoderTreeStructureData_1; }
	inline void set_BFinalFastEncoderTreeStructureData_1(ByteU5BU5D_t4116647657* value)
	{
		___BFinalFastEncoderTreeStructureData_1 = value;
		Il2CppCodeGenWriteBarrier((&___BFinalFastEncoderTreeStructureData_1), value);
	}

	inline static int32_t get_offset_of_FastEncoderLiteralCodeInfo_2() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___FastEncoderLiteralCodeInfo_2)); }
	inline UInt32U5BU5D_t2770800703* get_FastEncoderLiteralCodeInfo_2() const { return ___FastEncoderLiteralCodeInfo_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of_FastEncoderLiteralCodeInfo_2() { return &___FastEncoderLiteralCodeInfo_2; }
	inline void set_FastEncoderLiteralCodeInfo_2(UInt32U5BU5D_t2770800703* value)
	{
		___FastEncoderLiteralCodeInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___FastEncoderLiteralCodeInfo_2), value);
	}

	inline static int32_t get_offset_of_FastEncoderDistanceCodeInfo_3() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___FastEncoderDistanceCodeInfo_3)); }
	inline UInt32U5BU5D_t2770800703* get_FastEncoderDistanceCodeInfo_3() const { return ___FastEncoderDistanceCodeInfo_3; }
	inline UInt32U5BU5D_t2770800703** get_address_of_FastEncoderDistanceCodeInfo_3() { return &___FastEncoderDistanceCodeInfo_3; }
	inline void set_FastEncoderDistanceCodeInfo_3(UInt32U5BU5D_t2770800703* value)
	{
		___FastEncoderDistanceCodeInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___FastEncoderDistanceCodeInfo_3), value);
	}

	inline static int32_t get_offset_of_BitMask_4() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___BitMask_4)); }
	inline UInt32U5BU5D_t2770800703* get_BitMask_4() const { return ___BitMask_4; }
	inline UInt32U5BU5D_t2770800703** get_address_of_BitMask_4() { return &___BitMask_4; }
	inline void set_BitMask_4(UInt32U5BU5D_t2770800703* value)
	{
		___BitMask_4 = value;
		Il2CppCodeGenWriteBarrier((&___BitMask_4), value);
	}

	inline static int32_t get_offset_of_ExtraLengthBits_5() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___ExtraLengthBits_5)); }
	inline ByteU5BU5D_t4116647657* get_ExtraLengthBits_5() const { return ___ExtraLengthBits_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_ExtraLengthBits_5() { return &___ExtraLengthBits_5; }
	inline void set_ExtraLengthBits_5(ByteU5BU5D_t4116647657* value)
	{
		___ExtraLengthBits_5 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraLengthBits_5), value);
	}

	inline static int32_t get_offset_of_ExtraDistanceBits_6() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___ExtraDistanceBits_6)); }
	inline ByteU5BU5D_t4116647657* get_ExtraDistanceBits_6() const { return ___ExtraDistanceBits_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_ExtraDistanceBits_6() { return &___ExtraDistanceBits_6; }
	inline void set_ExtraDistanceBits_6(ByteU5BU5D_t4116647657* value)
	{
		___ExtraDistanceBits_6 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraDistanceBits_6), value);
	}

	inline static int32_t get_offset_of_distLookup_17() { return static_cast<int32_t>(offsetof(FastEncoderStatics_t1128548993_StaticFields, ___distLookup_17)); }
	inline ByteU5BU5D_t4116647657* get_distLookup_17() const { return ___distLookup_17; }
	inline ByteU5BU5D_t4116647657** get_address_of_distLookup_17() { return &___distLookup_17; }
	inline void set_distLookup_17(ByteU5BU5D_t4116647657* value)
	{
		___distLookup_17 = value;
		Il2CppCodeGenWriteBarrier((&___distLookup_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTENCODERSTATICS_T1128548993_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef SWITCH_T4228844028_H
#define SWITCH_T4228844028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Switch
struct  Switch_t4228844028  : public RuntimeObject
{
public:
	// System.String System.Diagnostics.Switch::name
	String_t* ___name_0;
	// System.String System.Diagnostics.Switch::description
	String_t* ___description_1;
	// System.Int32 System.Diagnostics.Switch::switchSetting
	int32_t ___switchSetting_2;
	// System.String System.Diagnostics.Switch::value
	String_t* ___value_3;
	// System.String System.Diagnostics.Switch::defaultSwitchValue
	String_t* ___defaultSwitchValue_4;
	// System.Boolean System.Diagnostics.Switch::initialized
	bool ___initialized_5;
	// System.Collections.Specialized.StringDictionary System.Diagnostics.Switch::attributes
	StringDictionary_t120437468 * ___attributes_6;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Switch_t4228844028, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(Switch_t4228844028, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((&___description_1), value);
	}

	inline static int32_t get_offset_of_switchSetting_2() { return static_cast<int32_t>(offsetof(Switch_t4228844028, ___switchSetting_2)); }
	inline int32_t get_switchSetting_2() const { return ___switchSetting_2; }
	inline int32_t* get_address_of_switchSetting_2() { return &___switchSetting_2; }
	inline void set_switchSetting_2(int32_t value)
	{
		___switchSetting_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Switch_t4228844028, ___value_3)); }
	inline String_t* get_value_3() const { return ___value_3; }
	inline String_t** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(String_t* value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}

	inline static int32_t get_offset_of_defaultSwitchValue_4() { return static_cast<int32_t>(offsetof(Switch_t4228844028, ___defaultSwitchValue_4)); }
	inline String_t* get_defaultSwitchValue_4() const { return ___defaultSwitchValue_4; }
	inline String_t** get_address_of_defaultSwitchValue_4() { return &___defaultSwitchValue_4; }
	inline void set_defaultSwitchValue_4(String_t* value)
	{
		___defaultSwitchValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSwitchValue_4), value);
	}

	inline static int32_t get_offset_of_initialized_5() { return static_cast<int32_t>(offsetof(Switch_t4228844028, ___initialized_5)); }
	inline bool get_initialized_5() const { return ___initialized_5; }
	inline bool* get_address_of_initialized_5() { return &___initialized_5; }
	inline void set_initialized_5(bool value)
	{
		___initialized_5 = value;
	}

	inline static int32_t get_offset_of_attributes_6() { return static_cast<int32_t>(offsetof(Switch_t4228844028, ___attributes_6)); }
	inline StringDictionary_t120437468 * get_attributes_6() const { return ___attributes_6; }
	inline StringDictionary_t120437468 ** get_address_of_attributes_6() { return &___attributes_6; }
	inline void set_attributes_6(StringDictionary_t120437468 * value)
	{
		___attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCH_T4228844028_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SR_T2098860177_H
#define SR_T2098860177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.SR
struct  SR_t2098860177  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T2098860177_H
#ifndef OUTPUTWINDOW_T1296654655_H
#define OUTPUTWINDOW_T1296654655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.OutputWindow
struct  OutputWindow_t1296654655  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.IO.Compression.OutputWindow::window
	ByteU5BU5D_t4116647657* ___window_2;
	// System.Int32 Mapbox.IO.Compression.OutputWindow::end
	int32_t ___end_3;
	// System.Int32 Mapbox.IO.Compression.OutputWindow::bytesUsed
	int32_t ___bytesUsed_4;

public:
	inline static int32_t get_offset_of_window_2() { return static_cast<int32_t>(offsetof(OutputWindow_t1296654655, ___window_2)); }
	inline ByteU5BU5D_t4116647657* get_window_2() const { return ___window_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_window_2() { return &___window_2; }
	inline void set_window_2(ByteU5BU5D_t4116647657* value)
	{
		___window_2 = value;
		Il2CppCodeGenWriteBarrier((&___window_2), value);
	}

	inline static int32_t get_offset_of_end_3() { return static_cast<int32_t>(offsetof(OutputWindow_t1296654655, ___end_3)); }
	inline int32_t get_end_3() const { return ___end_3; }
	inline int32_t* get_address_of_end_3() { return &___end_3; }
	inline void set_end_3(int32_t value)
	{
		___end_3 = value;
	}

	inline static int32_t get_offset_of_bytesUsed_4() { return static_cast<int32_t>(offsetof(OutputWindow_t1296654655, ___bytesUsed_4)); }
	inline int32_t get_bytesUsed_4() const { return ___bytesUsed_4; }
	inline int32_t* get_address_of_bytesUsed_4() { return &___bytesUsed_4; }
	inline void set_bytesUsed_4(int32_t value)
	{
		___bytesUsed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTWINDOW_T1296654655_H
#ifndef FASTENCODERWINDOW_T57591660_H
#define FASTENCODERWINDOW_T57591660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.FastEncoderWindow
struct  FastEncoderWindow_t57591660  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.IO.Compression.FastEncoderWindow::window
	ByteU5BU5D_t4116647657* ___window_0;
	// System.Int32 Mapbox.IO.Compression.FastEncoderWindow::bufPos
	int32_t ___bufPos_1;
	// System.Int32 Mapbox.IO.Compression.FastEncoderWindow::bufEnd
	int32_t ___bufEnd_2;
	// System.UInt16[] Mapbox.IO.Compression.FastEncoderWindow::prev
	UInt16U5BU5D_t3326319531* ___prev_15;
	// System.UInt16[] Mapbox.IO.Compression.FastEncoderWindow::lookup
	UInt16U5BU5D_t3326319531* ___lookup_16;

public:
	inline static int32_t get_offset_of_window_0() { return static_cast<int32_t>(offsetof(FastEncoderWindow_t57591660, ___window_0)); }
	inline ByteU5BU5D_t4116647657* get_window_0() const { return ___window_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_window_0() { return &___window_0; }
	inline void set_window_0(ByteU5BU5D_t4116647657* value)
	{
		___window_0 = value;
		Il2CppCodeGenWriteBarrier((&___window_0), value);
	}

	inline static int32_t get_offset_of_bufPos_1() { return static_cast<int32_t>(offsetof(FastEncoderWindow_t57591660, ___bufPos_1)); }
	inline int32_t get_bufPos_1() const { return ___bufPos_1; }
	inline int32_t* get_address_of_bufPos_1() { return &___bufPos_1; }
	inline void set_bufPos_1(int32_t value)
	{
		___bufPos_1 = value;
	}

	inline static int32_t get_offset_of_bufEnd_2() { return static_cast<int32_t>(offsetof(FastEncoderWindow_t57591660, ___bufEnd_2)); }
	inline int32_t get_bufEnd_2() const { return ___bufEnd_2; }
	inline int32_t* get_address_of_bufEnd_2() { return &___bufEnd_2; }
	inline void set_bufEnd_2(int32_t value)
	{
		___bufEnd_2 = value;
	}

	inline static int32_t get_offset_of_prev_15() { return static_cast<int32_t>(offsetof(FastEncoderWindow_t57591660, ___prev_15)); }
	inline UInt16U5BU5D_t3326319531* get_prev_15() const { return ___prev_15; }
	inline UInt16U5BU5D_t3326319531** get_address_of_prev_15() { return &___prev_15; }
	inline void set_prev_15(UInt16U5BU5D_t3326319531* value)
	{
		___prev_15 = value;
		Il2CppCodeGenWriteBarrier((&___prev_15), value);
	}

	inline static int32_t get_offset_of_lookup_16() { return static_cast<int32_t>(offsetof(FastEncoderWindow_t57591660, ___lookup_16)); }
	inline UInt16U5BU5D_t3326319531* get_lookup_16() const { return ___lookup_16; }
	inline UInt16U5BU5D_t3326319531** get_address_of_lookup_16() { return &___lookup_16; }
	inline void set_lookup_16(UInt16U5BU5D_t3326319531* value)
	{
		___lookup_16 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTENCODERWINDOW_T57591660_H
#ifndef GZIPCONSTANTS_T1076992185_H
#define GZIPCONSTANTS_T1076992185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.GZipConstants
struct  GZipConstants_t1076992185  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPCONSTANTS_T1076992185_H
#ifndef GZIPFORMATTER_T4203637370_H
#define GZIPFORMATTER_T4203637370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.GZipFormatter
struct  GZipFormatter_t4203637370  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.IO.Compression.GZipFormatter::headerBytes
	ByteU5BU5D_t4116647657* ___headerBytes_0;
	// System.UInt32 Mapbox.IO.Compression.GZipFormatter::_crc32
	uint32_t ____crc32_1;
	// System.Int64 Mapbox.IO.Compression.GZipFormatter::_inputStreamSizeModulo
	int64_t ____inputStreamSizeModulo_2;

public:
	inline static int32_t get_offset_of_headerBytes_0() { return static_cast<int32_t>(offsetof(GZipFormatter_t4203637370, ___headerBytes_0)); }
	inline ByteU5BU5D_t4116647657* get_headerBytes_0() const { return ___headerBytes_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_headerBytes_0() { return &___headerBytes_0; }
	inline void set_headerBytes_0(ByteU5BU5D_t4116647657* value)
	{
		___headerBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___headerBytes_0), value);
	}

	inline static int32_t get_offset_of__crc32_1() { return static_cast<int32_t>(offsetof(GZipFormatter_t4203637370, ____crc32_1)); }
	inline uint32_t get__crc32_1() const { return ____crc32_1; }
	inline uint32_t* get_address_of__crc32_1() { return &____crc32_1; }
	inline void set__crc32_1(uint32_t value)
	{
		____crc32_1 = value;
	}

	inline static int32_t get_offset_of__inputStreamSizeModulo_2() { return static_cast<int32_t>(offsetof(GZipFormatter_t4203637370, ____inputStreamSizeModulo_2)); }
	inline int64_t get__inputStreamSizeModulo_2() const { return ____inputStreamSizeModulo_2; }
	inline int64_t* get_address_of__inputStreamSizeModulo_2() { return &____inputStreamSizeModulo_2; }
	inline void set__inputStreamSizeModulo_2(int64_t value)
	{
		____inputStreamSizeModulo_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPFORMATTER_T4203637370_H
#ifndef HUFFMANTREE_T857975559_H
#define HUFFMANTREE_T857975559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.HuffmanTree
struct  HuffmanTree_t857975559  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.IO.Compression.HuffmanTree::tableBits
	int32_t ___tableBits_4;
	// System.Int16[] Mapbox.IO.Compression.HuffmanTree::table
	Int16U5BU5D_t3686840178* ___table_5;
	// System.Int16[] Mapbox.IO.Compression.HuffmanTree::left
	Int16U5BU5D_t3686840178* ___left_6;
	// System.Int16[] Mapbox.IO.Compression.HuffmanTree::right
	Int16U5BU5D_t3686840178* ___right_7;
	// System.Byte[] Mapbox.IO.Compression.HuffmanTree::codeLengthArray
	ByteU5BU5D_t4116647657* ___codeLengthArray_8;
	// System.Int32 Mapbox.IO.Compression.HuffmanTree::tableMask
	int32_t ___tableMask_9;

public:
	inline static int32_t get_offset_of_tableBits_4() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559, ___tableBits_4)); }
	inline int32_t get_tableBits_4() const { return ___tableBits_4; }
	inline int32_t* get_address_of_tableBits_4() { return &___tableBits_4; }
	inline void set_tableBits_4(int32_t value)
	{
		___tableBits_4 = value;
	}

	inline static int32_t get_offset_of_table_5() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559, ___table_5)); }
	inline Int16U5BU5D_t3686840178* get_table_5() const { return ___table_5; }
	inline Int16U5BU5D_t3686840178** get_address_of_table_5() { return &___table_5; }
	inline void set_table_5(Int16U5BU5D_t3686840178* value)
	{
		___table_5 = value;
		Il2CppCodeGenWriteBarrier((&___table_5), value);
	}

	inline static int32_t get_offset_of_left_6() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559, ___left_6)); }
	inline Int16U5BU5D_t3686840178* get_left_6() const { return ___left_6; }
	inline Int16U5BU5D_t3686840178** get_address_of_left_6() { return &___left_6; }
	inline void set_left_6(Int16U5BU5D_t3686840178* value)
	{
		___left_6 = value;
		Il2CppCodeGenWriteBarrier((&___left_6), value);
	}

	inline static int32_t get_offset_of_right_7() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559, ___right_7)); }
	inline Int16U5BU5D_t3686840178* get_right_7() const { return ___right_7; }
	inline Int16U5BU5D_t3686840178** get_address_of_right_7() { return &___right_7; }
	inline void set_right_7(Int16U5BU5D_t3686840178* value)
	{
		___right_7 = value;
		Il2CppCodeGenWriteBarrier((&___right_7), value);
	}

	inline static int32_t get_offset_of_codeLengthArray_8() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559, ___codeLengthArray_8)); }
	inline ByteU5BU5D_t4116647657* get_codeLengthArray_8() const { return ___codeLengthArray_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_codeLengthArray_8() { return &___codeLengthArray_8; }
	inline void set_codeLengthArray_8(ByteU5BU5D_t4116647657* value)
	{
		___codeLengthArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___codeLengthArray_8), value);
	}

	inline static int32_t get_offset_of_tableMask_9() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559, ___tableMask_9)); }
	inline int32_t get_tableMask_9() const { return ___tableMask_9; }
	inline int32_t* get_address_of_tableMask_9() { return &___tableMask_9; }
	inline void set_tableMask_9(int32_t value)
	{
		___tableMask_9 = value;
	}
};

struct HuffmanTree_t857975559_StaticFields
{
public:
	// Mapbox.IO.Compression.HuffmanTree Mapbox.IO.Compression.HuffmanTree::staticLiteralLengthTree
	HuffmanTree_t857975559 * ___staticLiteralLengthTree_10;
	// Mapbox.IO.Compression.HuffmanTree Mapbox.IO.Compression.HuffmanTree::staticDistanceTree
	HuffmanTree_t857975559 * ___staticDistanceTree_11;

public:
	inline static int32_t get_offset_of_staticLiteralLengthTree_10() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559_StaticFields, ___staticLiteralLengthTree_10)); }
	inline HuffmanTree_t857975559 * get_staticLiteralLengthTree_10() const { return ___staticLiteralLengthTree_10; }
	inline HuffmanTree_t857975559 ** get_address_of_staticLiteralLengthTree_10() { return &___staticLiteralLengthTree_10; }
	inline void set_staticLiteralLengthTree_10(HuffmanTree_t857975559 * value)
	{
		___staticLiteralLengthTree_10 = value;
		Il2CppCodeGenWriteBarrier((&___staticLiteralLengthTree_10), value);
	}

	inline static int32_t get_offset_of_staticDistanceTree_11() { return static_cast<int32_t>(offsetof(HuffmanTree_t857975559_StaticFields, ___staticDistanceTree_11)); }
	inline HuffmanTree_t857975559 * get_staticDistanceTree_11() const { return ___staticDistanceTree_11; }
	inline HuffmanTree_t857975559 ** get_address_of_staticDistanceTree_11() { return &___staticDistanceTree_11; }
	inline void set_staticDistanceTree_11(HuffmanTree_t857975559 * value)
	{
		___staticDistanceTree_11 = value;
		Il2CppCodeGenWriteBarrier((&___staticDistanceTree_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUFFMANTREE_T857975559_H
#ifndef INPUTBUFFER_T333709416_H
#define INPUTBUFFER_T333709416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.InputBuffer
struct  InputBuffer_t333709416  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.IO.Compression.InputBuffer::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 Mapbox.IO.Compression.InputBuffer::start
	int32_t ___start_1;
	// System.Int32 Mapbox.IO.Compression.InputBuffer::end
	int32_t ___end_2;
	// System.UInt32 Mapbox.IO.Compression.InputBuffer::bitBuffer
	uint32_t ___bitBuffer_3;
	// System.Int32 Mapbox.IO.Compression.InputBuffer::bitsInBuffer
	int32_t ___bitsInBuffer_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(InputBuffer_t333709416, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(InputBuffer_t333709416, ___start_1)); }
	inline int32_t get_start_1() const { return ___start_1; }
	inline int32_t* get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(int32_t value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(InputBuffer_t333709416, ___end_2)); }
	inline int32_t get_end_2() const { return ___end_2; }
	inline int32_t* get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(int32_t value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_bitBuffer_3() { return static_cast<int32_t>(offsetof(InputBuffer_t333709416, ___bitBuffer_3)); }
	inline uint32_t get_bitBuffer_3() const { return ___bitBuffer_3; }
	inline uint32_t* get_address_of_bitBuffer_3() { return &___bitBuffer_3; }
	inline void set_bitBuffer_3(uint32_t value)
	{
		___bitBuffer_3 = value;
	}

	inline static int32_t get_offset_of_bitsInBuffer_4() { return static_cast<int32_t>(offsetof(InputBuffer_t333709416, ___bitsInBuffer_4)); }
	inline int32_t get_bitsInBuffer_4() const { return ___bitsInBuffer_4; }
	inline int32_t* get_address_of_bitsInBuffer_4() { return &___bitsInBuffer_4; }
	inline void set_bitsInBuffer_4(int32_t value)
	{
		___bitsInBuffer_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUFFER_T333709416_H
#ifndef OUTPUTBUFFER_T1331609326_H
#define OUTPUTBUFFER_T1331609326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.OutputBuffer
struct  OutputBuffer_t1331609326  : public RuntimeObject
{
public:
	// System.Byte[] Mapbox.IO.Compression.OutputBuffer::byteBuffer
	ByteU5BU5D_t4116647657* ___byteBuffer_0;
	// System.Int32 Mapbox.IO.Compression.OutputBuffer::pos
	int32_t ___pos_1;
	// System.UInt32 Mapbox.IO.Compression.OutputBuffer::bitBuf
	uint32_t ___bitBuf_2;
	// System.Int32 Mapbox.IO.Compression.OutputBuffer::bitCount
	int32_t ___bitCount_3;

public:
	inline static int32_t get_offset_of_byteBuffer_0() { return static_cast<int32_t>(offsetof(OutputBuffer_t1331609326, ___byteBuffer_0)); }
	inline ByteU5BU5D_t4116647657* get_byteBuffer_0() const { return ___byteBuffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_byteBuffer_0() { return &___byteBuffer_0; }
	inline void set_byteBuffer_0(ByteU5BU5D_t4116647657* value)
	{
		___byteBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___byteBuffer_0), value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(OutputBuffer_t1331609326, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_bitBuf_2() { return static_cast<int32_t>(offsetof(OutputBuffer_t1331609326, ___bitBuf_2)); }
	inline uint32_t get_bitBuf_2() const { return ___bitBuf_2; }
	inline uint32_t* get_address_of_bitBuf_2() { return &___bitBuf_2; }
	inline void set_bitBuf_2(uint32_t value)
	{
		___bitBuf_2 = value;
	}

	inline static int32_t get_offset_of_bitCount_3() { return static_cast<int32_t>(offsetof(OutputBuffer_t1331609326, ___bitCount_3)); }
	inline int32_t get_bitCount_3() const { return ___bitCount_3; }
	inline int32_t* get_address_of_bitCount_3() { return &___bitCount_3; }
	inline void set_bitCount_3(int32_t value)
	{
		___bitCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTBUFFER_T1331609326_H
#ifndef OUTREC_T316877671_H
#define OUTREC_T316877671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec
struct  OutRec_t316877671  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec::Idx
	int32_t ___Idx_0;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec::IsHole
	bool ___IsHole_1;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec::IsOpen
	bool ___IsOpen_2;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec::FirstLeft
	OutRec_t316877671 * ___FirstLeft_3;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec::Pts
	OutPt_t2591102706 * ___Pts_4;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec::BottomPt
	OutPt_t2591102706 * ___BottomPt_5;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutRec::PolyNode
	PolyNode_t1300984468 * ___PolyNode_6;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(OutRec_t316877671, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_IsHole_1() { return static_cast<int32_t>(offsetof(OutRec_t316877671, ___IsHole_1)); }
	inline bool get_IsHole_1() const { return ___IsHole_1; }
	inline bool* get_address_of_IsHole_1() { return &___IsHole_1; }
	inline void set_IsHole_1(bool value)
	{
		___IsHole_1 = value;
	}

	inline static int32_t get_offset_of_IsOpen_2() { return static_cast<int32_t>(offsetof(OutRec_t316877671, ___IsOpen_2)); }
	inline bool get_IsOpen_2() const { return ___IsOpen_2; }
	inline bool* get_address_of_IsOpen_2() { return &___IsOpen_2; }
	inline void set_IsOpen_2(bool value)
	{
		___IsOpen_2 = value;
	}

	inline static int32_t get_offset_of_FirstLeft_3() { return static_cast<int32_t>(offsetof(OutRec_t316877671, ___FirstLeft_3)); }
	inline OutRec_t316877671 * get_FirstLeft_3() const { return ___FirstLeft_3; }
	inline OutRec_t316877671 ** get_address_of_FirstLeft_3() { return &___FirstLeft_3; }
	inline void set_FirstLeft_3(OutRec_t316877671 * value)
	{
		___FirstLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___FirstLeft_3), value);
	}

	inline static int32_t get_offset_of_Pts_4() { return static_cast<int32_t>(offsetof(OutRec_t316877671, ___Pts_4)); }
	inline OutPt_t2591102706 * get_Pts_4() const { return ___Pts_4; }
	inline OutPt_t2591102706 ** get_address_of_Pts_4() { return &___Pts_4; }
	inline void set_Pts_4(OutPt_t2591102706 * value)
	{
		___Pts_4 = value;
		Il2CppCodeGenWriteBarrier((&___Pts_4), value);
	}

	inline static int32_t get_offset_of_BottomPt_5() { return static_cast<int32_t>(offsetof(OutRec_t316877671, ___BottomPt_5)); }
	inline OutPt_t2591102706 * get_BottomPt_5() const { return ___BottomPt_5; }
	inline OutPt_t2591102706 ** get_address_of_BottomPt_5() { return &___BottomPt_5; }
	inline void set_BottomPt_5(OutPt_t2591102706 * value)
	{
		___BottomPt_5 = value;
		Il2CppCodeGenWriteBarrier((&___BottomPt_5), value);
	}

	inline static int32_t get_offset_of_PolyNode_6() { return static_cast<int32_t>(offsetof(OutRec_t316877671, ___PolyNode_6)); }
	inline PolyNode_t1300984468 * get_PolyNode_6() const { return ___PolyNode_6; }
	inline PolyNode_t1300984468 ** get_address_of_PolyNode_6() { return &___PolyNode_6; }
	inline void set_PolyNode_6(PolyNode_t1300984468 * value)
	{
		___PolyNode_6 = value;
		Il2CppCodeGenWriteBarrier((&___PolyNode_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTREC_T316877671_H
#ifndef LOCALMINIMA_T86068969_H
#define LOCALMINIMA_T86068969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima
struct  LocalMinima_t86068969  : public RuntimeObject
{
public:
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima::Y
	int64_t ___Y_0;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima::LeftBound
	TEdge_t1694054893 * ___LeftBound_1;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima::RightBound
	TEdge_t1694054893 * ___RightBound_2;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/LocalMinima::Next
	LocalMinima_t86068969 * ___Next_3;

public:
	inline static int32_t get_offset_of_Y_0() { return static_cast<int32_t>(offsetof(LocalMinima_t86068969, ___Y_0)); }
	inline int64_t get_Y_0() const { return ___Y_0; }
	inline int64_t* get_address_of_Y_0() { return &___Y_0; }
	inline void set_Y_0(int64_t value)
	{
		___Y_0 = value;
	}

	inline static int32_t get_offset_of_LeftBound_1() { return static_cast<int32_t>(offsetof(LocalMinima_t86068969, ___LeftBound_1)); }
	inline TEdge_t1694054893 * get_LeftBound_1() const { return ___LeftBound_1; }
	inline TEdge_t1694054893 ** get_address_of_LeftBound_1() { return &___LeftBound_1; }
	inline void set_LeftBound_1(TEdge_t1694054893 * value)
	{
		___LeftBound_1 = value;
		Il2CppCodeGenWriteBarrier((&___LeftBound_1), value);
	}

	inline static int32_t get_offset_of_RightBound_2() { return static_cast<int32_t>(offsetof(LocalMinima_t86068969, ___RightBound_2)); }
	inline TEdge_t1694054893 * get_RightBound_2() const { return ___RightBound_2; }
	inline TEdge_t1694054893 ** get_address_of_RightBound_2() { return &___RightBound_2; }
	inline void set_RightBound_2(TEdge_t1694054893 * value)
	{
		___RightBound_2 = value;
		Il2CppCodeGenWriteBarrier((&___RightBound_2), value);
	}

	inline static int32_t get_offset_of_Next_3() { return static_cast<int32_t>(offsetof(LocalMinima_t86068969, ___Next_3)); }
	inline LocalMinima_t86068969 * get_Next_3() const { return ___Next_3; }
	inline LocalMinima_t86068969 ** get_address_of_Next_3() { return &___Next_3; }
	inline void set_Next_3(LocalMinima_t86068969 * value)
	{
		___Next_3 = value;
		Il2CppCodeGenWriteBarrier((&___Next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALMINIMA_T86068969_H
#ifndef INTERNALCLIPPER_T4127247543_H
#define INTERNALCLIPPER_T4127247543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper
struct  InternalClipper_t4127247543  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCLIPPER_T4127247543_H
#ifndef MYINTERSECTNODESORT_T682547759_H
#define MYINTERSECTNODESORT_T682547759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/MyIntersectNodeSort
struct  MyIntersectNodeSort_t682547759  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYINTERSECTNODESORT_T682547759_H
#ifndef U3CGEOMETRYASWGS84U3EC__ANONSTOREY0_T3901700684_H
#define U3CGEOMETRYASWGS84U3EC__ANONSTOREY0_T3901700684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.ExtensionMethods.VectorTileFeatureExtensions/<GeometryAsWgs84>c__AnonStorey0
struct  U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684  : public RuntimeObject
{
public:
	// System.UInt64 Mapbox.VectorTile.ExtensionMethods.VectorTileFeatureExtensions/<GeometryAsWgs84>c__AnonStorey0::zoom
	uint64_t ___zoom_0;
	// System.UInt64 Mapbox.VectorTile.ExtensionMethods.VectorTileFeatureExtensions/<GeometryAsWgs84>c__AnonStorey0::tileColumn
	uint64_t ___tileColumn_1;
	// System.UInt64 Mapbox.VectorTile.ExtensionMethods.VectorTileFeatureExtensions/<GeometryAsWgs84>c__AnonStorey0::tileRow
	uint64_t ___tileRow_2;
	// Mapbox.VectorTile.VectorTileFeature Mapbox.VectorTile.ExtensionMethods.VectorTileFeatureExtensions/<GeometryAsWgs84>c__AnonStorey0::feature
	VectorTileFeature_t4093669591 * ___feature_3;

public:
	inline static int32_t get_offset_of_zoom_0() { return static_cast<int32_t>(offsetof(U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684, ___zoom_0)); }
	inline uint64_t get_zoom_0() const { return ___zoom_0; }
	inline uint64_t* get_address_of_zoom_0() { return &___zoom_0; }
	inline void set_zoom_0(uint64_t value)
	{
		___zoom_0 = value;
	}

	inline static int32_t get_offset_of_tileColumn_1() { return static_cast<int32_t>(offsetof(U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684, ___tileColumn_1)); }
	inline uint64_t get_tileColumn_1() const { return ___tileColumn_1; }
	inline uint64_t* get_address_of_tileColumn_1() { return &___tileColumn_1; }
	inline void set_tileColumn_1(uint64_t value)
	{
		___tileColumn_1 = value;
	}

	inline static int32_t get_offset_of_tileRow_2() { return static_cast<int32_t>(offsetof(U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684, ___tileRow_2)); }
	inline uint64_t get_tileRow_2() const { return ___tileRow_2; }
	inline uint64_t* get_address_of_tileRow_2() { return &___tileRow_2; }
	inline void set_tileRow_2(uint64_t value)
	{
		___tileRow_2 = value;
	}

	inline static int32_t get_offset_of_feature_3() { return static_cast<int32_t>(offsetof(U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684, ___feature_3)); }
	inline VectorTileFeature_t4093669591 * get_feature_3() const { return ___feature_3; }
	inline VectorTileFeature_t4093669591 ** get_address_of_feature_3() { return &___feature_3; }
	inline void set_feature_3(VectorTileFeature_t4093669591 * value)
	{
		___feature_3 = value;
		Il2CppCodeGenWriteBarrier((&___feature_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGEOMETRYASWGS84U3EC__ANONSTOREY0_T3901700684_H
#ifndef VECTORTILEFEATUREEXTENSIONS_T4023769631_H
#define VECTORTILEFEATUREEXTENSIONS_T4023769631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.ExtensionMethods.VectorTileFeatureExtensions
struct  VectorTileFeatureExtensions_t4023769631  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILEFEATUREEXTENSIONS_T4023769631_H
#ifndef VECTORTILEEXTENSIONS_T4243590528_H
#define VECTORTILEEXTENSIONS_T4243590528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.ExtensionMethods.VectorTileExtensions
struct  VectorTileExtensions_t4243590528  : public RuntimeObject
{
public:

public:
};

struct VectorTileExtensions_t4243590528_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.LatLng>,System.Collections.Generic.IEnumerable`1<Mapbox.VectorTile.Geometry.LatLng>> Mapbox.VectorTile.ExtensionMethods.VectorTileExtensions::<>f__am$cache0
	Func_2_t1001585409 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<Mapbox.VectorTile.Geometry.LatLng,System.String> Mapbox.VectorTile.ExtensionMethods.VectorTileExtensions::<>f__am$cache1
	Func_2_t3663939823 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<Mapbox.VectorTile.Geometry.LatLng,System.String> Mapbox.VectorTile.ExtensionMethods.VectorTileExtensions::<>f__am$cache2
	Func_2_t3663939823 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<Mapbox.VectorTile.Geometry.LatLng,System.String> Mapbox.VectorTile.ExtensionMethods.VectorTileExtensions::<>f__am$cache3
	Func_2_t3663939823 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<Mapbox.VectorTile.Geometry.LatLng,System.String> Mapbox.VectorTile.ExtensionMethods.VectorTileExtensions::<>f__am$cache4
	Func_2_t3663939823 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<Mapbox.VectorTile.Geometry.LatLng,System.String> Mapbox.VectorTile.ExtensionMethods.VectorTileExtensions::<>f__am$cache5
	Func_2_t3663939823 * ___U3CU3Ef__amU24cache5_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(VectorTileExtensions_t4243590528_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1001585409 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1001585409 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1001585409 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(VectorTileExtensions_t4243590528_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t3663939823 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t3663939823 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t3663939823 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(VectorTileExtensions_t4243590528_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t3663939823 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t3663939823 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t3663939823 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(VectorTileExtensions_t4243590528_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t3663939823 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t3663939823 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t3663939823 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(VectorTileExtensions_t4243590528_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t3663939823 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t3663939823 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t3663939823 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(VectorTileExtensions_t4243590528_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t3663939823 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t3663939823 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t3663939823 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILEEXTENSIONS_T4243590528_H
#ifndef ENUMEXTENSIONS_T2644584491_H
#define ENUMEXTENSIONS_T2644584491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.ExtensionMethods.EnumExtensions
struct  EnumExtensions_t2644584491  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMEXTENSIONS_T2644584491_H
#ifndef MAXIMA_T4278896992_H
#define MAXIMA_T4278896992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima
struct  Maxima_t4278896992  : public RuntimeObject
{
public:
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima::X
	int64_t ___X_0;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima::Next
	Maxima_t4278896992 * ___Next_1;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima::Prev
	Maxima_t4278896992 * ___Prev_2;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(Maxima_t4278896992, ___X_0)); }
	inline int64_t get_X_0() const { return ___X_0; }
	inline int64_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int64_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(Maxima_t4278896992, ___Next_1)); }
	inline Maxima_t4278896992 * get_Next_1() const { return ___Next_1; }
	inline Maxima_t4278896992 ** get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(Maxima_t4278896992 * value)
	{
		___Next_1 = value;
		Il2CppCodeGenWriteBarrier((&___Next_1), value);
	}

	inline static int32_t get_offset_of_Prev_2() { return static_cast<int32_t>(offsetof(Maxima_t4278896992, ___Prev_2)); }
	inline Maxima_t4278896992 * get_Prev_2() const { return ___Prev_2; }
	inline Maxima_t4278896992 ** get_address_of_Prev_2() { return &___Prev_2; }
	inline void set_Prev_2(Maxima_t4278896992 * value)
	{
		___Prev_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prev_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXIMA_T4278896992_H
#ifndef SCANBEAM_T3952834741_H
#define SCANBEAM_T3952834741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Scanbeam
struct  Scanbeam_t3952834741  : public RuntimeObject
{
public:
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Scanbeam::Y
	int64_t ___Y_0;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Scanbeam Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Scanbeam::Next
	Scanbeam_t3952834741 * ___Next_1;

public:
	inline static int32_t get_offset_of_Y_0() { return static_cast<int32_t>(offsetof(Scanbeam_t3952834741, ___Y_0)); }
	inline int64_t get_Y_0() const { return ___Y_0; }
	inline int64_t* get_address_of_Y_0() { return &___Y_0; }
	inline void set_Y_0(int64_t value)
	{
		___Y_0 = value;
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(Scanbeam_t3952834741, ___Next_1)); }
	inline Scanbeam_t3952834741 * get_Next_1() const { return ___Next_1; }
	inline Scanbeam_t3952834741 ** get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(Scanbeam_t3952834741 * value)
	{
		___Next_1 = value;
		Il2CppCodeGenWriteBarrier((&___Next_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANBEAM_T3952834741_H
#ifndef DOUBLEPOINT_T1607927371_H
#define DOUBLEPOINT_T1607927371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/DoublePoint
struct  DoublePoint_t1607927371 
{
public:
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/DoublePoint::X
	double ___X_0;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/DoublePoint::Y
	double ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(DoublePoint_t1607927371, ___X_0)); }
	inline double get_X_0() const { return ___X_0; }
	inline double* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(double value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(DoublePoint_t1607927371, ___Y_1)); }
	inline double get_Y_1() const { return ___Y_1; }
	inline double* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(double value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEPOINT_T1607927371_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTRECT_T752847524_H
#define INTRECT_T752847524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntRect
struct  IntRect_t752847524 
{
public:
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntRect::left
	int64_t ___left_0;
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntRect::top
	int64_t ___top_1;
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntRect::right
	int64_t ___right_2;
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntRect::bottom
	int64_t ___bottom_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(IntRect_t752847524, ___left_0)); }
	inline int64_t get_left_0() const { return ___left_0; }
	inline int64_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int64_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_top_1() { return static_cast<int32_t>(offsetof(IntRect_t752847524, ___top_1)); }
	inline int64_t get_top_1() const { return ___top_1; }
	inline int64_t* get_address_of_top_1() { return &___top_1; }
	inline void set_top_1(int64_t value)
	{
		___top_1 = value;
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(IntRect_t752847524, ___right_2)); }
	inline int64_t get_right_2() const { return ___right_2; }
	inline int64_t* get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(int64_t value)
	{
		___right_2 = value;
	}

	inline static int32_t get_offset_of_bottom_3() { return static_cast<int32_t>(offsetof(IntRect_t752847524, ___bottom_3)); }
	inline int64_t get_bottom_3() const { return ___bottom_3; }
	inline int64_t* get_address_of_bottom_3() { return &___bottom_3; }
	inline void set_bottom_3(int64_t value)
	{
		___bottom_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTRECT_T752847524_H
#ifndef INTPOINT_T2327573135_H
#define INTPOINT_T2327573135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint
struct  IntPoint_t2327573135 
{
public:
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint::X
	int64_t ___X_0;
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint::Y
	int64_t ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(IntPoint_t2327573135, ___X_0)); }
	inline int64_t get_X_0() const { return ___X_0; }
	inline int64_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int64_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(IntPoint_t2327573135, ___Y_1)); }
	inline int64_t get_Y_1() const { return ___Y_1; }
	inline int64_t* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(int64_t value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPOINT_T2327573135_H
#ifndef BUFFERSTATE_T2134702710_H
#define BUFFERSTATE_T2134702710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.OutputBuffer/BufferState
struct  BufferState_t2134702710 
{
public:
	// System.Int32 Mapbox.IO.Compression.OutputBuffer/BufferState::pos
	int32_t ___pos_0;
	// System.UInt32 Mapbox.IO.Compression.OutputBuffer/BufferState::bitBuf
	uint32_t ___bitBuf_1;
	// System.Int32 Mapbox.IO.Compression.OutputBuffer/BufferState::bitCount
	int32_t ___bitCount_2;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(BufferState_t2134702710, ___pos_0)); }
	inline int32_t get_pos_0() const { return ___pos_0; }
	inline int32_t* get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(int32_t value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_bitBuf_1() { return static_cast<int32_t>(offsetof(BufferState_t2134702710, ___bitBuf_1)); }
	inline uint32_t get_bitBuf_1() const { return ___bitBuf_1; }
	inline uint32_t* get_address_of_bitBuf_1() { return &___bitBuf_1; }
	inline void set_bitBuf_1(uint32_t value)
	{
		___bitBuf_1 = value;
	}

	inline static int32_t get_offset_of_bitCount_2() { return static_cast<int32_t>(offsetof(BufferState_t2134702710, ___bitCount_2)); }
	inline int32_t get_bitCount_2() const { return ___bitCount_2; }
	inline int32_t* get_address_of_bitCount_2() { return &___bitCount_2; }
	inline void set_bitCount_2(int32_t value)
	{
		___bitCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERSTATE_T2134702710_H
#ifndef GZIPSTREAM_T1509501570_H
#define GZIPSTREAM_T1509501570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.GZipStream
struct  GZipStream_t1509501570  : public Stream_t1273022909
{
public:
	// Mapbox.IO.Compression.DeflateStream Mapbox.IO.Compression.GZipStream::deflateStream
	DeflateStream_t2796728099 * ___deflateStream_1;

public:
	inline static int32_t get_offset_of_deflateStream_1() { return static_cast<int32_t>(offsetof(GZipStream_t1509501570, ___deflateStream_1)); }
	inline DeflateStream_t2796728099 * get_deflateStream_1() const { return ___deflateStream_1; }
	inline DeflateStream_t2796728099 ** get_address_of_deflateStream_1() { return &___deflateStream_1; }
	inline void set_deflateStream_1(DeflateStream_t2796728099 * value)
	{
		___deflateStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___deflateStream_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPSTREAM_T1509501570_H
#ifndef INPUTSTATE_T2211247823_H
#define INPUTSTATE_T2211247823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflateInput/InputState
struct  InputState_t2211247823 
{
public:
	// System.Int32 Mapbox.IO.Compression.DeflateInput/InputState::count
	int32_t ___count_0;
	// System.Int32 Mapbox.IO.Compression.DeflateInput/InputState::startIndex
	int32_t ___startIndex_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(InputState_t2211247823, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_startIndex_1() { return static_cast<int32_t>(offsetof(InputState_t2211247823, ___startIndex_1)); }
	inline int32_t get_startIndex_1() const { return ___startIndex_1; }
	inline int32_t* get_address_of_startIndex_1() { return &___startIndex_1; }
	inline void set_startIndex_1(int32_t value)
	{
		___startIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_T2211247823_H
#ifndef COMPRESSIONTRACINGSWITCH_T2383688933_H
#define COMPRESSIONTRACINGSWITCH_T2383688933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.CompressionTracingSwitch
struct  CompressionTracingSwitch_t2383688933  : public Switch_t4228844028
{
public:

public:
};

struct CompressionTracingSwitch_t2383688933_StaticFields
{
public:
	// Mapbox.IO.Compression.CompressionTracingSwitch Mapbox.IO.Compression.CompressionTracingSwitch::tracingSwitch
	CompressionTracingSwitch_t2383688933 * ___tracingSwitch_7;

public:
	inline static int32_t get_offset_of_tracingSwitch_7() { return static_cast<int32_t>(offsetof(CompressionTracingSwitch_t2383688933_StaticFields, ___tracingSwitch_7)); }
	inline CompressionTracingSwitch_t2383688933 * get_tracingSwitch_7() const { return ___tracingSwitch_7; }
	inline CompressionTracingSwitch_t2383688933 ** get_address_of_tracingSwitch_7() { return &___tracingSwitch_7; }
	inline void set_tracingSwitch_7(CompressionTracingSwitch_t2383688933 * value)
	{
		___tracingSwitch_7 = value;
		Il2CppCodeGenWriteBarrier((&___tracingSwitch_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONTRACINGSWITCH_T2383688933_H
#ifndef INT128_T2615162842_H
#define INT128_T2615162842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Int128
struct  Int128_t2615162842 
{
public:
	// System.Int64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Int128::hi
	int64_t ___hi_0;
	// System.UInt64 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Int128::lo
	uint64_t ___lo_1;

public:
	inline static int32_t get_offset_of_hi_0() { return static_cast<int32_t>(offsetof(Int128_t2615162842, ___hi_0)); }
	inline int64_t get_hi_0() const { return ___hi_0; }
	inline int64_t* get_address_of_hi_0() { return &___hi_0; }
	inline void set_hi_0(int64_t value)
	{
		___hi_0 = value;
	}

	inline static int32_t get_offset_of_lo_1() { return static_cast<int32_t>(offsetof(Int128_t2615162842, ___lo_1)); }
	inline uint64_t get_lo_1() const { return ___lo_1; }
	inline uint64_t* get_address_of_lo_1() { return &___lo_1; }
	inline void set_lo_1(uint64_t value)
	{
		___lo_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT128_T2615162842_H
#ifndef VECTOR2D_T1865246568_H
#define VECTOR2D_T1865246568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Vector2d
struct  Vector2d_t1865246568 
{
public:
	// System.Double Mapbox.Utils.Vector2d::x
	double ___x_1;
	// System.Double Mapbox.Utils.Vector2d::y
	double ___y_2;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___x_1)); }
	inline double get_x_1() const { return ___x_1; }
	inline double* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(double value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector2d_t1865246568, ___y_2)); }
	inline double get_y_2() const { return ___y_2; }
	inline double* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(double value)
	{
		___y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2D_T1865246568_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef CLIPPEREXCEPTION_T3118674656_H
#define CLIPPEREXCEPTION_T3118674656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperException
struct  ClipperException_t3118674656  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPEREXCEPTION_T3118674656_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef LATLNG_T1304626312_H
#define LATLNG_T1304626312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.LatLng
struct  LatLng_t1304626312 
{
public:
	union
	{
		struct
		{
			// System.Double Mapbox.VectorTile.Geometry.LatLng::<Lat>k__BackingField
			double ___U3CLatU3Ek__BackingField_0;
			// System.Double Mapbox.VectorTile.Geometry.LatLng::<Lng>k__BackingField
			double ___U3CLngU3Ek__BackingField_1;
		};
		uint8_t LatLng_t1304626312__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CLatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LatLng_t1304626312, ___U3CLatU3Ek__BackingField_0)); }
	inline double get_U3CLatU3Ek__BackingField_0() const { return ___U3CLatU3Ek__BackingField_0; }
	inline double* get_address_of_U3CLatU3Ek__BackingField_0() { return &___U3CLatU3Ek__BackingField_0; }
	inline void set_U3CLatU3Ek__BackingField_0(double value)
	{
		___U3CLatU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLngU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LatLng_t1304626312, ___U3CLngU3Ek__BackingField_1)); }
	inline double get_U3CLngU3Ek__BackingField_1() const { return ___U3CLngU3Ek__BackingField_1; }
	inline double* get_address_of_U3CLngU3Ek__BackingField_1() { return &___U3CLngU3Ek__BackingField_1; }
	inline void set_U3CLngU3Ek__BackingField_1(double value)
	{
		___U3CLngU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATLNG_T1304626312_H
#ifndef NULLABLE_1_T4282624060_H
#define NULLABLE_1_T4282624060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.UInt32>
struct  Nullable_1_t4282624060 
{
public:
	// T System.Nullable`1::value
	uint32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4282624060, ___value_0)); }
	inline uint32_t get_value_0() const { return ___value_0; }
	inline uint32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4282624060, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4282624060_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef MATHD_T279629051_H
#define MATHD_T279629051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.Mathd
struct  Mathd_t279629051 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Mathd_t279629051__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHD_T279629051_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef MATCHSTATE_T635901724_H
#define MATCHSTATE_T635901724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatchState
struct  MatchState_t635901724 
{
public:
	// System.Int32 MatchState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MatchState_t635901724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHSTATE_T635901724_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef GZIPHEADERSTATE_T2287451846_H
#define GZIPHEADERSTATE_T2287451846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.GZipDecoder/GzipHeaderState
struct  GzipHeaderState_t2287451846 
{
public:
	// System.Int32 Mapbox.IO.Compression.GZipDecoder/GzipHeaderState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GzipHeaderState_t2287451846, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPHEADERSTATE_T2287451846_H
#ifndef GZIPOPTIONALHEADERFLAGS_T2076442620_H
#define GZIPOPTIONALHEADERFLAGS_T2076442620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.GZipDecoder/GZipOptionalHeaderFlags
struct  GZipOptionalHeaderFlags_t2076442620 
{
public:
	// System.Int32 Mapbox.IO.Compression.GZipDecoder/GZipOptionalHeaderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GZipOptionalHeaderFlags_t2076442620, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPOPTIONALHEADERFLAGS_T2076442620_H
#ifndef INVALIDDATAEXCEPTION_T4045251031_H
#define INVALIDDATAEXCEPTION_T4045251031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.InvalidDataException
struct  InvalidDataException_t4045251031  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDDATAEXCEPTION_T4045251031_H
#ifndef RECTD_T151583371_H
#define RECTD_T151583371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Utils.RectD
struct  RectD_t151583371 
{
public:
	union
	{
		struct
		{
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Min>k__BackingField
			Vector2d_t1865246568  ___U3CMinU3Ek__BackingField_0;
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Max>k__BackingField
			Vector2d_t1865246568  ___U3CMaxU3Ek__BackingField_1;
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Size>k__BackingField
			Vector2d_t1865246568  ___U3CSizeU3Ek__BackingField_2;
			// Mapbox.Utils.Vector2d Mapbox.Utils.RectD::<Center>k__BackingField
			Vector2d_t1865246568  ___U3CCenterU3Ek__BackingField_3;
		};
		uint8_t RectD_t151583371__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CMinU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CMinU3Ek__BackingField_0)); }
	inline Vector2d_t1865246568  get_U3CMinU3Ek__BackingField_0() const { return ___U3CMinU3Ek__BackingField_0; }
	inline Vector2d_t1865246568 * get_address_of_U3CMinU3Ek__BackingField_0() { return &___U3CMinU3Ek__BackingField_0; }
	inline void set_U3CMinU3Ek__BackingField_0(Vector2d_t1865246568  value)
	{
		___U3CMinU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CMaxU3Ek__BackingField_1)); }
	inline Vector2d_t1865246568  get_U3CMaxU3Ek__BackingField_1() const { return ___U3CMaxU3Ek__BackingField_1; }
	inline Vector2d_t1865246568 * get_address_of_U3CMaxU3Ek__BackingField_1() { return &___U3CMaxU3Ek__BackingField_1; }
	inline void set_U3CMaxU3Ek__BackingField_1(Vector2d_t1865246568  value)
	{
		___U3CMaxU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CSizeU3Ek__BackingField_2)); }
	inline Vector2d_t1865246568  get_U3CSizeU3Ek__BackingField_2() const { return ___U3CSizeU3Ek__BackingField_2; }
	inline Vector2d_t1865246568 * get_address_of_U3CSizeU3Ek__BackingField_2() { return &___U3CSizeU3Ek__BackingField_2; }
	inline void set_U3CSizeU3Ek__BackingField_2(Vector2d_t1865246568  value)
	{
		___U3CSizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RectD_t151583371, ___U3CCenterU3Ek__BackingField_3)); }
	inline Vector2d_t1865246568  get_U3CCenterU3Ek__BackingField_3() const { return ___U3CCenterU3Ek__BackingField_3; }
	inline Vector2d_t1865246568 * get_address_of_U3CCenterU3Ek__BackingField_3() { return &___U3CCenterU3Ek__BackingField_3; }
	inline void set_U3CCenterU3Ek__BackingField_3(Vector2d_t1865246568  value)
	{
		___U3CCenterU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTD_T151583371_H
#ifndef INFLATERSTATE_T2157501130_H
#define INFLATERSTATE_T2157501130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.InflaterState
struct  InflaterState_t2157501130 
{
public:
	// System.Int32 Mapbox.IO.Compression.InflaterState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InflaterState_t2157501130, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERSTATE_T2157501130_H
#ifndef CLIPTYPE_T1616702040_H
#define CLIPTYPE_T1616702040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipType
struct  ClipType_t1616702040 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClipType_t1616702040, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPTYPE_T1616702040_H
#ifndef FEATURETYPE_T2360609914_H
#define FEATURETYPE_T2360609914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Contants.FeatureType
struct  FeatureType_t2360609914 
{
public:
	// System.Int32 Mapbox.VectorTile.Contants.FeatureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FeatureType_t2360609914, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURETYPE_T2360609914_H
#ifndef VALUETYPE_T2776630785_H
#define VALUETYPE_T2776630785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Contants.ValueType
struct  ValueType_t2776630785 
{
public:
	// System.Int32 Mapbox.VectorTile.Contants.ValueType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValueType_t2776630785, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUETYPE_T2776630785_H
#ifndef INTERSECTNODE_T3379514219_H
#define INTERSECTNODE_T3379514219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode
struct  IntersectNode_t3379514219  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode::Edge1
	TEdge_t1694054893 * ___Edge1_0;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode::Edge2
	TEdge_t1694054893 * ___Edge2_1;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode::Pt
	IntPoint_t2327573135  ___Pt_2;

public:
	inline static int32_t get_offset_of_Edge1_0() { return static_cast<int32_t>(offsetof(IntersectNode_t3379514219, ___Edge1_0)); }
	inline TEdge_t1694054893 * get_Edge1_0() const { return ___Edge1_0; }
	inline TEdge_t1694054893 ** get_address_of_Edge1_0() { return &___Edge1_0; }
	inline void set_Edge1_0(TEdge_t1694054893 * value)
	{
		___Edge1_0 = value;
		Il2CppCodeGenWriteBarrier((&___Edge1_0), value);
	}

	inline static int32_t get_offset_of_Edge2_1() { return static_cast<int32_t>(offsetof(IntersectNode_t3379514219, ___Edge2_1)); }
	inline TEdge_t1694054893 * get_Edge2_1() const { return ___Edge2_1; }
	inline TEdge_t1694054893 ** get_address_of_Edge2_1() { return &___Edge2_1; }
	inline void set_Edge2_1(TEdge_t1694054893 * value)
	{
		___Edge2_1 = value;
		Il2CppCodeGenWriteBarrier((&___Edge2_1), value);
	}

	inline static int32_t get_offset_of_Pt_2() { return static_cast<int32_t>(offsetof(IntersectNode_t3379514219, ___Pt_2)); }
	inline IntPoint_t2327573135  get_Pt_2() const { return ___Pt_2; }
	inline IntPoint_t2327573135 * get_address_of_Pt_2() { return &___Pt_2; }
	inline void set_Pt_2(IntPoint_t2327573135  value)
	{
		___Pt_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSECTNODE_T3379514219_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DIRECTION_T4237952965_H
#define DIRECTION_T4237952965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Direction
struct  Direction_t4237952965 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t4237952965, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T4237952965_H
#ifndef EDGESIDE_T2739901735_H
#define EDGESIDE_T2739901735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/EdgeSide
struct  EdgeSide_t2739901735 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/EdgeSide::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EdgeSide_t2739901735, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGESIDE_T2739901735_H
#ifndef BLOCKTYPE_T3928626740_H
#define BLOCKTYPE_T3928626740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.BlockType
struct  BlockType_t3928626740 
{
public:
	// System.Int32 Mapbox.IO.Compression.BlockType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockType_t3928626740, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKTYPE_T3928626740_H
#ifndef LAYERTYPE_T1746409905_H
#define LAYERTYPE_T1746409905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Contants.LayerType
struct  LayerType_t1746409905 
{
public:
	// System.Int32 Mapbox.VectorTile.Contants.LayerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayerType_t1746409905, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERTYPE_T1746409905_H
#ifndef OUTPT_T2591102706_H
#define OUTPT_T2591102706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt
struct  OutPt_t2591102706  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt::Idx
	int32_t ___Idx_0;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt::Pt
	IntPoint_t2327573135  ___Pt_1;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt::Next
	OutPt_t2591102706 * ___Next_2;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt::Prev
	OutPt_t2591102706 * ___Prev_3;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(OutPt_t2591102706, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_Pt_1() { return static_cast<int32_t>(offsetof(OutPt_t2591102706, ___Pt_1)); }
	inline IntPoint_t2327573135  get_Pt_1() const { return ___Pt_1; }
	inline IntPoint_t2327573135 * get_address_of_Pt_1() { return &___Pt_1; }
	inline void set_Pt_1(IntPoint_t2327573135  value)
	{
		___Pt_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(OutPt_t2591102706, ___Next_2)); }
	inline OutPt_t2591102706 * get_Next_2() const { return ___Next_2; }
	inline OutPt_t2591102706 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(OutPt_t2591102706 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}

	inline static int32_t get_offset_of_Prev_3() { return static_cast<int32_t>(offsetof(OutPt_t2591102706, ___Prev_3)); }
	inline OutPt_t2591102706 * get_Prev_3() const { return ___Prev_3; }
	inline OutPt_t2591102706 ** get_address_of_Prev_3() { return &___Prev_3; }
	inline void set_Prev_3(OutPt_t2591102706 * value)
	{
		___Prev_3 = value;
		Il2CppCodeGenWriteBarrier((&___Prev_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPT_T2591102706_H
#ifndef JOIN_T2349011362_H
#define JOIN_T2349011362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Join
struct  Join_t2349011362  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Join::OutPt1
	OutPt_t2591102706 * ___OutPt1_0;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/OutPt Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Join::OutPt2
	OutPt_t2591102706 * ___OutPt2_1;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Join::OffPt
	IntPoint_t2327573135  ___OffPt_2;

public:
	inline static int32_t get_offset_of_OutPt1_0() { return static_cast<int32_t>(offsetof(Join_t2349011362, ___OutPt1_0)); }
	inline OutPt_t2591102706 * get_OutPt1_0() const { return ___OutPt1_0; }
	inline OutPt_t2591102706 ** get_address_of_OutPt1_0() { return &___OutPt1_0; }
	inline void set_OutPt1_0(OutPt_t2591102706 * value)
	{
		___OutPt1_0 = value;
		Il2CppCodeGenWriteBarrier((&___OutPt1_0), value);
	}

	inline static int32_t get_offset_of_OutPt2_1() { return static_cast<int32_t>(offsetof(Join_t2349011362, ___OutPt2_1)); }
	inline OutPt_t2591102706 * get_OutPt2_1() const { return ___OutPt2_1; }
	inline OutPt_t2591102706 ** get_address_of_OutPt2_1() { return &___OutPt2_1; }
	inline void set_OutPt2_1(OutPt_t2591102706 * value)
	{
		___OutPt2_1 = value;
		Il2CppCodeGenWriteBarrier((&___OutPt2_1), value);
	}

	inline static int32_t get_offset_of_OffPt_2() { return static_cast<int32_t>(offsetof(Join_t2349011362, ___OffPt_2)); }
	inline IntPoint_t2327573135  get_OffPt_2() const { return ___OffPt_2; }
	inline IntPoint_t2327573135 * get_address_of_OffPt_2() { return &___OffPt_2; }
	inline void set_OffPt_2(IntPoint_t2327573135  value)
	{
		___OffPt_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOIN_T2349011362_H
#ifndef NODETYPE_T363087472_H
#define NODETYPE_T363087472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper/NodeType
struct  NodeType_t363087472 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper/NodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NodeType_t363087472, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODETYPE_T363087472_H
#ifndef CLIPPEROFFSET_T3668738110_H
#define CLIPPEROFFSET_T3668738110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset
struct  ClipperOffset_t3668738110  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint>> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_destPolys
	List_1_t976755323 * ___m_destPolys_0;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_srcPoly
	List_1_t3799647877 * ___m_srcPoly_1;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_destPoly
	List_1_t3799647877 * ___m_destPoly_2;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/DoublePoint> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_normals
	List_1_t3080002113 * ___m_normals_3;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_delta
	double ___m_delta_4;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_sinA
	double ___m_sinA_5;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_sin
	double ___m_sin_6;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_cos
	double ___m_cos_7;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_miterLim
	double ___m_miterLim_8;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_StepsPerRad
	double ___m_StepsPerRad_9;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_lowest
	IntPoint_t2327573135  ___m_lowest_10;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::m_polyNodes
	PolyNode_t1300984468 * ___m_polyNodes_11;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::<ArcTolerance>k__BackingField
	double ___U3CArcToleranceU3Ek__BackingField_12;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipperOffset::<MiterLimit>k__BackingField
	double ___U3CMiterLimitU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_m_destPolys_0() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_destPolys_0)); }
	inline List_1_t976755323 * get_m_destPolys_0() const { return ___m_destPolys_0; }
	inline List_1_t976755323 ** get_address_of_m_destPolys_0() { return &___m_destPolys_0; }
	inline void set_m_destPolys_0(List_1_t976755323 * value)
	{
		___m_destPolys_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_destPolys_0), value);
	}

	inline static int32_t get_offset_of_m_srcPoly_1() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_srcPoly_1)); }
	inline List_1_t3799647877 * get_m_srcPoly_1() const { return ___m_srcPoly_1; }
	inline List_1_t3799647877 ** get_address_of_m_srcPoly_1() { return &___m_srcPoly_1; }
	inline void set_m_srcPoly_1(List_1_t3799647877 * value)
	{
		___m_srcPoly_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_srcPoly_1), value);
	}

	inline static int32_t get_offset_of_m_destPoly_2() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_destPoly_2)); }
	inline List_1_t3799647877 * get_m_destPoly_2() const { return ___m_destPoly_2; }
	inline List_1_t3799647877 ** get_address_of_m_destPoly_2() { return &___m_destPoly_2; }
	inline void set_m_destPoly_2(List_1_t3799647877 * value)
	{
		___m_destPoly_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_destPoly_2), value);
	}

	inline static int32_t get_offset_of_m_normals_3() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_normals_3)); }
	inline List_1_t3080002113 * get_m_normals_3() const { return ___m_normals_3; }
	inline List_1_t3080002113 ** get_address_of_m_normals_3() { return &___m_normals_3; }
	inline void set_m_normals_3(List_1_t3080002113 * value)
	{
		___m_normals_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_normals_3), value);
	}

	inline static int32_t get_offset_of_m_delta_4() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_delta_4)); }
	inline double get_m_delta_4() const { return ___m_delta_4; }
	inline double* get_address_of_m_delta_4() { return &___m_delta_4; }
	inline void set_m_delta_4(double value)
	{
		___m_delta_4 = value;
	}

	inline static int32_t get_offset_of_m_sinA_5() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_sinA_5)); }
	inline double get_m_sinA_5() const { return ___m_sinA_5; }
	inline double* get_address_of_m_sinA_5() { return &___m_sinA_5; }
	inline void set_m_sinA_5(double value)
	{
		___m_sinA_5 = value;
	}

	inline static int32_t get_offset_of_m_sin_6() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_sin_6)); }
	inline double get_m_sin_6() const { return ___m_sin_6; }
	inline double* get_address_of_m_sin_6() { return &___m_sin_6; }
	inline void set_m_sin_6(double value)
	{
		___m_sin_6 = value;
	}

	inline static int32_t get_offset_of_m_cos_7() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_cos_7)); }
	inline double get_m_cos_7() const { return ___m_cos_7; }
	inline double* get_address_of_m_cos_7() { return &___m_cos_7; }
	inline void set_m_cos_7(double value)
	{
		___m_cos_7 = value;
	}

	inline static int32_t get_offset_of_m_miterLim_8() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_miterLim_8)); }
	inline double get_m_miterLim_8() const { return ___m_miterLim_8; }
	inline double* get_address_of_m_miterLim_8() { return &___m_miterLim_8; }
	inline void set_m_miterLim_8(double value)
	{
		___m_miterLim_8 = value;
	}

	inline static int32_t get_offset_of_m_StepsPerRad_9() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_StepsPerRad_9)); }
	inline double get_m_StepsPerRad_9() const { return ___m_StepsPerRad_9; }
	inline double* get_address_of_m_StepsPerRad_9() { return &___m_StepsPerRad_9; }
	inline void set_m_StepsPerRad_9(double value)
	{
		___m_StepsPerRad_9 = value;
	}

	inline static int32_t get_offset_of_m_lowest_10() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_lowest_10)); }
	inline IntPoint_t2327573135  get_m_lowest_10() const { return ___m_lowest_10; }
	inline IntPoint_t2327573135 * get_address_of_m_lowest_10() { return &___m_lowest_10; }
	inline void set_m_lowest_10(IntPoint_t2327573135  value)
	{
		___m_lowest_10 = value;
	}

	inline static int32_t get_offset_of_m_polyNodes_11() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___m_polyNodes_11)); }
	inline PolyNode_t1300984468 * get_m_polyNodes_11() const { return ___m_polyNodes_11; }
	inline PolyNode_t1300984468 ** get_address_of_m_polyNodes_11() { return &___m_polyNodes_11; }
	inline void set_m_polyNodes_11(PolyNode_t1300984468 * value)
	{
		___m_polyNodes_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_polyNodes_11), value);
	}

	inline static int32_t get_offset_of_U3CArcToleranceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___U3CArcToleranceU3Ek__BackingField_12)); }
	inline double get_U3CArcToleranceU3Ek__BackingField_12() const { return ___U3CArcToleranceU3Ek__BackingField_12; }
	inline double* get_address_of_U3CArcToleranceU3Ek__BackingField_12() { return &___U3CArcToleranceU3Ek__BackingField_12; }
	inline void set_U3CArcToleranceU3Ek__BackingField_12(double value)
	{
		___U3CArcToleranceU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CMiterLimitU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ClipperOffset_t3668738110, ___U3CMiterLimitU3Ek__BackingField_13)); }
	inline double get_U3CMiterLimitU3Ek__BackingField_13() const { return ___U3CMiterLimitU3Ek__BackingField_13; }
	inline double* get_address_of_U3CMiterLimitU3Ek__BackingField_13() { return &___U3CMiterLimitU3Ek__BackingField_13; }
	inline void set_U3CMiterLimitU3Ek__BackingField_13(double value)
	{
		___U3CMiterLimitU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPEROFFSET_T3668738110_H
#ifndef GEOMTYPE_T3056663235_H
#define GEOMTYPE_T3056663235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.GeomType
struct  GeomType_t3056663235 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.GeomType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GeomType_t3056663235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOMTYPE_T3056663235_H
#ifndef WIRETYPES_T1504741901_H
#define WIRETYPES_T1504741901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Contants.WireTypes
struct  WireTypes_t1504741901 
{
public:
	// System.Int32 Mapbox.VectorTile.Contants.WireTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WireTypes_t1504741901, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIRETYPES_T1504741901_H
#ifndef COMMANDS_T1803779524_H
#define COMMANDS_T1803779524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Contants.Commands
struct  Commands_t1803779524 
{
public:
	// System.Int32 Mapbox.VectorTile.Contants.Commands::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Commands_t1803779524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDS_T1803779524_H
#ifndef TILETYPE_T3106966029_H
#define TILETYPE_T3106966029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Contants.TileType
struct  TileType_t3106966029 
{
public:
	// System.Int32 Mapbox.VectorTile.Contants.TileType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TileType_t3106966029, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILETYPE_T3106966029_H
#ifndef COMPRESSIONLEVEL_T3212615018_H
#define COMPRESSIONLEVEL_T3212615018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.CompressionLevel
struct  CompressionLevel_t3212615018 
{
public:
	// System.Int32 Mapbox.IO.Compression.CompressionLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionLevel_t3212615018, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONLEVEL_T3212615018_H
#ifndef ENDTYPE_T3515135373_H
#define ENDTYPE_T3515135373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/EndType
struct  EndType_t3515135373 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/EndType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EndType_t3515135373, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDTYPE_T3515135373_H
#ifndef JOINTYPE_T3449044149_H
#define JOINTYPE_T3449044149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/JoinType
struct  JoinType_t3449044149 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/JoinType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoinType_t3449044149, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTYPE_T3449044149_H
#ifndef POLYFILLTYPE_T2091732334_H
#define POLYFILLTYPE_T2091732334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyFillType
struct  PolyFillType_t2091732334 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyFillType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyFillType_t2091732334, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFILLTYPE_T2091732334_H
#ifndef DEFLATERSTATE_T438385491_H
#define DEFLATERSTATE_T438385491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflaterManaged/DeflaterState
struct  DeflaterState_t438385491 
{
public:
	// System.Int32 Mapbox.IO.Compression.DeflaterManaged/DeflaterState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DeflaterState_t438385491, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATERSTATE_T438385491_H
#ifndef WORKERTYPE_T976665898_H
#define WORKERTYPE_T976665898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflateStream/WorkerType
struct  WorkerType_t976665898 
{
public:
	// System.Byte Mapbox.IO.Compression.DeflateStream/WorkerType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WorkerType_t976665898, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKERTYPE_T976665898_H
#ifndef POLYTYPE_T1741373358_H
#define POLYTYPE_T1741373358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyType
struct  PolyType_t1741373358 
{
public:
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyType_t1741373358, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYTYPE_T1741373358_H
#ifndef COMPRESSIONMODE_T2055613304_H
#define COMPRESSIONMODE_T2055613304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.CompressionMode
struct  CompressionMode_t2055613304 
{
public:
	// System.Int32 Mapbox.IO.Compression.CompressionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMode_t2055613304, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMODE_T2055613304_H
#ifndef COMPRESSIONTRACINGSWITCHLEVEL_T3010065102_H
#define COMPRESSIONTRACINGSWITCHLEVEL_T3010065102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.CompressionTracingSwitchLevel
struct  CompressionTracingSwitchLevel_t3010065102 
{
public:
	// System.Int32 Mapbox.IO.Compression.CompressionTracingSwitchLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionTracingSwitchLevel_t3010065102, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONTRACINGSWITCHLEVEL_T3010065102_H
#ifndef GZIPDECODER_T3447631347_H
#define GZIPDECODER_T3447631347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.GZipDecoder
struct  GZipDecoder_t3447631347  : public RuntimeObject
{
public:
	// Mapbox.IO.Compression.GZipDecoder/GzipHeaderState Mapbox.IO.Compression.GZipDecoder::gzipHeaderSubstate
	int32_t ___gzipHeaderSubstate_0;
	// Mapbox.IO.Compression.GZipDecoder/GzipHeaderState Mapbox.IO.Compression.GZipDecoder::gzipFooterSubstate
	int32_t ___gzipFooterSubstate_1;
	// System.Int32 Mapbox.IO.Compression.GZipDecoder::gzip_header_flag
	int32_t ___gzip_header_flag_2;
	// System.Int32 Mapbox.IO.Compression.GZipDecoder::gzip_header_xlen
	int32_t ___gzip_header_xlen_3;
	// System.UInt32 Mapbox.IO.Compression.GZipDecoder::expectedCrc32
	uint32_t ___expectedCrc32_4;
	// System.UInt32 Mapbox.IO.Compression.GZipDecoder::expectedOutputStreamSizeModulo
	uint32_t ___expectedOutputStreamSizeModulo_5;
	// System.Int32 Mapbox.IO.Compression.GZipDecoder::loopCounter
	int32_t ___loopCounter_6;
	// System.UInt32 Mapbox.IO.Compression.GZipDecoder::actualCrc32
	uint32_t ___actualCrc32_7;
	// System.Int64 Mapbox.IO.Compression.GZipDecoder::actualStreamSizeModulo
	int64_t ___actualStreamSizeModulo_8;

public:
	inline static int32_t get_offset_of_gzipHeaderSubstate_0() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___gzipHeaderSubstate_0)); }
	inline int32_t get_gzipHeaderSubstate_0() const { return ___gzipHeaderSubstate_0; }
	inline int32_t* get_address_of_gzipHeaderSubstate_0() { return &___gzipHeaderSubstate_0; }
	inline void set_gzipHeaderSubstate_0(int32_t value)
	{
		___gzipHeaderSubstate_0 = value;
	}

	inline static int32_t get_offset_of_gzipFooterSubstate_1() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___gzipFooterSubstate_1)); }
	inline int32_t get_gzipFooterSubstate_1() const { return ___gzipFooterSubstate_1; }
	inline int32_t* get_address_of_gzipFooterSubstate_1() { return &___gzipFooterSubstate_1; }
	inline void set_gzipFooterSubstate_1(int32_t value)
	{
		___gzipFooterSubstate_1 = value;
	}

	inline static int32_t get_offset_of_gzip_header_flag_2() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___gzip_header_flag_2)); }
	inline int32_t get_gzip_header_flag_2() const { return ___gzip_header_flag_2; }
	inline int32_t* get_address_of_gzip_header_flag_2() { return &___gzip_header_flag_2; }
	inline void set_gzip_header_flag_2(int32_t value)
	{
		___gzip_header_flag_2 = value;
	}

	inline static int32_t get_offset_of_gzip_header_xlen_3() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___gzip_header_xlen_3)); }
	inline int32_t get_gzip_header_xlen_3() const { return ___gzip_header_xlen_3; }
	inline int32_t* get_address_of_gzip_header_xlen_3() { return &___gzip_header_xlen_3; }
	inline void set_gzip_header_xlen_3(int32_t value)
	{
		___gzip_header_xlen_3 = value;
	}

	inline static int32_t get_offset_of_expectedCrc32_4() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___expectedCrc32_4)); }
	inline uint32_t get_expectedCrc32_4() const { return ___expectedCrc32_4; }
	inline uint32_t* get_address_of_expectedCrc32_4() { return &___expectedCrc32_4; }
	inline void set_expectedCrc32_4(uint32_t value)
	{
		___expectedCrc32_4 = value;
	}

	inline static int32_t get_offset_of_expectedOutputStreamSizeModulo_5() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___expectedOutputStreamSizeModulo_5)); }
	inline uint32_t get_expectedOutputStreamSizeModulo_5() const { return ___expectedOutputStreamSizeModulo_5; }
	inline uint32_t* get_address_of_expectedOutputStreamSizeModulo_5() { return &___expectedOutputStreamSizeModulo_5; }
	inline void set_expectedOutputStreamSizeModulo_5(uint32_t value)
	{
		___expectedOutputStreamSizeModulo_5 = value;
	}

	inline static int32_t get_offset_of_loopCounter_6() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___loopCounter_6)); }
	inline int32_t get_loopCounter_6() const { return ___loopCounter_6; }
	inline int32_t* get_address_of_loopCounter_6() { return &___loopCounter_6; }
	inline void set_loopCounter_6(int32_t value)
	{
		___loopCounter_6 = value;
	}

	inline static int32_t get_offset_of_actualCrc32_7() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___actualCrc32_7)); }
	inline uint32_t get_actualCrc32_7() const { return ___actualCrc32_7; }
	inline uint32_t* get_address_of_actualCrc32_7() { return &___actualCrc32_7; }
	inline void set_actualCrc32_7(uint32_t value)
	{
		___actualCrc32_7 = value;
	}

	inline static int32_t get_offset_of_actualStreamSizeModulo_8() { return static_cast<int32_t>(offsetof(GZipDecoder_t3447631347, ___actualStreamSizeModulo_8)); }
	inline int64_t get_actualStreamSizeModulo_8() const { return ___actualStreamSizeModulo_8; }
	inline int64_t* get_address_of_actualStreamSizeModulo_8() { return &___actualStreamSizeModulo_8; }
	inline void set_actualStreamSizeModulo_8(int64_t value)
	{
		___actualStreamSizeModulo_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPDECODER_T3447631347_H
#ifndef PBFREADER_T1662343237_H
#define PBFREADER_T1662343237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.PbfReader
struct  PbfReader_t1662343237  : public RuntimeObject
{
public:
	// System.Int32 Mapbox.VectorTile.PbfReader::<Tag>k__BackingField
	int32_t ___U3CTagU3Ek__BackingField_0;
	// System.UInt64 Mapbox.VectorTile.PbfReader::<Value>k__BackingField
	uint64_t ___U3CValueU3Ek__BackingField_1;
	// Mapbox.VectorTile.Contants.WireTypes Mapbox.VectorTile.PbfReader::<WireType>k__BackingField
	int32_t ___U3CWireTypeU3Ek__BackingField_2;
	// System.Byte[] Mapbox.VectorTile.PbfReader::_buffer
	ByteU5BU5D_t4116647657* ____buffer_3;
	// System.UInt64 Mapbox.VectorTile.PbfReader::_length
	uint64_t ____length_4;
	// System.UInt64 Mapbox.VectorTile.PbfReader::_pos
	uint64_t ____pos_5;

public:
	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PbfReader_t1662343237, ___U3CTagU3Ek__BackingField_0)); }
	inline int32_t get_U3CTagU3Ek__BackingField_0() const { return ___U3CTagU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTagU3Ek__BackingField_0() { return &___U3CTagU3Ek__BackingField_0; }
	inline void set_U3CTagU3Ek__BackingField_0(int32_t value)
	{
		___U3CTagU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PbfReader_t1662343237, ___U3CValueU3Ek__BackingField_1)); }
	inline uint64_t get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline uint64_t* get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(uint64_t value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CWireTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PbfReader_t1662343237, ___U3CWireTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CWireTypeU3Ek__BackingField_2() const { return ___U3CWireTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CWireTypeU3Ek__BackingField_2() { return &___U3CWireTypeU3Ek__BackingField_2; }
	inline void set_U3CWireTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CWireTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of__buffer_3() { return static_cast<int32_t>(offsetof(PbfReader_t1662343237, ____buffer_3)); }
	inline ByteU5BU5D_t4116647657* get__buffer_3() const { return ____buffer_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__buffer_3() { return &____buffer_3; }
	inline void set__buffer_3(ByteU5BU5D_t4116647657* value)
	{
		____buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_3), value);
	}

	inline static int32_t get_offset_of__length_4() { return static_cast<int32_t>(offsetof(PbfReader_t1662343237, ____length_4)); }
	inline uint64_t get__length_4() const { return ____length_4; }
	inline uint64_t* get_address_of__length_4() { return &____length_4; }
	inline void set__length_4(uint64_t value)
	{
		____length_4 = value;
	}

	inline static int32_t get_offset_of__pos_5() { return static_cast<int32_t>(offsetof(PbfReader_t1662343237, ____pos_5)); }
	inline uint64_t get__pos_5() const { return ____pos_5; }
	inline uint64_t* get_address_of__pos_5() { return &____pos_5; }
	inline void set__pos_5(uint64_t value)
	{
		____pos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBFREADER_T1662343237_H
#ifndef INFLATER_T10910524_H
#define INFLATER_T10910524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.Inflater
struct  Inflater_t10910524  : public RuntimeObject
{
public:
	// Mapbox.IO.Compression.OutputWindow Mapbox.IO.Compression.Inflater::output
	OutputWindow_t1296654655 * ___output_5;
	// Mapbox.IO.Compression.InputBuffer Mapbox.IO.Compression.Inflater::input
	InputBuffer_t333709416 * ___input_6;
	// Mapbox.IO.Compression.HuffmanTree Mapbox.IO.Compression.Inflater::literalLengthTree
	HuffmanTree_t857975559 * ___literalLengthTree_7;
	// Mapbox.IO.Compression.HuffmanTree Mapbox.IO.Compression.Inflater::distanceTree
	HuffmanTree_t857975559 * ___distanceTree_8;
	// Mapbox.IO.Compression.InflaterState Mapbox.IO.Compression.Inflater::state
	int32_t ___state_9;
	// System.Boolean Mapbox.IO.Compression.Inflater::hasFormatReader
	bool ___hasFormatReader_10;
	// System.Int32 Mapbox.IO.Compression.Inflater::bfinal
	int32_t ___bfinal_11;
	// Mapbox.IO.Compression.BlockType Mapbox.IO.Compression.Inflater::blockType
	int32_t ___blockType_12;
	// System.Byte[] Mapbox.IO.Compression.Inflater::blockLengthBuffer
	ByteU5BU5D_t4116647657* ___blockLengthBuffer_13;
	// System.Int32 Mapbox.IO.Compression.Inflater::blockLength
	int32_t ___blockLength_14;
	// System.Int32 Mapbox.IO.Compression.Inflater::length
	int32_t ___length_15;
	// System.Int32 Mapbox.IO.Compression.Inflater::distanceCode
	int32_t ___distanceCode_16;
	// System.Int32 Mapbox.IO.Compression.Inflater::extraBits
	int32_t ___extraBits_17;
	// System.Int32 Mapbox.IO.Compression.Inflater::loopCounter
	int32_t ___loopCounter_18;
	// System.Int32 Mapbox.IO.Compression.Inflater::literalLengthCodeCount
	int32_t ___literalLengthCodeCount_19;
	// System.Int32 Mapbox.IO.Compression.Inflater::distanceCodeCount
	int32_t ___distanceCodeCount_20;
	// System.Int32 Mapbox.IO.Compression.Inflater::codeLengthCodeCount
	int32_t ___codeLengthCodeCount_21;
	// System.Int32 Mapbox.IO.Compression.Inflater::codeArraySize
	int32_t ___codeArraySize_22;
	// System.Int32 Mapbox.IO.Compression.Inflater::lengthCode
	int32_t ___lengthCode_23;
	// System.Byte[] Mapbox.IO.Compression.Inflater::codeList
	ByteU5BU5D_t4116647657* ___codeList_24;
	// System.Byte[] Mapbox.IO.Compression.Inflater::codeLengthTreeCodeLength
	ByteU5BU5D_t4116647657* ___codeLengthTreeCodeLength_25;
	// Mapbox.IO.Compression.HuffmanTree Mapbox.IO.Compression.Inflater::codeLengthTree
	HuffmanTree_t857975559 * ___codeLengthTree_26;
	// Mapbox.IO.Compression.IFileFormatReader Mapbox.IO.Compression.Inflater::formatReader
	RuntimeObject* ___formatReader_27;

public:
	inline static int32_t get_offset_of_output_5() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___output_5)); }
	inline OutputWindow_t1296654655 * get_output_5() const { return ___output_5; }
	inline OutputWindow_t1296654655 ** get_address_of_output_5() { return &___output_5; }
	inline void set_output_5(OutputWindow_t1296654655 * value)
	{
		___output_5 = value;
		Il2CppCodeGenWriteBarrier((&___output_5), value);
	}

	inline static int32_t get_offset_of_input_6() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___input_6)); }
	inline InputBuffer_t333709416 * get_input_6() const { return ___input_6; }
	inline InputBuffer_t333709416 ** get_address_of_input_6() { return &___input_6; }
	inline void set_input_6(InputBuffer_t333709416 * value)
	{
		___input_6 = value;
		Il2CppCodeGenWriteBarrier((&___input_6), value);
	}

	inline static int32_t get_offset_of_literalLengthTree_7() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___literalLengthTree_7)); }
	inline HuffmanTree_t857975559 * get_literalLengthTree_7() const { return ___literalLengthTree_7; }
	inline HuffmanTree_t857975559 ** get_address_of_literalLengthTree_7() { return &___literalLengthTree_7; }
	inline void set_literalLengthTree_7(HuffmanTree_t857975559 * value)
	{
		___literalLengthTree_7 = value;
		Il2CppCodeGenWriteBarrier((&___literalLengthTree_7), value);
	}

	inline static int32_t get_offset_of_distanceTree_8() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___distanceTree_8)); }
	inline HuffmanTree_t857975559 * get_distanceTree_8() const { return ___distanceTree_8; }
	inline HuffmanTree_t857975559 ** get_address_of_distanceTree_8() { return &___distanceTree_8; }
	inline void set_distanceTree_8(HuffmanTree_t857975559 * value)
	{
		___distanceTree_8 = value;
		Il2CppCodeGenWriteBarrier((&___distanceTree_8), value);
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___state_9)); }
	inline int32_t get_state_9() const { return ___state_9; }
	inline int32_t* get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(int32_t value)
	{
		___state_9 = value;
	}

	inline static int32_t get_offset_of_hasFormatReader_10() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___hasFormatReader_10)); }
	inline bool get_hasFormatReader_10() const { return ___hasFormatReader_10; }
	inline bool* get_address_of_hasFormatReader_10() { return &___hasFormatReader_10; }
	inline void set_hasFormatReader_10(bool value)
	{
		___hasFormatReader_10 = value;
	}

	inline static int32_t get_offset_of_bfinal_11() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___bfinal_11)); }
	inline int32_t get_bfinal_11() const { return ___bfinal_11; }
	inline int32_t* get_address_of_bfinal_11() { return &___bfinal_11; }
	inline void set_bfinal_11(int32_t value)
	{
		___bfinal_11 = value;
	}

	inline static int32_t get_offset_of_blockType_12() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___blockType_12)); }
	inline int32_t get_blockType_12() const { return ___blockType_12; }
	inline int32_t* get_address_of_blockType_12() { return &___blockType_12; }
	inline void set_blockType_12(int32_t value)
	{
		___blockType_12 = value;
	}

	inline static int32_t get_offset_of_blockLengthBuffer_13() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___blockLengthBuffer_13)); }
	inline ByteU5BU5D_t4116647657* get_blockLengthBuffer_13() const { return ___blockLengthBuffer_13; }
	inline ByteU5BU5D_t4116647657** get_address_of_blockLengthBuffer_13() { return &___blockLengthBuffer_13; }
	inline void set_blockLengthBuffer_13(ByteU5BU5D_t4116647657* value)
	{
		___blockLengthBuffer_13 = value;
		Il2CppCodeGenWriteBarrier((&___blockLengthBuffer_13), value);
	}

	inline static int32_t get_offset_of_blockLength_14() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___blockLength_14)); }
	inline int32_t get_blockLength_14() const { return ___blockLength_14; }
	inline int32_t* get_address_of_blockLength_14() { return &___blockLength_14; }
	inline void set_blockLength_14(int32_t value)
	{
		___blockLength_14 = value;
	}

	inline static int32_t get_offset_of_length_15() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___length_15)); }
	inline int32_t get_length_15() const { return ___length_15; }
	inline int32_t* get_address_of_length_15() { return &___length_15; }
	inline void set_length_15(int32_t value)
	{
		___length_15 = value;
	}

	inline static int32_t get_offset_of_distanceCode_16() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___distanceCode_16)); }
	inline int32_t get_distanceCode_16() const { return ___distanceCode_16; }
	inline int32_t* get_address_of_distanceCode_16() { return &___distanceCode_16; }
	inline void set_distanceCode_16(int32_t value)
	{
		___distanceCode_16 = value;
	}

	inline static int32_t get_offset_of_extraBits_17() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___extraBits_17)); }
	inline int32_t get_extraBits_17() const { return ___extraBits_17; }
	inline int32_t* get_address_of_extraBits_17() { return &___extraBits_17; }
	inline void set_extraBits_17(int32_t value)
	{
		___extraBits_17 = value;
	}

	inline static int32_t get_offset_of_loopCounter_18() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___loopCounter_18)); }
	inline int32_t get_loopCounter_18() const { return ___loopCounter_18; }
	inline int32_t* get_address_of_loopCounter_18() { return &___loopCounter_18; }
	inline void set_loopCounter_18(int32_t value)
	{
		___loopCounter_18 = value;
	}

	inline static int32_t get_offset_of_literalLengthCodeCount_19() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___literalLengthCodeCount_19)); }
	inline int32_t get_literalLengthCodeCount_19() const { return ___literalLengthCodeCount_19; }
	inline int32_t* get_address_of_literalLengthCodeCount_19() { return &___literalLengthCodeCount_19; }
	inline void set_literalLengthCodeCount_19(int32_t value)
	{
		___literalLengthCodeCount_19 = value;
	}

	inline static int32_t get_offset_of_distanceCodeCount_20() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___distanceCodeCount_20)); }
	inline int32_t get_distanceCodeCount_20() const { return ___distanceCodeCount_20; }
	inline int32_t* get_address_of_distanceCodeCount_20() { return &___distanceCodeCount_20; }
	inline void set_distanceCodeCount_20(int32_t value)
	{
		___distanceCodeCount_20 = value;
	}

	inline static int32_t get_offset_of_codeLengthCodeCount_21() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___codeLengthCodeCount_21)); }
	inline int32_t get_codeLengthCodeCount_21() const { return ___codeLengthCodeCount_21; }
	inline int32_t* get_address_of_codeLengthCodeCount_21() { return &___codeLengthCodeCount_21; }
	inline void set_codeLengthCodeCount_21(int32_t value)
	{
		___codeLengthCodeCount_21 = value;
	}

	inline static int32_t get_offset_of_codeArraySize_22() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___codeArraySize_22)); }
	inline int32_t get_codeArraySize_22() const { return ___codeArraySize_22; }
	inline int32_t* get_address_of_codeArraySize_22() { return &___codeArraySize_22; }
	inline void set_codeArraySize_22(int32_t value)
	{
		___codeArraySize_22 = value;
	}

	inline static int32_t get_offset_of_lengthCode_23() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___lengthCode_23)); }
	inline int32_t get_lengthCode_23() const { return ___lengthCode_23; }
	inline int32_t* get_address_of_lengthCode_23() { return &___lengthCode_23; }
	inline void set_lengthCode_23(int32_t value)
	{
		___lengthCode_23 = value;
	}

	inline static int32_t get_offset_of_codeList_24() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___codeList_24)); }
	inline ByteU5BU5D_t4116647657* get_codeList_24() const { return ___codeList_24; }
	inline ByteU5BU5D_t4116647657** get_address_of_codeList_24() { return &___codeList_24; }
	inline void set_codeList_24(ByteU5BU5D_t4116647657* value)
	{
		___codeList_24 = value;
		Il2CppCodeGenWriteBarrier((&___codeList_24), value);
	}

	inline static int32_t get_offset_of_codeLengthTreeCodeLength_25() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___codeLengthTreeCodeLength_25)); }
	inline ByteU5BU5D_t4116647657* get_codeLengthTreeCodeLength_25() const { return ___codeLengthTreeCodeLength_25; }
	inline ByteU5BU5D_t4116647657** get_address_of_codeLengthTreeCodeLength_25() { return &___codeLengthTreeCodeLength_25; }
	inline void set_codeLengthTreeCodeLength_25(ByteU5BU5D_t4116647657* value)
	{
		___codeLengthTreeCodeLength_25 = value;
		Il2CppCodeGenWriteBarrier((&___codeLengthTreeCodeLength_25), value);
	}

	inline static int32_t get_offset_of_codeLengthTree_26() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___codeLengthTree_26)); }
	inline HuffmanTree_t857975559 * get_codeLengthTree_26() const { return ___codeLengthTree_26; }
	inline HuffmanTree_t857975559 ** get_address_of_codeLengthTree_26() { return &___codeLengthTree_26; }
	inline void set_codeLengthTree_26(HuffmanTree_t857975559 * value)
	{
		___codeLengthTree_26 = value;
		Il2CppCodeGenWriteBarrier((&___codeLengthTree_26), value);
	}

	inline static int32_t get_offset_of_formatReader_27() { return static_cast<int32_t>(offsetof(Inflater_t10910524, ___formatReader_27)); }
	inline RuntimeObject* get_formatReader_27() const { return ___formatReader_27; }
	inline RuntimeObject** get_address_of_formatReader_27() { return &___formatReader_27; }
	inline void set_formatReader_27(RuntimeObject* value)
	{
		___formatReader_27 = value;
		Il2CppCodeGenWriteBarrier((&___formatReader_27), value);
	}
};

struct Inflater_t10910524_StaticFields
{
public:
	// System.Byte[] Mapbox.IO.Compression.Inflater::extraLengthBits
	ByteU5BU5D_t4116647657* ___extraLengthBits_0;
	// System.Int32[] Mapbox.IO.Compression.Inflater::lengthBase
	Int32U5BU5D_t385246372* ___lengthBase_1;
	// System.Int32[] Mapbox.IO.Compression.Inflater::distanceBasePosition
	Int32U5BU5D_t385246372* ___distanceBasePosition_2;
	// System.Byte[] Mapbox.IO.Compression.Inflater::codeOrder
	ByteU5BU5D_t4116647657* ___codeOrder_3;
	// System.Byte[] Mapbox.IO.Compression.Inflater::staticDistanceTreeTable
	ByteU5BU5D_t4116647657* ___staticDistanceTreeTable_4;

public:
	inline static int32_t get_offset_of_extraLengthBits_0() { return static_cast<int32_t>(offsetof(Inflater_t10910524_StaticFields, ___extraLengthBits_0)); }
	inline ByteU5BU5D_t4116647657* get_extraLengthBits_0() const { return ___extraLengthBits_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_extraLengthBits_0() { return &___extraLengthBits_0; }
	inline void set_extraLengthBits_0(ByteU5BU5D_t4116647657* value)
	{
		___extraLengthBits_0 = value;
		Il2CppCodeGenWriteBarrier((&___extraLengthBits_0), value);
	}

	inline static int32_t get_offset_of_lengthBase_1() { return static_cast<int32_t>(offsetof(Inflater_t10910524_StaticFields, ___lengthBase_1)); }
	inline Int32U5BU5D_t385246372* get_lengthBase_1() const { return ___lengthBase_1; }
	inline Int32U5BU5D_t385246372** get_address_of_lengthBase_1() { return &___lengthBase_1; }
	inline void set_lengthBase_1(Int32U5BU5D_t385246372* value)
	{
		___lengthBase_1 = value;
		Il2CppCodeGenWriteBarrier((&___lengthBase_1), value);
	}

	inline static int32_t get_offset_of_distanceBasePosition_2() { return static_cast<int32_t>(offsetof(Inflater_t10910524_StaticFields, ___distanceBasePosition_2)); }
	inline Int32U5BU5D_t385246372* get_distanceBasePosition_2() const { return ___distanceBasePosition_2; }
	inline Int32U5BU5D_t385246372** get_address_of_distanceBasePosition_2() { return &___distanceBasePosition_2; }
	inline void set_distanceBasePosition_2(Int32U5BU5D_t385246372* value)
	{
		___distanceBasePosition_2 = value;
		Il2CppCodeGenWriteBarrier((&___distanceBasePosition_2), value);
	}

	inline static int32_t get_offset_of_codeOrder_3() { return static_cast<int32_t>(offsetof(Inflater_t10910524_StaticFields, ___codeOrder_3)); }
	inline ByteU5BU5D_t4116647657* get_codeOrder_3() const { return ___codeOrder_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_codeOrder_3() { return &___codeOrder_3; }
	inline void set_codeOrder_3(ByteU5BU5D_t4116647657* value)
	{
		___codeOrder_3 = value;
		Il2CppCodeGenWriteBarrier((&___codeOrder_3), value);
	}

	inline static int32_t get_offset_of_staticDistanceTreeTable_4() { return static_cast<int32_t>(offsetof(Inflater_t10910524_StaticFields, ___staticDistanceTreeTable_4)); }
	inline ByteU5BU5D_t4116647657* get_staticDistanceTreeTable_4() const { return ___staticDistanceTreeTable_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_staticDistanceTreeTable_4() { return &___staticDistanceTreeTable_4; }
	inline void set_staticDistanceTreeTable_4(ByteU5BU5D_t4116647657* value)
	{
		___staticDistanceTreeTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___staticDistanceTreeTable_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATER_T10910524_H
#ifndef DEFLATERMANAGED_T1452220717_H
#define DEFLATERMANAGED_T1452220717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflaterManaged
struct  DeflaterManaged_t1452220717  : public RuntimeObject
{
public:
	// Mapbox.IO.Compression.FastEncoder Mapbox.IO.Compression.DeflaterManaged::deflateEncoder
	FastEncoder_t887805019 * ___deflateEncoder_4;
	// Mapbox.IO.Compression.CopyEncoder Mapbox.IO.Compression.DeflaterManaged::copyEncoder
	CopyEncoder_t1719707359 * ___copyEncoder_5;
	// Mapbox.IO.Compression.DeflateInput Mapbox.IO.Compression.DeflaterManaged::input
	DeflateInput_t3892891873 * ___input_6;
	// Mapbox.IO.Compression.OutputBuffer Mapbox.IO.Compression.DeflaterManaged::output
	OutputBuffer_t1331609326 * ___output_7;
	// Mapbox.IO.Compression.DeflaterManaged/DeflaterState Mapbox.IO.Compression.DeflaterManaged::processingState
	int32_t ___processingState_8;
	// Mapbox.IO.Compression.DeflateInput Mapbox.IO.Compression.DeflaterManaged::inputFromHistory
	DeflateInput_t3892891873 * ___inputFromHistory_9;

public:
	inline static int32_t get_offset_of_deflateEncoder_4() { return static_cast<int32_t>(offsetof(DeflaterManaged_t1452220717, ___deflateEncoder_4)); }
	inline FastEncoder_t887805019 * get_deflateEncoder_4() const { return ___deflateEncoder_4; }
	inline FastEncoder_t887805019 ** get_address_of_deflateEncoder_4() { return &___deflateEncoder_4; }
	inline void set_deflateEncoder_4(FastEncoder_t887805019 * value)
	{
		___deflateEncoder_4 = value;
		Il2CppCodeGenWriteBarrier((&___deflateEncoder_4), value);
	}

	inline static int32_t get_offset_of_copyEncoder_5() { return static_cast<int32_t>(offsetof(DeflaterManaged_t1452220717, ___copyEncoder_5)); }
	inline CopyEncoder_t1719707359 * get_copyEncoder_5() const { return ___copyEncoder_5; }
	inline CopyEncoder_t1719707359 ** get_address_of_copyEncoder_5() { return &___copyEncoder_5; }
	inline void set_copyEncoder_5(CopyEncoder_t1719707359 * value)
	{
		___copyEncoder_5 = value;
		Il2CppCodeGenWriteBarrier((&___copyEncoder_5), value);
	}

	inline static int32_t get_offset_of_input_6() { return static_cast<int32_t>(offsetof(DeflaterManaged_t1452220717, ___input_6)); }
	inline DeflateInput_t3892891873 * get_input_6() const { return ___input_6; }
	inline DeflateInput_t3892891873 ** get_address_of_input_6() { return &___input_6; }
	inline void set_input_6(DeflateInput_t3892891873 * value)
	{
		___input_6 = value;
		Il2CppCodeGenWriteBarrier((&___input_6), value);
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(DeflaterManaged_t1452220717, ___output_7)); }
	inline OutputBuffer_t1331609326 * get_output_7() const { return ___output_7; }
	inline OutputBuffer_t1331609326 ** get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(OutputBuffer_t1331609326 * value)
	{
		___output_7 = value;
		Il2CppCodeGenWriteBarrier((&___output_7), value);
	}

	inline static int32_t get_offset_of_processingState_8() { return static_cast<int32_t>(offsetof(DeflaterManaged_t1452220717, ___processingState_8)); }
	inline int32_t get_processingState_8() const { return ___processingState_8; }
	inline int32_t* get_address_of_processingState_8() { return &___processingState_8; }
	inline void set_processingState_8(int32_t value)
	{
		___processingState_8 = value;
	}

	inline static int32_t get_offset_of_inputFromHistory_9() { return static_cast<int32_t>(offsetof(DeflaterManaged_t1452220717, ___inputFromHistory_9)); }
	inline DeflateInput_t3892891873 * get_inputFromHistory_9() const { return ___inputFromHistory_9; }
	inline DeflateInput_t3892891873 ** get_address_of_inputFromHistory_9() { return &___inputFromHistory_9; }
	inline void set_inputFromHistory_9(DeflateInput_t3892891873 * value)
	{
		___inputFromHistory_9 = value;
		Il2CppCodeGenWriteBarrier((&___inputFromHistory_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATERMANAGED_T1452220717_H
#ifndef POLYNODE_T1300984468_H
#define POLYNODE_T1300984468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode
struct  PolyNode_t1300984468  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode::m_Parent
	PolyNode_t1300984468 * ___m_Parent_0;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode::m_polygon
	List_1_t3799647877 * ___m_polygon_1;
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode::m_Index
	int32_t ___m_Index_2;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/JoinType Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode::m_jointype
	int32_t ___m_jointype_3;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/EndType Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode::m_endtype
	int32_t ___m_endtype_4;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode::m_Childs
	List_1_t2773059210 * ___m_Childs_5;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode::<IsOpen>k__BackingField
	bool ___U3CIsOpenU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_Parent_0() { return static_cast<int32_t>(offsetof(PolyNode_t1300984468, ___m_Parent_0)); }
	inline PolyNode_t1300984468 * get_m_Parent_0() const { return ___m_Parent_0; }
	inline PolyNode_t1300984468 ** get_address_of_m_Parent_0() { return &___m_Parent_0; }
	inline void set_m_Parent_0(PolyNode_t1300984468 * value)
	{
		___m_Parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_0), value);
	}

	inline static int32_t get_offset_of_m_polygon_1() { return static_cast<int32_t>(offsetof(PolyNode_t1300984468, ___m_polygon_1)); }
	inline List_1_t3799647877 * get_m_polygon_1() const { return ___m_polygon_1; }
	inline List_1_t3799647877 ** get_address_of_m_polygon_1() { return &___m_polygon_1; }
	inline void set_m_polygon_1(List_1_t3799647877 * value)
	{
		___m_polygon_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_polygon_1), value);
	}

	inline static int32_t get_offset_of_m_Index_2() { return static_cast<int32_t>(offsetof(PolyNode_t1300984468, ___m_Index_2)); }
	inline int32_t get_m_Index_2() const { return ___m_Index_2; }
	inline int32_t* get_address_of_m_Index_2() { return &___m_Index_2; }
	inline void set_m_Index_2(int32_t value)
	{
		___m_Index_2 = value;
	}

	inline static int32_t get_offset_of_m_jointype_3() { return static_cast<int32_t>(offsetof(PolyNode_t1300984468, ___m_jointype_3)); }
	inline int32_t get_m_jointype_3() const { return ___m_jointype_3; }
	inline int32_t* get_address_of_m_jointype_3() { return &___m_jointype_3; }
	inline void set_m_jointype_3(int32_t value)
	{
		___m_jointype_3 = value;
	}

	inline static int32_t get_offset_of_m_endtype_4() { return static_cast<int32_t>(offsetof(PolyNode_t1300984468, ___m_endtype_4)); }
	inline int32_t get_m_endtype_4() const { return ___m_endtype_4; }
	inline int32_t* get_address_of_m_endtype_4() { return &___m_endtype_4; }
	inline void set_m_endtype_4(int32_t value)
	{
		___m_endtype_4 = value;
	}

	inline static int32_t get_offset_of_m_Childs_5() { return static_cast<int32_t>(offsetof(PolyNode_t1300984468, ___m_Childs_5)); }
	inline List_1_t2773059210 * get_m_Childs_5() const { return ___m_Childs_5; }
	inline List_1_t2773059210 ** get_address_of_m_Childs_5() { return &___m_Childs_5; }
	inline void set_m_Childs_5(List_1_t2773059210 * value)
	{
		___m_Childs_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Childs_5), value);
	}

	inline static int32_t get_offset_of_U3CIsOpenU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PolyNode_t1300984468, ___U3CIsOpenU3Ek__BackingField_6)); }
	inline bool get_U3CIsOpenU3Ek__BackingField_6() const { return ___U3CIsOpenU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsOpenU3Ek__BackingField_6() { return &___U3CIsOpenU3Ek__BackingField_6; }
	inline void set_U3CIsOpenU3Ek__BackingField_6(bool value)
	{
		___U3CIsOpenU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYNODE_T1300984468_H
#ifndef TEDGE_T1694054893_H
#define TEDGE_T1694054893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge
struct  TEdge_t1694054893  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Bot
	IntPoint_t2327573135  ___Bot_0;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Curr
	IntPoint_t2327573135  ___Curr_1;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Top
	IntPoint_t2327573135  ___Top_2;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntPoint Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Delta
	IntPoint_t2327573135  ___Delta_3;
	// System.Double Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Dx
	double ___Dx_4;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyType Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::PolyTyp
	int32_t ___PolyTyp_5;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/EdgeSide Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Side
	int32_t ___Side_6;
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::WindDelta
	int32_t ___WindDelta_7;
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::WindCnt
	int32_t ___WindCnt_8;
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::WindCnt2
	int32_t ___WindCnt2_9;
	// System.Int32 Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::OutIdx
	int32_t ___OutIdx_10;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Next
	TEdge_t1694054893 * ___Next_11;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::Prev
	TEdge_t1694054893 * ___Prev_12;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::NextInLML
	TEdge_t1694054893 * ___NextInLML_13;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::NextInAEL
	TEdge_t1694054893 * ___NextInAEL_14;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::PrevInAEL
	TEdge_t1694054893 * ___PrevInAEL_15;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::NextInSEL
	TEdge_t1694054893 * ___NextInSEL_16;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge::PrevInSEL
	TEdge_t1694054893 * ___PrevInSEL_17;

public:
	inline static int32_t get_offset_of_Bot_0() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Bot_0)); }
	inline IntPoint_t2327573135  get_Bot_0() const { return ___Bot_0; }
	inline IntPoint_t2327573135 * get_address_of_Bot_0() { return &___Bot_0; }
	inline void set_Bot_0(IntPoint_t2327573135  value)
	{
		___Bot_0 = value;
	}

	inline static int32_t get_offset_of_Curr_1() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Curr_1)); }
	inline IntPoint_t2327573135  get_Curr_1() const { return ___Curr_1; }
	inline IntPoint_t2327573135 * get_address_of_Curr_1() { return &___Curr_1; }
	inline void set_Curr_1(IntPoint_t2327573135  value)
	{
		___Curr_1 = value;
	}

	inline static int32_t get_offset_of_Top_2() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Top_2)); }
	inline IntPoint_t2327573135  get_Top_2() const { return ___Top_2; }
	inline IntPoint_t2327573135 * get_address_of_Top_2() { return &___Top_2; }
	inline void set_Top_2(IntPoint_t2327573135  value)
	{
		___Top_2 = value;
	}

	inline static int32_t get_offset_of_Delta_3() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Delta_3)); }
	inline IntPoint_t2327573135  get_Delta_3() const { return ___Delta_3; }
	inline IntPoint_t2327573135 * get_address_of_Delta_3() { return &___Delta_3; }
	inline void set_Delta_3(IntPoint_t2327573135  value)
	{
		___Delta_3 = value;
	}

	inline static int32_t get_offset_of_Dx_4() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Dx_4)); }
	inline double get_Dx_4() const { return ___Dx_4; }
	inline double* get_address_of_Dx_4() { return &___Dx_4; }
	inline void set_Dx_4(double value)
	{
		___Dx_4 = value;
	}

	inline static int32_t get_offset_of_PolyTyp_5() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___PolyTyp_5)); }
	inline int32_t get_PolyTyp_5() const { return ___PolyTyp_5; }
	inline int32_t* get_address_of_PolyTyp_5() { return &___PolyTyp_5; }
	inline void set_PolyTyp_5(int32_t value)
	{
		___PolyTyp_5 = value;
	}

	inline static int32_t get_offset_of_Side_6() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Side_6)); }
	inline int32_t get_Side_6() const { return ___Side_6; }
	inline int32_t* get_address_of_Side_6() { return &___Side_6; }
	inline void set_Side_6(int32_t value)
	{
		___Side_6 = value;
	}

	inline static int32_t get_offset_of_WindDelta_7() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___WindDelta_7)); }
	inline int32_t get_WindDelta_7() const { return ___WindDelta_7; }
	inline int32_t* get_address_of_WindDelta_7() { return &___WindDelta_7; }
	inline void set_WindDelta_7(int32_t value)
	{
		___WindDelta_7 = value;
	}

	inline static int32_t get_offset_of_WindCnt_8() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___WindCnt_8)); }
	inline int32_t get_WindCnt_8() const { return ___WindCnt_8; }
	inline int32_t* get_address_of_WindCnt_8() { return &___WindCnt_8; }
	inline void set_WindCnt_8(int32_t value)
	{
		___WindCnt_8 = value;
	}

	inline static int32_t get_offset_of_WindCnt2_9() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___WindCnt2_9)); }
	inline int32_t get_WindCnt2_9() const { return ___WindCnt2_9; }
	inline int32_t* get_address_of_WindCnt2_9() { return &___WindCnt2_9; }
	inline void set_WindCnt2_9(int32_t value)
	{
		___WindCnt2_9 = value;
	}

	inline static int32_t get_offset_of_OutIdx_10() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___OutIdx_10)); }
	inline int32_t get_OutIdx_10() const { return ___OutIdx_10; }
	inline int32_t* get_address_of_OutIdx_10() { return &___OutIdx_10; }
	inline void set_OutIdx_10(int32_t value)
	{
		___OutIdx_10 = value;
	}

	inline static int32_t get_offset_of_Next_11() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Next_11)); }
	inline TEdge_t1694054893 * get_Next_11() const { return ___Next_11; }
	inline TEdge_t1694054893 ** get_address_of_Next_11() { return &___Next_11; }
	inline void set_Next_11(TEdge_t1694054893 * value)
	{
		___Next_11 = value;
		Il2CppCodeGenWriteBarrier((&___Next_11), value);
	}

	inline static int32_t get_offset_of_Prev_12() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___Prev_12)); }
	inline TEdge_t1694054893 * get_Prev_12() const { return ___Prev_12; }
	inline TEdge_t1694054893 ** get_address_of_Prev_12() { return &___Prev_12; }
	inline void set_Prev_12(TEdge_t1694054893 * value)
	{
		___Prev_12 = value;
		Il2CppCodeGenWriteBarrier((&___Prev_12), value);
	}

	inline static int32_t get_offset_of_NextInLML_13() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___NextInLML_13)); }
	inline TEdge_t1694054893 * get_NextInLML_13() const { return ___NextInLML_13; }
	inline TEdge_t1694054893 ** get_address_of_NextInLML_13() { return &___NextInLML_13; }
	inline void set_NextInLML_13(TEdge_t1694054893 * value)
	{
		___NextInLML_13 = value;
		Il2CppCodeGenWriteBarrier((&___NextInLML_13), value);
	}

	inline static int32_t get_offset_of_NextInAEL_14() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___NextInAEL_14)); }
	inline TEdge_t1694054893 * get_NextInAEL_14() const { return ___NextInAEL_14; }
	inline TEdge_t1694054893 ** get_address_of_NextInAEL_14() { return &___NextInAEL_14; }
	inline void set_NextInAEL_14(TEdge_t1694054893 * value)
	{
		___NextInAEL_14 = value;
		Il2CppCodeGenWriteBarrier((&___NextInAEL_14), value);
	}

	inline static int32_t get_offset_of_PrevInAEL_15() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___PrevInAEL_15)); }
	inline TEdge_t1694054893 * get_PrevInAEL_15() const { return ___PrevInAEL_15; }
	inline TEdge_t1694054893 ** get_address_of_PrevInAEL_15() { return &___PrevInAEL_15; }
	inline void set_PrevInAEL_15(TEdge_t1694054893 * value)
	{
		___PrevInAEL_15 = value;
		Il2CppCodeGenWriteBarrier((&___PrevInAEL_15), value);
	}

	inline static int32_t get_offset_of_NextInSEL_16() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___NextInSEL_16)); }
	inline TEdge_t1694054893 * get_NextInSEL_16() const { return ___NextInSEL_16; }
	inline TEdge_t1694054893 ** get_address_of_NextInSEL_16() { return &___NextInSEL_16; }
	inline void set_NextInSEL_16(TEdge_t1694054893 * value)
	{
		___NextInSEL_16 = value;
		Il2CppCodeGenWriteBarrier((&___NextInSEL_16), value);
	}

	inline static int32_t get_offset_of_PrevInSEL_17() { return static_cast<int32_t>(offsetof(TEdge_t1694054893, ___PrevInSEL_17)); }
	inline TEdge_t1694054893 * get_PrevInSEL_17() const { return ___PrevInSEL_17; }
	inline TEdge_t1694054893 ** get_address_of_PrevInSEL_17() { return &___PrevInSEL_17; }
	inline void set_PrevInSEL_17(TEdge_t1694054893 * value)
	{
		___PrevInSEL_17 = value;
		Il2CppCodeGenWriteBarrier((&___PrevInSEL_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEDGE_T1694054893_H
#ifndef VECTORTILEFEATURE_T4093669591_H
#define VECTORTILEFEATURE_T4093669591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.VectorTileFeature
struct  VectorTileFeature_t4093669591  : public RuntimeObject
{
public:
	// Mapbox.VectorTile.VectorTileLayer Mapbox.VectorTile.VectorTileFeature::_layer
	VectorTileLayer_t873169949 * ____layer_0;
	// System.Object Mapbox.VectorTile.VectorTileFeature::_cachedGeometry
	RuntimeObject * ____cachedGeometry_1;
	// System.Nullable`1<System.UInt32> Mapbox.VectorTile.VectorTileFeature::_clipBuffer
	Nullable_1_t4282624060  ____clipBuffer_2;
	// System.Nullable`1<System.Single> Mapbox.VectorTile.VectorTileFeature::_scale
	Nullable_1_t3119828856  ____scale_3;
	// System.Nullable`1<System.Single> Mapbox.VectorTile.VectorTileFeature::_previousScale
	Nullable_1_t3119828856  ____previousScale_4;
	// System.UInt64 Mapbox.VectorTile.VectorTileFeature::<Id>k__BackingField
	uint64_t ___U3CIdU3Ek__BackingField_5;
	// Mapbox.VectorTile.Geometry.GeomType Mapbox.VectorTile.VectorTileFeature::<GeometryType>k__BackingField
	int32_t ___U3CGeometryTypeU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<System.UInt32> Mapbox.VectorTile.VectorTileFeature::<GeometryCommands>k__BackingField
	List_1_t4032136720 * ___U3CGeometryCommandsU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<System.Int32> Mapbox.VectorTile.VectorTileFeature::<Tags>k__BackingField
	List_1_t128053199 * ___U3CTagsU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of__layer_0() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ____layer_0)); }
	inline VectorTileLayer_t873169949 * get__layer_0() const { return ____layer_0; }
	inline VectorTileLayer_t873169949 ** get_address_of__layer_0() { return &____layer_0; }
	inline void set__layer_0(VectorTileLayer_t873169949 * value)
	{
		____layer_0 = value;
		Il2CppCodeGenWriteBarrier((&____layer_0), value);
	}

	inline static int32_t get_offset_of__cachedGeometry_1() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ____cachedGeometry_1)); }
	inline RuntimeObject * get__cachedGeometry_1() const { return ____cachedGeometry_1; }
	inline RuntimeObject ** get_address_of__cachedGeometry_1() { return &____cachedGeometry_1; }
	inline void set__cachedGeometry_1(RuntimeObject * value)
	{
		____cachedGeometry_1 = value;
		Il2CppCodeGenWriteBarrier((&____cachedGeometry_1), value);
	}

	inline static int32_t get_offset_of__clipBuffer_2() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ____clipBuffer_2)); }
	inline Nullable_1_t4282624060  get__clipBuffer_2() const { return ____clipBuffer_2; }
	inline Nullable_1_t4282624060 * get_address_of__clipBuffer_2() { return &____clipBuffer_2; }
	inline void set__clipBuffer_2(Nullable_1_t4282624060  value)
	{
		____clipBuffer_2 = value;
	}

	inline static int32_t get_offset_of__scale_3() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ____scale_3)); }
	inline Nullable_1_t3119828856  get__scale_3() const { return ____scale_3; }
	inline Nullable_1_t3119828856 * get_address_of__scale_3() { return &____scale_3; }
	inline void set__scale_3(Nullable_1_t3119828856  value)
	{
		____scale_3 = value;
	}

	inline static int32_t get_offset_of__previousScale_4() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ____previousScale_4)); }
	inline Nullable_1_t3119828856  get__previousScale_4() const { return ____previousScale_4; }
	inline Nullable_1_t3119828856 * get_address_of__previousScale_4() { return &____previousScale_4; }
	inline void set__previousScale_4(Nullable_1_t3119828856  value)
	{
		____previousScale_4 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ___U3CIdU3Ek__BackingField_5)); }
	inline uint64_t get_U3CIdU3Ek__BackingField_5() const { return ___U3CIdU3Ek__BackingField_5; }
	inline uint64_t* get_address_of_U3CIdU3Ek__BackingField_5() { return &___U3CIdU3Ek__BackingField_5; }
	inline void set_U3CIdU3Ek__BackingField_5(uint64_t value)
	{
		___U3CIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CGeometryTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ___U3CGeometryTypeU3Ek__BackingField_6)); }
	inline int32_t get_U3CGeometryTypeU3Ek__BackingField_6() const { return ___U3CGeometryTypeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CGeometryTypeU3Ek__BackingField_6() { return &___U3CGeometryTypeU3Ek__BackingField_6; }
	inline void set_U3CGeometryTypeU3Ek__BackingField_6(int32_t value)
	{
		___U3CGeometryTypeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CGeometryCommandsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ___U3CGeometryCommandsU3Ek__BackingField_7)); }
	inline List_1_t4032136720 * get_U3CGeometryCommandsU3Ek__BackingField_7() const { return ___U3CGeometryCommandsU3Ek__BackingField_7; }
	inline List_1_t4032136720 ** get_address_of_U3CGeometryCommandsU3Ek__BackingField_7() { return &___U3CGeometryCommandsU3Ek__BackingField_7; }
	inline void set_U3CGeometryCommandsU3Ek__BackingField_7(List_1_t4032136720 * value)
	{
		___U3CGeometryCommandsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGeometryCommandsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CTagsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(VectorTileFeature_t4093669591, ___U3CTagsU3Ek__BackingField_8)); }
	inline List_1_t128053199 * get_U3CTagsU3Ek__BackingField_8() const { return ___U3CTagsU3Ek__BackingField_8; }
	inline List_1_t128053199 ** get_address_of_U3CTagsU3Ek__BackingField_8() { return &___U3CTagsU3Ek__BackingField_8; }
	inline void set_U3CTagsU3Ek__BackingField_8(List_1_t128053199 * value)
	{
		___U3CTagsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagsU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILEFEATURE_T4093669591_H
#ifndef CLIPPER_T4158555122_H
#define CLIPPER_T4158555122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper
struct  Clipper_t4158555122  : public ClipperBase_t2411222589
{
public:
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/ClipType Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_ClipType
	int32_t ___m_ClipType_18;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Maxima Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_Maxima
	Maxima_t4278896992 * ___m_Maxima_19;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/TEdge Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_SortedEdges
	TEdge_t1694054893 * ___m_SortedEdges_20;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_IntersectList
	List_1_t556621665 * ___m_IntersectList_21;
	// System.Collections.Generic.IComparer`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/IntersectNode> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_IntersectNodeComparer
	RuntimeObject* ___m_IntersectNodeComparer_22;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_ExecuteLocked
	bool ___m_ExecuteLocked_23;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyFillType Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_ClipFillType
	int32_t ___m_ClipFillType_24;
	// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyFillType Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_SubjFillType
	int32_t ___m_SubjFillType_25;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Join> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_Joins
	List_1_t3821086104 * ___m_Joins_26;
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Join> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_GhostJoins
	List_1_t3821086104 * ___m_GhostJoins_27;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::m_UsingPolyTree
	bool ___m_UsingPolyTree_28;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::<ReverseSolution>k__BackingField
	bool ___U3CReverseSolutionU3Ek__BackingField_29;
	// System.Boolean Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/Clipper::<StrictlySimple>k__BackingField
	bool ___U3CStrictlySimpleU3Ek__BackingField_30;

public:
	inline static int32_t get_offset_of_m_ClipType_18() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_ClipType_18)); }
	inline int32_t get_m_ClipType_18() const { return ___m_ClipType_18; }
	inline int32_t* get_address_of_m_ClipType_18() { return &___m_ClipType_18; }
	inline void set_m_ClipType_18(int32_t value)
	{
		___m_ClipType_18 = value;
	}

	inline static int32_t get_offset_of_m_Maxima_19() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_Maxima_19)); }
	inline Maxima_t4278896992 * get_m_Maxima_19() const { return ___m_Maxima_19; }
	inline Maxima_t4278896992 ** get_address_of_m_Maxima_19() { return &___m_Maxima_19; }
	inline void set_m_Maxima_19(Maxima_t4278896992 * value)
	{
		___m_Maxima_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Maxima_19), value);
	}

	inline static int32_t get_offset_of_m_SortedEdges_20() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_SortedEdges_20)); }
	inline TEdge_t1694054893 * get_m_SortedEdges_20() const { return ___m_SortedEdges_20; }
	inline TEdge_t1694054893 ** get_address_of_m_SortedEdges_20() { return &___m_SortedEdges_20; }
	inline void set_m_SortedEdges_20(TEdge_t1694054893 * value)
	{
		___m_SortedEdges_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortedEdges_20), value);
	}

	inline static int32_t get_offset_of_m_IntersectList_21() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_IntersectList_21)); }
	inline List_1_t556621665 * get_m_IntersectList_21() const { return ___m_IntersectList_21; }
	inline List_1_t556621665 ** get_address_of_m_IntersectList_21() { return &___m_IntersectList_21; }
	inline void set_m_IntersectList_21(List_1_t556621665 * value)
	{
		___m_IntersectList_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntersectList_21), value);
	}

	inline static int32_t get_offset_of_m_IntersectNodeComparer_22() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_IntersectNodeComparer_22)); }
	inline RuntimeObject* get_m_IntersectNodeComparer_22() const { return ___m_IntersectNodeComparer_22; }
	inline RuntimeObject** get_address_of_m_IntersectNodeComparer_22() { return &___m_IntersectNodeComparer_22; }
	inline void set_m_IntersectNodeComparer_22(RuntimeObject* value)
	{
		___m_IntersectNodeComparer_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntersectNodeComparer_22), value);
	}

	inline static int32_t get_offset_of_m_ExecuteLocked_23() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_ExecuteLocked_23)); }
	inline bool get_m_ExecuteLocked_23() const { return ___m_ExecuteLocked_23; }
	inline bool* get_address_of_m_ExecuteLocked_23() { return &___m_ExecuteLocked_23; }
	inline void set_m_ExecuteLocked_23(bool value)
	{
		___m_ExecuteLocked_23 = value;
	}

	inline static int32_t get_offset_of_m_ClipFillType_24() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_ClipFillType_24)); }
	inline int32_t get_m_ClipFillType_24() const { return ___m_ClipFillType_24; }
	inline int32_t* get_address_of_m_ClipFillType_24() { return &___m_ClipFillType_24; }
	inline void set_m_ClipFillType_24(int32_t value)
	{
		___m_ClipFillType_24 = value;
	}

	inline static int32_t get_offset_of_m_SubjFillType_25() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_SubjFillType_25)); }
	inline int32_t get_m_SubjFillType_25() const { return ___m_SubjFillType_25; }
	inline int32_t* get_address_of_m_SubjFillType_25() { return &___m_SubjFillType_25; }
	inline void set_m_SubjFillType_25(int32_t value)
	{
		___m_SubjFillType_25 = value;
	}

	inline static int32_t get_offset_of_m_Joins_26() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_Joins_26)); }
	inline List_1_t3821086104 * get_m_Joins_26() const { return ___m_Joins_26; }
	inline List_1_t3821086104 ** get_address_of_m_Joins_26() { return &___m_Joins_26; }
	inline void set_m_Joins_26(List_1_t3821086104 * value)
	{
		___m_Joins_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Joins_26), value);
	}

	inline static int32_t get_offset_of_m_GhostJoins_27() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_GhostJoins_27)); }
	inline List_1_t3821086104 * get_m_GhostJoins_27() const { return ___m_GhostJoins_27; }
	inline List_1_t3821086104 ** get_address_of_m_GhostJoins_27() { return &___m_GhostJoins_27; }
	inline void set_m_GhostJoins_27(List_1_t3821086104 * value)
	{
		___m_GhostJoins_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_GhostJoins_27), value);
	}

	inline static int32_t get_offset_of_m_UsingPolyTree_28() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___m_UsingPolyTree_28)); }
	inline bool get_m_UsingPolyTree_28() const { return ___m_UsingPolyTree_28; }
	inline bool* get_address_of_m_UsingPolyTree_28() { return &___m_UsingPolyTree_28; }
	inline void set_m_UsingPolyTree_28(bool value)
	{
		___m_UsingPolyTree_28 = value;
	}

	inline static int32_t get_offset_of_U3CReverseSolutionU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___U3CReverseSolutionU3Ek__BackingField_29)); }
	inline bool get_U3CReverseSolutionU3Ek__BackingField_29() const { return ___U3CReverseSolutionU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CReverseSolutionU3Ek__BackingField_29() { return &___U3CReverseSolutionU3Ek__BackingField_29; }
	inline void set_U3CReverseSolutionU3Ek__BackingField_29(bool value)
	{
		___U3CReverseSolutionU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CStrictlySimpleU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Clipper_t4158555122, ___U3CStrictlySimpleU3Ek__BackingField_30)); }
	inline bool get_U3CStrictlySimpleU3Ek__BackingField_30() const { return ___U3CStrictlySimpleU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CStrictlySimpleU3Ek__BackingField_30() { return &___U3CStrictlySimpleU3Ek__BackingField_30; }
	inline void set_U3CStrictlySimpleU3Ek__BackingField_30(bool value)
	{
		___U3CStrictlySimpleU3Ek__BackingField_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPER_T4158555122_H
#ifndef MATCH_T352475233_H
#define MATCH_T352475233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.Match
struct  Match_t352475233  : public RuntimeObject
{
public:
	// MatchState Mapbox.IO.Compression.Match::state
	int32_t ___state_0;
	// System.Int32 Mapbox.IO.Compression.Match::pos
	int32_t ___pos_1;
	// System.Int32 Mapbox.IO.Compression.Match::len
	int32_t ___len_2;
	// System.Byte Mapbox.IO.Compression.Match::symbol
	uint8_t ___symbol_3;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(Match_t352475233, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(Match_t352475233, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(Match_t352475233, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_symbol_3() { return static_cast<int32_t>(offsetof(Match_t352475233, ___symbol_3)); }
	inline uint8_t get_symbol_3() const { return ___symbol_3; }
	inline uint8_t* get_address_of_symbol_3() { return &___symbol_3; }
	inline void set_symbol_3(uint8_t value)
	{
		___symbol_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T352475233_H
#ifndef DEFLATESTREAM_T2796728099_H
#define DEFLATESTREAM_T2796728099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflateStream
struct  DeflateStream_t2796728099  : public Stream_t1273022909
{
public:
	// System.IO.Stream Mapbox.IO.Compression.DeflateStream::_stream
	Stream_t1273022909 * ____stream_2;
	// Mapbox.IO.Compression.CompressionMode Mapbox.IO.Compression.DeflateStream::_mode
	int32_t ____mode_3;
	// System.Boolean Mapbox.IO.Compression.DeflateStream::_leaveOpen
	bool ____leaveOpen_4;
	// Mapbox.IO.Compression.Inflater Mapbox.IO.Compression.DeflateStream::inflater
	Inflater_t10910524 * ___inflater_5;
	// Mapbox.IO.Compression.IDeflater Mapbox.IO.Compression.DeflateStream::deflater
	RuntimeObject* ___deflater_6;
	// System.Byte[] Mapbox.IO.Compression.DeflateStream::buffer
	ByteU5BU5D_t4116647657* ___buffer_7;
	// System.Int32 Mapbox.IO.Compression.DeflateStream::asyncOperations
	int32_t ___asyncOperations_8;
	// System.AsyncCallback Mapbox.IO.Compression.DeflateStream::m_CallBack
	AsyncCallback_t3962456242 * ___m_CallBack_9;
	// Mapbox.IO.Compression.DeflateStream/AsyncWriteDelegate Mapbox.IO.Compression.DeflateStream::m_AsyncWriterDelegate
	AsyncWriteDelegate_t47908213 * ___m_AsyncWriterDelegate_10;
	// Mapbox.IO.Compression.IFileFormatWriter Mapbox.IO.Compression.DeflateStream::formatWriter
	RuntimeObject* ___formatWriter_11;
	// System.Boolean Mapbox.IO.Compression.DeflateStream::wroteHeader
	bool ___wroteHeader_12;
	// System.Boolean Mapbox.IO.Compression.DeflateStream::wroteBytes
	bool ___wroteBytes_13;

public:
	inline static int32_t get_offset_of__stream_2() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ____stream_2)); }
	inline Stream_t1273022909 * get__stream_2() const { return ____stream_2; }
	inline Stream_t1273022909 ** get_address_of__stream_2() { return &____stream_2; }
	inline void set__stream_2(Stream_t1273022909 * value)
	{
		____stream_2 = value;
		Il2CppCodeGenWriteBarrier((&____stream_2), value);
	}

	inline static int32_t get_offset_of__mode_3() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ____mode_3)); }
	inline int32_t get__mode_3() const { return ____mode_3; }
	inline int32_t* get_address_of__mode_3() { return &____mode_3; }
	inline void set__mode_3(int32_t value)
	{
		____mode_3 = value;
	}

	inline static int32_t get_offset_of__leaveOpen_4() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ____leaveOpen_4)); }
	inline bool get__leaveOpen_4() const { return ____leaveOpen_4; }
	inline bool* get_address_of__leaveOpen_4() { return &____leaveOpen_4; }
	inline void set__leaveOpen_4(bool value)
	{
		____leaveOpen_4 = value;
	}

	inline static int32_t get_offset_of_inflater_5() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___inflater_5)); }
	inline Inflater_t10910524 * get_inflater_5() const { return ___inflater_5; }
	inline Inflater_t10910524 ** get_address_of_inflater_5() { return &___inflater_5; }
	inline void set_inflater_5(Inflater_t10910524 * value)
	{
		___inflater_5 = value;
		Il2CppCodeGenWriteBarrier((&___inflater_5), value);
	}

	inline static int32_t get_offset_of_deflater_6() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___deflater_6)); }
	inline RuntimeObject* get_deflater_6() const { return ___deflater_6; }
	inline RuntimeObject** get_address_of_deflater_6() { return &___deflater_6; }
	inline void set_deflater_6(RuntimeObject* value)
	{
		___deflater_6 = value;
		Il2CppCodeGenWriteBarrier((&___deflater_6), value);
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___buffer_7)); }
	inline ByteU5BU5D_t4116647657* get_buffer_7() const { return ___buffer_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(ByteU5BU5D_t4116647657* value)
	{
		___buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_7), value);
	}

	inline static int32_t get_offset_of_asyncOperations_8() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___asyncOperations_8)); }
	inline int32_t get_asyncOperations_8() const { return ___asyncOperations_8; }
	inline int32_t* get_address_of_asyncOperations_8() { return &___asyncOperations_8; }
	inline void set_asyncOperations_8(int32_t value)
	{
		___asyncOperations_8 = value;
	}

	inline static int32_t get_offset_of_m_CallBack_9() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___m_CallBack_9)); }
	inline AsyncCallback_t3962456242 * get_m_CallBack_9() const { return ___m_CallBack_9; }
	inline AsyncCallback_t3962456242 ** get_address_of_m_CallBack_9() { return &___m_CallBack_9; }
	inline void set_m_CallBack_9(AsyncCallback_t3962456242 * value)
	{
		___m_CallBack_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CallBack_9), value);
	}

	inline static int32_t get_offset_of_m_AsyncWriterDelegate_10() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___m_AsyncWriterDelegate_10)); }
	inline AsyncWriteDelegate_t47908213 * get_m_AsyncWriterDelegate_10() const { return ___m_AsyncWriterDelegate_10; }
	inline AsyncWriteDelegate_t47908213 ** get_address_of_m_AsyncWriterDelegate_10() { return &___m_AsyncWriterDelegate_10; }
	inline void set_m_AsyncWriterDelegate_10(AsyncWriteDelegate_t47908213 * value)
	{
		___m_AsyncWriterDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncWriterDelegate_10), value);
	}

	inline static int32_t get_offset_of_formatWriter_11() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___formatWriter_11)); }
	inline RuntimeObject* get_formatWriter_11() const { return ___formatWriter_11; }
	inline RuntimeObject** get_address_of_formatWriter_11() { return &___formatWriter_11; }
	inline void set_formatWriter_11(RuntimeObject* value)
	{
		___formatWriter_11 = value;
		Il2CppCodeGenWriteBarrier((&___formatWriter_11), value);
	}

	inline static int32_t get_offset_of_wroteHeader_12() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___wroteHeader_12)); }
	inline bool get_wroteHeader_12() const { return ___wroteHeader_12; }
	inline bool* get_address_of_wroteHeader_12() { return &___wroteHeader_12; }
	inline void set_wroteHeader_12(bool value)
	{
		___wroteHeader_12 = value;
	}

	inline static int32_t get_offset_of_wroteBytes_13() { return static_cast<int32_t>(offsetof(DeflateStream_t2796728099, ___wroteBytes_13)); }
	inline bool get_wroteBytes_13() const { return ___wroteBytes_13; }
	inline bool* get_address_of_wroteBytes_13() { return &___wroteBytes_13; }
	inline void set_wroteBytes_13(bool value)
	{
		___wroteBytes_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAM_T2796728099_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef POLYTREE_T3708317675_H
#define POLYTREE_T3708317675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyTree
struct  PolyTree_t3708317675  : public PolyNode_t1300984468
{
public:
	// System.Collections.Generic.List`1<Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyNode> Mapbox.VectorTile.Geometry.InteralClipperLib.InternalClipper/PolyTree::m_AllPolys
	List_1_t2773059210 * ___m_AllPolys_7;

public:
	inline static int32_t get_offset_of_m_AllPolys_7() { return static_cast<int32_t>(offsetof(PolyTree_t3708317675, ___m_AllPolys_7)); }
	inline List_1_t2773059210 * get_m_AllPolys_7() const { return ___m_AllPolys_7; }
	inline List_1_t2773059210 ** get_address_of_m_AllPolys_7() { return &___m_AllPolys_7; }
	inline void set_m_AllPolys_7(List_1_t2773059210 * value)
	{
		___m_AllPolys_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AllPolys_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYTREE_T3708317675_H
#ifndef ASYNCWRITEDELEGATE_T47908213_H
#define ASYNCWRITEDELEGATE_T47908213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.IO.Compression.DeflateStream/AsyncWriteDelegate
struct  AsyncWriteDelegate_t47908213  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCWRITEDELEGATE_T47908213_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DIRECTIONSEXAMPLE_T2773998098_H
#define DIRECTIONSEXAMPLE_T2773998098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Playground.DirectionsExample
struct  DirectionsExample_t2773998098  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Mapbox.Examples.Playground.DirectionsExample::_resultsText
	Text_t1901882714 * ____resultsText_2;
	// Mapbox.Examples.ForwardGeocodeUserInput Mapbox.Examples.Playground.DirectionsExample::_startLocationGeocoder
	ForwardGeocodeUserInput_t2575136032 * ____startLocationGeocoder_3;
	// Mapbox.Examples.ForwardGeocodeUserInput Mapbox.Examples.Playground.DirectionsExample::_endLocationGeocoder
	ForwardGeocodeUserInput_t2575136032 * ____endLocationGeocoder_4;
	// Mapbox.Directions.Directions Mapbox.Examples.Playground.DirectionsExample::_directions
	Directions_t1397515081 * ____directions_5;
	// Mapbox.Utils.Vector2d[] Mapbox.Examples.Playground.DirectionsExample::_coordinates
	Vector2dU5BU5D_t852968953* ____coordinates_6;
	// Mapbox.Directions.DirectionResource Mapbox.Examples.Playground.DirectionsExample::_directionResource
	DirectionResource_t3837219169 * ____directionResource_7;

public:
	inline static int32_t get_offset_of__resultsText_2() { return static_cast<int32_t>(offsetof(DirectionsExample_t2773998098, ____resultsText_2)); }
	inline Text_t1901882714 * get__resultsText_2() const { return ____resultsText_2; }
	inline Text_t1901882714 ** get_address_of__resultsText_2() { return &____resultsText_2; }
	inline void set__resultsText_2(Text_t1901882714 * value)
	{
		____resultsText_2 = value;
		Il2CppCodeGenWriteBarrier((&____resultsText_2), value);
	}

	inline static int32_t get_offset_of__startLocationGeocoder_3() { return static_cast<int32_t>(offsetof(DirectionsExample_t2773998098, ____startLocationGeocoder_3)); }
	inline ForwardGeocodeUserInput_t2575136032 * get__startLocationGeocoder_3() const { return ____startLocationGeocoder_3; }
	inline ForwardGeocodeUserInput_t2575136032 ** get_address_of__startLocationGeocoder_3() { return &____startLocationGeocoder_3; }
	inline void set__startLocationGeocoder_3(ForwardGeocodeUserInput_t2575136032 * value)
	{
		____startLocationGeocoder_3 = value;
		Il2CppCodeGenWriteBarrier((&____startLocationGeocoder_3), value);
	}

	inline static int32_t get_offset_of__endLocationGeocoder_4() { return static_cast<int32_t>(offsetof(DirectionsExample_t2773998098, ____endLocationGeocoder_4)); }
	inline ForwardGeocodeUserInput_t2575136032 * get__endLocationGeocoder_4() const { return ____endLocationGeocoder_4; }
	inline ForwardGeocodeUserInput_t2575136032 ** get_address_of__endLocationGeocoder_4() { return &____endLocationGeocoder_4; }
	inline void set__endLocationGeocoder_4(ForwardGeocodeUserInput_t2575136032 * value)
	{
		____endLocationGeocoder_4 = value;
		Il2CppCodeGenWriteBarrier((&____endLocationGeocoder_4), value);
	}

	inline static int32_t get_offset_of__directions_5() { return static_cast<int32_t>(offsetof(DirectionsExample_t2773998098, ____directions_5)); }
	inline Directions_t1397515081 * get__directions_5() const { return ____directions_5; }
	inline Directions_t1397515081 ** get_address_of__directions_5() { return &____directions_5; }
	inline void set__directions_5(Directions_t1397515081 * value)
	{
		____directions_5 = value;
		Il2CppCodeGenWriteBarrier((&____directions_5), value);
	}

	inline static int32_t get_offset_of__coordinates_6() { return static_cast<int32_t>(offsetof(DirectionsExample_t2773998098, ____coordinates_6)); }
	inline Vector2dU5BU5D_t852968953* get__coordinates_6() const { return ____coordinates_6; }
	inline Vector2dU5BU5D_t852968953** get_address_of__coordinates_6() { return &____coordinates_6; }
	inline void set__coordinates_6(Vector2dU5BU5D_t852968953* value)
	{
		____coordinates_6 = value;
		Il2CppCodeGenWriteBarrier((&____coordinates_6), value);
	}

	inline static int32_t get_offset_of__directionResource_7() { return static_cast<int32_t>(offsetof(DirectionsExample_t2773998098, ____directionResource_7)); }
	inline DirectionResource_t3837219169 * get__directionResource_7() const { return ____directionResource_7; }
	inline DirectionResource_t3837219169 ** get_address_of__directionResource_7() { return &____directionResource_7; }
	inline void set__directionResource_7(DirectionResource_t3837219169 * value)
	{
		____directionResource_7 = value;
		Il2CppCodeGenWriteBarrier((&____directionResource_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONSEXAMPLE_T2773998098_H
#ifndef RELOADMAP_T3484436943_H
#define RELOADMAP_T3484436943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.ReloadMap
struct  ReloadMap_t3484436943  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera Mapbox.Examples.ReloadMap::_camera
	Camera_t4157153871 * ____camera_2;
	// UnityEngine.Vector3 Mapbox.Examples.ReloadMap::_cameraStartPos
	Vector3_t3722313464  ____cameraStartPos_3;
	// Mapbox.Unity.Map.AbstractMap Mapbox.Examples.ReloadMap::_map
	AbstractMap_t3082917158 * ____map_4;
	// Mapbox.Examples.ForwardGeocodeUserInput Mapbox.Examples.ReloadMap::_forwardGeocoder
	ForwardGeocodeUserInput_t2575136032 * ____forwardGeocoder_5;
	// UnityEngine.UI.Slider Mapbox.Examples.ReloadMap::_zoomSlider
	Slider_t3903728902 * ____zoomSlider_6;

public:
	inline static int32_t get_offset_of__camera_2() { return static_cast<int32_t>(offsetof(ReloadMap_t3484436943, ____camera_2)); }
	inline Camera_t4157153871 * get__camera_2() const { return ____camera_2; }
	inline Camera_t4157153871 ** get_address_of__camera_2() { return &____camera_2; }
	inline void set__camera_2(Camera_t4157153871 * value)
	{
		____camera_2 = value;
		Il2CppCodeGenWriteBarrier((&____camera_2), value);
	}

	inline static int32_t get_offset_of__cameraStartPos_3() { return static_cast<int32_t>(offsetof(ReloadMap_t3484436943, ____cameraStartPos_3)); }
	inline Vector3_t3722313464  get__cameraStartPos_3() const { return ____cameraStartPos_3; }
	inline Vector3_t3722313464 * get_address_of__cameraStartPos_3() { return &____cameraStartPos_3; }
	inline void set__cameraStartPos_3(Vector3_t3722313464  value)
	{
		____cameraStartPos_3 = value;
	}

	inline static int32_t get_offset_of__map_4() { return static_cast<int32_t>(offsetof(ReloadMap_t3484436943, ____map_4)); }
	inline AbstractMap_t3082917158 * get__map_4() const { return ____map_4; }
	inline AbstractMap_t3082917158 ** get_address_of__map_4() { return &____map_4; }
	inline void set__map_4(AbstractMap_t3082917158 * value)
	{
		____map_4 = value;
		Il2CppCodeGenWriteBarrier((&____map_4), value);
	}

	inline static int32_t get_offset_of__forwardGeocoder_5() { return static_cast<int32_t>(offsetof(ReloadMap_t3484436943, ____forwardGeocoder_5)); }
	inline ForwardGeocodeUserInput_t2575136032 * get__forwardGeocoder_5() const { return ____forwardGeocoder_5; }
	inline ForwardGeocodeUserInput_t2575136032 ** get_address_of__forwardGeocoder_5() { return &____forwardGeocoder_5; }
	inline void set__forwardGeocoder_5(ForwardGeocodeUserInput_t2575136032 * value)
	{
		____forwardGeocoder_5 = value;
		Il2CppCodeGenWriteBarrier((&____forwardGeocoder_5), value);
	}

	inline static int32_t get_offset_of__zoomSlider_6() { return static_cast<int32_t>(offsetof(ReloadMap_t3484436943, ____zoomSlider_6)); }
	inline Slider_t3903728902 * get__zoomSlider_6() const { return ____zoomSlider_6; }
	inline Slider_t3903728902 ** get_address_of__zoomSlider_6() { return &____zoomSlider_6; }
	inline void set__zoomSlider_6(Slider_t3903728902 * value)
	{
		____zoomSlider_6 = value;
		Il2CppCodeGenWriteBarrier((&____zoomSlider_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELOADMAP_T3484436943_H
#ifndef RASTERTILEEXAMPLE_T3949613556_H
#define RASTERTILEEXAMPLE_T3949613556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Playground.RasterTileExample
struct  RasterTileExample_t3949613556  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Examples.ForwardGeocodeUserInput Mapbox.Examples.Playground.RasterTileExample::_searchLocation
	ForwardGeocodeUserInput_t2575136032 * ____searchLocation_2;
	// UnityEngine.UI.Slider Mapbox.Examples.Playground.RasterTileExample::_zoomSlider
	Slider_t3903728902 * ____zoomSlider_3;
	// UnityEngine.UI.Dropdown Mapbox.Examples.Playground.RasterTileExample::_stylesDropdown
	Dropdown_t2274391225 * ____stylesDropdown_4;
	// UnityEngine.UI.RawImage Mapbox.Examples.Playground.RasterTileExample::_imageContainer
	RawImage_t3182918964 * ____imageContainer_5;
	// Mapbox.Map.Map`1<Mapbox.Map.RasterTile> Mapbox.Examples.Playground.RasterTileExample::_map
	Map_1_t1665010825 * ____map_6;
	// System.String Mapbox.Examples.Playground.RasterTileExample::_latLon
	String_t* ____latLon_7;
	// System.String[] Mapbox.Examples.Playground.RasterTileExample::_mapboxStyles
	StringU5BU5D_t1281789340* ____mapboxStyles_8;
	// Mapbox.Utils.Vector2d Mapbox.Examples.Playground.RasterTileExample::_startLoc
	Vector2d_t1865246568  ____startLoc_9;
	// System.Int32 Mapbox.Examples.Playground.RasterTileExample::_mapstyle
	int32_t ____mapstyle_10;

public:
	inline static int32_t get_offset_of__searchLocation_2() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____searchLocation_2)); }
	inline ForwardGeocodeUserInput_t2575136032 * get__searchLocation_2() const { return ____searchLocation_2; }
	inline ForwardGeocodeUserInput_t2575136032 ** get_address_of__searchLocation_2() { return &____searchLocation_2; }
	inline void set__searchLocation_2(ForwardGeocodeUserInput_t2575136032 * value)
	{
		____searchLocation_2 = value;
		Il2CppCodeGenWriteBarrier((&____searchLocation_2), value);
	}

	inline static int32_t get_offset_of__zoomSlider_3() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____zoomSlider_3)); }
	inline Slider_t3903728902 * get__zoomSlider_3() const { return ____zoomSlider_3; }
	inline Slider_t3903728902 ** get_address_of__zoomSlider_3() { return &____zoomSlider_3; }
	inline void set__zoomSlider_3(Slider_t3903728902 * value)
	{
		____zoomSlider_3 = value;
		Il2CppCodeGenWriteBarrier((&____zoomSlider_3), value);
	}

	inline static int32_t get_offset_of__stylesDropdown_4() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____stylesDropdown_4)); }
	inline Dropdown_t2274391225 * get__stylesDropdown_4() const { return ____stylesDropdown_4; }
	inline Dropdown_t2274391225 ** get_address_of__stylesDropdown_4() { return &____stylesDropdown_4; }
	inline void set__stylesDropdown_4(Dropdown_t2274391225 * value)
	{
		____stylesDropdown_4 = value;
		Il2CppCodeGenWriteBarrier((&____stylesDropdown_4), value);
	}

	inline static int32_t get_offset_of__imageContainer_5() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____imageContainer_5)); }
	inline RawImage_t3182918964 * get__imageContainer_5() const { return ____imageContainer_5; }
	inline RawImage_t3182918964 ** get_address_of__imageContainer_5() { return &____imageContainer_5; }
	inline void set__imageContainer_5(RawImage_t3182918964 * value)
	{
		____imageContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____imageContainer_5), value);
	}

	inline static int32_t get_offset_of__map_6() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____map_6)); }
	inline Map_1_t1665010825 * get__map_6() const { return ____map_6; }
	inline Map_1_t1665010825 ** get_address_of__map_6() { return &____map_6; }
	inline void set__map_6(Map_1_t1665010825 * value)
	{
		____map_6 = value;
		Il2CppCodeGenWriteBarrier((&____map_6), value);
	}

	inline static int32_t get_offset_of__latLon_7() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____latLon_7)); }
	inline String_t* get__latLon_7() const { return ____latLon_7; }
	inline String_t** get_address_of__latLon_7() { return &____latLon_7; }
	inline void set__latLon_7(String_t* value)
	{
		____latLon_7 = value;
		Il2CppCodeGenWriteBarrier((&____latLon_7), value);
	}

	inline static int32_t get_offset_of__mapboxStyles_8() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____mapboxStyles_8)); }
	inline StringU5BU5D_t1281789340* get__mapboxStyles_8() const { return ____mapboxStyles_8; }
	inline StringU5BU5D_t1281789340** get_address_of__mapboxStyles_8() { return &____mapboxStyles_8; }
	inline void set__mapboxStyles_8(StringU5BU5D_t1281789340* value)
	{
		____mapboxStyles_8 = value;
		Il2CppCodeGenWriteBarrier((&____mapboxStyles_8), value);
	}

	inline static int32_t get_offset_of__startLoc_9() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____startLoc_9)); }
	inline Vector2d_t1865246568  get__startLoc_9() const { return ____startLoc_9; }
	inline Vector2d_t1865246568 * get_address_of__startLoc_9() { return &____startLoc_9; }
	inline void set__startLoc_9(Vector2d_t1865246568  value)
	{
		____startLoc_9 = value;
	}

	inline static int32_t get_offset_of__mapstyle_10() { return static_cast<int32_t>(offsetof(RasterTileExample_t3949613556, ____mapstyle_10)); }
	inline int32_t get__mapstyle_10() const { return ____mapstyle_10; }
	inline int32_t* get_address_of__mapstyle_10() { return &____mapstyle_10; }
	inline void set__mapstyle_10(int32_t value)
	{
		____mapstyle_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RASTERTILEEXAMPLE_T3949613556_H
#ifndef REVERSEGEOCODEREXAMPLE_T1816435679_H
#define REVERSEGEOCODEREXAMPLE_T1816435679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Playground.ReverseGeocoderExample
struct  ReverseGeocoderExample_t1816435679  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Examples.ReverseGeocodeUserInput Mapbox.Examples.Playground.ReverseGeocoderExample::_searchLocation
	ReverseGeocodeUserInput_t2632079094 * ____searchLocation_2;
	// UnityEngine.UI.Text Mapbox.Examples.Playground.ReverseGeocoderExample::_resultsText
	Text_t1901882714 * ____resultsText_3;

public:
	inline static int32_t get_offset_of__searchLocation_2() { return static_cast<int32_t>(offsetof(ReverseGeocoderExample_t1816435679, ____searchLocation_2)); }
	inline ReverseGeocodeUserInput_t2632079094 * get__searchLocation_2() const { return ____searchLocation_2; }
	inline ReverseGeocodeUserInput_t2632079094 ** get_address_of__searchLocation_2() { return &____searchLocation_2; }
	inline void set__searchLocation_2(ReverseGeocodeUserInput_t2632079094 * value)
	{
		____searchLocation_2 = value;
		Il2CppCodeGenWriteBarrier((&____searchLocation_2), value);
	}

	inline static int32_t get_offset_of__resultsText_3() { return static_cast<int32_t>(offsetof(ReverseGeocoderExample_t1816435679, ____resultsText_3)); }
	inline Text_t1901882714 * get__resultsText_3() const { return ____resultsText_3; }
	inline Text_t1901882714 ** get_address_of__resultsText_3() { return &____resultsText_3; }
	inline void set__resultsText_3(Text_t1901882714 * value)
	{
		____resultsText_3 = value;
		Il2CppCodeGenWriteBarrier((&____resultsText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVERSEGEOCODEREXAMPLE_T1816435679_H
#ifndef VECTORTILEEXAMPLE_T4002429299_H
#define VECTORTILEEXAMPLE_T4002429299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Playground.VectorTileExample
struct  VectorTileExample_t4002429299  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Examples.ForwardGeocodeUserInput Mapbox.Examples.Playground.VectorTileExample::_searchLocation
	ForwardGeocodeUserInput_t2575136032 * ____searchLocation_2;
	// UnityEngine.UI.Text Mapbox.Examples.Playground.VectorTileExample::_resultsText
	Text_t1901882714 * ____resultsText_3;
	// Mapbox.Map.Map`1<Mapbox.Map.VectorTile> Mapbox.Examples.Playground.VectorTileExample::_map
	Map_1_t3054239837 * ____map_4;

public:
	inline static int32_t get_offset_of__searchLocation_2() { return static_cast<int32_t>(offsetof(VectorTileExample_t4002429299, ____searchLocation_2)); }
	inline ForwardGeocodeUserInput_t2575136032 * get__searchLocation_2() const { return ____searchLocation_2; }
	inline ForwardGeocodeUserInput_t2575136032 ** get_address_of__searchLocation_2() { return &____searchLocation_2; }
	inline void set__searchLocation_2(ForwardGeocodeUserInput_t2575136032 * value)
	{
		____searchLocation_2 = value;
		Il2CppCodeGenWriteBarrier((&____searchLocation_2), value);
	}

	inline static int32_t get_offset_of__resultsText_3() { return static_cast<int32_t>(offsetof(VectorTileExample_t4002429299, ____resultsText_3)); }
	inline Text_t1901882714 * get__resultsText_3() const { return ____resultsText_3; }
	inline Text_t1901882714 ** get_address_of__resultsText_3() { return &____resultsText_3; }
	inline void set__resultsText_3(Text_t1901882714 * value)
	{
		____resultsText_3 = value;
		Il2CppCodeGenWriteBarrier((&____resultsText_3), value);
	}

	inline static int32_t get_offset_of__map_4() { return static_cast<int32_t>(offsetof(VectorTileExample_t4002429299, ____map_4)); }
	inline Map_1_t3054239837 * get__map_4() const { return ____map_4; }
	inline Map_1_t3054239837 ** get_address_of__map_4() { return &____map_4; }
	inline void set__map_4(Map_1_t3054239837 * value)
	{
		____map_4 = value;
		Il2CppCodeGenWriteBarrier((&____map_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORTILEEXAMPLE_T4002429299_H
#ifndef CAMERAMOVEMENT_T3562026478_H
#define CAMERAMOVEMENT_T3562026478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.CameraMovement
struct  CameraMovement_t3562026478  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mapbox.Examples.CameraMovement::_panSpeed
	float ____panSpeed_2;
	// System.Single Mapbox.Examples.CameraMovement::_zoomSpeed
	float ____zoomSpeed_3;
	// UnityEngine.Camera Mapbox.Examples.CameraMovement::_referenceCamera
	Camera_t4157153871 * ____referenceCamera_4;
	// UnityEngine.Quaternion Mapbox.Examples.CameraMovement::_originalRotation
	Quaternion_t2301928331  ____originalRotation_5;
	// UnityEngine.Vector3 Mapbox.Examples.CameraMovement::_origin
	Vector3_t3722313464  ____origin_6;
	// UnityEngine.Vector3 Mapbox.Examples.CameraMovement::_delta
	Vector3_t3722313464  ____delta_7;
	// System.Boolean Mapbox.Examples.CameraMovement::_shouldDrag
	bool ____shouldDrag_8;

public:
	inline static int32_t get_offset_of__panSpeed_2() { return static_cast<int32_t>(offsetof(CameraMovement_t3562026478, ____panSpeed_2)); }
	inline float get__panSpeed_2() const { return ____panSpeed_2; }
	inline float* get_address_of__panSpeed_2() { return &____panSpeed_2; }
	inline void set__panSpeed_2(float value)
	{
		____panSpeed_2 = value;
	}

	inline static int32_t get_offset_of__zoomSpeed_3() { return static_cast<int32_t>(offsetof(CameraMovement_t3562026478, ____zoomSpeed_3)); }
	inline float get__zoomSpeed_3() const { return ____zoomSpeed_3; }
	inline float* get_address_of__zoomSpeed_3() { return &____zoomSpeed_3; }
	inline void set__zoomSpeed_3(float value)
	{
		____zoomSpeed_3 = value;
	}

	inline static int32_t get_offset_of__referenceCamera_4() { return static_cast<int32_t>(offsetof(CameraMovement_t3562026478, ____referenceCamera_4)); }
	inline Camera_t4157153871 * get__referenceCamera_4() const { return ____referenceCamera_4; }
	inline Camera_t4157153871 ** get_address_of__referenceCamera_4() { return &____referenceCamera_4; }
	inline void set__referenceCamera_4(Camera_t4157153871 * value)
	{
		____referenceCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&____referenceCamera_4), value);
	}

	inline static int32_t get_offset_of__originalRotation_5() { return static_cast<int32_t>(offsetof(CameraMovement_t3562026478, ____originalRotation_5)); }
	inline Quaternion_t2301928331  get__originalRotation_5() const { return ____originalRotation_5; }
	inline Quaternion_t2301928331 * get_address_of__originalRotation_5() { return &____originalRotation_5; }
	inline void set__originalRotation_5(Quaternion_t2301928331  value)
	{
		____originalRotation_5 = value;
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(CameraMovement_t3562026478, ____origin_6)); }
	inline Vector3_t3722313464  get__origin_6() const { return ____origin_6; }
	inline Vector3_t3722313464 * get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(Vector3_t3722313464  value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__delta_7() { return static_cast<int32_t>(offsetof(CameraMovement_t3562026478, ____delta_7)); }
	inline Vector3_t3722313464  get__delta_7() const { return ____delta_7; }
	inline Vector3_t3722313464 * get_address_of__delta_7() { return &____delta_7; }
	inline void set__delta_7(Vector3_t3722313464  value)
	{
		____delta_7 = value;
	}

	inline static int32_t get_offset_of__shouldDrag_8() { return static_cast<int32_t>(offsetof(CameraMovement_t3562026478, ____shouldDrag_8)); }
	inline bool get__shouldDrag_8() const { return ____shouldDrag_8; }
	inline bool* get_address_of__shouldDrag_8() { return &____shouldDrag_8; }
	inline void set__shouldDrag_8(bool value)
	{
		____shouldDrag_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMOVEMENT_T3562026478_H
#ifndef SPAWNONGLOBEEXAMPLE_T1835218885_H
#define SPAWNONGLOBEEXAMPLE_T1835218885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.SpawnOnGlobeExample
struct  SpawnOnGlobeExample_t1835218885  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Unity.MeshGeneration.Factories.FlatSphereTerrainFactory Mapbox.Examples.SpawnOnGlobeExample::_globeFactory
	FlatSphereTerrainFactory_t1099794750 * ____globeFactory_2;
	// Mapbox.Utils.Vector2d[] Mapbox.Examples.SpawnOnGlobeExample::_locations
	Vector2dU5BU5D_t852968953* ____locations_3;
	// System.Single Mapbox.Examples.SpawnOnGlobeExample::_spawnScale
	float ____spawnScale_4;

public:
	inline static int32_t get_offset_of__globeFactory_2() { return static_cast<int32_t>(offsetof(SpawnOnGlobeExample_t1835218885, ____globeFactory_2)); }
	inline FlatSphereTerrainFactory_t1099794750 * get__globeFactory_2() const { return ____globeFactory_2; }
	inline FlatSphereTerrainFactory_t1099794750 ** get_address_of__globeFactory_2() { return &____globeFactory_2; }
	inline void set__globeFactory_2(FlatSphereTerrainFactory_t1099794750 * value)
	{
		____globeFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&____globeFactory_2), value);
	}

	inline static int32_t get_offset_of__locations_3() { return static_cast<int32_t>(offsetof(SpawnOnGlobeExample_t1835218885, ____locations_3)); }
	inline Vector2dU5BU5D_t852968953* get__locations_3() const { return ____locations_3; }
	inline Vector2dU5BU5D_t852968953** get_address_of__locations_3() { return &____locations_3; }
	inline void set__locations_3(Vector2dU5BU5D_t852968953* value)
	{
		____locations_3 = value;
		Il2CppCodeGenWriteBarrier((&____locations_3), value);
	}

	inline static int32_t get_offset_of__spawnScale_4() { return static_cast<int32_t>(offsetof(SpawnOnGlobeExample_t1835218885, ____spawnScale_4)); }
	inline float get__spawnScale_4() const { return ____spawnScale_4; }
	inline float* get_address_of__spawnScale_4() { return &____spawnScale_4; }
	inline void set__spawnScale_4(float value)
	{
		____spawnScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNONGLOBEEXAMPLE_T1835218885_H
#ifndef DRAGROTATE_T2912444650_H
#define DRAGROTATE_T2912444650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Scripts.Utilities.DragRotate
struct  DragRotate_t2912444650  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Mapbox.Examples.Scripts.Utilities.DragRotate::_objectToRotate
	Transform_t3600365921 * ____objectToRotate_2;
	// System.Single Mapbox.Examples.Scripts.Utilities.DragRotate::_multiplier
	float ____multiplier_3;
	// UnityEngine.Vector3 Mapbox.Examples.Scripts.Utilities.DragRotate::_startTouchPosition
	Vector3_t3722313464  ____startTouchPosition_4;

public:
	inline static int32_t get_offset_of__objectToRotate_2() { return static_cast<int32_t>(offsetof(DragRotate_t2912444650, ____objectToRotate_2)); }
	inline Transform_t3600365921 * get__objectToRotate_2() const { return ____objectToRotate_2; }
	inline Transform_t3600365921 ** get_address_of__objectToRotate_2() { return &____objectToRotate_2; }
	inline void set__objectToRotate_2(Transform_t3600365921 * value)
	{
		____objectToRotate_2 = value;
		Il2CppCodeGenWriteBarrier((&____objectToRotate_2), value);
	}

	inline static int32_t get_offset_of__multiplier_3() { return static_cast<int32_t>(offsetof(DragRotate_t2912444650, ____multiplier_3)); }
	inline float get__multiplier_3() const { return ____multiplier_3; }
	inline float* get_address_of__multiplier_3() { return &____multiplier_3; }
	inline void set__multiplier_3(float value)
	{
		____multiplier_3 = value;
	}

	inline static int32_t get_offset_of__startTouchPosition_4() { return static_cast<int32_t>(offsetof(DragRotate_t2912444650, ____startTouchPosition_4)); }
	inline Vector3_t3722313464  get__startTouchPosition_4() const { return ____startTouchPosition_4; }
	inline Vector3_t3722313464 * get_address_of__startTouchPosition_4() { return &____startTouchPosition_4; }
	inline void set__startTouchPosition_4(Vector3_t3722313464  value)
	{
		____startTouchPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGROTATE_T2912444650_H
#ifndef FORWARDGEOCODEREXAMPLE_T595455162_H
#define FORWARDGEOCODEREXAMPLE_T595455162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mapbox.Examples.Playground.ForwardGeocoderExample
struct  ForwardGeocoderExample_t595455162  : public MonoBehaviour_t3962482529
{
public:
	// Mapbox.Examples.ForwardGeocodeUserInput Mapbox.Examples.Playground.ForwardGeocoderExample::_searchLocation
	ForwardGeocodeUserInput_t2575136032 * ____searchLocation_2;
	// UnityEngine.UI.Text Mapbox.Examples.Playground.ForwardGeocoderExample::_resultsText
	Text_t1901882714 * ____resultsText_3;

public:
	inline static int32_t get_offset_of__searchLocation_2() { return static_cast<int32_t>(offsetof(ForwardGeocoderExample_t595455162, ____searchLocation_2)); }
	inline ForwardGeocodeUserInput_t2575136032 * get__searchLocation_2() const { return ____searchLocation_2; }
	inline ForwardGeocodeUserInput_t2575136032 ** get_address_of__searchLocation_2() { return &____searchLocation_2; }
	inline void set__searchLocation_2(ForwardGeocodeUserInput_t2575136032 * value)
	{
		____searchLocation_2 = value;
		Il2CppCodeGenWriteBarrier((&____searchLocation_2), value);
	}

	inline static int32_t get_offset_of__resultsText_3() { return static_cast<int32_t>(offsetof(ForwardGeocoderExample_t595455162, ____resultsText_3)); }
	inline Text_t1901882714 * get__resultsText_3() const { return ____resultsText_3; }
	inline Text_t1901882714 ** get_address_of__resultsText_3() { return &____resultsText_3; }
	inline void set__resultsText_3(Text_t1901882714 * value)
	{
		____resultsText_3 = value;
		Il2CppCodeGenWriteBarrier((&____resultsText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDGEOCODEREXAMPLE_T595455162_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (UnixTimestampUtils_t2933311910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (Mathd_t279629051)+ sizeof (RuntimeObject), sizeof(Mathd_t279629051 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3201[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (RectD_t151583371)+ sizeof (RuntimeObject), sizeof(RectD_t151583371 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3202[4] = 
{
	RectD_t151583371::get_offset_of_U3CMinU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectD_t151583371::get_offset_of_U3CMaxU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectD_t151583371::get_offset_of_U3CSizeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectD_t151583371::get_offset_of_U3CCenterU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (Vector2d_t1865246568)+ sizeof (RuntimeObject), sizeof(Vector2d_t1865246568 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3203[3] = 
{
	0,
	Vector2d_t1865246568::get_offset_of_x_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector2d_t1865246568::get_offset_of_y_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (EnumExtensions_t2644584491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (VectorTileExtensions_t4243590528), -1, sizeof(VectorTileExtensions_t4243590528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3205[6] = 
{
	VectorTileExtensions_t4243590528_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	VectorTileExtensions_t4243590528_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	VectorTileExtensions_t4243590528_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	VectorTileExtensions_t4243590528_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	VectorTileExtensions_t4243590528_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	VectorTileExtensions_t4243590528_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (VectorTileFeatureExtensions_t4023769631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[4] = 
{
	U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684::get_offset_of_zoom_0(),
	U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684::get_offset_of_tileColumn_1(),
	U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684::get_offset_of_tileRow_2(),
	U3CGeometryAsWgs84U3Ec__AnonStorey0_t3901700684::get_offset_of_feature_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (InternalClipper_t4127247543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (DoublePoint_t1607927371)+ sizeof (RuntimeObject), sizeof(DoublePoint_t1607927371 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3209[2] = 
{
	DoublePoint_t1607927371::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoublePoint_t1607927371::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (PolyTree_t3708317675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3210[1] = 
{
	PolyTree_t3708317675::get_offset_of_m_AllPolys_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (PolyNode_t1300984468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3211[7] = 
{
	PolyNode_t1300984468::get_offset_of_m_Parent_0(),
	PolyNode_t1300984468::get_offset_of_m_polygon_1(),
	PolyNode_t1300984468::get_offset_of_m_Index_2(),
	PolyNode_t1300984468::get_offset_of_m_jointype_3(),
	PolyNode_t1300984468::get_offset_of_m_endtype_4(),
	PolyNode_t1300984468::get_offset_of_m_Childs_5(),
	PolyNode_t1300984468::get_offset_of_U3CIsOpenU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (Int128_t2615162842)+ sizeof (RuntimeObject), sizeof(Int128_t2615162842 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3212[2] = 
{
	Int128_t2615162842::get_offset_of_hi_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Int128_t2615162842::get_offset_of_lo_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (IntPoint_t2327573135)+ sizeof (RuntimeObject), sizeof(IntPoint_t2327573135 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3213[2] = 
{
	IntPoint_t2327573135::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntPoint_t2327573135::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (IntRect_t752847524)+ sizeof (RuntimeObject), sizeof(IntRect_t752847524 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3214[4] = 
{
	IntRect_t752847524::get_offset_of_left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntRect_t752847524::get_offset_of_top_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntRect_t752847524::get_offset_of_right_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntRect_t752847524::get_offset_of_bottom_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (ClipType_t1616702040)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3215[5] = 
{
	ClipType_t1616702040::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (PolyType_t1741373358)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3216[3] = 
{
	PolyType_t1741373358::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (PolyFillType_t2091732334)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3217[5] = 
{
	PolyFillType_t2091732334::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (JoinType_t3449044149)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3218[4] = 
{
	JoinType_t3449044149::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (EndType_t3515135373)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3219[6] = 
{
	EndType_t3515135373::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (EdgeSide_t2739901735)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3220[3] = 
{
	EdgeSide_t2739901735::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (Direction_t4237952965)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3221[3] = 
{
	Direction_t4237952965::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (TEdge_t1694054893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3222[18] = 
{
	TEdge_t1694054893::get_offset_of_Bot_0(),
	TEdge_t1694054893::get_offset_of_Curr_1(),
	TEdge_t1694054893::get_offset_of_Top_2(),
	TEdge_t1694054893::get_offset_of_Delta_3(),
	TEdge_t1694054893::get_offset_of_Dx_4(),
	TEdge_t1694054893::get_offset_of_PolyTyp_5(),
	TEdge_t1694054893::get_offset_of_Side_6(),
	TEdge_t1694054893::get_offset_of_WindDelta_7(),
	TEdge_t1694054893::get_offset_of_WindCnt_8(),
	TEdge_t1694054893::get_offset_of_WindCnt2_9(),
	TEdge_t1694054893::get_offset_of_OutIdx_10(),
	TEdge_t1694054893::get_offset_of_Next_11(),
	TEdge_t1694054893::get_offset_of_Prev_12(),
	TEdge_t1694054893::get_offset_of_NextInLML_13(),
	TEdge_t1694054893::get_offset_of_NextInAEL_14(),
	TEdge_t1694054893::get_offset_of_PrevInAEL_15(),
	TEdge_t1694054893::get_offset_of_NextInSEL_16(),
	TEdge_t1694054893::get_offset_of_PrevInSEL_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (IntersectNode_t3379514219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3223[3] = 
{
	IntersectNode_t3379514219::get_offset_of_Edge1_0(),
	IntersectNode_t3379514219::get_offset_of_Edge2_1(),
	IntersectNode_t3379514219::get_offset_of_Pt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (MyIntersectNodeSort_t682547759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (LocalMinima_t86068969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[4] = 
{
	LocalMinima_t86068969::get_offset_of_Y_0(),
	LocalMinima_t86068969::get_offset_of_LeftBound_1(),
	LocalMinima_t86068969::get_offset_of_RightBound_2(),
	LocalMinima_t86068969::get_offset_of_Next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (Scanbeam_t3952834741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[2] = 
{
	Scanbeam_t3952834741::get_offset_of_Y_0(),
	Scanbeam_t3952834741::get_offset_of_Next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (Maxima_t4278896992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[3] = 
{
	Maxima_t4278896992::get_offset_of_X_0(),
	Maxima_t4278896992::get_offset_of_Next_1(),
	Maxima_t4278896992::get_offset_of_Prev_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (OutRec_t316877671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[7] = 
{
	OutRec_t316877671::get_offset_of_Idx_0(),
	OutRec_t316877671::get_offset_of_IsHole_1(),
	OutRec_t316877671::get_offset_of_IsOpen_2(),
	OutRec_t316877671::get_offset_of_FirstLeft_3(),
	OutRec_t316877671::get_offset_of_Pts_4(),
	OutRec_t316877671::get_offset_of_BottomPt_5(),
	OutRec_t316877671::get_offset_of_PolyNode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (OutPt_t2591102706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[4] = 
{
	OutPt_t2591102706::get_offset_of_Idx_0(),
	OutPt_t2591102706::get_offset_of_Pt_1(),
	OutPt_t2591102706::get_offset_of_Next_2(),
	OutPt_t2591102706::get_offset_of_Prev_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (Join_t2349011362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[3] = 
{
	Join_t2349011362::get_offset_of_OutPt1_0(),
	Join_t2349011362::get_offset_of_OutPt2_1(),
	Join_t2349011362::get_offset_of_OffPt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (ClipperBase_t2411222589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	ClipperBase_t2411222589::get_offset_of_m_MinimaList_6(),
	ClipperBase_t2411222589::get_offset_of_m_CurrentLM_7(),
	ClipperBase_t2411222589::get_offset_of_m_edges_8(),
	ClipperBase_t2411222589::get_offset_of_m_Scanbeam_9(),
	ClipperBase_t2411222589::get_offset_of_m_PolyOuts_10(),
	ClipperBase_t2411222589::get_offset_of_m_ActiveEdges_11(),
	ClipperBase_t2411222589::get_offset_of_m_UseFullRange_12(),
	ClipperBase_t2411222589::get_offset_of_m_HasOpenPaths_13(),
	ClipperBase_t2411222589::get_offset_of_U3CPreserveCollinearU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (Clipper_t4158555122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[16] = 
{
	0,
	0,
	0,
	Clipper_t4158555122::get_offset_of_m_ClipType_18(),
	Clipper_t4158555122::get_offset_of_m_Maxima_19(),
	Clipper_t4158555122::get_offset_of_m_SortedEdges_20(),
	Clipper_t4158555122::get_offset_of_m_IntersectList_21(),
	Clipper_t4158555122::get_offset_of_m_IntersectNodeComparer_22(),
	Clipper_t4158555122::get_offset_of_m_ExecuteLocked_23(),
	Clipper_t4158555122::get_offset_of_m_ClipFillType_24(),
	Clipper_t4158555122::get_offset_of_m_SubjFillType_25(),
	Clipper_t4158555122::get_offset_of_m_Joins_26(),
	Clipper_t4158555122::get_offset_of_m_GhostJoins_27(),
	Clipper_t4158555122::get_offset_of_m_UsingPolyTree_28(),
	Clipper_t4158555122::get_offset_of_U3CReverseSolutionU3Ek__BackingField_29(),
	Clipper_t4158555122::get_offset_of_U3CStrictlySimpleU3Ek__BackingField_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (NodeType_t363087472)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3233[4] = 
{
	NodeType_t363087472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (ClipperOffset_t3668738110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3234[16] = 
{
	ClipperOffset_t3668738110::get_offset_of_m_destPolys_0(),
	ClipperOffset_t3668738110::get_offset_of_m_srcPoly_1(),
	ClipperOffset_t3668738110::get_offset_of_m_destPoly_2(),
	ClipperOffset_t3668738110::get_offset_of_m_normals_3(),
	ClipperOffset_t3668738110::get_offset_of_m_delta_4(),
	ClipperOffset_t3668738110::get_offset_of_m_sinA_5(),
	ClipperOffset_t3668738110::get_offset_of_m_sin_6(),
	ClipperOffset_t3668738110::get_offset_of_m_cos_7(),
	ClipperOffset_t3668738110::get_offset_of_m_miterLim_8(),
	ClipperOffset_t3668738110::get_offset_of_m_StepsPerRad_9(),
	ClipperOffset_t3668738110::get_offset_of_m_lowest_10(),
	ClipperOffset_t3668738110::get_offset_of_m_polyNodes_11(),
	ClipperOffset_t3668738110::get_offset_of_U3CArcToleranceU3Ek__BackingField_12(),
	ClipperOffset_t3668738110::get_offset_of_U3CMiterLimitU3Ek__BackingField_13(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (ClipperException_t3118674656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (DecodeGeometry_t3735437420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (GeomType_t3056663235)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3237[5] = 
{
	GeomType_t3056663235::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (LatLng_t1304626312)+ sizeof (RuntimeObject), sizeof(LatLng_t1304626312 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3238[2] = 
{
	LatLng_t1304626312::get_offset_of_U3CLatU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LatLng_t1304626312::get_offset_of_U3CLngU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (UtilGeom_t2066125609), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (WireTypes_t1504741901)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3241[6] = 
{
	WireTypes_t1504741901::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (Commands_t1803779524)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3242[4] = 
{
	Commands_t1803779524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (TileType_t3106966029)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3243[2] = 
{
	TileType_t3106966029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (LayerType_t1746409905)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3244[7] = 
{
	LayerType_t1746409905::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (FeatureType_t2360609914)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3245[6] = 
{
	FeatureType_t2360609914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (ValueType_t2776630785)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3246[8] = 
{
	ValueType_t2776630785::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (ConstantsAsDictionary_t107503724), -1, sizeof(ConstantsAsDictionary_t107503724_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3247[4] = 
{
	ConstantsAsDictionary_t107503724_StaticFields::get_offset_of_TileType_0(),
	ConstantsAsDictionary_t107503724_StaticFields::get_offset_of_LayerType_1(),
	ConstantsAsDictionary_t107503724_StaticFields::get_offset_of_FeatureType_2(),
	ConstantsAsDictionary_t107503724_StaticFields::get_offset_of_GeomType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (PbfReader_t1662343237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3248[6] = 
{
	PbfReader_t1662343237::get_offset_of_U3CTagU3Ek__BackingField_0(),
	PbfReader_t1662343237::get_offset_of_U3CValueU3Ek__BackingField_1(),
	PbfReader_t1662343237::get_offset_of_U3CWireTypeU3Ek__BackingField_2(),
	PbfReader_t1662343237::get_offset_of__buffer_3(),
	PbfReader_t1662343237::get_offset_of__length_4(),
	PbfReader_t1662343237::get_offset_of__pos_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (VectorTile_t3467883484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[1] = 
{
	VectorTile_t3467883484::get_offset_of__VTR_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (VectorTileFeature_t4093669591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3250[9] = 
{
	VectorTileFeature_t4093669591::get_offset_of__layer_0(),
	VectorTileFeature_t4093669591::get_offset_of__cachedGeometry_1(),
	VectorTileFeature_t4093669591::get_offset_of__clipBuffer_2(),
	VectorTileFeature_t4093669591::get_offset_of__scale_3(),
	VectorTileFeature_t4093669591::get_offset_of__previousScale_4(),
	VectorTileFeature_t4093669591::get_offset_of_U3CIdU3Ek__BackingField_5(),
	VectorTileFeature_t4093669591::get_offset_of_U3CGeometryTypeU3Ek__BackingField_6(),
	VectorTileFeature_t4093669591::get_offset_of_U3CGeometryCommandsU3Ek__BackingField_7(),
	VectorTileFeature_t4093669591::get_offset_of_U3CTagsU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (VectorTileLayer_t873169949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3251[7] = 
{
	VectorTileLayer_t873169949::get_offset_of_U3CDataU3Ek__BackingField_0(),
	VectorTileLayer_t873169949::get_offset_of_U3CNameU3Ek__BackingField_1(),
	VectorTileLayer_t873169949::get_offset_of_U3CVersionU3Ek__BackingField_2(),
	VectorTileLayer_t873169949::get_offset_of_U3CExtentU3Ek__BackingField_3(),
	VectorTileLayer_t873169949::get_offset_of_U3C_FeaturesDataU3Ek__BackingField_4(),
	VectorTileLayer_t873169949::get_offset_of_U3CValuesU3Ek__BackingField_5(),
	VectorTileLayer_t873169949::get_offset_of_U3CKeysU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (VectorTileReader_t1753322980), -1, sizeof(VectorTileReader_t1753322980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3252[5] = 
{
	VectorTileReader_t1753322980::get_offset_of__Layers_0(),
	VectorTileReader_t1753322980::get_offset_of__Validate_1(),
	VectorTileReader_t1753322980_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	VectorTileReader_t1753322980_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	VectorTileReader_t1753322980_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (BlockType_t3928626740)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3253[4] = 
{
	BlockType_t3928626740::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (CompressionLevel_t3212615018)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3254[4] = 
{
	CompressionLevel_t3212615018::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (CompressionMode_t2055613304)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3255[3] = 
{
	CompressionMode_t2055613304::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (CompressionTracingSwitchLevel_t3010065102)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3256[4] = 
{
	CompressionTracingSwitchLevel_t3010065102::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (CompressionTracingSwitch_t2383688933), -1, sizeof(CompressionTracingSwitch_t2383688933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3257[1] = 
{
	CompressionTracingSwitch_t2383688933_StaticFields::get_offset_of_tracingSwitch_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (CopyEncoder_t1719707359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (Crc32Helper_t2911080199), -1, sizeof(Crc32Helper_t2911080199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3259[1] = 
{
	Crc32Helper_t2911080199_StaticFields::get_offset_of_crcTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (DeflateInput_t3892891873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3260[3] = 
{
	DeflateInput_t3892891873::get_offset_of_buffer_0(),
	DeflateInput_t3892891873::get_offset_of_count_1(),
	DeflateInput_t3892891873::get_offset_of_startIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (InputState_t2211247823)+ sizeof (RuntimeObject), sizeof(InputState_t2211247823 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3261[2] = 
{
	InputState_t2211247823::get_offset_of_count_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputState_t2211247823::get_offset_of_startIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (DeflaterManaged_t1452220717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3262[10] = 
{
	0,
	0,
	0,
	0,
	DeflaterManaged_t1452220717::get_offset_of_deflateEncoder_4(),
	DeflaterManaged_t1452220717::get_offset_of_copyEncoder_5(),
	DeflaterManaged_t1452220717::get_offset_of_input_6(),
	DeflaterManaged_t1452220717::get_offset_of_output_7(),
	DeflaterManaged_t1452220717::get_offset_of_processingState_8(),
	DeflaterManaged_t1452220717::get_offset_of_inputFromHistory_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (DeflaterState_t438385491)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3263[8] = 
{
	DeflaterState_t438385491::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (DeflateStream_t2796728099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3264[13] = 
{
	0,
	DeflateStream_t2796728099::get_offset_of__stream_2(),
	DeflateStream_t2796728099::get_offset_of__mode_3(),
	DeflateStream_t2796728099::get_offset_of__leaveOpen_4(),
	DeflateStream_t2796728099::get_offset_of_inflater_5(),
	DeflateStream_t2796728099::get_offset_of_deflater_6(),
	DeflateStream_t2796728099::get_offset_of_buffer_7(),
	DeflateStream_t2796728099::get_offset_of_asyncOperations_8(),
	DeflateStream_t2796728099::get_offset_of_m_CallBack_9(),
	DeflateStream_t2796728099::get_offset_of_m_AsyncWriterDelegate_10(),
	DeflateStream_t2796728099::get_offset_of_formatWriter_11(),
	DeflateStream_t2796728099::get_offset_of_wroteHeader_12(),
	DeflateStream_t2796728099::get_offset_of_wroteBytes_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (AsyncWriteDelegate_t47908213), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (WorkerType_t976665898)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3266[3] = 
{
	WorkerType_t976665898::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (DeflateStreamAsyncResult_t1893920535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3267[12] = 
{
	DeflateStreamAsyncResult_t1893920535::get_offset_of_buffer_0(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_offset_1(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_count_2(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_isWrite_3(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_AsyncObject_4(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_AsyncState_5(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_AsyncCallback_6(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_Result_7(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_CompletedSynchronously_8(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_InvokedCallback_9(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_Completed_10(),
	DeflateStreamAsyncResult_t1893920535::get_offset_of_m_Event_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (FastEncoder_t887805019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3268[3] = 
{
	FastEncoder_t887805019::get_offset_of_inputWindow_0(),
	FastEncoder_t887805019::get_offset_of_currentMatch_1(),
	FastEncoder_t887805019::get_offset_of_lastCompressionRatio_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (FastEncoderStatics_t1128548993), -1, sizeof(FastEncoderStatics_t1128548993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3269[18] = 
{
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_FastEncoderTreeStructureData_0(),
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_BFinalFastEncoderTreeStructureData_1(),
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_FastEncoderLiteralCodeInfo_2(),
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_FastEncoderDistanceCodeInfo_3(),
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_BitMask_4(),
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_ExtraLengthBits_5(),
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_ExtraDistanceBits_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FastEncoderStatics_t1128548993_StaticFields::get_offset_of_distLookup_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (FastEncoderWindow_t57591660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3270[17] = 
{
	FastEncoderWindow_t57591660::get_offset_of_window_0(),
	FastEncoderWindow_t57591660::get_offset_of_bufPos_1(),
	FastEncoderWindow_t57591660::get_offset_of_bufEnd_2(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FastEncoderWindow_t57591660::get_offset_of_prev_15(),
	FastEncoderWindow_t57591660::get_offset_of_lookup_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (GZipDecoder_t3447631347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3273[9] = 
{
	GZipDecoder_t3447631347::get_offset_of_gzipHeaderSubstate_0(),
	GZipDecoder_t3447631347::get_offset_of_gzipFooterSubstate_1(),
	GZipDecoder_t3447631347::get_offset_of_gzip_header_flag_2(),
	GZipDecoder_t3447631347::get_offset_of_gzip_header_xlen_3(),
	GZipDecoder_t3447631347::get_offset_of_expectedCrc32_4(),
	GZipDecoder_t3447631347::get_offset_of_expectedOutputStreamSizeModulo_5(),
	GZipDecoder_t3447631347::get_offset_of_loopCounter_6(),
	GZipDecoder_t3447631347::get_offset_of_actualCrc32_7(),
	GZipDecoder_t3447631347::get_offset_of_actualStreamSizeModulo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (GzipHeaderState_t2287451846)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3274[18] = 
{
	GzipHeaderState_t2287451846::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (GZipOptionalHeaderFlags_t2076442620)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3275[5] = 
{
	GZipOptionalHeaderFlags_t2076442620::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (GZipStream_t1509501570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3276[1] = 
{
	GZipStream_t1509501570::get_offset_of_deflateStream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (GZipConstants_t1076992185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3277[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (GZipFormatter_t4203637370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3278[3] = 
{
	GZipFormatter_t4203637370::get_offset_of_headerBytes_0(),
	GZipFormatter_t4203637370::get_offset_of__crc32_1(),
	GZipFormatter_t4203637370::get_offset_of__inputStreamSizeModulo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (HuffmanTree_t857975559), -1, sizeof(HuffmanTree_t857975559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3279[12] = 
{
	0,
	0,
	0,
	0,
	HuffmanTree_t857975559::get_offset_of_tableBits_4(),
	HuffmanTree_t857975559::get_offset_of_table_5(),
	HuffmanTree_t857975559::get_offset_of_left_6(),
	HuffmanTree_t857975559::get_offset_of_right_7(),
	HuffmanTree_t857975559::get_offset_of_codeLengthArray_8(),
	HuffmanTree_t857975559::get_offset_of_tableMask_9(),
	HuffmanTree_t857975559_StaticFields::get_offset_of_staticLiteralLengthTree_10(),
	HuffmanTree_t857975559_StaticFields::get_offset_of_staticDistanceTree_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (Inflater_t10910524), -1, sizeof(Inflater_t10910524_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3281[28] = 
{
	Inflater_t10910524_StaticFields::get_offset_of_extraLengthBits_0(),
	Inflater_t10910524_StaticFields::get_offset_of_lengthBase_1(),
	Inflater_t10910524_StaticFields::get_offset_of_distanceBasePosition_2(),
	Inflater_t10910524_StaticFields::get_offset_of_codeOrder_3(),
	Inflater_t10910524_StaticFields::get_offset_of_staticDistanceTreeTable_4(),
	Inflater_t10910524::get_offset_of_output_5(),
	Inflater_t10910524::get_offset_of_input_6(),
	Inflater_t10910524::get_offset_of_literalLengthTree_7(),
	Inflater_t10910524::get_offset_of_distanceTree_8(),
	Inflater_t10910524::get_offset_of_state_9(),
	Inflater_t10910524::get_offset_of_hasFormatReader_10(),
	Inflater_t10910524::get_offset_of_bfinal_11(),
	Inflater_t10910524::get_offset_of_blockType_12(),
	Inflater_t10910524::get_offset_of_blockLengthBuffer_13(),
	Inflater_t10910524::get_offset_of_blockLength_14(),
	Inflater_t10910524::get_offset_of_length_15(),
	Inflater_t10910524::get_offset_of_distanceCode_16(),
	Inflater_t10910524::get_offset_of_extraBits_17(),
	Inflater_t10910524::get_offset_of_loopCounter_18(),
	Inflater_t10910524::get_offset_of_literalLengthCodeCount_19(),
	Inflater_t10910524::get_offset_of_distanceCodeCount_20(),
	Inflater_t10910524::get_offset_of_codeLengthCodeCount_21(),
	Inflater_t10910524::get_offset_of_codeArraySize_22(),
	Inflater_t10910524::get_offset_of_lengthCode_23(),
	Inflater_t10910524::get_offset_of_codeList_24(),
	Inflater_t10910524::get_offset_of_codeLengthTreeCodeLength_25(),
	Inflater_t10910524::get_offset_of_codeLengthTree_26(),
	Inflater_t10910524::get_offset_of_formatReader_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (InflaterState_t2157501130)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3282[24] = 
{
	InflaterState_t2157501130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (InputBuffer_t333709416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[5] = 
{
	InputBuffer_t333709416::get_offset_of_buffer_0(),
	InputBuffer_t333709416::get_offset_of_start_1(),
	InputBuffer_t333709416::get_offset_of_end_2(),
	InputBuffer_t333709416::get_offset_of_bitBuffer_3(),
	InputBuffer_t333709416::get_offset_of_bitsInBuffer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (InvalidDataException_t4045251031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (Match_t352475233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3285[4] = 
{
	Match_t352475233::get_offset_of_state_0(),
	Match_t352475233::get_offset_of_pos_1(),
	Match_t352475233::get_offset_of_len_2(),
	Match_t352475233::get_offset_of_symbol_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (MatchState_t635901724)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3286[4] = 
{
	MatchState_t635901724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (OutputBuffer_t1331609326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3287[4] = 
{
	OutputBuffer_t1331609326::get_offset_of_byteBuffer_0(),
	OutputBuffer_t1331609326::get_offset_of_pos_1(),
	OutputBuffer_t1331609326::get_offset_of_bitBuf_2(),
	OutputBuffer_t1331609326::get_offset_of_bitCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (BufferState_t2134702710)+ sizeof (RuntimeObject), sizeof(BufferState_t2134702710 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3288[3] = 
{
	BufferState_t2134702710::get_offset_of_pos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BufferState_t2134702710::get_offset_of_bitBuf_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BufferState_t2134702710::get_offset_of_bitCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (OutputWindow_t1296654655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3289[5] = 
{
	0,
	0,
	OutputWindow_t1296654655::get_offset_of_window_2(),
	OutputWindow_t1296654655::get_offset_of_end_3(),
	OutputWindow_t1296654655::get_offset_of_bytesUsed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (SR_t2098860177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3290[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (DragRotate_t2912444650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[3] = 
{
	DragRotate_t2912444650::get_offset_of__objectToRotate_2(),
	DragRotate_t2912444650::get_offset_of__multiplier_3(),
	DragRotate_t2912444650::get_offset_of__startTouchPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (SpawnOnGlobeExample_t1835218885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[3] = 
{
	SpawnOnGlobeExample_t1835218885::get_offset_of__globeFactory_2(),
	SpawnOnGlobeExample_t1835218885::get_offset_of__locations_3(),
	SpawnOnGlobeExample_t1835218885::get_offset_of__spawnScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (ReloadMap_t3484436943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[5] = 
{
	ReloadMap_t3484436943::get_offset_of__camera_2(),
	ReloadMap_t3484436943::get_offset_of__cameraStartPos_3(),
	ReloadMap_t3484436943::get_offset_of__map_4(),
	ReloadMap_t3484436943::get_offset_of__forwardGeocoder_5(),
	ReloadMap_t3484436943::get_offset_of__zoomSlider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (DirectionsExample_t2773998098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3294[6] = 
{
	DirectionsExample_t2773998098::get_offset_of__resultsText_2(),
	DirectionsExample_t2773998098::get_offset_of__startLocationGeocoder_3(),
	DirectionsExample_t2773998098::get_offset_of__endLocationGeocoder_4(),
	DirectionsExample_t2773998098::get_offset_of__directions_5(),
	DirectionsExample_t2773998098::get_offset_of__coordinates_6(),
	DirectionsExample_t2773998098::get_offset_of__directionResource_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (ForwardGeocoderExample_t595455162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3295[2] = 
{
	ForwardGeocoderExample_t595455162::get_offset_of__searchLocation_2(),
	ForwardGeocoderExample_t595455162::get_offset_of__resultsText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (RasterTileExample_t3949613556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[9] = 
{
	RasterTileExample_t3949613556::get_offset_of__searchLocation_2(),
	RasterTileExample_t3949613556::get_offset_of__zoomSlider_3(),
	RasterTileExample_t3949613556::get_offset_of__stylesDropdown_4(),
	RasterTileExample_t3949613556::get_offset_of__imageContainer_5(),
	RasterTileExample_t3949613556::get_offset_of__map_6(),
	RasterTileExample_t3949613556::get_offset_of__latLon_7(),
	RasterTileExample_t3949613556::get_offset_of__mapboxStyles_8(),
	RasterTileExample_t3949613556::get_offset_of__startLoc_9(),
	RasterTileExample_t3949613556::get_offset_of__mapstyle_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (ReverseGeocoderExample_t1816435679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3297[2] = 
{
	ReverseGeocoderExample_t1816435679::get_offset_of__searchLocation_2(),
	ReverseGeocoderExample_t1816435679::get_offset_of__resultsText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (VectorTileExample_t4002429299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3298[3] = 
{
	VectorTileExample_t4002429299::get_offset_of__searchLocation_2(),
	VectorTileExample_t4002429299::get_offset_of__resultsText_3(),
	VectorTileExample_t4002429299::get_offset_of__map_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (CameraMovement_t3562026478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3299[7] = 
{
	CameraMovement_t3562026478::get_offset_of__panSpeed_2(),
	CameraMovement_t3562026478::get_offset_of__zoomSpeed_3(),
	CameraMovement_t3562026478::get_offset_of__referenceCamera_4(),
	CameraMovement_t3562026478::get_offset_of__originalRotation_5(),
	CameraMovement_t3562026478::get_offset_of__origin_6(),
	CameraMovement_t3562026478::get_offset_of__delta_7(),
	CameraMovement_t3562026478::get_offset_of__shouldDrag_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
